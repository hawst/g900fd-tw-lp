.class public Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;
.super Ljava/lang/Object;
.source "ShareAccessibilitySettingsCommonFunction.java"


# static fields
.field private static RevColorBlindTestCheck:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ShareAccessibilitySettingsCommonFunction"

.field private static final THEME_TW_DARK:I = 0x0

.field private static final THEME_TW_LIGHT:I = 0x1

.field private static actionMenuTextColor:I

.field private static deviceThemeStyle:I

.field private static is_switchaccess:I

.field private static is_talkback:I

.field public static mDeviceType:Ljava/lang/String;

.field private static mReceiveCVDType:I

.field private static mReceiveCVDseverity:F

.field private static mReceiveDominant_hand_type:I

.field private static mReceivePad_size:I

.field private static mReceivePointer_size:I

.field private static mReceivePointer_speed:I

.field private static mReceiveUserParameter:F

.field private static mTTS_DEFAULT_RATE_VALUE:Ljava/lang/String;

.field private static mTtsFlag:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 65
    sput v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mTtsFlag:I

    .line 66
    sput v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->is_talkback:I

    .line 67
    sput v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->is_switchaccess:I

    .line 69
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mTTS_DEFAULT_RATE_VALUE:Ljava/lang/String;

    .line 89
    sput v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->RevColorBlindTestCheck:I

    .line 91
    sput v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveDominant_hand_type:I

    .line 93
    const/4 v0, 0x2

    sput v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceivePointer_speed:I

    .line 95
    sput v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceivePointer_size:I

    .line 97
    sput v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceivePad_size:I

    .line 99
    const/4 v0, 0x3

    sput v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveCVDType:I

    .line 101
    sput v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveCVDseverity:F

    .line 103
    sput v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveUserParameter:F

    .line 113
    sput v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->deviceThemeStyle:I

    .line 115
    sput v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->actionMenuTextColor:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static LoadValue(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 17
    .param p0, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;
        }
    .end annotation

    .prologue
    .line 260
    const/4 v9, 0x0

    .line 261
    .local v9, "loadValue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v9, Ljava/util/HashMap;

    .end local v9    # "loadValue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 264
    .restart local v9    # "loadValue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    const-string v14, "ShareAccessibilitySettingsCommonFunction"

    const-string v15, "loadValue is entered"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v6

    .line 268
    .local v6, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v6}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 272
    .local v2, "db":Ljavax/xml/parsers/DocumentBuilder;
    :try_start_1
    new-instance v14, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    sput-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->fis:Ljava/io/FileInputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/StreamCorruptedException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 277
    :goto_0
    const/4 v3, 0x0

    .line 279
    .local v3, "doc":Lorg/w3c/dom/Document;
    :try_start_2
    sget-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v2, v14}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    :try_end_2
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .line 299
    :try_start_3
    invoke-interface {v3}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v14

    invoke-interface {v14}, Lorg/w3c/dom/Element;->normalize()V

    .line 301
    const-string v14, "SharingAccessibilitySettings"

    invoke-interface {v3, v14}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v12

    .line 303
    .local v12, "rootNodes":Lorg/w3c/dom/NodeList;
    const/4 v14, 0x0

    invoke-interface {v12, v14}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    if-nez v14, :cond_0

    .line 304
    const-string v14, "ShareAccessibilitySettingsCommonFunction"

    const-string v15, "It is not valid sharing accessibility settings file"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 307
    :try_start_4
    sget-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_8
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 347
    :goto_1
    :try_start_5
    sget-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_b

    .line 353
    .end local v2    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v3    # "doc":Lorg/w3c/dom/Document;
    .end local v6    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v12    # "rootNodes":Lorg/w3c/dom/NodeList;
    :goto_2
    return-object v9

    .line 273
    .restart local v2    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v6    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :catch_0
    move-exception v4

    .line 274
    .local v4, "e":Ljava/io/FileNotFoundException;
    :try_start_6
    const-string v14, "ShareAccessibilitySettingsCommonFunction"

    const-string v15, "FileNotFoundException : can\'t create FileInputStream"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_9
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 336
    .end local v2    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    .end local v6    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :catch_1
    move-exception v4

    .line 338
    .restart local v4    # "e":Ljava/io/FileNotFoundException;
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 347
    :try_start_8
    sget-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_2

    .line 348
    :catch_2
    move-exception v4

    .line 350
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 280
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v2    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v3    # "doc":Lorg/w3c/dom/Document;
    .restart local v6    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :catch_3
    move-exception v4

    .line 282
    .local v4, "e":Lorg/xml/sax/SAXException;
    :try_start_9
    const-string v14, "ShareAccessibilitySettingsCommonFunction"

    const-string v15, "It is not valid sharing accessibility settings file"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 285
    :try_start_a
    sget-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 292
    :goto_3
    :try_start_b
    const-string v14, "ShareAccessibilitySettingsCommonFunction"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "not xml file. loadValue is : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_b .. :try_end_b} :catch_6
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 347
    :try_start_c
    sget-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4

    goto :goto_2

    .line 348
    :catch_4
    move-exception v4

    .line 350
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 286
    .local v4, "e":Lorg/xml/sax/SAXException;
    :catch_5
    move-exception v5

    .line 288
    .local v5, "e1":Ljava/io/IOException;
    :try_start_d
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_d .. :try_end_d} :catch_6
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_3

    .line 339
    .end local v2    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v3    # "doc":Lorg/w3c/dom/Document;
    .end local v4    # "e":Lorg/xml/sax/SAXException;
    .end local v5    # "e1":Ljava/io/IOException;
    .end local v6    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :catch_6
    move-exception v4

    .line 341
    .local v4, "e":Ljava/io/StreamCorruptedException;
    :try_start_e
    invoke-virtual {v4}, Ljava/io/StreamCorruptedException;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 347
    :try_start_f
    sget-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_7

    goto :goto_2

    .line 348
    :catch_7
    move-exception v4

    .line 350
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 308
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v2    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v3    # "doc":Lorg/w3c/dom/Document;
    .restart local v6    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v12    # "rootNodes":Lorg/w3c/dom/NodeList;
    :catch_8
    move-exception v4

    .line 310
    .restart local v4    # "e":Ljava/io/IOException;
    :try_start_10
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_10 .. :try_end_10} :catch_6
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_9
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_1

    .line 342
    .end local v2    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v3    # "doc":Lorg/w3c/dom/Document;
    .end local v4    # "e":Ljava/io/IOException;
    .end local v6    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v12    # "rootNodes":Lorg/w3c/dom/NodeList;
    :catch_9
    move-exception v4

    .line 344
    .restart local v4    # "e":Ljava/io/IOException;
    :try_start_11
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 347
    :try_start_12
    sget-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_a

    goto :goto_2

    .line 348
    :catch_a
    move-exception v4

    .line 350
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 318
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v2    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v3    # "doc":Lorg/w3c/dom/Document;
    .restart local v6    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v12    # "rootNodes":Lorg/w3c/dom/NodeList;
    :cond_0
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_4
    :try_start_13
    sget-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    array-length v14, v14

    if-ge v8, v14, :cond_3

    .line 319
    sget-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v14, v14, v8

    invoke-interface {v3, v14}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    .line 320
    .local v7, "firstNodes":Lorg/w3c/dom/NodeList;
    const/4 v14, 0x0

    invoke-interface {v7, v14}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    .line 321
    .local v11, "node":Lorg/w3c/dom/Node;
    if-eqz v11, :cond_1

    .line 322
    invoke-interface {v11}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v10

    .line 323
    .local v10, "name":Ljava/lang/String;
    invoke-interface {v11}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v1

    .line 324
    .local v1, "childNode":Lorg/w3c/dom/Node;
    if-eqz v1, :cond_2

    .line 325
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v13

    .line 329
    .local v13, "value":Ljava/lang/String;
    :goto_5
    invoke-virtual {v9, v10, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    .end local v1    # "childNode":Lorg/w3c/dom/Node;
    .end local v10    # "name":Ljava/lang/String;
    .end local v13    # "value":Ljava/lang/String;
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 327
    .restart local v1    # "childNode":Lorg/w3c/dom/Node;
    .restart local v10    # "name":Ljava/lang/String;
    :cond_2
    const/4 v13, 0x0

    .restart local v13    # "value":Ljava/lang/String;
    goto :goto_5

    .line 333
    .end local v1    # "childNode":Lorg/w3c/dom/Node;
    .end local v7    # "firstNodes":Lorg/w3c/dom/NodeList;
    .end local v10    # "name":Ljava/lang/String;
    .end local v11    # "node":Lorg/w3c/dom/Node;
    .end local v13    # "value":Ljava/lang/String;
    :cond_3
    const-string v14, "ShareAccessibilitySettingsCommonFunction"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "loadValue : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_13
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_13} :catch_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_13 .. :try_end_13} :catch_6
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_9
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    goto/16 :goto_1

    .line 346
    .end local v2    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v3    # "doc":Lorg/w3c/dom/Document;
    .end local v6    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v8    # "i":I
    .end local v12    # "rootNodes":Lorg/w3c/dom/NodeList;
    :catchall_0
    move-exception v14

    .line 347
    :try_start_14
    sget-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->fis:Ljava/io/FileInputStream;

    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_c

    .line 351
    :goto_6
    throw v14

    .line 348
    .restart local v2    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v3    # "doc":Lorg/w3c/dom/Document;
    .restart local v6    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v12    # "rootNodes":Lorg/w3c/dom/NodeList;
    :catch_b
    move-exception v4

    .line 350
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 348
    .end local v2    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v3    # "doc":Lorg/w3c/dom/Document;
    .end local v4    # "e":Ljava/io/IOException;
    .end local v6    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v12    # "rootNodes":Lorg/w3c/dom/NodeList;
    :catch_c
    move-exception v4

    .line 350
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6
.end method

.method public static applySettings(Landroid/content/Context;Ljava/util/HashMap;)V
    .locals 11
    .param p0, "mContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "receivedSettingValue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 863
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    const-string v6, "applySettings entered"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    new-instance v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction$1;

    invoke-direct {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction$1;-><init>()V

    .line 872
    .local v2, "mInitListener":Landroid/speech/tts/TextToSpeech$OnInitListener;
    new-instance v3, Landroid/speech/tts/TextToSpeech;

    invoke-direct {v3, p0, v2}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    .line 874
    .local v3, "mTts":Landroid/speech/tts/TextToSpeech;
    sput v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mTtsFlag:I

    .line 875
    const/4 v5, 0x0

    sput-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mTTS_DEFAULT_RATE_VALUE:Ljava/lang/String;

    .line 877
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_4b

    .line 878
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_4a

    .line 880
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "long_press_timeout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 883
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1276
    :cond_0
    :goto_1
    :try_start_0
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ApplySetting: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v7, v7, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Value :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v8, v8, v1

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_8

    .line 877
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 885
    :cond_1
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_script_injection"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 887
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 889
    :cond_2
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_display_magnification_enabled"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 891
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 893
    :cond_3
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "speak_password"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 895
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 897
    :cond_4
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "lock_screen_lock_after_timeout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 899
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 901
    :cond_5
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "incall_power_button_behavior"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 905
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 907
    :cond_6
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "tts_engine"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 908
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "tts_default_synth"

    invoke-static {v5, v6}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    .line 910
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v3, v2, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setTtsEngine(Landroid/content/Context;Landroid/speech/tts/TextToSpeech;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    sget v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mTtsFlag:I

    if-ne v5, v10, :cond_0

    .line 915
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mTTS_DEFAULT_RATE_VALUE:Ljava/lang/String;

    if-eqz v5, :cond_7

    .line 916
    const-string v5, "tts_default_rate"

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mTTS_DEFAULT_RATE_VALUE:Ljava/lang/String;

    invoke-static {p0, v3, v5, v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setTtsRate(Landroid/content/Context;Landroid/speech/tts/TextToSpeech;Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    sput v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mTtsFlag:I

    goto/16 :goto_1

    .line 920
    :cond_7
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    const-string v6, "mTTS_DEFAULT_RATE_VALUE is null"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 924
    :cond_8
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "tts_default_rate"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 926
    const/4 v5, 0x1

    :try_start_1
    sput v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mTtsFlag:I

    .line 927
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    sput-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mTTS_DEFAULT_RATE_VALUE:Ljava/lang/String;

    .line 929
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v5, v6}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 931
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v3, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setTtsRate(Landroid/content/Context;Landroid/speech/tts/TextToSpeech;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 933
    :catch_0
    move-exception v0

    .line 934
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 935
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v3, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setTtsRate(Landroid/content/Context;Landroid/speech/tts/TextToSpeech;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 939
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_9
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "anykey_mode"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 941
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 943
    :cond_a
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "answering_bring_to_ear"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 945
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 947
    :cond_b
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "answering_accessibility_tapping"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 949
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 951
    :cond_c
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "font_size"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 952
    const-string v5, "font_scale"

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_d

    .line 955
    :try_start_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 957
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v7, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, "font_scale"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {p0, v7, v5, v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setFontSize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1

    .line 961
    :catch_1
    move-exception v0

    .line 962
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 963
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v7, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, "font_scale"

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {p0, v7, v5, v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setFontSize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 972
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_d
    :try_start_3
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 974
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "2"

    const-string v7, "1.0"

    invoke-static {p0, v5, v6, v7}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setFontSize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    .line 976
    :catch_2
    move-exception v0

    .line 977
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 978
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "2"

    const-string v7, "1.0"

    invoke-static {p0, v5, v6, v7}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setFontSize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 982
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_e
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "font_scale"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 984
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "mono_audio_db"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 987
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setMonoAudio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 990
    :cond_f
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "all_sound_off"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 993
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setTurnOffAllSound(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 995
    :cond_10
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "assistant_menu"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 997
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setAssistantMenu(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1001
    :cond_11
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "assistant_menu_dominant_hand_type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 1004
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveDominant_hand_type:I

    .line 1007
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mReceiveDominant_hand_type"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveDominant_hand_type:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1009
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveDominant_hand_type:I

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 1013
    :cond_12
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "assistant_menu_pointer_speed"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 1016
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceivePointer_speed:I

    .line 1019
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mReceivePointer_speed"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceivePointer_speed:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1021
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceivePointer_speed:I

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 1024
    :cond_13
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "assistant_menu_pointer_size"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1027
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceivePointer_size:I

    .line 1030
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mReceivePointer_size"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceivePointer_size:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1032
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceivePointer_size:I

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 1035
    :cond_14
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "assistant_menu_pad_size"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1038
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceivePad_size:I

    .line 1041
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mReceivePad_size"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceivePad_size:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1043
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceivePad_size:I

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 1047
    :cond_15
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "high_contrast"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 1049
    :try_start_4
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 1051
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setNegativeColour(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_1

    .line 1053
    :catch_3
    move-exception v0

    .line 1054
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 1055
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setNegativeColour(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1059
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_16
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_enabled"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 1062
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setAccessibilityEnabled(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1065
    :cond_17
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "switch_access_key"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 1066
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSwitchAccessEnabled(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1067
    :cond_18
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "color_blind"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 1068
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1070
    :cond_19
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "color_blind_test"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1074
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1076
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->RevColorBlindTestCheck:I

    goto/16 :goto_1

    .line 1078
    :cond_1a
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "color_blind_cvdtype"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 1080
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    sput v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveCVDType:I

    .line 1082
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mReceiveCVDType"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveCVDType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1083
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveCVDType:I

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 1085
    :cond_1b
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "color_blind_cvdseverity"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 1087
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    sput v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveCVDseverity:F

    .line 1089
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mReceiveCVDseverity"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveCVDseverity:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1090
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveCVDseverity:F

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    goto/16 :goto_1

    .line 1092
    :cond_1c
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "color_blind_user_parameter"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 1094
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    sput v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveUserParameter:F

    .line 1096
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mReceiveUserParameter"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveUserParameter:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1097
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    sget v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveUserParameter:F

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    goto/16 :goto_1

    .line 1099
    :cond_1d
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "speak_password"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 1101
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    sput v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->mReceiveUserParameter:F

    goto/16 :goto_1

    .line 1103
    :cond_1e
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "enable_accessibility_global_gesture_enabled"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 1105
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setGlobalSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1107
    :cond_1f
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "easy_interaction"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_20

    .line 1108
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1110
    :cond_20
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "lcd_curtain"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 1111
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1113
    :cond_21
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "notification_reminder"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_22

    .line 1114
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1116
    :cond_22
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "time_key"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_23

    .line 1117
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1119
    :cond_23
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "notification_reminder_selectable"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_24

    .line 1120
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1122
    :cond_24
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "time_key_selectable"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_25

    .line 1123
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1125
    :cond_25
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "notification_reminder_vibrate"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_26

    .line 1126
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1128
    :cond_26
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "notification_reminder_app_list"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_27

    .line 1129
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1131
    :cond_27
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "hearing_aid"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_28

    .line 1132
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setHearingAid(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1134
    :cond_28
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_captioning_enabled"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_29

    .line 1135
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1137
    :cond_29
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_sec_captioning_enabled"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2a

    .line 1138
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1140
    :cond_2a
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_captioning_font_scale"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2b

    .line 1141
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureFloatSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1143
    :cond_2b
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_captioning_preset"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2c

    .line 1144
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1146
    :cond_2c
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_captioning_typeface"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2d

    .line 1147
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureStringSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1149
    :cond_2d
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_captioning_foreground_color"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2e

    .line 1150
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1152
    :cond_2e
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_captioning_edge_type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2f

    .line 1153
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1155
    :cond_2f
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_captioning_edge_color"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_30

    .line 1156
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1158
    :cond_30
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_captioning_window_color"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_31

    .line 1159
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1161
    :cond_31
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_captioning_background_color"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_32

    .line 1162
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1164
    :cond_32
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_captioning_locale"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 1165
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSecureStringSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1167
    :cond_33
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "rapid_key_input"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_34

    .line 1168
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1170
    :cond_34
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "rapid_key_input_menu_checked"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_35

    .line 1171
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1173
    :cond_35
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "air_motion_wake_up"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_36

    .line 1174
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->broadcastAirWakeupChanged(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1176
    :cond_36
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "def_tactileassist_enable"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_37

    .line 1177
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setGlobalSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1179
    :cond_37
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "direct_access"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_38

    .line 1180
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1182
    :cond_38
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "direct_accessibility"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_39

    .line 1183
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1185
    :cond_39
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "direct_talkback"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3a

    .line 1186
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1188
    :cond_3a
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "direct_negative"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3b

    .line 1189
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1191
    :cond_3b
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "sound_detector"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3c

    .line 1192
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->soundDetector(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 1195
    :cond_3c
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "doorbell_detector"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3d

    .line 1196
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1198
    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->doorbellDetector(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 1199
    :cond_3d
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "accessibility_magnifier"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3e

    .line 1200
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1202
    :cond_3e
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "hover_zoom_value"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3f

    .line 1203
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1205
    :cond_3f
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "hover_zoom_magnifier_size"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_40

    .line 1206
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1208
    :cond_40
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "smart_scroll"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_41

    .line 1209
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1211
    :cond_41
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "face_smart_scroll"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_42

    .line 1212
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1214
    :cond_42
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "smart_scroll_sensitivity"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_43

    .line 1215
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1217
    :cond_43
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "smart_scroll_visual_feedback_icon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_44

    .line 1218
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1220
    :cond_44
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "VIB_NOTIFICATION_MAGNITUDE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_45

    .line 1221
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1223
    :cond_45
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "air_motion_call_accept"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_46

    .line 1225
    :try_start_5
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 1227
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->changeAirCallAccept(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_1

    .line 1229
    :catch_4
    move-exception v0

    .line 1230
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 1231
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->changeAirCallAccept(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1234
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_46
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "direct_access_control"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_47

    .line 1235
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1237
    :cond_47
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "audio_balance"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_48

    .line 1239
    :try_start_6
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "sound_balance"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 1241
    const-string v6, "sound_balance"

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_1

    .line 1243
    :catch_5
    move-exception v0

    .line 1244
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 1245
    const-string v6, "audio_balance"

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1248
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_48
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "sound_balance"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_49

    .line 1250
    :try_start_7
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "sound_balance"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 1252
    const-string v6, "sound_balance"

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_1

    .line 1254
    :catch_6
    move-exception v0

    .line 1255
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 1256
    const-string v6, "audio_balance"

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1261
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_49
    :try_start_8
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    .line 1263
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_8 .. :try_end_8} :catch_7

    goto/16 :goto_1

    .line 1265
    :catch_7
    move-exception v0

    .line 1266
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 1267
    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v5, v1

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0, v6, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1272
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_4a
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    const-string v6, "KeyList is null"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1283
    :catch_8
    move-exception v0

    .line 1285
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_2

    .line 1292
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_4b
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.samsung.android.app.shareaccessibilitysettings.SHARING_COMPLETE"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1294
    .local v4, "share_color_blind_value_intent":Landroid/content/Intent;
    invoke-virtual {p0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1296
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "share_color_blind_value_intent"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    const-string v5, "ShareAccessibilitySettingsCommonFunction"

    const-string v6, "ApplySetting is completed."

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1300
    return-void
.end method

.method public static autoTurnOffAirMotionEngine(Landroid/content/Context;)V
    .locals 4
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 1448
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isAllAirMotionDisabled2014(Landroid/content/Context;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_motion_call_accept"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "master_motion"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 1450
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_motion_engine"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1451
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.AIR_MOTION_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1452
    .local v0, "motion_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1453
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1455
    .end local v0    # "motion_changed":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method public static broadcastAirCallAcceptChanged(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "isEnable"    # Z

    .prologue
    .line 1531
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.AIR_CALL_ACCEPT_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1532
    .local v0, "motion_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1533
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1534
    return-void
.end method

.method public static broadcastAirWakeupChanged(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 1421
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, p1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1423
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.AIR_WAKE_UP_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1424
    .local v0, "motion_changed":Landroid/content/Intent;
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 1425
    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->turnOnAirMotionEngine(Landroid/content/Context;)V

    .line 1426
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1427
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1433
    :goto_0
    return-void

    .line 1429
    :cond_0
    const-string v1, "isEnable"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1430
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1431
    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->autoTurnOffAirMotionEngine(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static changeAirCallAccept(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1537
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1538
    .local v0, "call_accept":I
    if-ne v0, v3, :cond_0

    .line 1539
    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->turnOnAirMotionEngine(Landroid/content/Context;)V

    .line 1540
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_motion_call_accept"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1541
    invoke-static {p0, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->broadcastAirCallAcceptChanged(Landroid/content/Context;Z)V

    .line 1546
    :goto_0
    return-void

    .line 1543
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_motion_call_accept"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1544
    invoke-static {p0, v4}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->broadcastAirCallAcceptChanged(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public static createFolder(Ljava/lang/String;)V
    .locals 4
    .param p0, "strFolderPath"    # Ljava/lang/String;

    .prologue
    .line 220
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 221
    .local v0, "accFolder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 222
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 225
    const-string v1, "saveValueToFile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isDirectory : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 227
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    goto :goto_0
.end method

.method public static doorbellDetector(Landroid/content/Context;)V
    .locals 2
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 1553
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.settings.action.doorbell_detector"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1554
    .local v0, "mIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1555
    return-void
.end method

.method public static getAvailableExternalMemorySize(Landroid/content/Context;)J
    .locals 14
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 1876
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    .line 1877
    .local v3, "mExternalStorageSdPath":Ljava/lang/String;
    const-wide/16 v0, 0x0

    .line 1878
    .local v0, "freeSize":J
    const-string v9, "storage"

    invoke-virtual {p0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/storage/StorageManager;

    .line 1879
    .local v5, "storageManager":Landroid/os/storage/StorageManager;
    invoke-virtual {v5}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v6

    .line 1880
    .local v6, "storageVolumes":[Landroid/os/storage/StorageVolume;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v9, v6

    if-ge v2, v9, :cond_1

    .line 1881
    aget-object v8, v6, v2

    .line 1882
    .local v8, "volume":Landroid/os/storage/StorageVolume;
    const-string v9, "sd"

    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->isRemovable()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1883
    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 1884
    invoke-virtual {v5, v3}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1885
    .local v7, "strMountState":Ljava/lang/String;
    const-string v9, "mounted"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_0

    .line 1886
    new-instance v4, Landroid/os/StatFs;

    invoke-direct {v4, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1887
    .local v4, "stats":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v9

    int-to-long v12, v9

    mul-long v0, v10, v12

    .line 1880
    .end local v4    # "stats":Landroid/os/StatFs;
    .end local v7    # "strMountState":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1892
    .end local v8    # "volume":Landroid/os/storage/StorageVolume;
    :cond_1
    return-wide v0
.end method

.method public static getAvailableInternalMemorySize()J
    .locals 8

    .prologue
    .line 1860
    const-wide/16 v0, 0x0

    .line 1861
    .local v0, "freeSize":J
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    .line 1862
    .local v2, "internalStorageState":Ljava/lang/String;
    const-string v4, "mounted"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "unmounted"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "mounted_ro"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1866
    :cond_0
    new-instance v3, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1867
    .local v3, "stats":Landroid/os/StatFs;
    invoke-virtual {v3}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v6, v6

    mul-long v0, v4, v6

    .line 1870
    .end local v3    # "stats":Landroid/os/StatFs;
    :cond_1
    return-wide v0
.end method

.method private static getEnabledServicesFromSettings(Landroid/content/Context;)Ljava/util/Set;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1775
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "enabled_accessibility_services"

    invoke-static {v5, v6}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1777
    .local v4, "enabledServicesSetting":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 1778
    const-string v4, ""

    .line 1780
    :cond_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1781
    .local v3, "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->sStringColonSplitter:Landroid/text/TextUtils$SimpleStringSplitter;

    .line 1782
    .local v0, "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v0, v4}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 1783
    :cond_1
    :goto_0
    invoke-virtual {v0}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1784
    invoke-virtual {v0}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v1

    .line 1785
    .local v1, "componentNameString":Ljava/lang/String;
    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    .line 1786
    .local v2, "enabledService":Landroid/content/ComponentName;
    if-eqz v2, :cond_1

    .line 1787
    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1790
    .end local v1    # "componentNameString":Ljava/lang/String;
    .end local v2    # "enabledService":Landroid/content/ComponentName;
    :cond_2
    return-object v3
.end method

.method public static has7StepsHugeFontIndex()I
    .locals 2

    .prologue
    .line 1414
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_7_STEP_FONTSIZE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1415
    const/4 v0, 0x6

    .line 1417
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public static isActionbarLightTheme(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1805
    sget v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->actionMenuTextColor:I

    if-gez v0, :cond_0

    .line 1806
    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->parseDeviceTheme(Landroid/content/Context;)V

    .line 1809
    :cond_0
    sget v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->actionMenuTextColor:I

    const v1, 0x888888

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isAllAirMotionDisabled2014(Landroid/content/Context;Z)Z
    .locals 8
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "include_external_motion_settings"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1459
    if-eqz p1, :cond_1

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v6

    const-string v7, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_AIR_WAKE_UP"

    invoke-virtual {v6, v7}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "air_motion_wake_up"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 1461
    .local v2, "airWakeUp":I
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "air_motion_scroll"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1462
    .local v0, "airScroll":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "air_motion_turn"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 1464
    .local v1, "airTurn":I
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "air_motion_call_accept"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 1467
    .local v3, "callAccept":I
    :goto_1
    or-int v6, v2, v0

    or-int/2addr v6, v1

    or-int/2addr v6, v3

    if-ge v6, v5, :cond_0

    move v4, v5

    :cond_0
    return v4

    .end local v0    # "airScroll":I
    .end local v1    # "airTurn":I
    .end local v2    # "airWakeUp":I
    .end local v3    # "callAccept":I
    :cond_1
    move v2, v4

    .line 1459
    goto :goto_0

    .restart local v0    # "airScroll":I
    .restart local v1    # "airTurn":I
    .restart local v2    # "airWakeUp":I
    :cond_2
    move v3, v4

    .line 1464
    goto :goto_1
.end method

.method public static isExternalMemoryAvailable(Landroid/content/Context;)Z
    .locals 10
    .param p0, "mContext"    # Landroid/content/Context;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 235
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    .line 236
    .local v3, "mExternalStorageSdPath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 238
    .local v1, "isExternalMemoryAvailable":Z
    const-string v8, "storage"

    invoke-virtual {p0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/storage/StorageManager;

    .line 240
    .local v4, "storageManager":Landroid/os/storage/StorageManager;
    invoke-virtual {v4}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v5

    .line 241
    .local v5, "storageVolumes":[Landroid/os/storage/StorageVolume;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v8, v5

    if-ge v0, v8, :cond_2

    .line 242
    aget-object v7, v5, v0

    .line 243
    .local v7, "volume":Landroid/os/storage/StorageVolume;
    const-string v8, "sd"

    invoke-virtual {v7}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v7}, Landroid/os/storage/StorageVolume;->isRemovable()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 244
    invoke-virtual {v7}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 245
    invoke-virtual {v4, v3}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 246
    .local v6, "strMountState":Ljava/lang/String;
    const-string v8, "mounted"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    .line 247
    const/4 v1, 0x1

    :goto_1
    move v2, v1

    .line 254
    .end local v1    # "isExternalMemoryAvailable":Z
    .end local v6    # "strMountState":Ljava/lang/String;
    .end local v7    # "volume":Landroid/os/storage/StorageVolume;
    .local v2, "isExternalMemoryAvailable":I
    :goto_2
    return v2

    .line 249
    .end local v2    # "isExternalMemoryAvailable":I
    .restart local v1    # "isExternalMemoryAvailable":Z
    .restart local v6    # "strMountState":Ljava/lang/String;
    .restart local v7    # "volume":Landroid/os/storage/StorageVolume;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 241
    .end local v6    # "strMountState":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v7    # "volume":Landroid/os/storage/StorageVolume;
    :cond_2
    move v2, v1

    .line 254
    .restart local v2    # "isExternalMemoryAvailable":I
    goto :goto_2
.end method

.method public static isLightTheme(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1794
    sget v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->deviceThemeStyle:I

    if-gez v3, :cond_0

    .line 1795
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f040000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 1796
    .local v0, "isLightTheme":Z
    if-eqz v0, :cond_1

    sput v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->deviceThemeStyle:I

    .line 1800
    .end local v0    # "isLightTheme":Z
    :cond_0
    :goto_0
    sget v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->deviceThemeStyle:I

    if-ne v3, v1, :cond_2

    :goto_1
    return v1

    .line 1797
    .restart local v0    # "isLightTheme":Z
    :cond_1
    sput v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->deviceThemeStyle:I

    goto :goto_0

    .end local v0    # "isLightTheme":Z
    :cond_2
    move v1, v2

    .line 1800
    goto :goto_1
.end method

.method public static isSwitchAccessEnabled(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 1558
    const/16 v1, 0x3a

    .line 1559
    .local v1, "ENABLED_SERVICES_SEPARATOR":C
    const-string v0, "com.google.android.marvin.talkback"

    .line 1560
    .local v0, "DEFAULT_SCREENREADER_NAME":Ljava/lang/String;
    new-instance v6, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v8, 0x3a

    invoke-direct {v6, v8}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 1561
    .local v6, "sStringColonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    if-nez p0, :cond_1

    .line 1586
    :cond_0
    :goto_0
    return v7

    .line 1564
    :cond_1
    const/4 v5, 0x0

    .line 1565
    .local v5, "enabledServicesSetting":Ljava/lang/String;
    if-eqz p0, :cond_2

    .line 1566
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "enabled_accessibility_services"

    invoke-static {v8, v9}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1568
    :cond_2
    if-nez v5, :cond_3

    .line 1569
    const-string v5, ""

    .line 1572
    :cond_3
    move-object v2, v6

    .line 1574
    .local v2, "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v2, v5}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 1576
    :cond_4
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1577
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v3

    .line 1578
    .local v3, "componentNameString":Ljava/lang/String;
    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v4

    .line 1580
    .local v4, "enabledService":Landroid/content/ComponentName;
    if-eqz v4, :cond_4

    .line 1581
    const-string v8, "com.google.android.marvin.talkback"

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "com.google.android.marvin.talkback/com.googlecode.eyesfree.switchcontrol.SwitchControlService"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1583
    const/4 v7, 0x1

    goto :goto_0
.end method

.method public static isTalkBackEnabled(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 1590
    const/16 v1, 0x3a

    .line 1591
    .local v1, "ENABLED_SERVICES_SEPARATOR":C
    const-string v0, "com.google.android.marvin.talkback"

    .line 1592
    .local v0, "DEFAULT_SCREENREADER_NAME":Ljava/lang/String;
    new-instance v6, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v8, 0x3a

    invoke-direct {v6, v8}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 1594
    .local v6, "sStringColonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    if-nez p0, :cond_1

    .line 1619
    :cond_0
    :goto_0
    return v7

    .line 1597
    :cond_1
    const/4 v5, 0x0

    .line 1598
    .local v5, "enabledServicesSetting":Ljava/lang/String;
    if-eqz p0, :cond_2

    .line 1599
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "enabled_accessibility_services"

    invoke-static {v8, v9}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1601
    :cond_2
    if-nez v5, :cond_3

    .line 1602
    const-string v5, ""

    .line 1605
    :cond_3
    move-object v2, v6

    .line 1607
    .local v2, "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v2, v5}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 1609
    :cond_4
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1610
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v3

    .line 1611
    .local v3, "componentNameString":Ljava/lang/String;
    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v4

    .line 1613
    .local v4, "enabledService":Landroid/content/ComponentName;
    if-eqz v4, :cond_4

    .line 1614
    const-string v8, "com.google.android.marvin.talkback"

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1616
    const/4 v7, 0x1

    goto :goto_0
.end method

.method private static parseDeviceTheme(Landroid/content/Context;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const v13, 0xffffff

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 1813
    const-string v1, "com.android.settings"

    .line 1816
    .local v1, "appName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.android.settings"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 1817
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    const-string v6, "com.android.settings"

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v4

    .line 1818
    .local v4, "settingsApp":Landroid/content/Context;
    new-instance v6, Landroid/view/ContextThemeWrapper;

    iget v7, v0, Landroid/content/pm/ApplicationInfo;->theme:I

    invoke-direct {v6, v4, v7}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v6}, Landroid/view/ContextThemeWrapper;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    .line 1821
    .local v5, "settingsTheme":Landroid/content/res/Resources$Theme;
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 1822
    .local v3, "outValue":Landroid/util/TypedValue;
    const v6, 0x10102ce

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v3, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1823
    iget v6, v3, Landroid/util/TypedValue;->resourceId:I

    sparse-switch v6, :sswitch_data_0

    .line 1831
    const/4 v6, 0x0

    sput v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->deviceThemeStyle:I

    .line 1839
    :goto_0
    const-string v6, "ShareAccessibilitySettingsCommonFunction"

    const-string v7, "AssistantMenuDisplayUtil : actionBarStyle = 0x%x, deviceThemeStyle = %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    sget v10, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->deviceThemeStyle:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1844
    const v6, 0x1010361

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v3, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1845
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget v7, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    and-int/2addr v6, v13

    sput v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->actionMenuTextColor:I

    .line 1846
    const-string v6, "ShareAccessibilitySettingsCommonFunction"

    const-string v7, "AssistantMenuDisplayUtil : actionMenuTextColor = 0x%08x, %6x"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    sget v10, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->actionMenuTextColor:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1855
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "outValue":Landroid/util/TypedValue;
    .end local v4    # "settingsApp":Landroid/content/Context;
    .end local v5    # "settingsTheme":Landroid/content/res/Resources$Theme;
    :goto_1
    return-void

    .line 1826
    .restart local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v3    # "outValue":Landroid/util/TypedValue;
    .restart local v4    # "settingsApp":Landroid/content/Context;
    .restart local v5    # "settingsTheme":Landroid/content/res/Resources$Theme;
    :sswitch_0
    const/4 v6, 0x1

    sput v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->deviceThemeStyle:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1849
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "outValue":Landroid/util/TypedValue;
    .end local v4    # "settingsApp":Landroid/content/Context;
    .end local v5    # "settingsTheme":Landroid/content/res/Resources$Theme;
    :catch_0
    move-exception v2

    .line 1850
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "ShareAccessibilitySettingsCommonFunction"

    const-string v7, "AssistantMenuDisplayUtil : %s not found"

    new-array v8, v12, [Ljava/lang/Object;

    const-string v9, "com.android.settings"

    aput-object v9, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1852
    sput v11, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->deviceThemeStyle:I

    .line 1853
    sput v13, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->actionMenuTextColor:I

    goto :goto_1

    .line 1823
    :sswitch_data_0
    .sparse-switch
        0x10300e1 -> :sswitch_0
        0x10301a3 -> :sswitch_0
    .end sparse-switch
.end method

.method public static saveValue(Landroid/content/Context;)Ljava/util/HashMap;
    .locals 11
    .param p0, "mContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    .line 358
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 359
    .local v2, "settingValue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_4e

    .line 362
    :try_start_0
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "long_press_timeout"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 363
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/16 v7, 0x1f4

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 366
    :cond_0
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_script_injection"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 368
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 788
    :catch_0
    move-exception v0

    .line 790
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v4, "ShareAccessibilitySettingsCommonFunction"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  value is null"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 372
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_1
    :try_start_1
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_display_magnification_enabled"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 374
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 378
    :cond_2
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "speak_password"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 380
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 384
    :cond_3
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "incall_power_button_behavior"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 386
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 389
    :cond_4
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_enabled"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 390
    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v4

    if-ne v4, v10, :cond_5

    .line 391
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 393
    :cond_5
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 396
    :cond_6
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "switch_access_key"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 397
    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isSwitchAccessEnabled(Landroid/content/Context;)Z

    move-result v4

    if-ne v4, v10, :cond_7

    .line 398
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 400
    :cond_7
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 402
    :cond_8
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "tts_engine"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 403
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "tts_default_synth"

    invoke-static {v5, v6}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 405
    :cond_9
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "tts_default_rate"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 406
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v5, v6}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 408
    :cond_a
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "lock_screen_lock_after_timeout"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 409
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const-wide/16 v8, 0x1388

    invoke-static {v5, v6, v8, v9}, Landroid/provider/Settings$Secure;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 411
    :cond_b
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "font_scale"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 412
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "font_scale"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 414
    :cond_c
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "color_blind"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 415
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 419
    :cond_d
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "color_blind_test"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 421
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 425
    :cond_e
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "color_blind_cvdtype"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 427
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x3

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 430
    :cond_f
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "color_blind_cvdseverity"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 432
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 435
    :cond_10
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "color_blind_user_parameter"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 437
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 440
    :cond_11
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "answering_accessibility_tapping"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 442
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 446
    :cond_12
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "assistant_menu"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 447
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 452
    :cond_13
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "assistant_menu_dominant_hand_type"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 454
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 459
    :cond_14
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "assistant_menu_pointer_speed"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 461
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x2

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 466
    :cond_15
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "assistant_menu_pointer_size"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 468
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 473
    :cond_16
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "assistant_menu_pad_size"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 475
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 481
    :cond_17
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "access_control_use"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 482
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 486
    :cond_18
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "anykey_mode"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 488
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 492
    :cond_19
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "answering_bring_to_ear"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 494
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 498
    :cond_1a
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "mono_audio_db"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 500
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 504
    :cond_1b
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "all_sound_off"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 506
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 510
    :cond_1c
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "enable_accessibility_global_gesture_enabled"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 512
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 516
    :cond_1d
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "easy_interaction"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 518
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 522
    :cond_1e
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "lcd_curtain"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 524
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 528
    :cond_1f
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "notification_reminder"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 530
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 534
    :cond_20
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "time_key"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 536
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 540
    :cond_21
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "notification_reminder_selectable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 542
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 546
    :cond_22
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "time_key_selectable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 548
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 552
    :cond_23
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "notification_reminder_vibrate"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 554
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 558
    :cond_24
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "notification_reminder_app_list"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_27

    .line 560
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 563
    .local v3, "value":Ljava/lang/String;
    if-eqz v3, :cond_25

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_26

    .line 564
    :cond_25
    const-string v4, "ShareAccessibilitySettingsCommonFunction"

    const-string v5, "Default value is skip"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 566
    :cond_26
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 570
    .end local v3    # "value":Ljava/lang/String;
    :cond_27
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "sound_detector"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_28

    .line 572
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 576
    :cond_28
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "doorbell_detector"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 578
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 582
    :cond_29
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_magnifier"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 583
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 585
    :cond_2a
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "hover_zoom_value"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 586
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 588
    :cond_2b
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "hover_zoom_magnifier_size"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 589
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 591
    :cond_2c
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "air_motion_call_accept"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2d

    .line 593
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 597
    :cond_2d
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "hearing_aid"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2e

    .line 599
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 603
    :cond_2e
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_captioning_enabled"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2f

    .line 605
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 609
    :cond_2f
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_sec_captioning_enabled"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_30

    .line 611
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 615
    :cond_30
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_captioning_font_scale"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_31

    .line 617
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 621
    :cond_31
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_captioning_preset"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_32

    .line 623
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 627
    :cond_32
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_captioning_typeface"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_35

    .line 629
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-static {v4, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 632
    .restart local v3    # "value":Ljava/lang/String;
    if-eqz v3, :cond_33

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_34

    .line 633
    :cond_33
    const-string v4, "ShareAccessibilitySettingsCommonFunction"

    const-string v5, "Default value is skip"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 635
    :cond_34
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v5, v6}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 639
    .end local v3    # "value":Ljava/lang/String;
    :cond_35
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_captioning_foreground_color"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_36

    .line 641
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, -0x1

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 645
    :cond_36
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_captioning_edge_type"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_37

    .line 647
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 651
    :cond_37
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_captioning_edge_color"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_38

    .line 653
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/high16 v7, -0x1000000

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 657
    :cond_38
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_captioning_window_color"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_39

    .line 659
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 663
    :cond_39
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_captioning_background_color"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3a

    .line 665
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/high16 v7, -0x1000000

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 669
    :cond_3a
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "accessibility_captioning_locale"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 671
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-static {v4, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 674
    .restart local v3    # "value":Ljava/lang/String;
    if-eqz v3, :cond_3b

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_3c

    .line 675
    :cond_3b
    const-string v4, "ShareAccessibilitySettingsCommonFunction"

    const-string v5, "Default value is skip"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 677
    :cond_3c
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v5, v6}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 681
    .end local v3    # "value":Ljava/lang/String;
    :cond_3d
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "rapid_key_input"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3e

    .line 683
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 687
    :cond_3e
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "rapid_key_input_menu_checked"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3f

    .line 689
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 693
    :cond_3f
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "air_motion_wake_up"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_40

    .line 695
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 699
    :cond_40
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "def_tactileassist_enable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_41

    .line 701
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 705
    :cond_41
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "direct_access"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_42

    .line 707
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 711
    :cond_42
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "direct_accessibility"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_43

    .line 713
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 717
    :cond_43
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "direct_talkback"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_44

    .line 719
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 723
    :cond_44
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "smart_scroll"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_45

    .line 725
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 729
    :cond_45
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "face_smart_scroll"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_46

    .line 731
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 735
    :cond_46
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "smart_scroll_sensitivity"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_47

    .line 737
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 741
    :cond_47
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "smart_scroll_visual_feedback_icon"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_48

    .line 743
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 747
    :cond_48
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "VIB_NOTIFICATION_MAGNITUDE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_49

    .line 749
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 753
    :cond_49
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "direct_negative"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4a

    .line 755
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 759
    :cond_4a
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "direct_access_control"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4b

    .line 761
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 765
    :cond_4b
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "audio_balance"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    if-eqz v4, :cond_4c

    .line 768
    :try_start_2
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "sound_balance"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1

    .line 770
    :catch_1
    move-exception v0

    .line 771
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :try_start_3
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "audio_balance"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 774
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_4c
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string v5, "sound_balance"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_3 .. :try_end_3} :catch_0

    move-result v4

    if-eqz v4, :cond_4d

    .line 777
    :try_start_4
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "sound_balance"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1

    .line 779
    :catch_2
    move-exception v0

    .line 780
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :try_start_5
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "audio_balance"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 784
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_4d
    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_1

    .line 795
    :cond_4e
    const-string v4, "ShareAccessibilitySettingsCommonFunction"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SettingValue: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    return-object v2
.end method

.method public static saveValueToFile(Ljava/util/HashMap;Ljava/io/File;)V
    .locals 11
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    .line 120
    .local p0, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    new-instance v6, Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 123
    .local v6, "newxmlfile":Ljava/io/File;
    :try_start_1
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 129
    :goto_0
    const/4 v2, 0x0

    .line 131
    .local v2, "fileos":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .end local v2    # "fileos":Ljava/io/FileOutputStream;
    .local v3, "fileos":Ljava/io/FileOutputStream;
    move-object v2, v3

    .line 137
    .end local v3    # "fileos":Ljava/io/FileOutputStream;
    .restart local v2    # "fileos":Ljava/io/FileOutputStream;
    :goto_1
    :try_start_3
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v7

    .line 139
    .local v7, "serializer":Lorg/xmlpull/v1/XmlSerializer;
    const-string v8, "UTF-8"

    invoke-interface {v7, v2, v8}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 142
    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 144
    const-string v8, "http://xmlpull.org/v1/doc/features.html#indent-output"

    const/4 v9, 0x1

    invoke-interface {v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 146
    const-string v8, "ro.product.model"

    const-string v9, "Unknown"

    invoke-static {v8, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, "ModelNumber":Ljava/lang/String;
    const-string v8, "ShareAccessibilitySettingsCommonFunction"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "model is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    const/4 v8, 0x0

    const-string v9, "SharingAccessibilitySettings"

    invoke-interface {v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 156
    const/4 v8, 0x0

    const-string v9, "Version"

    const-string v10, "1.0"

    invoke-interface {v7, v8, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 157
    const/4 v8, 0x0

    const-string v9, "Platform"

    const-string v10, "JBP"

    invoke-interface {v7, v8, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 158
    const/4 v8, 0x0

    const-string v9, "ModelNumber"

    invoke-interface {v7, v8, v9, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 162
    invoke-virtual {p0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 165
    .local v4, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 166
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 167
    .local v5, "key":Ljava/lang/String;
    const/4 v8, 0x0

    invoke-interface {v7, v8, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 168
    invoke-virtual {p0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 169
    invoke-virtual {p0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-interface {v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 170
    const-string v9, "[saveValueToFile]"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "key : ["

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "]"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " : "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_0
    const/4 v8, 0x0

    invoke-interface {v7, v8, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 185
    .end local v0    # "ModelNumber":Ljava/lang/String;
    .end local v2    # "fileos":Ljava/io/FileOutputStream;
    .end local v4    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v5    # "key":Ljava/lang/String;
    .end local v6    # "newxmlfile":Ljava/io/File;
    .end local v7    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    :catch_0
    move-exception v1

    .line 187
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 193
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :goto_3
    return-void

    .line 124
    .restart local v6    # "newxmlfile":Ljava/io/File;
    :catch_1
    move-exception v1

    .line 125
    .local v1, "e":Ljava/io/IOException;
    :try_start_4
    const-string v8, "ShareAccessibilitySettingsCommonFunction"

    const-string v9, "IOException: exception in createNewFile() method"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 188
    .end local v1    # "e":Ljava/io/IOException;
    .end local v6    # "newxmlfile":Ljava/io/File;
    :catch_2
    move-exception v1

    .line 190
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 132
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "fileos":Ljava/io/FileOutputStream;
    .restart local v6    # "newxmlfile":Ljava/io/File;
    :catch_3
    move-exception v1

    .line 133
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_5
    const-string v8, "ShareAccessibilitySettingsCommonFunction"

    const-string v9, "FileNotFoundException : can\'t create FileOutputStream"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 177
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v0    # "ModelNumber":Ljava/lang/String;
    .restart local v4    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v7    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    :cond_1
    const/4 v8, 0x0

    const-string v9, "SharingAccessibilitySettings"

    invoke-interface {v7, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 178
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 180
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlSerializer;->flush()V

    .line 182
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 184
    const/4 v8, 0x0

    sput v8, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->SaveFlag:I
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_3
.end method

.method public static setAccessibilityEnabled(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 1635
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->is_talkback:I

    .line 1636
    sget v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->is_talkback:I

    if-ne v0, v1, :cond_0

    .line 1637
    const-string v0, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    invoke-static {p0, v0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->startTalkback(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 1646
    :goto_0
    return-void

    .line 1642
    :cond_0
    const-string v0, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->startTalkback(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static setAssistantMenu(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1493
    const/4 v0, 0x0

    .line 1495
    .local v0, "is_serviceOn":I
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1502
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1504
    const-string v1, "ShareAccessibilitySettingsCommonFunction"

    const-string v2, "AssistantMenu Service Start!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1509
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, p1, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1510
    return-void

    .line 1507
    :cond_0
    const-string v1, "ShareAccessibilitySettingsCommonFunction"

    const-string v2, "AssistantMenu Service Stop!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setFontSize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "index"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 1374
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "font_size"

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 1375
    .local v6, "previousIndex":I
    const/4 v3, 0x0

    .line 1376
    .local v3, "fontIndex":I
    const/4 v4, 0x0

    .line 1377
    .local v4, "fontScale":F
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->has7StepsHugeFontIndex()I

    move-result v0

    .line 1379
    .local v0, "HUGE_FONT_INDEX":I
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1380
    invoke-static {p3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    .line 1381
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "font_size"

    invoke-static {v8, v9, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1382
    sget-object v8, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->mCurConfig:Landroid/content/res/Configuration;

    iput v4, v8, Landroid/content/res/Configuration;->fontScale:F

    .line 1384
    if-ge v6, v0, :cond_1

    if-ne v3, v0, :cond_1

    .line 1385
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.settings.FONT_SIZE_CHANGED"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1386
    .local v5, "i":Landroid/content/Intent;
    const-string v8, "large_font"

    const/4 v9, 0x1

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1387
    invoke-virtual {p0, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1388
    const-string v8, "ShareAccessibilitySettingsCommonFunction"

    const-string v9, "android.settings.FONT_SIZE_CHANGED broadcast. extra(large_font) : 1"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1395
    .end local v5    # "i":Landroid/content/Intent;
    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1396
    .local v1, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v7

    .line 1400
    .local v7, "reply":Landroid/os/Parcel;
    :try_start_0
    const-string v8, "android.app.IActivityManager"

    invoke-virtual {v1, v8}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1402
    sget-object v8, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->mCurConfig:Landroid/content/res/Configuration;

    const/4 v9, 0x0

    invoke-virtual {v8, v1, v9}, Landroid/content/res/Configuration;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1403
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v8

    invoke-interface {v8}, Landroid/app/IActivityManager;->asBinder()Landroid/os/IBinder;

    move-result-object v8

    const/16 v9, 0x88

    const/4 v10, 0x0

    invoke-interface {v8, v9, v1, v7, v10}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 1404
    invoke-virtual {v7}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1411
    :goto_1
    return-void

    .line 1389
    .end local v1    # "data":Landroid/os/Parcel;
    .end local v7    # "reply":Landroid/os/Parcel;
    :cond_1
    if-ne v6, v0, :cond_0

    if-ge v3, v0, :cond_0

    .line 1390
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.settings.FONT_SIZE_CHANGED"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1391
    .restart local v5    # "i":Landroid/content/Intent;
    const-string v8, "large_font"

    invoke-virtual {v5, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1392
    invoke-virtual {p0, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1393
    const-string v8, "ShareAccessibilitySettingsCommonFunction"

    const-string v9, "android.settings.FONT_SIZE_CHANGED broadcast. extra(large_font) : 0"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1406
    .end local v5    # "i":Landroid/content/Intent;
    .restart local v1    # "data":Landroid/os/Parcel;
    .restart local v7    # "reply":Landroid/os/Parcel;
    :catch_0
    move-exception v2

    .line 1408
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public static setGlobalSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1366
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, p1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1370
    :goto_0
    return-void

    .line 1367
    :catch_0
    move-exception v0

    .line 1368
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setHearingAid(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1485
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1486
    .local v0, "mAudioManager":Landroid/media/AudioManager;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v2, p1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1487
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "call_hearing_aid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "ON"

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1488
    .local v1, "mAudioManagerParameter":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 1489
    return-void

    .line 1487
    .end local v1    # "mAudioManagerParameter":Ljava/lang/String;
    :cond_0
    const-string v2, "OFF"

    goto :goto_0
.end method

.method public static setMonoAudio(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1471
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, p1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1472
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.MONO_AUDIO_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1473
    .local v0, "mono_intent":Landroid/content/Intent;
    const-string v1, "mono"

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1474
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1475
    return-void
.end method

.method public static setNegativeColour(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1513
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1514
    .local v0, "negative_color":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, p1, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1516
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 1517
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.NEGATIVECOLOR_ON"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1519
    .local v2, "setOnNegativeColour_value_intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1521
    const-string v3, "ShareAccessibilitySettingsCommonFunction"

    const-string v4, "NEGATIVECOLOR_ON"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1529
    .end local v2    # "setOnNegativeColour_value_intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 1523
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.NEGATIVECOLOR_OFF"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1525
    .local v1, "setOffNegativeColour_value_intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1527
    const-string v3, "ShareAccessibilitySettingsCommonFunction"

    const-string v4, "NEGATIVECOLOR_OFF"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setSecureFloatSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1342
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v1, p1, v2}, Landroid/provider/Settings$Secure;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1346
    :goto_0
    return-void

    .line 1343
    :catch_0
    move-exception v0

    .line 1344
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setSecureIntSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1334
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, p1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1338
    :goto_0
    return-void

    .line 1335
    :catch_0
    move-exception v0

    .line 1336
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setSecureStringSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1350
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, p1, p2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1354
    :goto_0
    return-void

    .line 1351
    :catch_0
    move-exception v0

    .line 1352
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setSwitchAccessEnabled(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 1622
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->is_switchaccess:I

    .line 1623
    sget v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->is_switchaccess:I

    if-ne v0, v1, :cond_0

    .line 1624
    const-string v0, "com.google.android.marvin.talkback/com.googlecode.eyesfree.switchcontrol.SwitchControlService"

    invoke-static {p0, v0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->startSwitchAccess(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 1633
    :goto_0
    return-void

    .line 1629
    :cond_0
    const-string v0, "com.google.android.marvin.talkback/com.googlecode.eyesfree.switchcontrol.SwitchControlService"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->startSwitchAccess(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static setSystemSetting(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1358
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, p1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1362
    :goto_0
    return-void

    .line 1359
    :catch_0
    move-exception v0

    .line 1360
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setTtsEngine(Landroid/content/Context;Landroid/speech/tts/TextToSpeech;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "mTts"    # Landroid/speech/tts/TextToSpeech;
    .param p2, "mInitListener"    # Landroid/speech/tts/TextToSpeech$OnInitListener;
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "value"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 1651
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.speech.tts.engine.CHECK_TTS_DATA"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1652
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, p4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1653
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1657
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1661
    :goto_0
    if-eqz p1, :cond_0

    .line 1662
    invoke-virtual {p1}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 1663
    const/4 p1, 0x0

    .line 1665
    :cond_0
    new-instance p1, Landroid/speech/tts/TextToSpeech;

    .end local p1    # "mTts":Landroid/speech/tts/TextToSpeech;
    invoke-direct {p1, p0, p2, p4}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)V

    .line 1666
    .restart local p1    # "mTts":Landroid/speech/tts/TextToSpeech;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "tts_default_synth"

    invoke-static {v2, v3, p4}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1669
    return-void

    .line 1658
    :catch_0
    move-exception v0

    .line 1659
    .local v0, "ex":Landroid/content/ActivityNotFoundException;
    const-string v2, "ShareAccessibilitySettingsCommonFunction"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to check TTS data, no activity found for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setTtsRate(Landroid/content/Context;Landroid/speech/tts/TextToSpeech;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "mTts"    # Landroid/speech/tts/TextToSpeech;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 1672
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, p2, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1673
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/speech/tts/TextToSpeech;->setSpeechRate(F)I

    .line 1674
    return-void
.end method

.method public static setTurnOffAllSound(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1478
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ALL_SOUND_MUTE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1479
    .local v0, "all_sound_off_intent":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, p1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1480
    const-string v1, "mute"

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1481
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1482
    return-void
.end method

.method public static sharingFileNameCheck(Ljava/io/File;)Ljava/io/File;
    .locals 9
    .param p0, "file"    # Ljava/io/File;

    .prologue
    const/4 v8, 0x0

    .line 196
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 197
    .local v1, "fileAllPath":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 198
    .local v5, "nameCheckFile":Ljava/io/File;
    const/4 v0, 0x2

    .line 199
    .local v0, "count":I
    const-string v3, ""

    .line 200
    .local v3, "fileName":Ljava/lang/String;
    const-string v2, ""

    .line 202
    .local v2, "fileExt":Ljava/lang/String;
    :goto_0
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 203
    const-string v6, "."

    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v1, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 204
    const-string v6, "."

    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 206
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 207
    .local v4, "finalNameCheck":Ljava/lang/String;
    const-string v6, ")"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 208
    const-string v6, "("

    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v1, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 209
    add-int/lit8 v0, v0, 0x1

    .line 212
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 213
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 214
    new-instance v5, Ljava/io/File;

    .end local v5    # "nameCheckFile":Ljava/io/File;
    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 215
    .restart local v5    # "nameCheckFile":Ljava/io/File;
    goto :goto_0

    .line 216
    .end local v4    # "finalNameCheck":Ljava/lang/String;
    :cond_1
    return-object v5
.end method

.method public static soundDetector(Landroid/content/Context;)V
    .locals 2
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 1548
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.settings.action.sound_detector"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1549
    .local v0, "mIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1550
    return-void
.end method

.method public static startSwitchAccess(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 13
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "preferenceKey"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .prologue
    .line 1677
    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->getEnabledServicesFromSettings(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v3

    .line 1679
    .local v3, "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v8

    .line 1681
    .local v8, "toggledService":Landroid/content/ComponentName;
    if-eqz p2, :cond_0

    .line 1683
    const/4 v0, 0x1

    .line 1684
    .local v0, "accessibilityEnabled":Z
    invoke-interface {v3, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1701
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1707
    .local v4, "enabledServicesBuilder":Ljava/lang/StringBuilder;
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 1708
    .local v2, "enabledService":Landroid/content/ComponentName;
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1709
    const/16 v9, 0x3a

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1687
    .end local v0    # "accessibilityEnabled":Z
    .end local v2    # "enabledService":Landroid/content/ComponentName;
    .end local v4    # "enabledServicesBuilder":Ljava/lang/StringBuilder;
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_0
    const/4 v1, 0x0

    .line 1688
    .local v1, "enabledAndInstalledServiceCount":I
    sget-object v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->sInstalledServices:Ljava/util/Set;

    .line 1689
    .local v7, "installedServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .restart local v6    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 1690
    .restart local v2    # "enabledService":Landroid/content/ComponentName;
    invoke-interface {v7, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1691
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1695
    .end local v2    # "enabledService":Landroid/content/ComponentName;
    :cond_2
    const/4 v9, 0x1

    if-gt v1, v9, :cond_3

    const/4 v9, 0x1

    if-ne v1, v9, :cond_4

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 1698
    .restart local v0    # "accessibilityEnabled":Z
    :goto_3
    invoke-interface {v3, v8}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1695
    .end local v0    # "accessibilityEnabled":Z
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 1712
    .end local v1    # "enabledAndInstalledServiceCount":I
    .end local v7    # "installedServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    .restart local v0    # "accessibilityEnabled":Z
    .restart local v4    # "enabledServicesBuilder":Ljava/lang/StringBuilder;
    :cond_5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    .line 1713
    .local v5, "enabledServicesBuilderLength":I
    if-lez v5, :cond_6

    .line 1714
    add-int/lit8 v9, v5, -0x1

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 1716
    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "enabled_accessibility_services"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1719
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "accessibility_enabled"

    if-nez v0, :cond_7

    sget v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->is_talkback:I

    const/4 v12, 0x1

    if-eq v9, v12, :cond_7

    sget v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->is_switchaccess:I

    const/4 v12, 0x1

    if-ne v9, v12, :cond_8

    :cond_7
    const/4 v9, 0x1

    :goto_4
    invoke-static {v10, v11, v9}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1721
    return-void

    .line 1719
    :cond_8
    const/4 v9, 0x0

    goto :goto_4
.end method

.method public static startTalkback(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 13
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "preferenceKey"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .prologue
    .line 1724
    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->getEnabledServicesFromSettings(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v3

    .line 1727
    .local v3, "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v8

    .line 1730
    .local v8, "toggledService":Landroid/content/ComponentName;
    if-eqz p2, :cond_0

    .line 1732
    const/4 v0, 0x1

    .line 1733
    .local v0, "accessibilityEnabled":Z
    invoke-interface {v3, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1751
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1757
    .local v4, "enabledServicesBuilder":Ljava/lang/StringBuilder;
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 1758
    .local v2, "enabledService":Landroid/content/ComponentName;
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1759
    const/16 v9, 0x3a

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1736
    .end local v0    # "accessibilityEnabled":Z
    .end local v2    # "enabledService":Landroid/content/ComponentName;
    .end local v4    # "enabledServicesBuilder":Ljava/lang/StringBuilder;
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_0
    const/4 v1, 0x0

    .line 1737
    .local v1, "enabledAndInstalledServiceCount":I
    sget-object v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->sInstalledServices:Ljava/util/Set;

    .line 1738
    .local v7, "installedServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .restart local v6    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 1739
    .restart local v2    # "enabledService":Landroid/content/ComponentName;
    invoke-interface {v7, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1740
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1744
    .end local v2    # "enabledService":Landroid/content/ComponentName;
    :cond_2
    const/4 v9, 0x1

    if-gt v1, v9, :cond_3

    const/4 v9, 0x1

    if-ne v1, v9, :cond_4

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 1747
    .restart local v0    # "accessibilityEnabled":Z
    :goto_3
    invoke-interface {v3, v8}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1744
    .end local v0    # "accessibilityEnabled":Z
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 1762
    .end local v1    # "enabledAndInstalledServiceCount":I
    .end local v7    # "installedServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    .restart local v0    # "accessibilityEnabled":Z
    .restart local v4    # "enabledServicesBuilder":Ljava/lang/StringBuilder;
    :cond_5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    .line 1763
    .local v5, "enabledServicesBuilderLength":I
    if-lez v5, :cond_6

    .line 1764
    add-int/lit8 v9, v5, -0x1

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 1766
    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "enabled_accessibility_services"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1770
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "accessibility_enabled"

    if-nez v0, :cond_7

    sget v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->is_talkback:I

    const/4 v12, 0x1

    if-ne v9, v12, :cond_8

    :cond_7
    const/4 v9, 0x1

    :goto_4
    invoke-static {v10, v11, v9}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1772
    return-void

    .line 1770
    :cond_8
    const/4 v9, 0x0

    goto :goto_4
.end method

.method public static turnOnAirMotionEngine(Landroid/content/Context;)V
    .locals 5
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 1436
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_motion_engine"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0, v4}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isAllAirMotionDisabled2014(Landroid/content/Context;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1437
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_motion_engine"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1438
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.AIR_MOTION_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1439
    .local v0, "motion_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1440
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1442
    .end local v0    # "motion_changed":Landroid/content/Intent;
    :cond_0
    return-void
.end method
