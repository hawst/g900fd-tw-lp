.class Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$4;
.super Ljava/lang/Object;
.source "ShareAccessibilityExportImport.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x1

    .line 264
    if-nez p2, :cond_1

    .line 265
    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isDialogShown:Z
    invoke-static {v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$002(Z)Z

    .line 266
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 267
    .local v0, "import_internal_intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.myfiles.PICK_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 268
    const-string v1, "FOLDERPATH"

    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_MOST_INTERNAL_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 271
    const-string v1, "CONTENT_TYPE"

    const-string v2, "application/x-sasf"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    const-string v1, "JUST_SELECT_MODE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 273
    iget-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->startActivityForResult(Landroid/content/Intent;I)V

    .line 283
    .end local v0    # "import_internal_intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 274
    :cond_1
    if-ne p2, v3, :cond_0

    .line 275
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->getAvailableInternalMemorySize()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 276
    iget-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->showDialog(I)V

    goto :goto_0

    .line 278
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    const v3, 0x7f07000b

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    const v4, 0x7f070019

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$500(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
