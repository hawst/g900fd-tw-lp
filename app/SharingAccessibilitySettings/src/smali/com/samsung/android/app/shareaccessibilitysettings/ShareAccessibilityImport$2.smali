.class Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$2;
.super Ljava/lang/Object;
.source "ShareAccessibilityImport.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 79
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isSdCardAvailable:Z
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isDialogShown:Z
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->access$100()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    :cond_0
    const/4 v0, 0x0

    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isDialogShown:Z
    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->access$102(Z)Z

    .line 81
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;

    invoke-virtual {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->finish()V

    .line 84
    :cond_1
    const-string v0, "ShareAccessibilityImport"

    const-string v1, "Dialog Dismiss "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    return-void
.end method
