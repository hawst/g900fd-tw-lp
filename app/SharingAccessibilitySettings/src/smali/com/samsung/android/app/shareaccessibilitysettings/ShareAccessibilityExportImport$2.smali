.class Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$2;
.super Ljava/lang/Object;
.source "ShareAccessibilityExportImport.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x0

    .line 144
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isDialogShown:Z
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$000()Z

    move-result v0

    if-nez v0, :cond_1

    .line 145
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isSecondDialogShown:Z
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$100()Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    const-string v0, "ShareAccessibilityExportImport"

    const-string v1, "Dialog Dismiss "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    invoke-virtual {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->finish()V

    .line 150
    :cond_0
    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isSecondDialogShown:Z
    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$102(Z)Z

    .line 152
    :cond_1
    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isDialogShown:Z
    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$002(Z)Z

    .line 153
    return-void
.end method
