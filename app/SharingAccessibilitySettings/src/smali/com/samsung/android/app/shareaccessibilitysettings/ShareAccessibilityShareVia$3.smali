.class Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia$3;
.super Ljava/lang/Object;
.source "ShareAccessibilityShareVia.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 145
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;

    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->isShowDialog:Z
    invoke-static {v2, v4}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->access$002(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;Z)Z

    .line 146
    if-nez p2, :cond_0

    .line 148
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 149
    .local v1, "share_internal_memory_intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.myfiles.PICK_DATA_MULTIPLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    const-string v2, "FOLDERPATH"

    sget-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_MOST_INTERNAL_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    const-string v2, "CONTENT_TYPE"

    const-string v3, "application/x-sasf"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const-string v2, "JUST_SELECT_MODE"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 161
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;

    invoke-virtual {v2, v1, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->startActivityForResult(Landroid/content/Intent;I)V

    .line 183
    .end local v1    # "share_internal_memory_intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 165
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 166
    .local v0, "share_external_intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.myfiles.PICK_DATA_MULTIPLE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 168
    const-string v2, "FOLDERPATH"

    const-string v3, "/storage/extSdCard"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const-string v2, "CONTENT_TYPE"

    const-string v3, "application/x-sasf"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    const-string v2, "JUST_SELECT_MODE"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 178
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;

    invoke-virtual {v2, v0, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
