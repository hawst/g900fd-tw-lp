.class public Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;
.super Landroid/app/Activity;
.source "ShareAccessibilitySettingsNFCSend.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final ANDROIDBEAM:I = 0x2

.field private static final NFC:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ShareAccessibilitySettingsNFCSend"

.field private static settingValue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field mNfcAdapter:Landroid/nfc/NfcAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mContext:Landroid/content/Context;

    .line 36
    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    return-void
.end method

.method private getPayload()[B
    .locals 3

    .prologue
    .line 169
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->settingValue:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 170
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->saveValue(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->settingValue:Ljava/util/HashMap;

    .line 171
    const-string v0, "ShareAccessibilitySettingsNFCSend"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SettingValue:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->settingValue:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->settingValue:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method private shareAccessibilitySettingsNFCSend_Init()V
    .locals 2

    .prologue
    .line 153
    const-string v0, "ShareAccessibilitySettingsNFCSend"

    const-string v1, "init()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->settingValue:Ljava/util/HashMap;

    .line 155
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mContext:Landroid/content/Context;

    .line 156
    return-void
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 5
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    .line 160
    const-string v1, "ShareAccessibilitySettingsNFCSend"

    const-string v2, "createNdefMessage"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    new-instance v0, Landroid/nfc/NdefMessage;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/nfc/NdefRecord;

    const/4 v2, 0x0

    const-string v3, "application/x-sasf"

    invoke-direct {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->getPayload()[B

    move-result-object v4

    invoke-static {v3, v4}, Landroid/nfc/NdefRecord;->createMime(Ljava/lang/String;[B)Landroid/nfc/NdefRecord;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    .line 164
    .local v0, "msg":Landroid/nfc/NdefMessage;
    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 83
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 44
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 47
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 48
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07002d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 50
    const v3, 0x7f030002

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->setContentView(I)V

    .line 52
    invoke-direct {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->shareAccessibilitySettingsNFCSend_Init()V

    .line 54
    const v3, 0x7f090001

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 55
    .local v2, "mImgView":Landroid/widget/ImageView;
    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 56
    const v3, 0x7f030003

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 57
    invoke-virtual {v2}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    .line 58
    .local v1, "frameAnim":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 63
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 64
    iget-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-nez v3, :cond_0

    .line 65
    const-string v3, "ShareAccessibilitySettingsNFCSend"

    const-string v4, "mNfcAdapter is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v3}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 69
    invoke-virtual {p0, v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->showDialog(I)V

    .line 75
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    new-array v4, v5, [Landroid/app/Activity;

    invoke-virtual {v3, p0, p0, v4}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 76
    const-string v3, "ShareAccessibilitySettingsNFCSend"

    const-string v4, "setNdefPushMessageCallback"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 70
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v3}, Landroid/nfc/NfcAdapter;->isNdefPushEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 71
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->showDialog(I)V

    goto :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .param p1, "id"    # I

    .prologue
    const v6, 0x7f070005

    const v5, 0x7f070003

    .line 101
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 103
    .local v1, "dlg":Landroid/app/AlertDialog$Builder;
    const-string v2, "ShareAccessibilitySettingsNFCSend"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreateDialog id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    packed-switch p1, :pswitch_data_0

    .line 148
    :goto_0
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 149
    .local v0, "dialog":Landroid/app/AlertDialog;
    return-object v0

    .line 107
    .end local v0    # "dialog":Landroid/app/AlertDialog;
    :pswitch_0
    const v2, 0x7f07001b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f07001a

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend$2;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;)V

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend$1;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;)V

    invoke-virtual {v2, v6, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 127
    :pswitch_1
    const v2, 0x7f070001

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x7f070000

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend$4;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend$4;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;)V

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend$3;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend$3;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;)V

    invoke-virtual {v2, v6, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 178
    const-string v0, "ShareAccessibilitySettingsNFCSend"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    .line 181
    iput-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 184
    :cond_0
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->settingValue:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 185
    sput-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->settingValue:Ljava/util/HashMap;

    .line 187
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 188
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 87
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 95
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 89
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->finish()V

    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 192
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 193
    const-string v0, "ShareAccessibilitySettingsNFCSend"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-nez v0, :cond_0

    .line 196
    const-string v0, "ShareAccessibilitySettingsNFCSend"

    const-string v1, "mNfcAdapter is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    :goto_0
    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 199
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->showDialog(I)V

    .line 205
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    const/4 v1, 0x0

    new-array v1, v1, [Landroid/app/Activity;

    invoke-virtual {v0, p0, p0, v1}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 206
    const-string v0, "ShareAccessibilitySettingsNFCSend"

    const-string v1, "setNdefPushMessageCallback"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isNdefPushEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 201
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsNFCSend;->showDialog(I)V

    goto :goto_1
.end method
