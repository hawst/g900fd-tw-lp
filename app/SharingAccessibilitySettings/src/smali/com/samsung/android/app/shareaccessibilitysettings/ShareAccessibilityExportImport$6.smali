.class Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$6;
.super Ljava/lang/Object;
.source "ShareAccessibilityExportImport.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$6;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 322
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->createFolder(Ljava/lang/String;)V

    .line 324
    const/4 v0, 0x1

    sput v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->SaveFlag:I

    .line 325
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$300()Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$300()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$6;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$200(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->saveValue(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$302(Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 330
    const-string v0, "ShareAccessibilityExportImport"

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$300()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$300()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-static {v0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->saveValueToFile(Ljava/util/HashMap;Ljava/io/File;)V

    .line 336
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 338
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$6;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    sget-object v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->exportNotificationSet(Ljava/lang/String;Ljava/lang/String;I)V
    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$400(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;Ljava/lang/String;Ljava/lang/String;I)V

    .line 358
    :goto_0
    const-string v0, "finish"

    const-string v1, "finish"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$6;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    invoke-virtual {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->finish()V

    .line 360
    return-void

    .line 348
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$6;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    iget-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$6;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    const v2, 0x7f07000b

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$6;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    const v3, 0x7f070011

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$500(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method
