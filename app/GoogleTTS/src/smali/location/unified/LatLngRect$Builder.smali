.class public final Llocation/unified/LatLngRect$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LatLngRect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LatLngRect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Llocation/unified/LatLngRect;",
        "Llocation/unified/LatLngRect$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Llocation/unified/LatLngRect;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Llocation/unified/LatLngRect$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-static {}, Llocation/unified/LatLngRect$Builder;->create()Llocation/unified/LatLngRect$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Llocation/unified/LatLngRect$Builder;
    .locals 3

    .prologue
    .line 156
    new-instance v0, Llocation/unified/LatLngRect$Builder;

    invoke-direct {v0}, Llocation/unified/LatLngRect$Builder;-><init>()V

    .line 157
    .local v0, "builder":Llocation/unified/LatLngRect$Builder;
    new-instance v1, Llocation/unified/LatLngRect;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Llocation/unified/LatLngRect;-><init>(Llocation/unified/LatLngRect$1;)V

    iput-object v1, v0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    .line 158
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Llocation/unified/LatLngRect$Builder;->build()Llocation/unified/LatLngRect;

    move-result-object v0

    return-object v0
.end method

.method public build()Llocation/unified/LatLngRect;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/LatLngRect$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    invoke-static {v0}, Llocation/unified/LatLngRect$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 189
    :cond_0
    invoke-virtual {p0}, Llocation/unified/LatLngRect$Builder;->buildPartial()Llocation/unified/LatLngRect;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Llocation/unified/LatLngRect;
    .locals 3

    .prologue
    .line 202
    iget-object v1, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    if-nez v1, :cond_0

    .line 203
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 206
    :cond_0
    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    .line 207
    .local v0, "returnMe":Llocation/unified/LatLngRect;
    const/4 v1, 0x0

    iput-object v1, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    .line 208
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Llocation/unified/LatLngRect$Builder;->clone()Llocation/unified/LatLngRect$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Llocation/unified/LatLngRect$Builder;->clone()Llocation/unified/LatLngRect$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 147
    invoke-virtual {p0}, Llocation/unified/LatLngRect$Builder;->clone()Llocation/unified/LatLngRect$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Llocation/unified/LatLngRect$Builder;
    .locals 2

    .prologue
    .line 175
    invoke-static {}, Llocation/unified/LatLngRect$Builder;->create()Llocation/unified/LatLngRect$Builder;

    move-result-object v0

    iget-object v1, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    invoke-virtual {v0, v1}, Llocation/unified/LatLngRect$Builder;->mergeFrom(Llocation/unified/LatLngRect;)Llocation/unified/LatLngRect$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    invoke-virtual {v0}, Llocation/unified/LatLngRect;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 147
    check-cast p1, Llocation/unified/LatLngRect;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Llocation/unified/LatLngRect$Builder;->mergeFrom(Llocation/unified/LatLngRect;)Llocation/unified/LatLngRect$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Llocation/unified/LatLngRect;)Llocation/unified/LatLngRect$Builder;
    .locals 1
    .param p1, "other"    # Llocation/unified/LatLngRect;

    .prologue
    .line 212
    invoke-static {}, Llocation/unified/LatLngRect;->getDefaultInstance()Llocation/unified/LatLngRect;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-object p0

    .line 213
    :cond_1
    invoke-virtual {p1}, Llocation/unified/LatLngRect;->hasLo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    invoke-virtual {p1}, Llocation/unified/LatLngRect;->getLo()Llocation/unified/LatLng;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LatLngRect$Builder;->mergeLo(Llocation/unified/LatLng;)Llocation/unified/LatLngRect$Builder;

    .line 216
    :cond_2
    invoke-virtual {p1}, Llocation/unified/LatLngRect;->hasHi()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {p1}, Llocation/unified/LatLngRect;->getHi()Llocation/unified/LatLng;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LatLngRect$Builder;->mergeHi(Llocation/unified/LatLng;)Llocation/unified/LatLngRect$Builder;

    goto :goto_0
.end method

.method public mergeHi(Llocation/unified/LatLng;)Llocation/unified/LatLngRect$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LatLng;

    .prologue
    .line 318
    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    invoke-virtual {v0}, Llocation/unified/LatLngRect;->hasHi()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    # getter for: Llocation/unified/LatLngRect;->hi_:Llocation/unified/LatLng;
    invoke-static {v0}, Llocation/unified/LatLngRect;->access$600(Llocation/unified/LatLngRect;)Llocation/unified/LatLng;

    move-result-object v0

    invoke-static {}, Llocation/unified/LatLng;->getDefaultInstance()Llocation/unified/LatLng;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 320
    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    iget-object v1, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    # getter for: Llocation/unified/LatLngRect;->hi_:Llocation/unified/LatLng;
    invoke-static {v1}, Llocation/unified/LatLngRect;->access$600(Llocation/unified/LatLngRect;)Llocation/unified/LatLng;

    move-result-object v1

    invoke-static {v1}, Llocation/unified/LatLng;->newBuilder(Llocation/unified/LatLng;)Llocation/unified/LatLng$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Llocation/unified/LatLng$Builder;->mergeFrom(Llocation/unified/LatLng;)Llocation/unified/LatLng$Builder;

    move-result-object v1

    invoke-virtual {v1}, Llocation/unified/LatLng$Builder;->buildPartial()Llocation/unified/LatLng;

    move-result-object v1

    # setter for: Llocation/unified/LatLngRect;->hi_:Llocation/unified/LatLng;
    invoke-static {v0, v1}, Llocation/unified/LatLngRect;->access$602(Llocation/unified/LatLngRect;Llocation/unified/LatLng;)Llocation/unified/LatLng;

    .line 325
    :goto_0
    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LatLngRect;->hasHi:Z
    invoke-static {v0, v1}, Llocation/unified/LatLngRect;->access$502(Llocation/unified/LatLngRect;Z)Z

    .line 326
    return-object p0

    .line 323
    :cond_0
    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    # setter for: Llocation/unified/LatLngRect;->hi_:Llocation/unified/LatLng;
    invoke-static {v0, p1}, Llocation/unified/LatLngRect;->access$602(Llocation/unified/LatLngRect;Llocation/unified/LatLng;)Llocation/unified/LatLng;

    goto :goto_0
.end method

.method public mergeLo(Llocation/unified/LatLng;)Llocation/unified/LatLngRect$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LatLng;

    .prologue
    .line 281
    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    invoke-virtual {v0}, Llocation/unified/LatLngRect;->hasLo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    # getter for: Llocation/unified/LatLngRect;->lo_:Llocation/unified/LatLng;
    invoke-static {v0}, Llocation/unified/LatLngRect;->access$400(Llocation/unified/LatLngRect;)Llocation/unified/LatLng;

    move-result-object v0

    invoke-static {}, Llocation/unified/LatLng;->getDefaultInstance()Llocation/unified/LatLng;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 283
    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    iget-object v1, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    # getter for: Llocation/unified/LatLngRect;->lo_:Llocation/unified/LatLng;
    invoke-static {v1}, Llocation/unified/LatLngRect;->access$400(Llocation/unified/LatLngRect;)Llocation/unified/LatLng;

    move-result-object v1

    invoke-static {v1}, Llocation/unified/LatLng;->newBuilder(Llocation/unified/LatLng;)Llocation/unified/LatLng$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Llocation/unified/LatLng$Builder;->mergeFrom(Llocation/unified/LatLng;)Llocation/unified/LatLng$Builder;

    move-result-object v1

    invoke-virtual {v1}, Llocation/unified/LatLng$Builder;->buildPartial()Llocation/unified/LatLng;

    move-result-object v1

    # setter for: Llocation/unified/LatLngRect;->lo_:Llocation/unified/LatLng;
    invoke-static {v0, v1}, Llocation/unified/LatLngRect;->access$402(Llocation/unified/LatLngRect;Llocation/unified/LatLng;)Llocation/unified/LatLng;

    .line 288
    :goto_0
    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LatLngRect;->hasLo:Z
    invoke-static {v0, v1}, Llocation/unified/LatLngRect;->access$302(Llocation/unified/LatLngRect;Z)Z

    .line 289
    return-object p0

    .line 286
    :cond_0
    iget-object v0, p0, Llocation/unified/LatLngRect$Builder;->result:Llocation/unified/LatLngRect;

    # setter for: Llocation/unified/LatLngRect;->lo_:Llocation/unified/LatLng;
    invoke-static {v0, p1}, Llocation/unified/LatLngRect;->access$402(Llocation/unified/LatLngRect;Llocation/unified/LatLng;)Llocation/unified/LatLng;

    goto :goto_0
.end method
