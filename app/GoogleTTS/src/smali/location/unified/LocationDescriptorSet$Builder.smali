.class public final Llocation/unified/LocationDescriptorSet$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LocationDescriptorSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationDescriptorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Llocation/unified/LocationDescriptorSet;",
        "Llocation/unified/LocationDescriptorSet$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Llocation/unified/LocationDescriptorSet;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Llocation/unified/LocationDescriptorSet$Builder;
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Llocation/unified/LocationDescriptorSet$Builder;->create()Llocation/unified/LocationDescriptorSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Llocation/unified/LocationDescriptorSet$Builder;
    .locals 3

    .prologue
    .line 145
    new-instance v0, Llocation/unified/LocationDescriptorSet$Builder;

    invoke-direct {v0}, Llocation/unified/LocationDescriptorSet$Builder;-><init>()V

    .line 146
    .local v0, "builder":Llocation/unified/LocationDescriptorSet$Builder;
    new-instance v1, Llocation/unified/LocationDescriptorSet;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Llocation/unified/LocationDescriptorSet;-><init>(Llocation/unified/LocationDescriptorSet$1;)V

    iput-object v1, v0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    .line 147
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorSet$Builder;->build()Llocation/unified/LocationDescriptorSet;

    move-result-object v0

    return-object v0
.end method

.method public build()Llocation/unified/LocationDescriptorSet;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorSet$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    invoke-static {v0}, Llocation/unified/LocationDescriptorSet$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 178
    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorSet$Builder;->buildPartial()Llocation/unified/LocationDescriptorSet;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Llocation/unified/LocationDescriptorSet;
    .locals 3

    .prologue
    .line 191
    iget-object v1, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    if-nez v1, :cond_0

    .line 192
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 195
    :cond_0
    iget-object v1, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    # getter for: Llocation/unified/LocationDescriptorSet;->descriptors_:Ljava/util/List;
    invoke-static {v1}, Llocation/unified/LocationDescriptorSet;->access$300(Llocation/unified/LocationDescriptorSet;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 196
    iget-object v1, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    iget-object v2, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    # getter for: Llocation/unified/LocationDescriptorSet;->descriptors_:Ljava/util/List;
    invoke-static {v2}, Llocation/unified/LocationDescriptorSet;->access$300(Llocation/unified/LocationDescriptorSet;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Llocation/unified/LocationDescriptorSet;->descriptors_:Ljava/util/List;
    invoke-static {v1, v2}, Llocation/unified/LocationDescriptorSet;->access$302(Llocation/unified/LocationDescriptorSet;Ljava/util/List;)Ljava/util/List;

    .line 199
    :cond_1
    iget-object v0, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    .line 200
    .local v0, "returnMe":Llocation/unified/LocationDescriptorSet;
    const/4 v1, 0x0

    iput-object v1, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    .line 201
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorSet$Builder;->clone()Llocation/unified/LocationDescriptorSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorSet$Builder;->clone()Llocation/unified/LocationDescriptorSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorSet$Builder;->clone()Llocation/unified/LocationDescriptorSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Llocation/unified/LocationDescriptorSet$Builder;
    .locals 2

    .prologue
    .line 164
    invoke-static {}, Llocation/unified/LocationDescriptorSet$Builder;->create()Llocation/unified/LocationDescriptorSet$Builder;

    move-result-object v0

    iget-object v1, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    invoke-virtual {v0, v1}, Llocation/unified/LocationDescriptorSet$Builder;->mergeFrom(Llocation/unified/LocationDescriptorSet;)Llocation/unified/LocationDescriptorSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    invoke-virtual {v0}, Llocation/unified/LocationDescriptorSet;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 136
    check-cast p1, Llocation/unified/LocationDescriptorSet;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Llocation/unified/LocationDescriptorSet$Builder;->mergeFrom(Llocation/unified/LocationDescriptorSet;)Llocation/unified/LocationDescriptorSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Llocation/unified/LocationDescriptorSet;)Llocation/unified/LocationDescriptorSet$Builder;
    .locals 2
    .param p1, "other"    # Llocation/unified/LocationDescriptorSet;

    .prologue
    .line 205
    invoke-static {}, Llocation/unified/LocationDescriptorSet;->getDefaultInstance()Llocation/unified/LocationDescriptorSet;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-object p0

    .line 206
    :cond_1
    # getter for: Llocation/unified/LocationDescriptorSet;->descriptors_:Ljava/util/List;
    invoke-static {p1}, Llocation/unified/LocationDescriptorSet;->access$300(Llocation/unified/LocationDescriptorSet;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    iget-object v0, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    # getter for: Llocation/unified/LocationDescriptorSet;->descriptors_:Ljava/util/List;
    invoke-static {v0}, Llocation/unified/LocationDescriptorSet;->access$300(Llocation/unified/LocationDescriptorSet;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 208
    iget-object v0, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Llocation/unified/LocationDescriptorSet;->descriptors_:Ljava/util/List;
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptorSet;->access$302(Llocation/unified/LocationDescriptorSet;Ljava/util/List;)Ljava/util/List;

    .line 210
    :cond_2
    iget-object v0, p0, Llocation/unified/LocationDescriptorSet$Builder;->result:Llocation/unified/LocationDescriptorSet;

    # getter for: Llocation/unified/LocationDescriptorSet;->descriptors_:Ljava/util/List;
    invoke-static {v0}, Llocation/unified/LocationDescriptorSet;->access$300(Llocation/unified/LocationDescriptorSet;)Ljava/util/List;

    move-result-object v0

    # getter for: Llocation/unified/LocationDescriptorSet;->descriptors_:Ljava/util/List;
    invoke-static {p1}, Llocation/unified/LocationDescriptorSet;->access$300(Llocation/unified/LocationDescriptorSet;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
