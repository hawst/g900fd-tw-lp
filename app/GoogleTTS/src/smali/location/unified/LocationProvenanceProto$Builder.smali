.class public final Llocation/unified/LocationProvenanceProto$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LocationProvenanceProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationProvenanceProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Llocation/unified/LocationProvenanceProto;",
        "Llocation/unified/LocationProvenanceProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Llocation/unified/LocationProvenanceProto;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Llocation/unified/LocationProvenanceProto$Builder;
    .locals 1

    .prologue
    .line 176
    invoke-static {}, Llocation/unified/LocationProvenanceProto$Builder;->create()Llocation/unified/LocationProvenanceProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Llocation/unified/LocationProvenanceProto$Builder;
    .locals 3

    .prologue
    .line 185
    new-instance v0, Llocation/unified/LocationProvenanceProto$Builder;

    invoke-direct {v0}, Llocation/unified/LocationProvenanceProto$Builder;-><init>()V

    .line 186
    .local v0, "builder":Llocation/unified/LocationProvenanceProto$Builder;
    new-instance v1, Llocation/unified/LocationProvenanceProto;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Llocation/unified/LocationProvenanceProto;-><init>(Llocation/unified/LocationProvenanceProto$1;)V

    iput-object v1, v0, Llocation/unified/LocationProvenanceProto$Builder;->result:Llocation/unified/LocationProvenanceProto;

    .line 187
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Llocation/unified/LocationProvenanceProto$Builder;->build()Llocation/unified/LocationProvenanceProto;

    move-result-object v0

    return-object v0
.end method

.method public build()Llocation/unified/LocationProvenanceProto;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Llocation/unified/LocationProvenanceProto$Builder;->result:Llocation/unified/LocationProvenanceProto;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/LocationProvenanceProto$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 216
    iget-object v0, p0, Llocation/unified/LocationProvenanceProto$Builder;->result:Llocation/unified/LocationProvenanceProto;

    invoke-static {v0}, Llocation/unified/LocationProvenanceProto$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 218
    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationProvenanceProto$Builder;->buildPartial()Llocation/unified/LocationProvenanceProto;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Llocation/unified/LocationProvenanceProto;
    .locals 3

    .prologue
    .line 231
    iget-object v1, p0, Llocation/unified/LocationProvenanceProto$Builder;->result:Llocation/unified/LocationProvenanceProto;

    if-nez v1, :cond_0

    .line 232
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 235
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationProvenanceProto$Builder;->result:Llocation/unified/LocationProvenanceProto;

    .line 236
    .local v0, "returnMe":Llocation/unified/LocationProvenanceProto;
    const/4 v1, 0x0

    iput-object v1, p0, Llocation/unified/LocationProvenanceProto$Builder;->result:Llocation/unified/LocationProvenanceProto;

    .line 237
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Llocation/unified/LocationProvenanceProto$Builder;->clone()Llocation/unified/LocationProvenanceProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Llocation/unified/LocationProvenanceProto$Builder;->clone()Llocation/unified/LocationProvenanceProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 176
    invoke-virtual {p0}, Llocation/unified/LocationProvenanceProto$Builder;->clone()Llocation/unified/LocationProvenanceProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Llocation/unified/LocationProvenanceProto$Builder;
    .locals 2

    .prologue
    .line 204
    invoke-static {}, Llocation/unified/LocationProvenanceProto$Builder;->create()Llocation/unified/LocationProvenanceProto$Builder;

    move-result-object v0

    iget-object v1, p0, Llocation/unified/LocationProvenanceProto$Builder;->result:Llocation/unified/LocationProvenanceProto;

    invoke-virtual {v0, v1}, Llocation/unified/LocationProvenanceProto$Builder;->mergeFrom(Llocation/unified/LocationProvenanceProto;)Llocation/unified/LocationProvenanceProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Llocation/unified/LocationProvenanceProto$Builder;->result:Llocation/unified/LocationProvenanceProto;

    invoke-virtual {v0}, Llocation/unified/LocationProvenanceProto;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 176
    check-cast p1, Llocation/unified/LocationProvenanceProto;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Llocation/unified/LocationProvenanceProto$Builder;->mergeFrom(Llocation/unified/LocationProvenanceProto;)Llocation/unified/LocationProvenanceProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Llocation/unified/LocationProvenanceProto;)Llocation/unified/LocationProvenanceProto$Builder;
    .locals 1
    .param p1, "other"    # Llocation/unified/LocationProvenanceProto;

    .prologue
    .line 241
    invoke-static {}, Llocation/unified/LocationProvenanceProto;->getDefaultInstance()Llocation/unified/LocationProvenanceProto;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 242
    :cond_0
    return-object p0
.end method
