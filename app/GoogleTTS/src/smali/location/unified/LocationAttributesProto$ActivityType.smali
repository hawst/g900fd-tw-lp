.class public final enum Llocation/unified/LocationAttributesProto$ActivityType;
.super Ljava/lang/Enum;
.source "LocationAttributesProto.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationAttributesProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActivityType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Llocation/unified/LocationAttributesProto$ActivityType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Llocation/unified/LocationAttributesProto$ActivityType;

.field public static final enum ACTIVITY_IN_VEHICLE:Llocation/unified/LocationAttributesProto$ActivityType;

.field public static final enum ACTIVITY_ON_BICYCLE:Llocation/unified/LocationAttributesProto$ActivityType;

.field public static final enum ACTIVITY_ON_FOOT:Llocation/unified/LocationAttributesProto$ActivityType;

.field public static final enum ACTIVITY_STILL:Llocation/unified/LocationAttributesProto$ActivityType;

.field public static final enum ACTIVITY_UNKNOWN:Llocation/unified/LocationAttributesProto$ActivityType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Llocation/unified/LocationAttributesProto$ActivityType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Llocation/unified/LocationAttributesProto$ActivityType;

    const-string v1, "ACTIVITY_UNKNOWN"

    invoke-direct {v0, v1, v2, v2, v2}, Llocation/unified/LocationAttributesProto$ActivityType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationAttributesProto$ActivityType;->ACTIVITY_UNKNOWN:Llocation/unified/LocationAttributesProto$ActivityType;

    .line 25
    new-instance v0, Llocation/unified/LocationAttributesProto$ActivityType;

    const-string v1, "ACTIVITY_STILL"

    invoke-direct {v0, v1, v3, v3, v3}, Llocation/unified/LocationAttributesProto$ActivityType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationAttributesProto$ActivityType;->ACTIVITY_STILL:Llocation/unified/LocationAttributesProto$ActivityType;

    .line 26
    new-instance v0, Llocation/unified/LocationAttributesProto$ActivityType;

    const-string v1, "ACTIVITY_IN_VEHICLE"

    invoke-direct {v0, v1, v4, v4, v4}, Llocation/unified/LocationAttributesProto$ActivityType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationAttributesProto$ActivityType;->ACTIVITY_IN_VEHICLE:Llocation/unified/LocationAttributesProto$ActivityType;

    .line 27
    new-instance v0, Llocation/unified/LocationAttributesProto$ActivityType;

    const-string v1, "ACTIVITY_ON_BICYCLE"

    invoke-direct {v0, v1, v5, v5, v5}, Llocation/unified/LocationAttributesProto$ActivityType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationAttributesProto$ActivityType;->ACTIVITY_ON_BICYCLE:Llocation/unified/LocationAttributesProto$ActivityType;

    .line 28
    new-instance v0, Llocation/unified/LocationAttributesProto$ActivityType;

    const-string v1, "ACTIVITY_ON_FOOT"

    invoke-direct {v0, v1, v6, v6, v6}, Llocation/unified/LocationAttributesProto$ActivityType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationAttributesProto$ActivityType;->ACTIVITY_ON_FOOT:Llocation/unified/LocationAttributesProto$ActivityType;

    .line 22
    const/4 v0, 0x5

    new-array v0, v0, [Llocation/unified/LocationAttributesProto$ActivityType;

    sget-object v1, Llocation/unified/LocationAttributesProto$ActivityType;->ACTIVITY_UNKNOWN:Llocation/unified/LocationAttributesProto$ActivityType;

    aput-object v1, v0, v2

    sget-object v1, Llocation/unified/LocationAttributesProto$ActivityType;->ACTIVITY_STILL:Llocation/unified/LocationAttributesProto$ActivityType;

    aput-object v1, v0, v3

    sget-object v1, Llocation/unified/LocationAttributesProto$ActivityType;->ACTIVITY_IN_VEHICLE:Llocation/unified/LocationAttributesProto$ActivityType;

    aput-object v1, v0, v4

    sget-object v1, Llocation/unified/LocationAttributesProto$ActivityType;->ACTIVITY_ON_BICYCLE:Llocation/unified/LocationAttributesProto$ActivityType;

    aput-object v1, v0, v5

    sget-object v1, Llocation/unified/LocationAttributesProto$ActivityType;->ACTIVITY_ON_FOOT:Llocation/unified/LocationAttributesProto$ActivityType;

    aput-object v1, v0, v6

    sput-object v0, Llocation/unified/LocationAttributesProto$ActivityType;->$VALUES:[Llocation/unified/LocationAttributesProto$ActivityType;

    .line 50
    new-instance v0, Llocation/unified/LocationAttributesProto$ActivityType$1;

    invoke-direct {v0}, Llocation/unified/LocationAttributesProto$ActivityType$1;-><init>()V

    sput-object v0, Llocation/unified/LocationAttributesProto$ActivityType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    iput p3, p0, Llocation/unified/LocationAttributesProto$ActivityType;->index:I

    .line 61
    iput p4, p0, Llocation/unified/LocationAttributesProto$ActivityType;->value:I

    .line 62
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Llocation/unified/LocationAttributesProto$ActivityType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Llocation/unified/LocationAttributesProto$ActivityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Llocation/unified/LocationAttributesProto$ActivityType;

    return-object v0
.end method

.method public static values()[Llocation/unified/LocationAttributesProto$ActivityType;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Llocation/unified/LocationAttributesProto$ActivityType;->$VALUES:[Llocation/unified/LocationAttributesProto$ActivityType;

    invoke-virtual {v0}, [Llocation/unified/LocationAttributesProto$ActivityType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llocation/unified/LocationAttributesProto$ActivityType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Llocation/unified/LocationAttributesProto$ActivityType;->value:I

    return v0
.end method
