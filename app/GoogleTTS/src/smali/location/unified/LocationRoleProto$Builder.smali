.class public final Llocation/unified/LocationRoleProto$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LocationRoleProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationRoleProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Llocation/unified/LocationRoleProto;",
        "Llocation/unified/LocationRoleProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Llocation/unified/LocationRoleProto;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Llocation/unified/LocationRoleProto$Builder;
    .locals 1

    .prologue
    .line 170
    invoke-static {}, Llocation/unified/LocationRoleProto$Builder;->create()Llocation/unified/LocationRoleProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Llocation/unified/LocationRoleProto$Builder;
    .locals 3

    .prologue
    .line 179
    new-instance v0, Llocation/unified/LocationRoleProto$Builder;

    invoke-direct {v0}, Llocation/unified/LocationRoleProto$Builder;-><init>()V

    .line 180
    .local v0, "builder":Llocation/unified/LocationRoleProto$Builder;
    new-instance v1, Llocation/unified/LocationRoleProto;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Llocation/unified/LocationRoleProto;-><init>(Llocation/unified/LocationRoleProto$1;)V

    iput-object v1, v0, Llocation/unified/LocationRoleProto$Builder;->result:Llocation/unified/LocationRoleProto;

    .line 181
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 170
    invoke-virtual {p0}, Llocation/unified/LocationRoleProto$Builder;->build()Llocation/unified/LocationRoleProto;

    move-result-object v0

    return-object v0
.end method

.method public build()Llocation/unified/LocationRoleProto;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Llocation/unified/LocationRoleProto$Builder;->result:Llocation/unified/LocationRoleProto;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/LocationRoleProto$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Llocation/unified/LocationRoleProto$Builder;->result:Llocation/unified/LocationRoleProto;

    invoke-static {v0}, Llocation/unified/LocationRoleProto$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 212
    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationRoleProto$Builder;->buildPartial()Llocation/unified/LocationRoleProto;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Llocation/unified/LocationRoleProto;
    .locals 3

    .prologue
    .line 225
    iget-object v1, p0, Llocation/unified/LocationRoleProto$Builder;->result:Llocation/unified/LocationRoleProto;

    if-nez v1, :cond_0

    .line 226
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 229
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationRoleProto$Builder;->result:Llocation/unified/LocationRoleProto;

    .line 230
    .local v0, "returnMe":Llocation/unified/LocationRoleProto;
    const/4 v1, 0x0

    iput-object v1, p0, Llocation/unified/LocationRoleProto$Builder;->result:Llocation/unified/LocationRoleProto;

    .line 231
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 170
    invoke-virtual {p0}, Llocation/unified/LocationRoleProto$Builder;->clone()Llocation/unified/LocationRoleProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 170
    invoke-virtual {p0}, Llocation/unified/LocationRoleProto$Builder;->clone()Llocation/unified/LocationRoleProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-virtual {p0}, Llocation/unified/LocationRoleProto$Builder;->clone()Llocation/unified/LocationRoleProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Llocation/unified/LocationRoleProto$Builder;
    .locals 2

    .prologue
    .line 198
    invoke-static {}, Llocation/unified/LocationRoleProto$Builder;->create()Llocation/unified/LocationRoleProto$Builder;

    move-result-object v0

    iget-object v1, p0, Llocation/unified/LocationRoleProto$Builder;->result:Llocation/unified/LocationRoleProto;

    invoke-virtual {v0, v1}, Llocation/unified/LocationRoleProto$Builder;->mergeFrom(Llocation/unified/LocationRoleProto;)Llocation/unified/LocationRoleProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Llocation/unified/LocationRoleProto$Builder;->result:Llocation/unified/LocationRoleProto;

    invoke-virtual {v0}, Llocation/unified/LocationRoleProto;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 170
    check-cast p1, Llocation/unified/LocationRoleProto;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Llocation/unified/LocationRoleProto$Builder;->mergeFrom(Llocation/unified/LocationRoleProto;)Llocation/unified/LocationRoleProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Llocation/unified/LocationRoleProto;)Llocation/unified/LocationRoleProto$Builder;
    .locals 1
    .param p1, "other"    # Llocation/unified/LocationRoleProto;

    .prologue
    .line 235
    invoke-static {}, Llocation/unified/LocationRoleProto;->getDefaultInstance()Llocation/unified/LocationRoleProto;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 236
    :cond_0
    return-object p0
.end method
