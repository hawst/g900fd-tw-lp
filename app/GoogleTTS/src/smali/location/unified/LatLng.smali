.class public final Llocation/unified/LatLng;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LatLng.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llocation/unified/LatLng$1;,
        Llocation/unified/LatLng$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Llocation/unified/LatLng;


# instance fields
.field private hasLatitudeE7:Z

.field private hasLongitudeE7:Z

.field private latitudeE7_:I

.field private longitudeE7_:I

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 288
    new-instance v0, Llocation/unified/LatLng;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Llocation/unified/LatLng;-><init>(Z)V

    sput-object v0, Llocation/unified/LatLng;->defaultInstance:Llocation/unified/LatLng;

    .line 289
    invoke-static {}, Llocation/unified/LocationDescriptorProto;->internalForceInit()V

    .line 290
    sget-object v0, Llocation/unified/LatLng;->defaultInstance:Llocation/unified/LatLng;

    invoke-direct {v0}, Llocation/unified/LatLng;->initFields()V

    .line 291
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Llocation/unified/LatLng;->latitudeE7_:I

    .line 32
    iput v0, p0, Llocation/unified/LatLng;->longitudeE7_:I

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LatLng;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Llocation/unified/LatLng;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Llocation/unified/LatLng$1;)V
    .locals 0
    .param p1, "x0"    # Llocation/unified/LatLng$1;

    .prologue
    .line 5
    invoke-direct {p0}, Llocation/unified/LatLng;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Llocation/unified/LatLng;->latitudeE7_:I

    .line 32
    iput v0, p0, Llocation/unified/LatLng;->longitudeE7_:I

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LatLng;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Llocation/unified/LatLng;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LatLng;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LatLng;->hasLatitudeE7:Z

    return p1
.end method

.method static synthetic access$402(Llocation/unified/LatLng;I)I
    .locals 0
    .param p0, "x0"    # Llocation/unified/LatLng;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/LatLng;->latitudeE7_:I

    return p1
.end method

.method static synthetic access$502(Llocation/unified/LatLng;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LatLng;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LatLng;->hasLongitudeE7:Z

    return p1
.end method

.method static synthetic access$602(Llocation/unified/LatLng;I)I
    .locals 0
    .param p0, "x0"    # Llocation/unified/LatLng;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/LatLng;->longitudeE7_:I

    return p1
.end method

.method public static getDefaultInstance()Llocation/unified/LatLng;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Llocation/unified/LatLng;->defaultInstance:Llocation/unified/LatLng;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public static newBuilder()Llocation/unified/LatLng$Builder;
    .locals 1

    .prologue
    .line 138
    # invokes: Llocation/unified/LatLng$Builder;->create()Llocation/unified/LatLng$Builder;
    invoke-static {}, Llocation/unified/LatLng$Builder;->access$100()Llocation/unified/LatLng$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Llocation/unified/LatLng;)Llocation/unified/LatLng$Builder;
    .locals 1
    .param p0, "prototype"    # Llocation/unified/LatLng;

    .prologue
    .line 141
    invoke-static {}, Llocation/unified/LatLng;->newBuilder()Llocation/unified/LatLng$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Llocation/unified/LatLng$Builder;->mergeFrom(Llocation/unified/LatLng;)Llocation/unified/LatLng$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LatLng;->getDefaultInstanceForType()Llocation/unified/LatLng;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Llocation/unified/LatLng;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Llocation/unified/LatLng;->defaultInstance:Llocation/unified/LatLng;

    return-object v0
.end method

.method public getLatitudeE7()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Llocation/unified/LatLng;->latitudeE7_:I

    return v0
.end method

.method public getLongitudeE7()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Llocation/unified/LatLng;->longitudeE7_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 55
    iget v0, p0, Llocation/unified/LatLng;->memoizedSerializedSize:I

    .line 56
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 68
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 58
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 59
    invoke-virtual {p0}, Llocation/unified/LatLng;->hasLatitudeE7()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    const/4 v2, 0x1

    invoke-virtual {p0}, Llocation/unified/LatLng;->getLatitudeE7()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeSFixed32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 63
    :cond_1
    invoke-virtual {p0}, Llocation/unified/LatLng;->hasLongitudeE7()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 64
    const/4 v2, 0x2

    invoke-virtual {p0}, Llocation/unified/LatLng;->getLongitudeE7()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeSFixed32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 67
    :cond_2
    iput v0, p0, Llocation/unified/LatLng;->memoizedSerializedSize:I

    move v1, v0

    .line 68
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasLatitudeE7()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Llocation/unified/LatLng;->hasLatitudeE7:Z

    return v0
.end method

.method public hasLongitudeE7()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Llocation/unified/LatLng;->hasLongitudeE7:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LatLng;->toBuilder()Llocation/unified/LatLng$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Llocation/unified/LatLng$Builder;
    .locals 1

    .prologue
    .line 143
    invoke-static {p0}, Llocation/unified/LatLng;->newBuilder(Llocation/unified/LatLng;)Llocation/unified/LatLng$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Llocation/unified/LatLng;->getSerializedSize()I

    .line 45
    invoke-virtual {p0}, Llocation/unified/LatLng;->hasLatitudeE7()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const/4 v0, 0x1

    invoke-virtual {p0}, Llocation/unified/LatLng;->getLatitudeE7()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeSFixed32(II)V

    .line 48
    :cond_0
    invoke-virtual {p0}, Llocation/unified/LatLng;->hasLongitudeE7()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    const/4 v0, 0x2

    invoke-virtual {p0}, Llocation/unified/LatLng;->getLongitudeE7()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeSFixed32(II)V

    .line 51
    :cond_1
    return-void
.end method
