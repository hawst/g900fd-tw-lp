.class public final Llocation/unified/LocationProducerProto$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LocationProducerProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationProducerProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Llocation/unified/LocationProducerProto;",
        "Llocation/unified/LocationProducerProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Llocation/unified/LocationProducerProto;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Llocation/unified/LocationProducerProto$Builder;
    .locals 1

    .prologue
    .line 260
    invoke-static {}, Llocation/unified/LocationProducerProto$Builder;->create()Llocation/unified/LocationProducerProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Llocation/unified/LocationProducerProto$Builder;
    .locals 3

    .prologue
    .line 269
    new-instance v0, Llocation/unified/LocationProducerProto$Builder;

    invoke-direct {v0}, Llocation/unified/LocationProducerProto$Builder;-><init>()V

    .line 270
    .local v0, "builder":Llocation/unified/LocationProducerProto$Builder;
    new-instance v1, Llocation/unified/LocationProducerProto;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Llocation/unified/LocationProducerProto;-><init>(Llocation/unified/LocationProducerProto$1;)V

    iput-object v1, v0, Llocation/unified/LocationProducerProto$Builder;->result:Llocation/unified/LocationProducerProto;

    .line 271
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 260
    invoke-virtual {p0}, Llocation/unified/LocationProducerProto$Builder;->build()Llocation/unified/LocationProducerProto;

    move-result-object v0

    return-object v0
.end method

.method public build()Llocation/unified/LocationProducerProto;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Llocation/unified/LocationProducerProto$Builder;->result:Llocation/unified/LocationProducerProto;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/LocationProducerProto$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 300
    iget-object v0, p0, Llocation/unified/LocationProducerProto$Builder;->result:Llocation/unified/LocationProducerProto;

    invoke-static {v0}, Llocation/unified/LocationProducerProto$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 302
    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationProducerProto$Builder;->buildPartial()Llocation/unified/LocationProducerProto;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Llocation/unified/LocationProducerProto;
    .locals 3

    .prologue
    .line 315
    iget-object v1, p0, Llocation/unified/LocationProducerProto$Builder;->result:Llocation/unified/LocationProducerProto;

    if-nez v1, :cond_0

    .line 316
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 319
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationProducerProto$Builder;->result:Llocation/unified/LocationProducerProto;

    .line 320
    .local v0, "returnMe":Llocation/unified/LocationProducerProto;
    const/4 v1, 0x0

    iput-object v1, p0, Llocation/unified/LocationProducerProto$Builder;->result:Llocation/unified/LocationProducerProto;

    .line 321
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 260
    invoke-virtual {p0}, Llocation/unified/LocationProducerProto$Builder;->clone()Llocation/unified/LocationProducerProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 260
    invoke-virtual {p0}, Llocation/unified/LocationProducerProto$Builder;->clone()Llocation/unified/LocationProducerProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 260
    invoke-virtual {p0}, Llocation/unified/LocationProducerProto$Builder;->clone()Llocation/unified/LocationProducerProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Llocation/unified/LocationProducerProto$Builder;
    .locals 2

    .prologue
    .line 288
    invoke-static {}, Llocation/unified/LocationProducerProto$Builder;->create()Llocation/unified/LocationProducerProto$Builder;

    move-result-object v0

    iget-object v1, p0, Llocation/unified/LocationProducerProto$Builder;->result:Llocation/unified/LocationProducerProto;

    invoke-virtual {v0, v1}, Llocation/unified/LocationProducerProto$Builder;->mergeFrom(Llocation/unified/LocationProducerProto;)Llocation/unified/LocationProducerProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Llocation/unified/LocationProducerProto$Builder;->result:Llocation/unified/LocationProducerProto;

    invoke-virtual {v0}, Llocation/unified/LocationProducerProto;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 260
    check-cast p1, Llocation/unified/LocationProducerProto;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Llocation/unified/LocationProducerProto$Builder;->mergeFrom(Llocation/unified/LocationProducerProto;)Llocation/unified/LocationProducerProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Llocation/unified/LocationProducerProto;)Llocation/unified/LocationProducerProto$Builder;
    .locals 1
    .param p1, "other"    # Llocation/unified/LocationProducerProto;

    .prologue
    .line 325
    invoke-static {}, Llocation/unified/LocationProducerProto;->getDefaultInstance()Llocation/unified/LocationProducerProto;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 326
    :cond_0
    return-object p0
.end method
