.class public final enum Llocation/unified/LocationProvenance;
.super Ljava/lang/Enum;
.source "LocationProvenance.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Llocation/unified/LocationProvenance;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Llocation/unified/LocationProvenance;

.field public static final enum EVAL_BASE_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenance;

.field public static final enum EVAL_EXP_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenance;

.field public static final enum EVAL_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenance;

.field public static final enum GWS_MOBILE_CLIENT:Llocation/unified/LocationProvenance;

.field public static final enum LEGACY_MOBILE_FRONTEND_GLL_PARAM:Llocation/unified/LocationProvenance;

.field public static final enum LEGACY_MOBILE_FRONTEND_NEAR_PARAM:Llocation/unified/LocationProvenance;

.field public static final enum MAPS_FRONTEND_IL_DEBUG_IP:Llocation/unified/LocationProvenance;

.field public static final enum MOBILE_FE:Llocation/unified/LocationProvenance;

.field public static final enum TOOLBAR:Llocation/unified/LocationProvenance;

.field public static final enum UNREMARKABLE:Llocation/unified/LocationProvenance;

.field public static final enum XFF_HEADER:Llocation/unified/LocationProvenance;

.field public static final enum XGEO_HEADER:Llocation/unified/LocationProvenance;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Llocation/unified/LocationProvenance;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 7
    new-instance v0, Llocation/unified/LocationProvenance;

    const-string v1, "UNREMARKABLE"

    invoke-direct {v0, v1, v5, v5, v5}, Llocation/unified/LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenance;->UNREMARKABLE:Llocation/unified/LocationProvenance;

    .line 8
    new-instance v0, Llocation/unified/LocationProvenance;

    const-string v1, "TOOLBAR"

    invoke-direct {v0, v1, v6, v6, v6}, Llocation/unified/LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenance;->TOOLBAR:Llocation/unified/LocationProvenance;

    .line 9
    new-instance v0, Llocation/unified/LocationProvenance;

    const-string v1, "MOBILE_FE"

    invoke-direct {v0, v1, v7, v7, v7}, Llocation/unified/LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenance;->MOBILE_FE:Llocation/unified/LocationProvenance;

    .line 10
    new-instance v0, Llocation/unified/LocationProvenance;

    const-string v1, "LEGACY_MOBILE_FRONTEND_GLL_PARAM"

    invoke-direct {v0, v1, v8, v8, v8}, Llocation/unified/LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenance;->LEGACY_MOBILE_FRONTEND_GLL_PARAM:Llocation/unified/LocationProvenance;

    .line 11
    new-instance v0, Llocation/unified/LocationProvenance;

    const-string v1, "MAPS_FRONTEND_IL_DEBUG_IP"

    invoke-direct {v0, v1, v9, v9, v9}, Llocation/unified/LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenance;->MAPS_FRONTEND_IL_DEBUG_IP:Llocation/unified/LocationProvenance;

    .line 12
    new-instance v0, Llocation/unified/LocationProvenance;

    const-string v1, "LEGACY_MOBILE_FRONTEND_NEAR_PARAM"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenance;->LEGACY_MOBILE_FRONTEND_NEAR_PARAM:Llocation/unified/LocationProvenance;

    .line 13
    new-instance v0, Llocation/unified/LocationProvenance;

    const-string v1, "GWS_MOBILE_CLIENT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenance;->GWS_MOBILE_CLIENT:Llocation/unified/LocationProvenance;

    .line 14
    new-instance v0, Llocation/unified/LocationProvenance;

    const-string v1, "XFF_HEADER"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenance;->XFF_HEADER:Llocation/unified/LocationProvenance;

    .line 15
    new-instance v0, Llocation/unified/LocationProvenance;

    const-string v1, "XGEO_HEADER"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenance;->XGEO_HEADER:Llocation/unified/LocationProvenance;

    .line 16
    new-instance v0, Llocation/unified/LocationProvenance;

    const-string v1, "EVAL_UNIQUE_SELECTED_USER_LOCATION"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const/16 v4, 0x65

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenance;->EVAL_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenance;

    .line 17
    new-instance v0, Llocation/unified/LocationProvenance;

    const-string v1, "EVAL_BASE_UNIQUE_SELECTED_USER_LOCATION"

    const/16 v2, 0xa

    const/16 v3, 0xa

    const/16 v4, 0x66

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenance;->EVAL_BASE_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenance;

    .line 18
    new-instance v0, Llocation/unified/LocationProvenance;

    const-string v1, "EVAL_EXP_UNIQUE_SELECTED_USER_LOCATION"

    const/16 v2, 0xb

    const/16 v3, 0xb

    const/16 v4, 0x67

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenance;->EVAL_EXP_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenance;

    .line 5
    const/16 v0, 0xc

    new-array v0, v0, [Llocation/unified/LocationProvenance;

    sget-object v1, Llocation/unified/LocationProvenance;->UNREMARKABLE:Llocation/unified/LocationProvenance;

    aput-object v1, v0, v5

    sget-object v1, Llocation/unified/LocationProvenance;->TOOLBAR:Llocation/unified/LocationProvenance;

    aput-object v1, v0, v6

    sget-object v1, Llocation/unified/LocationProvenance;->MOBILE_FE:Llocation/unified/LocationProvenance;

    aput-object v1, v0, v7

    sget-object v1, Llocation/unified/LocationProvenance;->LEGACY_MOBILE_FRONTEND_GLL_PARAM:Llocation/unified/LocationProvenance;

    aput-object v1, v0, v8

    sget-object v1, Llocation/unified/LocationProvenance;->MAPS_FRONTEND_IL_DEBUG_IP:Llocation/unified/LocationProvenance;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Llocation/unified/LocationProvenance;->LEGACY_MOBILE_FRONTEND_NEAR_PARAM:Llocation/unified/LocationProvenance;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Llocation/unified/LocationProvenance;->GWS_MOBILE_CLIENT:Llocation/unified/LocationProvenance;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Llocation/unified/LocationProvenance;->XFF_HEADER:Llocation/unified/LocationProvenance;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Llocation/unified/LocationProvenance;->XGEO_HEADER:Llocation/unified/LocationProvenance;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Llocation/unified/LocationProvenance;->EVAL_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenance;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Llocation/unified/LocationProvenance;->EVAL_BASE_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenance;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Llocation/unified/LocationProvenance;->EVAL_EXP_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenance;

    aput-object v2, v0, v1

    sput-object v0, Llocation/unified/LocationProvenance;->$VALUES:[Llocation/unified/LocationProvenance;

    .line 47
    new-instance v0, Llocation/unified/LocationProvenance$1;

    invoke-direct {v0}, Llocation/unified/LocationProvenance$1;-><init>()V

    sput-object v0, Llocation/unified/LocationProvenance;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput p3, p0, Llocation/unified/LocationProvenance;->index:I

    .line 58
    iput p4, p0, Llocation/unified/LocationProvenance;->value:I

    .line 59
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Llocation/unified/LocationProvenance;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Llocation/unified/LocationProvenance;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Llocation/unified/LocationProvenance;

    return-object v0
.end method

.method public static values()[Llocation/unified/LocationProvenance;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Llocation/unified/LocationProvenance;->$VALUES:[Llocation/unified/LocationProvenance;

    invoke-virtual {v0}, [Llocation/unified/LocationProvenance;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llocation/unified/LocationProvenance;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Llocation/unified/LocationProvenance;->value:I

    return v0
.end method
