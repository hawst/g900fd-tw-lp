.class public final Llocation/unified/LocationAttributesProto;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LocationAttributesProto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llocation/unified/LocationAttributesProto$1;,
        Llocation/unified/LocationAttributesProto$Builder;,
        Llocation/unified/LocationAttributesProto$ActivityType;
    }
.end annotation


# static fields
.field private static final defaultInstance:Llocation/unified/LocationAttributesProto;


# instance fields
.field private altitudeMetersFromGround_:D

.field private bearingDegrees_:I

.field private boardedTransitVehicleToken_:Ljava/lang/String;

.field private detectedActivity_:Llocation/unified/LocationAttributesProto$ActivityType;

.field private fieldOfView_:Llocation/unified/FieldOfView;

.field private hasAltitudeMetersFromGround:Z

.field private hasBearingDegrees:Z

.field private hasBoardedTransitVehicleToken:Z

.field private hasDetectedActivity:Z

.field private hasFieldOfView:Z

.field private hasHeadingDegrees:Z

.field private hasRollDegrees:Z

.field private hasSpeedKph:Z

.field private hasTiltDegrees:Z

.field private headingDegrees_:I

.field private memoizedSerializedSize:I

.field private rollDegrees_:I

.field private speedKph_:I

.field private tiltDegrees_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 642
    new-instance v0, Llocation/unified/LocationAttributesProto;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Llocation/unified/LocationAttributesProto;-><init>(Z)V

    sput-object v0, Llocation/unified/LocationAttributesProto;->defaultInstance:Llocation/unified/LocationAttributesProto;

    .line 643
    invoke-static {}, Llocation/unified/LocationDescriptorProto;->internalForceInit()V

    .line 644
    sget-object v0, Llocation/unified/LocationAttributesProto;->defaultInstance:Llocation/unified/LocationAttributesProto;

    invoke-direct {v0}, Llocation/unified/LocationAttributesProto;->initFields()V

    .line 645
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 77
    iput v0, p0, Llocation/unified/LocationAttributesProto;->headingDegrees_:I

    .line 84
    iput v0, p0, Llocation/unified/LocationAttributesProto;->bearingDegrees_:I

    .line 91
    iput v0, p0, Llocation/unified/LocationAttributesProto;->speedKph_:I

    .line 98
    iput v0, p0, Llocation/unified/LocationAttributesProto;->tiltDegrees_:I

    .line 105
    iput v0, p0, Llocation/unified/LocationAttributesProto;->rollDegrees_:I

    .line 112
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Llocation/unified/LocationAttributesProto;->altitudeMetersFromGround_:D

    .line 126
    const-string v0, ""

    iput-object v0, p0, Llocation/unified/LocationAttributesProto;->boardedTransitVehicleToken_:Ljava/lang/String;

    .line 170
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationAttributesProto;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Llocation/unified/LocationAttributesProto;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Llocation/unified/LocationAttributesProto$1;)V
    .locals 0
    .param p1, "x0"    # Llocation/unified/LocationAttributesProto$1;

    .prologue
    .line 5
    invoke-direct {p0}, Llocation/unified/LocationAttributesProto;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 77
    iput v0, p0, Llocation/unified/LocationAttributesProto;->headingDegrees_:I

    .line 84
    iput v0, p0, Llocation/unified/LocationAttributesProto;->bearingDegrees_:I

    .line 91
    iput v0, p0, Llocation/unified/LocationAttributesProto;->speedKph_:I

    .line 98
    iput v0, p0, Llocation/unified/LocationAttributesProto;->tiltDegrees_:I

    .line 105
    iput v0, p0, Llocation/unified/LocationAttributesProto;->rollDegrees_:I

    .line 112
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Llocation/unified/LocationAttributesProto;->altitudeMetersFromGround_:D

    .line 126
    const-string v0, ""

    iput-object v0, p0, Llocation/unified/LocationAttributesProto;->boardedTransitVehicleToken_:Ljava/lang/String;

    .line 170
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationAttributesProto;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Llocation/unified/LocationAttributesProto;I)I
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/LocationAttributesProto;->speedKph_:I

    return p1
.end method

.method static synthetic access$1102(Llocation/unified/LocationAttributesProto;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationAttributesProto;->hasTiltDegrees:Z

    return p1
.end method

.method static synthetic access$1202(Llocation/unified/LocationAttributesProto;I)I
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/LocationAttributesProto;->tiltDegrees_:I

    return p1
.end method

.method static synthetic access$1302(Llocation/unified/LocationAttributesProto;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationAttributesProto;->hasRollDegrees:Z

    return p1
.end method

.method static synthetic access$1402(Llocation/unified/LocationAttributesProto;I)I
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/LocationAttributesProto;->rollDegrees_:I

    return p1
.end method

.method static synthetic access$1502(Llocation/unified/LocationAttributesProto;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationAttributesProto;->hasAltitudeMetersFromGround:Z

    return p1
.end method

.method static synthetic access$1602(Llocation/unified/LocationAttributesProto;D)D
    .locals 1
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # D

    .prologue
    .line 5
    iput-wide p1, p0, Llocation/unified/LocationAttributesProto;->altitudeMetersFromGround_:D

    return-wide p1
.end method

.method static synthetic access$1702(Llocation/unified/LocationAttributesProto;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationAttributesProto;->hasFieldOfView:Z

    return p1
.end method

.method static synthetic access$1800(Llocation/unified/LocationAttributesProto;)Llocation/unified/FieldOfView;
    .locals 1
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;

    .prologue
    .line 5
    iget-object v0, p0, Llocation/unified/LocationAttributesProto;->fieldOfView_:Llocation/unified/FieldOfView;

    return-object v0
.end method

.method static synthetic access$1802(Llocation/unified/LocationAttributesProto;Llocation/unified/FieldOfView;)Llocation/unified/FieldOfView;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # Llocation/unified/FieldOfView;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationAttributesProto;->fieldOfView_:Llocation/unified/FieldOfView;

    return-object p1
.end method

.method static synthetic access$1902(Llocation/unified/LocationAttributesProto;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationAttributesProto;->hasBoardedTransitVehicleToken:Z

    return p1
.end method

.method static synthetic access$2002(Llocation/unified/LocationAttributesProto;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationAttributesProto;->boardedTransitVehicleToken_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Llocation/unified/LocationAttributesProto;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationAttributesProto;->hasDetectedActivity:Z

    return p1
.end method

.method static synthetic access$402(Llocation/unified/LocationAttributesProto;Llocation/unified/LocationAttributesProto$ActivityType;)Llocation/unified/LocationAttributesProto$ActivityType;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # Llocation/unified/LocationAttributesProto$ActivityType;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationAttributesProto;->detectedActivity_:Llocation/unified/LocationAttributesProto$ActivityType;

    return-object p1
.end method

.method static synthetic access$502(Llocation/unified/LocationAttributesProto;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationAttributesProto;->hasHeadingDegrees:Z

    return p1
.end method

.method static synthetic access$602(Llocation/unified/LocationAttributesProto;I)I
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/LocationAttributesProto;->headingDegrees_:I

    return p1
.end method

.method static synthetic access$702(Llocation/unified/LocationAttributesProto;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationAttributesProto;->hasBearingDegrees:Z

    return p1
.end method

.method static synthetic access$802(Llocation/unified/LocationAttributesProto;I)I
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/LocationAttributesProto;->bearingDegrees_:I

    return p1
.end method

.method static synthetic access$902(Llocation/unified/LocationAttributesProto;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationAttributesProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationAttributesProto;->hasSpeedKph:Z

    return p1
.end method

.method public static getDefaultInstance()Llocation/unified/LocationAttributesProto;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Llocation/unified/LocationAttributesProto;->defaultInstance:Llocation/unified/LocationAttributesProto;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 131
    sget-object v0, Llocation/unified/LocationAttributesProto$ActivityType;->ACTIVITY_UNKNOWN:Llocation/unified/LocationAttributesProto$ActivityType;

    iput-object v0, p0, Llocation/unified/LocationAttributesProto;->detectedActivity_:Llocation/unified/LocationAttributesProto$ActivityType;

    .line 132
    invoke-static {}, Llocation/unified/FieldOfView;->getDefaultInstance()Llocation/unified/FieldOfView;

    move-result-object v0

    iput-object v0, p0, Llocation/unified/LocationAttributesProto;->fieldOfView_:Llocation/unified/FieldOfView;

    .line 133
    return-void
.end method

.method public static newBuilder()Llocation/unified/LocationAttributesProto$Builder;
    .locals 1

    .prologue
    .line 283
    # invokes: Llocation/unified/LocationAttributesProto$Builder;->create()Llocation/unified/LocationAttributesProto$Builder;
    invoke-static {}, Llocation/unified/LocationAttributesProto$Builder;->access$100()Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationAttributesProto$Builder;
    .locals 1
    .param p0, "prototype"    # Llocation/unified/LocationAttributesProto;

    .prologue
    .line 286
    invoke-static {}, Llocation/unified/LocationAttributesProto;->newBuilder()Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Llocation/unified/LocationAttributesProto$Builder;->mergeFrom(Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAltitudeMetersFromGround()D
    .locals 2

    .prologue
    .line 114
    iget-wide v0, p0, Llocation/unified/LocationAttributesProto;->altitudeMetersFromGround_:D

    return-wide v0
.end method

.method public getBearingDegrees()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Llocation/unified/LocationAttributesProto;->bearingDegrees_:I

    return v0
.end method

.method public getBoardedTransitVehicleToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Llocation/unified/LocationAttributesProto;->boardedTransitVehicleToken_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getDefaultInstanceForType()Llocation/unified/LocationAttributesProto;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Llocation/unified/LocationAttributesProto;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Llocation/unified/LocationAttributesProto;->defaultInstance:Llocation/unified/LocationAttributesProto;

    return-object v0
.end method

.method public getDetectedActivity()Llocation/unified/LocationAttributesProto$ActivityType;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Llocation/unified/LocationAttributesProto;->detectedActivity_:Llocation/unified/LocationAttributesProto$ActivityType;

    return-object v0
.end method

.method public getFieldOfView()Llocation/unified/FieldOfView;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Llocation/unified/LocationAttributesProto;->fieldOfView_:Llocation/unified/FieldOfView;

    return-object v0
.end method

.method public getHeadingDegrees()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Llocation/unified/LocationAttributesProto;->headingDegrees_:I

    return v0
.end method

.method public getRollDegrees()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Llocation/unified/LocationAttributesProto;->rollDegrees_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 172
    iget v0, p0, Llocation/unified/LocationAttributesProto;->memoizedSerializedSize:I

    .line 173
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 213
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 175
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 176
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasDetectedActivity()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 177
    const/4 v2, 0x1

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getDetectedActivity()Llocation/unified/LocationAttributesProto$ActivityType;

    move-result-object v3

    invoke-virtual {v3}, Llocation/unified/LocationAttributesProto$ActivityType;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 180
    :cond_1
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasHeadingDegrees()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 181
    const/4 v2, 0x2

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getHeadingDegrees()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 184
    :cond_2
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasBearingDegrees()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 185
    const/4 v2, 0x3

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getBearingDegrees()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 188
    :cond_3
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasSpeedKph()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 189
    const/4 v2, 0x4

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getSpeedKph()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 192
    :cond_4
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasTiltDegrees()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 193
    const/4 v2, 0x5

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getTiltDegrees()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 196
    :cond_5
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasRollDegrees()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 197
    const/4 v2, 0x6

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getRollDegrees()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 200
    :cond_6
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasAltitudeMetersFromGround()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 201
    const/4 v2, 0x7

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getAltitudeMetersFromGround()D

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeDoubleSize(ID)I

    move-result v2

    add-int/2addr v0, v2

    .line 204
    :cond_7
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasFieldOfView()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 205
    const/16 v2, 0x8

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getFieldOfView()Llocation/unified/FieldOfView;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 208
    :cond_8
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasBoardedTransitVehicleToken()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 209
    const/16 v2, 0x9

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getBoardedTransitVehicleToken()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 212
    :cond_9
    iput v0, p0, Llocation/unified/LocationAttributesProto;->memoizedSerializedSize:I

    move v1, v0

    .line 213
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto/16 :goto_0
.end method

.method public getSpeedKph()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Llocation/unified/LocationAttributesProto;->speedKph_:I

    return v0
.end method

.method public getTiltDegrees()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Llocation/unified/LocationAttributesProto;->tiltDegrees_:I

    return v0
.end method

.method public hasAltitudeMetersFromGround()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Llocation/unified/LocationAttributesProto;->hasAltitudeMetersFromGround:Z

    return v0
.end method

.method public hasBearingDegrees()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Llocation/unified/LocationAttributesProto;->hasBearingDegrees:Z

    return v0
.end method

.method public hasBoardedTransitVehicleToken()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Llocation/unified/LocationAttributesProto;->hasBoardedTransitVehicleToken:Z

    return v0
.end method

.method public hasDetectedActivity()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Llocation/unified/LocationAttributesProto;->hasDetectedActivity:Z

    return v0
.end method

.method public hasFieldOfView()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Llocation/unified/LocationAttributesProto;->hasFieldOfView:Z

    return v0
.end method

.method public hasHeadingDegrees()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Llocation/unified/LocationAttributesProto;->hasHeadingDegrees:Z

    return v0
.end method

.method public hasRollDegrees()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Llocation/unified/LocationAttributesProto;->hasRollDegrees:Z

    return v0
.end method

.method public hasSpeedKph()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Llocation/unified/LocationAttributesProto;->hasSpeedKph:Z

    return v0
.end method

.method public hasTiltDegrees()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Llocation/unified/LocationAttributesProto;->hasTiltDegrees:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->toBuilder()Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Llocation/unified/LocationAttributesProto$Builder;
    .locals 1

    .prologue
    .line 288
    invoke-static {p0}, Llocation/unified/LocationAttributesProto;->newBuilder(Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getSerializedSize()I

    .line 141
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasDetectedActivity()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const/4 v0, 0x1

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getDetectedActivity()Llocation/unified/LocationAttributesProto$ActivityType;

    move-result-object v1

    invoke-virtual {v1}, Llocation/unified/LocationAttributesProto$ActivityType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 144
    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasHeadingDegrees()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    const/4 v0, 0x2

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getHeadingDegrees()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 147
    :cond_1
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasBearingDegrees()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 148
    const/4 v0, 0x3

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getBearingDegrees()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 150
    :cond_2
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasSpeedKph()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 151
    const/4 v0, 0x4

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getSpeedKph()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 153
    :cond_3
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasTiltDegrees()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 154
    const/4 v0, 0x5

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getTiltDegrees()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 156
    :cond_4
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasRollDegrees()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 157
    const/4 v0, 0x6

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getRollDegrees()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 159
    :cond_5
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasAltitudeMetersFromGround()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 160
    const/4 v0, 0x7

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getAltitudeMetersFromGround()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeDouble(ID)V

    .line 162
    :cond_6
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasFieldOfView()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 163
    const/16 v0, 0x8

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getFieldOfView()Llocation/unified/FieldOfView;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 165
    :cond_7
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->hasBoardedTransitVehicleToken()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 166
    const/16 v0, 0x9

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto;->getBoardedTransitVehicleToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 168
    :cond_8
    return-void
.end method
