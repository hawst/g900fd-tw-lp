.class public final Llocation/unified/LocationRoleProto;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LocationRoleProto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llocation/unified/LocationRoleProto$1;,
        Llocation/unified/LocationRoleProto$Builder;,
        Llocation/unified/LocationRoleProto$LocationRole;
    }
.end annotation


# static fields
.field private static final defaultInstance:Llocation/unified/LocationRoleProto;


# instance fields
.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 263
    new-instance v0, Llocation/unified/LocationRoleProto;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Llocation/unified/LocationRoleProto;-><init>(Z)V

    sput-object v0, Llocation/unified/LocationRoleProto;->defaultInstance:Llocation/unified/LocationRoleProto;

    .line 264
    invoke-static {}, Llocation/unified/LocationDescriptorProto;->internalForceInit()V

    .line 265
    sget-object v0, Llocation/unified/LocationRoleProto;->defaultInstance:Llocation/unified/LocationRoleProto;

    invoke-direct {v0}, Llocation/unified/LocationRoleProto;->initFields()V

    .line 266
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 86
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationRoleProto;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Llocation/unified/LocationRoleProto;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Llocation/unified/LocationRoleProto$1;)V
    .locals 0
    .param p1, "x0"    # Llocation/unified/LocationRoleProto$1;

    .prologue
    .line 5
    invoke-direct {p0}, Llocation/unified/LocationRoleProto;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 86
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationRoleProto;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method public static getDefaultInstance()Llocation/unified/LocationRoleProto;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Llocation/unified/LocationRoleProto;->defaultInstance:Llocation/unified/LocationRoleProto;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public static newBuilder()Llocation/unified/LocationRoleProto$Builder;
    .locals 1

    .prologue
    .line 163
    # invokes: Llocation/unified/LocationRoleProto$Builder;->create()Llocation/unified/LocationRoleProto$Builder;
    invoke-static {}, Llocation/unified/LocationRoleProto$Builder;->access$100()Llocation/unified/LocationRoleProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Llocation/unified/LocationRoleProto;)Llocation/unified/LocationRoleProto$Builder;
    .locals 1
    .param p0, "prototype"    # Llocation/unified/LocationRoleProto;

    .prologue
    .line 166
    invoke-static {}, Llocation/unified/LocationRoleProto;->newBuilder()Llocation/unified/LocationRoleProto$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Llocation/unified/LocationRoleProto$Builder;->mergeFrom(Llocation/unified/LocationRoleProto;)Llocation/unified/LocationRoleProto$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LocationRoleProto;->getDefaultInstanceForType()Llocation/unified/LocationRoleProto;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Llocation/unified/LocationRoleProto;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Llocation/unified/LocationRoleProto;->defaultInstance:Llocation/unified/LocationRoleProto;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 88
    iget v0, p0, Llocation/unified/LocationRoleProto;->memoizedSerializedSize:I

    .line 89
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 93
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 91
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 92
    iput v0, p0, Llocation/unified/LocationRoleProto;->memoizedSerializedSize:I

    move v1, v0

    .line 93
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LocationRoleProto;->toBuilder()Llocation/unified/LocationRoleProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Llocation/unified/LocationRoleProto$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-static {p0}, Llocation/unified/LocationRoleProto;->newBuilder(Llocation/unified/LocationRoleProto;)Llocation/unified/LocationRoleProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 0
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-virtual {p0}, Llocation/unified/LocationRoleProto;->getSerializedSize()I

    .line 84
    return-void
.end method
