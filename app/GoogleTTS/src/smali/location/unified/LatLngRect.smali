.class public final Llocation/unified/LatLngRect;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LatLngRect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llocation/unified/LatLngRect$1;,
        Llocation/unified/LatLngRect$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Llocation/unified/LatLngRect;


# instance fields
.field private hasHi:Z

.field private hasLo:Z

.field private hi_:Llocation/unified/LatLng;

.field private lo_:Llocation/unified/LatLng;

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 338
    new-instance v0, Llocation/unified/LatLngRect;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Llocation/unified/LatLngRect;-><init>(Z)V

    sput-object v0, Llocation/unified/LatLngRect;->defaultInstance:Llocation/unified/LatLngRect;

    .line 339
    invoke-static {}, Llocation/unified/LocationDescriptorProto;->internalForceInit()V

    .line 340
    sget-object v0, Llocation/unified/LatLngRect;->defaultInstance:Llocation/unified/LatLngRect;

    invoke-direct {v0}, Llocation/unified/LatLngRect;->initFields()V

    .line 341
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LatLngRect;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Llocation/unified/LatLngRect;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Llocation/unified/LatLngRect$1;)V
    .locals 0
    .param p1, "x0"    # Llocation/unified/LatLngRect$1;

    .prologue
    .line 5
    invoke-direct {p0}, Llocation/unified/LatLngRect;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LatLngRect;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Llocation/unified/LatLngRect;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LatLngRect;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LatLngRect;->hasLo:Z

    return p1
.end method

.method static synthetic access$400(Llocation/unified/LatLngRect;)Llocation/unified/LatLng;
    .locals 1
    .param p0, "x0"    # Llocation/unified/LatLngRect;

    .prologue
    .line 5
    iget-object v0, p0, Llocation/unified/LatLngRect;->lo_:Llocation/unified/LatLng;

    return-object v0
.end method

.method static synthetic access$402(Llocation/unified/LatLngRect;Llocation/unified/LatLng;)Llocation/unified/LatLng;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LatLngRect;
    .param p1, "x1"    # Llocation/unified/LatLng;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LatLngRect;->lo_:Llocation/unified/LatLng;

    return-object p1
.end method

.method static synthetic access$502(Llocation/unified/LatLngRect;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LatLngRect;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LatLngRect;->hasHi:Z

    return p1
.end method

.method static synthetic access$600(Llocation/unified/LatLngRect;)Llocation/unified/LatLng;
    .locals 1
    .param p0, "x0"    # Llocation/unified/LatLngRect;

    .prologue
    .line 5
    iget-object v0, p0, Llocation/unified/LatLngRect;->hi_:Llocation/unified/LatLng;

    return-object v0
.end method

.method static synthetic access$602(Llocation/unified/LatLngRect;Llocation/unified/LatLng;)Llocation/unified/LatLng;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LatLngRect;
    .param p1, "x1"    # Llocation/unified/LatLng;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LatLngRect;->hi_:Llocation/unified/LatLng;

    return-object p1
.end method

.method public static getDefaultInstance()Llocation/unified/LatLngRect;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Llocation/unified/LatLngRect;->defaultInstance:Llocation/unified/LatLngRect;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Llocation/unified/LatLng;->getDefaultInstance()Llocation/unified/LatLng;

    move-result-object v0

    iput-object v0, p0, Llocation/unified/LatLngRect;->lo_:Llocation/unified/LatLng;

    .line 38
    invoke-static {}, Llocation/unified/LatLng;->getDefaultInstance()Llocation/unified/LatLng;

    move-result-object v0

    iput-object v0, p0, Llocation/unified/LatLngRect;->hi_:Llocation/unified/LatLng;

    .line 39
    return-void
.end method

.method public static newBuilder()Llocation/unified/LatLngRect$Builder;
    .locals 1

    .prologue
    .line 140
    # invokes: Llocation/unified/LatLngRect$Builder;->create()Llocation/unified/LatLngRect$Builder;
    invoke-static {}, Llocation/unified/LatLngRect$Builder;->access$100()Llocation/unified/LatLngRect$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Llocation/unified/LatLngRect;)Llocation/unified/LatLngRect$Builder;
    .locals 1
    .param p0, "prototype"    # Llocation/unified/LatLngRect;

    .prologue
    .line 143
    invoke-static {}, Llocation/unified/LatLngRect;->newBuilder()Llocation/unified/LatLngRect$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Llocation/unified/LatLngRect$Builder;->mergeFrom(Llocation/unified/LatLngRect;)Llocation/unified/LatLngRect$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LatLngRect;->getDefaultInstanceForType()Llocation/unified/LatLngRect;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Llocation/unified/LatLngRect;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Llocation/unified/LatLngRect;->defaultInstance:Llocation/unified/LatLngRect;

    return-object v0
.end method

.method public getHi()Llocation/unified/LatLng;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Llocation/unified/LatLngRect;->hi_:Llocation/unified/LatLng;

    return-object v0
.end method

.method public getLo()Llocation/unified/LatLng;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Llocation/unified/LatLngRect;->lo_:Llocation/unified/LatLng;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 57
    iget v0, p0, Llocation/unified/LatLngRect;->memoizedSerializedSize:I

    .line 58
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 70
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 60
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 61
    invoke-virtual {p0}, Llocation/unified/LatLngRect;->hasLo()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    const/4 v2, 0x1

    invoke-virtual {p0}, Llocation/unified/LatLngRect;->getLo()Llocation/unified/LatLng;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 65
    :cond_1
    invoke-virtual {p0}, Llocation/unified/LatLngRect;->hasHi()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 66
    const/4 v2, 0x2

    invoke-virtual {p0}, Llocation/unified/LatLngRect;->getHi()Llocation/unified/LatLng;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 69
    :cond_2
    iput v0, p0, Llocation/unified/LatLngRect;->memoizedSerializedSize:I

    move v1, v0

    .line 70
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasHi()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Llocation/unified/LatLngRect;->hasHi:Z

    return v0
.end method

.method public hasLo()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Llocation/unified/LatLngRect;->hasLo:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LatLngRect;->toBuilder()Llocation/unified/LatLngRect$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Llocation/unified/LatLngRect$Builder;
    .locals 1

    .prologue
    .line 145
    invoke-static {p0}, Llocation/unified/LatLngRect;->newBuilder(Llocation/unified/LatLngRect;)Llocation/unified/LatLngRect$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Llocation/unified/LatLngRect;->getSerializedSize()I

    .line 47
    invoke-virtual {p0}, Llocation/unified/LatLngRect;->hasLo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0}, Llocation/unified/LatLngRect;->getLo()Llocation/unified/LatLng;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 50
    :cond_0
    invoke-virtual {p0}, Llocation/unified/LatLngRect;->hasHi()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x2

    invoke-virtual {p0}, Llocation/unified/LatLngRect;->getHi()Llocation/unified/LatLng;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 53
    :cond_1
    return-void
.end method
