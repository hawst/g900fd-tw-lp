.class public final Llocation/unified/LocationProducerProto;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LocationProducerProto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llocation/unified/LocationProducerProto$1;,
        Llocation/unified/LocationProducerProto$Builder;,
        Llocation/unified/LocationProducerProto$LocationProducer;
    }
.end annotation


# static fields
.field private static final defaultInstance:Llocation/unified/LocationProducerProto;


# instance fields
.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 353
    new-instance v0, Llocation/unified/LocationProducerProto;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Llocation/unified/LocationProducerProto;-><init>(Z)V

    sput-object v0, Llocation/unified/LocationProducerProto;->defaultInstance:Llocation/unified/LocationProducerProto;

    .line 354
    invoke-static {}, Llocation/unified/LocationDescriptorProto;->internalForceInit()V

    .line 355
    sget-object v0, Llocation/unified/LocationProducerProto;->defaultInstance:Llocation/unified/LocationProducerProto;

    invoke-direct {v0}, Llocation/unified/LocationProducerProto;->initFields()V

    .line 356
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 176
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationProducerProto;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Llocation/unified/LocationProducerProto;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Llocation/unified/LocationProducerProto$1;)V
    .locals 0
    .param p1, "x0"    # Llocation/unified/LocationProducerProto$1;

    .prologue
    .line 5
    invoke-direct {p0}, Llocation/unified/LocationProducerProto;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 176
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationProducerProto;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method public static getDefaultInstance()Llocation/unified/LocationProducerProto;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Llocation/unified/LocationProducerProto;->defaultInstance:Llocation/unified/LocationProducerProto;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 166
    return-void
.end method

.method public static newBuilder()Llocation/unified/LocationProducerProto$Builder;
    .locals 1

    .prologue
    .line 253
    # invokes: Llocation/unified/LocationProducerProto$Builder;->create()Llocation/unified/LocationProducerProto$Builder;
    invoke-static {}, Llocation/unified/LocationProducerProto$Builder;->access$100()Llocation/unified/LocationProducerProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Llocation/unified/LocationProducerProto;)Llocation/unified/LocationProducerProto$Builder;
    .locals 1
    .param p0, "prototype"    # Llocation/unified/LocationProducerProto;

    .prologue
    .line 256
    invoke-static {}, Llocation/unified/LocationProducerProto;->newBuilder()Llocation/unified/LocationProducerProto$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Llocation/unified/LocationProducerProto$Builder;->mergeFrom(Llocation/unified/LocationProducerProto;)Llocation/unified/LocationProducerProto$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LocationProducerProto;->getDefaultInstanceForType()Llocation/unified/LocationProducerProto;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Llocation/unified/LocationProducerProto;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Llocation/unified/LocationProducerProto;->defaultInstance:Llocation/unified/LocationProducerProto;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 178
    iget v0, p0, Llocation/unified/LocationProducerProto;->memoizedSerializedSize:I

    .line 179
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 183
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 181
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 182
    iput v0, p0, Llocation/unified/LocationProducerProto;->memoizedSerializedSize:I

    move v1, v0

    .line 183
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LocationProducerProto;->toBuilder()Llocation/unified/LocationProducerProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Llocation/unified/LocationProducerProto$Builder;
    .locals 1

    .prologue
    .line 258
    invoke-static {p0}, Llocation/unified/LocationProducerProto;->newBuilder(Llocation/unified/LocationProducerProto;)Llocation/unified/LocationProducerProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 0
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    invoke-virtual {p0}, Llocation/unified/LocationProducerProto;->getSerializedSize()I

    .line 174
    return-void
.end method
