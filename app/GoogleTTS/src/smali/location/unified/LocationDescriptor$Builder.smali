.class public final Llocation/unified/LocationDescriptor$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LocationDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Llocation/unified/LocationDescriptor;",
        "Llocation/unified/LocationDescriptor$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Llocation/unified/LocationDescriptor;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 438
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Llocation/unified/LocationDescriptor$Builder;
    .locals 1

    .prologue
    .line 432
    invoke-static {}, Llocation/unified/LocationDescriptor$Builder;->create()Llocation/unified/LocationDescriptor$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Llocation/unified/LocationDescriptor$Builder;
    .locals 3

    .prologue
    .line 441
    new-instance v0, Llocation/unified/LocationDescriptor$Builder;

    invoke-direct {v0}, Llocation/unified/LocationDescriptor$Builder;-><init>()V

    .line 442
    .local v0, "builder":Llocation/unified/LocationDescriptor$Builder;
    new-instance v1, Llocation/unified/LocationDescriptor;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Llocation/unified/LocationDescriptor;-><init>(Llocation/unified/LocationDescriptor$1;)V

    iput-object v1, v0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    .line 443
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 432
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor$Builder;->build()Llocation/unified/LocationDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public build()Llocation/unified/LocationDescriptor;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    invoke-static {v0}, Llocation/unified/LocationDescriptor$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 474
    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor$Builder;->buildPartial()Llocation/unified/LocationDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Llocation/unified/LocationDescriptor;
    .locals 3

    .prologue
    .line 487
    iget-object v1, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    if-nez v1, :cond_0

    .line 488
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 491
    :cond_0
    iget-object v1, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;
    invoke-static {v1}, Llocation/unified/LocationDescriptor;->access$300(Llocation/unified/LocationDescriptor;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 492
    iget-object v1, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    iget-object v2, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;
    invoke-static {v2}, Llocation/unified/LocationDescriptor;->access$300(Llocation/unified/LocationDescriptor;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;
    invoke-static {v1, v2}, Llocation/unified/LocationDescriptor;->access$302(Llocation/unified/LocationDescriptor;Ljava/util/List;)Ljava/util/List;

    .line 495
    :cond_1
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    .line 496
    .local v0, "returnMe":Llocation/unified/LocationDescriptor;
    const/4 v1, 0x0

    iput-object v1, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    .line 497
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 432
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor$Builder;->clone()Llocation/unified/LocationDescriptor$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 432
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor$Builder;->clone()Llocation/unified/LocationDescriptor$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 432
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor$Builder;->clone()Llocation/unified/LocationDescriptor$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Llocation/unified/LocationDescriptor$Builder;
    .locals 2

    .prologue
    .line 460
    invoke-static {}, Llocation/unified/LocationDescriptor$Builder;->create()Llocation/unified/LocationDescriptor$Builder;

    move-result-object v0

    iget-object v1, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    invoke-virtual {v0, v1}, Llocation/unified/LocationDescriptor$Builder;->mergeFrom(Llocation/unified/LocationDescriptor;)Llocation/unified/LocationDescriptor$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    invoke-virtual {v0}, Llocation/unified/LocationDescriptor;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeAttributes(Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LocationAttributesProto;

    .prologue
    .line 1203
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    invoke-virtual {v0}, Llocation/unified/LocationDescriptor;->hasAttributes()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->attributes_:Llocation/unified/LocationAttributesProto;
    invoke-static {v0}, Llocation/unified/LocationDescriptor;->access$4100(Llocation/unified/LocationDescriptor;)Llocation/unified/LocationAttributesProto;

    move-result-object v0

    invoke-static {}, Llocation/unified/LocationAttributesProto;->getDefaultInstance()Llocation/unified/LocationAttributesProto;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1205
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    iget-object v1, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->attributes_:Llocation/unified/LocationAttributesProto;
    invoke-static {v1}, Llocation/unified/LocationDescriptor;->access$4100(Llocation/unified/LocationDescriptor;)Llocation/unified/LocationAttributesProto;

    move-result-object v1

    invoke-static {v1}, Llocation/unified/LocationAttributesProto;->newBuilder(Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Llocation/unified/LocationAttributesProto$Builder;->mergeFrom(Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v1

    invoke-virtual {v1}, Llocation/unified/LocationAttributesProto$Builder;->buildPartial()Llocation/unified/LocationAttributesProto;

    move-result-object v1

    # setter for: Llocation/unified/LocationDescriptor;->attributes_:Llocation/unified/LocationAttributesProto;
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$4102(Llocation/unified/LocationDescriptor;Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationAttributesProto;

    .line 1210
    :goto_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasAttributes:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$4002(Llocation/unified/LocationDescriptor;Z)Z

    .line 1211
    return-object p0

    .line 1208
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->attributes_:Llocation/unified/LocationAttributesProto;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$4102(Llocation/unified/LocationDescriptor;Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationAttributesProto;

    goto :goto_0
.end method

.method public mergeFeatureId(Llocation/unified/FeatureIdProto;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/FeatureIdProto;

    .prologue
    .line 991
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    invoke-virtual {v0}, Llocation/unified/LocationDescriptor;->hasFeatureId()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->featureId_:Llocation/unified/FeatureIdProto;
    invoke-static {v0}, Llocation/unified/LocationDescriptor;->access$2300(Llocation/unified/LocationDescriptor;)Llocation/unified/FeatureIdProto;

    move-result-object v0

    invoke-static {}, Llocation/unified/FeatureIdProto;->getDefaultInstance()Llocation/unified/FeatureIdProto;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 993
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    iget-object v1, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->featureId_:Llocation/unified/FeatureIdProto;
    invoke-static {v1}, Llocation/unified/LocationDescriptor;->access$2300(Llocation/unified/LocationDescriptor;)Llocation/unified/FeatureIdProto;

    move-result-object v1

    invoke-static {v1}, Llocation/unified/FeatureIdProto;->newBuilder(Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Llocation/unified/FeatureIdProto$Builder;->mergeFrom(Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto$Builder;

    move-result-object v1

    invoke-virtual {v1}, Llocation/unified/FeatureIdProto$Builder;->buildPartial()Llocation/unified/FeatureIdProto;

    move-result-object v1

    # setter for: Llocation/unified/LocationDescriptor;->featureId_:Llocation/unified/FeatureIdProto;
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$2302(Llocation/unified/LocationDescriptor;Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto;

    .line 998
    :goto_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasFeatureId:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$2202(Llocation/unified/LocationDescriptor;Z)Z

    .line 999
    return-object p0

    .line 996
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->featureId_:Llocation/unified/FeatureIdProto;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$2302(Llocation/unified/LocationDescriptor;Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 432
    check-cast p1, Llocation/unified/LocationDescriptor;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Llocation/unified/LocationDescriptor$Builder;->mergeFrom(Llocation/unified/LocationDescriptor;)Llocation/unified/LocationDescriptor$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Llocation/unified/LocationDescriptor;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "other"    # Llocation/unified/LocationDescriptor;

    .prologue
    .line 501
    invoke-static {}, Llocation/unified/LocationDescriptor;->getDefaultInstance()Llocation/unified/LocationDescriptor;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 568
    :cond_0
    :goto_0
    return-object p0

    .line 502
    :cond_1
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasRole()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 503
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getRole()Llocation/unified/LocationRole;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->setRole(Llocation/unified/LocationRole;)Llocation/unified/LocationDescriptor$Builder;

    .line 505
    :cond_2
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasProducer()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 506
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getProducer()Llocation/unified/LocationProducer;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->setProducer(Llocation/unified/LocationProducer;)Llocation/unified/LocationDescriptor$Builder;

    .line 508
    :cond_3
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasTimestamp()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 509
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getTimestamp()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Llocation/unified/LocationDescriptor$Builder;->setTimestamp(J)Llocation/unified/LocationDescriptor$Builder;

    .line 511
    :cond_4
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasLoc()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 512
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getLoc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->setLoc(Ljava/lang/String;)Llocation/unified/LocationDescriptor$Builder;

    .line 514
    :cond_5
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasLatlng()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 515
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getLatlng()Llocation/unified/LatLng;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->mergeLatlng(Llocation/unified/LatLng;)Llocation/unified/LocationDescriptor$Builder;

    .line 517
    :cond_6
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasLatlngSpan()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 518
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getLatlngSpan()Llocation/unified/LatLng;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->mergeLatlngSpan(Llocation/unified/LatLng;)Llocation/unified/LocationDescriptor$Builder;

    .line 520
    :cond_7
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasRect()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 521
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getRect()Llocation/unified/LatLngRect;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->mergeRect(Llocation/unified/LatLngRect;)Llocation/unified/LocationDescriptor$Builder;

    .line 523
    :cond_8
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasRadius()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 524
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getRadius()F

    move-result v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->setRadius(F)Llocation/unified/LocationDescriptor$Builder;

    .line 526
    :cond_9
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasConfidence()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 527
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getConfidence()I

    move-result v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->setConfidence(I)Llocation/unified/LocationDescriptor$Builder;

    .line 529
    :cond_a
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasFeatureId()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 530
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getFeatureId()Llocation/unified/FeatureIdProto;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->mergeFeatureId(Llocation/unified/FeatureIdProto;)Llocation/unified/LocationDescriptor$Builder;

    .line 532
    :cond_b
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasMid()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 533
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getMid()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Llocation/unified/LocationDescriptor$Builder;->setMid(J)Llocation/unified/LocationDescriptor$Builder;

    .line 535
    :cond_c
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasLevelFeatureId()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 536
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getLevelFeatureId()Llocation/unified/FeatureIdProto;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->mergeLevelFeatureId(Llocation/unified/FeatureIdProto;)Llocation/unified/LocationDescriptor$Builder;

    .line 538
    :cond_d
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasLevelNumber()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 539
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getLevelNumber()F

    move-result v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->setLevelNumber(F)Llocation/unified/LocationDescriptor$Builder;

    .line 541
    :cond_e
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasLanguage()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 542
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->setLanguage(Ljava/lang/String;)Llocation/unified/LocationDescriptor$Builder;

    .line 544
    :cond_f
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasProvenance()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 545
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getProvenance()Llocation/unified/LocationProvenance;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->setProvenance(Llocation/unified/LocationProvenance;)Llocation/unified/LocationDescriptor$Builder;

    .line 547
    :cond_10
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasHistoricalRole()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 548
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getHistoricalRole()Llocation/unified/LocationRole;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->setHistoricalRole(Llocation/unified/LocationRole;)Llocation/unified/LocationDescriptor$Builder;

    .line 550
    :cond_11
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasHistoricalProducer()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 551
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getHistoricalProducer()Llocation/unified/LocationProducer;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->setHistoricalProducer(Llocation/unified/LocationProducer;)Llocation/unified/LocationDescriptor$Builder;

    .line 553
    :cond_12
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasHistoricalProminence()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 554
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getHistoricalProminence()I

    move-result v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->setHistoricalProminence(I)Llocation/unified/LocationDescriptor$Builder;

    .line 556
    :cond_13
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasAttributes()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 557
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getAttributes()Llocation/unified/LocationAttributesProto;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->mergeAttributes(Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationDescriptor$Builder;

    .line 559
    :cond_14
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->hasDiagnosticInfo()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 560
    invoke-virtual {p1}, Llocation/unified/LocationDescriptor;->getDiagnosticInfo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationDescriptor$Builder;->setDiagnosticInfo(Ljava/lang/String;)Llocation/unified/LocationDescriptor$Builder;

    .line 562
    :cond_15
    # getter for: Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;
    invoke-static {p1}, Llocation/unified/LocationDescriptor;->access$300(Llocation/unified/LocationDescriptor;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 563
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;
    invoke-static {v0}, Llocation/unified/LocationDescriptor;->access$300(Llocation/unified/LocationDescriptor;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 564
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$302(Llocation/unified/LocationDescriptor;Ljava/util/List;)Ljava/util/List;

    .line 566
    :cond_16
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;
    invoke-static {v0}, Llocation/unified/LocationDescriptor;->access$300(Llocation/unified/LocationDescriptor;)Ljava/util/List;

    move-result-object v0

    # getter for: Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;
    invoke-static {p1}, Llocation/unified/LocationDescriptor;->access$300(Llocation/unified/LocationDescriptor;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public mergeLatlng(Llocation/unified/LatLng;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LatLng;

    .prologue
    .line 844
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    invoke-virtual {v0}, Llocation/unified/LocationDescriptor;->hasLatlng()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->latlng_:Llocation/unified/LatLng;
    invoke-static {v0}, Llocation/unified/LocationDescriptor;->access$1300(Llocation/unified/LocationDescriptor;)Llocation/unified/LatLng;

    move-result-object v0

    invoke-static {}, Llocation/unified/LatLng;->getDefaultInstance()Llocation/unified/LatLng;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 846
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    iget-object v1, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->latlng_:Llocation/unified/LatLng;
    invoke-static {v1}, Llocation/unified/LocationDescriptor;->access$1300(Llocation/unified/LocationDescriptor;)Llocation/unified/LatLng;

    move-result-object v1

    invoke-static {v1}, Llocation/unified/LatLng;->newBuilder(Llocation/unified/LatLng;)Llocation/unified/LatLng$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Llocation/unified/LatLng$Builder;->mergeFrom(Llocation/unified/LatLng;)Llocation/unified/LatLng$Builder;

    move-result-object v1

    invoke-virtual {v1}, Llocation/unified/LatLng$Builder;->buildPartial()Llocation/unified/LatLng;

    move-result-object v1

    # setter for: Llocation/unified/LocationDescriptor;->latlng_:Llocation/unified/LatLng;
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$1302(Llocation/unified/LocationDescriptor;Llocation/unified/LatLng;)Llocation/unified/LatLng;

    .line 851
    :goto_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasLatlng:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$1202(Llocation/unified/LocationDescriptor;Z)Z

    .line 852
    return-object p0

    .line 849
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->latlng_:Llocation/unified/LatLng;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$1302(Llocation/unified/LocationDescriptor;Llocation/unified/LatLng;)Llocation/unified/LatLng;

    goto :goto_0
.end method

.method public mergeLatlngSpan(Llocation/unified/LatLng;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LatLng;

    .prologue
    .line 881
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    invoke-virtual {v0}, Llocation/unified/LocationDescriptor;->hasLatlngSpan()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->latlngSpan_:Llocation/unified/LatLng;
    invoke-static {v0}, Llocation/unified/LocationDescriptor;->access$1500(Llocation/unified/LocationDescriptor;)Llocation/unified/LatLng;

    move-result-object v0

    invoke-static {}, Llocation/unified/LatLng;->getDefaultInstance()Llocation/unified/LatLng;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 883
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    iget-object v1, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->latlngSpan_:Llocation/unified/LatLng;
    invoke-static {v1}, Llocation/unified/LocationDescriptor;->access$1500(Llocation/unified/LocationDescriptor;)Llocation/unified/LatLng;

    move-result-object v1

    invoke-static {v1}, Llocation/unified/LatLng;->newBuilder(Llocation/unified/LatLng;)Llocation/unified/LatLng$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Llocation/unified/LatLng$Builder;->mergeFrom(Llocation/unified/LatLng;)Llocation/unified/LatLng$Builder;

    move-result-object v1

    invoke-virtual {v1}, Llocation/unified/LatLng$Builder;->buildPartial()Llocation/unified/LatLng;

    move-result-object v1

    # setter for: Llocation/unified/LocationDescriptor;->latlngSpan_:Llocation/unified/LatLng;
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$1502(Llocation/unified/LocationDescriptor;Llocation/unified/LatLng;)Llocation/unified/LatLng;

    .line 888
    :goto_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasLatlngSpan:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$1402(Llocation/unified/LocationDescriptor;Z)Z

    .line 889
    return-object p0

    .line 886
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->latlngSpan_:Llocation/unified/LatLng;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$1502(Llocation/unified/LocationDescriptor;Llocation/unified/LatLng;)Llocation/unified/LatLng;

    goto :goto_0
.end method

.method public mergeLevelFeatureId(Llocation/unified/FeatureIdProto;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/FeatureIdProto;

    .prologue
    .line 1046
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    invoke-virtual {v0}, Llocation/unified/LocationDescriptor;->hasLevelFeatureId()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->levelFeatureId_:Llocation/unified/FeatureIdProto;
    invoke-static {v0}, Llocation/unified/LocationDescriptor;->access$2700(Llocation/unified/LocationDescriptor;)Llocation/unified/FeatureIdProto;

    move-result-object v0

    invoke-static {}, Llocation/unified/FeatureIdProto;->getDefaultInstance()Llocation/unified/FeatureIdProto;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1048
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    iget-object v1, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->levelFeatureId_:Llocation/unified/FeatureIdProto;
    invoke-static {v1}, Llocation/unified/LocationDescriptor;->access$2700(Llocation/unified/LocationDescriptor;)Llocation/unified/FeatureIdProto;

    move-result-object v1

    invoke-static {v1}, Llocation/unified/FeatureIdProto;->newBuilder(Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Llocation/unified/FeatureIdProto$Builder;->mergeFrom(Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto$Builder;

    move-result-object v1

    invoke-virtual {v1}, Llocation/unified/FeatureIdProto$Builder;->buildPartial()Llocation/unified/FeatureIdProto;

    move-result-object v1

    # setter for: Llocation/unified/LocationDescriptor;->levelFeatureId_:Llocation/unified/FeatureIdProto;
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$2702(Llocation/unified/LocationDescriptor;Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto;

    .line 1053
    :goto_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasLevelFeatureId:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$2602(Llocation/unified/LocationDescriptor;Z)Z

    .line 1054
    return-object p0

    .line 1051
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->levelFeatureId_:Llocation/unified/FeatureIdProto;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$2702(Llocation/unified/LocationDescriptor;Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto;

    goto :goto_0
.end method

.method public mergeRect(Llocation/unified/LatLngRect;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LatLngRect;

    .prologue
    .line 918
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    invoke-virtual {v0}, Llocation/unified/LocationDescriptor;->hasRect()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->rect_:Llocation/unified/LatLngRect;
    invoke-static {v0}, Llocation/unified/LocationDescriptor;->access$1700(Llocation/unified/LocationDescriptor;)Llocation/unified/LatLngRect;

    move-result-object v0

    invoke-static {}, Llocation/unified/LatLngRect;->getDefaultInstance()Llocation/unified/LatLngRect;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 920
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    iget-object v1, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # getter for: Llocation/unified/LocationDescriptor;->rect_:Llocation/unified/LatLngRect;
    invoke-static {v1}, Llocation/unified/LocationDescriptor;->access$1700(Llocation/unified/LocationDescriptor;)Llocation/unified/LatLngRect;

    move-result-object v1

    invoke-static {v1}, Llocation/unified/LatLngRect;->newBuilder(Llocation/unified/LatLngRect;)Llocation/unified/LatLngRect$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Llocation/unified/LatLngRect$Builder;->mergeFrom(Llocation/unified/LatLngRect;)Llocation/unified/LatLngRect$Builder;

    move-result-object v1

    invoke-virtual {v1}, Llocation/unified/LatLngRect$Builder;->buildPartial()Llocation/unified/LatLngRect;

    move-result-object v1

    # setter for: Llocation/unified/LocationDescriptor;->rect_:Llocation/unified/LatLngRect;
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$1702(Llocation/unified/LocationDescriptor;Llocation/unified/LatLngRect;)Llocation/unified/LatLngRect;

    .line 925
    :goto_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasRect:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$1602(Llocation/unified/LocationDescriptor;Z)Z

    .line 926
    return-object p0

    .line 923
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->rect_:Llocation/unified/LatLngRect;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$1702(Llocation/unified/LocationDescriptor;Llocation/unified/LatLngRect;)Llocation/unified/LatLngRect;

    goto :goto_0
.end method

.method public setConfidence(I)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 960
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasConfidence:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$2002(Llocation/unified/LocationDescriptor;Z)Z

    .line 961
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->confidence_:I
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$2102(Llocation/unified/LocationDescriptor;I)I

    .line 962
    return-object p0
.end method

.method public setDiagnosticInfo(Ljava/lang/String;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1227
    if-nez p1, :cond_0

    .line 1228
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1230
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasDiagnosticInfo:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$4202(Llocation/unified/LocationDescriptor;Z)Z

    .line 1231
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->diagnosticInfo_:Ljava/lang/String;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$4302(Llocation/unified/LocationDescriptor;Ljava/lang/String;)Ljava/lang/String;

    .line 1232
    return-object p0
.end method

.method public setHistoricalProducer(Llocation/unified/LocationProducer;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LocationProducer;

    .prologue
    .line 1151
    if-nez p1, :cond_0

    .line 1152
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1154
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasHistoricalProducer:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$3602(Llocation/unified/LocationDescriptor;Z)Z

    .line 1155
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->historicalProducer_:Llocation/unified/LocationProducer;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$3702(Llocation/unified/LocationDescriptor;Llocation/unified/LocationProducer;)Llocation/unified/LocationProducer;

    .line 1156
    return-object p0
.end method

.method public setHistoricalProminence(I)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1172
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasHistoricalProminence:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$3802(Llocation/unified/LocationDescriptor;Z)Z

    .line 1173
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->historicalProminence_:I
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$3902(Llocation/unified/LocationDescriptor;I)I

    .line 1174
    return-object p0
.end method

.method public setHistoricalRole(Llocation/unified/LocationRole;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LocationRole;

    .prologue
    .line 1130
    if-nez p1, :cond_0

    .line 1131
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1133
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasHistoricalRole:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$3402(Llocation/unified/LocationDescriptor;Z)Z

    .line 1134
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->historicalRole_:Llocation/unified/LocationRole;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$3502(Llocation/unified/LocationDescriptor;Llocation/unified/LocationRole;)Llocation/unified/LocationRole;

    .line 1135
    return-object p0
.end method

.method public setLanguage(Ljava/lang/String;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1088
    if-nez p1, :cond_0

    .line 1089
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1091
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasLanguage:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$3002(Llocation/unified/LocationDescriptor;Z)Z

    .line 1092
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->language_:Ljava/lang/String;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$3102(Llocation/unified/LocationDescriptor;Ljava/lang/String;)Ljava/lang/String;

    .line 1093
    return-object p0
.end method

.method public setLatlng(Llocation/unified/LatLng;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LatLng;

    .prologue
    .line 831
    if-nez p1, :cond_0

    .line 832
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 834
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasLatlng:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$1202(Llocation/unified/LocationDescriptor;Z)Z

    .line 835
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->latlng_:Llocation/unified/LatLng;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$1302(Llocation/unified/LocationDescriptor;Llocation/unified/LatLng;)Llocation/unified/LatLng;

    .line 836
    return-object p0
.end method

.method public setLevelNumber(F)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 1070
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasLevelNumber:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$2802(Llocation/unified/LocationDescriptor;Z)Z

    .line 1071
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->levelNumber_:F
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$2902(Llocation/unified/LocationDescriptor;F)F

    .line 1072
    return-object p0
.end method

.method public setLoc(Ljava/lang/String;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 810
    if-nez p1, :cond_0

    .line 811
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 813
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasLoc:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$1002(Llocation/unified/LocationDescriptor;Z)Z

    .line 814
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->loc_:Ljava/lang/String;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$1102(Llocation/unified/LocationDescriptor;Ljava/lang/String;)Ljava/lang/String;

    .line 815
    return-object p0
.end method

.method public setMid(J)Llocation/unified/LocationDescriptor$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 1015
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasMid:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$2402(Llocation/unified/LocationDescriptor;Z)Z

    .line 1016
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->mid_:J
    invoke-static {v0, p1, p2}, Llocation/unified/LocationDescriptor;->access$2502(Llocation/unified/LocationDescriptor;J)J

    .line 1017
    return-object p0
.end method

.method public setProducer(Llocation/unified/LocationProducer;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LocationProducer;

    .prologue
    .line 771
    if-nez p1, :cond_0

    .line 772
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 774
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasProducer:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$602(Llocation/unified/LocationDescriptor;Z)Z

    .line 775
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->producer_:Llocation/unified/LocationProducer;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$702(Llocation/unified/LocationDescriptor;Llocation/unified/LocationProducer;)Llocation/unified/LocationProducer;

    .line 776
    return-object p0
.end method

.method public setProvenance(Llocation/unified/LocationProvenance;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LocationProvenance;

    .prologue
    .line 1109
    if-nez p1, :cond_0

    .line 1110
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1112
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasProvenance:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$3202(Llocation/unified/LocationDescriptor;Z)Z

    .line 1113
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->provenance_:Llocation/unified/LocationProvenance;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$3302(Llocation/unified/LocationDescriptor;Llocation/unified/LocationProvenance;)Llocation/unified/LocationProvenance;

    .line 1114
    return-object p0
.end method

.method public setRadius(F)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 942
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasRadius:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$1802(Llocation/unified/LocationDescriptor;Z)Z

    .line 943
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->radius_:F
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$1902(Llocation/unified/LocationDescriptor;F)F

    .line 944
    return-object p0
.end method

.method public setRole(Llocation/unified/LocationRole;)Llocation/unified/LocationDescriptor$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LocationRole;

    .prologue
    .line 750
    if-nez p1, :cond_0

    .line 751
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 753
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasRole:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$402(Llocation/unified/LocationDescriptor;Z)Z

    .line 754
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->role_:Llocation/unified/LocationRole;
    invoke-static {v0, p1}, Llocation/unified/LocationDescriptor;->access$502(Llocation/unified/LocationDescriptor;Llocation/unified/LocationRole;)Llocation/unified/LocationRole;

    .line 755
    return-object p0
.end method

.method public setTimestamp(J)Llocation/unified/LocationDescriptor$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 792
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationDescriptor;->hasTimestamp:Z
    invoke-static {v0, v1}, Llocation/unified/LocationDescriptor;->access$802(Llocation/unified/LocationDescriptor;Z)Z

    .line 793
    iget-object v0, p0, Llocation/unified/LocationDescriptor$Builder;->result:Llocation/unified/LocationDescriptor;

    # setter for: Llocation/unified/LocationDescriptor;->timestamp_:J
    invoke-static {v0, p1, p2}, Llocation/unified/LocationDescriptor;->access$902(Llocation/unified/LocationDescriptor;J)J

    .line 794
    return-object p0
.end method
