.class public final enum Llocation/unified/LocationRoleProto$LocationRole;
.super Ljava/lang/Enum;
.source "LocationRoleProto.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationRoleProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LocationRole"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Llocation/unified/LocationRoleProto$LocationRole;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Llocation/unified/LocationRoleProto$LocationRole;

.field public static final enum CURRENT_LOCATION:Llocation/unified/LocationRoleProto$LocationRole;

.field public static final enum DEFAULT_LOCATION:Llocation/unified/LocationRoleProto$LocationRole;

.field public static final enum HISTORICAL_LOCATION:Llocation/unified/LocationRoleProto$LocationRole;

.field public static final enum HISTORICAL_QUERY:Llocation/unified/LocationRoleProto$LocationRole;

.field public static final enum QUERY:Llocation/unified/LocationRoleProto$LocationRole;

.field public static final enum UNKNOWN_ROLE:Llocation/unified/LocationRoleProto$LocationRole;

.field public static final enum USER_SPECIFIED_FOR_REQUEST:Llocation/unified/LocationRoleProto$LocationRole;

.field public static final enum VIEWPORT:Llocation/unified/LocationRoleProto$LocationRole;

.field public static final enum WILDCARD_ROLE:Llocation/unified/LocationRoleProto$LocationRole;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Llocation/unified/LocationRoleProto$LocationRole;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 24
    new-instance v0, Llocation/unified/LocationRoleProto$LocationRole;

    const-string v1, "UNKNOWN_ROLE"

    invoke-direct {v0, v1, v5, v5, v5}, Llocation/unified/LocationRoleProto$LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRoleProto$LocationRole;->UNKNOWN_ROLE:Llocation/unified/LocationRoleProto$LocationRole;

    .line 25
    new-instance v0, Llocation/unified/LocationRoleProto$LocationRole;

    const-string v1, "CURRENT_LOCATION"

    invoke-direct {v0, v1, v6, v6, v6}, Llocation/unified/LocationRoleProto$LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRoleProto$LocationRole;->CURRENT_LOCATION:Llocation/unified/LocationRoleProto$LocationRole;

    .line 26
    new-instance v0, Llocation/unified/LocationRoleProto$LocationRole;

    const-string v1, "DEFAULT_LOCATION"

    invoke-direct {v0, v1, v7, v7, v7}, Llocation/unified/LocationRoleProto$LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRoleProto$LocationRole;->DEFAULT_LOCATION:Llocation/unified/LocationRoleProto$LocationRole;

    .line 27
    new-instance v0, Llocation/unified/LocationRoleProto$LocationRole;

    const-string v1, "QUERY"

    invoke-direct {v0, v1, v8, v8, v8}, Llocation/unified/LocationRoleProto$LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRoleProto$LocationRole;->QUERY:Llocation/unified/LocationRoleProto$LocationRole;

    .line 28
    new-instance v0, Llocation/unified/LocationRoleProto$LocationRole;

    const-string v1, "USER_SPECIFIED_FOR_REQUEST"

    invoke-direct {v0, v1, v9, v9, v9}, Llocation/unified/LocationRoleProto$LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRoleProto$LocationRole;->USER_SPECIFIED_FOR_REQUEST:Llocation/unified/LocationRoleProto$LocationRole;

    .line 29
    new-instance v0, Llocation/unified/LocationRoleProto$LocationRole;

    const-string v1, "HISTORICAL_QUERY"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationRoleProto$LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRoleProto$LocationRole;->HISTORICAL_QUERY:Llocation/unified/LocationRoleProto$LocationRole;

    .line 30
    new-instance v0, Llocation/unified/LocationRoleProto$LocationRole;

    const-string v1, "HISTORICAL_LOCATION"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationRoleProto$LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRoleProto$LocationRole;->HISTORICAL_LOCATION:Llocation/unified/LocationRoleProto$LocationRole;

    .line 31
    new-instance v0, Llocation/unified/LocationRoleProto$LocationRole;

    const-string v1, "VIEWPORT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationRoleProto$LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRoleProto$LocationRole;->VIEWPORT:Llocation/unified/LocationRoleProto$LocationRole;

    .line 32
    new-instance v0, Llocation/unified/LocationRoleProto$LocationRole;

    const-string v1, "WILDCARD_ROLE"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/4 v4, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationRoleProto$LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRoleProto$LocationRole;->WILDCARD_ROLE:Llocation/unified/LocationRoleProto$LocationRole;

    .line 22
    const/16 v0, 0x9

    new-array v0, v0, [Llocation/unified/LocationRoleProto$LocationRole;

    sget-object v1, Llocation/unified/LocationRoleProto$LocationRole;->UNKNOWN_ROLE:Llocation/unified/LocationRoleProto$LocationRole;

    aput-object v1, v0, v5

    sget-object v1, Llocation/unified/LocationRoleProto$LocationRole;->CURRENT_LOCATION:Llocation/unified/LocationRoleProto$LocationRole;

    aput-object v1, v0, v6

    sget-object v1, Llocation/unified/LocationRoleProto$LocationRole;->DEFAULT_LOCATION:Llocation/unified/LocationRoleProto$LocationRole;

    aput-object v1, v0, v7

    sget-object v1, Llocation/unified/LocationRoleProto$LocationRole;->QUERY:Llocation/unified/LocationRoleProto$LocationRole;

    aput-object v1, v0, v8

    sget-object v1, Llocation/unified/LocationRoleProto$LocationRole;->USER_SPECIFIED_FOR_REQUEST:Llocation/unified/LocationRoleProto$LocationRole;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Llocation/unified/LocationRoleProto$LocationRole;->HISTORICAL_QUERY:Llocation/unified/LocationRoleProto$LocationRole;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Llocation/unified/LocationRoleProto$LocationRole;->HISTORICAL_LOCATION:Llocation/unified/LocationRoleProto$LocationRole;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Llocation/unified/LocationRoleProto$LocationRole;->VIEWPORT:Llocation/unified/LocationRoleProto$LocationRole;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Llocation/unified/LocationRoleProto$LocationRole;->WILDCARD_ROLE:Llocation/unified/LocationRoleProto$LocationRole;

    aput-object v2, v0, v1

    sput-object v0, Llocation/unified/LocationRoleProto$LocationRole;->$VALUES:[Llocation/unified/LocationRoleProto$LocationRole;

    .line 58
    new-instance v0, Llocation/unified/LocationRoleProto$LocationRole$1;

    invoke-direct {v0}, Llocation/unified/LocationRoleProto$LocationRole$1;-><init>()V

    sput-object v0, Llocation/unified/LocationRoleProto$LocationRole;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    iput p3, p0, Llocation/unified/LocationRoleProto$LocationRole;->index:I

    .line 69
    iput p4, p0, Llocation/unified/LocationRoleProto$LocationRole;->value:I

    .line 70
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Llocation/unified/LocationRoleProto$LocationRole;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Llocation/unified/LocationRoleProto$LocationRole;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Llocation/unified/LocationRoleProto$LocationRole;

    return-object v0
.end method

.method public static values()[Llocation/unified/LocationRoleProto$LocationRole;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Llocation/unified/LocationRoleProto$LocationRole;->$VALUES:[Llocation/unified/LocationRoleProto$LocationRole;

    invoke-virtual {v0}, [Llocation/unified/LocationRoleProto$LocationRole;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llocation/unified/LocationRoleProto$LocationRole;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Llocation/unified/LocationRoleProto$LocationRole;->value:I

    return v0
.end method
