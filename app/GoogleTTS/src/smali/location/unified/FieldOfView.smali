.class public final Llocation/unified/FieldOfView;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FieldOfView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llocation/unified/FieldOfView$1;,
        Llocation/unified/FieldOfView$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Llocation/unified/FieldOfView;


# instance fields
.field private fieldOfViewXDegrees_:F

.field private fieldOfViewYDegrees_:F

.field private hasFieldOfViewXDegrees:Z

.field private hasFieldOfViewYDegrees:Z

.field private hasScreenWidthPixels:Z

.field private memoizedSerializedSize:I

.field private screenWidthPixels_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 327
    new-instance v0, Llocation/unified/FieldOfView;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Llocation/unified/FieldOfView;-><init>(Z)V

    sput-object v0, Llocation/unified/FieldOfView;->defaultInstance:Llocation/unified/FieldOfView;

    .line 328
    invoke-static {}, Llocation/unified/LocationDescriptorProto;->internalForceInit()V

    .line 329
    sget-object v0, Llocation/unified/FieldOfView;->defaultInstance:Llocation/unified/FieldOfView;

    invoke-direct {v0}, Llocation/unified/FieldOfView;->initFields()V

    .line 330
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Llocation/unified/FieldOfView;->fieldOfViewXDegrees_:F

    .line 32
    iput v0, p0, Llocation/unified/FieldOfView;->fieldOfViewYDegrees_:F

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Llocation/unified/FieldOfView;->screenWidthPixels_:I

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/FieldOfView;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Llocation/unified/FieldOfView;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Llocation/unified/FieldOfView$1;)V
    .locals 0
    .param p1, "x0"    # Llocation/unified/FieldOfView$1;

    .prologue
    .line 5
    invoke-direct {p0}, Llocation/unified/FieldOfView;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Llocation/unified/FieldOfView;->fieldOfViewXDegrees_:F

    .line 32
    iput v0, p0, Llocation/unified/FieldOfView;->fieldOfViewYDegrees_:F

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Llocation/unified/FieldOfView;->screenWidthPixels_:I

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/FieldOfView;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Llocation/unified/FieldOfView;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/FieldOfView;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/FieldOfView;->hasFieldOfViewXDegrees:Z

    return p1
.end method

.method static synthetic access$402(Llocation/unified/FieldOfView;F)F
    .locals 0
    .param p0, "x0"    # Llocation/unified/FieldOfView;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/FieldOfView;->fieldOfViewXDegrees_:F

    return p1
.end method

.method static synthetic access$502(Llocation/unified/FieldOfView;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/FieldOfView;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/FieldOfView;->hasFieldOfViewYDegrees:Z

    return p1
.end method

.method static synthetic access$602(Llocation/unified/FieldOfView;F)F
    .locals 0
    .param p0, "x0"    # Llocation/unified/FieldOfView;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/FieldOfView;->fieldOfViewYDegrees_:F

    return p1
.end method

.method static synthetic access$702(Llocation/unified/FieldOfView;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/FieldOfView;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/FieldOfView;->hasScreenWidthPixels:Z

    return p1
.end method

.method static synthetic access$802(Llocation/unified/FieldOfView;I)I
    .locals 0
    .param p0, "x0"    # Llocation/unified/FieldOfView;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/FieldOfView;->screenWidthPixels_:I

    return p1
.end method

.method public static getDefaultInstance()Llocation/unified/FieldOfView;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Llocation/unified/FieldOfView;->defaultInstance:Llocation/unified/FieldOfView;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public static newBuilder()Llocation/unified/FieldOfView$Builder;
    .locals 1

    .prologue
    .line 152
    # invokes: Llocation/unified/FieldOfView$Builder;->create()Llocation/unified/FieldOfView$Builder;
    invoke-static {}, Llocation/unified/FieldOfView$Builder;->access$100()Llocation/unified/FieldOfView$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Llocation/unified/FieldOfView;)Llocation/unified/FieldOfView$Builder;
    .locals 1
    .param p0, "prototype"    # Llocation/unified/FieldOfView;

    .prologue
    .line 155
    invoke-static {}, Llocation/unified/FieldOfView;->newBuilder()Llocation/unified/FieldOfView$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Llocation/unified/FieldOfView$Builder;->mergeFrom(Llocation/unified/FieldOfView;)Llocation/unified/FieldOfView$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/FieldOfView;->getDefaultInstanceForType()Llocation/unified/FieldOfView;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Llocation/unified/FieldOfView;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Llocation/unified/FieldOfView;->defaultInstance:Llocation/unified/FieldOfView;

    return-object v0
.end method

.method public getFieldOfViewXDegrees()F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Llocation/unified/FieldOfView;->fieldOfViewXDegrees_:F

    return v0
.end method

.method public getFieldOfViewYDegrees()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Llocation/unified/FieldOfView;->fieldOfViewYDegrees_:F

    return v0
.end method

.method public getScreenWidthPixels()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Llocation/unified/FieldOfView;->screenWidthPixels_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 65
    iget v0, p0, Llocation/unified/FieldOfView;->memoizedSerializedSize:I

    .line 66
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 82
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 68
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 69
    invoke-virtual {p0}, Llocation/unified/FieldOfView;->hasFieldOfViewXDegrees()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 70
    const/4 v2, 0x1

    invoke-virtual {p0}, Llocation/unified/FieldOfView;->getFieldOfViewXDegrees()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 73
    :cond_1
    invoke-virtual {p0}, Llocation/unified/FieldOfView;->hasFieldOfViewYDegrees()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 74
    const/4 v2, 0x2

    invoke-virtual {p0}, Llocation/unified/FieldOfView;->getFieldOfViewYDegrees()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 77
    :cond_2
    invoke-virtual {p0}, Llocation/unified/FieldOfView;->hasScreenWidthPixels()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 78
    const/4 v2, 0x3

    invoke-virtual {p0}, Llocation/unified/FieldOfView;->getScreenWidthPixels()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 81
    :cond_3
    iput v0, p0, Llocation/unified/FieldOfView;->memoizedSerializedSize:I

    move v1, v0

    .line 82
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasFieldOfViewXDegrees()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Llocation/unified/FieldOfView;->hasFieldOfViewXDegrees:Z

    return v0
.end method

.method public hasFieldOfViewYDegrees()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Llocation/unified/FieldOfView;->hasFieldOfViewYDegrees:Z

    return v0
.end method

.method public hasScreenWidthPixels()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Llocation/unified/FieldOfView;->hasScreenWidthPixels:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/FieldOfView;->toBuilder()Llocation/unified/FieldOfView$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Llocation/unified/FieldOfView$Builder;
    .locals 1

    .prologue
    .line 157
    invoke-static {p0}, Llocation/unified/FieldOfView;->newBuilder(Llocation/unified/FieldOfView;)Llocation/unified/FieldOfView$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Llocation/unified/FieldOfView;->getSerializedSize()I

    .line 52
    invoke-virtual {p0}, Llocation/unified/FieldOfView;->hasFieldOfViewXDegrees()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0}, Llocation/unified/FieldOfView;->getFieldOfViewXDegrees()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 55
    :cond_0
    invoke-virtual {p0}, Llocation/unified/FieldOfView;->hasFieldOfViewYDegrees()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    const/4 v0, 0x2

    invoke-virtual {p0}, Llocation/unified/FieldOfView;->getFieldOfViewYDegrees()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 58
    :cond_1
    invoke-virtual {p0}, Llocation/unified/FieldOfView;->hasScreenWidthPixels()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 59
    const/4 v0, 0x3

    invoke-virtual {p0}, Llocation/unified/FieldOfView;->getScreenWidthPixels()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 61
    :cond_2
    return-void
.end method
