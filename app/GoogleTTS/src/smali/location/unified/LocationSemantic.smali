.class public final enum Llocation/unified/LocationSemantic;
.super Ljava/lang/Enum;
.source "LocationSemantic.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Llocation/unified/LocationSemantic;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Llocation/unified/LocationSemantic;

.field public static final enum SEMANTIC_HOME:Llocation/unified/LocationSemantic;

.field public static final enum SEMANTIC_ONBOARD_TRANSIT:Llocation/unified/LocationSemantic;

.field public static final enum SEMANTIC_REROUTE_PROPOSED:Llocation/unified/LocationSemantic;

.field public static final enum SEMANTIC_REROUTE_SOURCE:Llocation/unified/LocationSemantic;

.field public static final enum SEMANTIC_REROUTE_TAKEN:Llocation/unified/LocationSemantic;

.field public static final enum SEMANTIC_UNKNOWN:Llocation/unified/LocationSemantic;

.field public static final enum SEMANTIC_WORK:Llocation/unified/LocationSemantic;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Llocation/unified/LocationSemantic;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 7
    new-instance v0, Llocation/unified/LocationSemantic;

    const-string v1, "SEMANTIC_UNKNOWN"

    invoke-direct {v0, v1, v5, v5, v5}, Llocation/unified/LocationSemantic;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationSemantic;->SEMANTIC_UNKNOWN:Llocation/unified/LocationSemantic;

    .line 8
    new-instance v0, Llocation/unified/LocationSemantic;

    const-string v1, "SEMANTIC_REROUTE_SOURCE"

    invoke-direct {v0, v1, v6, v6, v6}, Llocation/unified/LocationSemantic;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationSemantic;->SEMANTIC_REROUTE_SOURCE:Llocation/unified/LocationSemantic;

    .line 9
    new-instance v0, Llocation/unified/LocationSemantic;

    const-string v1, "SEMANTIC_REROUTE_PROPOSED"

    invoke-direct {v0, v1, v7, v7, v7}, Llocation/unified/LocationSemantic;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationSemantic;->SEMANTIC_REROUTE_PROPOSED:Llocation/unified/LocationSemantic;

    .line 10
    new-instance v0, Llocation/unified/LocationSemantic;

    const-string v1, "SEMANTIC_REROUTE_TAKEN"

    invoke-direct {v0, v1, v8, v8, v8}, Llocation/unified/LocationSemantic;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationSemantic;->SEMANTIC_REROUTE_TAKEN:Llocation/unified/LocationSemantic;

    .line 11
    new-instance v0, Llocation/unified/LocationSemantic;

    const-string v1, "SEMANTIC_HOME"

    invoke-direct {v0, v1, v9, v9, v9}, Llocation/unified/LocationSemantic;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationSemantic;->SEMANTIC_HOME:Llocation/unified/LocationSemantic;

    .line 12
    new-instance v0, Llocation/unified/LocationSemantic;

    const-string v1, "SEMANTIC_WORK"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationSemantic;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationSemantic;->SEMANTIC_WORK:Llocation/unified/LocationSemantic;

    .line 13
    new-instance v0, Llocation/unified/LocationSemantic;

    const-string v1, "SEMANTIC_ONBOARD_TRANSIT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationSemantic;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationSemantic;->SEMANTIC_ONBOARD_TRANSIT:Llocation/unified/LocationSemantic;

    .line 5
    const/4 v0, 0x7

    new-array v0, v0, [Llocation/unified/LocationSemantic;

    sget-object v1, Llocation/unified/LocationSemantic;->SEMANTIC_UNKNOWN:Llocation/unified/LocationSemantic;

    aput-object v1, v0, v5

    sget-object v1, Llocation/unified/LocationSemantic;->SEMANTIC_REROUTE_SOURCE:Llocation/unified/LocationSemantic;

    aput-object v1, v0, v6

    sget-object v1, Llocation/unified/LocationSemantic;->SEMANTIC_REROUTE_PROPOSED:Llocation/unified/LocationSemantic;

    aput-object v1, v0, v7

    sget-object v1, Llocation/unified/LocationSemantic;->SEMANTIC_REROUTE_TAKEN:Llocation/unified/LocationSemantic;

    aput-object v1, v0, v8

    sget-object v1, Llocation/unified/LocationSemantic;->SEMANTIC_HOME:Llocation/unified/LocationSemantic;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Llocation/unified/LocationSemantic;->SEMANTIC_WORK:Llocation/unified/LocationSemantic;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Llocation/unified/LocationSemantic;->SEMANTIC_ONBOARD_TRANSIT:Llocation/unified/LocationSemantic;

    aput-object v2, v0, v1

    sput-object v0, Llocation/unified/LocationSemantic;->$VALUES:[Llocation/unified/LocationSemantic;

    .line 37
    new-instance v0, Llocation/unified/LocationSemantic$1;

    invoke-direct {v0}, Llocation/unified/LocationSemantic$1;-><init>()V

    sput-object v0, Llocation/unified/LocationSemantic;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput p3, p0, Llocation/unified/LocationSemantic;->index:I

    .line 48
    iput p4, p0, Llocation/unified/LocationSemantic;->value:I

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Llocation/unified/LocationSemantic;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Llocation/unified/LocationSemantic;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Llocation/unified/LocationSemantic;

    return-object v0
.end method

.method public static values()[Llocation/unified/LocationSemantic;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Llocation/unified/LocationSemantic;->$VALUES:[Llocation/unified/LocationSemantic;

    invoke-virtual {v0}, [Llocation/unified/LocationSemantic;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llocation/unified/LocationSemantic;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Llocation/unified/LocationSemantic;->value:I

    return v0
.end method
