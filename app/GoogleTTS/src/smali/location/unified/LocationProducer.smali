.class public final enum Llocation/unified/LocationProducer;
.super Ljava/lang/Enum;
.source "LocationProducer.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Llocation/unified/LocationProducer;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Llocation/unified/LocationProducer;

.field public static final enum ADS_CRITERIA_ID:Llocation/unified/LocationProducer;

.field public static final enum ADS_GEO_PARAM:Llocation/unified/LocationProducer;

.field public static final enum ADS_PARTNER_GEO_PARAM:Llocation/unified/LocationProducer;

.field public static final enum CALENDAR:Llocation/unified/LocationProducer;

.field public static final enum CARRIER_COUNTRY:Llocation/unified/LocationProducer;

.field public static final enum CIRCULARS_FRONTEND:Llocation/unified/LocationProducer;

.field public static final enum DEFAULT_LOCATION_OVERRIDE_PRODUCER:Llocation/unified/LocationProducer;

.field public static final enum DEVICE_LOCATION:Llocation/unified/LocationProducer;

.field public static final enum GAIA_LOCATION_HISTORY:Llocation/unified/LocationProducer;

.field public static final enum GMAIL_THEME:Llocation/unified/LocationProducer;

.field public static final enum GOOGLE_HOST_DOMAIN:Llocation/unified/LocationProducer;

.field public static final enum GWS_MOBILE_HISTORY_ZWIEBACK:Llocation/unified/LocationProducer;

.field public static final enum HULK_USER_PLACES_CONFIRMED:Llocation/unified/LocationProducer;

.field public static final enum HULK_USER_PLACES_INFERRED:Llocation/unified/LocationProducer;

.field public static final enum IGOOGLE:Llocation/unified/LocationProducer;

.field public static final enum IP_ADDRESS:Llocation/unified/LocationProducer;

.field public static final enum IP_ADDRESS_REALTIME:Llocation/unified/LocationProducer;

.field public static final enum JURISDICTION_COUNTRY:Llocation/unified/LocationProducer;

.field public static final enum LEGACY_GL_COOKIE:Llocation/unified/LocationProducer;

.field public static final enum LEGACY_GL_PARAM:Llocation/unified/LocationProducer;

.field public static final enum LEGACY_MOBILE_FRONTEND_GLL:Llocation/unified/LocationProducer;

.field public static final enum LEGACY_MOBILE_FRONTEND_NEAR:Llocation/unified/LocationProducer;

.field public static final enum LEGACY_NEAR_PARAM:Llocation/unified/LocationProducer;

.field public static final enum LEGACY_PARTNER_GL_PARAM:Llocation/unified/LocationProducer;

.field public static final enum LEGACY_TOOLBAR_HEADER:Llocation/unified/LocationProducer;

.field public static final enum LOCAL_UNIVERSAL:Llocation/unified/LocationProducer;

.field public static final enum LOGGED_IN_USER_SPECIFIED:Llocation/unified/LocationProducer;

.field public static final enum MAPS_FRONTEND:Llocation/unified/LocationProducer;

.field public static final enum MOBILE_APP:Llocation/unified/LocationProducer;

.field public static final enum MOBILE_FE_HISTORY:Llocation/unified/LocationProducer;

.field public static final enum MOBILE_SELECTED:Llocation/unified/LocationProducer;

.field public static final enum OZ_FRONTEND:Llocation/unified/LocationProducer;

.field public static final enum PARTNER:Llocation/unified/LocationProducer;

.field public static final enum PREF_L_FIELD_ADDRESS:Llocation/unified/LocationProducer;

.field public static final enum PRODUCT_SEARCH_FRONTEND:Llocation/unified/LocationProducer;

.field public static final enum QREF:Llocation/unified/LocationProducer;

.field public static final enum QUERY_HISTORY_INFERRED:Llocation/unified/LocationProducer;

.field public static final enum QUERY_LOCATION_OVERRIDE_PRODUCER:Llocation/unified/LocationProducer;

.field public static final enum RQUERY:Llocation/unified/LocationProducer;

.field public static final enum SEARCH_TOOLBELT:Llocation/unified/LocationProducer;

.field public static final enum SHOPPING_SEARCH_API:Llocation/unified/LocationProducer;

.field public static final enum SHOWTIME_ONEBOX:Llocation/unified/LocationProducer;

.field public static final enum SMS_SEARCH:Llocation/unified/LocationProducer;

.field public static final enum SNAP_TO_PLACE_EXPLICIT:Llocation/unified/LocationProducer;

.field public static final enum SNAP_TO_PLACE_IMPLICIT:Llocation/unified/LocationProducer;

.field public static final enum SQUERY:Llocation/unified/LocationProducer;

.field public static final enum STICKINESS_PARAMS:Llocation/unified/LocationProducer;

.field public static final enum TACTILE_NEARBY_PARAM:Llocation/unified/LocationProducer;

.field public static final enum TURN_BY_TURN_NAVIGATION_REROUTE:Llocation/unified/LocationProducer;

.field public static final enum UNKNOWN_PRODUCER:Llocation/unified/LocationProducer;

.field public static final enum VIEWPORT_PARAMS:Llocation/unified/LocationProducer;

.field public static final enum WEB_SEARCH_PREFERENCES_PAGE:Llocation/unified/LocationProducer;

.field public static final enum WEB_SEARCH_RESULTS_PAGE_SHARED:Llocation/unified/LocationProducer;

.field public static final enum WILDCARD_PRODUCER:Llocation/unified/LocationProducer;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Llocation/unified/LocationProducer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 7
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "UNKNOWN_PRODUCER"

    invoke-direct {v0, v1, v5, v5, v5}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->UNKNOWN_PRODUCER:Llocation/unified/LocationProducer;

    .line 8
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "LOGGED_IN_USER_SPECIFIED"

    invoke-direct {v0, v1, v6, v6, v6}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->LOGGED_IN_USER_SPECIFIED:Llocation/unified/LocationProducer;

    .line 9
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "PREF_L_FIELD_ADDRESS"

    invoke-direct {v0, v1, v7, v7, v7}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->PREF_L_FIELD_ADDRESS:Llocation/unified/LocationProducer;

    .line 10
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "IP_ADDRESS"

    invoke-direct {v0, v1, v8, v8, v8}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->IP_ADDRESS:Llocation/unified/LocationProducer;

    .line 11
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "IP_ADDRESS_REALTIME"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v9, v9, v2}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->IP_ADDRESS_REALTIME:Llocation/unified/LocationProducer;

    .line 12
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "GOOGLE_HOST_DOMAIN"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3, v9}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->GOOGLE_HOST_DOMAIN:Llocation/unified/LocationProducer;

    .line 13
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "RQUERY"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->RQUERY:Llocation/unified/LocationProducer;

    .line 14
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "SQUERY"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->SQUERY:Llocation/unified/LocationProducer;

    .line 15
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "QUERY_LOCATION_OVERRIDE_PRODUCER"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x29

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->QUERY_LOCATION_OVERRIDE_PRODUCER:Llocation/unified/LocationProducer;

    .line 16
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "QREF"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const/16 v4, 0x2c

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->QREF:Llocation/unified/LocationProducer;

    .line 17
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "DEVICE_LOCATION"

    const/16 v2, 0xa

    const/16 v3, 0xa

    const/16 v4, 0xc

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->DEVICE_LOCATION:Llocation/unified/LocationProducer;

    .line 18
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "LEGACY_NEAR_PARAM"

    const/16 v2, 0xb

    const/16 v3, 0xb

    const/16 v4, 0xb

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->LEGACY_NEAR_PARAM:Llocation/unified/LocationProducer;

    .line 19
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "CARRIER_COUNTRY"

    const/16 v2, 0xc

    const/16 v3, 0xc

    const/16 v4, 0x11

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->CARRIER_COUNTRY:Llocation/unified/LocationProducer;

    .line 20
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "JURISDICTION_COUNTRY"

    const/16 v2, 0xd

    const/16 v3, 0xd

    const/16 v4, 0x33

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->JURISDICTION_COUNTRY:Llocation/unified/LocationProducer;

    .line 21
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "SHOWTIME_ONEBOX"

    const/16 v2, 0xe

    const/16 v3, 0xe

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->SHOWTIME_ONEBOX:Llocation/unified/LocationProducer;

    .line 22
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "LOCAL_UNIVERSAL"

    const/16 v2, 0xf

    const/16 v3, 0xf

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->LOCAL_UNIVERSAL:Llocation/unified/LocationProducer;

    .line 23
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "SEARCH_TOOLBELT"

    const/16 v2, 0x10

    const/16 v3, 0x10

    const/16 v4, 0xd

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->SEARCH_TOOLBELT:Llocation/unified/LocationProducer;

    .line 24
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "MOBILE_FE_HISTORY"

    const/16 v2, 0x11

    const/16 v3, 0x11

    const/16 v4, 0xe

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->MOBILE_FE_HISTORY:Llocation/unified/LocationProducer;

    .line 25
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "GWS_MOBILE_HISTORY_ZWIEBACK"

    const/16 v2, 0x12

    const/16 v3, 0x12

    const/16 v4, 0x22

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->GWS_MOBILE_HISTORY_ZWIEBACK:Llocation/unified/LocationProducer;

    .line 26
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "MOBILE_SELECTED"

    const/16 v2, 0x13

    const/16 v3, 0x13

    const/16 v4, 0xf

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->MOBILE_SELECTED:Llocation/unified/LocationProducer;

    .line 27
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "PARTNER"

    const/16 v2, 0x14

    const/16 v3, 0x14

    const/16 v4, 0x10

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->PARTNER:Llocation/unified/LocationProducer;

    .line 28
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "WEB_SEARCH_RESULTS_PAGE_SHARED"

    const/16 v2, 0x15

    const/16 v3, 0x15

    const/16 v4, 0x12

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->WEB_SEARCH_RESULTS_PAGE_SHARED:Llocation/unified/LocationProducer;

    .line 29
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "WEB_SEARCH_PREFERENCES_PAGE"

    const/16 v2, 0x16

    const/16 v3, 0x16

    const/16 v4, 0x14

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->WEB_SEARCH_PREFERENCES_PAGE:Llocation/unified/LocationProducer;

    .line 30
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "MAPS_FRONTEND"

    const/16 v2, 0x17

    const/16 v3, 0x17

    const/16 v4, 0x15

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->MAPS_FRONTEND:Llocation/unified/LocationProducer;

    .line 31
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "PRODUCT_SEARCH_FRONTEND"

    const/16 v2, 0x18

    const/16 v3, 0x18

    const/16 v4, 0x16

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->PRODUCT_SEARCH_FRONTEND:Llocation/unified/LocationProducer;

    .line 32
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "ADS_CRITERIA_ID"

    const/16 v2, 0x19

    const/16 v3, 0x19

    const/16 v4, 0x17

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->ADS_CRITERIA_ID:Llocation/unified/LocationProducer;

    .line 33
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "MOBILE_APP"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    const/16 v4, 0x18

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->MOBILE_APP:Llocation/unified/LocationProducer;

    .line 34
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "QUERY_HISTORY_INFERRED"

    const/16 v2, 0x1b

    const/16 v3, 0x1b

    const/16 v4, 0x19

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->QUERY_HISTORY_INFERRED:Llocation/unified/LocationProducer;

    .line 35
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "GMAIL_THEME"

    const/16 v2, 0x1c

    const/16 v3, 0x1c

    const/16 v4, 0x1a

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->GMAIL_THEME:Llocation/unified/LocationProducer;

    .line 36
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "IGOOGLE"

    const/16 v2, 0x1d

    const/16 v3, 0x1d

    const/16 v4, 0x1b

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->IGOOGLE:Llocation/unified/LocationProducer;

    .line 37
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "CALENDAR"

    const/16 v2, 0x1e

    const/16 v3, 0x1e

    const/16 v4, 0x1c

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->CALENDAR:Llocation/unified/LocationProducer;

    .line 38
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "SMS_SEARCH"

    const/16 v2, 0x1f

    const/16 v3, 0x1f

    const/16 v4, 0x1d

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->SMS_SEARCH:Llocation/unified/LocationProducer;

    .line 39
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "LEGACY_GL_PARAM"

    const/16 v2, 0x20

    const/16 v3, 0x20

    const/16 v4, 0x1e

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->LEGACY_GL_PARAM:Llocation/unified/LocationProducer;

    .line 40
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "LEGACY_PARTNER_GL_PARAM"

    const/16 v2, 0x21

    const/16 v3, 0x21

    const/16 v4, 0x1f

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->LEGACY_PARTNER_GL_PARAM:Llocation/unified/LocationProducer;

    .line 41
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "LEGACY_GL_COOKIE"

    const/16 v2, 0x22

    const/16 v3, 0x22

    const/16 v4, 0x23

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->LEGACY_GL_COOKIE:Llocation/unified/LocationProducer;

    .line 42
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "CIRCULARS_FRONTEND"

    const/16 v2, 0x23

    const/16 v3, 0x23

    const/16 v4, 0x21

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->CIRCULARS_FRONTEND:Llocation/unified/LocationProducer;

    .line 43
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "SHOPPING_SEARCH_API"

    const/16 v2, 0x24

    const/16 v3, 0x24

    const/16 v4, 0x24

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->SHOPPING_SEARCH_API:Llocation/unified/LocationProducer;

    .line 44
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "OZ_FRONTEND"

    const/16 v2, 0x25

    const/16 v3, 0x25

    const/16 v4, 0x25

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->OZ_FRONTEND:Llocation/unified/LocationProducer;

    .line 45
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "ADS_GEO_PARAM"

    const/16 v2, 0x26

    const/16 v3, 0x26

    const/16 v4, 0x26

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->ADS_GEO_PARAM:Llocation/unified/LocationProducer;

    .line 46
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "ADS_PARTNER_GEO_PARAM"

    const/16 v2, 0x27

    const/16 v3, 0x27

    const/16 v4, 0x27

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->ADS_PARTNER_GEO_PARAM:Llocation/unified/LocationProducer;

    .line 47
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "DEFAULT_LOCATION_OVERRIDE_PRODUCER"

    const/16 v2, 0x28

    const/16 v3, 0x28

    const/16 v4, 0x20

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->DEFAULT_LOCATION_OVERRIDE_PRODUCER:Llocation/unified/LocationProducer;

    .line 48
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "VIEWPORT_PARAMS"

    const/16 v2, 0x29

    const/16 v3, 0x29

    const/16 v4, 0x28

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->VIEWPORT_PARAMS:Llocation/unified/LocationProducer;

    .line 49
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "GAIA_LOCATION_HISTORY"

    const/16 v2, 0x2a

    const/16 v3, 0x2a

    const/16 v4, 0x2b

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->GAIA_LOCATION_HISTORY:Llocation/unified/LocationProducer;

    .line 50
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "STICKINESS_PARAMS"

    const/16 v2, 0x2b

    const/16 v3, 0x2b

    const/16 v4, 0x2d

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->STICKINESS_PARAMS:Llocation/unified/LocationProducer;

    .line 51
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "TURN_BY_TURN_NAVIGATION_REROUTE"

    const/16 v2, 0x2c

    const/16 v3, 0x2c

    const/16 v4, 0x2e

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->TURN_BY_TURN_NAVIGATION_REROUTE:Llocation/unified/LocationProducer;

    .line 52
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "SNAP_TO_PLACE_IMPLICIT"

    const/16 v2, 0x2d

    const/16 v3, 0x2d

    const/16 v4, 0x2f

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->SNAP_TO_PLACE_IMPLICIT:Llocation/unified/LocationProducer;

    .line 53
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "SNAP_TO_PLACE_EXPLICIT"

    const/16 v2, 0x2e

    const/16 v3, 0x2e

    const/16 v4, 0x30

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->SNAP_TO_PLACE_EXPLICIT:Llocation/unified/LocationProducer;

    .line 54
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "HULK_USER_PLACES_CONFIRMED"

    const/16 v2, 0x2f

    const/16 v3, 0x2f

    const/16 v4, 0x31

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->HULK_USER_PLACES_CONFIRMED:Llocation/unified/LocationProducer;

    .line 55
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "HULK_USER_PLACES_INFERRED"

    const/16 v2, 0x30

    const/16 v3, 0x30

    const/16 v4, 0x32

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->HULK_USER_PLACES_INFERRED:Llocation/unified/LocationProducer;

    .line 56
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "TACTILE_NEARBY_PARAM"

    const/16 v2, 0x31

    const/16 v3, 0x31

    const/16 v4, 0x34

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->TACTILE_NEARBY_PARAM:Llocation/unified/LocationProducer;

    .line 57
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "WILDCARD_PRODUCER"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/4 v4, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->WILDCARD_PRODUCER:Llocation/unified/LocationProducer;

    .line 58
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "LEGACY_TOOLBAR_HEADER"

    const/16 v2, 0x33

    const/16 v3, 0x33

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->LEGACY_TOOLBAR_HEADER:Llocation/unified/LocationProducer;

    .line 59
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "LEGACY_MOBILE_FRONTEND_GLL"

    const/16 v2, 0x34

    const/16 v3, 0x34

    const/16 v4, 0xa

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->LEGACY_MOBILE_FRONTEND_GLL:Llocation/unified/LocationProducer;

    .line 60
    new-instance v0, Llocation/unified/LocationProducer;

    const-string v1, "LEGACY_MOBILE_FRONTEND_NEAR"

    const/16 v2, 0x35

    const/16 v3, 0x35

    const/16 v4, 0x13

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProducer;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProducer;->LEGACY_MOBILE_FRONTEND_NEAR:Llocation/unified/LocationProducer;

    .line 5
    const/16 v0, 0x36

    new-array v0, v0, [Llocation/unified/LocationProducer;

    sget-object v1, Llocation/unified/LocationProducer;->UNKNOWN_PRODUCER:Llocation/unified/LocationProducer;

    aput-object v1, v0, v5

    sget-object v1, Llocation/unified/LocationProducer;->LOGGED_IN_USER_SPECIFIED:Llocation/unified/LocationProducer;

    aput-object v1, v0, v6

    sget-object v1, Llocation/unified/LocationProducer;->PREF_L_FIELD_ADDRESS:Llocation/unified/LocationProducer;

    aput-object v1, v0, v7

    sget-object v1, Llocation/unified/LocationProducer;->IP_ADDRESS:Llocation/unified/LocationProducer;

    aput-object v1, v0, v8

    sget-object v1, Llocation/unified/LocationProducer;->IP_ADDRESS_REALTIME:Llocation/unified/LocationProducer;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Llocation/unified/LocationProducer;->GOOGLE_HOST_DOMAIN:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Llocation/unified/LocationProducer;->RQUERY:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Llocation/unified/LocationProducer;->SQUERY:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Llocation/unified/LocationProducer;->QUERY_LOCATION_OVERRIDE_PRODUCER:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Llocation/unified/LocationProducer;->QREF:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Llocation/unified/LocationProducer;->DEVICE_LOCATION:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Llocation/unified/LocationProducer;->LEGACY_NEAR_PARAM:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Llocation/unified/LocationProducer;->CARRIER_COUNTRY:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Llocation/unified/LocationProducer;->JURISDICTION_COUNTRY:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Llocation/unified/LocationProducer;->SHOWTIME_ONEBOX:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Llocation/unified/LocationProducer;->LOCAL_UNIVERSAL:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Llocation/unified/LocationProducer;->SEARCH_TOOLBELT:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Llocation/unified/LocationProducer;->MOBILE_FE_HISTORY:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Llocation/unified/LocationProducer;->GWS_MOBILE_HISTORY_ZWIEBACK:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Llocation/unified/LocationProducer;->MOBILE_SELECTED:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Llocation/unified/LocationProducer;->PARTNER:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Llocation/unified/LocationProducer;->WEB_SEARCH_RESULTS_PAGE_SHARED:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Llocation/unified/LocationProducer;->WEB_SEARCH_PREFERENCES_PAGE:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Llocation/unified/LocationProducer;->MAPS_FRONTEND:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Llocation/unified/LocationProducer;->PRODUCT_SEARCH_FRONTEND:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Llocation/unified/LocationProducer;->ADS_CRITERIA_ID:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Llocation/unified/LocationProducer;->MOBILE_APP:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Llocation/unified/LocationProducer;->QUERY_HISTORY_INFERRED:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Llocation/unified/LocationProducer;->GMAIL_THEME:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Llocation/unified/LocationProducer;->IGOOGLE:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Llocation/unified/LocationProducer;->CALENDAR:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Llocation/unified/LocationProducer;->SMS_SEARCH:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Llocation/unified/LocationProducer;->LEGACY_GL_PARAM:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Llocation/unified/LocationProducer;->LEGACY_PARTNER_GL_PARAM:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Llocation/unified/LocationProducer;->LEGACY_GL_COOKIE:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Llocation/unified/LocationProducer;->CIRCULARS_FRONTEND:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Llocation/unified/LocationProducer;->SHOPPING_SEARCH_API:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Llocation/unified/LocationProducer;->OZ_FRONTEND:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Llocation/unified/LocationProducer;->ADS_GEO_PARAM:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Llocation/unified/LocationProducer;->ADS_PARTNER_GEO_PARAM:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Llocation/unified/LocationProducer;->DEFAULT_LOCATION_OVERRIDE_PRODUCER:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Llocation/unified/LocationProducer;->VIEWPORT_PARAMS:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Llocation/unified/LocationProducer;->GAIA_LOCATION_HISTORY:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Llocation/unified/LocationProducer;->STICKINESS_PARAMS:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Llocation/unified/LocationProducer;->TURN_BY_TURN_NAVIGATION_REROUTE:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Llocation/unified/LocationProducer;->SNAP_TO_PLACE_IMPLICIT:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Llocation/unified/LocationProducer;->SNAP_TO_PLACE_EXPLICIT:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Llocation/unified/LocationProducer;->HULK_USER_PLACES_CONFIRMED:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Llocation/unified/LocationProducer;->HULK_USER_PLACES_INFERRED:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Llocation/unified/LocationProducer;->TACTILE_NEARBY_PARAM:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Llocation/unified/LocationProducer;->WILDCARD_PRODUCER:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Llocation/unified/LocationProducer;->LEGACY_TOOLBAR_HEADER:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Llocation/unified/LocationProducer;->LEGACY_MOBILE_FRONTEND_GLL:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Llocation/unified/LocationProducer;->LEGACY_MOBILE_FRONTEND_NEAR:Llocation/unified/LocationProducer;

    aput-object v2, v0, v1

    sput-object v0, Llocation/unified/LocationProducer;->$VALUES:[Llocation/unified/LocationProducer;

    .line 131
    new-instance v0, Llocation/unified/LocationProducer$1;

    invoke-direct {v0}, Llocation/unified/LocationProducer$1;-><init>()V

    sput-object v0, Llocation/unified/LocationProducer;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 141
    iput p3, p0, Llocation/unified/LocationProducer;->index:I

    .line 142
    iput p4, p0, Llocation/unified/LocationProducer;->value:I

    .line 143
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Llocation/unified/LocationProducer;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Llocation/unified/LocationProducer;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Llocation/unified/LocationProducer;

    return-object v0
.end method

.method public static values()[Llocation/unified/LocationProducer;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Llocation/unified/LocationProducer;->$VALUES:[Llocation/unified/LocationProducer;

    invoke-virtual {v0}, [Llocation/unified/LocationProducer;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llocation/unified/LocationProducer;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Llocation/unified/LocationProducer;->value:I

    return v0
.end method
