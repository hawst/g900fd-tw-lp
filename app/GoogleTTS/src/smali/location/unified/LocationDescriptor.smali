.class public final Llocation/unified/LocationDescriptor;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LocationDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llocation/unified/LocationDescriptor$1;,
        Llocation/unified/LocationDescriptor$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Llocation/unified/LocationDescriptor;


# instance fields
.field private attributes_:Llocation/unified/LocationAttributesProto;

.field private confidence_:I

.field private diagnosticInfo_:Ljava/lang/String;

.field private featureId_:Llocation/unified/FeatureIdProto;

.field private hasAttributes:Z

.field private hasConfidence:Z

.field private hasDiagnosticInfo:Z

.field private hasFeatureId:Z

.field private hasHistoricalProducer:Z

.field private hasHistoricalProminence:Z

.field private hasHistoricalRole:Z

.field private hasLanguage:Z

.field private hasLatlng:Z

.field private hasLatlngSpan:Z

.field private hasLevelFeatureId:Z

.field private hasLevelNumber:Z

.field private hasLoc:Z

.field private hasMid:Z

.field private hasProducer:Z

.field private hasProvenance:Z

.field private hasRadius:Z

.field private hasRect:Z

.field private hasRole:Z

.field private hasTimestamp:Z

.field private historicalProducer_:Llocation/unified/LocationProducer;

.field private historicalProminence_:I

.field private historicalRole_:Llocation/unified/LocationRole;

.field private language_:Ljava/lang/String;

.field private latlngSpan_:Llocation/unified/LatLng;

.field private latlng_:Llocation/unified/LatLng;

.field private levelFeatureId_:Llocation/unified/FeatureIdProto;

.field private levelNumber_:F

.field private loc_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private mid_:J

.field private producer_:Llocation/unified/LocationProducer;

.field private provenance_:Llocation/unified/LocationProvenance;

.field private radius_:F

.field private rect_:Llocation/unified/LatLngRect;

.field private role_:Llocation/unified/LocationRole;

.field private semantic_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llocation/unified/LocationSemantic;",
            ">;"
        }
    .end annotation
.end field

.field private timestamp_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1284
    new-instance v0, Llocation/unified/LocationDescriptor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Llocation/unified/LocationDescriptor;-><init>(Z)V

    sput-object v0, Llocation/unified/LocationDescriptor;->defaultInstance:Llocation/unified/LocationDescriptor;

    .line 1285
    invoke-static {}, Llocation/unified/LocationDescriptorProto;->internalForceInit()V

    .line 1286
    sget-object v0, Llocation/unified/LocationDescriptor;->defaultInstance:Llocation/unified/LocationDescriptor;

    invoke-direct {v0}, Llocation/unified/LocationDescriptor;->initFields()V

    .line 1287
    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 39
    iput-wide v2, p0, Llocation/unified/LocationDescriptor;->timestamp_:J

    .line 46
    const-string v0, ""

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->loc_:Ljava/lang/String;

    .line 74
    iput v1, p0, Llocation/unified/LocationDescriptor;->radius_:F

    .line 81
    const/16 v0, 0x64

    iput v0, p0, Llocation/unified/LocationDescriptor;->confidence_:I

    .line 95
    iput-wide v2, p0, Llocation/unified/LocationDescriptor;->mid_:J

    .line 109
    iput v1, p0, Llocation/unified/LocationDescriptor;->levelNumber_:F

    .line 116
    const-string v0, ""

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->language_:Ljava/lang/String;

    .line 144
    const/4 v0, 0x0

    iput v0, p0, Llocation/unified/LocationDescriptor;->historicalProminence_:I

    .line 158
    const-string v0, ""

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->diagnosticInfo_:Ljava/lang/String;

    .line 164
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;

    .line 259
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationDescriptor;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Llocation/unified/LocationDescriptor;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Llocation/unified/LocationDescriptor$1;)V
    .locals 0
    .param p1, "x0"    # Llocation/unified/LocationDescriptor$1;

    .prologue
    .line 5
    invoke-direct {p0}, Llocation/unified/LocationDescriptor;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 4
    .param p1, "noInit"    # Z

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 39
    iput-wide v2, p0, Llocation/unified/LocationDescriptor;->timestamp_:J

    .line 46
    const-string v0, ""

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->loc_:Ljava/lang/String;

    .line 74
    iput v1, p0, Llocation/unified/LocationDescriptor;->radius_:F

    .line 81
    const/16 v0, 0x64

    iput v0, p0, Llocation/unified/LocationDescriptor;->confidence_:I

    .line 95
    iput-wide v2, p0, Llocation/unified/LocationDescriptor;->mid_:J

    .line 109
    iput v1, p0, Llocation/unified/LocationDescriptor;->levelNumber_:F

    .line 116
    const-string v0, ""

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->language_:Ljava/lang/String;

    .line 144
    const/4 v0, 0x0

    iput v0, p0, Llocation/unified/LocationDescriptor;->historicalProminence_:I

    .line 158
    const-string v0, ""

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->diagnosticInfo_:Ljava/lang/String;

    .line 164
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;

    .line 259
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationDescriptor;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasLoc:Z

    return p1
.end method

.method static synthetic access$1102(Llocation/unified/LocationDescriptor;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->loc_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasLatlng:Z

    return p1
.end method

.method static synthetic access$1300(Llocation/unified/LocationDescriptor;)Llocation/unified/LatLng;
    .locals 1
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;

    .prologue
    .line 5
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->latlng_:Llocation/unified/LatLng;

    return-object v0
.end method

.method static synthetic access$1302(Llocation/unified/LocationDescriptor;Llocation/unified/LatLng;)Llocation/unified/LatLng;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Llocation/unified/LatLng;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->latlng_:Llocation/unified/LatLng;

    return-object p1
.end method

.method static synthetic access$1402(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasLatlngSpan:Z

    return p1
.end method

.method static synthetic access$1500(Llocation/unified/LocationDescriptor;)Llocation/unified/LatLng;
    .locals 1
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;

    .prologue
    .line 5
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->latlngSpan_:Llocation/unified/LatLng;

    return-object v0
.end method

.method static synthetic access$1502(Llocation/unified/LocationDescriptor;Llocation/unified/LatLng;)Llocation/unified/LatLng;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Llocation/unified/LatLng;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->latlngSpan_:Llocation/unified/LatLng;

    return-object p1
.end method

.method static synthetic access$1602(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasRect:Z

    return p1
.end method

.method static synthetic access$1700(Llocation/unified/LocationDescriptor;)Llocation/unified/LatLngRect;
    .locals 1
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;

    .prologue
    .line 5
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->rect_:Llocation/unified/LatLngRect;

    return-object v0
.end method

.method static synthetic access$1702(Llocation/unified/LocationDescriptor;Llocation/unified/LatLngRect;)Llocation/unified/LatLngRect;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Llocation/unified/LatLngRect;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->rect_:Llocation/unified/LatLngRect;

    return-object p1
.end method

.method static synthetic access$1802(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasRadius:Z

    return p1
.end method

.method static synthetic access$1902(Llocation/unified/LocationDescriptor;F)F
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/LocationDescriptor;->radius_:F

    return p1
.end method

.method static synthetic access$2002(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasConfidence:Z

    return p1
.end method

.method static synthetic access$2102(Llocation/unified/LocationDescriptor;I)I
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/LocationDescriptor;->confidence_:I

    return p1
.end method

.method static synthetic access$2202(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasFeatureId:Z

    return p1
.end method

.method static synthetic access$2300(Llocation/unified/LocationDescriptor;)Llocation/unified/FeatureIdProto;
    .locals 1
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;

    .prologue
    .line 5
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->featureId_:Llocation/unified/FeatureIdProto;

    return-object v0
.end method

.method static synthetic access$2302(Llocation/unified/LocationDescriptor;Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Llocation/unified/FeatureIdProto;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->featureId_:Llocation/unified/FeatureIdProto;

    return-object p1
.end method

.method static synthetic access$2402(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasMid:Z

    return p1
.end method

.method static synthetic access$2502(Llocation/unified/LocationDescriptor;J)J
    .locals 1
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # J

    .prologue
    .line 5
    iput-wide p1, p0, Llocation/unified/LocationDescriptor;->mid_:J

    return-wide p1
.end method

.method static synthetic access$2602(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasLevelFeatureId:Z

    return p1
.end method

.method static synthetic access$2700(Llocation/unified/LocationDescriptor;)Llocation/unified/FeatureIdProto;
    .locals 1
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;

    .prologue
    .line 5
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->levelFeatureId_:Llocation/unified/FeatureIdProto;

    return-object v0
.end method

.method static synthetic access$2702(Llocation/unified/LocationDescriptor;Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Llocation/unified/FeatureIdProto;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->levelFeatureId_:Llocation/unified/FeatureIdProto;

    return-object p1
.end method

.method static synthetic access$2802(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasLevelNumber:Z

    return p1
.end method

.method static synthetic access$2902(Llocation/unified/LocationDescriptor;F)F
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/LocationDescriptor;->levelNumber_:F

    return p1
.end method

.method static synthetic access$300(Llocation/unified/LocationDescriptor;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;

    .prologue
    .line 5
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3002(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasLanguage:Z

    return p1
.end method

.method static synthetic access$302(Llocation/unified/LocationDescriptor;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3102(Llocation/unified/LocationDescriptor;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->language_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3202(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasProvenance:Z

    return p1
.end method

.method static synthetic access$3302(Llocation/unified/LocationDescriptor;Llocation/unified/LocationProvenance;)Llocation/unified/LocationProvenance;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Llocation/unified/LocationProvenance;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->provenance_:Llocation/unified/LocationProvenance;

    return-object p1
.end method

.method static synthetic access$3402(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasHistoricalRole:Z

    return p1
.end method

.method static synthetic access$3502(Llocation/unified/LocationDescriptor;Llocation/unified/LocationRole;)Llocation/unified/LocationRole;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Llocation/unified/LocationRole;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->historicalRole_:Llocation/unified/LocationRole;

    return-object p1
.end method

.method static synthetic access$3602(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasHistoricalProducer:Z

    return p1
.end method

.method static synthetic access$3702(Llocation/unified/LocationDescriptor;Llocation/unified/LocationProducer;)Llocation/unified/LocationProducer;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Llocation/unified/LocationProducer;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->historicalProducer_:Llocation/unified/LocationProducer;

    return-object p1
.end method

.method static synthetic access$3802(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasHistoricalProminence:Z

    return p1
.end method

.method static synthetic access$3902(Llocation/unified/LocationDescriptor;I)I
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Llocation/unified/LocationDescriptor;->historicalProminence_:I

    return p1
.end method

.method static synthetic access$4002(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasAttributes:Z

    return p1
.end method

.method static synthetic access$402(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasRole:Z

    return p1
.end method

.method static synthetic access$4100(Llocation/unified/LocationDescriptor;)Llocation/unified/LocationAttributesProto;
    .locals 1
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;

    .prologue
    .line 5
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->attributes_:Llocation/unified/LocationAttributesProto;

    return-object v0
.end method

.method static synthetic access$4102(Llocation/unified/LocationDescriptor;Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationAttributesProto;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Llocation/unified/LocationAttributesProto;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->attributes_:Llocation/unified/LocationAttributesProto;

    return-object p1
.end method

.method static synthetic access$4202(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasDiagnosticInfo:Z

    return p1
.end method

.method static synthetic access$4302(Llocation/unified/LocationDescriptor;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->diagnosticInfo_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Llocation/unified/LocationDescriptor;Llocation/unified/LocationRole;)Llocation/unified/LocationRole;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Llocation/unified/LocationRole;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->role_:Llocation/unified/LocationRole;

    return-object p1
.end method

.method static synthetic access$602(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasProducer:Z

    return p1
.end method

.method static synthetic access$702(Llocation/unified/LocationDescriptor;Llocation/unified/LocationProducer;)Llocation/unified/LocationProducer;
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Llocation/unified/LocationProducer;

    .prologue
    .line 5
    iput-object p1, p0, Llocation/unified/LocationDescriptor;->producer_:Llocation/unified/LocationProducer;

    return-object p1
.end method

.method static synthetic access$802(Llocation/unified/LocationDescriptor;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/LocationDescriptor;->hasTimestamp:Z

    return p1
.end method

.method static synthetic access$902(Llocation/unified/LocationDescriptor;J)J
    .locals 1
    .param p0, "x0"    # Llocation/unified/LocationDescriptor;
    .param p1, "x1"    # J

    .prologue
    .line 5
    iput-wide p1, p0, Llocation/unified/LocationDescriptor;->timestamp_:J

    return-wide p1
.end method

.method public static getDefaultInstance()Llocation/unified/LocationDescriptor;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Llocation/unified/LocationDescriptor;->defaultInstance:Llocation/unified/LocationDescriptor;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 175
    sget-object v0, Llocation/unified/LocationRole;->UNKNOWN_ROLE:Llocation/unified/LocationRole;

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->role_:Llocation/unified/LocationRole;

    .line 176
    sget-object v0, Llocation/unified/LocationProducer;->UNKNOWN_PRODUCER:Llocation/unified/LocationProducer;

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->producer_:Llocation/unified/LocationProducer;

    .line 177
    invoke-static {}, Llocation/unified/LatLng;->getDefaultInstance()Llocation/unified/LatLng;

    move-result-object v0

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->latlng_:Llocation/unified/LatLng;

    .line 178
    invoke-static {}, Llocation/unified/LatLng;->getDefaultInstance()Llocation/unified/LatLng;

    move-result-object v0

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->latlngSpan_:Llocation/unified/LatLng;

    .line 179
    invoke-static {}, Llocation/unified/LatLngRect;->getDefaultInstance()Llocation/unified/LatLngRect;

    move-result-object v0

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->rect_:Llocation/unified/LatLngRect;

    .line 180
    invoke-static {}, Llocation/unified/FeatureIdProto;->getDefaultInstance()Llocation/unified/FeatureIdProto;

    move-result-object v0

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->featureId_:Llocation/unified/FeatureIdProto;

    .line 181
    invoke-static {}, Llocation/unified/FeatureIdProto;->getDefaultInstance()Llocation/unified/FeatureIdProto;

    move-result-object v0

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->levelFeatureId_:Llocation/unified/FeatureIdProto;

    .line 182
    sget-object v0, Llocation/unified/LocationProvenance;->UNREMARKABLE:Llocation/unified/LocationProvenance;

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->provenance_:Llocation/unified/LocationProvenance;

    .line 183
    sget-object v0, Llocation/unified/LocationRole;->UNKNOWN_ROLE:Llocation/unified/LocationRole;

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->historicalRole_:Llocation/unified/LocationRole;

    .line 184
    sget-object v0, Llocation/unified/LocationProducer;->UNKNOWN_PRODUCER:Llocation/unified/LocationProducer;

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->historicalProducer_:Llocation/unified/LocationProducer;

    .line 185
    invoke-static {}, Llocation/unified/LocationAttributesProto;->getDefaultInstance()Llocation/unified/LocationAttributesProto;

    move-result-object v0

    iput-object v0, p0, Llocation/unified/LocationDescriptor;->attributes_:Llocation/unified/LocationAttributesProto;

    .line 186
    return-void
.end method

.method public static newBuilder()Llocation/unified/LocationDescriptor$Builder;
    .locals 1

    .prologue
    .line 425
    # invokes: Llocation/unified/LocationDescriptor$Builder;->create()Llocation/unified/LocationDescriptor$Builder;
    invoke-static {}, Llocation/unified/LocationDescriptor$Builder;->access$100()Llocation/unified/LocationDescriptor$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Llocation/unified/LocationDescriptor;)Llocation/unified/LocationDescriptor$Builder;
    .locals 1
    .param p0, "prototype"    # Llocation/unified/LocationDescriptor;

    .prologue
    .line 428
    invoke-static {}, Llocation/unified/LocationDescriptor;->newBuilder()Llocation/unified/LocationDescriptor$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Llocation/unified/LocationDescriptor$Builder;->mergeFrom(Llocation/unified/LocationDescriptor;)Llocation/unified/LocationDescriptor$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAttributes()Llocation/unified/LocationAttributesProto;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->attributes_:Llocation/unified/LocationAttributesProto;

    return-object v0
.end method

.method public getConfidence()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Llocation/unified/LocationDescriptor;->confidence_:I

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getDefaultInstanceForType()Llocation/unified/LocationDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Llocation/unified/LocationDescriptor;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Llocation/unified/LocationDescriptor;->defaultInstance:Llocation/unified/LocationDescriptor;

    return-object v0
.end method

.method public getDiagnosticInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->diagnosticInfo_:Ljava/lang/String;

    return-object v0
.end method

.method public getFeatureId()Llocation/unified/FeatureIdProto;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->featureId_:Llocation/unified/FeatureIdProto;

    return-object v0
.end method

.method public getHistoricalProducer()Llocation/unified/LocationProducer;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->historicalProducer_:Llocation/unified/LocationProducer;

    return-object v0
.end method

.method public getHistoricalProminence()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Llocation/unified/LocationDescriptor;->historicalProminence_:I

    return v0
.end method

.method public getHistoricalRole()Llocation/unified/LocationRole;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->historicalRole_:Llocation/unified/LocationRole;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->language_:Ljava/lang/String;

    return-object v0
.end method

.method public getLatlng()Llocation/unified/LatLng;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->latlng_:Llocation/unified/LatLng;

    return-object v0
.end method

.method public getLatlngSpan()Llocation/unified/LatLng;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->latlngSpan_:Llocation/unified/LatLng;

    return-object v0
.end method

.method public getLevelFeatureId()Llocation/unified/FeatureIdProto;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->levelFeatureId_:Llocation/unified/FeatureIdProto;

    return-object v0
.end method

.method public getLevelNumber()F
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Llocation/unified/LocationDescriptor;->levelNumber_:F

    return v0
.end method

.method public getLoc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->loc_:Ljava/lang/String;

    return-object v0
.end method

.method public getMid()J
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Llocation/unified/LocationDescriptor;->mid_:J

    return-wide v0
.end method

.method public getProducer()Llocation/unified/LocationProducer;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->producer_:Llocation/unified/LocationProducer;

    return-object v0
.end method

.method public getProvenance()Llocation/unified/LocationProvenance;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->provenance_:Llocation/unified/LocationProvenance;

    return-object v0
.end method

.method public getRadius()F
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Llocation/unified/LocationDescriptor;->radius_:F

    return v0
.end method

.method public getRect()Llocation/unified/LatLngRect;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->rect_:Llocation/unified/LatLngRect;

    return-object v0
.end method

.method public getRole()Llocation/unified/LocationRole;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->role_:Llocation/unified/LocationRole;

    return-object v0
.end method

.method public getSemanticList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Llocation/unified/LocationSemantic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Llocation/unified/LocationDescriptor;->semantic_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    .line 261
    iget v3, p0, Llocation/unified/LocationDescriptor;->memoizedSerializedSize:I

    .line 262
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 355
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 264
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 265
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasRole()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 266
    const/4 v5, 0x1

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getRole()Llocation/unified/LocationRole;

    move-result-object v6

    invoke-virtual {v6}, Llocation/unified/LocationRole;->getNumber()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 269
    :cond_1
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasProducer()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 270
    const/4 v5, 0x2

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getProducer()Llocation/unified/LocationProducer;

    move-result-object v6

    invoke-virtual {v6}, Llocation/unified/LocationProducer;->getNumber()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 273
    :cond_2
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasTimestamp()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 274
    const/4 v5, 0x3

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getTimestamp()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v3, v5

    .line 277
    :cond_3
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasLoc()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 278
    const/4 v5, 0x4

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getLoc()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 281
    :cond_4
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasLatlng()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 282
    const/4 v5, 0x5

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getLatlng()Llocation/unified/LatLng;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 285
    :cond_5
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasLatlngSpan()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 286
    const/4 v5, 0x6

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getLatlngSpan()Llocation/unified/LatLng;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 289
    :cond_6
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasRadius()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 290
    const/4 v5, 0x7

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getRadius()F

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v5

    add-int/2addr v3, v5

    .line 293
    :cond_7
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasConfidence()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 294
    const/16 v5, 0x8

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getConfidence()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 297
    :cond_8
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasProvenance()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 298
    const/16 v5, 0x9

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getProvenance()Llocation/unified/LocationProvenance;

    move-result-object v6

    invoke-virtual {v6}, Llocation/unified/LocationProvenance;->getNumber()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 301
    :cond_9
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasFeatureId()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 302
    const/16 v5, 0xa

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getFeatureId()Llocation/unified/FeatureIdProto;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 305
    :cond_a
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasLanguage()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 306
    const/16 v5, 0xb

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 309
    :cond_b
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasHistoricalRole()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 310
    const/16 v5, 0xc

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getHistoricalRole()Llocation/unified/LocationRole;

    move-result-object v6

    invoke-virtual {v6}, Llocation/unified/LocationRole;->getNumber()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 313
    :cond_c
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasHistoricalProducer()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 314
    const/16 v5, 0xd

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getHistoricalProducer()Llocation/unified/LocationProducer;

    move-result-object v6

    invoke-virtual {v6}, Llocation/unified/LocationProducer;->getNumber()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 317
    :cond_d
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasRect()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 318
    const/16 v5, 0xe

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getRect()Llocation/unified/LatLngRect;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 321
    :cond_e
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasHistoricalProminence()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 322
    const/16 v5, 0xf

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getHistoricalProminence()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 325
    :cond_f
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasMid()Z

    move-result v5

    if-eqz v5, :cond_10

    .line 326
    const/16 v5, 0x10

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getMid()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v5

    add-int/2addr v3, v5

    .line 329
    :cond_10
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasLevelFeatureId()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 330
    const/16 v5, 0x11

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getLevelFeatureId()Llocation/unified/FeatureIdProto;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 333
    :cond_11
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasLevelNumber()Z

    move-result v5

    if-eqz v5, :cond_12

    .line 334
    const/16 v5, 0x12

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getLevelNumber()F

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v5

    add-int/2addr v3, v5

    .line 337
    :cond_12
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasAttributes()Z

    move-result v5

    if-eqz v5, :cond_13

    .line 338
    const/16 v5, 0x13

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getAttributes()Llocation/unified/LocationAttributesProto;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 341
    :cond_13
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasDiagnosticInfo()Z

    move-result v5

    if-eqz v5, :cond_14

    .line 342
    const/16 v5, 0x14

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getDiagnosticInfo()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 346
    :cond_14
    const/4 v0, 0x0

    .line 347
    .local v0, "dataSize":I
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getSemanticList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_15

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llocation/unified/LocationSemantic;

    .line 348
    .local v1, "element":Llocation/unified/LocationSemantic;
    invoke-virtual {v1}, Llocation/unified/LocationSemantic;->getNumber()I

    move-result v5

    invoke-static {v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSizeNoTag(I)I

    move-result v5

    add-int/2addr v0, v5

    .line 350
    goto :goto_1

    .line 351
    .end local v1    # "element":Llocation/unified/LocationSemantic;
    :cond_15
    add-int/2addr v3, v0

    .line 352
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getSemanticList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    .line 354
    iput v3, p0, Llocation/unified/LocationDescriptor;->memoizedSerializedSize:I

    move v4, v3

    .line 355
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Llocation/unified/LocationDescriptor;->timestamp_:J

    return-wide v0
.end method

.method public hasAttributes()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasAttributes:Z

    return v0
.end method

.method public hasConfidence()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasConfidence:Z

    return v0
.end method

.method public hasDiagnosticInfo()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasDiagnosticInfo:Z

    return v0
.end method

.method public hasFeatureId()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasFeatureId:Z

    return v0
.end method

.method public hasHistoricalProducer()Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasHistoricalProducer:Z

    return v0
.end method

.method public hasHistoricalProminence()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasHistoricalProminence:Z

    return v0
.end method

.method public hasHistoricalRole()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasHistoricalRole:Z

    return v0
.end method

.method public hasLanguage()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasLanguage:Z

    return v0
.end method

.method public hasLatlng()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasLatlng:Z

    return v0
.end method

.method public hasLatlngSpan()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasLatlngSpan:Z

    return v0
.end method

.method public hasLevelFeatureId()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasLevelFeatureId:Z

    return v0
.end method

.method public hasLevelNumber()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasLevelNumber:Z

    return v0
.end method

.method public hasLoc()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasLoc:Z

    return v0
.end method

.method public hasMid()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasMid:Z

    return v0
.end method

.method public hasProducer()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasProducer:Z

    return v0
.end method

.method public hasProvenance()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasProvenance:Z

    return v0
.end method

.method public hasRadius()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasRadius:Z

    return v0
.end method

.method public hasRect()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasRect:Z

    return v0
.end method

.method public hasRole()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasRole:Z

    return v0
.end method

.method public hasTimestamp()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Llocation/unified/LocationDescriptor;->hasTimestamp:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->toBuilder()Llocation/unified/LocationDescriptor$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Llocation/unified/LocationDescriptor$Builder;
    .locals 1

    .prologue
    .line 430
    invoke-static {p0}, Llocation/unified/LocationDescriptor;->newBuilder(Llocation/unified/LocationDescriptor;)Llocation/unified/LocationDescriptor$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getSerializedSize()I

    .line 194
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasRole()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 195
    const/4 v2, 0x1

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getRole()Llocation/unified/LocationRole;

    move-result-object v3

    invoke-virtual {v3}, Llocation/unified/LocationRole;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 197
    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasProducer()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 198
    const/4 v2, 0x2

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getProducer()Llocation/unified/LocationProducer;

    move-result-object v3

    invoke-virtual {v3}, Llocation/unified/LocationProducer;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 200
    :cond_1
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasTimestamp()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 201
    const/4 v2, 0x3

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getTimestamp()J

    move-result-wide v4

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 203
    :cond_2
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasLoc()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 204
    const/4 v2, 0x4

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getLoc()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 206
    :cond_3
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasLatlng()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 207
    const/4 v2, 0x5

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getLatlng()Llocation/unified/LatLng;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 209
    :cond_4
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasLatlngSpan()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 210
    const/4 v2, 0x6

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getLatlngSpan()Llocation/unified/LatLng;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 212
    :cond_5
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasRadius()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 213
    const/4 v2, 0x7

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getRadius()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 215
    :cond_6
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasConfidence()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 216
    const/16 v2, 0x8

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getConfidence()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 218
    :cond_7
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasProvenance()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 219
    const/16 v2, 0x9

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getProvenance()Llocation/unified/LocationProvenance;

    move-result-object v3

    invoke-virtual {v3}, Llocation/unified/LocationProvenance;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 221
    :cond_8
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasFeatureId()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 222
    const/16 v2, 0xa

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getFeatureId()Llocation/unified/FeatureIdProto;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 224
    :cond_9
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasLanguage()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 225
    const/16 v2, 0xb

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 227
    :cond_a
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasHistoricalRole()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 228
    const/16 v2, 0xc

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getHistoricalRole()Llocation/unified/LocationRole;

    move-result-object v3

    invoke-virtual {v3}, Llocation/unified/LocationRole;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 230
    :cond_b
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasHistoricalProducer()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 231
    const/16 v2, 0xd

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getHistoricalProducer()Llocation/unified/LocationProducer;

    move-result-object v3

    invoke-virtual {v3}, Llocation/unified/LocationProducer;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 233
    :cond_c
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasRect()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 234
    const/16 v2, 0xe

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getRect()Llocation/unified/LatLngRect;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 236
    :cond_d
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasHistoricalProminence()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 237
    const/16 v2, 0xf

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getHistoricalProminence()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 239
    :cond_e
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasMid()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 240
    const/16 v2, 0x10

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getMid()J

    move-result-wide v4

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    .line 242
    :cond_f
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasLevelFeatureId()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 243
    const/16 v2, 0x11

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getLevelFeatureId()Llocation/unified/FeatureIdProto;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 245
    :cond_10
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasLevelNumber()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 246
    const/16 v2, 0x12

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getLevelNumber()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 248
    :cond_11
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasAttributes()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 249
    const/16 v2, 0x13

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getAttributes()Llocation/unified/LocationAttributesProto;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 251
    :cond_12
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->hasDiagnosticInfo()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 252
    const/16 v2, 0x14

    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getDiagnosticInfo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 254
    :cond_13
    invoke-virtual {p0}, Llocation/unified/LocationDescriptor;->getSemanticList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llocation/unified/LocationSemantic;

    .line 255
    .local v0, "element":Llocation/unified/LocationSemantic;
    const/16 v2, 0x15

    invoke-virtual {v0}, Llocation/unified/LocationSemantic;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    goto :goto_0

    .line 257
    .end local v0    # "element":Llocation/unified/LocationSemantic;
    :cond_14
    return-void
.end method
