.class public final Llocation/unified/LocationAttributesProto$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LocationAttributesProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationAttributesProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Llocation/unified/LocationAttributesProto;",
        "Llocation/unified/LocationAttributesProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Llocation/unified/LocationAttributesProto;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 296
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Llocation/unified/LocationAttributesProto$Builder;
    .locals 1

    .prologue
    .line 290
    invoke-static {}, Llocation/unified/LocationAttributesProto$Builder;->create()Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Llocation/unified/LocationAttributesProto$Builder;
    .locals 3

    .prologue
    .line 299
    new-instance v0, Llocation/unified/LocationAttributesProto$Builder;

    invoke-direct {v0}, Llocation/unified/LocationAttributesProto$Builder;-><init>()V

    .line 300
    .local v0, "builder":Llocation/unified/LocationAttributesProto$Builder;
    new-instance v1, Llocation/unified/LocationAttributesProto;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Llocation/unified/LocationAttributesProto;-><init>(Llocation/unified/LocationAttributesProto$1;)V

    iput-object v1, v0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    .line 301
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto$Builder;->build()Llocation/unified/LocationAttributesProto;

    move-result-object v0

    return-object v0
.end method

.method public build()Llocation/unified/LocationAttributesProto;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 330
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    invoke-static {v0}, Llocation/unified/LocationAttributesProto$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 332
    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto$Builder;->buildPartial()Llocation/unified/LocationAttributesProto;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Llocation/unified/LocationAttributesProto;
    .locals 3

    .prologue
    .line 345
    iget-object v1, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    if-nez v1, :cond_0

    .line 346
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 349
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    .line 350
    .local v0, "returnMe":Llocation/unified/LocationAttributesProto;
    const/4 v1, 0x0

    iput-object v1, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    .line 351
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto$Builder;->clone()Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto$Builder;->clone()Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 290
    invoke-virtual {p0}, Llocation/unified/LocationAttributesProto$Builder;->clone()Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Llocation/unified/LocationAttributesProto$Builder;
    .locals 2

    .prologue
    .line 318
    invoke-static {}, Llocation/unified/LocationAttributesProto$Builder;->create()Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v0

    iget-object v1, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    invoke-virtual {v0, v1}, Llocation/unified/LocationAttributesProto$Builder;->mergeFrom(Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    invoke-virtual {v0}, Llocation/unified/LocationAttributesProto;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFieldOfView(Llocation/unified/FieldOfView;)Llocation/unified/LocationAttributesProto$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/FieldOfView;

    .prologue
    .line 601
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    invoke-virtual {v0}, Llocation/unified/LocationAttributesProto;->hasFieldOfView()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    # getter for: Llocation/unified/LocationAttributesProto;->fieldOfView_:Llocation/unified/FieldOfView;
    invoke-static {v0}, Llocation/unified/LocationAttributesProto;->access$1800(Llocation/unified/LocationAttributesProto;)Llocation/unified/FieldOfView;

    move-result-object v0

    invoke-static {}, Llocation/unified/FieldOfView;->getDefaultInstance()Llocation/unified/FieldOfView;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 603
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    iget-object v1, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    # getter for: Llocation/unified/LocationAttributesProto;->fieldOfView_:Llocation/unified/FieldOfView;
    invoke-static {v1}, Llocation/unified/LocationAttributesProto;->access$1800(Llocation/unified/LocationAttributesProto;)Llocation/unified/FieldOfView;

    move-result-object v1

    invoke-static {v1}, Llocation/unified/FieldOfView;->newBuilder(Llocation/unified/FieldOfView;)Llocation/unified/FieldOfView$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Llocation/unified/FieldOfView$Builder;->mergeFrom(Llocation/unified/FieldOfView;)Llocation/unified/FieldOfView$Builder;

    move-result-object v1

    invoke-virtual {v1}, Llocation/unified/FieldOfView$Builder;->buildPartial()Llocation/unified/FieldOfView;

    move-result-object v1

    # setter for: Llocation/unified/LocationAttributesProto;->fieldOfView_:Llocation/unified/FieldOfView;
    invoke-static {v0, v1}, Llocation/unified/LocationAttributesProto;->access$1802(Llocation/unified/LocationAttributesProto;Llocation/unified/FieldOfView;)Llocation/unified/FieldOfView;

    .line 608
    :goto_0
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationAttributesProto;->hasFieldOfView:Z
    invoke-static {v0, v1}, Llocation/unified/LocationAttributesProto;->access$1702(Llocation/unified/LocationAttributesProto;Z)Z

    .line 609
    return-object p0

    .line 606
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    # setter for: Llocation/unified/LocationAttributesProto;->fieldOfView_:Llocation/unified/FieldOfView;
    invoke-static {v0, p1}, Llocation/unified/LocationAttributesProto;->access$1802(Llocation/unified/LocationAttributesProto;Llocation/unified/FieldOfView;)Llocation/unified/FieldOfView;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 290
    check-cast p1, Llocation/unified/LocationAttributesProto;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Llocation/unified/LocationAttributesProto$Builder;->mergeFrom(Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationAttributesProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Llocation/unified/LocationAttributesProto;)Llocation/unified/LocationAttributesProto$Builder;
    .locals 2
    .param p1, "other"    # Llocation/unified/LocationAttributesProto;

    .prologue
    .line 355
    invoke-static {}, Llocation/unified/LocationAttributesProto;->getDefaultInstance()Llocation/unified/LocationAttributesProto;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 383
    :cond_0
    :goto_0
    return-object p0

    .line 356
    :cond_1
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->hasDetectedActivity()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 357
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->getDetectedActivity()Llocation/unified/LocationAttributesProto$ActivityType;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationAttributesProto$Builder;->setDetectedActivity(Llocation/unified/LocationAttributesProto$ActivityType;)Llocation/unified/LocationAttributesProto$Builder;

    .line 359
    :cond_2
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->hasHeadingDegrees()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 360
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->getHeadingDegrees()I

    move-result v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationAttributesProto$Builder;->setHeadingDegrees(I)Llocation/unified/LocationAttributesProto$Builder;

    .line 362
    :cond_3
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->hasBearingDegrees()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 363
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->getBearingDegrees()I

    move-result v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationAttributesProto$Builder;->setBearingDegrees(I)Llocation/unified/LocationAttributesProto$Builder;

    .line 365
    :cond_4
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->hasSpeedKph()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 366
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->getSpeedKph()I

    move-result v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationAttributesProto$Builder;->setSpeedKph(I)Llocation/unified/LocationAttributesProto$Builder;

    .line 368
    :cond_5
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->hasTiltDegrees()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 369
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->getTiltDegrees()I

    move-result v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationAttributesProto$Builder;->setTiltDegrees(I)Llocation/unified/LocationAttributesProto$Builder;

    .line 371
    :cond_6
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->hasRollDegrees()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 372
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->getRollDegrees()I

    move-result v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationAttributesProto$Builder;->setRollDegrees(I)Llocation/unified/LocationAttributesProto$Builder;

    .line 374
    :cond_7
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->hasAltitudeMetersFromGround()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 375
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->getAltitudeMetersFromGround()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Llocation/unified/LocationAttributesProto$Builder;->setAltitudeMetersFromGround(D)Llocation/unified/LocationAttributesProto$Builder;

    .line 377
    :cond_8
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->hasFieldOfView()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 378
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->getFieldOfView()Llocation/unified/FieldOfView;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationAttributesProto$Builder;->mergeFieldOfView(Llocation/unified/FieldOfView;)Llocation/unified/LocationAttributesProto$Builder;

    .line 380
    :cond_9
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->hasBoardedTransitVehicleToken()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    invoke-virtual {p1}, Llocation/unified/LocationAttributesProto;->getBoardedTransitVehicleToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Llocation/unified/LocationAttributesProto$Builder;->setBoardedTransitVehicleToken(Ljava/lang/String;)Llocation/unified/LocationAttributesProto$Builder;

    goto :goto_0
.end method

.method public setAltitudeMetersFromGround(D)Llocation/unified/LocationAttributesProto$Builder;
    .locals 3
    .param p1, "value"    # D

    .prologue
    .line 570
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationAttributesProto;->hasAltitudeMetersFromGround:Z
    invoke-static {v0, v1}, Llocation/unified/LocationAttributesProto;->access$1502(Llocation/unified/LocationAttributesProto;Z)Z

    .line 571
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    # setter for: Llocation/unified/LocationAttributesProto;->altitudeMetersFromGround_:D
    invoke-static {v0, p1, p2}, Llocation/unified/LocationAttributesProto;->access$1602(Llocation/unified/LocationAttributesProto;D)D

    .line 572
    return-object p0
.end method

.method public setBearingDegrees(I)Llocation/unified/LocationAttributesProto$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 498
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationAttributesProto;->hasBearingDegrees:Z
    invoke-static {v0, v1}, Llocation/unified/LocationAttributesProto;->access$702(Llocation/unified/LocationAttributesProto;Z)Z

    .line 499
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    # setter for: Llocation/unified/LocationAttributesProto;->bearingDegrees_:I
    invoke-static {v0, p1}, Llocation/unified/LocationAttributesProto;->access$802(Llocation/unified/LocationAttributesProto;I)I

    .line 500
    return-object p0
.end method

.method public setBoardedTransitVehicleToken(Ljava/lang/String;)Llocation/unified/LocationAttributesProto$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 625
    if-nez p1, :cond_0

    .line 626
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 628
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationAttributesProto;->hasBoardedTransitVehicleToken:Z
    invoke-static {v0, v1}, Llocation/unified/LocationAttributesProto;->access$1902(Llocation/unified/LocationAttributesProto;Z)Z

    .line 629
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    # setter for: Llocation/unified/LocationAttributesProto;->boardedTransitVehicleToken_:Ljava/lang/String;
    invoke-static {v0, p1}, Llocation/unified/LocationAttributesProto;->access$2002(Llocation/unified/LocationAttributesProto;Ljava/lang/String;)Ljava/lang/String;

    .line 630
    return-object p0
.end method

.method public setDetectedActivity(Llocation/unified/LocationAttributesProto$ActivityType;)Llocation/unified/LocationAttributesProto$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LocationAttributesProto$ActivityType;

    .prologue
    .line 459
    if-nez p1, :cond_0

    .line 460
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 462
    :cond_0
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationAttributesProto;->hasDetectedActivity:Z
    invoke-static {v0, v1}, Llocation/unified/LocationAttributesProto;->access$302(Llocation/unified/LocationAttributesProto;Z)Z

    .line 463
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    # setter for: Llocation/unified/LocationAttributesProto;->detectedActivity_:Llocation/unified/LocationAttributesProto$ActivityType;
    invoke-static {v0, p1}, Llocation/unified/LocationAttributesProto;->access$402(Llocation/unified/LocationAttributesProto;Llocation/unified/LocationAttributesProto$ActivityType;)Llocation/unified/LocationAttributesProto$ActivityType;

    .line 464
    return-object p0
.end method

.method public setHeadingDegrees(I)Llocation/unified/LocationAttributesProto$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 480
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationAttributesProto;->hasHeadingDegrees:Z
    invoke-static {v0, v1}, Llocation/unified/LocationAttributesProto;->access$502(Llocation/unified/LocationAttributesProto;Z)Z

    .line 481
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    # setter for: Llocation/unified/LocationAttributesProto;->headingDegrees_:I
    invoke-static {v0, p1}, Llocation/unified/LocationAttributesProto;->access$602(Llocation/unified/LocationAttributesProto;I)I

    .line 482
    return-object p0
.end method

.method public setRollDegrees(I)Llocation/unified/LocationAttributesProto$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 552
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationAttributesProto;->hasRollDegrees:Z
    invoke-static {v0, v1}, Llocation/unified/LocationAttributesProto;->access$1302(Llocation/unified/LocationAttributesProto;Z)Z

    .line 553
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    # setter for: Llocation/unified/LocationAttributesProto;->rollDegrees_:I
    invoke-static {v0, p1}, Llocation/unified/LocationAttributesProto;->access$1402(Llocation/unified/LocationAttributesProto;I)I

    .line 554
    return-object p0
.end method

.method public setSpeedKph(I)Llocation/unified/LocationAttributesProto$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 516
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationAttributesProto;->hasSpeedKph:Z
    invoke-static {v0, v1}, Llocation/unified/LocationAttributesProto;->access$902(Llocation/unified/LocationAttributesProto;Z)Z

    .line 517
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    # setter for: Llocation/unified/LocationAttributesProto;->speedKph_:I
    invoke-static {v0, p1}, Llocation/unified/LocationAttributesProto;->access$1002(Llocation/unified/LocationAttributesProto;I)I

    .line 518
    return-object p0
.end method

.method public setTiltDegrees(I)Llocation/unified/LocationAttributesProto$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 534
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    const/4 v1, 0x1

    # setter for: Llocation/unified/LocationAttributesProto;->hasTiltDegrees:Z
    invoke-static {v0, v1}, Llocation/unified/LocationAttributesProto;->access$1102(Llocation/unified/LocationAttributesProto;Z)Z

    .line 535
    iget-object v0, p0, Llocation/unified/LocationAttributesProto$Builder;->result:Llocation/unified/LocationAttributesProto;

    # setter for: Llocation/unified/LocationAttributesProto;->tiltDegrees_:I
    invoke-static {v0, p1}, Llocation/unified/LocationAttributesProto;->access$1202(Llocation/unified/LocationAttributesProto;I)I

    .line 536
    return-object p0
.end method
