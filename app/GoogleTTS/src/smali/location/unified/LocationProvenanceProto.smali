.class public final Llocation/unified/LocationProvenanceProto;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LocationProvenanceProto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llocation/unified/LocationProvenanceProto$1;,
        Llocation/unified/LocationProvenanceProto$Builder;,
        Llocation/unified/LocationProvenanceProto$LocationProvenance;
    }
.end annotation


# static fields
.field private static final defaultInstance:Llocation/unified/LocationProvenanceProto;


# instance fields
.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 269
    new-instance v0, Llocation/unified/LocationProvenanceProto;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Llocation/unified/LocationProvenanceProto;-><init>(Z)V

    sput-object v0, Llocation/unified/LocationProvenanceProto;->defaultInstance:Llocation/unified/LocationProvenanceProto;

    .line 270
    invoke-static {}, Llocation/unified/LocationDescriptorProto;->internalForceInit()V

    .line 271
    sget-object v0, Llocation/unified/LocationProvenanceProto;->defaultInstance:Llocation/unified/LocationProvenanceProto;

    invoke-direct {v0}, Llocation/unified/LocationProvenanceProto;->initFields()V

    .line 272
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 92
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationProvenanceProto;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Llocation/unified/LocationProvenanceProto;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Llocation/unified/LocationProvenanceProto$1;)V
    .locals 0
    .param p1, "x0"    # Llocation/unified/LocationProvenanceProto$1;

    .prologue
    .line 5
    invoke-direct {p0}, Llocation/unified/LocationProvenanceProto;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 92
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationProvenanceProto;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method public static getDefaultInstance()Llocation/unified/LocationProvenanceProto;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Llocation/unified/LocationProvenanceProto;->defaultInstance:Llocation/unified/LocationProvenanceProto;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public static newBuilder()Llocation/unified/LocationProvenanceProto$Builder;
    .locals 1

    .prologue
    .line 169
    # invokes: Llocation/unified/LocationProvenanceProto$Builder;->create()Llocation/unified/LocationProvenanceProto$Builder;
    invoke-static {}, Llocation/unified/LocationProvenanceProto$Builder;->access$100()Llocation/unified/LocationProvenanceProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Llocation/unified/LocationProvenanceProto;)Llocation/unified/LocationProvenanceProto$Builder;
    .locals 1
    .param p0, "prototype"    # Llocation/unified/LocationProvenanceProto;

    .prologue
    .line 172
    invoke-static {}, Llocation/unified/LocationProvenanceProto;->newBuilder()Llocation/unified/LocationProvenanceProto$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Llocation/unified/LocationProvenanceProto$Builder;->mergeFrom(Llocation/unified/LocationProvenanceProto;)Llocation/unified/LocationProvenanceProto$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LocationProvenanceProto;->getDefaultInstanceForType()Llocation/unified/LocationProvenanceProto;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Llocation/unified/LocationProvenanceProto;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Llocation/unified/LocationProvenanceProto;->defaultInstance:Llocation/unified/LocationProvenanceProto;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 94
    iget v0, p0, Llocation/unified/LocationProvenanceProto;->memoizedSerializedSize:I

    .line 95
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 99
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 97
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 98
    iput v0, p0, Llocation/unified/LocationProvenanceProto;->memoizedSerializedSize:I

    move v1, v0

    .line 99
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/LocationProvenanceProto;->toBuilder()Llocation/unified/LocationProvenanceProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Llocation/unified/LocationProvenanceProto$Builder;
    .locals 1

    .prologue
    .line 174
    invoke-static {p0}, Llocation/unified/LocationProvenanceProto;->newBuilder(Llocation/unified/LocationProvenanceProto;)Llocation/unified/LocationProvenanceProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 0
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    invoke-virtual {p0}, Llocation/unified/LocationProvenanceProto;->getSerializedSize()I

    .line 90
    return-void
.end method
