.class public final Llocation/unified/FeatureIdProto;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FeatureIdProto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llocation/unified/FeatureIdProto$1;,
        Llocation/unified/FeatureIdProto$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Llocation/unified/FeatureIdProto;


# instance fields
.field private cellId_:J

.field private fprint_:J

.field private hasCellId:Z

.field private hasFprint:Z

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 288
    new-instance v0, Llocation/unified/FeatureIdProto;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Llocation/unified/FeatureIdProto;-><init>(Z)V

    sput-object v0, Llocation/unified/FeatureIdProto;->defaultInstance:Llocation/unified/FeatureIdProto;

    .line 289
    invoke-static {}, Llocation/unified/LocationDescriptorProto;->internalForceInit()V

    .line 290
    sget-object v0, Llocation/unified/FeatureIdProto;->defaultInstance:Llocation/unified/FeatureIdProto;

    invoke-direct {v0}, Llocation/unified/FeatureIdProto;->initFields()V

    .line 291
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput-wide v0, p0, Llocation/unified/FeatureIdProto;->cellId_:J

    .line 32
    iput-wide v0, p0, Llocation/unified/FeatureIdProto;->fprint_:J

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/FeatureIdProto;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Llocation/unified/FeatureIdProto;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Llocation/unified/FeatureIdProto$1;)V
    .locals 0
    .param p1, "x0"    # Llocation/unified/FeatureIdProto$1;

    .prologue
    .line 5
    invoke-direct {p0}, Llocation/unified/FeatureIdProto;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const-wide/16 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput-wide v0, p0, Llocation/unified/FeatureIdProto;->cellId_:J

    .line 32
    iput-wide v0, p0, Llocation/unified/FeatureIdProto;->fprint_:J

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/FeatureIdProto;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Llocation/unified/FeatureIdProto;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/FeatureIdProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/FeatureIdProto;->hasCellId:Z

    return p1
.end method

.method static synthetic access$402(Llocation/unified/FeatureIdProto;J)J
    .locals 1
    .param p0, "x0"    # Llocation/unified/FeatureIdProto;
    .param p1, "x1"    # J

    .prologue
    .line 5
    iput-wide p1, p0, Llocation/unified/FeatureIdProto;->cellId_:J

    return-wide p1
.end method

.method static synthetic access$502(Llocation/unified/FeatureIdProto;Z)Z
    .locals 0
    .param p0, "x0"    # Llocation/unified/FeatureIdProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Llocation/unified/FeatureIdProto;->hasFprint:Z

    return p1
.end method

.method static synthetic access$602(Llocation/unified/FeatureIdProto;J)J
    .locals 1
    .param p0, "x0"    # Llocation/unified/FeatureIdProto;
    .param p1, "x1"    # J

    .prologue
    .line 5
    iput-wide p1, p0, Llocation/unified/FeatureIdProto;->fprint_:J

    return-wide p1
.end method

.method public static getDefaultInstance()Llocation/unified/FeatureIdProto;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Llocation/unified/FeatureIdProto;->defaultInstance:Llocation/unified/FeatureIdProto;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public static newBuilder()Llocation/unified/FeatureIdProto$Builder;
    .locals 1

    .prologue
    .line 138
    # invokes: Llocation/unified/FeatureIdProto$Builder;->create()Llocation/unified/FeatureIdProto$Builder;
    invoke-static {}, Llocation/unified/FeatureIdProto$Builder;->access$100()Llocation/unified/FeatureIdProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto$Builder;
    .locals 1
    .param p0, "prototype"    # Llocation/unified/FeatureIdProto;

    .prologue
    .line 141
    invoke-static {}, Llocation/unified/FeatureIdProto;->newBuilder()Llocation/unified/FeatureIdProto$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Llocation/unified/FeatureIdProto$Builder;->mergeFrom(Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCellId()J
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, Llocation/unified/FeatureIdProto;->cellId_:J

    return-wide v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/FeatureIdProto;->getDefaultInstanceForType()Llocation/unified/FeatureIdProto;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Llocation/unified/FeatureIdProto;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Llocation/unified/FeatureIdProto;->defaultInstance:Llocation/unified/FeatureIdProto;

    return-object v0
.end method

.method public getFprint()J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Llocation/unified/FeatureIdProto;->fprint_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 55
    iget v0, p0, Llocation/unified/FeatureIdProto;->memoizedSerializedSize:I

    .line 56
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 68
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 58
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 59
    invoke-virtual {p0}, Llocation/unified/FeatureIdProto;->hasCellId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    const/4 v2, 0x1

    invoke-virtual {p0}, Llocation/unified/FeatureIdProto;->getCellId()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeFixed64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 63
    :cond_1
    invoke-virtual {p0}, Llocation/unified/FeatureIdProto;->hasFprint()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 64
    const/4 v2, 0x2

    invoke-virtual {p0}, Llocation/unified/FeatureIdProto;->getFprint()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeFixed64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 67
    :cond_2
    iput v0, p0, Llocation/unified/FeatureIdProto;->memoizedSerializedSize:I

    move v1, v0

    .line 68
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasCellId()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Llocation/unified/FeatureIdProto;->hasCellId:Z

    return v0
.end method

.method public hasFprint()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Llocation/unified/FeatureIdProto;->hasFprint:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Llocation/unified/FeatureIdProto;->toBuilder()Llocation/unified/FeatureIdProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Llocation/unified/FeatureIdProto$Builder;
    .locals 1

    .prologue
    .line 143
    invoke-static {p0}, Llocation/unified/FeatureIdProto;->newBuilder(Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Llocation/unified/FeatureIdProto;->getSerializedSize()I

    .line 45
    invoke-virtual {p0}, Llocation/unified/FeatureIdProto;->hasCellId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const/4 v0, 0x1

    invoke-virtual {p0}, Llocation/unified/FeatureIdProto;->getCellId()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFixed64(IJ)V

    .line 48
    :cond_0
    invoke-virtual {p0}, Llocation/unified/FeatureIdProto;->hasFprint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    const/4 v0, 0x2

    invoke-virtual {p0}, Llocation/unified/FeatureIdProto;->getFprint()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFixed64(IJ)V

    .line 51
    :cond_1
    return-void
.end method
