.class public final Llocation/unified/FeatureIdProto$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FeatureIdProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/FeatureIdProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Llocation/unified/FeatureIdProto;",
        "Llocation/unified/FeatureIdProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Llocation/unified/FeatureIdProto;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Llocation/unified/FeatureIdProto$Builder;
    .locals 1

    .prologue
    .line 145
    invoke-static {}, Llocation/unified/FeatureIdProto$Builder;->create()Llocation/unified/FeatureIdProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Llocation/unified/FeatureIdProto$Builder;
    .locals 3

    .prologue
    .line 154
    new-instance v0, Llocation/unified/FeatureIdProto$Builder;

    invoke-direct {v0}, Llocation/unified/FeatureIdProto$Builder;-><init>()V

    .line 155
    .local v0, "builder":Llocation/unified/FeatureIdProto$Builder;
    new-instance v1, Llocation/unified/FeatureIdProto;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Llocation/unified/FeatureIdProto;-><init>(Llocation/unified/FeatureIdProto$1;)V

    iput-object v1, v0, Llocation/unified/FeatureIdProto$Builder;->result:Llocation/unified/FeatureIdProto;

    .line 156
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Llocation/unified/FeatureIdProto$Builder;->build()Llocation/unified/FeatureIdProto;

    move-result-object v0

    return-object v0
.end method

.method public build()Llocation/unified/FeatureIdProto;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Llocation/unified/FeatureIdProto$Builder;->result:Llocation/unified/FeatureIdProto;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/FeatureIdProto$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Llocation/unified/FeatureIdProto$Builder;->result:Llocation/unified/FeatureIdProto;

    invoke-static {v0}, Llocation/unified/FeatureIdProto$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 187
    :cond_0
    invoke-virtual {p0}, Llocation/unified/FeatureIdProto$Builder;->buildPartial()Llocation/unified/FeatureIdProto;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Llocation/unified/FeatureIdProto;
    .locals 3

    .prologue
    .line 200
    iget-object v1, p0, Llocation/unified/FeatureIdProto$Builder;->result:Llocation/unified/FeatureIdProto;

    if-nez v1, :cond_0

    .line 201
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 204
    :cond_0
    iget-object v0, p0, Llocation/unified/FeatureIdProto$Builder;->result:Llocation/unified/FeatureIdProto;

    .line 205
    .local v0, "returnMe":Llocation/unified/FeatureIdProto;
    const/4 v1, 0x0

    iput-object v1, p0, Llocation/unified/FeatureIdProto$Builder;->result:Llocation/unified/FeatureIdProto;

    .line 206
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Llocation/unified/FeatureIdProto$Builder;->clone()Llocation/unified/FeatureIdProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Llocation/unified/FeatureIdProto$Builder;->clone()Llocation/unified/FeatureIdProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 145
    invoke-virtual {p0}, Llocation/unified/FeatureIdProto$Builder;->clone()Llocation/unified/FeatureIdProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Llocation/unified/FeatureIdProto$Builder;
    .locals 2

    .prologue
    .line 173
    invoke-static {}, Llocation/unified/FeatureIdProto$Builder;->create()Llocation/unified/FeatureIdProto$Builder;

    move-result-object v0

    iget-object v1, p0, Llocation/unified/FeatureIdProto$Builder;->result:Llocation/unified/FeatureIdProto;

    invoke-virtual {v0, v1}, Llocation/unified/FeatureIdProto$Builder;->mergeFrom(Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Llocation/unified/FeatureIdProto$Builder;->result:Llocation/unified/FeatureIdProto;

    invoke-virtual {v0}, Llocation/unified/FeatureIdProto;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 145
    check-cast p1, Llocation/unified/FeatureIdProto;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Llocation/unified/FeatureIdProto$Builder;->mergeFrom(Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Llocation/unified/FeatureIdProto;)Llocation/unified/FeatureIdProto$Builder;
    .locals 2
    .param p1, "other"    # Llocation/unified/FeatureIdProto;

    .prologue
    .line 210
    invoke-static {}, Llocation/unified/FeatureIdProto;->getDefaultInstance()Llocation/unified/FeatureIdProto;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-object p0

    .line 211
    :cond_1
    invoke-virtual {p1}, Llocation/unified/FeatureIdProto;->hasCellId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 212
    invoke-virtual {p1}, Llocation/unified/FeatureIdProto;->getCellId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Llocation/unified/FeatureIdProto$Builder;->setCellId(J)Llocation/unified/FeatureIdProto$Builder;

    .line 214
    :cond_2
    invoke-virtual {p1}, Llocation/unified/FeatureIdProto;->hasFprint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    invoke-virtual {p1}, Llocation/unified/FeatureIdProto;->getFprint()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Llocation/unified/FeatureIdProto$Builder;->setFprint(J)Llocation/unified/FeatureIdProto$Builder;

    goto :goto_0
.end method

.method public setCellId(J)Llocation/unified/FeatureIdProto$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 256
    iget-object v0, p0, Llocation/unified/FeatureIdProto$Builder;->result:Llocation/unified/FeatureIdProto;

    const/4 v1, 0x1

    # setter for: Llocation/unified/FeatureIdProto;->hasCellId:Z
    invoke-static {v0, v1}, Llocation/unified/FeatureIdProto;->access$302(Llocation/unified/FeatureIdProto;Z)Z

    .line 257
    iget-object v0, p0, Llocation/unified/FeatureIdProto$Builder;->result:Llocation/unified/FeatureIdProto;

    # setter for: Llocation/unified/FeatureIdProto;->cellId_:J
    invoke-static {v0, p1, p2}, Llocation/unified/FeatureIdProto;->access$402(Llocation/unified/FeatureIdProto;J)J

    .line 258
    return-object p0
.end method

.method public setFprint(J)Llocation/unified/FeatureIdProto$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 274
    iget-object v0, p0, Llocation/unified/FeatureIdProto$Builder;->result:Llocation/unified/FeatureIdProto;

    const/4 v1, 0x1

    # setter for: Llocation/unified/FeatureIdProto;->hasFprint:Z
    invoke-static {v0, v1}, Llocation/unified/FeatureIdProto;->access$502(Llocation/unified/FeatureIdProto;Z)Z

    .line 275
    iget-object v0, p0, Llocation/unified/FeatureIdProto$Builder;->result:Llocation/unified/FeatureIdProto;

    # setter for: Llocation/unified/FeatureIdProto;->fprint_:J
    invoke-static {v0, p1, p2}, Llocation/unified/FeatureIdProto;->access$602(Llocation/unified/FeatureIdProto;J)J

    .line 276
    return-object p0
.end method
