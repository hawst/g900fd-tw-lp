.class public final Llocation/unified/FieldOfView$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FieldOfView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/FieldOfView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Llocation/unified/FieldOfView;",
        "Llocation/unified/FieldOfView$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Llocation/unified/FieldOfView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Llocation/unified/FieldOfView$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-static {}, Llocation/unified/FieldOfView$Builder;->create()Llocation/unified/FieldOfView$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Llocation/unified/FieldOfView$Builder;
    .locals 3

    .prologue
    .line 168
    new-instance v0, Llocation/unified/FieldOfView$Builder;

    invoke-direct {v0}, Llocation/unified/FieldOfView$Builder;-><init>()V

    .line 169
    .local v0, "builder":Llocation/unified/FieldOfView$Builder;
    new-instance v1, Llocation/unified/FieldOfView;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Llocation/unified/FieldOfView;-><init>(Llocation/unified/FieldOfView$1;)V

    iput-object v1, v0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    .line 170
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Llocation/unified/FieldOfView$Builder;->build()Llocation/unified/FieldOfView;

    move-result-object v0

    return-object v0
.end method

.method public build()Llocation/unified/FieldOfView;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/FieldOfView$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    invoke-static {v0}, Llocation/unified/FieldOfView$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 201
    :cond_0
    invoke-virtual {p0}, Llocation/unified/FieldOfView$Builder;->buildPartial()Llocation/unified/FieldOfView;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Llocation/unified/FieldOfView;
    .locals 3

    .prologue
    .line 214
    iget-object v1, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    if-nez v1, :cond_0

    .line 215
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 218
    :cond_0
    iget-object v0, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    .line 219
    .local v0, "returnMe":Llocation/unified/FieldOfView;
    const/4 v1, 0x0

    iput-object v1, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    .line 220
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Llocation/unified/FieldOfView$Builder;->clone()Llocation/unified/FieldOfView$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Llocation/unified/FieldOfView$Builder;->clone()Llocation/unified/FieldOfView$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 159
    invoke-virtual {p0}, Llocation/unified/FieldOfView$Builder;->clone()Llocation/unified/FieldOfView$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Llocation/unified/FieldOfView$Builder;
    .locals 2

    .prologue
    .line 187
    invoke-static {}, Llocation/unified/FieldOfView$Builder;->create()Llocation/unified/FieldOfView$Builder;

    move-result-object v0

    iget-object v1, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    invoke-virtual {v0, v1}, Llocation/unified/FieldOfView$Builder;->mergeFrom(Llocation/unified/FieldOfView;)Llocation/unified/FieldOfView$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    invoke-virtual {v0}, Llocation/unified/FieldOfView;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 159
    check-cast p1, Llocation/unified/FieldOfView;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Llocation/unified/FieldOfView$Builder;->mergeFrom(Llocation/unified/FieldOfView;)Llocation/unified/FieldOfView$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Llocation/unified/FieldOfView;)Llocation/unified/FieldOfView$Builder;
    .locals 1
    .param p1, "other"    # Llocation/unified/FieldOfView;

    .prologue
    .line 224
    invoke-static {}, Llocation/unified/FieldOfView;->getDefaultInstance()Llocation/unified/FieldOfView;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 234
    :cond_0
    :goto_0
    return-object p0

    .line 225
    :cond_1
    invoke-virtual {p1}, Llocation/unified/FieldOfView;->hasFieldOfViewXDegrees()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 226
    invoke-virtual {p1}, Llocation/unified/FieldOfView;->getFieldOfViewXDegrees()F

    move-result v0

    invoke-virtual {p0, v0}, Llocation/unified/FieldOfView$Builder;->setFieldOfViewXDegrees(F)Llocation/unified/FieldOfView$Builder;

    .line 228
    :cond_2
    invoke-virtual {p1}, Llocation/unified/FieldOfView;->hasFieldOfViewYDegrees()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 229
    invoke-virtual {p1}, Llocation/unified/FieldOfView;->getFieldOfViewYDegrees()F

    move-result v0

    invoke-virtual {p0, v0}, Llocation/unified/FieldOfView$Builder;->setFieldOfViewYDegrees(F)Llocation/unified/FieldOfView$Builder;

    .line 231
    :cond_3
    invoke-virtual {p1}, Llocation/unified/FieldOfView;->hasScreenWidthPixels()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p1}, Llocation/unified/FieldOfView;->getScreenWidthPixels()I

    move-result v0

    invoke-virtual {p0, v0}, Llocation/unified/FieldOfView$Builder;->setScreenWidthPixels(I)Llocation/unified/FieldOfView$Builder;

    goto :goto_0
.end method

.method public setFieldOfViewXDegrees(F)Llocation/unified/FieldOfView$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 277
    iget-object v0, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    const/4 v1, 0x1

    # setter for: Llocation/unified/FieldOfView;->hasFieldOfViewXDegrees:Z
    invoke-static {v0, v1}, Llocation/unified/FieldOfView;->access$302(Llocation/unified/FieldOfView;Z)Z

    .line 278
    iget-object v0, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    # setter for: Llocation/unified/FieldOfView;->fieldOfViewXDegrees_:F
    invoke-static {v0, p1}, Llocation/unified/FieldOfView;->access$402(Llocation/unified/FieldOfView;F)F

    .line 279
    return-object p0
.end method

.method public setFieldOfViewYDegrees(F)Llocation/unified/FieldOfView$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 295
    iget-object v0, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    const/4 v1, 0x1

    # setter for: Llocation/unified/FieldOfView;->hasFieldOfViewYDegrees:Z
    invoke-static {v0, v1}, Llocation/unified/FieldOfView;->access$502(Llocation/unified/FieldOfView;Z)Z

    .line 296
    iget-object v0, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    # setter for: Llocation/unified/FieldOfView;->fieldOfViewYDegrees_:F
    invoke-static {v0, p1}, Llocation/unified/FieldOfView;->access$602(Llocation/unified/FieldOfView;F)F

    .line 297
    return-object p0
.end method

.method public setScreenWidthPixels(I)Llocation/unified/FieldOfView$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 313
    iget-object v0, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    const/4 v1, 0x1

    # setter for: Llocation/unified/FieldOfView;->hasScreenWidthPixels:Z
    invoke-static {v0, v1}, Llocation/unified/FieldOfView;->access$702(Llocation/unified/FieldOfView;Z)Z

    .line 314
    iget-object v0, p0, Llocation/unified/FieldOfView$Builder;->result:Llocation/unified/FieldOfView;

    # setter for: Llocation/unified/FieldOfView;->screenWidthPixels_:I
    invoke-static {v0, p1}, Llocation/unified/FieldOfView;->access$802(Llocation/unified/FieldOfView;I)I

    .line 315
    return-object p0
.end method
