.class public final enum Llocation/unified/LocationRole;
.super Ljava/lang/Enum;
.source "LocationRole.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Llocation/unified/LocationRole;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Llocation/unified/LocationRole;

.field public static final enum CURRENT_LOCATION:Llocation/unified/LocationRole;

.field public static final enum DEFAULT_LOCATION:Llocation/unified/LocationRole;

.field public static final enum HISTORICAL_LOCATION:Llocation/unified/LocationRole;

.field public static final enum HISTORICAL_QUERY:Llocation/unified/LocationRole;

.field public static final enum QUERY:Llocation/unified/LocationRole;

.field public static final enum UNKNOWN_ROLE:Llocation/unified/LocationRole;

.field public static final enum USER_SPECIFIED_FOR_REQUEST:Llocation/unified/LocationRole;

.field public static final enum VIEWPORT:Llocation/unified/LocationRole;

.field public static final enum WILDCARD_ROLE:Llocation/unified/LocationRole;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Llocation/unified/LocationRole;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 7
    new-instance v0, Llocation/unified/LocationRole;

    const-string v1, "UNKNOWN_ROLE"

    invoke-direct {v0, v1, v5, v5, v5}, Llocation/unified/LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRole;->UNKNOWN_ROLE:Llocation/unified/LocationRole;

    .line 8
    new-instance v0, Llocation/unified/LocationRole;

    const-string v1, "CURRENT_LOCATION"

    invoke-direct {v0, v1, v6, v6, v6}, Llocation/unified/LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRole;->CURRENT_LOCATION:Llocation/unified/LocationRole;

    .line 9
    new-instance v0, Llocation/unified/LocationRole;

    const-string v1, "DEFAULT_LOCATION"

    invoke-direct {v0, v1, v7, v7, v7}, Llocation/unified/LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRole;->DEFAULT_LOCATION:Llocation/unified/LocationRole;

    .line 10
    new-instance v0, Llocation/unified/LocationRole;

    const-string v1, "QUERY"

    invoke-direct {v0, v1, v8, v8, v8}, Llocation/unified/LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRole;->QUERY:Llocation/unified/LocationRole;

    .line 11
    new-instance v0, Llocation/unified/LocationRole;

    const-string v1, "USER_SPECIFIED_FOR_REQUEST"

    invoke-direct {v0, v1, v9, v9, v9}, Llocation/unified/LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRole;->USER_SPECIFIED_FOR_REQUEST:Llocation/unified/LocationRole;

    .line 12
    new-instance v0, Llocation/unified/LocationRole;

    const-string v1, "HISTORICAL_QUERY"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRole;->HISTORICAL_QUERY:Llocation/unified/LocationRole;

    .line 13
    new-instance v0, Llocation/unified/LocationRole;

    const-string v1, "HISTORICAL_LOCATION"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRole;->HISTORICAL_LOCATION:Llocation/unified/LocationRole;

    .line 14
    new-instance v0, Llocation/unified/LocationRole;

    const-string v1, "VIEWPORT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRole;->VIEWPORT:Llocation/unified/LocationRole;

    .line 15
    new-instance v0, Llocation/unified/LocationRole;

    const-string v1, "WILDCARD_ROLE"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/4 v4, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationRole;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationRole;->WILDCARD_ROLE:Llocation/unified/LocationRole;

    .line 5
    const/16 v0, 0x9

    new-array v0, v0, [Llocation/unified/LocationRole;

    sget-object v1, Llocation/unified/LocationRole;->UNKNOWN_ROLE:Llocation/unified/LocationRole;

    aput-object v1, v0, v5

    sget-object v1, Llocation/unified/LocationRole;->CURRENT_LOCATION:Llocation/unified/LocationRole;

    aput-object v1, v0, v6

    sget-object v1, Llocation/unified/LocationRole;->DEFAULT_LOCATION:Llocation/unified/LocationRole;

    aput-object v1, v0, v7

    sget-object v1, Llocation/unified/LocationRole;->QUERY:Llocation/unified/LocationRole;

    aput-object v1, v0, v8

    sget-object v1, Llocation/unified/LocationRole;->USER_SPECIFIED_FOR_REQUEST:Llocation/unified/LocationRole;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Llocation/unified/LocationRole;->HISTORICAL_QUERY:Llocation/unified/LocationRole;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Llocation/unified/LocationRole;->HISTORICAL_LOCATION:Llocation/unified/LocationRole;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Llocation/unified/LocationRole;->VIEWPORT:Llocation/unified/LocationRole;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Llocation/unified/LocationRole;->WILDCARD_ROLE:Llocation/unified/LocationRole;

    aput-object v2, v0, v1

    sput-object v0, Llocation/unified/LocationRole;->$VALUES:[Llocation/unified/LocationRole;

    .line 41
    new-instance v0, Llocation/unified/LocationRole$1;

    invoke-direct {v0}, Llocation/unified/LocationRole$1;-><init>()V

    sput-object v0, Llocation/unified/LocationRole;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput p3, p0, Llocation/unified/LocationRole;->index:I

    .line 52
    iput p4, p0, Llocation/unified/LocationRole;->value:I

    .line 53
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Llocation/unified/LocationRole;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Llocation/unified/LocationRole;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Llocation/unified/LocationRole;

    return-object v0
.end method

.method public static values()[Llocation/unified/LocationRole;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Llocation/unified/LocationRole;->$VALUES:[Llocation/unified/LocationRole;

    invoke-virtual {v0}, [Llocation/unified/LocationRole;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llocation/unified/LocationRole;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Llocation/unified/LocationRole;->value:I

    return v0
.end method
