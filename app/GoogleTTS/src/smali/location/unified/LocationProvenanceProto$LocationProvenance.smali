.class public final enum Llocation/unified/LocationProvenanceProto$LocationProvenance;
.super Ljava/lang/Enum;
.source "LocationProvenanceProto.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationProvenanceProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LocationProvenance"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Llocation/unified/LocationProvenanceProto$LocationProvenance;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field public static final enum EVAL_BASE_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field public static final enum EVAL_EXP_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field public static final enum EVAL_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field public static final enum GWS_MOBILE_CLIENT:Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field public static final enum LEGACY_MOBILE_FRONTEND_GLL_PARAM:Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field public static final enum LEGACY_MOBILE_FRONTEND_NEAR_PARAM:Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field public static final enum MAPS_FRONTEND_IL_DEBUG_IP:Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field public static final enum MOBILE_FE:Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field public static final enum TOOLBAR:Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field public static final enum UNREMARKABLE:Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field public static final enum XFF_HEADER:Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field public static final enum XGEO_HEADER:Llocation/unified/LocationProvenanceProto$LocationProvenance;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Llocation/unified/LocationProvenanceProto$LocationProvenance;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 24
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    const-string v1, "UNREMARKABLE"

    invoke-direct {v0, v1, v5, v5, v5}, Llocation/unified/LocationProvenanceProto$LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->UNREMARKABLE:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 25
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    const-string v1, "TOOLBAR"

    invoke-direct {v0, v1, v6, v6, v6}, Llocation/unified/LocationProvenanceProto$LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->TOOLBAR:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 26
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    const-string v1, "MOBILE_FE"

    invoke-direct {v0, v1, v7, v7, v7}, Llocation/unified/LocationProvenanceProto$LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->MOBILE_FE:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 27
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    const-string v1, "LEGACY_MOBILE_FRONTEND_GLL_PARAM"

    invoke-direct {v0, v1, v8, v8, v8}, Llocation/unified/LocationProvenanceProto$LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->LEGACY_MOBILE_FRONTEND_GLL_PARAM:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 28
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    const-string v1, "MAPS_FRONTEND_IL_DEBUG_IP"

    invoke-direct {v0, v1, v9, v9, v9}, Llocation/unified/LocationProvenanceProto$LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->MAPS_FRONTEND_IL_DEBUG_IP:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 29
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    const-string v1, "LEGACY_MOBILE_FRONTEND_NEAR_PARAM"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenanceProto$LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->LEGACY_MOBILE_FRONTEND_NEAR_PARAM:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 30
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    const-string v1, "GWS_MOBILE_CLIENT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenanceProto$LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->GWS_MOBILE_CLIENT:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 31
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    const-string v1, "XFF_HEADER"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenanceProto$LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->XFF_HEADER:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 32
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    const-string v1, "XGEO_HEADER"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenanceProto$LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->XGEO_HEADER:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 33
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    const-string v1, "EVAL_UNIQUE_SELECTED_USER_LOCATION"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const/16 v4, 0x65

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenanceProto$LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->EVAL_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 34
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    const-string v1, "EVAL_BASE_UNIQUE_SELECTED_USER_LOCATION"

    const/16 v2, 0xa

    const/16 v3, 0xa

    const/16 v4, 0x66

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenanceProto$LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->EVAL_BASE_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 35
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    const-string v1, "EVAL_EXP_UNIQUE_SELECTED_USER_LOCATION"

    const/16 v2, 0xb

    const/16 v3, 0xb

    const/16 v4, 0x67

    invoke-direct {v0, v1, v2, v3, v4}, Llocation/unified/LocationProvenanceProto$LocationProvenance;-><init>(Ljava/lang/String;III)V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->EVAL_EXP_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 22
    const/16 v0, 0xc

    new-array v0, v0, [Llocation/unified/LocationProvenanceProto$LocationProvenance;

    sget-object v1, Llocation/unified/LocationProvenanceProto$LocationProvenance;->UNREMARKABLE:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    aput-object v1, v0, v5

    sget-object v1, Llocation/unified/LocationProvenanceProto$LocationProvenance;->TOOLBAR:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    aput-object v1, v0, v6

    sget-object v1, Llocation/unified/LocationProvenanceProto$LocationProvenance;->MOBILE_FE:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    aput-object v1, v0, v7

    sget-object v1, Llocation/unified/LocationProvenanceProto$LocationProvenance;->LEGACY_MOBILE_FRONTEND_GLL_PARAM:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    aput-object v1, v0, v8

    sget-object v1, Llocation/unified/LocationProvenanceProto$LocationProvenance;->MAPS_FRONTEND_IL_DEBUG_IP:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Llocation/unified/LocationProvenanceProto$LocationProvenance;->LEGACY_MOBILE_FRONTEND_NEAR_PARAM:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Llocation/unified/LocationProvenanceProto$LocationProvenance;->GWS_MOBILE_CLIENT:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Llocation/unified/LocationProvenanceProto$LocationProvenance;->XFF_HEADER:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Llocation/unified/LocationProvenanceProto$LocationProvenance;->XGEO_HEADER:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Llocation/unified/LocationProvenanceProto$LocationProvenance;->EVAL_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Llocation/unified/LocationProvenanceProto$LocationProvenance;->EVAL_BASE_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Llocation/unified/LocationProvenanceProto$LocationProvenance;->EVAL_EXP_UNIQUE_SELECTED_USER_LOCATION:Llocation/unified/LocationProvenanceProto$LocationProvenance;

    aput-object v2, v0, v1

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->$VALUES:[Llocation/unified/LocationProvenanceProto$LocationProvenance;

    .line 64
    new-instance v0, Llocation/unified/LocationProvenanceProto$LocationProvenance$1;

    invoke-direct {v0}, Llocation/unified/LocationProvenanceProto$LocationProvenance$1;-><init>()V

    sput-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 74
    iput p3, p0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->index:I

    .line 75
    iput p4, p0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->value:I

    .line 76
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Llocation/unified/LocationProvenanceProto$LocationProvenance;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;

    return-object v0
.end method

.method public static values()[Llocation/unified/LocationProvenanceProto$LocationProvenance;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->$VALUES:[Llocation/unified/LocationProvenanceProto$LocationProvenance;

    invoke-virtual {v0}, [Llocation/unified/LocationProvenanceProto$LocationProvenance;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llocation/unified/LocationProvenanceProto$LocationProvenance;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Llocation/unified/LocationProvenanceProto$LocationProvenance;->value:I

    return v0
.end method
