.class public final Lgreco/ResourceDef;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "ResourceDef.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgreco/ResourceDef$1;,
        Lgreco/ResourceDef$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lgreco/ResourceDef;


# instance fields
.field private classname_:Ljava/lang/String;

.field private hasClassname:Z

.field private hasName:Z

.field private hasParams:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private params_:Lgreco/Params;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 361
    new-instance v0, Lgreco/ResourceDef;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lgreco/ResourceDef;-><init>(Z)V

    sput-object v0, Lgreco/ResourceDef;->defaultInstance:Lgreco/ResourceDef;

    .line 362
    invoke-static {}, Lgreco/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 363
    sget-object v0, Lgreco/ResourceDef;->defaultInstance:Lgreco/ResourceDef;

    invoke-direct {v0}, Lgreco/ResourceDef;->initFields()V

    .line 364
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lgreco/ResourceDef;->name_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lgreco/ResourceDef;->classname_:Ljava/lang/String;

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lgreco/ResourceDef;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lgreco/ResourceDef;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lgreco/ResourceDef$1;)V
    .locals 0
    .param p1, "x0"    # Lgreco/ResourceDef$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lgreco/ResourceDef;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lgreco/ResourceDef;->name_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lgreco/ResourceDef;->classname_:Ljava/lang/String;

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lgreco/ResourceDef;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lgreco/ResourceDef;Z)Z
    .locals 0
    .param p0, "x0"    # Lgreco/ResourceDef;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lgreco/ResourceDef;->hasName:Z

    return p1
.end method

.method static synthetic access$402(Lgreco/ResourceDef;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lgreco/ResourceDef;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lgreco/ResourceDef;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lgreco/ResourceDef;Z)Z
    .locals 0
    .param p0, "x0"    # Lgreco/ResourceDef;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lgreco/ResourceDef;->hasClassname:Z

    return p1
.end method

.method static synthetic access$602(Lgreco/ResourceDef;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lgreco/ResourceDef;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lgreco/ResourceDef;->classname_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lgreco/ResourceDef;Z)Z
    .locals 0
    .param p0, "x0"    # Lgreco/ResourceDef;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lgreco/ResourceDef;->hasParams:Z

    return p1
.end method

.method static synthetic access$800(Lgreco/ResourceDef;)Lgreco/Params;
    .locals 1
    .param p0, "x0"    # Lgreco/ResourceDef;

    .prologue
    .line 5
    iget-object v0, p0, Lgreco/ResourceDef;->params_:Lgreco/Params;

    return-object v0
.end method

.method static synthetic access$802(Lgreco/ResourceDef;Lgreco/Params;)Lgreco/Params;
    .locals 0
    .param p0, "x0"    # Lgreco/ResourceDef;
    .param p1, "x1"    # Lgreco/Params;

    .prologue
    .line 5
    iput-object p1, p0, Lgreco/ResourceDef;->params_:Lgreco/Params;

    return-object p1
.end method

.method public static getDefaultInstance()Lgreco/ResourceDef;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lgreco/ResourceDef;->defaultInstance:Lgreco/ResourceDef;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lgreco/Params;->getDefaultInstance()Lgreco/Params;

    move-result-object v0

    iput-object v0, p0, Lgreco/ResourceDef;->params_:Lgreco/Params;

    .line 45
    return-void
.end method

.method public static newBuilder()Lgreco/ResourceDef$Builder;
    .locals 1

    .prologue
    .line 156
    # invokes: Lgreco/ResourceDef$Builder;->create()Lgreco/ResourceDef$Builder;
    invoke-static {}, Lgreco/ResourceDef$Builder;->access$100()Lgreco/ResourceDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lgreco/ResourceDef;)Lgreco/ResourceDef$Builder;
    .locals 1
    .param p0, "prototype"    # Lgreco/ResourceDef;

    .prologue
    .line 159
    invoke-static {}, Lgreco/ResourceDef;->newBuilder()Lgreco/ResourceDef$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lgreco/ResourceDef$Builder;->mergeFrom(Lgreco/ResourceDef;)Lgreco/ResourceDef$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassname()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lgreco/ResourceDef;->classname_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lgreco/ResourceDef;->getDefaultInstanceForType()Lgreco/ResourceDef;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lgreco/ResourceDef;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lgreco/ResourceDef;->defaultInstance:Lgreco/ResourceDef;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lgreco/ResourceDef;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getParams()Lgreco/Params;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lgreco/ResourceDef;->params_:Lgreco/Params;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 69
    iget v0, p0, Lgreco/ResourceDef;->memoizedSerializedSize:I

    .line 70
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 86
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 72
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 73
    invoke-virtual {p0}, Lgreco/ResourceDef;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 74
    const/4 v2, 0x1

    invoke-virtual {p0}, Lgreco/ResourceDef;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 77
    :cond_1
    invoke-virtual {p0}, Lgreco/ResourceDef;->hasClassname()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 78
    const/4 v2, 0x2

    invoke-virtual {p0}, Lgreco/ResourceDef;->getClassname()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 81
    :cond_2
    invoke-virtual {p0}, Lgreco/ResourceDef;->hasParams()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 82
    const/4 v2, 0x3

    invoke-virtual {p0}, Lgreco/ResourceDef;->getParams()Lgreco/Params;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 85
    :cond_3
    iput v0, p0, Lgreco/ResourceDef;->memoizedSerializedSize:I

    move v1, v0

    .line 86
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasClassname()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lgreco/ResourceDef;->hasClassname:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lgreco/ResourceDef;->hasName:Z

    return v0
.end method

.method public hasParams()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lgreco/ResourceDef;->hasParams:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lgreco/ResourceDef;->hasParams()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lgreco/ResourceDef;->getParams()Lgreco/Params;

    move-result-object v0

    invoke-virtual {v0}, Lgreco/Params;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 50
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lgreco/ResourceDef;->toBuilder()Lgreco/ResourceDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lgreco/ResourceDef$Builder;
    .locals 1

    .prologue
    .line 161
    invoke-static {p0}, Lgreco/ResourceDef;->newBuilder(Lgreco/ResourceDef;)Lgreco/ResourceDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lgreco/ResourceDef;->getSerializedSize()I

    .line 56
    invoke-virtual {p0}, Lgreco/ResourceDef;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0}, Lgreco/ResourceDef;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 59
    :cond_0
    invoke-virtual {p0}, Lgreco/ResourceDef;->hasClassname()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    const/4 v0, 0x2

    invoke-virtual {p0}, Lgreco/ResourceDef;->getClassname()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 62
    :cond_1
    invoke-virtual {p0}, Lgreco/ResourceDef;->hasParams()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    const/4 v0, 0x3

    invoke-virtual {p0}, Lgreco/ResourceDef;->getParams()Lgreco/Params;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 65
    :cond_2
    return-void
.end method
