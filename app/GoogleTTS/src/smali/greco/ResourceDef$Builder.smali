.class public final Lgreco/ResourceDef$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ResourceDef.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgreco/ResourceDef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lgreco/ResourceDef;",
        "Lgreco/ResourceDef$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lgreco/ResourceDef;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lgreco/ResourceDef$Builder;
    .locals 1

    .prologue
    .line 163
    invoke-static {}, Lgreco/ResourceDef$Builder;->create()Lgreco/ResourceDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lgreco/ResourceDef$Builder;
    .locals 3

    .prologue
    .line 172
    new-instance v0, Lgreco/ResourceDef$Builder;

    invoke-direct {v0}, Lgreco/ResourceDef$Builder;-><init>()V

    .line 173
    .local v0, "builder":Lgreco/ResourceDef$Builder;
    new-instance v1, Lgreco/ResourceDef;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lgreco/ResourceDef;-><init>(Lgreco/ResourceDef$1;)V

    iput-object v1, v0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    .line 174
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lgreco/ResourceDef$Builder;->build()Lgreco/ResourceDef;

    move-result-object v0

    return-object v0
.end method

.method public build()Lgreco/ResourceDef;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgreco/ResourceDef$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    invoke-static {v0}, Lgreco/ResourceDef$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 205
    :cond_0
    invoke-virtual {p0}, Lgreco/ResourceDef$Builder;->buildPartial()Lgreco/ResourceDef;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lgreco/ResourceDef;
    .locals 3

    .prologue
    .line 218
    iget-object v1, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    if-nez v1, :cond_0

    .line 219
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 222
    :cond_0
    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    .line 223
    .local v0, "returnMe":Lgreco/ResourceDef;
    const/4 v1, 0x0

    iput-object v1, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    .line 224
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lgreco/ResourceDef$Builder;->clone()Lgreco/ResourceDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lgreco/ResourceDef$Builder;->clone()Lgreco/ResourceDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lgreco/ResourceDef$Builder;
    .locals 2

    .prologue
    .line 191
    invoke-static {}, Lgreco/ResourceDef$Builder;->create()Lgreco/ResourceDef$Builder;

    move-result-object v0

    iget-object v1, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    invoke-virtual {v0, v1}, Lgreco/ResourceDef$Builder;->mergeFrom(Lgreco/ResourceDef;)Lgreco/ResourceDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 163
    invoke-virtual {p0}, Lgreco/ResourceDef$Builder;->clone()Lgreco/ResourceDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    invoke-virtual {v0}, Lgreco/ResourceDef;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 163
    check-cast p1, Lgreco/ResourceDef;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lgreco/ResourceDef$Builder;->mergeFrom(Lgreco/ResourceDef;)Lgreco/ResourceDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lgreco/ResourceDef;)Lgreco/ResourceDef$Builder;
    .locals 1
    .param p1, "other"    # Lgreco/ResourceDef;

    .prologue
    .line 228
    invoke-static {}, Lgreco/ResourceDef;->getDefaultInstance()Lgreco/ResourceDef;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-object p0

    .line 229
    :cond_1
    invoke-virtual {p1}, Lgreco/ResourceDef;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 230
    invoke-virtual {p1}, Lgreco/ResourceDef;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgreco/ResourceDef$Builder;->setName(Ljava/lang/String;)Lgreco/ResourceDef$Builder;

    .line 232
    :cond_2
    invoke-virtual {p1}, Lgreco/ResourceDef;->hasClassname()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 233
    invoke-virtual {p1}, Lgreco/ResourceDef;->getClassname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgreco/ResourceDef$Builder;->setClassname(Ljava/lang/String;)Lgreco/ResourceDef$Builder;

    .line 235
    :cond_3
    invoke-virtual {p1}, Lgreco/ResourceDef;->hasParams()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    invoke-virtual {p1}, Lgreco/ResourceDef;->getParams()Lgreco/Params;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgreco/ResourceDef$Builder;->mergeParams(Lgreco/Params;)Lgreco/ResourceDef$Builder;

    goto :goto_0
.end method

.method public mergeParams(Lgreco/Params;)Lgreco/ResourceDef$Builder;
    .locals 2
    .param p1, "value"    # Lgreco/Params;

    .prologue
    .line 341
    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    invoke-virtual {v0}, Lgreco/ResourceDef;->hasParams()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    # getter for: Lgreco/ResourceDef;->params_:Lgreco/Params;
    invoke-static {v0}, Lgreco/ResourceDef;->access$800(Lgreco/ResourceDef;)Lgreco/Params;

    move-result-object v0

    invoke-static {}, Lgreco/Params;->getDefaultInstance()Lgreco/Params;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 343
    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    iget-object v1, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    # getter for: Lgreco/ResourceDef;->params_:Lgreco/Params;
    invoke-static {v1}, Lgreco/ResourceDef;->access$800(Lgreco/ResourceDef;)Lgreco/Params;

    move-result-object v1

    invoke-static {v1}, Lgreco/Params;->newBuilder(Lgreco/Params;)Lgreco/Params$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lgreco/Params$Builder;->mergeFrom(Lgreco/Params;)Lgreco/Params$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lgreco/Params$Builder;->buildPartial()Lgreco/Params;

    move-result-object v1

    # setter for: Lgreco/ResourceDef;->params_:Lgreco/Params;
    invoke-static {v0, v1}, Lgreco/ResourceDef;->access$802(Lgreco/ResourceDef;Lgreco/Params;)Lgreco/Params;

    .line 348
    :goto_0
    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    const/4 v1, 0x1

    # setter for: Lgreco/ResourceDef;->hasParams:Z
    invoke-static {v0, v1}, Lgreco/ResourceDef;->access$702(Lgreco/ResourceDef;Z)Z

    .line 349
    return-object p0

    .line 346
    :cond_0
    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    # setter for: Lgreco/ResourceDef;->params_:Lgreco/Params;
    invoke-static {v0, p1}, Lgreco/ResourceDef;->access$802(Lgreco/ResourceDef;Lgreco/Params;)Lgreco/Params;

    goto :goto_0
.end method

.method public setClassname(Ljava/lang/String;)Lgreco/ResourceDef$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 307
    if-nez p1, :cond_0

    .line 308
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 310
    :cond_0
    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    const/4 v1, 0x1

    # setter for: Lgreco/ResourceDef;->hasClassname:Z
    invoke-static {v0, v1}, Lgreco/ResourceDef;->access$502(Lgreco/ResourceDef;Z)Z

    .line 311
    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    # setter for: Lgreco/ResourceDef;->classname_:Ljava/lang/String;
    invoke-static {v0, p1}, Lgreco/ResourceDef;->access$602(Lgreco/ResourceDef;Ljava/lang/String;)Ljava/lang/String;

    .line 312
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lgreco/ResourceDef$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 286
    if-nez p1, :cond_0

    .line 287
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 289
    :cond_0
    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    const/4 v1, 0x1

    # setter for: Lgreco/ResourceDef;->hasName:Z
    invoke-static {v0, v1}, Lgreco/ResourceDef;->access$302(Lgreco/ResourceDef;Z)Z

    .line 290
    iget-object v0, p0, Lgreco/ResourceDef$Builder;->result:Lgreco/ResourceDef;

    # setter for: Lgreco/ResourceDef;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lgreco/ResourceDef;->access$402(Lgreco/ResourceDef;Ljava/lang/String;)Ljava/lang/String;

    .line 291
    return-object p0
.end method
