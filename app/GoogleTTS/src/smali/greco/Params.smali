.class public final Lgreco/Params;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
.source "Params.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgreco/Params$1;,
        Lgreco/Params$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage",
        "<",
        "Lgreco/Params;",
        ">;"
    }
.end annotation


# static fields
.field private static final defaultInstance:Lgreco/Params;

.field public static final logId:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension",
            "<",
            "Lcom/google/speech/recognizer/Loggable;",
            "Lgreco/Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->newGeneratedExtension()Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    move-result-object v0

    sput-object v0, Lgreco/Params;->logId:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    .line 224
    new-instance v0, Lgreco/Params;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lgreco/Params;-><init>(Z)V

    sput-object v0, Lgreco/Params;->defaultInstance:Lgreco/Params;

    .line 225
    invoke-static {}, Lgreco/ParamsProto;->internalForceInit()V

    .line 226
    sget-object v0, Lgreco/Params;->defaultInstance:Lgreco/Params;

    invoke-direct {v0}, Lgreco/Params;->initFields()V

    .line 227
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lgreco/Params;->memoizedSerializedSize:I

    .line 10
    invoke-direct {p0}, Lgreco/Params;->initFields()V

    .line 11
    return-void
.end method

.method synthetic constructor <init>(Lgreco/Params$1;)V
    .locals 0
    .param p1, "x0"    # Lgreco/Params$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lgreco/Params;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lgreco/Params;->memoizedSerializedSize:I

    .line 12
    return-void
.end method

.method public static getDefaultInstance()Lgreco/Params;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lgreco/Params;->defaultInstance:Lgreco/Params;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public static newBuilder()Lgreco/Params$Builder;
    .locals 1

    .prologue
    .line 123
    # invokes: Lgreco/Params$Builder;->create()Lgreco/Params$Builder;
    invoke-static {}, Lgreco/Params$Builder;->access$100()Lgreco/Params$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lgreco/Params;)Lgreco/Params$Builder;
    .locals 1
    .param p0, "prototype"    # Lgreco/Params;

    .prologue
    .line 126
    invoke-static {}, Lgreco/Params;->newBuilder()Lgreco/Params$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lgreco/Params$Builder;->mergeFrom(Lgreco/Params;)Lgreco/Params$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lgreco/Params;->getDefaultInstanceForType()Lgreco/Params;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lgreco/Params;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lgreco/Params;->defaultInstance:Lgreco/Params;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 47
    iget v0, p0, Lgreco/Params;->memoizedSerializedSize:I

    .line 48
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 53
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 50
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 51
    invoke-virtual {p0}, Lgreco/Params;->extensionsSerializedSize()I

    move-result v2

    add-int/2addr v0, v2

    .line 52
    iput v0, p0, Lgreco/Params;->memoizedSerializedSize:I

    move v1, v0

    .line 53
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lgreco/Params;->extensionsAreInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 34
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lgreco/Params;->toBuilder()Lgreco/Params$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lgreco/Params$Builder;
    .locals 1

    .prologue
    .line 128
    invoke-static {p0}, Lgreco/Params;->newBuilder(Lgreco/Params;)Lgreco/Params$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p0}, Lgreco/Params;->getSerializedSize()I

    .line 41
    invoke-virtual {p0}, Lgreco/Params;->newExtensionWriter()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;

    move-result-object v0

    .line 42
    .local v0, "extensionWriter":Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;->writeUntil(ILcom/google/protobuf/CodedOutputStream;)V

    .line 43
    return-void
.end method
