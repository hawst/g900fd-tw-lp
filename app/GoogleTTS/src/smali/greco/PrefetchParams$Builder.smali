.class public final Lgreco/PrefetchParams$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PrefetchParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgreco/PrefetchParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lgreco/PrefetchParams;",
        "Lgreco/PrefetchParams$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lgreco/PrefetchParams;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lgreco/PrefetchParams$Builder;
    .locals 1

    .prologue
    .line 138
    invoke-static {}, Lgreco/PrefetchParams$Builder;->create()Lgreco/PrefetchParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lgreco/PrefetchParams$Builder;
    .locals 3

    .prologue
    .line 147
    new-instance v0, Lgreco/PrefetchParams$Builder;

    invoke-direct {v0}, Lgreco/PrefetchParams$Builder;-><init>()V

    .line 148
    .local v0, "builder":Lgreco/PrefetchParams$Builder;
    new-instance v1, Lgreco/PrefetchParams;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lgreco/PrefetchParams;-><init>(Lgreco/PrefetchParams$1;)V

    iput-object v1, v0, Lgreco/PrefetchParams$Builder;->result:Lgreco/PrefetchParams;

    .line 149
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lgreco/PrefetchParams$Builder;->build()Lgreco/PrefetchParams;

    move-result-object v0

    return-object v0
.end method

.method public build()Lgreco/PrefetchParams;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lgreco/PrefetchParams$Builder;->result:Lgreco/PrefetchParams;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgreco/PrefetchParams$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    iget-object v0, p0, Lgreco/PrefetchParams$Builder;->result:Lgreco/PrefetchParams;

    invoke-static {v0}, Lgreco/PrefetchParams$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 180
    :cond_0
    invoke-virtual {p0}, Lgreco/PrefetchParams$Builder;->buildPartial()Lgreco/PrefetchParams;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lgreco/PrefetchParams;
    .locals 3

    .prologue
    .line 193
    iget-object v1, p0, Lgreco/PrefetchParams$Builder;->result:Lgreco/PrefetchParams;

    if-nez v1, :cond_0

    .line 194
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 197
    :cond_0
    iget-object v0, p0, Lgreco/PrefetchParams$Builder;->result:Lgreco/PrefetchParams;

    .line 198
    .local v0, "returnMe":Lgreco/PrefetchParams;
    const/4 v1, 0x0

    iput-object v1, p0, Lgreco/PrefetchParams$Builder;->result:Lgreco/PrefetchParams;

    .line 199
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lgreco/PrefetchParams$Builder;->clone()Lgreco/PrefetchParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lgreco/PrefetchParams$Builder;->clone()Lgreco/PrefetchParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lgreco/PrefetchParams$Builder;
    .locals 2

    .prologue
    .line 166
    invoke-static {}, Lgreco/PrefetchParams$Builder;->create()Lgreco/PrefetchParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lgreco/PrefetchParams$Builder;->result:Lgreco/PrefetchParams;

    invoke-virtual {v0, v1}, Lgreco/PrefetchParams$Builder;->mergeFrom(Lgreco/PrefetchParams;)Lgreco/PrefetchParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p0}, Lgreco/PrefetchParams$Builder;->clone()Lgreco/PrefetchParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lgreco/PrefetchParams$Builder;->result:Lgreco/PrefetchParams;

    invoke-virtual {v0}, Lgreco/PrefetchParams;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 138
    check-cast p1, Lgreco/PrefetchParams;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lgreco/PrefetchParams$Builder;->mergeFrom(Lgreco/PrefetchParams;)Lgreco/PrefetchParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lgreco/PrefetchParams;)Lgreco/PrefetchParams$Builder;
    .locals 1
    .param p1, "other"    # Lgreco/PrefetchParams;

    .prologue
    .line 203
    invoke-static {}, Lgreco/PrefetchParams;->getDefaultInstance()Lgreco/PrefetchParams;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-object p0

    .line 204
    :cond_1
    invoke-virtual {p1}, Lgreco/PrefetchParams;->hasPrefetch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {p1}, Lgreco/PrefetchParams;->getPrefetch()Z

    move-result v0

    invoke-virtual {p0, v0}, Lgreco/PrefetchParams$Builder;->setPrefetch(Z)Lgreco/PrefetchParams$Builder;

    goto :goto_0
.end method

.method public setPrefetch(Z)Lgreco/PrefetchParams$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 242
    iget-object v0, p0, Lgreco/PrefetchParams$Builder;->result:Lgreco/PrefetchParams;

    const/4 v1, 0x1

    # setter for: Lgreco/PrefetchParams;->hasPrefetch:Z
    invoke-static {v0, v1}, Lgreco/PrefetchParams;->access$302(Lgreco/PrefetchParams;Z)Z

    .line 243
    iget-object v0, p0, Lgreco/PrefetchParams$Builder;->result:Lgreco/PrefetchParams;

    # setter for: Lgreco/PrefetchParams;->prefetch_:Z
    invoke-static {v0, p1}, Lgreco/PrefetchParams;->access$402(Lgreco/PrefetchParams;Z)Z

    .line 244
    return-object p0
.end method
