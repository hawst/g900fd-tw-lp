.class public final Lgreco/ResourceLoadParams;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "ResourceLoadParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgreco/ResourceLoadParams$1;,
        Lgreco/ResourceLoadParams$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lgreco/ResourceLoadParams;

.field public static final id:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension",
            "<",
            "Lgreco/Params;",
            "Lgreco/ResourceLoadParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private hasSkipFileNotFound:Z

.field private memoizedSerializedSize:I

.field private skipFileNotFound_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->newGeneratedExtension()Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    move-result-object v0

    sput-object v0, Lgreco/ResourceLoadParams;->id:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    .line 256
    new-instance v0, Lgreco/ResourceLoadParams;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lgreco/ResourceLoadParams;-><init>(Z)V

    sput-object v0, Lgreco/ResourceLoadParams;->defaultInstance:Lgreco/ResourceLoadParams;

    .line 257
    invoke-static {}, Lgreco/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 258
    sget-object v0, Lgreco/ResourceLoadParams;->defaultInstance:Lgreco/ResourceLoadParams;

    invoke-direct {v0}, Lgreco/ResourceLoadParams;->initFields()V

    .line 259
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgreco/ResourceLoadParams;->skipFileNotFound_:Z

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lgreco/ResourceLoadParams;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lgreco/ResourceLoadParams;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lgreco/ResourceLoadParams$1;)V
    .locals 0
    .param p1, "x0"    # Lgreco/ResourceLoadParams$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lgreco/ResourceLoadParams;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgreco/ResourceLoadParams;->skipFileNotFound_:Z

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lgreco/ResourceLoadParams;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lgreco/ResourceLoadParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lgreco/ResourceLoadParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lgreco/ResourceLoadParams;->hasSkipFileNotFound:Z

    return p1
.end method

.method static synthetic access$402(Lgreco/ResourceLoadParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lgreco/ResourceLoadParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lgreco/ResourceLoadParams;->skipFileNotFound_:Z

    return p1
.end method

.method public static getDefaultInstance()Lgreco/ResourceLoadParams;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lgreco/ResourceLoadParams;->defaultInstance:Lgreco/ResourceLoadParams;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public static newBuilder()Lgreco/ResourceLoadParams$Builder;
    .locals 1

    .prologue
    .line 131
    # invokes: Lgreco/ResourceLoadParams$Builder;->create()Lgreco/ResourceLoadParams$Builder;
    invoke-static {}, Lgreco/ResourceLoadParams$Builder;->access$100()Lgreco/ResourceLoadParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lgreco/ResourceLoadParams;)Lgreco/ResourceLoadParams$Builder;
    .locals 1
    .param p0, "prototype"    # Lgreco/ResourceLoadParams;

    .prologue
    .line 134
    invoke-static {}, Lgreco/ResourceLoadParams;->newBuilder()Lgreco/ResourceLoadParams$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lgreco/ResourceLoadParams$Builder;->mergeFrom(Lgreco/ResourceLoadParams;)Lgreco/ResourceLoadParams$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lgreco/ResourceLoadParams;->getDefaultInstanceForType()Lgreco/ResourceLoadParams;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lgreco/ResourceLoadParams;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lgreco/ResourceLoadParams;->defaultInstance:Lgreco/ResourceLoadParams;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 52
    iget v0, p0, Lgreco/ResourceLoadParams;->memoizedSerializedSize:I

    .line 53
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 61
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 55
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 56
    invoke-virtual {p0}, Lgreco/ResourceLoadParams;->hasSkipFileNotFound()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 57
    const/4 v2, 0x2

    invoke-virtual {p0}, Lgreco/ResourceLoadParams;->getSkipFileNotFound()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 60
    :cond_1
    iput v0, p0, Lgreco/ResourceLoadParams;->memoizedSerializedSize:I

    move v1, v0

    .line 61
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getSkipFileNotFound()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lgreco/ResourceLoadParams;->skipFileNotFound_:Z

    return v0
.end method

.method public hasSkipFileNotFound()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lgreco/ResourceLoadParams;->hasSkipFileNotFound:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lgreco/ResourceLoadParams;->toBuilder()Lgreco/ResourceLoadParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lgreco/ResourceLoadParams$Builder;
    .locals 1

    .prologue
    .line 136
    invoke-static {p0}, Lgreco/ResourceLoadParams;->newBuilder(Lgreco/ResourceLoadParams;)Lgreco/ResourceLoadParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Lgreco/ResourceLoadParams;->getSerializedSize()I

    .line 45
    invoke-virtual {p0}, Lgreco/ResourceLoadParams;->hasSkipFileNotFound()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const/4 v0, 0x2

    invoke-virtual {p0}, Lgreco/ResourceLoadParams;->getSkipFileNotFound()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 48
    :cond_0
    return-void
.end method
