.class public final Lgreco/ResourceLoadParams$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ResourceLoadParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgreco/ResourceLoadParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lgreco/ResourceLoadParams;",
        "Lgreco/ResourceLoadParams$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lgreco/ResourceLoadParams;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lgreco/ResourceLoadParams$Builder;
    .locals 1

    .prologue
    .line 138
    invoke-static {}, Lgreco/ResourceLoadParams$Builder;->create()Lgreco/ResourceLoadParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lgreco/ResourceLoadParams$Builder;
    .locals 3

    .prologue
    .line 147
    new-instance v0, Lgreco/ResourceLoadParams$Builder;

    invoke-direct {v0}, Lgreco/ResourceLoadParams$Builder;-><init>()V

    .line 148
    .local v0, "builder":Lgreco/ResourceLoadParams$Builder;
    new-instance v1, Lgreco/ResourceLoadParams;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lgreco/ResourceLoadParams;-><init>(Lgreco/ResourceLoadParams$1;)V

    iput-object v1, v0, Lgreco/ResourceLoadParams$Builder;->result:Lgreco/ResourceLoadParams;

    .line 149
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lgreco/ResourceLoadParams$Builder;->build()Lgreco/ResourceLoadParams;

    move-result-object v0

    return-object v0
.end method

.method public build()Lgreco/ResourceLoadParams;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lgreco/ResourceLoadParams$Builder;->result:Lgreco/ResourceLoadParams;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgreco/ResourceLoadParams$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    iget-object v0, p0, Lgreco/ResourceLoadParams$Builder;->result:Lgreco/ResourceLoadParams;

    invoke-static {v0}, Lgreco/ResourceLoadParams$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 180
    :cond_0
    invoke-virtual {p0}, Lgreco/ResourceLoadParams$Builder;->buildPartial()Lgreco/ResourceLoadParams;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lgreco/ResourceLoadParams;
    .locals 3

    .prologue
    .line 193
    iget-object v1, p0, Lgreco/ResourceLoadParams$Builder;->result:Lgreco/ResourceLoadParams;

    if-nez v1, :cond_0

    .line 194
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 197
    :cond_0
    iget-object v0, p0, Lgreco/ResourceLoadParams$Builder;->result:Lgreco/ResourceLoadParams;

    .line 198
    .local v0, "returnMe":Lgreco/ResourceLoadParams;
    const/4 v1, 0x0

    iput-object v1, p0, Lgreco/ResourceLoadParams$Builder;->result:Lgreco/ResourceLoadParams;

    .line 199
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lgreco/ResourceLoadParams$Builder;->clone()Lgreco/ResourceLoadParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lgreco/ResourceLoadParams$Builder;->clone()Lgreco/ResourceLoadParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lgreco/ResourceLoadParams$Builder;
    .locals 2

    .prologue
    .line 166
    invoke-static {}, Lgreco/ResourceLoadParams$Builder;->create()Lgreco/ResourceLoadParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lgreco/ResourceLoadParams$Builder;->result:Lgreco/ResourceLoadParams;

    invoke-virtual {v0, v1}, Lgreco/ResourceLoadParams$Builder;->mergeFrom(Lgreco/ResourceLoadParams;)Lgreco/ResourceLoadParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p0}, Lgreco/ResourceLoadParams$Builder;->clone()Lgreco/ResourceLoadParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lgreco/ResourceLoadParams$Builder;->result:Lgreco/ResourceLoadParams;

    invoke-virtual {v0}, Lgreco/ResourceLoadParams;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 138
    check-cast p1, Lgreco/ResourceLoadParams;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lgreco/ResourceLoadParams$Builder;->mergeFrom(Lgreco/ResourceLoadParams;)Lgreco/ResourceLoadParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lgreco/ResourceLoadParams;)Lgreco/ResourceLoadParams$Builder;
    .locals 1
    .param p1, "other"    # Lgreco/ResourceLoadParams;

    .prologue
    .line 203
    invoke-static {}, Lgreco/ResourceLoadParams;->getDefaultInstance()Lgreco/ResourceLoadParams;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-object p0

    .line 204
    :cond_1
    invoke-virtual {p1}, Lgreco/ResourceLoadParams;->hasSkipFileNotFound()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {p1}, Lgreco/ResourceLoadParams;->getSkipFileNotFound()Z

    move-result v0

    invoke-virtual {p0, v0}, Lgreco/ResourceLoadParams$Builder;->setSkipFileNotFound(Z)Lgreco/ResourceLoadParams$Builder;

    goto :goto_0
.end method

.method public setSkipFileNotFound(Z)Lgreco/ResourceLoadParams$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 242
    iget-object v0, p0, Lgreco/ResourceLoadParams$Builder;->result:Lgreco/ResourceLoadParams;

    const/4 v1, 0x1

    # setter for: Lgreco/ResourceLoadParams;->hasSkipFileNotFound:Z
    invoke-static {v0, v1}, Lgreco/ResourceLoadParams;->access$302(Lgreco/ResourceLoadParams;Z)Z

    .line 243
    iget-object v0, p0, Lgreco/ResourceLoadParams$Builder;->result:Lgreco/ResourceLoadParams;

    # setter for: Lgreco/ResourceLoadParams;->skipFileNotFound_:Z
    invoke-static {v0, p1}, Lgreco/ResourceLoadParams;->access$402(Lgreco/ResourceLoadParams;Z)Z

    .line 244
    return-object p0
.end method
