.class public final Lgreco/Params$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
.source "Params.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgreco/Params;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
        "<",
        "Lgreco/Params;",
        "Lgreco/Params$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lgreco/Params;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lgreco/Params$Builder;
    .locals 1

    .prologue
    .line 130
    invoke-static {}, Lgreco/Params$Builder;->create()Lgreco/Params$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lgreco/Params$Builder;
    .locals 3

    .prologue
    .line 139
    new-instance v0, Lgreco/Params$Builder;

    invoke-direct {v0}, Lgreco/Params$Builder;-><init>()V

    .line 140
    .local v0, "builder":Lgreco/Params$Builder;
    new-instance v1, Lgreco/Params;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lgreco/Params;-><init>(Lgreco/Params$1;)V

    iput-object v1, v0, Lgreco/Params$Builder;->result:Lgreco/Params;

    .line 141
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lgreco/Params$Builder;->build()Lgreco/Params;

    move-result-object v0

    return-object v0
.end method

.method public build()Lgreco/Params;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lgreco/Params$Builder;->result:Lgreco/Params;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgreco/Params$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lgreco/Params$Builder;->result:Lgreco/Params;

    invoke-static {v0}, Lgreco/Params$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 172
    :cond_0
    invoke-virtual {p0}, Lgreco/Params$Builder;->buildPartial()Lgreco/Params;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lgreco/Params;
    .locals 3

    .prologue
    .line 185
    iget-object v1, p0, Lgreco/Params$Builder;->result:Lgreco/Params;

    if-nez v1, :cond_0

    .line 186
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 189
    :cond_0
    iget-object v0, p0, Lgreco/Params$Builder;->result:Lgreco/Params;

    .line 190
    .local v0, "returnMe":Lgreco/Params;
    const/4 v1, 0x0

    iput-object v1, p0, Lgreco/Params$Builder;->result:Lgreco/Params;

    .line 191
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lgreco/Params$Builder;->clone()Lgreco/Params$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lgreco/Params$Builder;->clone()Lgreco/Params$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lgreco/Params$Builder;->clone()Lgreco/Params$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lgreco/Params$Builder;
    .locals 2

    .prologue
    .line 158
    invoke-static {}, Lgreco/Params$Builder;->create()Lgreco/Params$Builder;

    move-result-object v0

    iget-object v1, p0, Lgreco/Params$Builder;->result:Lgreco/Params;

    invoke-virtual {v0, v1}, Lgreco/Params$Builder;->mergeFrom(Lgreco/Params;)Lgreco/Params$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 130
    invoke-virtual {p0}, Lgreco/Params$Builder;->clone()Lgreco/Params$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic internalGetResult()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lgreco/Params$Builder;->internalGetResult()Lgreco/Params;

    move-result-object v0

    return-object v0
.end method

.method protected internalGetResult()Lgreco/Params;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lgreco/Params$Builder;->result:Lgreco/Params;

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lgreco/Params$Builder;->result:Lgreco/Params;

    invoke-virtual {v0}, Lgreco/Params;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 130
    check-cast p1, Lgreco/Params;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lgreco/Params$Builder;->mergeFrom(Lgreco/Params;)Lgreco/Params$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lgreco/Params;)Lgreco/Params$Builder;
    .locals 1
    .param p1, "other"    # Lgreco/Params;

    .prologue
    .line 195
    invoke-static {}, Lgreco/Params;->getDefaultInstance()Lgreco/Params;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 197
    :goto_0
    return-object p0

    .line 196
    :cond_0
    invoke-virtual {p0, p1}, Lgreco/Params$Builder;->mergeExtensionFields(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;)V

    goto :goto_0
.end method
