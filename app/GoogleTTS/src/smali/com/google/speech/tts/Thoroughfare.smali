.class public final Lcom/google/speech/tts/Thoroughfare;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Thoroughfare.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Thoroughfare$1;,
        Lcom/google/speech/tts/Thoroughfare$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Thoroughfare;


# instance fields
.field private dependentName_:Ljava/lang/String;

.field private fieldOrder_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasDependentName:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasName:Z

.field private hasNumber:Z

.field private hasPremises:Z

.field private hasPreserveOrder:Z

.field private hasSubpremises:Z

.field private memoizedSerializedSize:I

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private name_:Ljava/lang/String;

.field private number_:Ljava/lang/String;

.field private premises_:Ljava/lang/String;

.field private preserveOrder_:Z

.field private subpremises_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 579
    new-instance v0, Lcom/google/speech/tts/Thoroughfare;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Thoroughfare;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Thoroughfare;->defaultInstance:Lcom/google/speech/tts/Thoroughfare;

    .line 580
    invoke-static {}, Lcom/google/speech/tts/AddressProto;->internalForceInit()V

    .line 581
    sget-object v0, Lcom/google/speech/tts/Thoroughfare;->defaultInstance:Lcom/google/speech/tts/Thoroughfare;

    invoke-direct {v0}, Lcom/google/speech/tts/Thoroughfare;->initFields()V

    .line 582
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->number_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->dependentName_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->name_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->premises_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->subpremises_:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/tts/Thoroughfare;->preserveOrder_:Z

    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;

    .line 118
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Thoroughfare;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Thoroughfare;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Thoroughfare$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Thoroughfare$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Thoroughfare;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->number_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->dependentName_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->name_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->premises_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->subpremises_:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/tts/Thoroughfare;->preserveOrder_:Z

    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;

    .line 118
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Thoroughfare;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Thoroughfare;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Thoroughfare;->hasPremises:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/Thoroughfare;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Thoroughfare;->premises_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/Thoroughfare;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Thoroughfare;->hasSubpremises:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/Thoroughfare;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Thoroughfare;->subpremises_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/Thoroughfare;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Thoroughfare;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/Thoroughfare;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Thoroughfare;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/Thoroughfare;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Thoroughfare;->hasPreserveOrder:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/Thoroughfare;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Thoroughfare;->preserveOrder_:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Thoroughfare;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Thoroughfare;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Thoroughfare;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Thoroughfare;->hasNumber:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Thoroughfare;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Thoroughfare;->number_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Thoroughfare;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Thoroughfare;->hasDependentName:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Thoroughfare;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Thoroughfare;->dependentName_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Thoroughfare;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Thoroughfare;->hasName:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Thoroughfare;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Thoroughfare;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Thoroughfare;->name_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Thoroughfare;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Thoroughfare;->defaultInstance:Lcom/google/speech/tts/Thoroughfare;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 1

    .prologue
    .line 232
    # invokes: Lcom/google/speech/tts/Thoroughfare$Builder;->create()Lcom/google/speech/tts/Thoroughfare$Builder;
    invoke-static {}, Lcom/google/speech/tts/Thoroughfare$Builder;->access$100()Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Thoroughfare;

    .prologue
    .line 235
    invoke-static {}, Lcom/google/speech/tts/Thoroughfare;->newBuilder()Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Thoroughfare$Builder;->mergeFrom(Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getDefaultInstanceForType()Lcom/google/speech/tts/Thoroughfare;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Thoroughfare;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Thoroughfare;->defaultInstance:Lcom/google/speech/tts/Thoroughfare;

    return-object v0
.end method

.method public getDependentName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->dependentName_:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldOrderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->number_:Ljava/lang/String;

    return-object v0
.end method

.method public getPremises()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->premises_:Ljava/lang/String;

    return-object v0
.end method

.method public getPreserveOrder()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/speech/tts/Thoroughfare;->preserveOrder_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 120
    iget v3, p0, Lcom/google/speech/tts/Thoroughfare;->memoizedSerializedSize:I

    .line 121
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 162
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 123
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 124
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasNumber()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 125
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getNumber()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 128
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasDependentName()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 129
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getDependentName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 132
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasName()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 133
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 136
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasPremises()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 137
    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getPremises()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 140
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasSubpremises()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 141
    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getSubpremises()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 144
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasMorphosyntacticFeatures()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 145
    const/4 v5, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 148
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasPreserveOrder()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 149
    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getPreserveOrder()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 153
    :cond_7
    const/4 v0, 0x0

    .line 154
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 155
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 157
    goto :goto_1

    .line 158
    .end local v1    # "element":Ljava/lang/String;
    :cond_8
    add-int/2addr v3, v0

    .line 159
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 161
    iput v3, p0, Lcom/google/speech/tts/Thoroughfare;->memoizedSerializedSize:I

    move v4, v3

    .line 162
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getSubpremises()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare;->subpremises_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDependentName()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Thoroughfare;->hasDependentName:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/Thoroughfare;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/Thoroughfare;->hasName:Z

    return v0
.end method

.method public hasNumber()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Thoroughfare;->hasNumber:Z

    return v0
.end method

.method public hasPremises()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/Thoroughfare;->hasPremises:Z

    return v0
.end method

.method public hasPreserveOrder()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/Thoroughfare;->hasPreserveOrder:Z

    return v0
.end method

.method public hasSubpremises()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/Thoroughfare;->hasSubpremises:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->toBuilder()Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 1

    .prologue
    .line 237
    invoke-static {p0}, Lcom/google/speech/tts/Thoroughfare;->newBuilder(Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getSerializedSize()I

    .line 92
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasNumber()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 93
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 95
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasDependentName()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 96
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getDependentName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 98
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasName()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 99
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 101
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasPremises()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 102
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getPremises()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 104
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasSubpremises()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 105
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getSubpremises()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 107
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 108
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 110
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->hasPreserveOrder()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 111
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getPreserveOrder()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 113
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare;->getFieldOrderList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 114
    .local v0, "element":Ljava/lang/String;
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 116
    .end local v0    # "element":Ljava/lang/String;
    :cond_7
    return-void
.end method
