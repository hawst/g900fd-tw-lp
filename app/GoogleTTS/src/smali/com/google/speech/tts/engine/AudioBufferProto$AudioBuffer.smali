.class public final Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AudioBufferProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/AudioBufferProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AudioBuffer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;


# instance fields
.field private hasSampleRate:Z

.field private hasSamples:Z

.field private memoizedSerializedSize:I

.field private sampleRate_:I

.field private samples_:Lcom/google/protobuf/ByteString;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 297
    new-instance v0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->defaultInstance:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    .line 298
    invoke-static {}, Lcom/google/speech/tts/engine/AudioBufferProto;->internalForceInit()V

    .line 299
    sget-object v0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->defaultInstance:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->initFields()V

    .line 300
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->samples_:Lcom/google/protobuf/ByteString;

    .line 38
    const/16 v0, 0x3e80

    iput v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->sampleRate_:I

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->memoizedSerializedSize:I

    .line 15
    invoke-direct {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->initFields()V

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/engine/AudioBufferProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/engine/AudioBufferProto$1;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->samples_:Lcom/google/protobuf/ByteString;

    .line 38
    const/16 v0, 0x3e80

    iput v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->sampleRate_:I

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->memoizedSerializedSize:I

    .line 17
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->hasSamples:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    .param p1, "x1"    # Lcom/google/protobuf/ByteString;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->samples_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->hasSampleRate:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    .param p1, "x1"    # I

    .prologue
    .line 11
    iput p1, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->sampleRate_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->defaultInstance:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
    .locals 1

    .prologue
    .line 144
    # invokes: Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->create()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
    invoke-static {}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->access$100()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    .prologue
    .line 147
    invoke-static {}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->newBuilder()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->mergeFrom(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->getDefaultInstanceForType()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->defaultInstance:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    return-object v0
.end method

.method public getSampleRate()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->sampleRate_:I

    return v0
.end method

.method public getSamples()Lcom/google/protobuf/ByteString;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->samples_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 61
    iget v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->memoizedSerializedSize:I

    .line 62
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 74
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 64
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->hasSamples()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 66
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->getSamples()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 69
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->hasSampleRate()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 70
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->getSampleRate()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 73
    :cond_2
    iput v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->memoizedSerializedSize:I

    move v1, v0

    .line 74
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasSampleRate()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->hasSampleRate:Z

    return v0
.end method

.method public hasSamples()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->hasSamples:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->toBuilder()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
    .locals 1

    .prologue
    .line 149
    invoke-static {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->newBuilder(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->getSerializedSize()I

    .line 51
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->hasSamples()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->getSamples()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->hasSampleRate()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->getSampleRate()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 57
    :cond_1
    return-void
.end method
