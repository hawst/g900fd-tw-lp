.class public final Lcom/google/speech/tts/engine/FstRule$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FstRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/FstRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/engine/FstRule;",
        "Lcom/google/speech/tts/engine/FstRule$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/engine/FstRule;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/engine/FstRule$Builder;
    .locals 1

    .prologue
    .line 160
    invoke-static {}, Lcom/google/speech/tts/engine/FstRule$Builder;->create()Lcom/google/speech/tts/engine/FstRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/engine/FstRule$Builder;
    .locals 3

    .prologue
    .line 169
    new-instance v0, Lcom/google/speech/tts/engine/FstRule$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/FstRule$Builder;-><init>()V

    .line 170
    .local v0, "builder":Lcom/google/speech/tts/engine/FstRule$Builder;
    new-instance v1, Lcom/google/speech/tts/engine/FstRule;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/engine/FstRule;-><init>(Lcom/google/speech/tts/engine/FstRule$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    .line 171
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule$Builder;->build()Lcom/google/speech/tts/engine/FstRule;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/engine/FstRule;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    invoke-static {v0}, Lcom/google/speech/tts/engine/FstRule$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 202
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule$Builder;->buildPartial()Lcom/google/speech/tts/engine/FstRule;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/engine/FstRule;
    .locals 3

    .prologue
    .line 215
    iget-object v1, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    if-nez v1, :cond_0

    .line 216
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    .line 220
    .local v0, "returnMe":Lcom/google/speech/tts/engine/FstRule;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    .line 221
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule$Builder;->clone()Lcom/google/speech/tts/engine/FstRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule$Builder;->clone()Lcom/google/speech/tts/engine/FstRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/engine/FstRule$Builder;
    .locals 2

    .prologue
    .line 188
    invoke-static {}, Lcom/google/speech/tts/engine/FstRule$Builder;->create()Lcom/google/speech/tts/engine/FstRule$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/engine/FstRule$Builder;->mergeFrom(Lcom/google/speech/tts/engine/FstRule;)Lcom/google/speech/tts/engine/FstRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule$Builder;->clone()Lcom/google/speech/tts/engine/FstRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    invoke-virtual {v0}, Lcom/google/speech/tts/engine/FstRule;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 160
    check-cast p1, Lcom/google/speech/tts/engine/FstRule;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/engine/FstRule$Builder;->mergeFrom(Lcom/google/speech/tts/engine/FstRule;)Lcom/google/speech/tts/engine/FstRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/engine/FstRule;)Lcom/google/speech/tts/engine/FstRule$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/engine/FstRule;

    .prologue
    .line 225
    invoke-static {}, Lcom/google/speech/tts/engine/FstRule;->getDefaultInstance()Lcom/google/speech/tts/engine/FstRule;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-object p0

    .line 226
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/FstRule;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 227
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/FstRule;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/FstRule$Builder;->setName(Ljava/lang/String;)Lcom/google/speech/tts/engine/FstRule$Builder;

    .line 229
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/FstRule;->hasParens()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 230
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/FstRule;->getParens()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/FstRule$Builder;->setParens(Ljava/lang/String;)Lcom/google/speech/tts/engine/FstRule$Builder;

    .line 232
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/FstRule;->hasRedup()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/FstRule;->getRedup()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/FstRule$Builder;->setRedup(Ljava/lang/String;)Lcom/google/speech/tts/engine/FstRule$Builder;

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/tts/engine/FstRule$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 278
    if-nez p1, :cond_0

    .line 279
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/FstRule;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/FstRule;->access$302(Lcom/google/speech/tts/engine/FstRule;Z)Z

    .line 282
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    # setter for: Lcom/google/speech/tts/engine/FstRule;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/FstRule;->access$402(Lcom/google/speech/tts/engine/FstRule;Ljava/lang/String;)Ljava/lang/String;

    .line 283
    return-object p0
.end method

.method public setParens(Ljava/lang/String;)Lcom/google/speech/tts/engine/FstRule$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 299
    if-nez p1, :cond_0

    .line 300
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/FstRule;->hasParens:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/FstRule;->access$502(Lcom/google/speech/tts/engine/FstRule;Z)Z

    .line 303
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    # setter for: Lcom/google/speech/tts/engine/FstRule;->parens_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/FstRule;->access$602(Lcom/google/speech/tts/engine/FstRule;Ljava/lang/String;)Ljava/lang/String;

    .line 304
    return-object p0
.end method

.method public setRedup(Ljava/lang/String;)Lcom/google/speech/tts/engine/FstRule$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 320
    if-nez p1, :cond_0

    .line 321
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/FstRule;->hasRedup:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/FstRule;->access$702(Lcom/google/speech/tts/engine/FstRule;Z)Z

    .line 324
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule$Builder;->result:Lcom/google/speech/tts/engine/FstRule;

    # setter for: Lcom/google/speech/tts/engine/FstRule;->redup_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/FstRule;->access$802(Lcom/google/speech/tts/engine/FstRule;Ljava/lang/String;)Ljava/lang/String;

    .line 325
    return-object p0
.end method
