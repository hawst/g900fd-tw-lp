.class public final Lcom/google/speech/tts/Thoroughfare$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Thoroughfare.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Thoroughfare;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Thoroughfare;",
        "Lcom/google/speech/tts/Thoroughfare$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Thoroughfare;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 245
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 1

    .prologue
    .line 239
    invoke-static {}, Lcom/google/speech/tts/Thoroughfare$Builder;->create()Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 3

    .prologue
    .line 248
    new-instance v0, Lcom/google/speech/tts/Thoroughfare$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Thoroughfare$Builder;-><init>()V

    .line 249
    .local v0, "builder":Lcom/google/speech/tts/Thoroughfare$Builder;
    new-instance v1, Lcom/google/speech/tts/Thoroughfare;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Thoroughfare;-><init>(Lcom/google/speech/tts/Thoroughfare$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    .line 250
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare$Builder;->build()Lcom/google/speech/tts/Thoroughfare;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Thoroughfare;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    invoke-static {v0}, Lcom/google/speech/tts/Thoroughfare$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 281
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare$Builder;->buildPartial()Lcom/google/speech/tts/Thoroughfare;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Thoroughfare;
    .locals 3

    .prologue
    .line 294
    iget-object v1, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    if-nez v1, :cond_0

    .line 295
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 298
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    # getter for: Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Thoroughfare;->access$300(Lcom/google/speech/tts/Thoroughfare;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 299
    iget-object v1, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    iget-object v2, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    # getter for: Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Thoroughfare;->access$300(Lcom/google/speech/tts/Thoroughfare;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Thoroughfare;->access$302(Lcom/google/speech/tts/Thoroughfare;Ljava/util/List;)Ljava/util/List;

    .line 302
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    .line 303
    .local v0, "returnMe":Lcom/google/speech/tts/Thoroughfare;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    .line 304
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare$Builder;->clone()Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare$Builder;->clone()Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 2

    .prologue
    .line 267
    invoke-static {}, Lcom/google/speech/tts/Thoroughfare$Builder;->create()Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Thoroughfare$Builder;->mergeFrom(Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/google/speech/tts/Thoroughfare$Builder;->clone()Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    invoke-virtual {v0}, Lcom/google/speech/tts/Thoroughfare;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 239
    check-cast p1, Lcom/google/speech/tts/Thoroughfare;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Thoroughfare$Builder;->mergeFrom(Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Thoroughfare;

    .prologue
    .line 308
    invoke-static {}, Lcom/google/speech/tts/Thoroughfare;->getDefaultInstance()Lcom/google/speech/tts/Thoroughfare;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 336
    :cond_0
    :goto_0
    return-object p0

    .line 309
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->hasNumber()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 310
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->getNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Thoroughfare$Builder;->setNumber(Ljava/lang/String;)Lcom/google/speech/tts/Thoroughfare$Builder;

    .line 312
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->hasDependentName()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 313
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->getDependentName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Thoroughfare$Builder;->setDependentName(Ljava/lang/String;)Lcom/google/speech/tts/Thoroughfare$Builder;

    .line 315
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->hasName()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 316
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Thoroughfare$Builder;->setName(Ljava/lang/String;)Lcom/google/speech/tts/Thoroughfare$Builder;

    .line 318
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->hasPremises()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 319
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->getPremises()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Thoroughfare$Builder;->setPremises(Ljava/lang/String;)Lcom/google/speech/tts/Thoroughfare$Builder;

    .line 321
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->hasSubpremises()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 322
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->getSubpremises()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Thoroughfare$Builder;->setSubpremises(Ljava/lang/String;)Lcom/google/speech/tts/Thoroughfare$Builder;

    .line 324
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 325
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Thoroughfare$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Thoroughfare$Builder;

    .line 327
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->hasPreserveOrder()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 328
    invoke-virtual {p1}, Lcom/google/speech/tts/Thoroughfare;->getPreserveOrder()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Thoroughfare$Builder;->setPreserveOrder(Z)Lcom/google/speech/tts/Thoroughfare$Builder;

    .line 330
    :cond_8
    # getter for: Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Thoroughfare;->access$300(Lcom/google/speech/tts/Thoroughfare;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    # getter for: Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Thoroughfare;->access$300(Lcom/google/speech/tts/Thoroughfare;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 332
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Thoroughfare;->access$302(Lcom/google/speech/tts/Thoroughfare;Ljava/util/List;)Ljava/util/List;

    .line 334
    :cond_9
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    # getter for: Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Thoroughfare;->access$300(Lcom/google/speech/tts/Thoroughfare;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Thoroughfare;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Thoroughfare;->access$300(Lcom/google/speech/tts/Thoroughfare;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setDependentName(Ljava/lang/String;)Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 420
    if-nez p1, :cond_0

    .line 421
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Thoroughfare;->hasDependentName:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Thoroughfare;->access$602(Lcom/google/speech/tts/Thoroughfare;Z)Z

    .line 424
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    # setter for: Lcom/google/speech/tts/Thoroughfare;->dependentName_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Thoroughfare;->access$702(Lcom/google/speech/tts/Thoroughfare;Ljava/lang/String;)Ljava/lang/String;

    .line 425
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 504
    if-nez p1, :cond_0

    .line 505
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Thoroughfare;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Thoroughfare;->access$1402(Lcom/google/speech/tts/Thoroughfare;Z)Z

    .line 508
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    # setter for: Lcom/google/speech/tts/Thoroughfare;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Thoroughfare;->access$1502(Lcom/google/speech/tts/Thoroughfare;Ljava/lang/String;)Ljava/lang/String;

    .line 509
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 441
    if-nez p1, :cond_0

    .line 442
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Thoroughfare;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Thoroughfare;->access$802(Lcom/google/speech/tts/Thoroughfare;Z)Z

    .line 445
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    # setter for: Lcom/google/speech/tts/Thoroughfare;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Thoroughfare;->access$902(Lcom/google/speech/tts/Thoroughfare;Ljava/lang/String;)Ljava/lang/String;

    .line 446
    return-object p0
.end method

.method public setNumber(Ljava/lang/String;)Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 399
    if-nez p1, :cond_0

    .line 400
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 402
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Thoroughfare;->hasNumber:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Thoroughfare;->access$402(Lcom/google/speech/tts/Thoroughfare;Z)Z

    .line 403
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    # setter for: Lcom/google/speech/tts/Thoroughfare;->number_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Thoroughfare;->access$502(Lcom/google/speech/tts/Thoroughfare;Ljava/lang/String;)Ljava/lang/String;

    .line 404
    return-object p0
.end method

.method public setPremises(Ljava/lang/String;)Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 462
    if-nez p1, :cond_0

    .line 463
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 465
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Thoroughfare;->hasPremises:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Thoroughfare;->access$1002(Lcom/google/speech/tts/Thoroughfare;Z)Z

    .line 466
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    # setter for: Lcom/google/speech/tts/Thoroughfare;->premises_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Thoroughfare;->access$1102(Lcom/google/speech/tts/Thoroughfare;Ljava/lang/String;)Ljava/lang/String;

    .line 467
    return-object p0
.end method

.method public setPreserveOrder(Z)Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Thoroughfare;->hasPreserveOrder:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Thoroughfare;->access$1602(Lcom/google/speech/tts/Thoroughfare;Z)Z

    .line 526
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    # setter for: Lcom/google/speech/tts/Thoroughfare;->preserveOrder_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Thoroughfare;->access$1702(Lcom/google/speech/tts/Thoroughfare;Z)Z

    .line 527
    return-object p0
.end method

.method public setSubpremises(Ljava/lang/String;)Lcom/google/speech/tts/Thoroughfare$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 483
    if-nez p1, :cond_0

    .line 484
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Thoroughfare;->hasSubpremises:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Thoroughfare;->access$1202(Lcom/google/speech/tts/Thoroughfare;Z)Z

    .line 487
    iget-object v0, p0, Lcom/google/speech/tts/Thoroughfare$Builder;->result:Lcom/google/speech/tts/Thoroughfare;

    # setter for: Lcom/google/speech/tts/Thoroughfare;->subpremises_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Thoroughfare;->access$1302(Lcom/google/speech/tts/Thoroughfare;Ljava/lang/String;)Ljava/lang/String;

    .line 488
    return-object p0
.end method
