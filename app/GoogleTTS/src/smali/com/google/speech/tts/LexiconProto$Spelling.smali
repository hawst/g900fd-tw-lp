.class public final Lcom/google/speech/tts/LexiconProto$Spelling;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Spelling"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/LexiconProto$Spelling;


# instance fields
.field private hasId:Z

.field private hasV:Z

.field private id_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private v_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 612
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Spelling;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/LexiconProto$Spelling;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Spelling;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Spelling;

    .line 613
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 614
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Spelling;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Spelling;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Spelling;->initFields()V

    .line 615
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 324
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 341
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->id_:Ljava/lang/String;

    .line 348
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->v_:Ljava/lang/String;

    .line 371
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->memoizedSerializedSize:I

    .line 325
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->initFields()V

    .line 326
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/LexiconProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/LexiconProto$1;

    .prologue
    .line 321
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 327
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 341
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->id_:Ljava/lang/String;

    .line 348
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->v_:Ljava/lang/String;

    .line 371
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->memoizedSerializedSize:I

    .line 327
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/LexiconProto$Spelling;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Spelling;
    .param p1, "x1"    # Z

    .prologue
    .line 321
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->hasId:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/LexiconProto$Spelling;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Spelling;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 321
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->id_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/LexiconProto$Spelling;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Spelling;
    .param p1, "x1"    # Z

    .prologue
    .line 321
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->hasV:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/LexiconProto$Spelling;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Spelling;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 321
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->v_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/LexiconProto$Spelling;
    .locals 1

    .prologue
    .line 331
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Spelling;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Spelling;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 353
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
    .locals 1

    .prologue
    .line 456
    # invokes: Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->create()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->access$800()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/LexiconProto$Spelling;)Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/LexiconProto$Spelling;

    .prologue
    .line 459
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Spelling;->newBuilder()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Spelling;)Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 321
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto$Spelling;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto$Spelling;
    .locals 1

    .prologue
    .line 335
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Spelling;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Spelling;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 373
    iget v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->memoizedSerializedSize:I

    .line 374
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 386
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 376
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 377
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->hasId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 378
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 381
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->hasV()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 382
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->getV()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 385
    :cond_2
    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->memoizedSerializedSize:I

    move v1, v0

    .line 386
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->v_:Ljava/lang/String;

    return-object v0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 342
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->hasId:Z

    return v0
.end method

.method public hasV()Z
    .locals 1

    .prologue
    .line 349
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->hasV:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 355
    iget-boolean v1, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->hasId:Z

    if-nez v1, :cond_1

    .line 357
    :cond_0
    :goto_0
    return v0

    .line 356
    :cond_1
    iget-boolean v1, p0, Lcom/google/speech/tts/LexiconProto$Spelling;->hasV:Z

    if-eqz v1, :cond_0

    .line 357
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 321
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->toBuilder()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
    .locals 1

    .prologue
    .line 461
    invoke-static {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->newBuilder(Lcom/google/speech/tts/LexiconProto$Spelling;)Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 362
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->getSerializedSize()I

    .line 363
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 366
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->hasV()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling;->getV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 369
    :cond_1
    return-void
.end method
