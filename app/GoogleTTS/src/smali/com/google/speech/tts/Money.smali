.class public final Lcom/google/speech/tts/Money;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Money.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Money$1;,
        Lcom/google/speech/tts/Money$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Money;


# instance fields
.field private amount_:Lcom/google/speech/tts/Decimal;

.field private codeSwitch_:Ljava/lang/String;

.field private currency_:Ljava/lang/String;

.field private fieldOrder_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasAmount:Z

.field private hasCodeSwitch:Z

.field private hasCurrency:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasPreserveOrder:Z

.field private hasQuantity:Z

.field private hasStyle:Z

.field private memoizedSerializedSize:I

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private preserveOrder_:Z

.field private quantity_:J

.field private style_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 597
    new-instance v0, Lcom/google/speech/tts/Money;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Money;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Money;->defaultInstance:Lcom/google/speech/tts/Money;

    .line 598
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 599
    sget-object v0, Lcom/google/speech/tts/Money;->defaultInstance:Lcom/google/speech/tts/Money;

    invoke-direct {v0}, Lcom/google/speech/tts/Money;->initFields()V

    .line 600
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/tts/Money;->quantity_:J

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Money;->currency_:Ljava/lang/String;

    .line 46
    iput v2, p0, Lcom/google/speech/tts/Money;->style_:I

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Money;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 60
    iput-boolean v2, p0, Lcom/google/speech/tts/Money;->preserveOrder_:Z

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Money;->codeSwitch_:Ljava/lang/String;

    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;

    .line 121
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Money;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Money;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Money$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Money$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Money;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3
    .param p1, "noInit"    # Z

    .prologue
    const/4 v2, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/tts/Money;->quantity_:J

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Money;->currency_:Ljava/lang/String;

    .line 46
    iput v2, p0, Lcom/google/speech/tts/Money;->style_:I

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Money;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 60
    iput-boolean v2, p0, Lcom/google/speech/tts/Money;->preserveOrder_:Z

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Money;->codeSwitch_:Ljava/lang/String;

    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;

    .line 121
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Money;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Money;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Money;->hasStyle:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/Money;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Money;->style_:I

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/Money;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Money;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/Money;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Money;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/Money;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Money;->hasPreserveOrder:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/Money;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Money;->preserveOrder_:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/Money;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Money;->hasCodeSwitch:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/Money;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Money;->codeSwitch_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Money;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Money;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Money;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Money;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Money;->hasAmount:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Decimal;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Money;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Money;->amount_:Lcom/google/speech/tts/Decimal;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Money;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Money;->amount_:Lcom/google/speech/tts/Decimal;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Money;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Money;->hasQuantity:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Money;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # J

    .prologue
    .line 5
    iput-wide p1, p0, Lcom/google/speech/tts/Money;->quantity_:J

    return-wide p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Money;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Money;->hasCurrency:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Money;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Money;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Money;->currency_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Money;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Money;->defaultInstance:Lcom/google/speech/tts/Money;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 84
    invoke-static {}, Lcom/google/speech/tts/Decimal;->getDefaultInstance()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Money;->amount_:Lcom/google/speech/tts/Decimal;

    .line 85
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Money$Builder;
    .locals 1

    .prologue
    .line 235
    # invokes: Lcom/google/speech/tts/Money$Builder;->create()Lcom/google/speech/tts/Money$Builder;
    invoke-static {}, Lcom/google/speech/tts/Money$Builder;->access$100()Lcom/google/speech/tts/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Money;

    .prologue
    .line 238
    invoke-static {}, Lcom/google/speech/tts/Money;->newBuilder()Lcom/google/speech/tts/Money$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Money$Builder;->mergeFrom(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAmount()Lcom/google/speech/tts/Decimal;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Money;->amount_:Lcom/google/speech/tts/Decimal;

    return-object v0
.end method

.method public getCodeSwitch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/speech/tts/Money;->codeSwitch_:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/Money;->currency_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getDefaultInstanceForType()Lcom/google/speech/tts/Money;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Money;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Money;->defaultInstance:Lcom/google/speech/tts/Money;

    return-object v0
.end method

.method public getFieldOrderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/tts/Money;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getPreserveOrder()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/speech/tts/Money;->preserveOrder_:Z

    return v0
.end method

.method public getQuantity()J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/google/speech/tts/Money;->quantity_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    .line 123
    iget v3, p0, Lcom/google/speech/tts/Money;->memoizedSerializedSize:I

    .line 124
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 165
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 126
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 127
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasAmount()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 128
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getAmount()Lcom/google/speech/tts/Decimal;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 131
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasQuantity()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 132
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getQuantity()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v3, v5

    .line 135
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasCurrency()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 136
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getCurrency()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 139
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasStyle()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 140
    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getStyle()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 143
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasMorphosyntacticFeatures()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 144
    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 147
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasPreserveOrder()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 148
    const/4 v5, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getPreserveOrder()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 151
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasCodeSwitch()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 152
    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getCodeSwitch()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 156
    :cond_7
    const/4 v0, 0x0

    .line 157
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 158
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 160
    goto :goto_1

    .line 161
    .end local v1    # "element":Ljava/lang/String;
    :cond_8
    add-int/2addr v3, v0

    .line 162
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 164
    iput v3, p0, Lcom/google/speech/tts/Money;->memoizedSerializedSize:I

    move v4, v3

    .line 165
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getStyle()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/speech/tts/Money;->style_:I

    return v0
.end method

.method public hasAmount()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Money;->hasAmount:Z

    return v0
.end method

.method public hasCodeSwitch()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/Money;->hasCodeSwitch:Z

    return v0
.end method

.method public hasCurrency()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/Money;->hasCurrency:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/Money;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasPreserveOrder()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/Money;->hasPreserveOrder:Z

    return v0
.end method

.method public hasQuantity()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Money;->hasQuantity:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/Money;->hasStyle:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 87
    iget-boolean v1, p0, Lcom/google/speech/tts/Money;->hasAmount:Z

    if-nez v1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v0

    .line 88
    :cond_1
    iget-boolean v1, p0, Lcom/google/speech/tts/Money;->hasCurrency:Z

    if-eqz v1, :cond_0

    .line 89
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->toBuilder()Lcom/google/speech/tts/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Money$Builder;
    .locals 1

    .prologue
    .line 240
    invoke-static {p0}, Lcom/google/speech/tts/Money;->newBuilder(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getSerializedSize()I

    .line 95
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasAmount()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 96
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getAmount()Lcom/google/speech/tts/Decimal;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 98
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasQuantity()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 99
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getQuantity()J

    move-result-wide v4

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 101
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasCurrency()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 102
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getCurrency()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 104
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 105
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getStyle()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 107
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 108
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 110
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasPreserveOrder()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 111
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getPreserveOrder()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 113
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->hasCodeSwitch()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 114
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getCodeSwitch()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 116
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Money;->getFieldOrderList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 117
    .local v0, "element":Ljava/lang/String;
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 119
    .end local v0    # "element":Ljava/lang/String;
    :cond_7
    return-void
.end method
