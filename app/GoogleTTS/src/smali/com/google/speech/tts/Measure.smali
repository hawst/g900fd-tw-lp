.class public final Lcom/google/speech/tts/Measure;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Measure.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Measure$1;,
        Lcom/google/speech/tts/Measure$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Measure;


# instance fields
.field private cardinal_:Lcom/google/speech/tts/Cardinal;

.field private codeSwitch_:Ljava/lang/String;

.field private decimal_:Lcom/google/speech/tts/Decimal;

.field private fieldOrder_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private fraction_:Lcom/google/speech/tts/Fraction;

.field private hasCardinal:Z

.field private hasCodeSwitch:Z

.field private hasDecimal:Z

.field private hasFraction:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasPreserveOrder:Z

.field private hasStyle:Z

.field private hasUnits:Z

.field private memoizedSerializedSize:I

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private preserveOrder_:Z

.field private style_:I

.field private units_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 690
    new-instance v0, Lcom/google/speech/tts/Measure;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Measure;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Measure;->defaultInstance:Lcom/google/speech/tts/Measure;

    .line 691
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 692
    sget-object v0, Lcom/google/speech/tts/Measure;->defaultInstance:Lcom/google/speech/tts/Measure;

    invoke-direct {v0}, Lcom/google/speech/tts/Measure;->initFields()V

    .line 693
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Measure;->units_:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/google/speech/tts/Measure;->style_:I

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Measure;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 67
    iput-boolean v1, p0, Lcom/google/speech/tts/Measure;->preserveOrder_:Z

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Measure;->codeSwitch_:Ljava/lang/String;

    .line 80
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;

    .line 137
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Measure;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Measure;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Measure$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Measure$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Measure;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Measure;->units_:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/google/speech/tts/Measure;->style_:I

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Measure;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 67
    iput-boolean v1, p0, Lcom/google/speech/tts/Measure;->preserveOrder_:Z

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Measure;->codeSwitch_:Ljava/lang/String;

    .line 80
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;

    .line 137
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Measure;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Measure;->hasUnits:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/Measure;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Measure;->units_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Measure;->hasStyle:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/Measure;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Measure;->style_:I

    return p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Measure;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/Measure;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Measure;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Measure;->hasPreserveOrder:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Measure;->preserveOrder_:Z

    return p1
.end method

.method static synthetic access$1802(Lcom/google/speech/tts/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Measure;->hasCodeSwitch:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/google/speech/tts/Measure;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Measure;->codeSwitch_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Measure;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Measure;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Measure;->hasDecimal:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Decimal;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Measure;->decimal_:Lcom/google/speech/tts/Decimal;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Measure;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Measure;->decimal_:Lcom/google/speech/tts/Decimal;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Measure;->hasFraction:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Fraction;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Measure;->fraction_:Lcom/google/speech/tts/Fraction;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Measure;Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Lcom/google/speech/tts/Fraction;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Measure;->fraction_:Lcom/google/speech/tts/Fraction;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Measure;->hasCardinal:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Cardinal;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Measure;->cardinal_:Lcom/google/speech/tts/Cardinal;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Measure;Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Measure;
    .param p1, "x1"    # Lcom/google/speech/tts/Cardinal;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Measure;->cardinal_:Lcom/google/speech/tts/Cardinal;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Measure;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Measure;->defaultInstance:Lcom/google/speech/tts/Measure;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Lcom/google/speech/tts/Decimal;->getDefaultInstance()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Measure;->decimal_:Lcom/google/speech/tts/Decimal;

    .line 92
    invoke-static {}, Lcom/google/speech/tts/Fraction;->getDefaultInstance()Lcom/google/speech/tts/Fraction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Measure;->fraction_:Lcom/google/speech/tts/Fraction;

    .line 93
    invoke-static {}, Lcom/google/speech/tts/Cardinal;->getDefaultInstance()Lcom/google/speech/tts/Cardinal;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Measure;->cardinal_:Lcom/google/speech/tts/Cardinal;

    .line 94
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Measure$Builder;
    .locals 1

    .prologue
    .line 255
    # invokes: Lcom/google/speech/tts/Measure$Builder;->create()Lcom/google/speech/tts/Measure$Builder;
    invoke-static {}, Lcom/google/speech/tts/Measure$Builder;->access$100()Lcom/google/speech/tts/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Measure;

    .prologue
    .line 258
    invoke-static {}, Lcom/google/speech/tts/Measure;->newBuilder()Lcom/google/speech/tts/Measure$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Measure$Builder;->mergeFrom(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCardinal()Lcom/google/speech/tts/Cardinal;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/Measure;->cardinal_:Lcom/google/speech/tts/Cardinal;

    return-object v0
.end method

.method public getCodeSwitch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/speech/tts/Measure;->codeSwitch_:Ljava/lang/String;

    return-object v0
.end method

.method public getDecimal()Lcom/google/speech/tts/Decimal;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Measure;->decimal_:Lcom/google/speech/tts/Decimal;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getDefaultInstanceForType()Lcom/google/speech/tts/Measure;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Measure;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Measure;->defaultInstance:Lcom/google/speech/tts/Measure;

    return-object v0
.end method

.method public getFieldOrderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method public getFraction()Lcom/google/speech/tts/Fraction;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/Measure;->fraction_:Lcom/google/speech/tts/Fraction;

    return-object v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/speech/tts/Measure;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getPreserveOrder()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/speech/tts/Measure;->preserveOrder_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 139
    iget v3, p0, Lcom/google/speech/tts/Measure;->memoizedSerializedSize:I

    .line 140
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 185
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 142
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 143
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasDecimal()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 144
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getDecimal()Lcom/google/speech/tts/Decimal;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 147
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasFraction()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 148
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getFraction()Lcom/google/speech/tts/Fraction;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 151
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasCardinal()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 152
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getCardinal()Lcom/google/speech/tts/Cardinal;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 155
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasUnits()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 156
    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getUnits()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 159
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasStyle()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 160
    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getStyle()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 163
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasMorphosyntacticFeatures()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 164
    const/4 v5, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 167
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasPreserveOrder()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 168
    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getPreserveOrder()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 171
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasCodeSwitch()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 172
    const/16 v5, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getCodeSwitch()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 176
    :cond_8
    const/4 v0, 0x0

    .line 177
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 178
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 180
    goto :goto_1

    .line 181
    .end local v1    # "element":Ljava/lang/String;
    :cond_9
    add-int/2addr v3, v0

    .line 182
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 184
    iput v3, p0, Lcom/google/speech/tts/Measure;->memoizedSerializedSize:I

    move v4, v3

    .line 185
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getStyle()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/speech/tts/Measure;->style_:I

    return v0
.end method

.method public getUnits()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/tts/Measure;->units_:Ljava/lang/String;

    return-object v0
.end method

.method public hasCardinal()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/Measure;->hasCardinal:Z

    return v0
.end method

.method public hasCodeSwitch()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/speech/tts/Measure;->hasCodeSwitch:Z

    return v0
.end method

.method public hasDecimal()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Measure;->hasDecimal:Z

    return v0
.end method

.method public hasFraction()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Measure;->hasFraction:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/Measure;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasPreserveOrder()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/Measure;->hasPreserveOrder:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/Measure;->hasStyle:Z

    return v0
.end method

.method public hasUnits()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/Measure;->hasUnits:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 96
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasFraction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 97
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getFraction()Lcom/google/speech/tts/Fraction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Fraction;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v0

    .line 99
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasCardinal()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 100
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getCardinal()Lcom/google/speech/tts/Cardinal;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Cardinal;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->toBuilder()Lcom/google/speech/tts/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Measure$Builder;
    .locals 1

    .prologue
    .line 260
    invoke-static {p0}, Lcom/google/speech/tts/Measure;->newBuilder(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getSerializedSize()I

    .line 108
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasDecimal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getDecimal()Lcom/google/speech/tts/Decimal;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasFraction()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 112
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getFraction()Lcom/google/speech/tts/Fraction;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 114
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasCardinal()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 115
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getCardinal()Lcom/google/speech/tts/Cardinal;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 117
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasUnits()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 118
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getUnits()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 120
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 121
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getStyle()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 123
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 124
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 126
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasPreserveOrder()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 127
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getPreserveOrder()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 129
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->hasCodeSwitch()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 130
    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getCodeSwitch()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 132
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure;->getFieldOrderList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 133
    .local v0, "element":Ljava/lang/String;
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 135
    .end local v0    # "element":Ljava/lang/String;
    :cond_8
    return-void
.end method
