.class public final Lcom/google/speech/tts/MarkupWord;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MarkupWord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/MarkupWord$1;,
        Lcom/google/speech/tts/MarkupWord$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/MarkupWord;


# instance fields
.field private hasId:Z

.field private hasVariant:Z

.field private id_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private variant_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 295
    new-instance v0, Lcom/google/speech/tts/MarkupWord;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/MarkupWord;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/MarkupWord;->defaultInstance:Lcom/google/speech/tts/MarkupWord;

    .line 296
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 297
    sget-object v0, Lcom/google/speech/tts/MarkupWord;->defaultInstance:Lcom/google/speech/tts/MarkupWord;

    invoke-direct {v0}, Lcom/google/speech/tts/MarkupWord;->initFields()V

    .line 298
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/MarkupWord;->id_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/MarkupWord;->variant_:Ljava/lang/String;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/MarkupWord;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/MarkupWord;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/MarkupWord$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/MarkupWord$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/MarkupWord;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/MarkupWord;->id_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/MarkupWord;->variant_:Ljava/lang/String;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/MarkupWord;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/tts/MarkupWord;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/MarkupWord;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/MarkupWord;->hasId:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/MarkupWord;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/MarkupWord;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/MarkupWord;->id_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/MarkupWord;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/MarkupWord;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/MarkupWord;->hasVariant:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/MarkupWord;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/MarkupWord;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/MarkupWord;->variant_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/MarkupWord;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/MarkupWord;->defaultInstance:Lcom/google/speech/tts/MarkupWord;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/MarkupWord$Builder;
    .locals 1

    .prologue
    .line 139
    # invokes: Lcom/google/speech/tts/MarkupWord$Builder;->create()Lcom/google/speech/tts/MarkupWord$Builder;
    invoke-static {}, Lcom/google/speech/tts/MarkupWord$Builder;->access$100()Lcom/google/speech/tts/MarkupWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/MarkupWord;)Lcom/google/speech/tts/MarkupWord$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/MarkupWord;

    .prologue
    .line 142
    invoke-static {}, Lcom/google/speech/tts/MarkupWord;->newBuilder()Lcom/google/speech/tts/MarkupWord$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/MarkupWord$Builder;->mergeFrom(Lcom/google/speech/tts/MarkupWord;)Lcom/google/speech/tts/MarkupWord$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/MarkupWord;->getDefaultInstanceForType()Lcom/google/speech/tts/MarkupWord;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/MarkupWord;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/MarkupWord;->defaultInstance:Lcom/google/speech/tts/MarkupWord;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/MarkupWord;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 56
    iget v0, p0, Lcom/google/speech/tts/MarkupWord;->memoizedSerializedSize:I

    .line 57
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 69
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 59
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/google/speech/tts/MarkupWord;->hasId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 61
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/MarkupWord;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 64
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/MarkupWord;->hasVariant()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 65
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/MarkupWord;->getVariant()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 68
    :cond_2
    iput v0, p0, Lcom/google/speech/tts/MarkupWord;->memoizedSerializedSize:I

    move v1, v0

    .line 69
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getVariant()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/MarkupWord;->variant_:Ljava/lang/String;

    return-object v0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/MarkupWord;->hasId:Z

    return v0
.end method

.method public hasVariant()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/MarkupWord;->hasVariant:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/speech/tts/MarkupWord;->hasId:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 40
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/MarkupWord;->toBuilder()Lcom/google/speech/tts/MarkupWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/MarkupWord$Builder;
    .locals 1

    .prologue
    .line 144
    invoke-static {p0}, Lcom/google/speech/tts/MarkupWord;->newBuilder(Lcom/google/speech/tts/MarkupWord;)Lcom/google/speech/tts/MarkupWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/speech/tts/MarkupWord;->getSerializedSize()I

    .line 46
    invoke-virtual {p0}, Lcom/google/speech/tts/MarkupWord;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/MarkupWord;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 49
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/MarkupWord;->hasVariant()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/MarkupWord;->getVariant()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 52
    :cond_1
    return-void
.end method
