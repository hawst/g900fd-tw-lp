.class public final Lcom/google/speech/tts/VoiceModPair$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "VoiceModPair.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/VoiceModPair;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/VoiceModPair;",
        "Lcom/google/speech/tts/VoiceModPair$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/VoiceModPair;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/VoiceModPair$Builder;
    .locals 1

    .prologue
    .line 161
    invoke-static {}, Lcom/google/speech/tts/VoiceModPair$Builder;->create()Lcom/google/speech/tts/VoiceModPair$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/VoiceModPair$Builder;
    .locals 3

    .prologue
    .line 170
    new-instance v0, Lcom/google/speech/tts/VoiceModPair$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/VoiceModPair$Builder;-><init>()V

    .line 171
    .local v0, "builder":Lcom/google/speech/tts/VoiceModPair$Builder;
    new-instance v1, Lcom/google/speech/tts/VoiceModPair;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/VoiceModPair;-><init>(Lcom/google/speech/tts/VoiceModPair$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    .line 172
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair$Builder;->build()Lcom/google/speech/tts/VoiceModPair;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/VoiceModPair;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    invoke-static {v0}, Lcom/google/speech/tts/VoiceModPair$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 203
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair$Builder;->buildPartial()Lcom/google/speech/tts/VoiceModPair;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/VoiceModPair;
    .locals 3

    .prologue
    .line 216
    iget-object v1, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    if-nez v1, :cond_0

    .line 217
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    .line 221
    .local v0, "returnMe":Lcom/google/speech/tts/VoiceModPair;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    .line 222
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair$Builder;->clone()Lcom/google/speech/tts/VoiceModPair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair$Builder;->clone()Lcom/google/speech/tts/VoiceModPair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/VoiceModPair$Builder;
    .locals 2

    .prologue
    .line 189
    invoke-static {}, Lcom/google/speech/tts/VoiceModPair$Builder;->create()Lcom/google/speech/tts/VoiceModPair$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/VoiceModPair$Builder;->mergeFrom(Lcom/google/speech/tts/VoiceModPair;)Lcom/google/speech/tts/VoiceModPair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair$Builder;->clone()Lcom/google/speech/tts/VoiceModPair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    invoke-virtual {v0}, Lcom/google/speech/tts/VoiceModPair;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 161
    check-cast p1, Lcom/google/speech/tts/VoiceModPair;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/VoiceModPair$Builder;->mergeFrom(Lcom/google/speech/tts/VoiceModPair;)Lcom/google/speech/tts/VoiceModPair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/VoiceModPair;)Lcom/google/speech/tts/VoiceModPair$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/VoiceModPair;

    .prologue
    .line 226
    invoke-static {}, Lcom/google/speech/tts/VoiceModPair;->getDefaultInstance()Lcom/google/speech/tts/VoiceModPair;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-object p0

    .line 227
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/VoiceModPair;->hasT()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 228
    invoke-virtual {p1}, Lcom/google/speech/tts/VoiceModPair;->getT()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/VoiceModPair$Builder;->setT(F)Lcom/google/speech/tts/VoiceModPair$Builder;

    .line 230
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/VoiceModPair;->hasV()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 231
    invoke-virtual {p1}, Lcom/google/speech/tts/VoiceModPair;->getV()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/VoiceModPair$Builder;->setV(F)Lcom/google/speech/tts/VoiceModPair$Builder;

    .line 233
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/VoiceModPair;->hasDeviation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-virtual {p1}, Lcom/google/speech/tts/VoiceModPair;->getDeviation()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/VoiceModPair$Builder;->setDeviation(F)Lcom/google/speech/tts/VoiceModPair$Builder;

    goto :goto_0
.end method

.method public setDeviation(F)Lcom/google/speech/tts/VoiceModPair$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/VoiceModPair;->hasDeviation:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/VoiceModPair;->access$702(Lcom/google/speech/tts/VoiceModPair;Z)Z

    .line 316
    iget-object v0, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    # setter for: Lcom/google/speech/tts/VoiceModPair;->deviation_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/VoiceModPair;->access$802(Lcom/google/speech/tts/VoiceModPair;F)F

    .line 317
    return-object p0
.end method

.method public setT(F)Lcom/google/speech/tts/VoiceModPair$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/VoiceModPair;->hasT:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/VoiceModPair;->access$302(Lcom/google/speech/tts/VoiceModPair;Z)Z

    .line 280
    iget-object v0, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    # setter for: Lcom/google/speech/tts/VoiceModPair;->t_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/VoiceModPair;->access$402(Lcom/google/speech/tts/VoiceModPair;F)F

    .line 281
    return-object p0
.end method

.method public setV(F)Lcom/google/speech/tts/VoiceModPair$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/VoiceModPair;->hasV:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/VoiceModPair;->access$502(Lcom/google/speech/tts/VoiceModPair;Z)Z

    .line 298
    iget-object v0, p0, Lcom/google/speech/tts/VoiceModPair$Builder;->result:Lcom/google/speech/tts/VoiceModPair;

    # setter for: Lcom/google/speech/tts/VoiceModPair;->v_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/VoiceModPair;->access$602(Lcom/google/speech/tts/VoiceModPair;F)F

    .line 299
    return-object p0
.end method
