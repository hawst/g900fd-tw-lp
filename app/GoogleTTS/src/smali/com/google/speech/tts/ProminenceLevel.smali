.class public final enum Lcom/google/speech/tts/ProminenceLevel;
.super Ljava/lang/Enum;
.source "ProminenceLevel.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/ProminenceLevel;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/ProminenceLevel;

.field public static final enum MEDIUM:Lcom/google/speech/tts/ProminenceLevel;

.field public static final enum MILD:Lcom/google/speech/tts/ProminenceLevel;

.field public static final enum NONE:Lcom/google/speech/tts/ProminenceLevel;

.field public static final enum STRONG:Lcom/google/speech/tts/ProminenceLevel;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/ProminenceLevel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lcom/google/speech/tts/ProminenceLevel;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/speech/tts/ProminenceLevel;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/ProminenceLevel;->NONE:Lcom/google/speech/tts/ProminenceLevel;

    .line 8
    new-instance v0, Lcom/google/speech/tts/ProminenceLevel;

    const-string v1, "MILD"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/speech/tts/ProminenceLevel;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/ProminenceLevel;->MILD:Lcom/google/speech/tts/ProminenceLevel;

    .line 9
    new-instance v0, Lcom/google/speech/tts/ProminenceLevel;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/google/speech/tts/ProminenceLevel;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/ProminenceLevel;->MEDIUM:Lcom/google/speech/tts/ProminenceLevel;

    .line 10
    new-instance v0, Lcom/google/speech/tts/ProminenceLevel;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/speech/tts/ProminenceLevel;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/ProminenceLevel;->STRONG:Lcom/google/speech/tts/ProminenceLevel;

    .line 5
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/speech/tts/ProminenceLevel;

    sget-object v1, Lcom/google/speech/tts/ProminenceLevel;->NONE:Lcom/google/speech/tts/ProminenceLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/speech/tts/ProminenceLevel;->MILD:Lcom/google/speech/tts/ProminenceLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/speech/tts/ProminenceLevel;->MEDIUM:Lcom/google/speech/tts/ProminenceLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/speech/tts/ProminenceLevel;->STRONG:Lcom/google/speech/tts/ProminenceLevel;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/speech/tts/ProminenceLevel;->$VALUES:[Lcom/google/speech/tts/ProminenceLevel;

    .line 31
    new-instance v0, Lcom/google/speech/tts/ProminenceLevel$1;

    invoke-direct {v0}, Lcom/google/speech/tts/ProminenceLevel$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/ProminenceLevel;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/google/speech/tts/ProminenceLevel;->index:I

    .line 42
    iput p4, p0, Lcom/google/speech/tts/ProminenceLevel;->value:I

    .line 43
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/ProminenceLevel;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/google/speech/tts/ProminenceLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/ProminenceLevel;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/ProminenceLevel;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/google/speech/tts/ProminenceLevel;->$VALUES:[Lcom/google/speech/tts/ProminenceLevel;

    invoke-virtual {v0}, [Lcom/google/speech/tts/ProminenceLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/ProminenceLevel;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/google/speech/tts/ProminenceLevel;->value:I

    return v0
.end method
