.class public final enum Lcom/google/speech/tts/ProminenceType;
.super Ljava/lang/Enum;
.source "ProminenceType.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/ProminenceType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/ProminenceType;

.field public static final enum CONTRASTIVE_FIRST:Lcom/google/speech/tts/ProminenceType;

.field public static final enum CONTRASTIVE_SECOND:Lcom/google/speech/tts/ProminenceType;

.field public static final enum STANDARD:Lcom/google/speech/tts/ProminenceType;

.field public static final enum UNDEFINED:Lcom/google/speech/tts/ProminenceType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/ProminenceType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lcom/google/speech/tts/ProminenceType;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/speech/tts/ProminenceType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/ProminenceType;->UNDEFINED:Lcom/google/speech/tts/ProminenceType;

    .line 8
    new-instance v0, Lcom/google/speech/tts/ProminenceType;

    const-string v1, "STANDARD"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/speech/tts/ProminenceType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/ProminenceType;->STANDARD:Lcom/google/speech/tts/ProminenceType;

    .line 9
    new-instance v0, Lcom/google/speech/tts/ProminenceType;

    const-string v1, "CONTRASTIVE_FIRST"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/google/speech/tts/ProminenceType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/ProminenceType;->CONTRASTIVE_FIRST:Lcom/google/speech/tts/ProminenceType;

    .line 10
    new-instance v0, Lcom/google/speech/tts/ProminenceType;

    const-string v1, "CONTRASTIVE_SECOND"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/speech/tts/ProminenceType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/ProminenceType;->CONTRASTIVE_SECOND:Lcom/google/speech/tts/ProminenceType;

    .line 5
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/speech/tts/ProminenceType;

    sget-object v1, Lcom/google/speech/tts/ProminenceType;->UNDEFINED:Lcom/google/speech/tts/ProminenceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/speech/tts/ProminenceType;->STANDARD:Lcom/google/speech/tts/ProminenceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/speech/tts/ProminenceType;->CONTRASTIVE_FIRST:Lcom/google/speech/tts/ProminenceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/speech/tts/ProminenceType;->CONTRASTIVE_SECOND:Lcom/google/speech/tts/ProminenceType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/speech/tts/ProminenceType;->$VALUES:[Lcom/google/speech/tts/ProminenceType;

    .line 31
    new-instance v0, Lcom/google/speech/tts/ProminenceType$1;

    invoke-direct {v0}, Lcom/google/speech/tts/ProminenceType$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/ProminenceType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/google/speech/tts/ProminenceType;->index:I

    .line 42
    iput p4, p0, Lcom/google/speech/tts/ProminenceType;->value:I

    .line 43
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/ProminenceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/google/speech/tts/ProminenceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/ProminenceType;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/ProminenceType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/google/speech/tts/ProminenceType;->$VALUES:[Lcom/google/speech/tts/ProminenceType;

    invoke-virtual {v0}, [Lcom/google/speech/tts/ProminenceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/ProminenceType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/google/speech/tts/ProminenceType;->value:I

    return v0
.end method
