.class public final Lcom/google/speech/tts/engine/TextnormParams$Pause;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "TextnormParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/TextnormParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Pause"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/engine/TextnormParams$Pause;


# instance fields
.field private duration_:F

.field private hasDuration:Z

.field private hasLength:Z

.field private hasName:Z

.field private length_:Lspeech/patts/Token$PauseLength;

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 358
    new-instance v0, Lcom/google/speech/tts/engine/TextnormParams$Pause;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->defaultInstance:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    .line 359
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParamsProto;->internalForceInit()V

    .line 360
    sget-object v0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->defaultInstance:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->initFields()V

    .line 361
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->duration_:F

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->name_:Ljava/lang/String;

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->memoizedSerializedSize:I

    .line 26
    invoke-direct {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->initFields()V

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/engine/TextnormParams$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/engine/TextnormParams$1;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->duration_:F

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->name_:Ljava/lang/String;

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->memoizedSerializedSize:I

    .line 28
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/tts/engine/TextnormParams$Pause;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams$Pause;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasDuration:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/engine/TextnormParams$Pause;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams$Pause;
    .param p1, "x1"    # F

    .prologue
    .line 22
    iput p1, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->duration_:F

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/engine/TextnormParams$Pause;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams$Pause;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasName:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/engine/TextnormParams$Pause;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams$Pause;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/engine/TextnormParams$Pause;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams$Pause;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasLength:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/engine/TextnormParams$Pause;Lspeech/patts/Token$PauseLength;)Lspeech/patts/Token$PauseLength;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams$Pause;
    .param p1, "x1"    # Lspeech/patts/Token$PauseLength;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->length_:Lspeech/patts/Token$PauseLength;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/engine/TextnormParams$Pause;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->defaultInstance:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lspeech/patts/Token$PauseLength;->PAUSE_NONE:Lspeech/patts/Token$PauseLength;

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->length_:Lspeech/patts/Token$PauseLength;

    .line 62
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    .locals 1

    .prologue
    .line 173
    # invokes: Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->create()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->access$100()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/engine/TextnormParams$Pause;)Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/engine/TextnormParams$Pause;

    .prologue
    .line 176
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->newBuilder()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->mergeFrom(Lcom/google/speech/tts/engine/TextnormParams$Pause;)Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->getDefaultInstanceForType()Lcom/google/speech/tts/engine/TextnormParams$Pause;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/engine/TextnormParams$Pause;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->defaultInstance:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    return-object v0
.end method

.method public getDuration()F
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->duration_:F

    return v0
.end method

.method public getLength()Lspeech/patts/Token$PauseLength;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->length_:Lspeech/patts/Token$PauseLength;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 86
    iget v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->memoizedSerializedSize:I

    .line 87
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 103
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 89
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 90
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasDuration()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 91
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->getDuration()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 94
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasName()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 95
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 98
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasLength()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 99
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->getLength()Lspeech/patts/Token$PauseLength;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/Token$PauseLength;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 102
    :cond_3
    iput v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->memoizedSerializedSize:I

    move v1, v0

    .line 103
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasDuration()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasDuration:Z

    return v0
.end method

.method public hasLength()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasLength:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasName:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 64
    iget-boolean v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasDuration:Z

    if-nez v1, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v0

    .line 65
    :cond_1
    iget-boolean v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasName:Z

    if-eqz v1, :cond_0

    .line 66
    iget-boolean v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasLength:Z

    if-eqz v1, :cond_0

    .line 67
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->toBuilder()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    .locals 1

    .prologue
    .line 178
    invoke-static {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->newBuilder(Lcom/google/speech/tts/engine/TextnormParams$Pause;)Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->getSerializedSize()I

    .line 73
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasDuration()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->getDuration()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasName()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 79
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasLength()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->getLength()Lspeech/patts/Token$PauseLength;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/Token$PauseLength;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 82
    :cond_2
    return-void
.end method
