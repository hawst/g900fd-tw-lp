.class public final enum Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;
.super Ljava/lang/Enum;
.source "LexiconProto.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto$Morphology;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Animacy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

.field public static final enum ANIMATE:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

.field public static final enum INANIMATE:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

.field public static final enum UNKNOWN_ANIMACY:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1489
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    const-string v1, "UNKNOWN_ANIMACY"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->UNKNOWN_ANIMACY:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    .line 1490
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    const-string v1, "ANIMATE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->ANIMATE:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    .line 1491
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    const-string v1, "INANIMATE"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->INANIMATE:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    .line 1487
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->UNKNOWN_ANIMACY:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->ANIMATE:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->INANIMATE:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->$VALUES:[Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    .line 1511
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy$1;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1520
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1521
    iput p3, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->index:I

    .line 1522
    iput p4, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->value:I

    .line 1523
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1487
    const-class v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;
    .locals 1

    .prologue
    .line 1487
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->$VALUES:[Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    invoke-virtual {v0}, [Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 1495
    iget v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->value:I

    return v0
.end method
