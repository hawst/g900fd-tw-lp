.class public final Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "TextnormParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/TextnormParams$Pause;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/engine/TextnormParams$Pause;",
        "Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/engine/TextnormParams$Pause;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    .locals 1

    .prologue
    .line 180
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->create()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    .locals 3

    .prologue
    .line 189
    new-instance v0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;-><init>()V

    .line 190
    .local v0, "builder":Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    new-instance v1, Lcom/google/speech/tts/engine/TextnormParams$Pause;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/engine/TextnormParams$Pause;-><init>(Lcom/google/speech/tts/engine/TextnormParams$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    .line 191
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->build()Lcom/google/speech/tts/engine/TextnormParams$Pause;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/engine/TextnormParams$Pause;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 222
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->buildPartial()Lcom/google/speech/tts/engine/TextnormParams$Pause;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/engine/TextnormParams$Pause;
    .locals 3

    .prologue
    .line 235
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    if-nez v1, :cond_0

    .line 236
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    .line 240
    .local v0, "returnMe":Lcom/google/speech/tts/engine/TextnormParams$Pause;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    .line 241
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->clone()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->clone()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    .locals 2

    .prologue
    .line 208
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->create()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->mergeFrom(Lcom/google/speech/tts/engine/TextnormParams$Pause;)Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->clone()Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    invoke-virtual {v0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 180
    check-cast p1, Lcom/google/speech/tts/engine/TextnormParams$Pause;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->mergeFrom(Lcom/google/speech/tts/engine/TextnormParams$Pause;)Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/engine/TextnormParams$Pause;)Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/engine/TextnormParams$Pause;

    .prologue
    .line 245
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->getDefaultInstance()Lcom/google/speech/tts/engine/TextnormParams$Pause;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 255
    :cond_0
    :goto_0
    return-object p0

    .line 246
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasDuration()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 247
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->getDuration()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->setDuration(F)Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    .line 249
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasName()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 250
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->setName(Ljava/lang/String;)Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    .line 252
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasLength()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->getLength()Lspeech/patts/Token$PauseLength;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->setLength(Lspeech/patts/Token$PauseLength;)Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;

    goto :goto_0
.end method

.method public setDuration(F)Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasDuration:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->access$302(Lcom/google/speech/tts/engine/TextnormParams$Pause;Z)Z

    .line 303
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    # setter for: Lcom/google/speech/tts/engine/TextnormParams$Pause;->duration_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->access$402(Lcom/google/speech/tts/engine/TextnormParams$Pause;F)F

    .line 304
    return-object p0
.end method

.method public setLength(Lspeech/patts/Token$PauseLength;)Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/Token$PauseLength;

    .prologue
    .line 341
    if-nez p1, :cond_0

    .line 342
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasLength:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->access$702(Lcom/google/speech/tts/engine/TextnormParams$Pause;Z)Z

    .line 345
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    # setter for: Lcom/google/speech/tts/engine/TextnormParams$Pause;->length_:Lspeech/patts/Token$PauseLength;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->access$802(Lcom/google/speech/tts/engine/TextnormParams$Pause;Lspeech/patts/Token$PauseLength;)Lspeech/patts/Token$PauseLength;

    .line 346
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 320
    if-nez p1, :cond_0

    .line 321
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/TextnormParams$Pause;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->access$502(Lcom/google/speech/tts/engine/TextnormParams$Pause;Z)Z

    .line 324
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Pause$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams$Pause;

    # setter for: Lcom/google/speech/tts/engine/TextnormParams$Pause;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->access$602(Lcom/google/speech/tts/engine/TextnormParams$Pause;Ljava/lang/String;)Ljava/lang/String;

    .line 325
    return-object p0
.end method
