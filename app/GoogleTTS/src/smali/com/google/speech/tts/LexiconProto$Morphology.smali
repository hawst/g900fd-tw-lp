.class public final Lcom/google/speech/tts/LexiconProto$Morphology;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Morphology"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/LexiconProto$Morphology$Builder;,
        Lcom/google/speech/tts/LexiconProto$Morphology$Number;,
        Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;,
        Lcom/google/speech/tts/LexiconProto$Morphology$Gender;,
        Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/LexiconProto$Morphology;


# instance fields
.field private animacy_:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

.field private gender_:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

.field private hasAnimacy:Z

.field private hasGender:Z

.field private hasNumber:Z

.field private hasPos:Z

.field private memoizedSerializedSize:I

.field private number_:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

.field private pos_:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1947
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/LexiconProto$Morphology;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Morphology;

    .line 1948
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 1949
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Morphology;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Morphology;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Morphology;->initFields()V

    .line 1950
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1377
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1626
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->memoizedSerializedSize:I

    .line 1378
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->initFields()V

    .line 1379
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/LexiconProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/LexiconProto$1;

    .prologue
    .line 1374
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 1380
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1626
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->memoizedSerializedSize:I

    .line 1380
    return-void
.end method

.method static synthetic access$3002(Lcom/google/speech/tts/LexiconProto$Morphology;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Morphology;
    .param p1, "x1"    # Z

    .prologue
    .line 1374
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->hasPos:Z

    return p1
.end method

.method static synthetic access$3102(Lcom/google/speech/tts/LexiconProto$Morphology;Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;)Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Morphology;
    .param p1, "x1"    # Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .prologue
    .line 1374
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->pos_:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    return-object p1
.end method

.method static synthetic access$3202(Lcom/google/speech/tts/LexiconProto$Morphology;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Morphology;
    .param p1, "x1"    # Z

    .prologue
    .line 1374
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->hasGender:Z

    return p1
.end method

.method static synthetic access$3302(Lcom/google/speech/tts/LexiconProto$Morphology;Lcom/google/speech/tts/LexiconProto$Morphology$Gender;)Lcom/google/speech/tts/LexiconProto$Morphology$Gender;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Morphology;
    .param p1, "x1"    # Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    .prologue
    .line 1374
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->gender_:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    return-object p1
.end method

.method static synthetic access$3402(Lcom/google/speech/tts/LexiconProto$Morphology;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Morphology;
    .param p1, "x1"    # Z

    .prologue
    .line 1374
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->hasAnimacy:Z

    return p1
.end method

.method static synthetic access$3502(Lcom/google/speech/tts/LexiconProto$Morphology;Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;)Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Morphology;
    .param p1, "x1"    # Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    .prologue
    .line 1374
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->animacy_:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    return-object p1
.end method

.method static synthetic access$3602(Lcom/google/speech/tts/LexiconProto$Morphology;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Morphology;
    .param p1, "x1"    # Z

    .prologue
    .line 1374
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->hasNumber:Z

    return p1
.end method

.method static synthetic access$3702(Lcom/google/speech/tts/LexiconProto$Morphology;Lcom/google/speech/tts/LexiconProto$Morphology$Number;)Lcom/google/speech/tts/LexiconProto$Morphology$Number;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Morphology;
    .param p1, "x1"    # Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    .prologue
    .line 1374
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->number_:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/LexiconProto$Morphology;
    .locals 1

    .prologue
    .line 1384
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Morphology;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Morphology;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 1600
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->UNKNOWN_POS:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->pos_:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .line 1601
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->UNKNOWN_GENDER:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->gender_:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    .line 1602
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->UNKNOWN_ANIMACY:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->animacy_:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    .line 1603
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->UNKNOWN_NUMBER:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->number_:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    .line 1604
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    .locals 1

    .prologue
    .line 1719
    # invokes: Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->create()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->access$2800()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/LexiconProto$Morphology;

    .prologue
    .line 1722
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Morphology;->newBuilder()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAnimacy()Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;
    .locals 1

    .prologue
    .line 1590
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->animacy_:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1374
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto$Morphology;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto$Morphology;
    .locals 1

    .prologue
    .line 1388
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Morphology;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Morphology;

    return-object v0
.end method

.method public getGender()Lcom/google/speech/tts/LexiconProto$Morphology$Gender;
    .locals 1

    .prologue
    .line 1583
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->gender_:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    return-object v0
.end method

.method public getNumber()Lcom/google/speech/tts/LexiconProto$Morphology$Number;
    .locals 1

    .prologue
    .line 1597
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->number_:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    return-object v0
.end method

.method public getPos()Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;
    .locals 1

    .prologue
    .line 1576
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->pos_:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 1628
    iget v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->memoizedSerializedSize:I

    .line 1629
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 1649
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 1631
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 1632
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->hasPos()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1633
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->getPos()Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1636
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->hasGender()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1637
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->getGender()Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1640
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->hasAnimacy()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1641
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->getAnimacy()Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1644
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->hasNumber()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1645
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->getNumber()Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1648
    :cond_4
    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->memoizedSerializedSize:I

    move v1, v0

    .line 1649
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasAnimacy()Z
    .locals 1

    .prologue
    .line 1589
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->hasAnimacy:Z

    return v0
.end method

.method public hasGender()Z
    .locals 1

    .prologue
    .line 1582
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->hasGender:Z

    return v0
.end method

.method public hasNumber()Z
    .locals 1

    .prologue
    .line 1596
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->hasNumber:Z

    return v0
.end method

.method public hasPos()Z
    .locals 1

    .prologue
    .line 1575
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology;->hasPos:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 1606
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1374
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->toBuilder()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    .locals 1

    .prologue
    .line 1724
    invoke-static {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->newBuilder(Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1611
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->getSerializedSize()I

    .line 1612
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->hasPos()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1613
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->getPos()Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 1615
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->hasGender()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1616
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->getGender()Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 1618
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->hasAnimacy()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1619
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->getAnimacy()Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 1621
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->hasNumber()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1622
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology;->getNumber()Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 1624
    :cond_3
    return-void
.end method
