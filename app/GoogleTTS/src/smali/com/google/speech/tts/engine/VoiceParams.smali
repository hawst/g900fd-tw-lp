.class public final Lcom/google/speech/tts/engine/VoiceParams;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "VoiceParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/engine/VoiceParams$1;,
        Lcom/google/speech/tts/engine/VoiceParams$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/engine/VoiceParams;

.field public static final id:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension",
            "<",
            "Lgreco/Params;",
            "Lcom/google/speech/tts/engine/VoiceParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private description_:Ljava/lang/String;

.field private engineOverridesFile_:Ljava/lang/String;

.field private gender_:Ljava/lang/String;

.field private hasDescription:Z

.field private hasEngineOverridesFile:Z

.field private hasGender:Z

.field private hasLanguage:Z

.field private hasQuality:Z

.field private hasRegion:Z

.field private hasSampleRate:Z

.field private hasServerVoiceName:Z

.field private hasSpeaker:Z

.field private language_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private quality_:Ljava/lang/String;

.field private region_:Ljava/lang/String;

.field private sampleRate_:I

.field private serverVoiceName_:Ljava/lang/String;

.field private speaker_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->newGeneratedExtension()Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    move-result-object v0

    sput-object v0, Lcom/google/speech/tts/engine/VoiceParams;->id:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    .line 592
    new-instance v0, Lcom/google/speech/tts/engine/VoiceParams;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/engine/VoiceParams;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/engine/VoiceParams;->defaultInstance:Lcom/google/speech/tts/engine/VoiceParams;

    .line 593
    invoke-static {}, Lcom/google/speech/tts/engine/VoiceParamsProto;->internalForceInit()V

    .line 594
    sget-object v0, Lcom/google/speech/tts/engine/VoiceParams;->defaultInstance:Lcom/google/speech/tts/engine/VoiceParams;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/VoiceParams;->initFields()V

    .line 595
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->description_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->language_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->region_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->speaker_:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->gender_:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->quality_:Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->sampleRate_:I

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->serverVoiceName_:Ljava/lang/String;

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->engineOverridesFile_:Ljava/lang/String;

    .line 130
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/engine/VoiceParams;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/engine/VoiceParams$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/engine/VoiceParams$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/engine/VoiceParams;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->description_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->language_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->region_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->speaker_:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->gender_:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->quality_:Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->sampleRate_:I

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->serverVoiceName_:Ljava/lang/String;

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->engineOverridesFile_:Ljava/lang/String;

    .line 130
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->speaker_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/engine/VoiceParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasGender:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->gender_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/engine/VoiceParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasQuality:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->quality_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/engine/VoiceParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasSampleRate:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/engine/VoiceParams;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->sampleRate_:I

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/engine/VoiceParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasServerVoiceName:Z

    return p1
.end method

.method static synthetic access$1802(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->serverVoiceName_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1902(Lcom/google/speech/tts/engine/VoiceParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasEngineOverridesFile:Z

    return p1
.end method

.method static synthetic access$2002(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->engineOverridesFile_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/speech/tts/engine/VoiceParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasDescription:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->description_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/engine/VoiceParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasLanguage:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->language_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/engine/VoiceParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasRegion:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->region_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/engine/VoiceParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/VoiceParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasSpeaker:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/engine/VoiceParams;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/engine/VoiceParams;->defaultInstance:Lcom/google/speech/tts/engine/VoiceParams;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 1

    .prologue
    .line 243
    # invokes: Lcom/google/speech/tts/engine/VoiceParams$Builder;->create()Lcom/google/speech/tts/engine/VoiceParams$Builder;
    invoke-static {}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->access$100()Lcom/google/speech/tts/engine/VoiceParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/engine/VoiceParams;)Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/engine/VoiceParams;

    .prologue
    .line 246
    invoke-static {}, Lcom/google/speech/tts/engine/VoiceParams;->newBuilder()Lcom/google/speech/tts/engine/VoiceParams$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->mergeFrom(Lcom/google/speech/tts/engine/VoiceParams;)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getDefaultInstanceForType()Lcom/google/speech/tts/engine/VoiceParams;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/engine/VoiceParams;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/engine/VoiceParams;->defaultInstance:Lcom/google/speech/tts/engine/VoiceParams;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getEngineOverridesFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->engineOverridesFile_:Ljava/lang/String;

    return-object v0
.end method

.method public getGender()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->gender_:Ljava/lang/String;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->language_:Ljava/lang/String;

    return-object v0
.end method

.method public getQuality()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->quality_:Ljava/lang/String;

    return-object v0
.end method

.method public getRegion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->region_:Ljava/lang/String;

    return-object v0
.end method

.method public getSampleRate()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->sampleRate_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 132
    iget v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->memoizedSerializedSize:I

    .line 133
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 173
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 135
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 136
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasDescription()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 137
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 140
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasLanguage()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 141
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 144
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasRegion()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 145
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getRegion()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 148
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasSpeaker()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 149
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getSpeaker()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 152
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasGender()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 153
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getGender()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 156
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasQuality()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 157
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getQuality()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 160
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasSampleRate()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 161
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getSampleRate()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 164
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasServerVoiceName()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 165
    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getServerVoiceName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 168
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasEngineOverridesFile()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 169
    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getEngineOverridesFile()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 172
    :cond_9
    iput v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->memoizedSerializedSize:I

    move v1, v0

    .line 173
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto/16 :goto_0
.end method

.method public getServerVoiceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->serverVoiceName_:Ljava/lang/String;

    return-object v0
.end method

.method public getSpeaker()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->speaker_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDescription()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasDescription:Z

    return v0
.end method

.method public hasEngineOverridesFile()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasEngineOverridesFile:Z

    return v0
.end method

.method public hasGender()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasGender:Z

    return v0
.end method

.method public hasLanguage()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasLanguage:Z

    return v0
.end method

.method public hasQuality()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasQuality:Z

    return v0
.end method

.method public hasRegion()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasRegion:Z

    return v0
.end method

.method public hasSampleRate()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasSampleRate:Z

    return v0
.end method

.method public hasServerVoiceName()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasServerVoiceName:Z

    return v0
.end method

.method public hasSpeaker()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/VoiceParams;->hasSpeaker:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->toBuilder()Lcom/google/speech/tts/engine/VoiceParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 1

    .prologue
    .line 248
    invoke-static {p0}, Lcom/google/speech/tts/engine/VoiceParams;->newBuilder(Lcom/google/speech/tts/engine/VoiceParams;)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getSerializedSize()I

    .line 101
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasDescription()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 104
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasLanguage()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 107
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasRegion()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getRegion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 110
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasSpeaker()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 111
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getSpeaker()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 113
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasGender()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 114
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getGender()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 116
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasQuality()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 117
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getQuality()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 119
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasSampleRate()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 120
    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getSampleRate()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 122
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasServerVoiceName()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 123
    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getServerVoiceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 125
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->hasEngineOverridesFile()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 126
    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams;->getEngineOverridesFile()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 128
    :cond_8
    return-void
.end method
