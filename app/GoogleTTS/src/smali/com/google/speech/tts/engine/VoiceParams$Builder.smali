.class public final Lcom/google/speech/tts/engine/VoiceParams$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "VoiceParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/VoiceParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/engine/VoiceParams;",
        "Lcom/google/speech/tts/engine/VoiceParams$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/engine/VoiceParams;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 256
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 1

    .prologue
    .line 250
    invoke-static {}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->create()Lcom/google/speech/tts/engine/VoiceParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 3

    .prologue
    .line 259
    new-instance v0, Lcom/google/speech/tts/engine/VoiceParams$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;-><init>()V

    .line 260
    .local v0, "builder":Lcom/google/speech/tts/engine/VoiceParams$Builder;
    new-instance v1, Lcom/google/speech/tts/engine/VoiceParams;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/engine/VoiceParams;-><init>(Lcom/google/speech/tts/engine/VoiceParams$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    .line 261
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->build()Lcom/google/speech/tts/engine/VoiceParams;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/engine/VoiceParams;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    invoke-static {v0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 292
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->buildPartial()Lcom/google/speech/tts/engine/VoiceParams;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/engine/VoiceParams;
    .locals 3

    .prologue
    .line 305
    iget-object v1, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    if-nez v1, :cond_0

    .line 306
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    .line 310
    .local v0, "returnMe":Lcom/google/speech/tts/engine/VoiceParams;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    .line 311
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->clone()Lcom/google/speech/tts/engine/VoiceParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->clone()Lcom/google/speech/tts/engine/VoiceParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 2

    .prologue
    .line 278
    invoke-static {}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->create()Lcom/google/speech/tts/engine/VoiceParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->mergeFrom(Lcom/google/speech/tts/engine/VoiceParams;)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->clone()Lcom/google/speech/tts/engine/VoiceParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    invoke-virtual {v0}, Lcom/google/speech/tts/engine/VoiceParams;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 250
    check-cast p1, Lcom/google/speech/tts/engine/VoiceParams;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->mergeFrom(Lcom/google/speech/tts/engine/VoiceParams;)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/engine/VoiceParams;)Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/engine/VoiceParams;

    .prologue
    .line 315
    invoke-static {}, Lcom/google/speech/tts/engine/VoiceParams;->getDefaultInstance()Lcom/google/speech/tts/engine/VoiceParams;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-object p0

    .line 316
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->hasDescription()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 317
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->setDescription(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    .line 319
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->hasLanguage()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 320
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->setLanguage(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    .line 322
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->hasRegion()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 323
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->getRegion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->setRegion(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    .line 325
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->hasSpeaker()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 326
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->getSpeaker()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->setSpeaker(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    .line 328
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->hasGender()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 329
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->getGender()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->setGender(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    .line 331
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->hasQuality()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 332
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->getQuality()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->setQuality(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    .line 334
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->hasSampleRate()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 335
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->getSampleRate()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->setSampleRate(I)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    .line 337
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->hasServerVoiceName()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 338
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->getServerVoiceName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->setServerVoiceName(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    .line 340
    :cond_9
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->hasEngineOverridesFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/VoiceParams;->getEngineOverridesFile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/VoiceParams$Builder;->setEngineOverridesFile(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;

    goto :goto_0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 410
    if-nez p1, :cond_0

    .line 411
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->hasDescription:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/VoiceParams;->access$302(Lcom/google/speech/tts/engine/VoiceParams;Z)Z

    .line 414
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->description_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/VoiceParams;->access$402(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;

    .line 415
    return-object p0
.end method

.method public setEngineOverridesFile(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 575
    if-nez p1, :cond_0

    .line 576
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 578
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->hasEngineOverridesFile:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/VoiceParams;->access$1902(Lcom/google/speech/tts/engine/VoiceParams;Z)Z

    .line 579
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->engineOverridesFile_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/VoiceParams;->access$2002(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;

    .line 580
    return-object p0
.end method

.method public setGender(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 494
    if-nez p1, :cond_0

    .line 495
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->hasGender:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/VoiceParams;->access$1102(Lcom/google/speech/tts/engine/VoiceParams;Z)Z

    .line 498
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->gender_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/VoiceParams;->access$1202(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;

    .line 499
    return-object p0
.end method

.method public setLanguage(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 431
    if-nez p1, :cond_0

    .line 432
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 434
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->hasLanguage:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/VoiceParams;->access$502(Lcom/google/speech/tts/engine/VoiceParams;Z)Z

    .line 435
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->language_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/VoiceParams;->access$602(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;

    .line 436
    return-object p0
.end method

.method public setQuality(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 515
    if-nez p1, :cond_0

    .line 516
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->hasQuality:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/VoiceParams;->access$1302(Lcom/google/speech/tts/engine/VoiceParams;Z)Z

    .line 519
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->quality_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/VoiceParams;->access$1402(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;

    .line 520
    return-object p0
.end method

.method public setRegion(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 452
    if-nez p1, :cond_0

    .line 453
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 455
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->hasRegion:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/VoiceParams;->access$702(Lcom/google/speech/tts/engine/VoiceParams;Z)Z

    .line 456
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->region_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/VoiceParams;->access$802(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;

    .line 457
    return-object p0
.end method

.method public setSampleRate(I)Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->hasSampleRate:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/VoiceParams;->access$1502(Lcom/google/speech/tts/engine/VoiceParams;Z)Z

    .line 537
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->sampleRate_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/VoiceParams;->access$1602(Lcom/google/speech/tts/engine/VoiceParams;I)I

    .line 538
    return-object p0
.end method

.method public setServerVoiceName(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 554
    if-nez p1, :cond_0

    .line 555
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 557
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->hasServerVoiceName:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/VoiceParams;->access$1702(Lcom/google/speech/tts/engine/VoiceParams;Z)Z

    .line 558
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->serverVoiceName_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/VoiceParams;->access$1802(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;

    .line 559
    return-object p0
.end method

.method public setSpeaker(Ljava/lang/String;)Lcom/google/speech/tts/engine/VoiceParams$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 473
    if-nez p1, :cond_0

    .line 474
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->hasSpeaker:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/VoiceParams;->access$902(Lcom/google/speech/tts/engine/VoiceParams;Z)Z

    .line 477
    iget-object v0, p0, Lcom/google/speech/tts/engine/VoiceParams$Builder;->result:Lcom/google/speech/tts/engine/VoiceParams;

    # setter for: Lcom/google/speech/tts/engine/VoiceParams;->speaker_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/VoiceParams;->access$1002(Lcom/google/speech/tts/engine/VoiceParams;Ljava/lang/String;)Ljava/lang/String;

    .line 478
    return-object p0
.end method
