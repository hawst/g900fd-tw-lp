.class public final Lcom/google/speech/tts/LexiconProto$Entry$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto$Entry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/LexiconProto$Entry;",
        "Lcom/google/speech/tts/LexiconProto$Entry$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/LexiconProto$Entry;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2244
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$3900()Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 1

    .prologue
    .line 2238
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->create()Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 3

    .prologue
    .line 2247
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;-><init>()V

    .line 2248
    .local v0, "builder":Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    new-instance v1, Lcom/google/speech/tts/LexiconProto$Entry;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/LexiconProto$Entry;-><init>(Lcom/google/speech/tts/LexiconProto$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    .line 2249
    return-object v0
.end method


# virtual methods
.method public addP(Lcom/google/speech/tts/LexiconProto$Pronunciation;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/LexiconProto$Pronunciation;

    .prologue
    .line 2439
    if-nez p1, :cond_0

    .line 2440
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2442
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4100(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2443
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4102(Lcom/google/speech/tts/LexiconProto$Entry;Ljava/util/List;)Ljava/util/List;

    .line 2445
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4100(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2446
    return-object p0
.end method

.method public addS(Lcom/google/speech/tts/LexiconProto$Spelling;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/LexiconProto$Spelling;

    .prologue
    .line 2490
    if-nez p1, :cond_0

    .line 2491
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2493
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4200(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2494
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4202(Lcom/google/speech/tts/LexiconProto$Entry;Ljava/util/List;)Ljava/util/List;

    .line 2496
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4200(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2497
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2238
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->build()Lcom/google/speech/tts/LexiconProto$Entry;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/LexiconProto$Entry;
    .locals 1

    .prologue
    .line 2277
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2278
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2280
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->buildPartial()Lcom/google/speech/tts/LexiconProto$Entry;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/LexiconProto$Entry;
    .locals 3

    .prologue
    .line 2293
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    if-nez v1, :cond_0

    .line 2294
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2297
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4100(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 2298
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    iget-object v2, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4100(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4102(Lcom/google/speech/tts/LexiconProto$Entry;Ljava/util/List;)Ljava/util/List;

    .line 2301
    :cond_1
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4200(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 2302
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    iget-object v2, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4200(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4202(Lcom/google/speech/tts/LexiconProto$Entry;Ljava/util/List;)Ljava/util/List;

    .line 2305
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    .line 2306
    .local v0, "returnMe":Lcom/google/speech/tts/LexiconProto$Entry;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    .line 2307
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2238
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2238
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 2

    .prologue
    .line 2266
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->create()Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Entry;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2238
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 2274
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 2238
    check-cast p1, Lcom/google/speech/tts/LexiconProto$Entry;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Entry;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/LexiconProto$Entry;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/LexiconProto$Entry;

    .prologue
    .line 2311
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Entry;->getDefaultInstance()Lcom/google/speech/tts/LexiconProto$Entry;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 2336
    :cond_0
    :goto_0
    return-object p0

    .line 2312
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Entry;->hasId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2313
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Entry;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->setId(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    .line 2315
    :cond_2
    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4100(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2316
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4100(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2317
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4102(Lcom/google/speech/tts/LexiconProto$Entry;Ljava/util/List;)Ljava/util/List;

    .line 2319
    :cond_3
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4100(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4100(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2321
    :cond_4
    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4200(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2322
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4200(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2323
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4202(Lcom/google/speech/tts/LexiconProto$Entry;Ljava/util/List;)Ljava/util/List;

    .line 2325
    :cond_5
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4200(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4200(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2327
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Entry;->hasC()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2328
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Entry;->getC()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->setC(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    .line 2330
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Entry;->hasSyn()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2331
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Entry;->getSyn()Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->setSyn(Lcom/google/speech/tts/LexiconProto$Entry$Syntax;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    .line 2333
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Entry;->hasM()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2334
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Entry;->getM()Lcom/google/speech/tts/LexiconProto$Morphology;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->mergeM(Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    goto/16 :goto_0
.end method

.method public mergeM(Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/LexiconProto$Morphology;

    .prologue
    .line 2582
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->hasM()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->m_:Lcom/google/speech/tts/LexiconProto$Morphology;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->access$5000(Lcom/google/speech/tts/LexiconProto$Entry;)Lcom/google/speech/tts/LexiconProto$Morphology;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Morphology;->getDefaultInstance()Lcom/google/speech/tts/LexiconProto$Morphology;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2584
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # getter for: Lcom/google/speech/tts/LexiconProto$Entry;->m_:Lcom/google/speech/tts/LexiconProto$Morphology;
    invoke-static {v1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$5000(Lcom/google/speech/tts/LexiconProto$Entry;)Lcom/google/speech/tts/LexiconProto$Morphology;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/LexiconProto$Morphology;->newBuilder(Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->buildPartial()Lcom/google/speech/tts/LexiconProto$Morphology;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->m_:Lcom/google/speech/tts/LexiconProto$Morphology;
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$5002(Lcom/google/speech/tts/LexiconProto$Entry;Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Morphology;

    .line 2589
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->hasM:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4902(Lcom/google/speech/tts/LexiconProto$Entry;Z)Z

    .line 2590
    return-object p0

    .line 2587
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->m_:Lcom/google/speech/tts/LexiconProto$Morphology;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$5002(Lcom/google/speech/tts/LexiconProto$Entry;Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Morphology;

    goto :goto_0
.end method

.method public setC(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2527
    if-nez p1, :cond_0

    .line 2528
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2530
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->hasC:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4502(Lcom/google/speech/tts/LexiconProto$Entry;Z)Z

    .line 2531
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->c_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4602(Lcom/google/speech/tts/LexiconProto$Entry;Ljava/lang/String;)Ljava/lang/String;

    .line 2532
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2404
    if-nez p1, :cond_0

    .line 2405
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2407
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->hasId:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4302(Lcom/google/speech/tts/LexiconProto$Entry;Z)Z

    .line 2408
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->id_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4402(Lcom/google/speech/tts/LexiconProto$Entry;Ljava/lang/String;)Ljava/lang/String;

    .line 2409
    return-object p0
.end method

.method public setSyn(Lcom/google/speech/tts/LexiconProto$Entry$Syntax;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .prologue
    .line 2548
    if-nez p1, :cond_0

    .line 2549
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2551
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->hasSyn:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4702(Lcom/google/speech/tts/LexiconProto$Entry;Z)Z

    .line 2552
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->result:Lcom/google/speech/tts/LexiconProto$Entry;

    # setter for: Lcom/google/speech/tts/LexiconProto$Entry;->syn_:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Entry;->access$4802(Lcom/google/speech/tts/LexiconProto$Entry;Lcom/google/speech/tts/LexiconProto$Entry$Syntax;)Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 2553
    return-object p0
.end method
