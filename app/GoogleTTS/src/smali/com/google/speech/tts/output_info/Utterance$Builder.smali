.class public final Lcom/google/speech/tts/output_info/Utterance$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Utterance.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/output_info/Utterance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/output_info/Utterance;",
        "Lcom/google/speech/tts/output_info/Utterance$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/output_info/Utterance;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/output_info/Utterance$Builder;
    .locals 1

    .prologue
    .line 158
    invoke-static {}, Lcom/google/speech/tts/output_info/Utterance$Builder;->create()Lcom/google/speech/tts/output_info/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/output_info/Utterance$Builder;
    .locals 3

    .prologue
    .line 167
    new-instance v0, Lcom/google/speech/tts/output_info/Utterance$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/output_info/Utterance$Builder;-><init>()V

    .line 168
    .local v0, "builder":Lcom/google/speech/tts/output_info/Utterance$Builder;
    new-instance v1, Lcom/google/speech/tts/output_info/Utterance;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/output_info/Utterance;-><init>(Lcom/google/speech/tts/output_info/Utterance$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    .line 169
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance$Builder;->build()Lcom/google/speech/tts/output_info/Utterance;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/output_info/Utterance;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    invoke-static {v0}, Lcom/google/speech/tts/output_info/Utterance$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance$Builder;->buildPartial()Lcom/google/speech/tts/output_info/Utterance;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/output_info/Utterance;
    .locals 3

    .prologue
    .line 213
    iget-object v1, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    if-nez v1, :cond_0

    .line 214
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    # getter for: Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/output_info/Utterance;->access$300(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 218
    iget-object v1, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    iget-object v2, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    # getter for: Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/output_info/Utterance;->access$300(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/output_info/Utterance;->access$302(Lcom/google/speech/tts/output_info/Utterance;Ljava/util/List;)Ljava/util/List;

    .line 221
    :cond_1
    iget-object v1, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    # getter for: Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/output_info/Utterance;->access$400(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 222
    iget-object v1, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    iget-object v2, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    # getter for: Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/output_info/Utterance;->access$400(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/output_info/Utterance;->access$402(Lcom/google/speech/tts/output_info/Utterance;Ljava/util/List;)Ljava/util/List;

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    .line 226
    .local v0, "returnMe":Lcom/google/speech/tts/output_info/Utterance;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    .line 227
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance$Builder;->clone()Lcom/google/speech/tts/output_info/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance$Builder;->clone()Lcom/google/speech/tts/output_info/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/output_info/Utterance$Builder;
    .locals 2

    .prologue
    .line 186
    invoke-static {}, Lcom/google/speech/tts/output_info/Utterance$Builder;->create()Lcom/google/speech/tts/output_info/Utterance$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/output_info/Utterance$Builder;->mergeFrom(Lcom/google/speech/tts/output_info/Utterance;)Lcom/google/speech/tts/output_info/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance$Builder;->clone()Lcom/google/speech/tts/output_info/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    invoke-virtual {v0}, Lcom/google/speech/tts/output_info/Utterance;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 158
    check-cast p1, Lcom/google/speech/tts/output_info/Utterance;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/output_info/Utterance$Builder;->mergeFrom(Lcom/google/speech/tts/output_info/Utterance;)Lcom/google/speech/tts/output_info/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/output_info/Utterance;)Lcom/google/speech/tts/output_info/Utterance$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/output_info/Utterance;

    .prologue
    .line 231
    invoke-static {}, Lcom/google/speech/tts/output_info/Utterance;->getDefaultInstance()Lcom/google/speech/tts/output_info/Utterance;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-object p0

    .line 232
    :cond_1
    # getter for: Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/output_info/Utterance;->access$300(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 233
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    # getter for: Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/output_info/Utterance;->access$300(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 234
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/output_info/Utterance;->access$302(Lcom/google/speech/tts/output_info/Utterance;Ljava/util/List;)Ljava/util/List;

    .line 236
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    # getter for: Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/output_info/Utterance;->access$300(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/output_info/Utterance;->access$300(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 238
    :cond_3
    # getter for: Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/output_info/Utterance;->access$400(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    # getter for: Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/output_info/Utterance;->access$400(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 240
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/output_info/Utterance;->access$402(Lcom/google/speech/tts/output_info/Utterance;Ljava/util/List;)Ljava/util/List;

    .line 242
    :cond_4
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance$Builder;->result:Lcom/google/speech/tts/output_info/Utterance;

    # getter for: Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/output_info/Utterance;->access$400(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/output_info/Utterance;->access$400(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
