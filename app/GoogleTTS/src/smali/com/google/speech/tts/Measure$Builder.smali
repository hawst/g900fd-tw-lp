.class public final Lcom/google/speech/tts/Measure$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Measure.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Measure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Measure;",
        "Lcom/google/speech/tts/Measure$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Measure;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Measure$Builder;
    .locals 1

    .prologue
    .line 262
    invoke-static {}, Lcom/google/speech/tts/Measure$Builder;->create()Lcom/google/speech/tts/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Measure$Builder;
    .locals 3

    .prologue
    .line 271
    new-instance v0, Lcom/google/speech/tts/Measure$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Measure$Builder;-><init>()V

    .line 272
    .local v0, "builder":Lcom/google/speech/tts/Measure$Builder;
    new-instance v1, Lcom/google/speech/tts/Measure;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Measure;-><init>(Lcom/google/speech/tts/Measure$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    .line 273
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure$Builder;->build()Lcom/google/speech/tts/Measure;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Measure;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Measure$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    invoke-static {v0}, Lcom/google/speech/tts/Measure$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 304
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure$Builder;->buildPartial()Lcom/google/speech/tts/Measure;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Measure;
    .locals 3

    .prologue
    .line 317
    iget-object v1, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    if-nez v1, :cond_0

    .line 318
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 321
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # getter for: Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Measure;->access$300(Lcom/google/speech/tts/Measure;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 322
    iget-object v1, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    iget-object v2, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # getter for: Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Measure;->access$300(Lcom/google/speech/tts/Measure;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Measure;->access$302(Lcom/google/speech/tts/Measure;Ljava/util/List;)Ljava/util/List;

    .line 325
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    .line 326
    .local v0, "returnMe":Lcom/google/speech/tts/Measure;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    .line 327
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure$Builder;->clone()Lcom/google/speech/tts/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure$Builder;->clone()Lcom/google/speech/tts/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Measure$Builder;
    .locals 2

    .prologue
    .line 290
    invoke-static {}, Lcom/google/speech/tts/Measure$Builder;->create()Lcom/google/speech/tts/Measure$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Measure$Builder;->mergeFrom(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/speech/tts/Measure$Builder;->clone()Lcom/google/speech/tts/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    invoke-virtual {v0}, Lcom/google/speech/tts/Measure;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeCardinal(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Measure$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Cardinal;

    .prologue
    .line 531
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    invoke-virtual {v0}, Lcom/google/speech/tts/Measure;->hasCardinal()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # getter for: Lcom/google/speech/tts/Measure;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v0}, Lcom/google/speech/tts/Measure;->access$900(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Cardinal;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Cardinal;->getDefaultInstance()Lcom/google/speech/tts/Cardinal;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 533
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    iget-object v1, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # getter for: Lcom/google/speech/tts/Measure;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v1}, Lcom/google/speech/tts/Measure;->access$900(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Cardinal;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Cardinal;->newBuilder(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Cardinal$Builder;->mergeFrom(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Cardinal$Builder;->buildPartial()Lcom/google/speech/tts/Cardinal;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Measure;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$902(Lcom/google/speech/tts/Measure;Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal;

    .line 538
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Measure;->hasCardinal:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$802(Lcom/google/speech/tts/Measure;Z)Z

    .line 539
    return-object p0

    .line 536
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # setter for: Lcom/google/speech/tts/Measure;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Measure;->access$902(Lcom/google/speech/tts/Measure;Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal;

    goto :goto_0
.end method

.method public mergeDecimal(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Measure$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    invoke-virtual {v0}, Lcom/google/speech/tts/Measure;->hasDecimal()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # getter for: Lcom/google/speech/tts/Measure;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0}, Lcom/google/speech/tts/Measure;->access$500(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Decimal;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Decimal;->getDefaultInstance()Lcom/google/speech/tts/Decimal;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 459
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    iget-object v1, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # getter for: Lcom/google/speech/tts/Measure;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v1}, Lcom/google/speech/tts/Measure;->access$500(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Decimal;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Decimal;->newBuilder(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Decimal$Builder;->mergeFrom(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Decimal$Builder;->buildPartial()Lcom/google/speech/tts/Decimal;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Measure;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$502(Lcom/google/speech/tts/Measure;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;

    .line 464
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Measure;->hasDecimal:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$402(Lcom/google/speech/tts/Measure;Z)Z

    .line 465
    return-object p0

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # setter for: Lcom/google/speech/tts/Measure;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Measure;->access$502(Lcom/google/speech/tts/Measure;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;

    goto :goto_0
.end method

.method public mergeFraction(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Measure$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Fraction;

    .prologue
    .line 494
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    invoke-virtual {v0}, Lcom/google/speech/tts/Measure;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # getter for: Lcom/google/speech/tts/Measure;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v0}, Lcom/google/speech/tts/Measure;->access$700(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Fraction;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Fraction;->getDefaultInstance()Lcom/google/speech/tts/Fraction;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 496
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    iget-object v1, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # getter for: Lcom/google/speech/tts/Measure;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v1}, Lcom/google/speech/tts/Measure;->access$700(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Fraction;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Fraction;->newBuilder(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Fraction$Builder;->mergeFrom(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Fraction$Builder;->buildPartial()Lcom/google/speech/tts/Fraction;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Measure;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$702(Lcom/google/speech/tts/Measure;Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction;

    .line 501
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Measure;->hasFraction:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$602(Lcom/google/speech/tts/Measure;Z)Z

    .line 502
    return-object p0

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # setter for: Lcom/google/speech/tts/Measure;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Measure;->access$702(Lcom/google/speech/tts/Measure;Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 262
    check-cast p1, Lcom/google/speech/tts/Measure;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Measure$Builder;->mergeFrom(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Measure;

    .prologue
    .line 331
    invoke-static {}, Lcom/google/speech/tts/Measure;->getDefaultInstance()Lcom/google/speech/tts/Measure;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-object p0

    .line 332
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->hasDecimal()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 333
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->getDecimal()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Measure$Builder;->mergeDecimal(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Measure$Builder;

    .line 335
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 336
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->getFraction()Lcom/google/speech/tts/Fraction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Measure$Builder;->mergeFraction(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Measure$Builder;

    .line 338
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->hasCardinal()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 339
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->getCardinal()Lcom/google/speech/tts/Cardinal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Measure$Builder;->mergeCardinal(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Measure$Builder;

    .line 341
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->hasUnits()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 342
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->getUnits()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Measure$Builder;->setUnits(Ljava/lang/String;)Lcom/google/speech/tts/Measure$Builder;

    .line 344
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 345
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->getStyle()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Measure$Builder;->setStyle(I)Lcom/google/speech/tts/Measure$Builder;

    .line 347
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 348
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Measure$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Measure$Builder;

    .line 350
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->hasPreserveOrder()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 351
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->getPreserveOrder()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Measure$Builder;->setPreserveOrder(Z)Lcom/google/speech/tts/Measure$Builder;

    .line 353
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 354
    invoke-virtual {p1}, Lcom/google/speech/tts/Measure;->getCodeSwitch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Measure$Builder;->setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Measure$Builder;

    .line 356
    :cond_9
    # getter for: Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Measure;->access$300(Lcom/google/speech/tts/Measure;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # getter for: Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Measure;->access$300(Lcom/google/speech/tts/Measure;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 358
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$302(Lcom/google/speech/tts/Measure;Ljava/util/List;)Ljava/util/List;

    .line 360
    :cond_a
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # getter for: Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Measure;->access$300(Lcom/google/speech/tts/Measure;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Measure;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Measure;->access$300(Lcom/google/speech/tts/Measure;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setCardinal(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Measure$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Cardinal;

    .prologue
    .line 518
    if-nez p1, :cond_0

    .line 519
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 521
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Measure;->hasCardinal:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$802(Lcom/google/speech/tts/Measure;Z)Z

    .line 522
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # setter for: Lcom/google/speech/tts/Measure;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Measure;->access$902(Lcom/google/speech/tts/Measure;Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal;

    .line 523
    return-object p0
.end method

.method public setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Measure$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 633
    if-nez p1, :cond_0

    .line 634
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 636
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Measure;->hasCodeSwitch:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$1802(Lcom/google/speech/tts/Measure;Z)Z

    .line 637
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # setter for: Lcom/google/speech/tts/Measure;->codeSwitch_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Measure;->access$1902(Lcom/google/speech/tts/Measure;Ljava/lang/String;)Ljava/lang/String;

    .line 638
    return-object p0
.end method

.method public setDecimal(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Measure$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 444
    if-nez p1, :cond_0

    .line 445
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Measure;->hasDecimal:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$402(Lcom/google/speech/tts/Measure;Z)Z

    .line 448
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # setter for: Lcom/google/speech/tts/Measure;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Measure;->access$502(Lcom/google/speech/tts/Measure;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;

    .line 449
    return-object p0
.end method

.method public setFraction(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Measure$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Fraction;

    .prologue
    .line 481
    if-nez p1, :cond_0

    .line 482
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Measure;->hasFraction:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$602(Lcom/google/speech/tts/Measure;Z)Z

    .line 485
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # setter for: Lcom/google/speech/tts/Measure;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Measure;->access$702(Lcom/google/speech/tts/Measure;Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction;

    .line 486
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Measure$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 594
    if-nez p1, :cond_0

    .line 595
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 597
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Measure;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$1402(Lcom/google/speech/tts/Measure;Z)Z

    .line 598
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # setter for: Lcom/google/speech/tts/Measure;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Measure;->access$1502(Lcom/google/speech/tts/Measure;Ljava/lang/String;)Ljava/lang/String;

    .line 599
    return-object p0
.end method

.method public setPreserveOrder(Z)Lcom/google/speech/tts/Measure$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Measure;->hasPreserveOrder:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$1602(Lcom/google/speech/tts/Measure;Z)Z

    .line 616
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # setter for: Lcom/google/speech/tts/Measure;->preserveOrder_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Measure;->access$1702(Lcom/google/speech/tts/Measure;Z)Z

    .line 617
    return-object p0
.end method

.method public setStyle(I)Lcom/google/speech/tts/Measure$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Measure;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$1202(Lcom/google/speech/tts/Measure;Z)Z

    .line 577
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # setter for: Lcom/google/speech/tts/Measure;->style_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Measure;->access$1302(Lcom/google/speech/tts/Measure;I)I

    .line 578
    return-object p0
.end method

.method public setUnits(Ljava/lang/String;)Lcom/google/speech/tts/Measure$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 555
    if-nez p1, :cond_0

    .line 556
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 558
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Measure;->hasUnits:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Measure;->access$1002(Lcom/google/speech/tts/Measure;Z)Z

    .line 559
    iget-object v0, p0, Lcom/google/speech/tts/Measure$Builder;->result:Lcom/google/speech/tts/Measure;

    # setter for: Lcom/google/speech/tts/Measure;->units_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Measure;->access$1102(Lcom/google/speech/tts/Measure;Ljava/lang/String;)Ljava/lang/String;

    .line 560
    return-object p0
.end method
