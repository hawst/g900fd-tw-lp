.class public final enum Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;
.super Ljava/lang/Enum;
.source "LexiconProto.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto$Morphology;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PartOfSpeech"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

.field public static final enum ADJECTIVE:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

.field public static final enum ADVERB:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

.field public static final enum CONJUNCTION:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

.field public static final enum INTERJECTION:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

.field public static final enum NOUN:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

.field public static final enum PREPOSITION:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

.field public static final enum PRONOUN:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

.field public static final enum UNKNOWN_POS:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

.field public static final enum VERB:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1393
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    const-string v1, "UNKNOWN_POS"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->UNKNOWN_POS:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .line 1394
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    const-string v1, "NOUN"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->NOUN:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .line 1395
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    const-string v1, "PRONOUN"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->PRONOUN:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .line 1396
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    const-string v1, "ADJECTIVE"

    invoke-direct {v0, v1, v8, v8, v8}, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->ADJECTIVE:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .line 1397
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    const-string v1, "VERB"

    invoke-direct {v0, v1, v9, v9, v9}, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->VERB:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .line 1398
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    const-string v1, "ADVERB"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->ADVERB:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .line 1399
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    const-string v1, "PREPOSITION"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->PREPOSITION:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .line 1400
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    const-string v1, "CONJUNCTION"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->CONJUNCTION:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .line 1401
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    const-string v1, "INTERJECTION"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->INTERJECTION:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .line 1391
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->UNKNOWN_POS:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->NOUN:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->PRONOUN:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->ADJECTIVE:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->VERB:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->ADVERB:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->PREPOSITION:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->CONJUNCTION:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->INTERJECTION:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->$VALUES:[Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .line 1427
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech$1;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1436
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1437
    iput p3, p0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->index:I

    .line 1438
    iput p4, p0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->value:I

    .line 1439
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1391
    const-class v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;
    .locals 1

    .prologue
    .line 1391
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->$VALUES:[Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    invoke-virtual {v0}, [Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 1405
    iget v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;->value:I

    return v0
.end method
