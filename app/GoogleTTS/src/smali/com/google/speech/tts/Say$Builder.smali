.class public final Lcom/google/speech/tts/Say$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Say.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Say;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Say;",
        "Lcom/google/speech/tts/Say$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Say;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 471
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Say$Builder;
    .locals 1

    .prologue
    .line 465
    invoke-static {}, Lcom/google/speech/tts/Say$Builder;->create()Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Say$Builder;
    .locals 3

    .prologue
    .line 474
    new-instance v0, Lcom/google/speech/tts/Say$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Say$Builder;-><init>()V

    .line 475
    .local v0, "builder":Lcom/google/speech/tts/Say$Builder;
    new-instance v1, Lcom/google/speech/tts/Say;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Say;-><init>(Lcom/google/speech/tts/Say$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    .line 476
    return-object v0
.end method


# virtual methods
.method public addWordSequence(Lcom/google/speech/tts/MarkupWord;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/MarkupWord;

    .prologue
    .line 1300
    if-nez p1, :cond_0

    .line 1301
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1303
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$300(Lcom/google/speech/tts/Say;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1304
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$302(Lcom/google/speech/tts/Say;Ljava/util/List;)Ljava/util/List;

    .line 1306
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$300(Lcom/google/speech/tts/Say;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1307
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/google/speech/tts/Say$Builder;->build()Lcom/google/speech/tts/Say;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Say;
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Say$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-static {v0}, Lcom/google/speech/tts/Say$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 507
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Say$Builder;->buildPartial()Lcom/google/speech/tts/Say;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Say;
    .locals 3

    .prologue
    .line 520
    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    if-nez v1, :cond_0

    .line 521
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 524
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$300(Lcom/google/speech/tts/Say;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 525
    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v2, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Say;->access$300(Lcom/google/speech/tts/Say;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Say;->access$302(Lcom/google/speech/tts/Say;Ljava/util/List;)Ljava/util/List;

    .line 528
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    .line 529
    .local v0, "returnMe":Lcom/google/speech/tts/Say;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    .line 530
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/google/speech/tts/Say$Builder;->clone()Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/google/speech/tts/Say$Builder;->clone()Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Say$Builder;
    .locals 2

    .prologue
    .line 493
    invoke-static {}, Lcom/google/speech/tts/Say$Builder;->create()Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Say$Builder;->mergeFrom(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/google/speech/tts/Say$Builder;->clone()Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeAddress(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Address;

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasAddress()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->address_:Lcom/google/speech/tts/Address;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$2500(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Address;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Address;->getDefaultInstance()Lcom/google/speech/tts/Address;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1132
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->address_:Lcom/google/speech/tts/Address;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$2500(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Address;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Address;->newBuilder(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Address$Builder;->mergeFrom(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Address$Builder;->buildPartial()Lcom/google/speech/tts/Address;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->address_:Lcom/google/speech/tts/Address;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2502(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address;

    .line 1137
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasAddress:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2402(Lcom/google/speech/tts/Say;Z)Z

    .line 1138
    return-object p0

    .line 1135
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->address_:Lcom/google/speech/tts/Address;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$2502(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address;

    goto :goto_0
.end method

.method public mergeCardinal(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Cardinal;

    .prologue
    .line 850
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasCardinal()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$900(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Cardinal;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Cardinal;->getDefaultInstance()Lcom/google/speech/tts/Cardinal;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 852
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$900(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Cardinal;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Cardinal;->newBuilder(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Cardinal$Builder;->mergeFrom(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Cardinal$Builder;->buildPartial()Lcom/google/speech/tts/Cardinal;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal;

    .line 857
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasCardinal:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$802(Lcom/google/speech/tts/Say;Z)Z

    .line 858
    return-object p0

    .line 855
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal;

    goto :goto_0
.end method

.method public mergeDate(Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Date;

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->date_:Lcom/google/speech/tts/Date;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$2300(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Date;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Date;->getDefaultInstance()Lcom/google/speech/tts/Date;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1095
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->date_:Lcom/google/speech/tts/Date;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$2300(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Date;->newBuilder(Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Date$Builder;->mergeFrom(Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Date$Builder;->buildPartial()Lcom/google/speech/tts/Date;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->date_:Lcom/google/speech/tts/Date;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2302(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date;

    .line 1100
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasDate:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2202(Lcom/google/speech/tts/Say;Z)Z

    .line 1101
    return-object p0

    .line 1098
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->date_:Lcom/google/speech/tts/Date;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$2302(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date;

    goto :goto_0
.end method

.method public mergeDecimal(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 924
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasDecimal()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$1300(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Decimal;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Decimal;->getDefaultInstance()Lcom/google/speech/tts/Decimal;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 926
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$1300(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Decimal;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Decimal;->newBuilder(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Decimal$Builder;->mergeFrom(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Decimal$Builder;->buildPartial()Lcom/google/speech/tts/Decimal;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1302(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;

    .line 931
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasDecimal:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1202(Lcom/google/speech/tts/Say;Z)Z

    .line 932
    return-object p0

    .line 929
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$1302(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;

    goto :goto_0
.end method

.method public mergeElectronic(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Electronic;

    .prologue
    .line 1241
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasElectronic()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->electronic_:Lcom/google/speech/tts/Electronic;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$3100(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Electronic;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Electronic;->getDefaultInstance()Lcom/google/speech/tts/Electronic;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1243
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->electronic_:Lcom/google/speech/tts/Electronic;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$3100(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Electronic;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Electronic;->newBuilder(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Electronic$Builder;->mergeFrom(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Electronic$Builder;->buildPartial()Lcom/google/speech/tts/Electronic;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->electronic_:Lcom/google/speech/tts/Electronic;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$3102(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic;

    .line 1248
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasElectronic:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$3002(Lcom/google/speech/tts/Say;Z)Z

    .line 1249
    return-object p0

    .line 1246
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->electronic_:Lcom/google/speech/tts/Electronic;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$3102(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic;

    goto :goto_0
.end method

.method public mergeFraction(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Fraction;

    .prologue
    .line 961
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$1500(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Fraction;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Fraction;->getDefaultInstance()Lcom/google/speech/tts/Fraction;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 963
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$1500(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Fraction;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Fraction;->newBuilder(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Fraction$Builder;->mergeFrom(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Fraction$Builder;->buildPartial()Lcom/google/speech/tts/Fraction;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1502(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction;

    .line 968
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasFraction:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1402(Lcom/google/speech/tts/Say;Z)Z

    .line 969
    return-object p0

    .line 966
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$1502(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 465
    check-cast p1, Lcom/google/speech/tts/Say;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Say$Builder;->mergeFrom(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 534
    invoke-static {}, Lcom/google/speech/tts/Say;->getDefaultInstance()Lcom/google/speech/tts/Say;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 604
    :cond_0
    :goto_0
    return-object p0

    .line 535
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasText()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 536
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->setText(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    .line 538
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 539
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    .line 541
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasCardinal()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 542
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getCardinal()Lcom/google/speech/tts/Cardinal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeCardinal(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Say$Builder;

    .line 544
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasOrdinal()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 545
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getOrdinal()Lcom/google/speech/tts/Ordinal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeOrdinal(Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Say$Builder;

    .line 547
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasDecimal()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 548
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getDecimal()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeDecimal(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Say$Builder;

    .line 550
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 551
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getFraction()Lcom/google/speech/tts/Fraction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeFraction(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Say$Builder;

    .line 553
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasDigit()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 554
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getDigit()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->setDigit(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    .line 556
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 557
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getTime()Lcom/google/speech/tts/Time;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeTime(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Say$Builder;

    .line 559
    :cond_9
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasMeasure()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 560
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getMeasure()Lcom/google/speech/tts/Measure;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeMeasure(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Say$Builder;

    .line 562
    :cond_a
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 563
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getDate()Lcom/google/speech/tts/Date;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeDate(Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Say$Builder;

    .line 565
    :cond_b
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasAddress()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 566
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getAddress()Lcom/google/speech/tts/Address;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeAddress(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Say$Builder;

    .line 568
    :cond_c
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasTelephone()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 569
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getTelephone()Lcom/google/speech/tts/Telephone;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeTelephone(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Say$Builder;

    .line 571
    :cond_d
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasMoney()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 572
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getMoney()Lcom/google/speech/tts/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeMoney(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Say$Builder;

    .line 574
    :cond_e
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasElectronic()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 575
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getElectronic()Lcom/google/speech/tts/Electronic;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeElectronic(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Say$Builder;

    .line 577
    :cond_f
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasWords()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 578
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getWords()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->setWords(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    .line 580
    :cond_10
    # getter for: Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Say;->access$300(Lcom/google/speech/tts/Say;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 581
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$300(Lcom/google/speech/tts/Say;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 582
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$302(Lcom/google/speech/tts/Say;Ljava/util/List;)Ljava/util/List;

    .line 584
    :cond_11
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$300(Lcom/google/speech/tts/Say;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Say;->access$300(Lcom/google/speech/tts/Say;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 586
    :cond_12
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasLetters()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 587
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getLetters()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->setLetters(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    .line 589
    :cond_13
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasVerbatim()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 590
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getVerbatim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->setVerbatim(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    .line 592
    :cond_14
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasVoicemod()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 593
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getVoicemod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeVoicemod(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/Say$Builder;

    .line 595
    :cond_15
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasPause()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 596
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getPause()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->setPause(F)Lcom/google/speech/tts/Say$Builder;

    .line 598
    :cond_16
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasProsodicFeatures()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 599
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getProsodicFeatures()Lcom/google/speech/tts/ProsodicFeatures;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeProsodicFeatures(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/Say$Builder;

    .line 601
    :cond_17
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->hasLocation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602
    invoke-virtual {p1}, Lcom/google/speech/tts/Say;->getLocation()Llocation/unified/LocationDescriptor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Say$Builder;->mergeLocation(Llocation/unified/LocationDescriptor;)Lcom/google/speech/tts/Say$Builder;

    goto/16 :goto_0
.end method

.method public mergeLocation(Llocation/unified/LocationDescriptor;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Llocation/unified/LocationDescriptor;

    .prologue
    .line 1484
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasLocation()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->location_:Llocation/unified/LocationDescriptor;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$4500(Lcom/google/speech/tts/Say;)Llocation/unified/LocationDescriptor;

    move-result-object v0

    invoke-static {}, Llocation/unified/LocationDescriptor;->getDefaultInstance()Llocation/unified/LocationDescriptor;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1486
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->location_:Llocation/unified/LocationDescriptor;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$4500(Lcom/google/speech/tts/Say;)Llocation/unified/LocationDescriptor;

    move-result-object v1

    invoke-static {v1}, Llocation/unified/LocationDescriptor;->newBuilder(Llocation/unified/LocationDescriptor;)Llocation/unified/LocationDescriptor$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Llocation/unified/LocationDescriptor$Builder;->mergeFrom(Llocation/unified/LocationDescriptor;)Llocation/unified/LocationDescriptor$Builder;

    move-result-object v1

    invoke-virtual {v1}, Llocation/unified/LocationDescriptor$Builder;->buildPartial()Llocation/unified/LocationDescriptor;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->location_:Llocation/unified/LocationDescriptor;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$4502(Lcom/google/speech/tts/Say;Llocation/unified/LocationDescriptor;)Llocation/unified/LocationDescriptor;

    .line 1491
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasLocation:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$4402(Lcom/google/speech/tts/Say;Z)Z

    .line 1492
    return-object p0

    .line 1489
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->location_:Llocation/unified/LocationDescriptor;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$4502(Lcom/google/speech/tts/Say;Llocation/unified/LocationDescriptor;)Llocation/unified/LocationDescriptor;

    goto :goto_0
.end method

.method public mergeMeasure(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Measure;

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasMeasure()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->measure_:Lcom/google/speech/tts/Measure;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$2100(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Measure;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Measure;->getDefaultInstance()Lcom/google/speech/tts/Measure;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1058
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->measure_:Lcom/google/speech/tts/Measure;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$2100(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Measure;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Measure;->newBuilder(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Measure$Builder;->mergeFrom(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Measure$Builder;->buildPartial()Lcom/google/speech/tts/Measure;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->measure_:Lcom/google/speech/tts/Measure;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2102(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure;

    .line 1063
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasMeasure:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2002(Lcom/google/speech/tts/Say;Z)Z

    .line 1064
    return-object p0

    .line 1061
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->measure_:Lcom/google/speech/tts/Measure;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$2102(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure;

    goto :goto_0
.end method

.method public mergeMoney(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Money;

    .prologue
    .line 1204
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasMoney()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->money_:Lcom/google/speech/tts/Money;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$2900(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Money;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Money;->getDefaultInstance()Lcom/google/speech/tts/Money;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1206
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->money_:Lcom/google/speech/tts/Money;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$2900(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Money;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Money;->newBuilder(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Money$Builder;->mergeFrom(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Money$Builder;->buildPartial()Lcom/google/speech/tts/Money;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->money_:Lcom/google/speech/tts/Money;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money;

    .line 1211
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasMoney:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2802(Lcom/google/speech/tts/Say;Z)Z

    .line 1212
    return-object p0

    .line 1209
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->money_:Lcom/google/speech/tts/Money;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$2902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money;

    goto :goto_0
.end method

.method public mergeOrdinal(Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Ordinal;

    .prologue
    .line 887
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasOrdinal()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->ordinal_:Lcom/google/speech/tts/Ordinal;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$1100(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Ordinal;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Ordinal;->getDefaultInstance()Lcom/google/speech/tts/Ordinal;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 889
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->ordinal_:Lcom/google/speech/tts/Ordinal;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$1100(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Ordinal;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Ordinal;->newBuilder(Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Ordinal$Builder;->mergeFrom(Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Ordinal$Builder;->buildPartial()Lcom/google/speech/tts/Ordinal;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->ordinal_:Lcom/google/speech/tts/Ordinal;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1102(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal;

    .line 894
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasOrdinal:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1002(Lcom/google/speech/tts/Say;Z)Z

    .line 895
    return-object p0

    .line 892
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->ordinal_:Lcom/google/speech/tts/Ordinal;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$1102(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal;

    goto :goto_0
.end method

.method public mergeProsodicFeatures(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/ProsodicFeatures;

    .prologue
    .line 1447
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasProsodicFeatures()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->prosodicFeatures_:Lcom/google/speech/tts/ProsodicFeatures;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$4300(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/ProsodicFeatures;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/ProsodicFeatures;->getDefaultInstance()Lcom/google/speech/tts/ProsodicFeatures;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1449
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->prosodicFeatures_:Lcom/google/speech/tts/ProsodicFeatures;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$4300(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/ProsodicFeatures;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/ProsodicFeatures;->newBuilder(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->mergeFrom(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->buildPartial()Lcom/google/speech/tts/ProsodicFeatures;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->prosodicFeatures_:Lcom/google/speech/tts/ProsodicFeatures;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$4302(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/ProsodicFeatures;

    .line 1454
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasProsodicFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$4202(Lcom/google/speech/tts/Say;Z)Z

    .line 1455
    return-object p0

    .line 1452
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->prosodicFeatures_:Lcom/google/speech/tts/ProsodicFeatures;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$4302(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/ProsodicFeatures;

    goto :goto_0
.end method

.method public mergeTelephone(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Telephone;

    .prologue
    .line 1167
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasTelephone()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->telephone_:Lcom/google/speech/tts/Telephone;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$2700(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Telephone;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Telephone;->getDefaultInstance()Lcom/google/speech/tts/Telephone;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1169
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->telephone_:Lcom/google/speech/tts/Telephone;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$2700(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Telephone;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Telephone;->newBuilder(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Telephone$Builder;->mergeFrom(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Telephone$Builder;->buildPartial()Lcom/google/speech/tts/Telephone;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->telephone_:Lcom/google/speech/tts/Telephone;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2702(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone;

    .line 1174
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasTelephone:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2602(Lcom/google/speech/tts/Say;Z)Z

    .line 1175
    return-object p0

    .line 1172
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->telephone_:Lcom/google/speech/tts/Telephone;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$2702(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone;

    goto :goto_0
.end method

.method public mergeTime(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Time;

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->time_:Lcom/google/speech/tts/Time;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$1900(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Time;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Time;->getDefaultInstance()Lcom/google/speech/tts/Time;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1021
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->time_:Lcom/google/speech/tts/Time;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$1900(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Time;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Time;->newBuilder(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Time$Builder;->mergeFrom(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Time$Builder;->buildPartial()Lcom/google/speech/tts/Time;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->time_:Lcom/google/speech/tts/Time;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time;

    .line 1026
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasTime:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1802(Lcom/google/speech/tts/Say;Z)Z

    .line 1027
    return-object p0

    .line 1024
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->time_:Lcom/google/speech/tts/Time;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$1902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time;

    goto :goto_0
.end method

.method public mergeVoicemod(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/VoiceMod;

    .prologue
    .line 1392
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasVoicemod()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0}, Lcom/google/speech/tts/Say;->access$3900(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/VoiceMod;->getDefaultInstance()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1394
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    iget-object v1, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # getter for: Lcom/google/speech/tts/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v1}, Lcom/google/speech/tts/Say;->access$3900(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/VoiceMod;->newBuilder(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/VoiceMod$Builder;->mergeFrom(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/VoiceMod$Builder;->buildPartial()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$3902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;

    .line 1399
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasVoicemod:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$3802(Lcom/google/speech/tts/Say;Z)Z

    .line 1400
    return-object p0

    .line 1397
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$3902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;

    goto :goto_0
.end method

.method public setCardinal(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Cardinal;

    .prologue
    .line 837
    if-nez p1, :cond_0

    .line 838
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 840
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasCardinal:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$802(Lcom/google/speech/tts/Say;Z)Z

    .line 841
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal;

    .line 842
    return-object p0
.end method

.method public setDate(Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Date;

    .prologue
    .line 1080
    if-nez p1, :cond_0

    .line 1081
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1083
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasDate:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2202(Lcom/google/speech/tts/Say;Z)Z

    .line 1084
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->date_:Lcom/google/speech/tts/Date;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$2302(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date;

    .line 1085
    return-object p0
.end method

.method public setDecimal(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 911
    if-nez p1, :cond_0

    .line 912
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 914
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasDecimal:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1202(Lcom/google/speech/tts/Say;Z)Z

    .line 915
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$1302(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;

    .line 916
    return-object p0
.end method

.method public setDigit(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 985
    if-nez p1, :cond_0

    .line 986
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 988
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasDigit:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1602(Lcom/google/speech/tts/Say;Z)Z

    .line 989
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->digit_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$1702(Lcom/google/speech/tts/Say;Ljava/lang/String;)Ljava/lang/String;

    .line 990
    return-object p0
.end method

.method public setElectronic(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Electronic;

    .prologue
    .line 1228
    if-nez p1, :cond_0

    .line 1229
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1231
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasElectronic:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$3002(Lcom/google/speech/tts/Say;Z)Z

    .line 1232
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->electronic_:Lcom/google/speech/tts/Electronic;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$3102(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic;

    .line 1233
    return-object p0
.end method

.method public setFraction(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Fraction;

    .prologue
    .line 948
    if-nez p1, :cond_0

    .line 949
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 951
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasFraction:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1402(Lcom/google/speech/tts/Say;Z)Z

    .line 952
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$1502(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction;

    .line 953
    return-object p0
.end method

.method public setLetters(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1337
    if-nez p1, :cond_0

    .line 1338
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1340
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasLetters:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$3402(Lcom/google/speech/tts/Say;Z)Z

    .line 1341
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->letters_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$3502(Lcom/google/speech/tts/Say;Ljava/lang/String;)Ljava/lang/String;

    .line 1342
    return-object p0
.end method

.method public setLocation(Llocation/unified/LocationDescriptor$Builder;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "builderForValue"    # Llocation/unified/LocationDescriptor$Builder;

    .prologue
    .line 1479
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasLocation:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$4402(Lcom/google/speech/tts/Say;Z)Z

    .line 1480
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    invoke-virtual {p1}, Llocation/unified/LocationDescriptor$Builder;->build()Llocation/unified/LocationDescriptor;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Say;->location_:Llocation/unified/LocationDescriptor;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$4502(Lcom/google/speech/tts/Say;Llocation/unified/LocationDescriptor;)Llocation/unified/LocationDescriptor;

    .line 1481
    return-object p0
.end method

.method public setMeasure(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Measure;

    .prologue
    .line 1043
    if-nez p1, :cond_0

    .line 1044
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1046
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasMeasure:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2002(Lcom/google/speech/tts/Say;Z)Z

    .line 1047
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->measure_:Lcom/google/speech/tts/Measure;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$2102(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure;

    .line 1048
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 816
    if-nez p1, :cond_0

    .line 817
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 819
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$602(Lcom/google/speech/tts/Say;Z)Z

    .line 820
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$702(Lcom/google/speech/tts/Say;Ljava/lang/String;)Ljava/lang/String;

    .line 821
    return-object p0
.end method

.method public setOrdinal(Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Ordinal;

    .prologue
    .line 874
    if-nez p1, :cond_0

    .line 875
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 877
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasOrdinal:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1002(Lcom/google/speech/tts/Say;Z)Z

    .line 878
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->ordinal_:Lcom/google/speech/tts/Ordinal;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$1102(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal;

    .line 879
    return-object p0
.end method

.method public setPause(F)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 1416
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasPause:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$4002(Lcom/google/speech/tts/Say;Z)Z

    .line 1417
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->pause_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$4102(Lcom/google/speech/tts/Say;F)F

    .line 1418
    return-object p0
.end method

.method public setTelephone(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Telephone;

    .prologue
    .line 1154
    if-nez p1, :cond_0

    .line 1155
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1157
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasTelephone:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$2602(Lcom/google/speech/tts/Say;Z)Z

    .line 1158
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->telephone_:Lcom/google/speech/tts/Telephone;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$2702(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone;

    .line 1159
    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 795
    if-nez p1, :cond_0

    .line 796
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 798
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasText:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$402(Lcom/google/speech/tts/Say;Z)Z

    .line 799
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->text_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$502(Lcom/google/speech/tts/Say;Ljava/lang/String;)Ljava/lang/String;

    .line 800
    return-object p0
.end method

.method public setTime(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Time;

    .prologue
    .line 1006
    if-nez p1, :cond_0

    .line 1007
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1009
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasTime:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$1802(Lcom/google/speech/tts/Say;Z)Z

    .line 1010
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->time_:Lcom/google/speech/tts/Time;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$1902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time;

    .line 1011
    return-object p0
.end method

.method public setVerbatim(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1358
    if-nez p1, :cond_0

    .line 1359
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1361
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasVerbatim:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$3602(Lcom/google/speech/tts/Say;Z)Z

    .line 1362
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->verbatim_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$3702(Lcom/google/speech/tts/Say;Ljava/lang/String;)Ljava/lang/String;

    .line 1363
    return-object p0
.end method

.method public setVoicemod(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/VoiceMod;

    .prologue
    .line 1379
    if-nez p1, :cond_0

    .line 1380
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1382
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasVoicemod:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$3802(Lcom/google/speech/tts/Say;Z)Z

    .line 1383
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$3902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;

    .line 1384
    return-object p0
.end method

.method public setWords(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1265
    if-nez p1, :cond_0

    .line 1266
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1268
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Say;->hasWords:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Say;->access$3202(Lcom/google/speech/tts/Say;Z)Z

    .line 1269
    iget-object v0, p0, Lcom/google/speech/tts/Say$Builder;->result:Lcom/google/speech/tts/Say;

    # setter for: Lcom/google/speech/tts/Say;->words_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Say;->access$3302(Lcom/google/speech/tts/Say;Ljava/lang/String;)Ljava/lang/String;

    .line 1270
    return-object p0
.end method
