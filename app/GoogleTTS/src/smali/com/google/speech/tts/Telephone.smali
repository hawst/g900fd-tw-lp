.class public final Lcom/google/speech/tts/Telephone;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Telephone.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Telephone$1;,
        Lcom/google/speech/tts/Telephone$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Telephone;


# instance fields
.field private codeSwitch_:Ljava/lang/String;

.field private countryCode_:Ljava/lang/String;

.field private extension_:Ljava/lang/String;

.field private fieldOrder_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasCodeSwitch:Z

.field private hasCountryCode:Z

.field private hasExtension:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasPreserveOrder:Z

.field private hasStyle:Z

.field private memoizedSerializedSize:I

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private numberPart_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private preserveOrder_:Z

.field private style_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 612
    new-instance v0, Lcom/google/speech/tts/Telephone;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Telephone;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Telephone;->defaultInstance:Lcom/google/speech/tts/Telephone;

    .line 613
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 614
    sget-object v0, Lcom/google/speech/tts/Telephone;->defaultInstance:Lcom/google/speech/tts/Telephone;

    invoke-direct {v0}, Lcom/google/speech/tts/Telephone;->initFields()V

    .line 615
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Telephone;->countryCode_:Ljava/lang/String;

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Telephone;->extension_:Ljava/lang/String;

    .line 51
    iput v1, p0, Lcom/google/speech/tts/Telephone;->style_:I

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Telephone;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 65
    iput-boolean v1, p0, Lcom/google/speech/tts/Telephone;->preserveOrder_:Z

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Telephone;->codeSwitch_:Ljava/lang/String;

    .line 78
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;

    .line 123
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Telephone;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Telephone;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Telephone$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Telephone$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Telephone;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Telephone;->countryCode_:Ljava/lang/String;

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Telephone;->extension_:Ljava/lang/String;

    .line 51
    iput v1, p0, Lcom/google/speech/tts/Telephone;->style_:I

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Telephone;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 65
    iput-boolean v1, p0, Lcom/google/speech/tts/Telephone;->preserveOrder_:Z

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Telephone;->codeSwitch_:Ljava/lang/String;

    .line 78
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;

    .line 123
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Telephone;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Telephone;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Telephone;->style_:I

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/Telephone;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Telephone;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/Telephone;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Telephone;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/Telephone;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Telephone;->hasPreserveOrder:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/Telephone;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Telephone;->preserveOrder_:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/Telephone;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Telephone;->hasCodeSwitch:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/Telephone;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Telephone;->codeSwitch_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Telephone;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Telephone;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/speech/tts/Telephone;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Telephone;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Telephone;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Telephone;->hasCountryCode:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Telephone;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Telephone;->countryCode_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Telephone;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Telephone;->hasExtension:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Telephone;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Telephone;->extension_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Telephone;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Telephone;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Telephone;->hasStyle:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Telephone;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Telephone;->defaultInstance:Lcom/google/speech/tts/Telephone;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Telephone$Builder;
    .locals 1

    .prologue
    .line 242
    # invokes: Lcom/google/speech/tts/Telephone$Builder;->create()Lcom/google/speech/tts/Telephone$Builder;
    invoke-static {}, Lcom/google/speech/tts/Telephone$Builder;->access$100()Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Telephone;

    .prologue
    .line 245
    invoke-static {}, Lcom/google/speech/tts/Telephone;->newBuilder()Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Telephone$Builder;->mergeFrom(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCodeSwitch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/speech/tts/Telephone;->codeSwitch_:Ljava/lang/String;

    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Telephone;->countryCode_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getDefaultInstanceForType()Lcom/google/speech/tts/Telephone;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Telephone;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Telephone;->defaultInstance:Lcom/google/speech/tts/Telephone;

    return-object v0
.end method

.method public getExtension()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/speech/tts/Telephone;->extension_:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldOrderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/speech/tts/Telephone;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getNumberPartList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;

    return-object v0
.end method

.method public getPreserveOrder()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/speech/tts/Telephone;->preserveOrder_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 125
    iget v3, p0, Lcom/google/speech/tts/Telephone;->memoizedSerializedSize:I

    .line 126
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 172
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 128
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 129
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->hasCountryCode()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 130
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getCountryCode()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 134
    :cond_1
    const/4 v0, 0x0

    .line 135
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getNumberPartList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 136
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 138
    goto :goto_1

    .line 139
    .end local v1    # "element":Ljava/lang/String;
    :cond_2
    add-int/2addr v3, v0

    .line 140
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getNumberPartList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 142
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->hasExtension()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 143
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getExtension()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 146
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->hasStyle()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 147
    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getStyle()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 150
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->hasMorphosyntacticFeatures()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 151
    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 154
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->hasPreserveOrder()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 155
    const/4 v5, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getPreserveOrder()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 158
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->hasCodeSwitch()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 159
    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getCodeSwitch()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 163
    :cond_7
    const/4 v0, 0x0

    .line 164
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 165
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 167
    goto :goto_2

    .line 168
    .end local v1    # "element":Ljava/lang/String;
    :cond_8
    add-int/2addr v3, v0

    .line 169
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 171
    iput v3, p0, Lcom/google/speech/tts/Telephone;->memoizedSerializedSize:I

    move v4, v3

    .line 172
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getStyle()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/speech/tts/Telephone;->style_:I

    return v0
.end method

.method public hasCodeSwitch()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/google/speech/tts/Telephone;->hasCodeSwitch:Z

    return v0
.end method

.method public hasCountryCode()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Telephone;->hasCountryCode:Z

    return v0
.end method

.method public hasExtension()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/speech/tts/Telephone;->hasExtension:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/speech/tts/Telephone;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasPreserveOrder()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/google/speech/tts/Telephone;->hasPreserveOrder:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/speech/tts/Telephone;->hasStyle:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->toBuilder()Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Telephone$Builder;
    .locals 1

    .prologue
    .line 247
    invoke-static {p0}, Lcom/google/speech/tts/Telephone;->newBuilder(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getSerializedSize()I

    .line 97
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->hasCountryCode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 98
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getNumberPartList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 101
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 103
    .end local v0    # "element":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->hasExtension()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 104
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getExtension()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 106
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 107
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getStyle()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 109
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 110
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 112
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->hasPreserveOrder()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 113
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getPreserveOrder()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 115
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->hasCodeSwitch()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 116
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getCodeSwitch()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 118
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone;->getFieldOrderList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 119
    .restart local v0    # "element":Ljava/lang/String;
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_1

    .line 121
    .end local v0    # "element":Ljava/lang/String;
    :cond_7
    return-void
.end method
