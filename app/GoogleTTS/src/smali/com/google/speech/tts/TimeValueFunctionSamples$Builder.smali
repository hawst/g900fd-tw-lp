.class public final Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "TimeValueFunctionSamples.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/TimeValueFunctionSamples;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/TimeValueFunctionSamples;",
        "Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/TimeValueFunctionSamples;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-static {}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->create()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
    .locals 3

    .prologue
    .line 156
    new-instance v0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;-><init>()V

    .line 157
    .local v0, "builder":Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
    new-instance v1, Lcom/google/speech/tts/TimeValueFunctionSamples;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/TimeValueFunctionSamples;-><init>(Lcom/google/speech/tts/TimeValueFunctionSamples$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->result:Lcom/google/speech/tts/TimeValueFunctionSamples;

    .line 158
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->build()Lcom/google/speech/tts/TimeValueFunctionSamples;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/TimeValueFunctionSamples;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->result:Lcom/google/speech/tts/TimeValueFunctionSamples;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->result:Lcom/google/speech/tts/TimeValueFunctionSamples;

    invoke-static {v0}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 189
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->buildPartial()Lcom/google/speech/tts/TimeValueFunctionSamples;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/TimeValueFunctionSamples;
    .locals 3

    .prologue
    .line 202
    iget-object v1, p0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->result:Lcom/google/speech/tts/TimeValueFunctionSamples;

    if-nez v1, :cond_0

    .line 203
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->result:Lcom/google/speech/tts/TimeValueFunctionSamples;

    .line 207
    .local v0, "returnMe":Lcom/google/speech/tts/TimeValueFunctionSamples;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->result:Lcom/google/speech/tts/TimeValueFunctionSamples;

    .line 208
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->clone()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->clone()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
    .locals 2

    .prologue
    .line 175
    invoke-static {}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->create()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->result:Lcom/google/speech/tts/TimeValueFunctionSamples;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->mergeFrom(Lcom/google/speech/tts/TimeValueFunctionSamples;)Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->clone()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->result:Lcom/google/speech/tts/TimeValueFunctionSamples;

    invoke-virtual {v0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 147
    check-cast p1, Lcom/google/speech/tts/TimeValueFunctionSamples;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->mergeFrom(Lcom/google/speech/tts/TimeValueFunctionSamples;)Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/TimeValueFunctionSamples;)Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/TimeValueFunctionSamples;

    .prologue
    .line 212
    invoke-static {}, Lcom/google/speech/tts/TimeValueFunctionSamples;->getDefaultInstance()Lcom/google/speech/tts/TimeValueFunctionSamples;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-object p0

    .line 213
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/TimeValueFunctionSamples;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    invoke-virtual {p1}, Lcom/google/speech/tts/TimeValueFunctionSamples;->getTime()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->setTime(F)Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    .line 216
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/TimeValueFunctionSamples;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {p1}, Lcom/google/speech/tts/TimeValueFunctionSamples;->getValue()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->setValue(F)Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    goto :goto_0
.end method

.method public setTime(F)Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->result:Lcom/google/speech/tts/TimeValueFunctionSamples;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/TimeValueFunctionSamples;->hasTime:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/TimeValueFunctionSamples;->access$302(Lcom/google/speech/tts/TimeValueFunctionSamples;Z)Z

    .line 259
    iget-object v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->result:Lcom/google/speech/tts/TimeValueFunctionSamples;

    # setter for: Lcom/google/speech/tts/TimeValueFunctionSamples;->time_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/TimeValueFunctionSamples;->access$402(Lcom/google/speech/tts/TimeValueFunctionSamples;F)F

    .line 260
    return-object p0
.end method

.method public setValue(F)Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->result:Lcom/google/speech/tts/TimeValueFunctionSamples;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/TimeValueFunctionSamples;->hasValue:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/TimeValueFunctionSamples;->access$502(Lcom/google/speech/tts/TimeValueFunctionSamples;Z)Z

    .line 277
    iget-object v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->result:Lcom/google/speech/tts/TimeValueFunctionSamples;

    # setter for: Lcom/google/speech/tts/TimeValueFunctionSamples;->value_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/TimeValueFunctionSamples;->access$602(Lcom/google/speech/tts/TimeValueFunctionSamples;F)F

    .line 278
    return-object p0
.end method
