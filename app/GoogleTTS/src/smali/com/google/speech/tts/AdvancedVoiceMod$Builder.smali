.class public final Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AdvancedVoiceMod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/AdvancedVoiceMod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/AdvancedVoiceMod;",
        "Lcom/google/speech/tts/AdvancedVoiceMod$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/AdvancedVoiceMod;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 1

    .prologue
    .line 278
    invoke-static {}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->create()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 3

    .prologue
    .line 287
    new-instance v0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;-><init>()V

    .line 288
    .local v0, "builder":Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    new-instance v1, Lcom/google/speech/tts/AdvancedVoiceMod;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/AdvancedVoiceMod;-><init>(Lcom/google/speech/tts/AdvancedVoiceMod$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    .line 289
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->build()Lcom/google/speech/tts/AdvancedVoiceMod;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/AdvancedVoiceMod;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    invoke-static {v0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 320
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->buildPartial()Lcom/google/speech/tts/AdvancedVoiceMod;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/AdvancedVoiceMod;
    .locals 3

    .prologue
    .line 333
    iget-object v1, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    if-nez v1, :cond_0

    .line 334
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 337
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # getter for: Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$300(Lcom/google/speech/tts/AdvancedVoiceMod;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 338
    iget-object v1, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    iget-object v2, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # getter for: Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$300(Lcom/google/speech/tts/AdvancedVoiceMod;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$302(Lcom/google/speech/tts/AdvancedVoiceMod;Ljava/util/List;)Ljava/util/List;

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    .line 342
    .local v0, "returnMe":Lcom/google/speech/tts/AdvancedVoiceMod;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    .line 343
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->clone()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->clone()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 2

    .prologue
    .line 306
    invoke-static {}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->create()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->mergeFrom(Lcom/google/speech/tts/AdvancedVoiceMod;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->clone()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    invoke-virtual {v0}, Lcom/google/speech/tts/AdvancedVoiceMod;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 278
    check-cast p1, Lcom/google/speech/tts/AdvancedVoiceMod;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->mergeFrom(Lcom/google/speech/tts/AdvancedVoiceMod;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/AdvancedVoiceMod;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/AdvancedVoiceMod;

    .prologue
    .line 347
    invoke-static {}, Lcom/google/speech/tts/AdvancedVoiceMod;->getDefaultInstance()Lcom/google/speech/tts/AdvancedVoiceMod;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 381
    :cond_0
    :goto_0
    return-object p0

    .line 348
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasWordIndex()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 349
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->getWordIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->setWordIndex(I)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    .line 351
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasItem()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 352
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->getItem()Lcom/google/speech/tts/VoiceModLinguisticItemType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->setItem(Lcom/google/speech/tts/VoiceModLinguisticItemType;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    .line 354
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasItemIndex()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 355
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->getItemIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->setItemIndex(I)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    .line 357
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasInterval()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 358
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->getInterval()Lcom/google/speech/tts/VoiceModIntervalType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->setInterval(Lcom/google/speech/tts/VoiceModIntervalType;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    .line 360
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasSpanId()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 361
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->getSpanId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->setSpanId(I)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    .line 363
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasTransform()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 364
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->getTransform()Lcom/google/speech/tts/VoiceModTransformType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->setTransform(Lcom/google/speech/tts/VoiceModTransformType;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    .line 366
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasFactor()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 367
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->getFactor()Lcom/google/speech/tts/VoiceModFactorType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->setFactor(Lcom/google/speech/tts/VoiceModFactorType;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    .line 369
    :cond_8
    # getter for: Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$300(Lcom/google/speech/tts/AdvancedVoiceMod;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 370
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # getter for: Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$300(Lcom/google/speech/tts/AdvancedVoiceMod;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 371
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$302(Lcom/google/speech/tts/AdvancedVoiceMod;Ljava/util/List;)Ljava/util/List;

    .line 373
    :cond_9
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # getter for: Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$300(Lcom/google/speech/tts/AdvancedVoiceMod;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$300(Lcom/google/speech/tts/AdvancedVoiceMod;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 375
    :cond_a
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasMeanParams()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 376
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->getMeanParams()Lcom/google/speech/tts/MeanModelParams;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->mergeMeanParams(Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    .line 378
    :cond_b
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasPiecewiseParams()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 379
    invoke-virtual {p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->getPiecewiseParams()Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->mergePiecewiseParams(Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    goto/16 :goto_0
.end method

.method public mergeMeanParams(Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/MeanModelParams;

    .prologue
    .line 682
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    invoke-virtual {v0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasMeanParams()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # getter for: Lcom/google/speech/tts/AdvancedVoiceMod;->meanParams_:Lcom/google/speech/tts/MeanModelParams;
    invoke-static {v0}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1900(Lcom/google/speech/tts/AdvancedVoiceMod;)Lcom/google/speech/tts/MeanModelParams;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/MeanModelParams;->getDefaultInstance()Lcom/google/speech/tts/MeanModelParams;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 684
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    iget-object v1, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # getter for: Lcom/google/speech/tts/AdvancedVoiceMod;->meanParams_:Lcom/google/speech/tts/MeanModelParams;
    invoke-static {v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1900(Lcom/google/speech/tts/AdvancedVoiceMod;)Lcom/google/speech/tts/MeanModelParams;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/MeanModelParams;->newBuilder(Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/MeanModelParams$Builder;->mergeFrom(Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/MeanModelParams$Builder;->buildPartial()Lcom/google/speech/tts/MeanModelParams;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->meanParams_:Lcom/google/speech/tts/MeanModelParams;
    invoke-static {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1902(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/MeanModelParams;

    .line 689
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->hasMeanParams:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1802(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z

    .line 690
    return-object p0

    .line 687
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->meanParams_:Lcom/google/speech/tts/MeanModelParams;
    invoke-static {v0, p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1902(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/MeanModelParams;

    goto :goto_0
.end method

.method public mergePiecewiseParams(Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/PiecewiseModelParams;

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    invoke-virtual {v0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasPiecewiseParams()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # getter for: Lcom/google/speech/tts/AdvancedVoiceMod;->piecewiseParams_:Lcom/google/speech/tts/PiecewiseModelParams;
    invoke-static {v0}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$2100(Lcom/google/speech/tts/AdvancedVoiceMod;)Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/PiecewiseModelParams;->getDefaultInstance()Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 721
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    iget-object v1, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # getter for: Lcom/google/speech/tts/AdvancedVoiceMod;->piecewiseParams_:Lcom/google/speech/tts/PiecewiseModelParams;
    invoke-static {v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$2100(Lcom/google/speech/tts/AdvancedVoiceMod;)Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/PiecewiseModelParams;->newBuilder(Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->mergeFrom(Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->buildPartial()Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->piecewiseParams_:Lcom/google/speech/tts/PiecewiseModelParams;
    invoke-static {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$2102(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/PiecewiseModelParams;

    .line 726
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->hasPiecewiseParams:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$2002(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z

    .line 727
    return-object p0

    .line 724
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->piecewiseParams_:Lcom/google/speech/tts/PiecewiseModelParams;
    invoke-static {v0, p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$2102(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/PiecewiseModelParams;

    goto :goto_0
.end method

.method public setFactor(Lcom/google/speech/tts/VoiceModFactorType;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/VoiceModFactorType;

    .prologue
    .line 597
    if-nez p1, :cond_0

    .line 598
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->hasFactor:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1602(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z

    .line 601
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->factor_:Lcom/google/speech/tts/VoiceModFactorType;
    invoke-static {v0, p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1702(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/VoiceModFactorType;)Lcom/google/speech/tts/VoiceModFactorType;

    .line 602
    return-object p0
.end method

.method public setInterval(Lcom/google/speech/tts/VoiceModIntervalType;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/VoiceModIntervalType;

    .prologue
    .line 537
    if-nez p1, :cond_0

    .line 538
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 540
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->hasInterval:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1002(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z

    .line 541
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->interval_:Lcom/google/speech/tts/VoiceModIntervalType;
    invoke-static {v0, p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1102(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/VoiceModIntervalType;)Lcom/google/speech/tts/VoiceModIntervalType;

    .line 542
    return-object p0
.end method

.method public setItem(Lcom/google/speech/tts/VoiceModLinguisticItemType;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/VoiceModLinguisticItemType;

    .prologue
    .line 498
    if-nez p1, :cond_0

    .line 499
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 501
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->hasItem:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$602(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z

    .line 502
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->item_:Lcom/google/speech/tts/VoiceModLinguisticItemType;
    invoke-static {v0, p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$702(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/VoiceModLinguisticItemType;)Lcom/google/speech/tts/VoiceModLinguisticItemType;

    .line 503
    return-object p0
.end method

.method public setItemIndex(I)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->hasItemIndex:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$802(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z

    .line 520
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->itemIndex_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$902(Lcom/google/speech/tts/AdvancedVoiceMod;I)I

    .line 521
    return-object p0
.end method

.method public setSpanId(I)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->hasSpanId:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1202(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z

    .line 559
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->spanId_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1302(Lcom/google/speech/tts/AdvancedVoiceMod;I)I

    .line 560
    return-object p0
.end method

.method public setTransform(Lcom/google/speech/tts/VoiceModTransformType;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/VoiceModTransformType;

    .prologue
    .line 576
    if-nez p1, :cond_0

    .line 577
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->hasTransform:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1402(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z

    .line 580
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->transform_:Lcom/google/speech/tts/VoiceModTransformType;
    invoke-static {v0, p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$1502(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/VoiceModTransformType;)Lcom/google/speech/tts/VoiceModTransformType;

    .line 581
    return-object p0
.end method

.method public setWordIndex(I)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->hasWordIndex:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$402(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z

    .line 481
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->result:Lcom/google/speech/tts/AdvancedVoiceMod;

    # setter for: Lcom/google/speech/tts/AdvancedVoiceMod;->wordIndex_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/AdvancedVoiceMod;->access$502(Lcom/google/speech/tts/AdvancedVoiceMod;I)I

    .line 482
    return-object p0
.end method
