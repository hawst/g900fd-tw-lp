.class public final Lcom/google/speech/tts/MeanModelParams;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MeanModelParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/MeanModelParams$1;,
        Lcom/google/speech/tts/MeanModelParams$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/MeanModelParams;


# instance fields
.field private deviation_:F

.field private hasDeviation:Z

.field private hasMean:Z

.field private mean_:F

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 289
    new-instance v0, Lcom/google/speech/tts/MeanModelParams;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/MeanModelParams;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/MeanModelParams;->defaultInstance:Lcom/google/speech/tts/MeanModelParams;

    .line 290
    invoke-static {}, Lcom/google/speech/tts/AdvancedVoiceModProto;->internalForceInit()V

    .line 291
    sget-object v0, Lcom/google/speech/tts/MeanModelParams;->defaultInstance:Lcom/google/speech/tts/MeanModelParams;

    invoke-direct {v0}, Lcom/google/speech/tts/MeanModelParams;->initFields()V

    .line 292
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/tts/MeanModelParams;->mean_:F

    .line 32
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/speech/tts/MeanModelParams;->deviation_:F

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/MeanModelParams;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/MeanModelParams;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/MeanModelParams$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/MeanModelParams$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/MeanModelParams;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/tts/MeanModelParams;->mean_:F

    .line 32
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/speech/tts/MeanModelParams;->deviation_:F

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/MeanModelParams;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/tts/MeanModelParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/MeanModelParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/MeanModelParams;->hasMean:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/MeanModelParams;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/MeanModelParams;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/MeanModelParams;->mean_:F

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/MeanModelParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/MeanModelParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/MeanModelParams;->hasDeviation:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/MeanModelParams;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/MeanModelParams;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/MeanModelParams;->deviation_:F

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/MeanModelParams;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/MeanModelParams;->defaultInstance:Lcom/google/speech/tts/MeanModelParams;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/MeanModelParams$Builder;
    .locals 1

    .prologue
    .line 139
    # invokes: Lcom/google/speech/tts/MeanModelParams$Builder;->create()Lcom/google/speech/tts/MeanModelParams$Builder;
    invoke-static {}, Lcom/google/speech/tts/MeanModelParams$Builder;->access$100()Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/MeanModelParams$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/MeanModelParams;

    .prologue
    .line 142
    invoke-static {}, Lcom/google/speech/tts/MeanModelParams;->newBuilder()Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/MeanModelParams$Builder;->mergeFrom(Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams;->getDefaultInstanceForType()Lcom/google/speech/tts/MeanModelParams;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/MeanModelParams;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/MeanModelParams;->defaultInstance:Lcom/google/speech/tts/MeanModelParams;

    return-object v0
.end method

.method public getDeviation()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/speech/tts/MeanModelParams;->deviation_:F

    return v0
.end method

.method public getMean()F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/tts/MeanModelParams;->mean_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 56
    iget v0, p0, Lcom/google/speech/tts/MeanModelParams;->memoizedSerializedSize:I

    .line 57
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 69
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 59
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams;->hasMean()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 61
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams;->getMean()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 64
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams;->hasDeviation()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 65
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams;->getDeviation()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 68
    :cond_2
    iput v0, p0, Lcom/google/speech/tts/MeanModelParams;->memoizedSerializedSize:I

    move v1, v0

    .line 69
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasDeviation()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/MeanModelParams;->hasDeviation:Z

    return v0
.end method

.method public hasMean()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/MeanModelParams;->hasMean:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/speech/tts/MeanModelParams;->hasMean:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 40
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams;->toBuilder()Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/MeanModelParams$Builder;
    .locals 1

    .prologue
    .line 144
    invoke-static {p0}, Lcom/google/speech/tts/MeanModelParams;->newBuilder(Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams;->getSerializedSize()I

    .line 46
    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams;->hasMean()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams;->getMean()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 49
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams;->hasDeviation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams;->getDeviation()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 52
    :cond_1
    return-void
.end method
