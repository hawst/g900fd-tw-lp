.class public final Lcom/google/speech/tts/SpeechMorphingTarget$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SpeechMorphingTarget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/SpeechMorphingTarget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/SpeechMorphingTarget;",
        "Lcom/google/speech/tts/SpeechMorphingTarget$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/SpeechMorphingTarget;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/SpeechMorphingTarget$Builder;
    .locals 1

    .prologue
    .line 187
    invoke-static {}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->create()Lcom/google/speech/tts/SpeechMorphingTarget$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/SpeechMorphingTarget$Builder;
    .locals 3

    .prologue
    .line 196
    new-instance v0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;-><init>()V

    .line 197
    .local v0, "builder":Lcom/google/speech/tts/SpeechMorphingTarget$Builder;
    new-instance v1, Lcom/google/speech/tts/SpeechMorphingTarget;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/SpeechMorphingTarget;-><init>(Lcom/google/speech/tts/SpeechMorphingTarget$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->result:Lcom/google/speech/tts/SpeechMorphingTarget;

    .line 198
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->build()Lcom/google/speech/tts/SpeechMorphingTarget;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/SpeechMorphingTarget;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->result:Lcom/google/speech/tts/SpeechMorphingTarget;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->result:Lcom/google/speech/tts/SpeechMorphingTarget;

    invoke-static {v0}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 229
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->buildPartial()Lcom/google/speech/tts/SpeechMorphingTarget;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/SpeechMorphingTarget;
    .locals 3

    .prologue
    .line 242
    iget-object v1, p0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->result:Lcom/google/speech/tts/SpeechMorphingTarget;

    if-nez v1, :cond_0

    .line 243
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->result:Lcom/google/speech/tts/SpeechMorphingTarget;

    .line 247
    .local v0, "returnMe":Lcom/google/speech/tts/SpeechMorphingTarget;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->result:Lcom/google/speech/tts/SpeechMorphingTarget;

    .line 248
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->clone()Lcom/google/speech/tts/SpeechMorphingTarget$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->clone()Lcom/google/speech/tts/SpeechMorphingTarget$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/SpeechMorphingTarget$Builder;
    .locals 2

    .prologue
    .line 215
    invoke-static {}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->create()Lcom/google/speech/tts/SpeechMorphingTarget$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->result:Lcom/google/speech/tts/SpeechMorphingTarget;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->mergeFrom(Lcom/google/speech/tts/SpeechMorphingTarget;)Lcom/google/speech/tts/SpeechMorphingTarget$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->clone()Lcom/google/speech/tts/SpeechMorphingTarget$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->result:Lcom/google/speech/tts/SpeechMorphingTarget;

    invoke-virtual {v0}, Lcom/google/speech/tts/SpeechMorphingTarget;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 187
    check-cast p1, Lcom/google/speech/tts/SpeechMorphingTarget;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->mergeFrom(Lcom/google/speech/tts/SpeechMorphingTarget;)Lcom/google/speech/tts/SpeechMorphingTarget$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/SpeechMorphingTarget;)Lcom/google/speech/tts/SpeechMorphingTarget$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/SpeechMorphingTarget;

    .prologue
    .line 252
    invoke-static {}, Lcom/google/speech/tts/SpeechMorphingTarget;->getDefaultInstance()Lcom/google/speech/tts/SpeechMorphingTarget;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-object p0

    .line 253
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/SpeechMorphingTarget;->hasType()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 254
    invoke-virtual {p1}, Lcom/google/speech/tts/SpeechMorphingTarget;->getType()Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->setType(Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;)Lcom/google/speech/tts/SpeechMorphingTarget$Builder;

    .line 256
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/SpeechMorphingTarget;->hasReference()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    invoke-virtual {p1}, Lcom/google/speech/tts/SpeechMorphingTarget;->getReference()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->setReference(Ljava/lang/String;)Lcom/google/speech/tts/SpeechMorphingTarget$Builder;

    goto :goto_0
.end method

.method public setReference(Ljava/lang/String;)Lcom/google/speech/tts/SpeechMorphingTarget$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 323
    if-nez p1, :cond_0

    .line 324
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->result:Lcom/google/speech/tts/SpeechMorphingTarget;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/SpeechMorphingTarget;->hasReference:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/SpeechMorphingTarget;->access$502(Lcom/google/speech/tts/SpeechMorphingTarget;Z)Z

    .line 327
    iget-object v0, p0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->result:Lcom/google/speech/tts/SpeechMorphingTarget;

    # setter for: Lcom/google/speech/tts/SpeechMorphingTarget;->reference_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/SpeechMorphingTarget;->access$602(Lcom/google/speech/tts/SpeechMorphingTarget;Ljava/lang/String;)Ljava/lang/String;

    .line 328
    return-object p0
.end method

.method public setType(Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;)Lcom/google/speech/tts/SpeechMorphingTarget$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    .prologue
    .line 302
    if-nez p1, :cond_0

    .line 303
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->result:Lcom/google/speech/tts/SpeechMorphingTarget;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/SpeechMorphingTarget;->hasType:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/SpeechMorphingTarget;->access$302(Lcom/google/speech/tts/SpeechMorphingTarget;Z)Z

    .line 306
    iget-object v0, p0, Lcom/google/speech/tts/SpeechMorphingTarget$Builder;->result:Lcom/google/speech/tts/SpeechMorphingTarget;

    # setter for: Lcom/google/speech/tts/SpeechMorphingTarget;->type_:Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;
    invoke-static {v0, p1}, Lcom/google/speech/tts/SpeechMorphingTarget;->access$402(Lcom/google/speech/tts/SpeechMorphingTarget;Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;)Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    .line 307
    return-object p0
.end method
