.class public final Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionParamsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/SessionParamsProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SessionParams"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

.field public static final id:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension",
            "<",
            "Lgreco/Params;",
            "Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private allowAdvancedVoicemod_:Z

.field private audioBufferSize_:I

.field private bufferedMode_:Z

.field private generateDiagnostics_:Z

.field private hasAllowAdvancedVoicemod:Z

.field private hasAudioBufferSize:Z

.field private hasBufferedMode:Z

.field private hasGenerateDiagnostics:Z

.field private hasLogPerformance:Z

.field private hasSessionId:Z

.field private hasTimePointing:Z

.field private logPerformance_:Z

.field private memoizedSerializedSize:I

.field private sessionId_:J

.field private timePointing_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->newGeneratedExtension()Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    move-result-object v0

    sput-object v0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->id:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    .line 497
    new-instance v0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->defaultInstance:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    .line 498
    invoke-static {}, Lcom/google/speech/tts/engine/SessionParamsProto;->internalForceInit()V

    .line 499
    sget-object v0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->defaultInstance:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->initFields()V

    .line 500
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 15
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 39
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->sessionId_:J

    .line 46
    iput-boolean v2, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->bufferedMode_:Z

    .line 53
    const/16 v0, 0x400

    iput v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->audioBufferSize_:I

    .line 60
    iput-boolean v2, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->timePointing_:Z

    .line 67
    iput-boolean v2, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->generateDiagnostics_:Z

    .line 74
    iput-boolean v2, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->logPerformance_:Z

    .line 81
    iput-boolean v2, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->allowAdvancedVoicemod_:Z

    .line 117
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->memoizedSerializedSize:I

    .line 16
    invoke-direct {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->initFields()V

    .line 17
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/engine/SessionParamsProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$1;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3
    .param p1, "noInit"    # Z

    .prologue
    const/4 v2, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 39
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->sessionId_:J

    .line 46
    iput-boolean v2, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->bufferedMode_:Z

    .line 53
    const/16 v0, 0x400

    iput v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->audioBufferSize_:I

    .line 60
    iput-boolean v2, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->timePointing_:Z

    .line 67
    iput-boolean v2, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->generateDiagnostics_:Z

    .line 74
    iput-boolean v2, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->logPerformance_:Z

    .line 81
    iput-boolean v2, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->allowAdvancedVoicemod_:Z

    .line 117
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->memoizedSerializedSize:I

    .line 18
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->timePointing_:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasGenerateDiagnostics:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->generateDiagnostics_:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasLogPerformance:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->logPerformance_:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasAllowAdvancedVoicemod:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->allowAdvancedVoicemod_:Z

    return p1
.end method

.method static synthetic access$302(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasSessionId:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # J

    .prologue
    .line 12
    iput-wide p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->sessionId_:J

    return-wide p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasBufferedMode:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->bufferedMode_:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasAudioBufferSize:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # I

    .prologue
    .line 12
    iput p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->audioBufferSize_:I

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasTimePointing:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->defaultInstance:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 1

    .prologue
    .line 222
    # invokes: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->create()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    invoke-static {}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->access$100()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    .prologue
    .line 225
    invoke-static {}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->newBuilder()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->mergeFrom(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAllowAdvancedVoicemod()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->allowAdvancedVoicemod_:Z

    return v0
.end method

.method public getAudioBufferSize()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->audioBufferSize_:I

    return v0
.end method

.method public getBufferedMode()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->bufferedMode_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getDefaultInstanceForType()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->defaultInstance:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    return-object v0
.end method

.method public getGenerateDiagnostics()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->generateDiagnostics_:Z

    return v0
.end method

.method public getLogPerformance()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->logPerformance_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 119
    iget v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->memoizedSerializedSize:I

    .line 120
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 152
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 122
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 123
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasSessionId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 124
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getSessionId()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 127
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasBufferedMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 128
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getBufferedMode()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 131
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasAudioBufferSize()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 132
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getAudioBufferSize()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 135
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasTimePointing()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 136
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getTimePointing()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 139
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasGenerateDiagnostics()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 140
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getGenerateDiagnostics()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 143
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasLogPerformance()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 144
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getLogPerformance()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 147
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasAllowAdvancedVoicemod()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 148
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getAllowAdvancedVoicemod()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 151
    :cond_7
    iput v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->memoizedSerializedSize:I

    move v1, v0

    .line 152
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getSessionId()J
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->sessionId_:J

    return-wide v0
.end method

.method public getTimePointing()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->timePointing_:Z

    return v0
.end method

.method public hasAllowAdvancedVoicemod()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasAllowAdvancedVoicemod:Z

    return v0
.end method

.method public hasAudioBufferSize()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasAudioBufferSize:Z

    return v0
.end method

.method public hasBufferedMode()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasBufferedMode:Z

    return v0
.end method

.method public hasGenerateDiagnostics()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasGenerateDiagnostics:Z

    return v0
.end method

.method public hasLogPerformance()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasLogPerformance:Z

    return v0
.end method

.method public hasSessionId()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasSessionId:Z

    return v0
.end method

.method public hasTimePointing()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasTimePointing:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->toBuilder()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 1

    .prologue
    .line 227
    invoke-static {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->newBuilder(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getSerializedSize()I

    .line 94
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasSessionId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getSessionId()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 97
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasBufferedMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getBufferedMode()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 100
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasAudioBufferSize()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 101
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getAudioBufferSize()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 103
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasTimePointing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 104
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getTimePointing()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 106
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasGenerateDiagnostics()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 107
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getGenerateDiagnostics()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 109
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasLogPerformance()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 110
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getLogPerformance()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 112
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasAllowAdvancedVoicemod()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 113
    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getAllowAdvancedVoicemod()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 115
    :cond_6
    return-void
.end method
