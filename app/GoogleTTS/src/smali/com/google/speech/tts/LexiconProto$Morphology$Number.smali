.class public final enum Lcom/google/speech/tts/LexiconProto$Morphology$Number;
.super Ljava/lang/Enum;
.source "LexiconProto.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto$Morphology;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Number"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/LexiconProto$Morphology$Number;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/LexiconProto$Morphology$Number;

.field public static final enum DUAL:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

.field public static final enum PLU:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

.field public static final enum SINGLE:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

.field public static final enum UNKNOWN_NUMBER:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Morphology$Number;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1530
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    const-string v1, "UNKNOWN_NUMBER"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/speech/tts/LexiconProto$Morphology$Number;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->UNKNOWN_NUMBER:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    .line 1531
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    const-string v1, "SINGLE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/speech/tts/LexiconProto$Morphology$Number;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->SINGLE:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    .line 1532
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    const-string v1, "DUAL"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/google/speech/tts/LexiconProto$Morphology$Number;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->DUAL:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    .line 1533
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    const-string v1, "PLU"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/speech/tts/LexiconProto$Morphology$Number;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->PLU:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    .line 1528
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->UNKNOWN_NUMBER:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->SINGLE:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->DUAL:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->PLU:Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->$VALUES:[Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    .line 1554
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number$1;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Morphology$Number$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1563
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1564
    iput p3, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->index:I

    .line 1565
    iput p4, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->value:I

    .line 1566
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Morphology$Number;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1528
    const-class v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/LexiconProto$Morphology$Number;
    .locals 1

    .prologue
    .line 1528
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->$VALUES:[Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    invoke-virtual {v0}, [Lcom/google/speech/tts/LexiconProto$Morphology$Number;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 1537
    iget v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Number;->value:I

    return v0
.end method
