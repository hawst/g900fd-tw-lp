.class public final Lcom/google/speech/tts/engine/TextnormParams;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "TextnormParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/engine/TextnormParams$1;,
        Lcom/google/speech/tts/engine/TextnormParams$Builder;,
        Lcom/google/speech/tts/engine/TextnormParams$Pause;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/engine/TextnormParams;


# instance fields
.field private duplicatedMessageTypes_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasParserSkipUnknownWordFraction:Z

.field private memoizedSerializedSize:I

.field private numberFactorization_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private parserSkipUnknownWordFraction_:F

.field private pause_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/engine/TextnormParams$Pause;",
            ">;"
        }
    .end annotation
.end field

.field private tokenizationRule_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/engine/FstRule;",
            ">;"
        }
    .end annotation
.end field

.field private translitRule_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/engine/FstRule;",
            ">;"
        }
    .end annotation
.end field

.field private verbalizationRule_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/engine/FstRule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1105
    new-instance v0, Lcom/google/speech/tts/engine/TextnormParams;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/engine/TextnormParams;->defaultInstance:Lcom/google/speech/tts/engine/TextnormParams;

    .line 1106
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParamsProto;->internalForceInit()V

    .line 1107
    sget-object v0, Lcom/google/speech/tts/engine/TextnormParams;->defaultInstance:Lcom/google/speech/tts/engine/TextnormParams;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/TextnormParams;->initFields()V

    .line 1108
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 368
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;

    .line 380
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;

    .line 392
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;

    .line 404
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;

    .line 416
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;

    .line 428
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;

    .line 441
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->parserSkipUnknownWordFraction_:F

    .line 489
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/engine/TextnormParams;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/engine/TextnormParams$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/engine/TextnormParams$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/engine/TextnormParams;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 368
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;

    .line 380
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;

    .line 392
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;

    .line 404
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;

    .line 416
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;

    .line 428
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;

    .line 441
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->parserSkipUnknownWordFraction_:F

    .line 489
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1200(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/google/speech/tts/engine/TextnormParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/TextnormParams;->hasParserSkipUnknownWordFraction:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/google/speech/tts/engine/TextnormParams;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TextnormParams;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/engine/TextnormParams;->parserSkipUnknownWordFraction_:F

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/engine/TextnormParams;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/engine/TextnormParams;->defaultInstance:Lcom/google/speech/tts/engine/TextnormParams;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 446
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/engine/TextnormParams$Builder;
    .locals 1

    .prologue
    .line 604
    # invokes: Lcom/google/speech/tts/engine/TextnormParams$Builder;->create()Lcom/google/speech/tts/engine/TextnormParams$Builder;
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->access$1000()Lcom/google/speech/tts/engine/TextnormParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/engine/TextnormParams;)Lcom/google/speech/tts/engine/TextnormParams$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/engine/TextnormParams;

    .prologue
    .line 607
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParams;->newBuilder()Lcom/google/speech/tts/engine/TextnormParams$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->mergeFrom(Lcom/google/speech/tts/engine/TextnormParams;)Lcom/google/speech/tts/engine/TextnormParams$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getDefaultInstanceForType()Lcom/google/speech/tts/engine/TextnormParams;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/engine/TextnormParams;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/engine/TextnormParams;->defaultInstance:Lcom/google/speech/tts/engine/TextnormParams;

    return-object v0
.end method

.method public getDuplicatedMessageTypesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;

    return-object v0
.end method

.method public getNumberFactorizationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;

    return-object v0
.end method

.method public getParserSkipUnknownWordFraction()F
    .locals 1

    .prologue
    .line 443
    iget v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->parserSkipUnknownWordFraction_:F

    return v0
.end method

.method public getPauseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/engine/TextnormParams$Pause;",
            ">;"
        }
    .end annotation

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 491
    iget v3, p0, Lcom/google/speech/tts/engine/TextnormParams;->memoizedSerializedSize:I

    .line 492
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 534
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 494
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 496
    const/4 v0, 0x0

    .line 497
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getNumberFactorizationList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 498
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 500
    goto :goto_1

    .line 501
    .end local v1    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v3, v0

    .line 502
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getNumberFactorizationList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 504
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getTokenizationRuleList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/tts/engine/FstRule;

    .line 505
    .local v1, "element":Lcom/google/speech/tts/engine/FstRule;
    const/4 v5, 0x2

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 507
    goto :goto_2

    .line 508
    .end local v1    # "element":Lcom/google/speech/tts/engine/FstRule;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getVerbalizationRuleList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/tts/engine/FstRule;

    .line 509
    .restart local v1    # "element":Lcom/google/speech/tts/engine/FstRule;
    const/4 v5, 0x3

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 511
    goto :goto_3

    .line 512
    .end local v1    # "element":Lcom/google/speech/tts/engine/FstRule;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getTranslitRuleList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/tts/engine/FstRule;

    .line 513
    .restart local v1    # "element":Lcom/google/speech/tts/engine/FstRule;
    const/4 v5, 0x4

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 515
    goto :goto_4

    .line 516
    .end local v1    # "element":Lcom/google/speech/tts/engine/FstRule;
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getPauseList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/tts/engine/TextnormParams$Pause;

    .line 517
    .local v1, "element":Lcom/google/speech/tts/engine/TextnormParams$Pause;
    const/4 v5, 0x5

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 519
    goto :goto_5

    .line 521
    .end local v1    # "element":Lcom/google/speech/tts/engine/TextnormParams$Pause;
    :cond_5
    const/4 v0, 0x0

    .line 522
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getDuplicatedMessageTypesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 523
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 525
    goto :goto_6

    .line 526
    .end local v1    # "element":Ljava/lang/String;
    :cond_6
    add-int/2addr v3, v0

    .line 527
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getDuplicatedMessageTypesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 529
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->hasParserSkipUnknownWordFraction()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 530
    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getParserSkipUnknownWordFraction()F

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v5

    add-int/2addr v3, v5

    .line 533
    :cond_7
    iput v3, p0, Lcom/google/speech/tts/engine/TextnormParams;->memoizedSerializedSize:I

    move v4, v3

    .line 534
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getTokenizationRuleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/engine/FstRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;

    return-object v0
.end method

.method public getTranslitRuleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/engine/FstRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;

    return-object v0
.end method

.method public getVerbalizationRuleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/engine/FstRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;

    return-object v0
.end method

.method public hasParserSkipUnknownWordFraction()Z
    .locals 1

    .prologue
    .line 442
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/TextnormParams;->hasParserSkipUnknownWordFraction:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 448
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getTokenizationRuleList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/engine/FstRule;

    .line 449
    .local v0, "element":Lcom/google/speech/tts/engine/FstRule;
    invoke-virtual {v0}, Lcom/google/speech/tts/engine/FstRule;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 460
    .end local v0    # "element":Lcom/google/speech/tts/engine/FstRule;
    :goto_0
    return v2

    .line 451
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getVerbalizationRuleList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/engine/FstRule;

    .line 452
    .restart local v0    # "element":Lcom/google/speech/tts/engine/FstRule;
    invoke-virtual {v0}, Lcom/google/speech/tts/engine/FstRule;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 454
    .end local v0    # "element":Lcom/google/speech/tts/engine/FstRule;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getTranslitRuleList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/engine/FstRule;

    .line 455
    .restart local v0    # "element":Lcom/google/speech/tts/engine/FstRule;
    invoke-virtual {v0}, Lcom/google/speech/tts/engine/FstRule;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_4

    goto :goto_0

    .line 457
    .end local v0    # "element":Lcom/google/speech/tts/engine/FstRule;
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getPauseList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/engine/TextnormParams$Pause;

    .line 458
    .local v0, "element":Lcom/google/speech/tts/engine/TextnormParams$Pause;
    invoke-virtual {v0}, Lcom/google/speech/tts/engine/TextnormParams$Pause;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_6

    goto :goto_0

    .line 460
    .end local v0    # "element":Lcom/google/speech/tts/engine/TextnormParams$Pause;
    :cond_7
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->toBuilder()Lcom/google/speech/tts/engine/TextnormParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/engine/TextnormParams$Builder;
    .locals 1

    .prologue
    .line 609
    invoke-static {p0}, Lcom/google/speech/tts/engine/TextnormParams;->newBuilder(Lcom/google/speech/tts/engine/TextnormParams;)Lcom/google/speech/tts/engine/TextnormParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getSerializedSize()I

    .line 466
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getNumberFactorizationList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 467
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 469
    .end local v0    # "element":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getTokenizationRuleList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/engine/FstRule;

    .line 470
    .local v0, "element":Lcom/google/speech/tts/engine/FstRule;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 472
    .end local v0    # "element":Lcom/google/speech/tts/engine/FstRule;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getVerbalizationRuleList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/engine/FstRule;

    .line 473
    .restart local v0    # "element":Lcom/google/speech/tts/engine/FstRule;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    .line 475
    .end local v0    # "element":Lcom/google/speech/tts/engine/FstRule;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getTranslitRuleList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/engine/FstRule;

    .line 476
    .restart local v0    # "element":Lcom/google/speech/tts/engine/FstRule;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    .line 478
    .end local v0    # "element":Lcom/google/speech/tts/engine/FstRule;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getPauseList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/engine/TextnormParams$Pause;

    .line 479
    .local v0, "element":Lcom/google/speech/tts/engine/TextnormParams$Pause;
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_4

    .line 481
    .end local v0    # "element":Lcom/google/speech/tts/engine/TextnormParams$Pause;
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getDuplicatedMessageTypesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 482
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_5

    .line 484
    .end local v0    # "element":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->hasParserSkipUnknownWordFraction()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 485
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams;->getParserSkipUnknownWordFraction()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 487
    :cond_6
    return-void
.end method
