.class public final Lcom/google/speech/tts/Fraction$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Fraction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Fraction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Fraction;",
        "Lcom/google/speech/tts/Fraction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Fraction;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Fraction$Builder;
    .locals 1

    .prologue
    .line 255
    invoke-static {}, Lcom/google/speech/tts/Fraction$Builder;->create()Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Fraction$Builder;
    .locals 3

    .prologue
    .line 264
    new-instance v0, Lcom/google/speech/tts/Fraction$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Fraction$Builder;-><init>()V

    .line 265
    .local v0, "builder":Lcom/google/speech/tts/Fraction$Builder;
    new-instance v1, Lcom/google/speech/tts/Fraction;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Fraction;-><init>(Lcom/google/speech/tts/Fraction$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    .line 266
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction$Builder;->build()Lcom/google/speech/tts/Fraction;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Fraction;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    invoke-static {v0}, Lcom/google/speech/tts/Fraction$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 297
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction$Builder;->buildPartial()Lcom/google/speech/tts/Fraction;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Fraction;
    .locals 3

    .prologue
    .line 310
    iget-object v1, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    if-nez v1, :cond_0

    .line 311
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 314
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    # getter for: Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Fraction;->access$300(Lcom/google/speech/tts/Fraction;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 315
    iget-object v1, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    iget-object v2, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    # getter for: Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Fraction;->access$300(Lcom/google/speech/tts/Fraction;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Fraction;->access$302(Lcom/google/speech/tts/Fraction;Ljava/util/List;)Ljava/util/List;

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    .line 319
    .local v0, "returnMe":Lcom/google/speech/tts/Fraction;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    .line 320
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction$Builder;->clone()Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction$Builder;->clone()Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Fraction$Builder;
    .locals 2

    .prologue
    .line 283
    invoke-static {}, Lcom/google/speech/tts/Fraction$Builder;->create()Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Fraction$Builder;->mergeFrom(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction$Builder;->clone()Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    invoke-virtual {v0}, Lcom/google/speech/tts/Fraction;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 255
    check-cast p1, Lcom/google/speech/tts/Fraction;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Fraction$Builder;->mergeFrom(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Fraction;

    .prologue
    .line 324
    invoke-static {}, Lcom/google/speech/tts/Fraction;->getDefaultInstance()Lcom/google/speech/tts/Fraction;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 355
    :cond_0
    :goto_0
    return-object p0

    .line 325
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->hasIntegerPart()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 326
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->getIntegerPart()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Fraction$Builder;->setIntegerPart(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;

    .line 328
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->hasNumerator()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 329
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->getNumerator()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Fraction$Builder;->setNumerator(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;

    .line 331
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->hasDenominator()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 332
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->getDenominator()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Fraction$Builder;->setDenominator(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;

    .line 334
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 335
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->getStyle()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Fraction$Builder;->setStyle(I)Lcom/google/speech/tts/Fraction$Builder;

    .line 337
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 338
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Fraction$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;

    .line 340
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->hasPreserveOrder()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 341
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->getPreserveOrder()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Fraction$Builder;->setPreserveOrder(Z)Lcom/google/speech/tts/Fraction$Builder;

    .line 343
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 344
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->getCodeSwitch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Fraction$Builder;->setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;

    .line 346
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->hasNegative()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 347
    invoke-virtual {p1}, Lcom/google/speech/tts/Fraction;->getNegative()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Fraction$Builder;->setNegative(Z)Lcom/google/speech/tts/Fraction$Builder;

    .line 349
    :cond_9
    # getter for: Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Fraction;->access$300(Lcom/google/speech/tts/Fraction;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    # getter for: Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Fraction;->access$300(Lcom/google/speech/tts/Fraction;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 351
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Fraction;->access$302(Lcom/google/speech/tts/Fraction;Ljava/util/List;)Ljava/util/List;

    .line 353
    :cond_a
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    # getter for: Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Fraction;->access$300(Lcom/google/speech/tts/Fraction;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Fraction;->access$300(Lcom/google/speech/tts/Fraction;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 542
    if-nez p1, :cond_0

    .line 543
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Fraction;->hasCodeSwitch:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Fraction;->access$1602(Lcom/google/speech/tts/Fraction;Z)Z

    .line 546
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    # setter for: Lcom/google/speech/tts/Fraction;->codeSwitch_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Fraction;->access$1702(Lcom/google/speech/tts/Fraction;Ljava/lang/String;)Ljava/lang/String;

    .line 547
    return-object p0
.end method

.method public setDenominator(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 464
    if-nez p1, :cond_0

    .line 465
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Fraction;->hasDenominator:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Fraction;->access$802(Lcom/google/speech/tts/Fraction;Z)Z

    .line 468
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    # setter for: Lcom/google/speech/tts/Fraction;->denominator_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Fraction;->access$902(Lcom/google/speech/tts/Fraction;Ljava/lang/String;)Ljava/lang/String;

    .line 469
    return-object p0
.end method

.method public setIntegerPart(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 422
    if-nez p1, :cond_0

    .line 423
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Fraction;->hasIntegerPart:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Fraction;->access$402(Lcom/google/speech/tts/Fraction;Z)Z

    .line 426
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    # setter for: Lcom/google/speech/tts/Fraction;->integerPart_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Fraction;->access$502(Lcom/google/speech/tts/Fraction;Ljava/lang/String;)Ljava/lang/String;

    .line 427
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 503
    if-nez p1, :cond_0

    .line 504
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Fraction;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Fraction;->access$1202(Lcom/google/speech/tts/Fraction;Z)Z

    .line 507
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    # setter for: Lcom/google/speech/tts/Fraction;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Fraction;->access$1302(Lcom/google/speech/tts/Fraction;Ljava/lang/String;)Ljava/lang/String;

    .line 508
    return-object p0
.end method

.method public setNegative(Z)Lcom/google/speech/tts/Fraction$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Fraction;->hasNegative:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Fraction;->access$1802(Lcom/google/speech/tts/Fraction;Z)Z

    .line 564
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    # setter for: Lcom/google/speech/tts/Fraction;->negative_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Fraction;->access$1902(Lcom/google/speech/tts/Fraction;Z)Z

    .line 565
    return-object p0
.end method

.method public setNumerator(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 443
    if-nez p1, :cond_0

    .line 444
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 446
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Fraction;->hasNumerator:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Fraction;->access$602(Lcom/google/speech/tts/Fraction;Z)Z

    .line 447
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    # setter for: Lcom/google/speech/tts/Fraction;->numerator_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Fraction;->access$702(Lcom/google/speech/tts/Fraction;Ljava/lang/String;)Ljava/lang/String;

    .line 448
    return-object p0
.end method

.method public setPreserveOrder(Z)Lcom/google/speech/tts/Fraction$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 524
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Fraction;->hasPreserveOrder:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Fraction;->access$1402(Lcom/google/speech/tts/Fraction;Z)Z

    .line 525
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    # setter for: Lcom/google/speech/tts/Fraction;->preserveOrder_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Fraction;->access$1502(Lcom/google/speech/tts/Fraction;Z)Z

    .line 526
    return-object p0
.end method

.method public setStyle(I)Lcom/google/speech/tts/Fraction$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 485
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Fraction;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Fraction;->access$1002(Lcom/google/speech/tts/Fraction;Z)Z

    .line 486
    iget-object v0, p0, Lcom/google/speech/tts/Fraction$Builder;->result:Lcom/google/speech/tts/Fraction;

    # setter for: Lcom/google/speech/tts/Fraction;->style_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Fraction;->access$1102(Lcom/google/speech/tts/Fraction;I)I

    .line 487
    return-object p0
.end method
