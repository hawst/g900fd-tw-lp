.class public final Lcom/google/speech/tts/engine/FstRule;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FstRule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/engine/FstRule$1;,
        Lcom/google/speech/tts/engine/FstRule$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/engine/FstRule;


# instance fields
.field private hasName:Z

.field private hasParens:Z

.field private hasRedup:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private parens_:Ljava/lang/String;

.field private redup_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 337
    new-instance v0, Lcom/google/speech/tts/engine/FstRule;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/engine/FstRule;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/engine/FstRule;->defaultInstance:Lcom/google/speech/tts/engine/FstRule;

    .line 338
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParamsProto;->internalForceInit()V

    .line 339
    sget-object v0, Lcom/google/speech/tts/engine/FstRule;->defaultInstance:Lcom/google/speech/tts/engine/FstRule;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/FstRule;->initFields()V

    .line 340
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/FstRule;->name_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/FstRule;->parens_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/FstRule;->redup_:Ljava/lang/String;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/FstRule;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/engine/FstRule;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/engine/FstRule$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/engine/FstRule$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/engine/FstRule;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/FstRule;->name_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/FstRule;->parens_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/engine/FstRule;->redup_:Ljava/lang/String;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/FstRule;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/tts/engine/FstRule;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/FstRule;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/FstRule;->hasName:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/engine/FstRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/FstRule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/FstRule;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/engine/FstRule;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/FstRule;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/FstRule;->hasParens:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/engine/FstRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/FstRule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/FstRule;->parens_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/engine/FstRule;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/FstRule;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/FstRule;->hasRedup:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/engine/FstRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/FstRule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/engine/FstRule;->redup_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/engine/FstRule;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/engine/FstRule;->defaultInstance:Lcom/google/speech/tts/engine/FstRule;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/engine/FstRule$Builder;
    .locals 1

    .prologue
    .line 153
    # invokes: Lcom/google/speech/tts/engine/FstRule$Builder;->create()Lcom/google/speech/tts/engine/FstRule$Builder;
    invoke-static {}, Lcom/google/speech/tts/engine/FstRule$Builder;->access$100()Lcom/google/speech/tts/engine/FstRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/engine/FstRule;)Lcom/google/speech/tts/engine/FstRule$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/engine/FstRule;

    .prologue
    .line 156
    invoke-static {}, Lcom/google/speech/tts/engine/FstRule;->newBuilder()Lcom/google/speech/tts/engine/FstRule$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/engine/FstRule$Builder;->mergeFrom(Lcom/google/speech/tts/engine/FstRule;)Lcom/google/speech/tts/engine/FstRule$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->getDefaultInstanceForType()Lcom/google/speech/tts/engine/FstRule;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/engine/FstRule;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/engine/FstRule;->defaultInstance:Lcom/google/speech/tts/engine/FstRule;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getParens()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule;->parens_:Ljava/lang/String;

    return-object v0
.end method

.method public getRedup()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/engine/FstRule;->redup_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 66
    iget v0, p0, Lcom/google/speech/tts/engine/FstRule;->memoizedSerializedSize:I

    .line 67
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 83
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 69
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 70
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 74
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->hasParens()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 75
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->getParens()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 78
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->hasRedup()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 79
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->getRedup()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 82
    :cond_3
    iput v0, p0, Lcom/google/speech/tts/engine/FstRule;->memoizedSerializedSize:I

    move v1, v0

    .line 83
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/FstRule;->hasName:Z

    return v0
.end method

.method public hasParens()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/FstRule;->hasParens:Z

    return v0
.end method

.method public hasRedup()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/FstRule;->hasRedup:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/FstRule;->hasName:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 47
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->toBuilder()Lcom/google/speech/tts/engine/FstRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/engine/FstRule$Builder;
    .locals 1

    .prologue
    .line 158
    invoke-static {p0}, Lcom/google/speech/tts/engine/FstRule;->newBuilder(Lcom/google/speech/tts/engine/FstRule;)Lcom/google/speech/tts/engine/FstRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->getSerializedSize()I

    .line 53
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 56
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->hasParens()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->getParens()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 59
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->hasRedup()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/FstRule;->getRedup()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 62
    :cond_2
    return-void
.end method
