.class public final Lcom/google/speech/tts/output_info/Word;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Word.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/output_info/Word$1;,
        Lcom/google/speech/tts/output_info/Word$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/output_info/Word;


# instance fields
.field private hasTokenIndex:Z

.field private memoizedSerializedSize:I

.field private syllable_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/output_info/Syllable;",
            ">;"
        }
    .end annotation
.end field

.field private tokenIndex_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 336
    new-instance v0, Lcom/google/speech/tts/output_info/Word;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/output_info/Word;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/output_info/Word;->defaultInstance:Lcom/google/speech/tts/output_info/Word;

    .line 337
    invoke-static {}, Lcom/google/speech/tts/output_info/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 338
    sget-object v0, Lcom/google/speech/tts/output_info/Word;->defaultInstance:Lcom/google/speech/tts/output_info/Word;

    invoke-direct {v0}, Lcom/google/speech/tts/output_info/Word;->initFields()V

    .line 339
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v1, p0, Lcom/google/speech/tts/output_info/Word;->tokenIndex_:I

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/output_info/Word;->syllable_:Ljava/util/List;

    .line 59
    iput v1, p0, Lcom/google/speech/tts/output_info/Word;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/output_info/Word;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/output_info/Word$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/output_info/Word$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/output_info/Word;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, -0x1

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v1, p0, Lcom/google/speech/tts/output_info/Word;->tokenIndex_:I

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/output_info/Word;->syllable_:Ljava/util/List;

    .line 59
    iput v1, p0, Lcom/google/speech/tts/output_info/Word;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lcom/google/speech/tts/output_info/Word;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Word;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Word;->syllable_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/output_info/Word;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Word;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/output_info/Word;->syllable_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/output_info/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/output_info/Word;->hasTokenIndex:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/output_info/Word;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Word;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/output_info/Word;->tokenIndex_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/output_info/Word;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/output_info/Word;->defaultInstance:Lcom/google/speech/tts/output_info/Word;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/output_info/Word$Builder;
    .locals 1

    .prologue
    .line 144
    # invokes: Lcom/google/speech/tts/output_info/Word$Builder;->create()Lcom/google/speech/tts/output_info/Word$Builder;
    invoke-static {}, Lcom/google/speech/tts/output_info/Word$Builder;->access$100()Lcom/google/speech/tts/output_info/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/output_info/Word;)Lcom/google/speech/tts/output_info/Word$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/output_info/Word;

    .prologue
    .line 147
    invoke-static {}, Lcom/google/speech/tts/output_info/Word;->newBuilder()Lcom/google/speech/tts/output_info/Word$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/output_info/Word$Builder;->mergeFrom(Lcom/google/speech/tts/output_info/Word;)Lcom/google/speech/tts/output_info/Word$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Word;->getDefaultInstanceForType()Lcom/google/speech/tts/output_info/Word;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/output_info/Word;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/output_info/Word;->defaultInstance:Lcom/google/speech/tts/output_info/Word;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 61
    iget v2, p0, Lcom/google/speech/tts/output_info/Word;->memoizedSerializedSize:I

    .line 62
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 74
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 64
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Word;->hasTokenIndex()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 66
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Word;->getTokenIndex()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 69
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Word;->getSyllableList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/output_info/Syllable;

    .line 70
    .local v0, "element":Lcom/google/speech/tts/output_info/Syllable;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 72
    goto :goto_1

    .line 73
    .end local v0    # "element":Lcom/google/speech/tts/output_info/Syllable;
    :cond_2
    iput v2, p0, Lcom/google/speech/tts/output_info/Word;->memoizedSerializedSize:I

    move v3, v2

    .line 74
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getSyllableList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/output_info/Syllable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Word;->syllable_:Ljava/util/List;

    return-object v0
.end method

.method public getTokenIndex()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/tts/output_info/Word;->tokenIndex_:I

    return v0
.end method

.method public hasTokenIndex()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/output_info/Word;->hasTokenIndex:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/speech/tts/output_info/Word;->hasTokenIndex:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 45
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Word;->toBuilder()Lcom/google/speech/tts/output_info/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/output_info/Word$Builder;
    .locals 1

    .prologue
    .line 149
    invoke-static {p0}, Lcom/google/speech/tts/output_info/Word;->newBuilder(Lcom/google/speech/tts/output_info/Word;)Lcom/google/speech/tts/output_info/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Word;->getSerializedSize()I

    .line 51
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Word;->hasTokenIndex()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Word;->getTokenIndex()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Word;->getSyllableList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/output_info/Syllable;

    .line 55
    .local v0, "element":Lcom/google/speech/tts/output_info/Syllable;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 57
    .end local v0    # "element":Lcom/google/speech/tts/output_info/Syllable;
    :cond_1
    return-void
.end method
