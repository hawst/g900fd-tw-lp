.class public final Lcom/google/speech/tts/output_info/Token;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Token.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/output_info/Token$1;,
        Lcom/google/speech/tts/output_info/Token$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/output_info/Token;


# instance fields
.field private end_:Lcom/google/speech/tts/output_info/InputIndex;

.field private hasEnd:Z

.field private hasStart:Z

.field private memoizedSerializedSize:I

.field private start_:Lcom/google/speech/tts/output_info/InputIndex;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 338
    new-instance v0, Lcom/google/speech/tts/output_info/Token;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/output_info/Token;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/output_info/Token;->defaultInstance:Lcom/google/speech/tts/output_info/Token;

    .line 339
    invoke-static {}, Lcom/google/speech/tts/output_info/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 340
    sget-object v0, Lcom/google/speech/tts/output_info/Token;->defaultInstance:Lcom/google/speech/tts/output_info/Token;

    invoke-direct {v0}, Lcom/google/speech/tts/output_info/Token;->initFields()V

    .line 341
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/output_info/Token;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/output_info/Token;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/output_info/Token$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/output_info/Token$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/output_info/Token;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/output_info/Token;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/tts/output_info/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/output_info/Token;->hasStart:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/speech/tts/output_info/Token;)Lcom/google/speech/tts/output_info/InputIndex;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token;->start_:Lcom/google/speech/tts/output_info/InputIndex;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/speech/tts/output_info/Token;Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/output_info/InputIndex;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/output_info/Token;->start_:Lcom/google/speech/tts/output_info/InputIndex;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/output_info/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/output_info/Token;->hasEnd:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/speech/tts/output_info/Token;)Lcom/google/speech/tts/output_info/InputIndex;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token;->end_:Lcom/google/speech/tts/output_info/InputIndex;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/speech/tts/output_info/Token;Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/output_info/InputIndex;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/output_info/Token;->end_:Lcom/google/speech/tts/output_info/InputIndex;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/output_info/Token;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/output_info/Token;->defaultInstance:Lcom/google/speech/tts/output_info/Token;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Lcom/google/speech/tts/output_info/InputIndex;->getDefaultInstance()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/output_info/Token;->start_:Lcom/google/speech/tts/output_info/InputIndex;

    .line 38
    invoke-static {}, Lcom/google/speech/tts/output_info/InputIndex;->getDefaultInstance()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/output_info/Token;->end_:Lcom/google/speech/tts/output_info/InputIndex;

    .line 39
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/output_info/Token$Builder;
    .locals 1

    .prologue
    .line 140
    # invokes: Lcom/google/speech/tts/output_info/Token$Builder;->create()Lcom/google/speech/tts/output_info/Token$Builder;
    invoke-static {}, Lcom/google/speech/tts/output_info/Token$Builder;->access$100()Lcom/google/speech/tts/output_info/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/output_info/Token;)Lcom/google/speech/tts/output_info/Token$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/output_info/Token;

    .prologue
    .line 143
    invoke-static {}, Lcom/google/speech/tts/output_info/Token;->newBuilder()Lcom/google/speech/tts/output_info/Token$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/output_info/Token$Builder;->mergeFrom(Lcom/google/speech/tts/output_info/Token;)Lcom/google/speech/tts/output_info/Token$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token;->getDefaultInstanceForType()Lcom/google/speech/tts/output_info/Token;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/output_info/Token;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/output_info/Token;->defaultInstance:Lcom/google/speech/tts/output_info/Token;

    return-object v0
.end method

.method public getEnd()Lcom/google/speech/tts/output_info/InputIndex;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token;->end_:Lcom/google/speech/tts/output_info/InputIndex;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 57
    iget v0, p0, Lcom/google/speech/tts/output_info/Token;->memoizedSerializedSize:I

    .line 58
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 70
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 60
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 61
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token;->hasStart()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token;->getStart()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 65
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token;->hasEnd()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 66
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token;->getEnd()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 69
    :cond_2
    iput v0, p0, Lcom/google/speech/tts/output_info/Token;->memoizedSerializedSize:I

    move v1, v0

    .line 70
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getStart()Lcom/google/speech/tts/output_info/InputIndex;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token;->start_:Lcom/google/speech/tts/output_info/InputIndex;

    return-object v0
.end method

.method public hasEnd()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/output_info/Token;->hasEnd:Z

    return v0
.end method

.method public hasStart()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/output_info/Token;->hasStart:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token;->toBuilder()Lcom/google/speech/tts/output_info/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/output_info/Token$Builder;
    .locals 1

    .prologue
    .line 145
    invoke-static {p0}, Lcom/google/speech/tts/output_info/Token;->newBuilder(Lcom/google/speech/tts/output_info/Token;)Lcom/google/speech/tts/output_info/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token;->getSerializedSize()I

    .line 47
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token;->hasStart()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token;->getStart()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token;->hasEnd()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token;->getEnd()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 53
    :cond_1
    return-void
.end method
