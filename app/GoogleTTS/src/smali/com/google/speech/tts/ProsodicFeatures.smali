.class public final Lcom/google/speech/tts/ProsodicFeatures;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "ProsodicFeatures.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/ProsodicFeatures$1;,
        Lcom/google/speech/tts/ProsodicFeatures$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/ProsodicFeatures;


# instance fields
.field private containsHead_:Z

.field private containsNucleus_:Z

.field private hasContainsHead:Z

.field private hasContainsNucleus:Z

.field private hasIntonationType:Z

.field private hasProminence:Z

.field private intonationType_:Lcom/google/speech/tts/IntonationType;

.field private memoizedSerializedSize:I

.field private prominence_:Lcom/google/speech/tts/Prominence;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 399
    new-instance v0, Lcom/google/speech/tts/ProsodicFeatures;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/ProsodicFeatures;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/ProsodicFeatures;->defaultInstance:Lcom/google/speech/tts/ProsodicFeatures;

    .line 400
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 401
    sget-object v0, Lcom/google/speech/tts/ProsodicFeatures;->defaultInstance:Lcom/google/speech/tts/ProsodicFeatures;

    invoke-direct {v0}, Lcom/google/speech/tts/ProsodicFeatures;->initFields()V

    .line 402
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->containsHead_:Z

    .line 32
    iput-boolean v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->containsNucleus_:Z

    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/ProsodicFeatures;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/ProsodicFeatures$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/ProsodicFeatures$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/ProsodicFeatures;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->containsHead_:Z

    .line 32
    iput-boolean v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->containsNucleus_:Z

    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1000(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/Prominence;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/ProsodicFeatures;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->prominence_:Lcom/google/speech/tts/Prominence;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/ProsodicFeatures;Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/ProsodicFeatures;
    .param p1, "x1"    # Lcom/google/speech/tts/Prominence;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/ProsodicFeatures;->prominence_:Lcom/google/speech/tts/Prominence;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/speech/tts/ProsodicFeatures;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/ProsodicFeatures;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/ProsodicFeatures;->hasContainsHead:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/ProsodicFeatures;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/ProsodicFeatures;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/ProsodicFeatures;->containsHead_:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/ProsodicFeatures;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/ProsodicFeatures;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/ProsodicFeatures;->hasContainsNucleus:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/ProsodicFeatures;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/ProsodicFeatures;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/ProsodicFeatures;->containsNucleus_:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/ProsodicFeatures;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/ProsodicFeatures;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/ProsodicFeatures;->hasIntonationType:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/ProsodicFeatures;Lcom/google/speech/tts/IntonationType;)Lcom/google/speech/tts/IntonationType;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/ProsodicFeatures;
    .param p1, "x1"    # Lcom/google/speech/tts/IntonationType;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/ProsodicFeatures;->intonationType_:Lcom/google/speech/tts/IntonationType;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/ProsodicFeatures;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/ProsodicFeatures;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/ProsodicFeatures;->hasProminence:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/ProsodicFeatures;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/ProsodicFeatures;->defaultInstance:Lcom/google/speech/tts/ProsodicFeatures;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/google/speech/tts/IntonationType;->UNKNOWN:Lcom/google/speech/tts/IntonationType;

    iput-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->intonationType_:Lcom/google/speech/tts/IntonationType;

    .line 52
    invoke-static {}, Lcom/google/speech/tts/Prominence;->getDefaultInstance()Lcom/google/speech/tts/Prominence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->prominence_:Lcom/google/speech/tts/Prominence;

    .line 53
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/ProsodicFeatures$Builder;
    .locals 1

    .prologue
    .line 168
    # invokes: Lcom/google/speech/tts/ProsodicFeatures$Builder;->create()Lcom/google/speech/tts/ProsodicFeatures$Builder;
    invoke-static {}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->access$100()Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/ProsodicFeatures$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/ProsodicFeatures;

    .prologue
    .line 171
    invoke-static {}, Lcom/google/speech/tts/ProsodicFeatures;->newBuilder()Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->mergeFrom(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getContainsHead()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->containsHead_:Z

    return v0
.end method

.method public getContainsNucleus()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->containsNucleus_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->getDefaultInstanceForType()Lcom/google/speech/tts/ProsodicFeatures;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/ProsodicFeatures;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/ProsodicFeatures;->defaultInstance:Lcom/google/speech/tts/ProsodicFeatures;

    return-object v0
.end method

.method public getIntonationType()Lcom/google/speech/tts/IntonationType;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->intonationType_:Lcom/google/speech/tts/IntonationType;

    return-object v0
.end method

.method public getProminence()Lcom/google/speech/tts/Prominence;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->prominence_:Lcom/google/speech/tts/Prominence;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 77
    iget v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->memoizedSerializedSize:I

    .line 78
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 98
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 80
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->hasContainsHead()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 82
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->getContainsHead()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 85
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->hasContainsNucleus()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 86
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->getContainsNucleus()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 89
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->hasIntonationType()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 90
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->getIntonationType()Lcom/google/speech/tts/IntonationType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/IntonationType;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 93
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->hasProminence()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 94
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->getProminence()Lcom/google/speech/tts/Prominence;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 97
    :cond_4
    iput v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->memoizedSerializedSize:I

    move v1, v0

    .line 98
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasContainsHead()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->hasContainsHead:Z

    return v0
.end method

.method public hasContainsNucleus()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->hasContainsNucleus:Z

    return v0
.end method

.method public hasIntonationType()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->hasIntonationType:Z

    return v0
.end method

.method public hasProminence()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/ProsodicFeatures;->hasProminence:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->toBuilder()Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/ProsodicFeatures$Builder;
    .locals 1

    .prologue
    .line 173
    invoke-static {p0}, Lcom/google/speech/tts/ProsodicFeatures;->newBuilder(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->getSerializedSize()I

    .line 61
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->hasContainsHead()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->getContainsHead()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 64
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->hasContainsNucleus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->getContainsNucleus()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 67
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->hasIntonationType()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->getIntonationType()Lcom/google/speech/tts/IntonationType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/IntonationType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 70
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->hasProminence()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 71
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures;->getProminence()Lcom/google/speech/tts/Prominence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 73
    :cond_3
    return-void
.end method
