.class public final Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExpandedPronunciation"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;


# instance fields
.field private memoizedSerializedSize:I

.field private syllable_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Syllable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 914
    new-instance v0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->defaultInstance:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    .line 915
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 916
    sget-object v0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->defaultInstance:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->initFields()V

    .line 917
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 623
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 639
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;

    .line 666
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->memoizedSerializedSize:I

    .line 624
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->initFields()V

    .line 625
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/LexiconProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/LexiconProto$1;

    .prologue
    .line 620
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 626
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 639
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;

    .line 666
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->memoizedSerializedSize:I

    .line 626
    return-void
.end method

.method static synthetic access$1700(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 620
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;
    .locals 1

    .prologue
    .line 630
    sget-object v0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->defaultInstance:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 650
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;
    .locals 1

    .prologue
    .line 747
    # invokes: Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->create()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->access$1500()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    .prologue
    .line 750
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->newBuilder()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 620
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;
    .locals 1

    .prologue
    .line 634
    sget-object v0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->defaultInstance:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 668
    iget v2, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->memoizedSerializedSize:I

    .line 669
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 677
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 671
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 672
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->getSyllableList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Syllable;

    .line 673
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Syllable;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 675
    goto :goto_1

    .line 676
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Syllable;
    :cond_1
    iput v2, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->memoizedSerializedSize:I

    move v3, v2

    .line 677
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getSyllableList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Syllable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    .line 652
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->getSyllableList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Syllable;

    .line 653
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Syllable;
    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$Syllable;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 655
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Syllable;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 620
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->toBuilder()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;
    .locals 1

    .prologue
    .line 752
    invoke-static {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->newBuilder(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 660
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->getSerializedSize()I

    .line 661
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->getSyllableList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Syllable;

    .line 662
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Syllable;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 664
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Syllable;
    :cond_0
    return-void
.end method
