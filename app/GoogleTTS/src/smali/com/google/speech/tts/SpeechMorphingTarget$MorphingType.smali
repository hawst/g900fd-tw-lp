.class public final enum Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;
.super Ljava/lang/Enum;
.source "SpeechMorphingTarget.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/SpeechMorphingTarget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MorphingType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

.field public static final enum NONE:Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

.field public static final enum UNKNOWN_MORPHING_TYPE:Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

.field public static final enum VOICE_TIMBRE_USING_ODFWW:Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    const-string v1, "UNKNOWN_MORPHING_TYPE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->UNKNOWN_MORPHING_TYPE:Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    .line 25
    new-instance v0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->NONE:Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    .line 26
    new-instance v0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    const-string v1, "VOICE_TIMBRE_USING_ODFWW"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->VOICE_TIMBRE_USING_ODFWW:Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    sget-object v1, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->UNKNOWN_MORPHING_TYPE:Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->NONE:Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->VOICE_TIMBRE_USING_ODFWW:Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->$VALUES:[Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    .line 46
    new-instance v0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType$1;

    invoke-direct {v0}, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput p3, p0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->index:I

    .line 57
    iput p4, p0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->value:I

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->$VALUES:[Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    invoke-virtual {v0}, [Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/google/speech/tts/SpeechMorphingTarget$MorphingType;->value:I

    return v0
.end method
