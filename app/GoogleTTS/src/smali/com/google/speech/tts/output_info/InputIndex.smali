.class public final Lcom/google/speech/tts/output_info/InputIndex;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "InputIndex.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/output_info/InputIndex$1;,
        Lcom/google/speech/tts/output_info/InputIndex$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/output_info/InputIndex;


# instance fields
.field private hasUnicodeIndex:Z

.field private memoizedSerializedSize:I

.field private unicodeIndex_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 249
    new-instance v0, Lcom/google/speech/tts/output_info/InputIndex;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/output_info/InputIndex;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/output_info/InputIndex;->defaultInstance:Lcom/google/speech/tts/output_info/InputIndex;

    .line 250
    invoke-static {}, Lcom/google/speech/tts/output_info/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 251
    sget-object v0, Lcom/google/speech/tts/output_info/InputIndex;->defaultInstance:Lcom/google/speech/tts/output_info/InputIndex;

    invoke-direct {v0}, Lcom/google/speech/tts/output_info/InputIndex;->initFields()V

    .line 252
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/tts/output_info/InputIndex;->unicodeIndex_:I

    .line 43
    iput v0, p0, Lcom/google/speech/tts/output_info/InputIndex;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/output_info/InputIndex;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/output_info/InputIndex$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/output_info/InputIndex$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/output_info/InputIndex;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, -0x1

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/tts/output_info/InputIndex;->unicodeIndex_:I

    .line 43
    iput v0, p0, Lcom/google/speech/tts/output_info/InputIndex;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/tts/output_info/InputIndex;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/InputIndex;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/output_info/InputIndex;->hasUnicodeIndex:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/output_info/InputIndex;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/InputIndex;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/output_info/InputIndex;->unicodeIndex_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/output_info/InputIndex;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/output_info/InputIndex;->defaultInstance:Lcom/google/speech/tts/output_info/InputIndex;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/output_info/InputIndex$Builder;
    .locals 1

    .prologue
    .line 124
    # invokes: Lcom/google/speech/tts/output_info/InputIndex$Builder;->create()Lcom/google/speech/tts/output_info/InputIndex$Builder;
    invoke-static {}, Lcom/google/speech/tts/output_info/InputIndex$Builder;->access$100()Lcom/google/speech/tts/output_info/InputIndex$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/output_info/InputIndex;

    .prologue
    .line 127
    invoke-static {}, Lcom/google/speech/tts/output_info/InputIndex;->newBuilder()Lcom/google/speech/tts/output_info/InputIndex$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/output_info/InputIndex$Builder;->mergeFrom(Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/InputIndex;->getDefaultInstanceForType()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/output_info/InputIndex;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/output_info/InputIndex;->defaultInstance:Lcom/google/speech/tts/output_info/InputIndex;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 45
    iget v0, p0, Lcom/google/speech/tts/output_info/InputIndex;->memoizedSerializedSize:I

    .line 46
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 54
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 48
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/InputIndex;->hasUnicodeIndex()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 50
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/InputIndex;->getUnicodeIndex()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 53
    :cond_1
    iput v0, p0, Lcom/google/speech/tts/output_info/InputIndex;->memoizedSerializedSize:I

    move v1, v0

    .line 54
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getUnicodeIndex()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/tts/output_info/InputIndex;->unicodeIndex_:I

    return v0
.end method

.method public hasUnicodeIndex()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/output_info/InputIndex;->hasUnicodeIndex:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/InputIndex;->toBuilder()Lcom/google/speech/tts/output_info/InputIndex$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/output_info/InputIndex$Builder;
    .locals 1

    .prologue
    .line 129
    invoke-static {p0}, Lcom/google/speech/tts/output_info/InputIndex;->newBuilder(Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/InputIndex;->getSerializedSize()I

    .line 38
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/InputIndex;->hasUnicodeIndex()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/InputIndex;->getUnicodeIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 41
    :cond_0
    return-void
.end method
