.class public final Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto$Pronunciation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/LexiconProto$Pronunciation;",
        "Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/LexiconProto$Pronunciation;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
    .locals 1

    .prologue
    .line 164
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->create()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
    .locals 3

    .prologue
    .line 173
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;-><init>()V

    .line 174
    .local v0, "builder":Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
    new-instance v1, Lcom/google/speech/tts/LexiconProto$Pronunciation;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/LexiconProto$Pronunciation;-><init>(Lcom/google/speech/tts/LexiconProto$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    .line 175
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->build()Lcom/google/speech/tts/LexiconProto$Pronunciation;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/LexiconProto$Pronunciation;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->buildPartial()Lcom/google/speech/tts/LexiconProto$Pronunciation;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/LexiconProto$Pronunciation;
    .locals 3

    .prologue
    .line 219
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    if-nez v1, :cond_0

    .line 220
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    .line 224
    .local v0, "returnMe":Lcom/google/speech/tts/LexiconProto$Pronunciation;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    .line 225
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
    .locals 2

    .prologue
    .line 192
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->create()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Pronunciation;)Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 164
    check-cast p1, Lcom/google/speech/tts/LexiconProto$Pronunciation;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Pronunciation;)Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/LexiconProto$Pronunciation;)Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/LexiconProto$Pronunciation;

    .prologue
    .line 229
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->getDefaultInstance()Lcom/google/speech/tts/LexiconProto$Pronunciation;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-object p0

    .line 230
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->setId(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    .line 233
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->getV()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->setV(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    goto :goto_0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 275
    if-nez p1, :cond_0

    .line 276
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasId:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->access$302(Lcom/google/speech/tts/LexiconProto$Pronunciation;Z)Z

    .line 279
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    # setter for: Lcom/google/speech/tts/LexiconProto$Pronunciation;->id_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->access$402(Lcom/google/speech/tts/LexiconProto$Pronunciation;Ljava/lang/String;)Ljava/lang/String;

    .line 280
    return-object p0
.end method

.method public setV(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 296
    if-nez p1, :cond_0

    .line 297
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasV:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->access$502(Lcom/google/speech/tts/LexiconProto$Pronunciation;Z)Z

    .line 300
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    # setter for: Lcom/google/speech/tts/LexiconProto$Pronunciation;->v_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->access$602(Lcom/google/speech/tts/LexiconProto$Pronunciation;Ljava/lang/String;)Ljava/lang/String;

    .line 301
    return-object p0
.end method
