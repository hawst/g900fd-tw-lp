.class public final enum Lcom/google/speech/tts/TimeAddressingType;
.super Ljava/lang/Enum;
.source "TimeAddressingType.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/TimeAddressingType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/TimeAddressingType;

.field public static final enum PHONEME_INDEX:Lcom/google/speech/tts/TimeAddressingType;

.field public static final enum UNIFORM:Lcom/google/speech/tts/TimeAddressingType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/TimeAddressingType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lcom/google/speech/tts/TimeAddressingType;

    const-string v1, "UNIFORM"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/speech/tts/TimeAddressingType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/TimeAddressingType;->UNIFORM:Lcom/google/speech/tts/TimeAddressingType;

    .line 8
    new-instance v0, Lcom/google/speech/tts/TimeAddressingType;

    const-string v1, "PHONEME_INDEX"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/speech/tts/TimeAddressingType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/TimeAddressingType;->PHONEME_INDEX:Lcom/google/speech/tts/TimeAddressingType;

    .line 5
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/speech/tts/TimeAddressingType;

    sget-object v1, Lcom/google/speech/tts/TimeAddressingType;->UNIFORM:Lcom/google/speech/tts/TimeAddressingType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/speech/tts/TimeAddressingType;->PHONEME_INDEX:Lcom/google/speech/tts/TimeAddressingType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/speech/tts/TimeAddressingType;->$VALUES:[Lcom/google/speech/tts/TimeAddressingType;

    .line 27
    new-instance v0, Lcom/google/speech/tts/TimeAddressingType$1;

    invoke-direct {v0}, Lcom/google/speech/tts/TimeAddressingType$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/TimeAddressingType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput p3, p0, Lcom/google/speech/tts/TimeAddressingType;->index:I

    .line 38
    iput p4, p0, Lcom/google/speech/tts/TimeAddressingType;->value:I

    .line 39
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/TimeAddressingType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/google/speech/tts/TimeAddressingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/TimeAddressingType;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/TimeAddressingType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/google/speech/tts/TimeAddressingType;->$VALUES:[Lcom/google/speech/tts/TimeAddressingType;

    invoke-virtual {v0}, [Lcom/google/speech/tts/TimeAddressingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/TimeAddressingType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 12
    iget v0, p0, Lcom/google/speech/tts/TimeAddressingType;->value:I

    return v0
.end method
