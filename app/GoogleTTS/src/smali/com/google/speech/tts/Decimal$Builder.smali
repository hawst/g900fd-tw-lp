.class public final Lcom/google/speech/tts/Decimal$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Decimal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Decimal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Decimal;",
        "Lcom/google/speech/tts/Decimal$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Decimal;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 273
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Decimal$Builder;
    .locals 1

    .prologue
    .line 267
    invoke-static {}, Lcom/google/speech/tts/Decimal$Builder;->create()Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Decimal$Builder;
    .locals 3

    .prologue
    .line 276
    new-instance v0, Lcom/google/speech/tts/Decimal$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Decimal$Builder;-><init>()V

    .line 277
    .local v0, "builder":Lcom/google/speech/tts/Decimal$Builder;
    new-instance v1, Lcom/google/speech/tts/Decimal;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Decimal;-><init>(Lcom/google/speech/tts/Decimal$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    .line 278
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal$Builder;->build()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Decimal;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    invoke-static {v0}, Lcom/google/speech/tts/Decimal$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 309
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal$Builder;->buildPartial()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Decimal;
    .locals 3

    .prologue
    .line 322
    iget-object v1, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    if-nez v1, :cond_0

    .line 323
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 326
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # getter for: Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Decimal;->access$300(Lcom/google/speech/tts/Decimal;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 327
    iget-object v1, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    iget-object v2, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # getter for: Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Decimal;->access$300(Lcom/google/speech/tts/Decimal;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Decimal;->access$302(Lcom/google/speech/tts/Decimal;Ljava/util/List;)Ljava/util/List;

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    .line 331
    .local v0, "returnMe":Lcom/google/speech/tts/Decimal;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    .line 332
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal$Builder;->clone()Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal$Builder;->clone()Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Decimal$Builder;
    .locals 2

    .prologue
    .line 295
    invoke-static {}, Lcom/google/speech/tts/Decimal$Builder;->create()Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Decimal$Builder;->mergeFrom(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal$Builder;->clone()Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    invoke-virtual {v0}, Lcom/google/speech/tts/Decimal;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 267
    check-cast p1, Lcom/google/speech/tts/Decimal;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Decimal$Builder;->mergeFrom(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 336
    invoke-static {}, Lcom/google/speech/tts/Decimal;->getDefaultInstance()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 370
    :cond_0
    :goto_0
    return-object p0

    .line 337
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->hasNegative()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 338
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->getNegative()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Decimal$Builder;->setNegative(Z)Lcom/google/speech/tts/Decimal$Builder;

    .line 340
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->hasIntegerPart()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 341
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->getIntegerPart()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Decimal$Builder;->setIntegerPart(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;

    .line 343
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->hasFractionalPart()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 344
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->getFractionalPart()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Decimal$Builder;->setFractionalPart(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;

    .line 346
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->hasQuantity()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 347
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->getQuantity()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Decimal$Builder;->setQuantity(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;

    .line 349
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->hasExponent()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 350
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->getExponent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Decimal$Builder;->setExponent(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;

    .line 352
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 353
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->getStyle()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Decimal$Builder;->setStyle(I)Lcom/google/speech/tts/Decimal$Builder;

    .line 355
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 356
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Decimal$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;

    .line 358
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->hasPreserveOrder()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 359
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->getPreserveOrder()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Decimal$Builder;->setPreserveOrder(Z)Lcom/google/speech/tts/Decimal$Builder;

    .line 361
    :cond_9
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 362
    invoke-virtual {p1}, Lcom/google/speech/tts/Decimal;->getCodeSwitch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Decimal$Builder;->setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;

    .line 364
    :cond_a
    # getter for: Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Decimal;->access$300(Lcom/google/speech/tts/Decimal;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # getter for: Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Decimal;->access$300(Lcom/google/speech/tts/Decimal;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 366
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Decimal;->access$302(Lcom/google/speech/tts/Decimal;Ljava/util/List;)Ljava/util/List;

    .line 368
    :cond_b
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # getter for: Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Decimal;->access$300(Lcom/google/speech/tts/Decimal;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Decimal;->access$300(Lcom/google/speech/tts/Decimal;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 600
    if-nez p1, :cond_0

    .line 601
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 603
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Decimal;->hasCodeSwitch:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Decimal;->access$2002(Lcom/google/speech/tts/Decimal;Z)Z

    .line 604
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # setter for: Lcom/google/speech/tts/Decimal;->codeSwitch_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Decimal;->access$2102(Lcom/google/speech/tts/Decimal;Ljava/lang/String;)Ljava/lang/String;

    .line 605
    return-object p0
.end method

.method public setExponent(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 522
    if-nez p1, :cond_0

    .line 523
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 525
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Decimal;->hasExponent:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Decimal;->access$1202(Lcom/google/speech/tts/Decimal;Z)Z

    .line 526
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # setter for: Lcom/google/speech/tts/Decimal;->exponent_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Decimal;->access$1302(Lcom/google/speech/tts/Decimal;Ljava/lang/String;)Ljava/lang/String;

    .line 527
    return-object p0
.end method

.method public setFractionalPart(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 480
    if-nez p1, :cond_0

    .line 481
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Decimal;->hasFractionalPart:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Decimal;->access$802(Lcom/google/speech/tts/Decimal;Z)Z

    .line 484
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # setter for: Lcom/google/speech/tts/Decimal;->fractionalPart_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Decimal;->access$902(Lcom/google/speech/tts/Decimal;Ljava/lang/String;)Ljava/lang/String;

    .line 485
    return-object p0
.end method

.method public setIntegerPart(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 459
    if-nez p1, :cond_0

    .line 460
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Decimal;->hasIntegerPart:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Decimal;->access$602(Lcom/google/speech/tts/Decimal;Z)Z

    .line 463
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # setter for: Lcom/google/speech/tts/Decimal;->integerPart_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Decimal;->access$702(Lcom/google/speech/tts/Decimal;Ljava/lang/String;)Ljava/lang/String;

    .line 464
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 561
    if-nez p1, :cond_0

    .line 562
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 564
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Decimal;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Decimal;->access$1602(Lcom/google/speech/tts/Decimal;Z)Z

    .line 565
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # setter for: Lcom/google/speech/tts/Decimal;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Decimal;->access$1702(Lcom/google/speech/tts/Decimal;Ljava/lang/String;)Ljava/lang/String;

    .line 566
    return-object p0
.end method

.method public setNegative(Z)Lcom/google/speech/tts/Decimal$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Decimal;->hasNegative:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Decimal;->access$402(Lcom/google/speech/tts/Decimal;Z)Z

    .line 442
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # setter for: Lcom/google/speech/tts/Decimal;->negative_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Decimal;->access$502(Lcom/google/speech/tts/Decimal;Z)Z

    .line 443
    return-object p0
.end method

.method public setPreserveOrder(Z)Lcom/google/speech/tts/Decimal$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Decimal;->hasPreserveOrder:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Decimal;->access$1802(Lcom/google/speech/tts/Decimal;Z)Z

    .line 583
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # setter for: Lcom/google/speech/tts/Decimal;->preserveOrder_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Decimal;->access$1902(Lcom/google/speech/tts/Decimal;Z)Z

    .line 584
    return-object p0
.end method

.method public setQuantity(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 501
    if-nez p1, :cond_0

    .line 502
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Decimal;->hasQuantity:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Decimal;->access$1002(Lcom/google/speech/tts/Decimal;Z)Z

    .line 505
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # setter for: Lcom/google/speech/tts/Decimal;->quantity_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Decimal;->access$1102(Lcom/google/speech/tts/Decimal;Ljava/lang/String;)Ljava/lang/String;

    .line 506
    return-object p0
.end method

.method public setStyle(I)Lcom/google/speech/tts/Decimal$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Decimal;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Decimal;->access$1402(Lcom/google/speech/tts/Decimal;Z)Z

    .line 544
    iget-object v0, p0, Lcom/google/speech/tts/Decimal$Builder;->result:Lcom/google/speech/tts/Decimal;

    # setter for: Lcom/google/speech/tts/Decimal;->style_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Decimal;->access$1502(Lcom/google/speech/tts/Decimal;I)I

    .line 545
    return-object p0
.end method
