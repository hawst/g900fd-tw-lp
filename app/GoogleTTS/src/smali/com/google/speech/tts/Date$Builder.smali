.class public final Lcom/google/speech/tts/Date$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Date.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Date;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Date;",
        "Lcom/google/speech/tts/Date$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Date;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 301
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Date$Builder;
    .locals 1

    .prologue
    .line 295
    invoke-static {}, Lcom/google/speech/tts/Date$Builder;->create()Lcom/google/speech/tts/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Date$Builder;
    .locals 3

    .prologue
    .line 304
    new-instance v0, Lcom/google/speech/tts/Date$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Date$Builder;-><init>()V

    .line 305
    .local v0, "builder":Lcom/google/speech/tts/Date$Builder;
    new-instance v1, Lcom/google/speech/tts/Date;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Date;-><init>(Lcom/google/speech/tts/Date$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    .line 306
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/speech/tts/Date$Builder;->build()Lcom/google/speech/tts/Date;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Date;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Date$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    invoke-static {v0}, Lcom/google/speech/tts/Date$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 337
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Date$Builder;->buildPartial()Lcom/google/speech/tts/Date;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Date;
    .locals 3

    .prologue
    .line 350
    iget-object v1, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    if-nez v1, :cond_0

    .line 351
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 354
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # getter for: Lcom/google/speech/tts/Date;->fieldOrder_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Date;->access$300(Lcom/google/speech/tts/Date;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 355
    iget-object v1, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    iget-object v2, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # getter for: Lcom/google/speech/tts/Date;->fieldOrder_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Date;->access$300(Lcom/google/speech/tts/Date;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Date;->fieldOrder_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Date;->access$302(Lcom/google/speech/tts/Date;Ljava/util/List;)Ljava/util/List;

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    .line 359
    .local v0, "returnMe":Lcom/google/speech/tts/Date;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    .line 360
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/speech/tts/Date$Builder;->clone()Lcom/google/speech/tts/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/speech/tts/Date$Builder;->clone()Lcom/google/speech/tts/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Date$Builder;
    .locals 2

    .prologue
    .line 323
    invoke-static {}, Lcom/google/speech/tts/Date$Builder;->create()Lcom/google/speech/tts/Date$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Date$Builder;->mergeFrom(Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/speech/tts/Date$Builder;->clone()Lcom/google/speech/tts/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    invoke-virtual {v0}, Lcom/google/speech/tts/Date;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 295
    check-cast p1, Lcom/google/speech/tts/Date;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Date$Builder;->mergeFrom(Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Date;

    .prologue
    .line 364
    invoke-static {}, Lcom/google/speech/tts/Date;->getDefaultInstance()Lcom/google/speech/tts/Date;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 404
    :cond_0
    :goto_0
    return-object p0

    .line 365
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->hasWeekday()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 366
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->getWeekday()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Date$Builder;->setWeekday(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;

    .line 368
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->hasDay()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 369
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->getDay()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Date$Builder;->setDay(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;

    .line 371
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->hasMonth()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 372
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->getMonth()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Date$Builder;->setMonth(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;

    .line 374
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->hasYear()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 375
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->getYear()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Date$Builder;->setYear(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;

    .line 377
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 378
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->getStyle()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Date$Builder;->setStyle(I)Lcom/google/speech/tts/Date$Builder;

    .line 380
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->hasText()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 381
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Date$Builder;->setText(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;

    .line 383
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->hasShortYear()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 384
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->getShortYear()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Date$Builder;->setShortYear(Z)Lcom/google/speech/tts/Date$Builder;

    .line 386
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->hasEra()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 387
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->getEra()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Date$Builder;->setEra(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;

    .line 389
    :cond_9
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 390
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Date$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;

    .line 392
    :cond_a
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->hasPreserveOrder()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 393
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->getPreserveOrder()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Date$Builder;->setPreserveOrder(Z)Lcom/google/speech/tts/Date$Builder;

    .line 395
    :cond_b
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 396
    invoke-virtual {p1}, Lcom/google/speech/tts/Date;->getCodeSwitch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Date$Builder;->setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;

    .line 398
    :cond_c
    # getter for: Lcom/google/speech/tts/Date;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Date;->access$300(Lcom/google/speech/tts/Date;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # getter for: Lcom/google/speech/tts/Date;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Date;->access$300(Lcom/google/speech/tts/Date;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 400
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Date;->fieldOrder_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Date;->access$302(Lcom/google/speech/tts/Date;Ljava/util/List;)Ljava/util/List;

    .line 402
    :cond_d
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # getter for: Lcom/google/speech/tts/Date;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Date;->access$300(Lcom/google/speech/tts/Date;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Date;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Date;->access$300(Lcom/google/speech/tts/Date;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 684
    if-nez p1, :cond_0

    .line 685
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 687
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Date;->hasCodeSwitch:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Date;->access$2402(Lcom/google/speech/tts/Date;Z)Z

    .line 688
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # setter for: Lcom/google/speech/tts/Date;->codeSwitch_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Date;->access$2502(Lcom/google/speech/tts/Date;Ljava/lang/String;)Ljava/lang/String;

    .line 689
    return-object p0
.end method

.method public setDay(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 504
    if-nez p1, :cond_0

    .line 505
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Date;->hasDay:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Date;->access$602(Lcom/google/speech/tts/Date;Z)Z

    .line 508
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # setter for: Lcom/google/speech/tts/Date;->day_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Date;->access$702(Lcom/google/speech/tts/Date;Ljava/lang/String;)Ljava/lang/String;

    .line 509
    return-object p0
.end method

.method public setEra(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 624
    if-nez p1, :cond_0

    .line 625
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 627
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Date;->hasEra:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Date;->access$1802(Lcom/google/speech/tts/Date;Z)Z

    .line 628
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # setter for: Lcom/google/speech/tts/Date;->era_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Date;->access$1902(Lcom/google/speech/tts/Date;Ljava/lang/String;)Ljava/lang/String;

    .line 629
    return-object p0
.end method

.method public setMonth(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 525
    if-nez p1, :cond_0

    .line 526
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Date;->hasMonth:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Date;->access$802(Lcom/google/speech/tts/Date;Z)Z

    .line 529
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # setter for: Lcom/google/speech/tts/Date;->month_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Date;->access$902(Lcom/google/speech/tts/Date;Ljava/lang/String;)Ljava/lang/String;

    .line 530
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 645
    if-nez p1, :cond_0

    .line 646
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 648
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Date;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Date;->access$2002(Lcom/google/speech/tts/Date;Z)Z

    .line 649
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # setter for: Lcom/google/speech/tts/Date;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Date;->access$2102(Lcom/google/speech/tts/Date;Ljava/lang/String;)Ljava/lang/String;

    .line 650
    return-object p0
.end method

.method public setPreserveOrder(Z)Lcom/google/speech/tts/Date$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 666
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Date;->hasPreserveOrder:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Date;->access$2202(Lcom/google/speech/tts/Date;Z)Z

    .line 667
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # setter for: Lcom/google/speech/tts/Date;->preserveOrder_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Date;->access$2302(Lcom/google/speech/tts/Date;Z)Z

    .line 668
    return-object p0
.end method

.method public setShortYear(Z)Lcom/google/speech/tts/Date$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Date;->hasShortYear:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Date;->access$1602(Lcom/google/speech/tts/Date;Z)Z

    .line 607
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # setter for: Lcom/google/speech/tts/Date;->shortYear_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Date;->access$1702(Lcom/google/speech/tts/Date;Z)Z

    .line 608
    return-object p0
.end method

.method public setStyle(I)Lcom/google/speech/tts/Date$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Date;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Date;->access$1202(Lcom/google/speech/tts/Date;Z)Z

    .line 568
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # setter for: Lcom/google/speech/tts/Date;->style_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Date;->access$1302(Lcom/google/speech/tts/Date;I)I

    .line 569
    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 585
    if-nez p1, :cond_0

    .line 586
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 588
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Date;->hasText:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Date;->access$1402(Lcom/google/speech/tts/Date;Z)Z

    .line 589
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # setter for: Lcom/google/speech/tts/Date;->text_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Date;->access$1502(Lcom/google/speech/tts/Date;Ljava/lang/String;)Ljava/lang/String;

    .line 590
    return-object p0
.end method

.method public setWeekday(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 483
    if-nez p1, :cond_0

    .line 484
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Date;->hasWeekday:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Date;->access$402(Lcom/google/speech/tts/Date;Z)Z

    .line 487
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # setter for: Lcom/google/speech/tts/Date;->weekday_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Date;->access$502(Lcom/google/speech/tts/Date;Ljava/lang/String;)Ljava/lang/String;

    .line 488
    return-object p0
.end method

.method public setYear(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 546
    if-nez p1, :cond_0

    .line 547
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 549
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Date;->hasYear:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Date;->access$1002(Lcom/google/speech/tts/Date;Z)Z

    .line 550
    iget-object v0, p0, Lcom/google/speech/tts/Date$Builder;->result:Lcom/google/speech/tts/Date;

    # setter for: Lcom/google/speech/tts/Date;->year_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Date;->access$1102(Lcom/google/speech/tts/Date;Ljava/lang/String;)Ljava/lang/String;

    .line 551
    return-object p0
.end method
