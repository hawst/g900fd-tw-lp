.class public final Lcom/google/speech/tts/VoiceModPair;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "VoiceModPair.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/VoiceModPair$1;,
        Lcom/google/speech/tts/VoiceModPair$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/VoiceModPair;


# instance fields
.field private deviation_:F

.field private hasDeviation:Z

.field private hasT:Z

.field private hasV:Z

.field private memoizedSerializedSize:I

.field private t_:F

.field private v_:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 329
    new-instance v0, Lcom/google/speech/tts/VoiceModPair;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/VoiceModPair;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/VoiceModPair;->defaultInstance:Lcom/google/speech/tts/VoiceModPair;

    .line 330
    invoke-static {}, Lcom/google/speech/tts/VoiceModProto;->internalForceInit()V

    .line 331
    sget-object v0, Lcom/google/speech/tts/VoiceModPair;->defaultInstance:Lcom/google/speech/tts/VoiceModPair;

    invoke-direct {v0}, Lcom/google/speech/tts/VoiceModPair;->initFields()V

    .line 332
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/tts/VoiceModPair;->t_:F

    .line 32
    iput v0, p0, Lcom/google/speech/tts/VoiceModPair;->v_:F

    .line 39
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/speech/tts/VoiceModPair;->deviation_:F

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/VoiceModPair;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/VoiceModPair;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/VoiceModPair$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/VoiceModPair$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/VoiceModPair;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/tts/VoiceModPair;->t_:F

    .line 32
    iput v0, p0, Lcom/google/speech/tts/VoiceModPair;->v_:F

    .line 39
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/speech/tts/VoiceModPair;->deviation_:F

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/VoiceModPair;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/tts/VoiceModPair;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VoiceModPair;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/VoiceModPair;->hasT:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/VoiceModPair;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VoiceModPair;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/VoiceModPair;->t_:F

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/VoiceModPair;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VoiceModPair;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/VoiceModPair;->hasV:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/VoiceModPair;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VoiceModPair;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/VoiceModPair;->v_:F

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/VoiceModPair;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VoiceModPair;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/VoiceModPair;->hasDeviation:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/VoiceModPair;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VoiceModPair;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/VoiceModPair;->deviation_:F

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/VoiceModPair;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/VoiceModPair;->defaultInstance:Lcom/google/speech/tts/VoiceModPair;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/VoiceModPair$Builder;
    .locals 1

    .prologue
    .line 154
    # invokes: Lcom/google/speech/tts/VoiceModPair$Builder;->create()Lcom/google/speech/tts/VoiceModPair$Builder;
    invoke-static {}, Lcom/google/speech/tts/VoiceModPair$Builder;->access$100()Lcom/google/speech/tts/VoiceModPair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/VoiceModPair;)Lcom/google/speech/tts/VoiceModPair$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/VoiceModPair;

    .prologue
    .line 157
    invoke-static {}, Lcom/google/speech/tts/VoiceModPair;->newBuilder()Lcom/google/speech/tts/VoiceModPair$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/VoiceModPair$Builder;->mergeFrom(Lcom/google/speech/tts/VoiceModPair;)Lcom/google/speech/tts/VoiceModPair$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->getDefaultInstanceForType()Lcom/google/speech/tts/VoiceModPair;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/VoiceModPair;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/VoiceModPair;->defaultInstance:Lcom/google/speech/tts/VoiceModPair;

    return-object v0
.end method

.method public getDeviation()F
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/speech/tts/VoiceModPair;->deviation_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 67
    iget v0, p0, Lcom/google/speech/tts/VoiceModPair;->memoizedSerializedSize:I

    .line 68
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 84
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 70
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 71
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->hasT()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 72
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->getT()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 75
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->hasV()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 76
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->getV()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 79
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->hasDeviation()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 80
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->getDeviation()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 83
    :cond_3
    iput v0, p0, Lcom/google/speech/tts/VoiceModPair;->memoizedSerializedSize:I

    move v1, v0

    .line 84
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getT()F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/tts/VoiceModPair;->t_:F

    return v0
.end method

.method public getV()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/speech/tts/VoiceModPair;->v_:F

    return v0
.end method

.method public hasDeviation()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/VoiceModPair;->hasDeviation:Z

    return v0
.end method

.method public hasT()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/VoiceModPair;->hasT:Z

    return v0
.end method

.method public hasV()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/VoiceModPair;->hasV:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 46
    iget-boolean v1, p0, Lcom/google/speech/tts/VoiceModPair;->hasT:Z

    if-nez v1, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v0

    .line 47
    :cond_1
    iget-boolean v1, p0, Lcom/google/speech/tts/VoiceModPair;->hasV:Z

    if-eqz v1, :cond_0

    .line 48
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->toBuilder()Lcom/google/speech/tts/VoiceModPair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/VoiceModPair$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-static {p0}, Lcom/google/speech/tts/VoiceModPair;->newBuilder(Lcom/google/speech/tts/VoiceModPair;)Lcom/google/speech/tts/VoiceModPair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->getSerializedSize()I

    .line 54
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->hasT()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->getT()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->hasV()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->getV()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 60
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->hasDeviation()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceModPair;->getDeviation()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 63
    :cond_2
    return-void
.end method
