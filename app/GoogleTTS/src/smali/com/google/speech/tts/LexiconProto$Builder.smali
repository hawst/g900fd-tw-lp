.class public final Lcom/google/speech/tts/LexiconProto$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/LexiconProto;",
        "Lcom/google/speech/tts/LexiconProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/LexiconProto;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2762
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$5200()Lcom/google/speech/tts/LexiconProto$Builder;
    .locals 1

    .prologue
    .line 2756
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Builder;->create()Lcom/google/speech/tts/LexiconProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/LexiconProto$Builder;
    .locals 3

    .prologue
    .line 2765
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Builder;-><init>()V

    .line 2766
    .local v0, "builder":Lcom/google/speech/tts/LexiconProto$Builder;
    new-instance v1, Lcom/google/speech/tts/LexiconProto;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/LexiconProto;-><init>(Lcom/google/speech/tts/LexiconProto$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    .line 2767
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2756
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Builder;->build()Lcom/google/speech/tts/LexiconProto;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/LexiconProto;
    .locals 1

    .prologue
    .line 2795
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2796
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2798
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Builder;->buildPartial()Lcom/google/speech/tts/LexiconProto;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/LexiconProto;
    .locals 3

    .prologue
    .line 2811
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    if-nez v1, :cond_0

    .line 2812
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2815
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    # getter for: Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/LexiconProto;->access$5400(Lcom/google/speech/tts/LexiconProto;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 2816
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    iget-object v2, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    # getter for: Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/LexiconProto;->access$5400(Lcom/google/speech/tts/LexiconProto;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/LexiconProto;->access$5402(Lcom/google/speech/tts/LexiconProto;Ljava/util/List;)Ljava/util/List;

    .line 2819
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    .line 2820
    .local v0, "returnMe":Lcom/google/speech/tts/LexiconProto;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    .line 2821
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2756
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2756
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/LexiconProto$Builder;
    .locals 2

    .prologue
    .line 2784
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Builder;->create()Lcom/google/speech/tts/LexiconProto$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/LexiconProto$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto;)Lcom/google/speech/tts/LexiconProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2756
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 2792
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 2756
    check-cast p1, Lcom/google/speech/tts/LexiconProto;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/LexiconProto$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto;)Lcom/google/speech/tts/LexiconProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/LexiconProto;)Lcom/google/speech/tts/LexiconProto$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/LexiconProto;

    .prologue
    .line 2825
    invoke-static {}, Lcom/google/speech/tts/LexiconProto;->getDefaultInstance()Lcom/google/speech/tts/LexiconProto;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 2838
    :cond_0
    :goto_0
    return-object p0

    .line 2826
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto;->hasHeader()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2827
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto;->getHeader()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Builder;->setHeader(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Builder;

    .line 2829
    :cond_2
    # getter for: Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/LexiconProto;->access$5400(Lcom/google/speech/tts/LexiconProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2830
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    # getter for: Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto;->access$5400(Lcom/google/speech/tts/LexiconProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2831
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto;->access$5402(Lcom/google/speech/tts/LexiconProto;Ljava/util/List;)Ljava/util/List;

    .line 2833
    :cond_3
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    # getter for: Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto;->access$5400(Lcom/google/speech/tts/LexiconProto;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/LexiconProto;->access$5400(Lcom/google/speech/tts/LexiconProto;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2835
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto;->hasLegalVowels()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2836
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto;->getLegalVowels()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Builder;->setLegalVowels(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Builder;

    goto :goto_0
.end method

.method public setHeader(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2883
    if-nez p1, :cond_0

    .line 2884
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2886
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto;->hasHeader:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto;->access$5502(Lcom/google/speech/tts/LexiconProto;Z)Z

    .line 2887
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    # setter for: Lcom/google/speech/tts/LexiconProto;->header_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto;->access$5602(Lcom/google/speech/tts/LexiconProto;Ljava/lang/String;)Ljava/lang/String;

    .line 2888
    return-object p0
.end method

.method public setLegalVowels(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2955
    if-nez p1, :cond_0

    .line 2956
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2958
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto;->hasLegalVowels:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto;->access$5702(Lcom/google/speech/tts/LexiconProto;Z)Z

    .line 2959
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Builder;->result:Lcom/google/speech/tts/LexiconProto;

    # setter for: Lcom/google/speech/tts/LexiconProto;->legalVowels_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto;->access$5802(Lcom/google/speech/tts/LexiconProto;Ljava/lang/String;)Ljava/lang/String;

    .line 2960
    return-object p0
.end method
