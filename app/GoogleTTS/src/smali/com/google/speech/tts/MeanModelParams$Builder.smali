.class public final Lcom/google/speech/tts/MeanModelParams$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MeanModelParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/MeanModelParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/MeanModelParams;",
        "Lcom/google/speech/tts/MeanModelParams$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/MeanModelParams;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/MeanModelParams$Builder;
    .locals 1

    .prologue
    .line 146
    invoke-static {}, Lcom/google/speech/tts/MeanModelParams$Builder;->create()Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/MeanModelParams$Builder;
    .locals 3

    .prologue
    .line 155
    new-instance v0, Lcom/google/speech/tts/MeanModelParams$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/MeanModelParams$Builder;-><init>()V

    .line 156
    .local v0, "builder":Lcom/google/speech/tts/MeanModelParams$Builder;
    new-instance v1, Lcom/google/speech/tts/MeanModelParams;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/MeanModelParams;-><init>(Lcom/google/speech/tts/MeanModelParams$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/MeanModelParams$Builder;->result:Lcom/google/speech/tts/MeanModelParams;

    .line 157
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams$Builder;->build()Lcom/google/speech/tts/MeanModelParams;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/MeanModelParams;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/speech/tts/MeanModelParams$Builder;->result:Lcom/google/speech/tts/MeanModelParams;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/speech/tts/MeanModelParams$Builder;->result:Lcom/google/speech/tts/MeanModelParams;

    invoke-static {v0}, Lcom/google/speech/tts/MeanModelParams$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams$Builder;->buildPartial()Lcom/google/speech/tts/MeanModelParams;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/MeanModelParams;
    .locals 3

    .prologue
    .line 201
    iget-object v1, p0, Lcom/google/speech/tts/MeanModelParams$Builder;->result:Lcom/google/speech/tts/MeanModelParams;

    if-nez v1, :cond_0

    .line 202
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/MeanModelParams$Builder;->result:Lcom/google/speech/tts/MeanModelParams;

    .line 206
    .local v0, "returnMe":Lcom/google/speech/tts/MeanModelParams;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/MeanModelParams$Builder;->result:Lcom/google/speech/tts/MeanModelParams;

    .line 207
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams$Builder;->clone()Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams$Builder;->clone()Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/MeanModelParams$Builder;
    .locals 2

    .prologue
    .line 174
    invoke-static {}, Lcom/google/speech/tts/MeanModelParams$Builder;->create()Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/MeanModelParams$Builder;->result:Lcom/google/speech/tts/MeanModelParams;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/MeanModelParams$Builder;->mergeFrom(Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/speech/tts/MeanModelParams$Builder;->clone()Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/speech/tts/MeanModelParams$Builder;->result:Lcom/google/speech/tts/MeanModelParams;

    invoke-virtual {v0}, Lcom/google/speech/tts/MeanModelParams;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 146
    check-cast p1, Lcom/google/speech/tts/MeanModelParams;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/MeanModelParams$Builder;->mergeFrom(Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/MeanModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/MeanModelParams$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/MeanModelParams;

    .prologue
    .line 211
    invoke-static {}, Lcom/google/speech/tts/MeanModelParams;->getDefaultInstance()Lcom/google/speech/tts/MeanModelParams;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-object p0

    .line 212
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/MeanModelParams;->hasMean()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    invoke-virtual {p1}, Lcom/google/speech/tts/MeanModelParams;->getMean()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/MeanModelParams$Builder;->setMean(F)Lcom/google/speech/tts/MeanModelParams$Builder;

    .line 215
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/MeanModelParams;->hasDeviation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {p1}, Lcom/google/speech/tts/MeanModelParams;->getDeviation()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/MeanModelParams$Builder;->setDeviation(F)Lcom/google/speech/tts/MeanModelParams$Builder;

    goto :goto_0
.end method

.method public setDeviation(F)Lcom/google/speech/tts/MeanModelParams$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/speech/tts/MeanModelParams$Builder;->result:Lcom/google/speech/tts/MeanModelParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/MeanModelParams;->hasDeviation:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/MeanModelParams;->access$502(Lcom/google/speech/tts/MeanModelParams;Z)Z

    .line 276
    iget-object v0, p0, Lcom/google/speech/tts/MeanModelParams$Builder;->result:Lcom/google/speech/tts/MeanModelParams;

    # setter for: Lcom/google/speech/tts/MeanModelParams;->deviation_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/MeanModelParams;->access$602(Lcom/google/speech/tts/MeanModelParams;F)F

    .line 277
    return-object p0
.end method

.method public setMean(F)Lcom/google/speech/tts/MeanModelParams$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/speech/tts/MeanModelParams$Builder;->result:Lcom/google/speech/tts/MeanModelParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/MeanModelParams;->hasMean:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/MeanModelParams;->access$302(Lcom/google/speech/tts/MeanModelParams;Z)Z

    .line 258
    iget-object v0, p0, Lcom/google/speech/tts/MeanModelParams$Builder;->result:Lcom/google/speech/tts/MeanModelParams;

    # setter for: Lcom/google/speech/tts/MeanModelParams;->mean_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/MeanModelParams;->access$402(Lcom/google/speech/tts/MeanModelParams;F)F

    .line 259
    return-object p0
.end method
