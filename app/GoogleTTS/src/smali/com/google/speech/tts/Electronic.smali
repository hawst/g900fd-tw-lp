.class public final Lcom/google/speech/tts/Electronic;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Electronic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Electronic$1;,
        Lcom/google/speech/tts/Electronic$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Electronic;


# instance fields
.field private codeSwitch_:Ljava/lang/String;

.field private domain_:Ljava/lang/String;

.field private fieldOrder_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private fragmentId_:Ljava/lang/String;

.field private hasCodeSwitch:Z

.field private hasDomain:Z

.field private hasFragmentId:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasPassword:Z

.field private hasPath:Z

.field private hasPort:Z

.field private hasPreserveOrder:Z

.field private hasProtocol:Z

.field private hasQueryString:Z

.field private hasUsername:Z

.field private memoizedSerializedSize:I

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private password_:Ljava/lang/String;

.field private path_:Ljava/lang/String;

.field private port_:I

.field private preserveOrder_:Z

.field private protocol_:Ljava/lang/String;

.field private queryString_:Ljava/lang/String;

.field private username_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 744
    new-instance v0, Lcom/google/speech/tts/Electronic;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Electronic;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Electronic;->defaultInstance:Lcom/google/speech/tts/Electronic;

    .line 745
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 746
    sget-object v0, Lcom/google/speech/tts/Electronic;->defaultInstance:Lcom/google/speech/tts/Electronic;

    invoke-direct {v0}, Lcom/google/speech/tts/Electronic;->initFields()V

    .line 747
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->protocol_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->username_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->password_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->domain_:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/google/speech/tts/Electronic;->port_:I

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->path_:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->queryString_:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->fragmentId_:Ljava/lang/String;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 88
    iput-boolean v1, p0, Lcom/google/speech/tts/Electronic;->preserveOrder_:Z

    .line 95
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->codeSwitch_:Ljava/lang/String;

    .line 101
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;

    .line 158
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Electronic;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Electronic;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Electronic$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Electronic$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Electronic;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->protocol_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->username_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->password_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->domain_:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/google/speech/tts/Electronic;->port_:I

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->path_:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->queryString_:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->fragmentId_:Ljava/lang/String;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 88
    iput-boolean v1, p0, Lcom/google/speech/tts/Electronic;->preserveOrder_:Z

    .line 95
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->codeSwitch_:Ljava/lang/String;

    .line 101
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;

    .line 158
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Electronic;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Electronic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Electronic;->hasDomain:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Electronic;->domain_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/Electronic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Electronic;->hasPort:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/Electronic;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Electronic;->port_:I

    return p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/Electronic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Electronic;->hasPath:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Electronic;->path_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/Electronic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Electronic;->hasQueryString:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Electronic;->queryString_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/google/speech/tts/Electronic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Electronic;->hasFragmentId:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Electronic;->fragmentId_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2002(Lcom/google/speech/tts/Electronic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Electronic;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$2102(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Electronic;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2202(Lcom/google/speech/tts/Electronic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Electronic;->hasPreserveOrder:Z

    return p1
.end method

.method static synthetic access$2302(Lcom/google/speech/tts/Electronic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Electronic;->preserveOrder_:Z

    return p1
.end method

.method static synthetic access$2402(Lcom/google/speech/tts/Electronic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Electronic;->hasCodeSwitch:Z

    return p1
.end method

.method static synthetic access$2502(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Electronic;->codeSwitch_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Electronic;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Electronic;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Electronic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Electronic;->hasProtocol:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Electronic;->protocol_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Electronic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Electronic;->hasUsername:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Electronic;->username_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Electronic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Electronic;->hasPassword:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Electronic;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Electronic;->password_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Electronic;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Electronic;->defaultInstance:Lcom/google/speech/tts/Electronic;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Electronic$Builder;
    .locals 1

    .prologue
    .line 288
    # invokes: Lcom/google/speech/tts/Electronic$Builder;->create()Lcom/google/speech/tts/Electronic$Builder;
    invoke-static {}, Lcom/google/speech/tts/Electronic$Builder;->access$100()Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Electronic;

    .prologue
    .line 291
    invoke-static {}, Lcom/google/speech/tts/Electronic;->newBuilder()Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Electronic$Builder;->mergeFrom(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCodeSwitch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/speech/tts/Electronic;->codeSwitch_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getDefaultInstanceForType()Lcom/google/speech/tts/Electronic;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Electronic;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Electronic;->defaultInstance:Lcom/google/speech/tts/Electronic;

    return-object v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/tts/Electronic;->domain_:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldOrderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method public getFragmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/speech/tts/Electronic;->fragmentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/speech/tts/Electronic;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/Electronic;->password_:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/speech/tts/Electronic;->path_:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/speech/tts/Electronic;->port_:I

    return v0
.end method

.method public getPreserveOrder()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/google/speech/tts/Electronic;->preserveOrder_:Z

    return v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Electronic;->protocol_:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/speech/tts/Electronic;->queryString_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 160
    iget v3, p0, Lcom/google/speech/tts/Electronic;->memoizedSerializedSize:I

    .line 161
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 218
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 163
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 164
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasProtocol()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 165
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 168
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasUsername()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 169
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getUsername()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 172
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasPassword()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 173
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getPassword()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 176
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasDomain()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 177
    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getDomain()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 180
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasPort()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 181
    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getPort()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 184
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasPath()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 185
    const/4 v5, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 188
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasQueryString()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 189
    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getQueryString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 192
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasFragmentId()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 193
    const/16 v5, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getFragmentId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 196
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasMorphosyntacticFeatures()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 197
    const/16 v5, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 200
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasPreserveOrder()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 201
    const/16 v5, 0xa

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getPreserveOrder()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 204
    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasCodeSwitch()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 205
    const/16 v5, 0xb

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getCodeSwitch()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 209
    :cond_b
    const/4 v0, 0x0

    .line 210
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 211
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 213
    goto :goto_1

    .line 214
    .end local v1    # "element":Ljava/lang/String;
    :cond_c
    add-int/2addr v3, v0

    .line 215
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 217
    iput v3, p0, Lcom/google/speech/tts/Electronic;->memoizedSerializedSize:I

    move v4, v3

    .line 218
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/Electronic;->username_:Ljava/lang/String;

    return-object v0
.end method

.method public hasCodeSwitch()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/speech/tts/Electronic;->hasCodeSwitch:Z

    return v0
.end method

.method public hasDomain()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/Electronic;->hasDomain:Z

    return v0
.end method

.method public hasFragmentId()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/speech/tts/Electronic;->hasFragmentId:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/speech/tts/Electronic;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasPassword()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/Electronic;->hasPassword:Z

    return v0
.end method

.method public hasPath()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/Electronic;->hasPath:Z

    return v0
.end method

.method public hasPort()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/Electronic;->hasPort:Z

    return v0
.end method

.method public hasPreserveOrder()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/speech/tts/Electronic;->hasPreserveOrder:Z

    return v0
.end method

.method public hasProtocol()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Electronic;->hasProtocol:Z

    return v0
.end method

.method public hasQueryString()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/Electronic;->hasQueryString:Z

    return v0
.end method

.method public hasUsername()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Electronic;->hasUsername:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->toBuilder()Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Electronic$Builder;
    .locals 1

    .prologue
    .line 293
    invoke-static {p0}, Lcom/google/speech/tts/Electronic;->newBuilder(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getSerializedSize()I

    .line 120
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasProtocol()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 121
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getProtocol()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 123
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasUsername()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 124
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getUsername()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 126
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasPassword()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 127
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getPassword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 129
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasDomain()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 130
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getDomain()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 132
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasPort()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 133
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getPort()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 135
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasPath()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 136
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 138
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasQueryString()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 139
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getQueryString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 141
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasFragmentId()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 142
    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getFragmentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 144
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 145
    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 147
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasPreserveOrder()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 148
    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getPreserveOrder()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 150
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->hasCodeSwitch()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 151
    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getCodeSwitch()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 153
    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic;->getFieldOrderList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 154
    .local v0, "element":Ljava/lang/String;
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 156
    .end local v0    # "element":Ljava/lang/String;
    :cond_b
    return-void
.end method
