.class public final Lcom/google/speech/tts/Style;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Style.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Style$1;,
        Lcom/google/speech/tts/Style$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Style;


# instance fields
.field private date_:I

.field private fraction_:I

.field private hasDate:Z

.field private hasFraction:Z

.field private hasMeasure:Z

.field private hasMoney:Z

.field private hasTelephone:Z

.field private hasTime:Z

.field private measure_:I

.field private memoizedSerializedSize:I

.field private money_:I

.field private telephone_:I

.field private time_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 444
    new-instance v0, Lcom/google/speech/tts/Style;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Style;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Style;->defaultInstance:Lcom/google/speech/tts/Style;

    .line 445
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 446
    sget-object v0, Lcom/google/speech/tts/Style;->defaultInstance:Lcom/google/speech/tts/Style;

    invoke-direct {v0}, Lcom/google/speech/tts/Style;->initFields()V

    .line 447
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/tts/Style;->fraction_:I

    .line 32
    iput v0, p0, Lcom/google/speech/tts/Style;->time_:I

    .line 39
    iput v0, p0, Lcom/google/speech/tts/Style;->money_:I

    .line 46
    iput v0, p0, Lcom/google/speech/tts/Style;->measure_:I

    .line 53
    iput v0, p0, Lcom/google/speech/tts/Style;->telephone_:I

    .line 60
    iput v0, p0, Lcom/google/speech/tts/Style;->date_:I

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Style;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Style;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Style$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Style$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Style;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/tts/Style;->fraction_:I

    .line 32
    iput v0, p0, Lcom/google/speech/tts/Style;->time_:I

    .line 39
    iput v0, p0, Lcom/google/speech/tts/Style;->money_:I

    .line 46
    iput v0, p0, Lcom/google/speech/tts/Style;->measure_:I

    .line 53
    iput v0, p0, Lcom/google/speech/tts/Style;->telephone_:I

    .line 60
    iput v0, p0, Lcom/google/speech/tts/Style;->date_:I

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Style;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Style;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Style;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Style;->measure_:I

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/Style;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Style;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Style;->hasTelephone:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/Style;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Style;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Style;->telephone_:I

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/Style;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Style;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Style;->hasDate:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/Style;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Style;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Style;->date_:I

    return p1
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Style;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Style;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Style;->hasFraction:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Style;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Style;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Style;->fraction_:I

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Style;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Style;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Style;->hasTime:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Style;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Style;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Style;->time_:I

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Style;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Style;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Style;->hasMoney:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Style;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Style;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Style;->money_:I

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Style;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Style;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Style;->hasMeasure:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Style;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Style;->defaultInstance:Lcom/google/speech/tts/Style;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Style$Builder;
    .locals 1

    .prologue
    .line 194
    # invokes: Lcom/google/speech/tts/Style$Builder;->create()Lcom/google/speech/tts/Style$Builder;
    invoke-static {}, Lcom/google/speech/tts/Style$Builder;->access$100()Lcom/google/speech/tts/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Style$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Style;

    .prologue
    .line 197
    invoke-static {}, Lcom/google/speech/tts/Style;->newBuilder()Lcom/google/speech/tts/Style$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Style$Builder;->mergeFrom(Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Style$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDate()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/google/speech/tts/Style;->date_:I

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getDefaultInstanceForType()Lcom/google/speech/tts/Style;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Style;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Style;->defaultInstance:Lcom/google/speech/tts/Style;

    return-object v0
.end method

.method public getFraction()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/tts/Style;->fraction_:I

    return v0
.end method

.method public getMeasure()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/speech/tts/Style;->measure_:I

    return v0
.end method

.method public getMoney()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/speech/tts/Style;->money_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 95
    iget v0, p0, Lcom/google/speech/tts/Style;->memoizedSerializedSize:I

    .line 96
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 124
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 98
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 99
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->hasFraction()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 100
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getFraction()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 103
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->hasTime()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 104
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getTime()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 107
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->hasMoney()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 108
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getMoney()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 111
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->hasMeasure()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 112
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getMeasure()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 115
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->hasTelephone()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 116
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getTelephone()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 119
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->hasDate()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 120
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getDate()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 123
    :cond_6
    iput v0, p0, Lcom/google/speech/tts/Style;->memoizedSerializedSize:I

    move v1, v0

    .line 124
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getTelephone()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/speech/tts/Style;->telephone_:I

    return v0
.end method

.method public getTime()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/speech/tts/Style;->time_:I

    return v0
.end method

.method public hasDate()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/Style;->hasDate:Z

    return v0
.end method

.method public hasFraction()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Style;->hasFraction:Z

    return v0
.end method

.method public hasMeasure()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/Style;->hasMeasure:Z

    return v0
.end method

.method public hasMoney()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/Style;->hasMoney:Z

    return v0
.end method

.method public hasTelephone()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/Style;->hasTelephone:Z

    return v0
.end method

.method public hasTime()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Style;->hasTime:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->toBuilder()Lcom/google/speech/tts/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Style$Builder;
    .locals 1

    .prologue
    .line 199
    invoke-static {p0}, Lcom/google/speech/tts/Style;->newBuilder(Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getSerializedSize()I

    .line 73
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getFraction()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getTime()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 79
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->hasMoney()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getMoney()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 82
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->hasMeasure()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 83
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getMeasure()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 85
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->hasTelephone()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 86
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getTelephone()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 88
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 89
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Style;->getDate()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 91
    :cond_5
    return-void
.end method
