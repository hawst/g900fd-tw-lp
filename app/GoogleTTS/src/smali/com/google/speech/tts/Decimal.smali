.class public final Lcom/google/speech/tts/Decimal;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Decimal.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Decimal$1;,
        Lcom/google/speech/tts/Decimal$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Decimal;


# instance fields
.field private codeSwitch_:Ljava/lang/String;

.field private exponent_:Ljava/lang/String;

.field private fieldOrder_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private fractionalPart_:Ljava/lang/String;

.field private hasCodeSwitch:Z

.field private hasExponent:Z

.field private hasFractionalPart:Z

.field private hasIntegerPart:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasNegative:Z

.field private hasPreserveOrder:Z

.field private hasQuantity:Z

.field private hasStyle:Z

.field private integerPart_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private negative_:Z

.field private preserveOrder_:Z

.field private quantity_:Ljava/lang/String;

.field private style_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 657
    new-instance v0, Lcom/google/speech/tts/Decimal;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Decimal;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Decimal;->defaultInstance:Lcom/google/speech/tts/Decimal;

    .line 658
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 659
    sget-object v0, Lcom/google/speech/tts/Decimal;->defaultInstance:Lcom/google/speech/tts/Decimal;

    invoke-direct {v0}, Lcom/google/speech/tts/Decimal;->initFields()V

    .line 660
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput-boolean v1, p0, Lcom/google/speech/tts/Decimal;->negative_:Z

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->integerPart_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->fractionalPart_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->quantity_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->exponent_:Ljava/lang/String;

    .line 60
    iput v1, p0, Lcom/google/speech/tts/Decimal;->style_:I

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 74
    iput-boolean v1, p0, Lcom/google/speech/tts/Decimal;->preserveOrder_:Z

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->codeSwitch_:Ljava/lang/String;

    .line 87
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;

    .line 138
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Decimal;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Decimal;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Decimal$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Decimal$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Decimal;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput-boolean v1, p0, Lcom/google/speech/tts/Decimal;->negative_:Z

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->integerPart_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->fractionalPart_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->quantity_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->exponent_:Ljava/lang/String;

    .line 60
    iput v1, p0, Lcom/google/speech/tts/Decimal;->style_:I

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 74
    iput-boolean v1, p0, Lcom/google/speech/tts/Decimal;->preserveOrder_:Z

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->codeSwitch_:Ljava/lang/String;

    .line 87
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;

    .line 138
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Decimal;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Decimal;->hasQuantity:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/Decimal;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Decimal;->quantity_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Decimal;->hasExponent:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/Decimal;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Decimal;->exponent_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Decimal;->hasStyle:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/Decimal;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Decimal;->style_:I

    return p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Decimal;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/Decimal;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Decimal;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/google/speech/tts/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Decimal;->hasPreserveOrder:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/google/speech/tts/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Decimal;->preserveOrder_:Z

    return p1
.end method

.method static synthetic access$2002(Lcom/google/speech/tts/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Decimal;->hasCodeSwitch:Z

    return p1
.end method

.method static synthetic access$2102(Lcom/google/speech/tts/Decimal;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Decimal;->codeSwitch_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Decimal;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Decimal;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Decimal;->hasNegative:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Decimal;->negative_:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Decimal;->hasIntegerPart:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Decimal;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Decimal;->integerPart_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Decimal;->hasFractionalPart:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Decimal;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Decimal;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Decimal;->fractionalPart_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Decimal;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Decimal;->defaultInstance:Lcom/google/speech/tts/Decimal;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Decimal$Builder;
    .locals 1

    .prologue
    .line 260
    # invokes: Lcom/google/speech/tts/Decimal$Builder;->create()Lcom/google/speech/tts/Decimal$Builder;
    invoke-static {}, Lcom/google/speech/tts/Decimal$Builder;->access$100()Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 263
    invoke-static {}, Lcom/google/speech/tts/Decimal;->newBuilder()Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Decimal$Builder;->mergeFrom(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCodeSwitch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/speech/tts/Decimal;->codeSwitch_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getDefaultInstanceForType()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Decimal;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Decimal;->defaultInstance:Lcom/google/speech/tts/Decimal;

    return-object v0
.end method

.method public getExponent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/tts/Decimal;->exponent_:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldOrderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/speech/tts/Decimal;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method public getFractionalPart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/Decimal;->fractionalPart_:Ljava/lang/String;

    return-object v0
.end method

.method public getIntegerPart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/Decimal;->integerPart_:Ljava/lang/String;

    return-object v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/speech/tts/Decimal;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getNegative()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/speech/tts/Decimal;->negative_:Z

    return v0
.end method

.method public getPreserveOrder()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/speech/tts/Decimal;->preserveOrder_:Z

    return v0
.end method

.method public getQuantity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/tts/Decimal;->quantity_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 140
    iget v3, p0, Lcom/google/speech/tts/Decimal;->memoizedSerializedSize:I

    .line 141
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 190
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 143
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 144
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasNegative()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 145
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getNegative()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 148
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasIntegerPart()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 149
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getIntegerPart()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 152
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasFractionalPart()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 153
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getFractionalPart()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 156
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasQuantity()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 157
    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getQuantity()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 160
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasExponent()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 161
    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getExponent()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 164
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasStyle()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 165
    const/4 v5, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getStyle()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 168
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasMorphosyntacticFeatures()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 169
    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 172
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasPreserveOrder()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 173
    const/16 v5, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getPreserveOrder()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 176
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasCodeSwitch()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 177
    const/16 v5, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getCodeSwitch()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 181
    :cond_9
    const/4 v0, 0x0

    .line 182
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 183
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 185
    goto :goto_1

    .line 186
    .end local v1    # "element":Ljava/lang/String;
    :cond_a
    add-int/2addr v3, v0

    .line 187
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 189
    iput v3, p0, Lcom/google/speech/tts/Decimal;->memoizedSerializedSize:I

    move v4, v3

    .line 190
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getStyle()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/google/speech/tts/Decimal;->style_:I

    return v0
.end method

.method public hasCodeSwitch()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/speech/tts/Decimal;->hasCodeSwitch:Z

    return v0
.end method

.method public hasExponent()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/Decimal;->hasExponent:Z

    return v0
.end method

.method public hasFractionalPart()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/Decimal;->hasFractionalPart:Z

    return v0
.end method

.method public hasIntegerPart()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Decimal;->hasIntegerPart:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/Decimal;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasNegative()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Decimal;->hasNegative:Z

    return v0
.end method

.method public hasPreserveOrder()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/speech/tts/Decimal;->hasPreserveOrder:Z

    return v0
.end method

.method public hasQuantity()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/Decimal;->hasQuantity:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/Decimal;->hasStyle:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->toBuilder()Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Decimal$Builder;
    .locals 1

    .prologue
    .line 265
    invoke-static {p0}, Lcom/google/speech/tts/Decimal;->newBuilder(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getSerializedSize()I

    .line 106
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasNegative()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getNegative()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 109
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasIntegerPart()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 110
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getIntegerPart()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 112
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasFractionalPart()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 113
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getFractionalPart()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 115
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasQuantity()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 116
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getQuantity()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 118
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasExponent()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 119
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getExponent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 121
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 122
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getStyle()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 124
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 125
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 127
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasPreserveOrder()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 128
    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getPreserveOrder()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 130
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->hasCodeSwitch()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 131
    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getCodeSwitch()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 133
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/Decimal;->getFieldOrderList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 134
    .local v0, "element":Ljava/lang/String;
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 136
    .end local v0    # "element":Ljava/lang/String;
    :cond_9
    return-void
.end method
