.class public final enum Lcom/google/speech/tts/LexiconProto$Morphology$Gender;
.super Ljava/lang/Enum;
.source "LexiconProto.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto$Morphology;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Gender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/LexiconProto$Morphology$Gender;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

.field public static final enum FEM:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

.field public static final enum MAS:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

.field public static final enum NEU:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

.field public static final enum UNKNOWN_GENDER:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Morphology$Gender;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1446
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    const-string v1, "UNKNOWN_GENDER"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->UNKNOWN_GENDER:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    .line 1447
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    const-string v1, "NEU"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->NEU:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    .line 1448
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    const-string v1, "MAS"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->MAS:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    .line 1449
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    const-string v1, "FEM"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->FEM:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    .line 1444
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->UNKNOWN_GENDER:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->NEU:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->MAS:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->FEM:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->$VALUES:[Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    .line 1470
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender$1;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Morphology$Gender$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1479
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1480
    iput p3, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->index:I

    .line 1481
    iput p4, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->value:I

    .line 1482
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Morphology$Gender;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1444
    const-class v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/LexiconProto$Morphology$Gender;
    .locals 1

    .prologue
    .line 1444
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->$VALUES:[Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    invoke-virtual {v0}, [Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 1453
    iget v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Gender;->value:I

    return v0
.end method
