.class public final Lcom/google/speech/tts/VoiceMod$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "VoiceMod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/VoiceMod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/VoiceMod;",
        "Lcom/google/speech/tts/VoiceMod$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/VoiceMod;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/VoiceMod$Builder;
    .locals 1

    .prologue
    .line 203
    invoke-static {}, Lcom/google/speech/tts/VoiceMod$Builder;->create()Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/VoiceMod$Builder;
    .locals 3

    .prologue
    .line 212
    new-instance v0, Lcom/google/speech/tts/VoiceMod$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/VoiceMod$Builder;-><init>()V

    .line 213
    .local v0, "builder":Lcom/google/speech/tts/VoiceMod$Builder;
    new-instance v1, Lcom/google/speech/tts/VoiceMod;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/VoiceMod;-><init>(Lcom/google/speech/tts/VoiceMod$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    .line 214
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceMod$Builder;->build()Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/VoiceMod;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceMod$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    invoke-static {v0}, Lcom/google/speech/tts/VoiceMod$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 245
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceMod$Builder;->buildPartial()Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/VoiceMod;
    .locals 3

    .prologue
    .line 258
    iget-object v1, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    if-nez v1, :cond_0

    .line 259
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 262
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    # getter for: Lcom/google/speech/tts/VoiceMod;->f0Target_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/VoiceMod;->access$300(Lcom/google/speech/tts/VoiceMod;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 263
    iget-object v1, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    iget-object v2, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    # getter for: Lcom/google/speech/tts/VoiceMod;->f0Target_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/VoiceMod;->access$300(Lcom/google/speech/tts/VoiceMod;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/VoiceMod;->f0Target_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/VoiceMod;->access$302(Lcom/google/speech/tts/VoiceMod;Ljava/util/List;)Ljava/util/List;

    .line 266
    :cond_1
    iget-object v1, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    # getter for: Lcom/google/speech/tts/VoiceMod;->advanced_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/VoiceMod;->access$400(Lcom/google/speech/tts/VoiceMod;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 267
    iget-object v1, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    iget-object v2, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    # getter for: Lcom/google/speech/tts/VoiceMod;->advanced_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/VoiceMod;->access$400(Lcom/google/speech/tts/VoiceMod;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/VoiceMod;->advanced_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/VoiceMod;->access$402(Lcom/google/speech/tts/VoiceMod;Ljava/util/List;)Ljava/util/List;

    .line 270
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    .line 271
    .local v0, "returnMe":Lcom/google/speech/tts/VoiceMod;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    .line 272
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceMod$Builder;->clone()Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceMod$Builder;->clone()Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/VoiceMod$Builder;
    .locals 2

    .prologue
    .line 231
    invoke-static {}, Lcom/google/speech/tts/VoiceMod$Builder;->create()Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/VoiceMod$Builder;->mergeFrom(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/speech/tts/VoiceMod$Builder;->clone()Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    invoke-virtual {v0}, Lcom/google/speech/tts/VoiceMod;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 203
    check-cast p1, Lcom/google/speech/tts/VoiceMod;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/VoiceMod$Builder;->mergeFrom(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/VoiceMod;

    .prologue
    .line 276
    invoke-static {}, Lcom/google/speech/tts/VoiceMod;->getDefaultInstance()Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 298
    :cond_0
    :goto_0
    return-object p0

    .line 277
    :cond_1
    # getter for: Lcom/google/speech/tts/VoiceMod;->f0Target_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/VoiceMod;->access$300(Lcom/google/speech/tts/VoiceMod;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 278
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    # getter for: Lcom/google/speech/tts/VoiceMod;->f0Target_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/VoiceMod;->access$300(Lcom/google/speech/tts/VoiceMod;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 279
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/VoiceMod;->f0Target_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/VoiceMod;->access$302(Lcom/google/speech/tts/VoiceMod;Ljava/util/List;)Ljava/util/List;

    .line 281
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    # getter for: Lcom/google/speech/tts/VoiceMod;->f0Target_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/VoiceMod;->access$300(Lcom/google/speech/tts/VoiceMod;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/VoiceMod;->f0Target_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/VoiceMod;->access$300(Lcom/google/speech/tts/VoiceMod;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 283
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/VoiceMod;->hasF0Scale()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 284
    invoke-virtual {p1}, Lcom/google/speech/tts/VoiceMod;->getF0Scale()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/VoiceMod$Builder;->setF0Scale(F)Lcom/google/speech/tts/VoiceMod$Builder;

    .line 286
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/VoiceMod;->hasDurScale()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 287
    invoke-virtual {p1}, Lcom/google/speech/tts/VoiceMod;->getDurScale()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/VoiceMod$Builder;->setDurScale(F)Lcom/google/speech/tts/VoiceMod$Builder;

    .line 289
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/VoiceMod;->hasWordsPerMinute()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 290
    invoke-virtual {p1}, Lcom/google/speech/tts/VoiceMod;->getWordsPerMinute()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/VoiceMod$Builder;->setWordsPerMinute(I)Lcom/google/speech/tts/VoiceMod$Builder;

    .line 292
    :cond_6
    # getter for: Lcom/google/speech/tts/VoiceMod;->advanced_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/VoiceMod;->access$400(Lcom/google/speech/tts/VoiceMod;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    # getter for: Lcom/google/speech/tts/VoiceMod;->advanced_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/VoiceMod;->access$400(Lcom/google/speech/tts/VoiceMod;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 294
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/VoiceMod;->advanced_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/VoiceMod;->access$402(Lcom/google/speech/tts/VoiceMod;Ljava/util/List;)Ljava/util/List;

    .line 296
    :cond_7
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    # getter for: Lcom/google/speech/tts/VoiceMod;->advanced_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/VoiceMod;->access$400(Lcom/google/speech/tts/VoiceMod;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/VoiceMod;->advanced_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/VoiceMod;->access$400(Lcom/google/speech/tts/VoiceMod;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setDurScale(F)Lcom/google/speech/tts/VoiceMod$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/VoiceMod;->hasDurScale:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/VoiceMod;->access$702(Lcom/google/speech/tts/VoiceMod;Z)Z

    .line 423
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    # setter for: Lcom/google/speech/tts/VoiceMod;->durScale_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/VoiceMod;->access$802(Lcom/google/speech/tts/VoiceMod;F)F

    .line 424
    return-object p0
.end method

.method public setF0Scale(F)Lcom/google/speech/tts/VoiceMod$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/VoiceMod;->hasF0Scale:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/VoiceMod;->access$502(Lcom/google/speech/tts/VoiceMod;Z)Z

    .line 405
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    # setter for: Lcom/google/speech/tts/VoiceMod;->f0Scale_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/VoiceMod;->access$602(Lcom/google/speech/tts/VoiceMod;F)F

    .line 406
    return-object p0
.end method

.method public setWordsPerMinute(I)Lcom/google/speech/tts/VoiceMod$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/VoiceMod;->hasWordsPerMinute:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/VoiceMod;->access$902(Lcom/google/speech/tts/VoiceMod;Z)Z

    .line 441
    iget-object v0, p0, Lcom/google/speech/tts/VoiceMod$Builder;->result:Lcom/google/speech/tts/VoiceMod;

    # setter for: Lcom/google/speech/tts/VoiceMod;->wordsPerMinute_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/VoiceMod;->access$1002(Lcom/google/speech/tts/VoiceMod;I)I

    .line 442
    return-object p0
.end method
