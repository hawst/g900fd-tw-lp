.class public final Lcom/google/speech/tts/engine/TextnormParams$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "TextnormParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/TextnormParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/engine/TextnormParams;",
        "Lcom/google/speech/tts/engine/TextnormParams$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/engine/TextnormParams;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 617
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1000()Lcom/google/speech/tts/engine/TextnormParams$Builder;
    .locals 1

    .prologue
    .line 611
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->create()Lcom/google/speech/tts/engine/TextnormParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/engine/TextnormParams$Builder;
    .locals 3

    .prologue
    .line 620
    new-instance v0, Lcom/google/speech/tts/engine/TextnormParams$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/TextnormParams$Builder;-><init>()V

    .line 621
    .local v0, "builder":Lcom/google/speech/tts/engine/TextnormParams$Builder;
    new-instance v1, Lcom/google/speech/tts/engine/TextnormParams;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/engine/TextnormParams;-><init>(Lcom/google/speech/tts/engine/TextnormParams$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    .line 622
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 611
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->build()Lcom/google/speech/tts/engine/TextnormParams;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/engine/TextnormParams;
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 651
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 653
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->buildPartial()Lcom/google/speech/tts/engine/TextnormParams;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/engine/TextnormParams;
    .locals 3

    .prologue
    .line 666
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    if-nez v1, :cond_0

    .line 667
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 670
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1200(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 671
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    iget-object v2, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/engine/TextnormParams;->access$1200(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/engine/TextnormParams;->access$1202(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;

    .line 674
    :cond_1
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1300(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 675
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    iget-object v2, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/engine/TextnormParams;->access$1300(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/engine/TextnormParams;->access$1302(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;

    .line 678
    :cond_2
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1400(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 679
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    iget-object v2, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/engine/TextnormParams;->access$1400(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/engine/TextnormParams;->access$1402(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;

    .line 682
    :cond_3
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1500(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 683
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    iget-object v2, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/engine/TextnormParams;->access$1500(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/engine/TextnormParams;->access$1502(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;

    .line 686
    :cond_4
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1600(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_5

    .line 687
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    iget-object v2, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/engine/TextnormParams;->access$1600(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/engine/TextnormParams;->access$1602(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;

    .line 690
    :cond_5
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1700(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_6

    .line 691
    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    iget-object v2, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/engine/TextnormParams;->access$1700(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/engine/TextnormParams;->access$1702(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;

    .line 694
    :cond_6
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    .line 695
    .local v0, "returnMe":Lcom/google/speech/tts/engine/TextnormParams;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    .line 696
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 611
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->clone()Lcom/google/speech/tts/engine/TextnormParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 611
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->clone()Lcom/google/speech/tts/engine/TextnormParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/engine/TextnormParams$Builder;
    .locals 2

    .prologue
    .line 639
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->create()Lcom/google/speech/tts/engine/TextnormParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->mergeFrom(Lcom/google/speech/tts/engine/TextnormParams;)Lcom/google/speech/tts/engine/TextnormParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 611
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->clone()Lcom/google/speech/tts/engine/TextnormParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 647
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    invoke-virtual {v0}, Lcom/google/speech/tts/engine/TextnormParams;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 611
    check-cast p1, Lcom/google/speech/tts/engine/TextnormParams;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->mergeFrom(Lcom/google/speech/tts/engine/TextnormParams;)Lcom/google/speech/tts/engine/TextnormParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/engine/TextnormParams;)Lcom/google/speech/tts/engine/TextnormParams$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/engine/TextnormParams;

    .prologue
    .line 700
    invoke-static {}, Lcom/google/speech/tts/engine/TextnormParams;->getDefaultInstance()Lcom/google/speech/tts/engine/TextnormParams;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 740
    :cond_0
    :goto_0
    return-object p0

    .line 701
    :cond_1
    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1200(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 702
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams;->access$1200(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 703
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1202(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;

    .line 705
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams;->access$1200(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->numberFactorization_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1200(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 707
    :cond_3
    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1300(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 708
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams;->access$1300(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 709
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1302(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;

    .line 711
    :cond_4
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams;->access$1300(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->tokenizationRule_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1300(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 713
    :cond_5
    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1400(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 714
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams;->access$1400(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 715
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1402(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;

    .line 717
    :cond_6
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams;->access$1400(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->verbalizationRule_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1400(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 719
    :cond_7
    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1500(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 720
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams;->access$1500(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 721
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1502(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;

    .line 723
    :cond_8
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams;->access$1500(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->translitRule_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1500(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 725
    :cond_9
    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1600(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 726
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams;->access$1600(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 727
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1602(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;

    .line 729
    :cond_a
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams;->access$1600(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->pause_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1600(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 731
    :cond_b
    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1700(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 732
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams;->access$1700(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 733
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1702(Lcom/google/speech/tts/engine/TextnormParams;Ljava/util/List;)Ljava/util/List;

    .line 735
    :cond_c
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TextnormParams;->access$1700(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/engine/TextnormParams;->duplicatedMessageTypes_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1700(Lcom/google/speech/tts/engine/TextnormParams;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 737
    :cond_d
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TextnormParams;->hasParserSkipUnknownWordFraction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 738
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TextnormParams;->getParserSkipUnknownWordFraction()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/TextnormParams$Builder;->setParserSkipUnknownWordFraction(F)Lcom/google/speech/tts/engine/TextnormParams$Builder;

    goto/16 :goto_0
.end method

.method public setParserSkipUnknownWordFraction(F)Lcom/google/speech/tts/engine/TextnormParams$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 1091
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->hasParserSkipUnknownWordFraction:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1802(Lcom/google/speech/tts/engine/TextnormParams;Z)Z

    .line 1092
    iget-object v0, p0, Lcom/google/speech/tts/engine/TextnormParams$Builder;->result:Lcom/google/speech/tts/engine/TextnormParams;

    # setter for: Lcom/google/speech/tts/engine/TextnormParams;->parserSkipUnknownWordFraction_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/TextnormParams;->access$1902(Lcom/google/speech/tts/engine/TextnormParams;F)F

    .line 1093
    return-object p0
.end method
