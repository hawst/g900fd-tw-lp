.class public final Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionParamsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;",
        "Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 1

    .prologue
    .line 229
    invoke-static {}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->create()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 3

    .prologue
    .line 238
    new-instance v0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;-><init>()V

    .line 239
    .local v0, "builder":Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    new-instance v1, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;-><init>(Lcom/google/speech/tts/engine/SessionParamsProto$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    .line 240
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->build()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    invoke-static {v0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 271
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->buildPartial()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    .locals 3

    .prologue
    .line 284
    iget-object v1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    if-nez v1, :cond_0

    .line 285
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    .line 289
    .local v0, "returnMe":Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    .line 290
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->clone()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->clone()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 2

    .prologue
    .line 257
    invoke-static {}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->create()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->mergeFrom(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->clone()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    invoke-virtual {v0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 229
    check-cast p1, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->mergeFrom(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    .prologue
    .line 294
    invoke-static {}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getDefaultInstance()Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 316
    :cond_0
    :goto_0
    return-object p0

    .line 295
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasSessionId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 296
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getSessionId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->setSessionId(J)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    .line 298
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasBufferedMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 299
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getBufferedMode()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->setBufferedMode(Z)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    .line 301
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasAudioBufferSize()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 302
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getAudioBufferSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->setAudioBufferSize(I)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    .line 304
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasTimePointing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 305
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getTimePointing()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->setTimePointing(Z)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    .line 307
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasGenerateDiagnostics()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 308
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getGenerateDiagnostics()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->setGenerateDiagnostics(Z)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    .line 310
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasLogPerformance()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 311
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getLogPerformance()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->setLogPerformance(Z)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    .line 313
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasAllowAdvancedVoicemod()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->getAllowAdvancedVoicemod()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->setAllowAdvancedVoicemod(Z)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;

    goto :goto_0
.end method

.method public setAllowAdvancedVoicemod(Z)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasAllowAdvancedVoicemod:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$1502(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z

    .line 484
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->allowAdvancedVoicemod_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$1602(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z

    .line 485
    return-object p0
.end method

.method public setAudioBufferSize(I)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasAudioBufferSize:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$702(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z

    .line 412
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->audioBufferSize_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$802(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;I)I

    .line 413
    return-object p0
.end method

.method public setBufferedMode(Z)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasBufferedMode:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$502(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z

    .line 394
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->bufferedMode_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$602(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z

    .line 395
    return-object p0
.end method

.method public setGenerateDiagnostics(Z)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasGenerateDiagnostics:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$1102(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z

    .line 448
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->generateDiagnostics_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$1202(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z

    .line 449
    return-object p0
.end method

.method public setLogPerformance(Z)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasLogPerformance:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$1302(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z

    .line 466
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->logPerformance_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$1402(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z

    .line 467
    return-object p0
.end method

.method public setSessionId(J)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasSessionId:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$302(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z

    .line 376
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->sessionId_:J
    invoke-static {v0, p1, p2}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$402(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;J)J

    .line 377
    return-object p0
.end method

.method public setTimePointing(Z)Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->hasTimePointing:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$902(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z

    .line 430
    iget-object v0, p0, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams$Builder;->result:Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;

    # setter for: Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->timePointing_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;->access$1002(Lcom/google/speech/tts/engine/SessionParamsProto$SessionParams;Z)Z

    .line 431
    return-object p0
.end method
