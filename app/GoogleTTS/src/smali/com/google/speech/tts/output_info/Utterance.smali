.class public final Lcom/google/speech/tts/output_info/Utterance;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Utterance.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/output_info/Utterance$1;,
        Lcom/google/speech/tts/output_info/Utterance$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/output_info/Utterance;


# instance fields
.field private memoizedSerializedSize:I

.field private token_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/output_info/Token;",
            ">;"
        }
    .end annotation
.end field

.field private word_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/output_info/Word;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 385
    new-instance v0, Lcom/google/speech/tts/output_info/Utterance;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/output_info/Utterance;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/output_info/Utterance;->defaultInstance:Lcom/google/speech/tts/output_info/Utterance;

    .line 386
    invoke-static {}, Lcom/google/speech/tts/output_info/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 387
    sget-object v0, Lcom/google/speech/tts/output_info/Utterance;->defaultInstance:Lcom/google/speech/tts/output_info/Utterance;

    invoke-direct {v0}, Lcom/google/speech/tts/output_info/Utterance;->initFields()V

    .line 388
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/output_info/Utterance;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/output_info/Utterance;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/output_info/Utterance$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/output_info/Utterance$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/output_info/Utterance;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/output_info/Utterance;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Utterance;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/output_info/Utterance;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Utterance;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/speech/tts/output_info/Utterance;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Utterance;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/speech/tts/output_info/Utterance;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Utterance;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/output_info/Utterance;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/output_info/Utterance;->defaultInstance:Lcom/google/speech/tts/output_info/Utterance;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 47
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/output_info/Utterance$Builder;
    .locals 1

    .prologue
    .line 151
    # invokes: Lcom/google/speech/tts/output_info/Utterance$Builder;->create()Lcom/google/speech/tts/output_info/Utterance$Builder;
    invoke-static {}, Lcom/google/speech/tts/output_info/Utterance$Builder;->access$100()Lcom/google/speech/tts/output_info/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/output_info/Utterance;)Lcom/google/speech/tts/output_info/Utterance$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/output_info/Utterance;

    .prologue
    .line 154
    invoke-static {}, Lcom/google/speech/tts/output_info/Utterance;->newBuilder()Lcom/google/speech/tts/output_info/Utterance$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/output_info/Utterance$Builder;->mergeFrom(Lcom/google/speech/tts/output_info/Utterance;)Lcom/google/speech/tts/output_info/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance;->getDefaultInstanceForType()Lcom/google/speech/tts/output_info/Utterance;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/output_info/Utterance;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/output_info/Utterance;->defaultInstance:Lcom/google/speech/tts/output_info/Utterance;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 68
    iget v2, p0, Lcom/google/speech/tts/output_info/Utterance;->memoizedSerializedSize:I

    .line 69
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 81
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 71
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance;->getTokenList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/output_info/Token;

    .line 73
    .local v0, "element":Lcom/google/speech/tts/output_info/Token;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 75
    goto :goto_1

    .line 76
    .end local v0    # "element":Lcom/google/speech/tts/output_info/Token;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance;->getWordList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/output_info/Word;

    .line 77
    .local v0, "element":Lcom/google/speech/tts/output_info/Word;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 79
    goto :goto_2

    .line 80
    .end local v0    # "element":Lcom/google/speech/tts/output_info/Word;
    :cond_2
    iput v2, p0, Lcom/google/speech/tts/output_info/Utterance;->memoizedSerializedSize:I

    move v3, v2

    .line 81
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getTokenList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/output_info/Token;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance;->token_:Ljava/util/List;

    return-object v0
.end method

.method public getWordList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/output_info/Word;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Utterance;->word_:Ljava/util/List;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance;->getWordList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/output_info/Word;

    .line 50
    .local v0, "element":Lcom/google/speech/tts/output_info/Word;
    invoke-virtual {v0}, Lcom/google/speech/tts/output_info/Word;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 52
    .end local v0    # "element":Lcom/google/speech/tts/output_info/Word;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance;->toBuilder()Lcom/google/speech/tts/output_info/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/output_info/Utterance$Builder;
    .locals 1

    .prologue
    .line 156
    invoke-static {p0}, Lcom/google/speech/tts/output_info/Utterance;->newBuilder(Lcom/google/speech/tts/output_info/Utterance;)Lcom/google/speech/tts/output_info/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance;->getSerializedSize()I

    .line 58
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance;->getTokenList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/output_info/Token;

    .line 59
    .local v0, "element":Lcom/google/speech/tts/output_info/Token;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 61
    .end local v0    # "element":Lcom/google/speech/tts/output_info/Token;
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Utterance;->getWordList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/output_info/Word;

    .line 62
    .local v0, "element":Lcom/google/speech/tts/output_info/Word;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 64
    .end local v0    # "element":Lcom/google/speech/tts/output_info/Word;
    :cond_1
    return-void
.end method
