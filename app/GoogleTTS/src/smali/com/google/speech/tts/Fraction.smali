.class public final Lcom/google/speech/tts/Fraction;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Fraction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Fraction$1;,
        Lcom/google/speech/tts/Fraction$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Fraction;


# instance fields
.field private codeSwitch_:Ljava/lang/String;

.field private denominator_:Ljava/lang/String;

.field private fieldOrder_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasCodeSwitch:Z

.field private hasDenominator:Z

.field private hasIntegerPart:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasNegative:Z

.field private hasNumerator:Z

.field private hasPreserveOrder:Z

.field private hasStyle:Z

.field private integerPart_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private negative_:Z

.field private numerator_:Ljava/lang/String;

.field private preserveOrder_:Z

.field private style_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 617
    new-instance v0, Lcom/google/speech/tts/Fraction;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Fraction;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Fraction;->defaultInstance:Lcom/google/speech/tts/Fraction;

    .line 618
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 619
    sget-object v0, Lcom/google/speech/tts/Fraction;->defaultInstance:Lcom/google/speech/tts/Fraction;

    invoke-direct {v0}, Lcom/google/speech/tts/Fraction;->initFields()V

    .line 620
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Fraction;->integerPart_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Fraction;->numerator_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Fraction;->denominator_:Ljava/lang/String;

    .line 46
    iput v1, p0, Lcom/google/speech/tts/Fraction;->style_:I

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Fraction;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 60
    iput-boolean v1, p0, Lcom/google/speech/tts/Fraction;->preserveOrder_:Z

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Fraction;->codeSwitch_:Ljava/lang/String;

    .line 74
    iput-boolean v1, p0, Lcom/google/speech/tts/Fraction;->negative_:Z

    .line 80
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;

    .line 130
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Fraction;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Fraction;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Fraction$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Fraction$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Fraction;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Fraction;->integerPart_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Fraction;->numerator_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Fraction;->denominator_:Ljava/lang/String;

    .line 46
    iput v1, p0, Lcom/google/speech/tts/Fraction;->style_:I

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Fraction;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 60
    iput-boolean v1, p0, Lcom/google/speech/tts/Fraction;->preserveOrder_:Z

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Fraction;->codeSwitch_:Ljava/lang/String;

    .line 74
    iput-boolean v1, p0, Lcom/google/speech/tts/Fraction;->negative_:Z

    .line 80
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;

    .line 130
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Fraction;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Fraction;->hasStyle:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/Fraction;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Fraction;->style_:I

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Fraction;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/Fraction;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Fraction;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Fraction;->hasPreserveOrder:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Fraction;->preserveOrder_:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Fraction;->hasCodeSwitch:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/Fraction;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Fraction;->codeSwitch_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/google/speech/tts/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Fraction;->hasNegative:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/google/speech/tts/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Fraction;->negative_:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Fraction;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Fraction;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Fraction;->hasIntegerPart:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Fraction;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Fraction;->integerPart_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Fraction;->hasNumerator:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Fraction;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Fraction;->numerator_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Fraction;->hasDenominator:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Fraction;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Fraction;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Fraction;->denominator_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Fraction;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Fraction;->defaultInstance:Lcom/google/speech/tts/Fraction;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Fraction$Builder;
    .locals 1

    .prologue
    .line 248
    # invokes: Lcom/google/speech/tts/Fraction$Builder;->create()Lcom/google/speech/tts/Fraction$Builder;
    invoke-static {}, Lcom/google/speech/tts/Fraction$Builder;->access$100()Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Fraction;

    .prologue
    .line 251
    invoke-static {}, Lcom/google/speech/tts/Fraction;->newBuilder()Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Fraction$Builder;->mergeFrom(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCodeSwitch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/speech/tts/Fraction;->codeSwitch_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getDefaultInstanceForType()Lcom/google/speech/tts/Fraction;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Fraction;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Fraction;->defaultInstance:Lcom/google/speech/tts/Fraction;

    return-object v0
.end method

.method public getDenominator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/Fraction;->denominator_:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldOrderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/speech/tts/Fraction;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method public getIntegerPart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Fraction;->integerPart_:Ljava/lang/String;

    return-object v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/tts/Fraction;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getNegative()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/speech/tts/Fraction;->negative_:Z

    return v0
.end method

.method public getNumerator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/Fraction;->numerator_:Ljava/lang/String;

    return-object v0
.end method

.method public getPreserveOrder()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/speech/tts/Fraction;->preserveOrder_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 132
    iget v3, p0, Lcom/google/speech/tts/Fraction;->memoizedSerializedSize:I

    .line 133
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 178
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 135
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 136
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasIntegerPart()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 137
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getIntegerPart()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 140
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasNumerator()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 141
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getNumerator()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 144
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasDenominator()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 145
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getDenominator()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 148
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasStyle()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 149
    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getStyle()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 152
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasMorphosyntacticFeatures()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 153
    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 156
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasPreserveOrder()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 157
    const/4 v5, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getPreserveOrder()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 160
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasCodeSwitch()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 161
    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getCodeSwitch()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 164
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasNegative()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 165
    const/16 v5, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getNegative()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 169
    :cond_8
    const/4 v0, 0x0

    .line 170
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 171
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 173
    goto :goto_1

    .line 174
    .end local v1    # "element":Ljava/lang/String;
    :cond_9
    add-int/2addr v3, v0

    .line 175
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 177
    iput v3, p0, Lcom/google/speech/tts/Fraction;->memoizedSerializedSize:I

    move v4, v3

    .line 178
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getStyle()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/speech/tts/Fraction;->style_:I

    return v0
.end method

.method public hasCodeSwitch()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/Fraction;->hasCodeSwitch:Z

    return v0
.end method

.method public hasDenominator()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/Fraction;->hasDenominator:Z

    return v0
.end method

.method public hasIntegerPart()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Fraction;->hasIntegerPart:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/Fraction;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasNegative()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/speech/tts/Fraction;->hasNegative:Z

    return v0
.end method

.method public hasNumerator()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Fraction;->hasNumerator:Z

    return v0
.end method

.method public hasPreserveOrder()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/Fraction;->hasPreserveOrder:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/Fraction;->hasStyle:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 93
    iget-boolean v1, p0, Lcom/google/speech/tts/Fraction;->hasNumerator:Z

    if-nez v1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 94
    :cond_1
    iget-boolean v1, p0, Lcom/google/speech/tts/Fraction;->hasDenominator:Z

    if-eqz v1, :cond_0

    .line 95
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->toBuilder()Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Fraction$Builder;
    .locals 1

    .prologue
    .line 253
    invoke-static {p0}, Lcom/google/speech/tts/Fraction;->newBuilder(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getSerializedSize()I

    .line 101
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasIntegerPart()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getIntegerPart()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 104
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasNumerator()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 105
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getNumerator()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 107
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasDenominator()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 108
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getDenominator()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 110
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 111
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getStyle()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 113
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 114
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 116
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasPreserveOrder()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 117
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getPreserveOrder()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 119
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasCodeSwitch()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 120
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getCodeSwitch()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 122
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->hasNegative()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 123
    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getNegative()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 125
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Fraction;->getFieldOrderList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 126
    .local v0, "element":Ljava/lang/String;
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 128
    .end local v0    # "element":Ljava/lang/String;
    :cond_8
    return-void
.end method
