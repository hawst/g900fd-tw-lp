.class public final Lcom/google/speech/tts/Ordinal$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Ordinal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Ordinal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Ordinal;",
        "Lcom/google/speech/tts/Ordinal$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Ordinal;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Ordinal$Builder;
    .locals 1

    .prologue
    .line 198
    invoke-static {}, Lcom/google/speech/tts/Ordinal$Builder;->create()Lcom/google/speech/tts/Ordinal$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Ordinal$Builder;
    .locals 3

    .prologue
    .line 207
    new-instance v0, Lcom/google/speech/tts/Ordinal$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Ordinal$Builder;-><init>()V

    .line 208
    .local v0, "builder":Lcom/google/speech/tts/Ordinal$Builder;
    new-instance v1, Lcom/google/speech/tts/Ordinal;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Ordinal;-><init>(Lcom/google/speech/tts/Ordinal$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    .line 209
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/speech/tts/Ordinal$Builder;->build()Lcom/google/speech/tts/Ordinal;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Ordinal;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Ordinal$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    invoke-static {v0}, Lcom/google/speech/tts/Ordinal$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 240
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Ordinal$Builder;->buildPartial()Lcom/google/speech/tts/Ordinal;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Ordinal;
    .locals 3

    .prologue
    .line 253
    iget-object v1, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    if-nez v1, :cond_0

    .line 254
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 257
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    # getter for: Lcom/google/speech/tts/Ordinal;->fieldOrder_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Ordinal;->access$300(Lcom/google/speech/tts/Ordinal;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 258
    iget-object v1, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    iget-object v2, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    # getter for: Lcom/google/speech/tts/Ordinal;->fieldOrder_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Ordinal;->access$300(Lcom/google/speech/tts/Ordinal;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Ordinal;->fieldOrder_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Ordinal;->access$302(Lcom/google/speech/tts/Ordinal;Ljava/util/List;)Ljava/util/List;

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    .line 262
    .local v0, "returnMe":Lcom/google/speech/tts/Ordinal;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    .line 263
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/speech/tts/Ordinal$Builder;->clone()Lcom/google/speech/tts/Ordinal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/speech/tts/Ordinal$Builder;->clone()Lcom/google/speech/tts/Ordinal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Ordinal$Builder;
    .locals 2

    .prologue
    .line 226
    invoke-static {}, Lcom/google/speech/tts/Ordinal$Builder;->create()Lcom/google/speech/tts/Ordinal$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Ordinal$Builder;->mergeFrom(Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/speech/tts/Ordinal$Builder;->clone()Lcom/google/speech/tts/Ordinal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    invoke-virtual {v0}, Lcom/google/speech/tts/Ordinal;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 198
    check-cast p1, Lcom/google/speech/tts/Ordinal;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Ordinal$Builder;->mergeFrom(Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Ordinal;

    .prologue
    .line 267
    invoke-static {}, Lcom/google/speech/tts/Ordinal;->getDefaultInstance()Lcom/google/speech/tts/Ordinal;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 286
    :cond_0
    :goto_0
    return-object p0

    .line 268
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Ordinal;->hasInteger()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 269
    invoke-virtual {p1}, Lcom/google/speech/tts/Ordinal;->getInteger()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Ordinal$Builder;->setInteger(Ljava/lang/String;)Lcom/google/speech/tts/Ordinal$Builder;

    .line 271
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Ordinal;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 272
    invoke-virtual {p1}, Lcom/google/speech/tts/Ordinal;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Ordinal$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Ordinal$Builder;

    .line 274
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Ordinal;->hasPreserveOrder()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 275
    invoke-virtual {p1}, Lcom/google/speech/tts/Ordinal;->getPreserveOrder()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Ordinal$Builder;->setPreserveOrder(Z)Lcom/google/speech/tts/Ordinal$Builder;

    .line 277
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Ordinal;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 278
    invoke-virtual {p1}, Lcom/google/speech/tts/Ordinal;->getCodeSwitch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Ordinal$Builder;->setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Ordinal$Builder;

    .line 280
    :cond_5
    # getter for: Lcom/google/speech/tts/Ordinal;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Ordinal;->access$300(Lcom/google/speech/tts/Ordinal;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    # getter for: Lcom/google/speech/tts/Ordinal;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Ordinal;->access$300(Lcom/google/speech/tts/Ordinal;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 282
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Ordinal;->fieldOrder_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Ordinal;->access$302(Lcom/google/speech/tts/Ordinal;Ljava/util/List;)Ljava/util/List;

    .line 284
    :cond_6
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    # getter for: Lcom/google/speech/tts/Ordinal;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Ordinal;->access$300(Lcom/google/speech/tts/Ordinal;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Ordinal;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Ordinal;->access$300(Lcom/google/speech/tts/Ordinal;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Ordinal$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 397
    if-nez p1, :cond_0

    .line 398
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Ordinal;->hasCodeSwitch:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Ordinal;->access$1002(Lcom/google/speech/tts/Ordinal;Z)Z

    .line 401
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    # setter for: Lcom/google/speech/tts/Ordinal;->codeSwitch_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Ordinal;->access$1102(Lcom/google/speech/tts/Ordinal;Ljava/lang/String;)Ljava/lang/String;

    .line 402
    return-object p0
.end method

.method public setInteger(Ljava/lang/String;)Lcom/google/speech/tts/Ordinal$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 337
    if-nez p1, :cond_0

    .line 338
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Ordinal;->hasInteger:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Ordinal;->access$402(Lcom/google/speech/tts/Ordinal;Z)Z

    .line 341
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    # setter for: Lcom/google/speech/tts/Ordinal;->integer_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Ordinal;->access$502(Lcom/google/speech/tts/Ordinal;Ljava/lang/String;)Ljava/lang/String;

    .line 342
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Ordinal$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 358
    if-nez p1, :cond_0

    .line 359
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Ordinal;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Ordinal;->access$602(Lcom/google/speech/tts/Ordinal;Z)Z

    .line 362
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    # setter for: Lcom/google/speech/tts/Ordinal;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Ordinal;->access$702(Lcom/google/speech/tts/Ordinal;Ljava/lang/String;)Ljava/lang/String;

    .line 363
    return-object p0
.end method

.method public setPreserveOrder(Z)Lcom/google/speech/tts/Ordinal$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Ordinal;->hasPreserveOrder:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Ordinal;->access$802(Lcom/google/speech/tts/Ordinal;Z)Z

    .line 380
    iget-object v0, p0, Lcom/google/speech/tts/Ordinal$Builder;->result:Lcom/google/speech/tts/Ordinal;

    # setter for: Lcom/google/speech/tts/Ordinal;->preserveOrder_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Ordinal;->access$902(Lcom/google/speech/tts/Ordinal;Z)Z

    .line 381
    return-object p0
.end method
