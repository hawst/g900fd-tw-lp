.class public final Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;",
        "Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 760
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1500()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;
    .locals 1

    .prologue
    .line 754
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->create()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;
    .locals 3

    .prologue
    .line 763
    new-instance v0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;-><init>()V

    .line 764
    .local v0, "builder":Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;
    new-instance v1, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;-><init>(Lcom/google/speech/tts/LexiconProto$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    .line 765
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 754
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->build()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;
    .locals 1

    .prologue
    .line 793
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 794
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 796
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->buildPartial()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;
    .locals 3

    .prologue
    .line 809
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    if-nez v1, :cond_0

    .line 810
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 813
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    # getter for: Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->access$1700(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 814
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    iget-object v2, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    # getter for: Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->access$1700(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->access$1702(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;Ljava/util/List;)Ljava/util/List;

    .line 817
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    .line 818
    .local v0, "returnMe":Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    .line 819
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 754
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->clone()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 754
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->clone()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;
    .locals 2

    .prologue
    .line 782
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->create()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 754
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->clone()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 754
    check-cast p1, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    .prologue
    .line 823
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->getDefaultInstance()Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 830
    :cond_0
    :goto_0
    return-object p0

    .line 824
    :cond_1
    # getter for: Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->access$1700(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 825
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    # getter for: Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->access$1700(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 826
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->access$1702(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;Ljava/util/List;)Ljava/util/List;

    .line 828
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation$Builder;->result:Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;

    # getter for: Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->access$1700(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->syllable_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;->access$1700(Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
