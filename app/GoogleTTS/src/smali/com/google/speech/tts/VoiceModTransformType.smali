.class public final enum Lcom/google/speech/tts/VoiceModTransformType;
.super Ljava/lang/Enum;
.source "VoiceModTransformType.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/VoiceModTransformType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/VoiceModTransformType;

.field public static final enum MEAN:Lcom/google/speech/tts/VoiceModTransformType;

.field public static final enum OPERATOR:Lcom/google/speech/tts/VoiceModTransformType;

.field public static final enum PIECEWISE_LINEAR:Lcom/google/speech/tts/VoiceModTransformType;

.field public static final enum RANDOM_OPERATOR:Lcom/google/speech/tts/VoiceModTransformType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/VoiceModTransformType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lcom/google/speech/tts/VoiceModTransformType;

    const-string v1, "OPERATOR"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/speech/tts/VoiceModTransformType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModTransformType;->OPERATOR:Lcom/google/speech/tts/VoiceModTransformType;

    .line 8
    new-instance v0, Lcom/google/speech/tts/VoiceModTransformType;

    const-string v1, "RANDOM_OPERATOR"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/speech/tts/VoiceModTransformType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModTransformType;->RANDOM_OPERATOR:Lcom/google/speech/tts/VoiceModTransformType;

    .line 9
    new-instance v0, Lcom/google/speech/tts/VoiceModTransformType;

    const-string v1, "MEAN"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/google/speech/tts/VoiceModTransformType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModTransformType;->MEAN:Lcom/google/speech/tts/VoiceModTransformType;

    .line 10
    new-instance v0, Lcom/google/speech/tts/VoiceModTransformType;

    const-string v1, "PIECEWISE_LINEAR"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/speech/tts/VoiceModTransformType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModTransformType;->PIECEWISE_LINEAR:Lcom/google/speech/tts/VoiceModTransformType;

    .line 5
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/speech/tts/VoiceModTransformType;

    sget-object v1, Lcom/google/speech/tts/VoiceModTransformType;->OPERATOR:Lcom/google/speech/tts/VoiceModTransformType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/speech/tts/VoiceModTransformType;->RANDOM_OPERATOR:Lcom/google/speech/tts/VoiceModTransformType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/speech/tts/VoiceModTransformType;->MEAN:Lcom/google/speech/tts/VoiceModTransformType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/speech/tts/VoiceModTransformType;->PIECEWISE_LINEAR:Lcom/google/speech/tts/VoiceModTransformType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/speech/tts/VoiceModTransformType;->$VALUES:[Lcom/google/speech/tts/VoiceModTransformType;

    .line 31
    new-instance v0, Lcom/google/speech/tts/VoiceModTransformType$1;

    invoke-direct {v0}, Lcom/google/speech/tts/VoiceModTransformType$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/VoiceModTransformType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/google/speech/tts/VoiceModTransformType;->index:I

    .line 42
    iput p4, p0, Lcom/google/speech/tts/VoiceModTransformType;->value:I

    .line 43
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/VoiceModTransformType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/google/speech/tts/VoiceModTransformType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/VoiceModTransformType;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/VoiceModTransformType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/google/speech/tts/VoiceModTransformType;->$VALUES:[Lcom/google/speech/tts/VoiceModTransformType;

    invoke-virtual {v0}, [Lcom/google/speech/tts/VoiceModTransformType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/VoiceModTransformType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/google/speech/tts/VoiceModTransformType;->value:I

    return v0
.end method
