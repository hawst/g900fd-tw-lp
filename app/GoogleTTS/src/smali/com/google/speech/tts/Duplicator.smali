.class public final Lcom/google/speech/tts/Duplicator;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Duplicator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Duplicator$1;,
        Lcom/google/speech/tts/Duplicator$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Duplicator;


# instance fields
.field private date_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Date;",
            ">;"
        }
    .end annotation
.end field

.field private decimal_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Decimal;",
            ">;"
        }
    .end annotation
.end field

.field private fraction_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Fraction;",
            ">;"
        }
    .end annotation
.end field

.field private measure_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Measure;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private money_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Money;",
            ">;"
        }
    .end annotation
.end field

.field private ordinal_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Ordinal;",
            ">;"
        }
    .end annotation
.end field

.field private time_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Time;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 824
    new-instance v0, Lcom/google/speech/tts/Duplicator;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Duplicator;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Duplicator;->defaultInstance:Lcom/google/speech/tts/Duplicator;

    .line 825
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 826
    sget-object v0, Lcom/google/speech/tts/Duplicator;->defaultInstance:Lcom/google/speech/tts/Duplicator;

    invoke-direct {v0}, Lcom/google/speech/tts/Duplicator;->initFields()V

    .line 827
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;

    .line 72
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;

    .line 84
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;

    .line 96
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;

    .line 150
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Duplicator;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Duplicator;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Duplicator$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Duplicator$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Duplicator;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;

    .line 72
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;

    .line 84
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;

    .line 96
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;

    .line 150
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Duplicator;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Duplicator;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Duplicator;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Duplicator;->defaultInstance:Lcom/google/speech/tts/Duplicator;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 107
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Duplicator$Builder;
    .locals 1

    .prologue
    .line 255
    # invokes: Lcom/google/speech/tts/Duplicator$Builder;->create()Lcom/google/speech/tts/Duplicator$Builder;
    invoke-static {}, Lcom/google/speech/tts/Duplicator$Builder;->access$100()Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Duplicator;)Lcom/google/speech/tts/Duplicator$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Duplicator;

    .prologue
    .line 258
    invoke-static {}, Lcom/google/speech/tts/Duplicator;->newBuilder()Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Duplicator$Builder;->mergeFrom(Lcom/google/speech/tts/Duplicator;)Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDateList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Date;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;

    return-object v0
.end method

.method public getDecimalList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Decimal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getDefaultInstanceForType()Lcom/google/speech/tts/Duplicator;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Duplicator;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Duplicator;->defaultInstance:Lcom/google/speech/tts/Duplicator;

    return-object v0
.end method

.method public getFractionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Fraction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;

    return-object v0
.end method

.method public getMeasureList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Measure;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;

    return-object v0
.end method

.method public getMoneyList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Money;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;

    return-object v0
.end method

.method public getOrdinalList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Ordinal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 152
    iget v2, p0, Lcom/google/speech/tts/Duplicator;->memoizedSerializedSize:I

    .line 153
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 185
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 155
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 156
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getDateList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Date;

    .line 157
    .local v0, "element":Lcom/google/speech/tts/Date;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 159
    goto :goto_1

    .line 160
    .end local v0    # "element":Lcom/google/speech/tts/Date;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getDecimalList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Decimal;

    .line 161
    .local v0, "element":Lcom/google/speech/tts/Decimal;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 163
    goto :goto_2

    .line 164
    .end local v0    # "element":Lcom/google/speech/tts/Decimal;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getFractionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Fraction;

    .line 165
    .local v0, "element":Lcom/google/speech/tts/Fraction;
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 167
    goto :goto_3

    .line 168
    .end local v0    # "element":Lcom/google/speech/tts/Fraction;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getMeasureList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Measure;

    .line 169
    .local v0, "element":Lcom/google/speech/tts/Measure;
    const/4 v4, 0x4

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 171
    goto :goto_4

    .line 172
    .end local v0    # "element":Lcom/google/speech/tts/Measure;
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getMoneyList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Money;

    .line 173
    .local v0, "element":Lcom/google/speech/tts/Money;
    const/4 v4, 0x5

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 175
    goto :goto_5

    .line 176
    .end local v0    # "element":Lcom/google/speech/tts/Money;
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getOrdinalList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Ordinal;

    .line 177
    .local v0, "element":Lcom/google/speech/tts/Ordinal;
    const/4 v4, 0x6

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 179
    goto :goto_6

    .line 180
    .end local v0    # "element":Lcom/google/speech/tts/Ordinal;
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getTimeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Time;

    .line 181
    .local v0, "element":Lcom/google/speech/tts/Time;
    const/4 v4, 0x7

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 183
    goto :goto_7

    .line 184
    .end local v0    # "element":Lcom/google/speech/tts/Time;
    :cond_7
    iput v2, p0, Lcom/google/speech/tts/Duplicator;->memoizedSerializedSize:I

    move v3, v2

    .line 185
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto/16 :goto_0
.end method

.method public getTimeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Time;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 109
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getFractionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Fraction;

    .line 110
    .local v0, "element":Lcom/google/speech/tts/Fraction;
    invoke-virtual {v0}, Lcom/google/speech/tts/Fraction;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 121
    .end local v0    # "element":Lcom/google/speech/tts/Fraction;
    :goto_0
    return v2

    .line 112
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getMeasureList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Measure;

    .line 113
    .local v0, "element":Lcom/google/speech/tts/Measure;
    invoke-virtual {v0}, Lcom/google/speech/tts/Measure;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 115
    .end local v0    # "element":Lcom/google/speech/tts/Measure;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getMoneyList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Money;

    .line 116
    .local v0, "element":Lcom/google/speech/tts/Money;
    invoke-virtual {v0}, Lcom/google/speech/tts/Money;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_4

    goto :goto_0

    .line 118
    .end local v0    # "element":Lcom/google/speech/tts/Money;
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getOrdinalList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Ordinal;

    .line 119
    .local v0, "element":Lcom/google/speech/tts/Ordinal;
    invoke-virtual {v0}, Lcom/google/speech/tts/Ordinal;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_6

    goto :goto_0

    .line 121
    .end local v0    # "element":Lcom/google/speech/tts/Ordinal;
    :cond_7
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->toBuilder()Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Duplicator$Builder;
    .locals 1

    .prologue
    .line 260
    invoke-static {p0}, Lcom/google/speech/tts/Duplicator;->newBuilder(Lcom/google/speech/tts/Duplicator;)Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getSerializedSize()I

    .line 127
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getDateList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Date;

    .line 128
    .local v0, "element":Lcom/google/speech/tts/Date;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 130
    .end local v0    # "element":Lcom/google/speech/tts/Date;
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getDecimalList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Decimal;

    .line 131
    .local v0, "element":Lcom/google/speech/tts/Decimal;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 133
    .end local v0    # "element":Lcom/google/speech/tts/Decimal;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getFractionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Fraction;

    .line 134
    .local v0, "element":Lcom/google/speech/tts/Fraction;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    .line 136
    .end local v0    # "element":Lcom/google/speech/tts/Fraction;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getMeasureList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Measure;

    .line 137
    .local v0, "element":Lcom/google/speech/tts/Measure;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    .line 139
    .end local v0    # "element":Lcom/google/speech/tts/Measure;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getMoneyList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Money;

    .line 140
    .local v0, "element":Lcom/google/speech/tts/Money;
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_4

    .line 142
    .end local v0    # "element":Lcom/google/speech/tts/Money;
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getOrdinalList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Ordinal;

    .line 143
    .local v0, "element":Lcom/google/speech/tts/Ordinal;
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_5

    .line 145
    .end local v0    # "element":Lcom/google/speech/tts/Ordinal;
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator;->getTimeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Time;

    .line 146
    .local v0, "element":Lcom/google/speech/tts/Time;
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_6

    .line 148
    .end local v0    # "element":Lcom/google/speech/tts/Time;
    :cond_6
    return-void
.end method
