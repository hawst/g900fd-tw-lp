.class public final Lcom/google/speech/tts/Style$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Style.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Style;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Style;",
        "Lcom/google/speech/tts/Style$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Style;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Style$Builder;
    .locals 1

    .prologue
    .line 201
    invoke-static {}, Lcom/google/speech/tts/Style$Builder;->create()Lcom/google/speech/tts/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Style$Builder;
    .locals 3

    .prologue
    .line 210
    new-instance v0, Lcom/google/speech/tts/Style$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Style$Builder;-><init>()V

    .line 211
    .local v0, "builder":Lcom/google/speech/tts/Style$Builder;
    new-instance v1, Lcom/google/speech/tts/Style;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Style;-><init>(Lcom/google/speech/tts/Style$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    .line 212
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/speech/tts/Style$Builder;->build()Lcom/google/speech/tts/Style;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Style;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Style$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    invoke-static {v0}, Lcom/google/speech/tts/Style$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 243
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Style$Builder;->buildPartial()Lcom/google/speech/tts/Style;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Style;
    .locals 3

    .prologue
    .line 256
    iget-object v1, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    if-nez v1, :cond_0

    .line 257
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    .line 261
    .local v0, "returnMe":Lcom/google/speech/tts/Style;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    .line 262
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/speech/tts/Style$Builder;->clone()Lcom/google/speech/tts/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/speech/tts/Style$Builder;->clone()Lcom/google/speech/tts/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Style$Builder;
    .locals 2

    .prologue
    .line 229
    invoke-static {}, Lcom/google/speech/tts/Style$Builder;->create()Lcom/google/speech/tts/Style$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Style$Builder;->mergeFrom(Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/speech/tts/Style$Builder;->clone()Lcom/google/speech/tts/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    invoke-virtual {v0}, Lcom/google/speech/tts/Style;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 201
    check-cast p1, Lcom/google/speech/tts/Style;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Style$Builder;->mergeFrom(Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Style$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/Style;

    .prologue
    .line 266
    invoke-static {}, Lcom/google/speech/tts/Style;->getDefaultInstance()Lcom/google/speech/tts/Style;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 285
    :cond_0
    :goto_0
    return-object p0

    .line 267
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Style;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 268
    invoke-virtual {p1}, Lcom/google/speech/tts/Style;->getFraction()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Style$Builder;->setFraction(I)Lcom/google/speech/tts/Style$Builder;

    .line 270
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Style;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 271
    invoke-virtual {p1}, Lcom/google/speech/tts/Style;->getTime()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Style$Builder;->setTime(I)Lcom/google/speech/tts/Style$Builder;

    .line 273
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Style;->hasMoney()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 274
    invoke-virtual {p1}, Lcom/google/speech/tts/Style;->getMoney()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Style$Builder;->setMoney(I)Lcom/google/speech/tts/Style$Builder;

    .line 276
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Style;->hasMeasure()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 277
    invoke-virtual {p1}, Lcom/google/speech/tts/Style;->getMeasure()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Style$Builder;->setMeasure(I)Lcom/google/speech/tts/Style$Builder;

    .line 279
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Style;->hasTelephone()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 280
    invoke-virtual {p1}, Lcom/google/speech/tts/Style;->getTelephone()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Style$Builder;->setTelephone(I)Lcom/google/speech/tts/Style$Builder;

    .line 282
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Style;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {p1}, Lcom/google/speech/tts/Style;->getDate()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Style$Builder;->setDate(I)Lcom/google/speech/tts/Style$Builder;

    goto :goto_0
.end method

.method public setDate(I)Lcom/google/speech/tts/Style$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Style;->hasDate:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Style;->access$1302(Lcom/google/speech/tts/Style;Z)Z

    .line 431
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    # setter for: Lcom/google/speech/tts/Style;->date_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Style;->access$1402(Lcom/google/speech/tts/Style;I)I

    .line 432
    return-object p0
.end method

.method public setFraction(I)Lcom/google/speech/tts/Style$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Style;->hasFraction:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Style;->access$302(Lcom/google/speech/tts/Style;Z)Z

    .line 341
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    # setter for: Lcom/google/speech/tts/Style;->fraction_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Style;->access$402(Lcom/google/speech/tts/Style;I)I

    .line 342
    return-object p0
.end method

.method public setMeasure(I)Lcom/google/speech/tts/Style$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Style;->hasMeasure:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Style;->access$902(Lcom/google/speech/tts/Style;Z)Z

    .line 395
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    # setter for: Lcom/google/speech/tts/Style;->measure_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Style;->access$1002(Lcom/google/speech/tts/Style;I)I

    .line 396
    return-object p0
.end method

.method public setMoney(I)Lcom/google/speech/tts/Style$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Style;->hasMoney:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Style;->access$702(Lcom/google/speech/tts/Style;Z)Z

    .line 377
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    # setter for: Lcom/google/speech/tts/Style;->money_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Style;->access$802(Lcom/google/speech/tts/Style;I)I

    .line 378
    return-object p0
.end method

.method public setTelephone(I)Lcom/google/speech/tts/Style$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Style;->hasTelephone:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Style;->access$1102(Lcom/google/speech/tts/Style;Z)Z

    .line 413
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    # setter for: Lcom/google/speech/tts/Style;->telephone_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Style;->access$1202(Lcom/google/speech/tts/Style;I)I

    .line 414
    return-object p0
.end method

.method public setTime(I)Lcom/google/speech/tts/Style$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Style;->hasTime:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Style;->access$502(Lcom/google/speech/tts/Style;Z)Z

    .line 359
    iget-object v0, p0, Lcom/google/speech/tts/Style$Builder;->result:Lcom/google/speech/tts/Style;

    # setter for: Lcom/google/speech/tts/Style;->time_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Style;->access$602(Lcom/google/speech/tts/Style;I)I

    .line 360
    return-object p0
.end method
