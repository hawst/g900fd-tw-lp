.class public final Lcom/google/speech/tts/TimeValueFunctionSamples;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "TimeValueFunctionSamples.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/TimeValueFunctionSamples$1;,
        Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/TimeValueFunctionSamples;


# instance fields
.field private hasTime:Z

.field private hasValue:Z

.field private memoizedSerializedSize:I

.field private time_:F

.field private value_:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 290
    new-instance v0, Lcom/google/speech/tts/TimeValueFunctionSamples;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/TimeValueFunctionSamples;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/TimeValueFunctionSamples;->defaultInstance:Lcom/google/speech/tts/TimeValueFunctionSamples;

    .line 291
    invoke-static {}, Lcom/google/speech/tts/AdvancedVoiceModProto;->internalForceInit()V

    .line 292
    sget-object v0, Lcom/google/speech/tts/TimeValueFunctionSamples;->defaultInstance:Lcom/google/speech/tts/TimeValueFunctionSamples;

    invoke-direct {v0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->initFields()V

    .line 293
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->time_:F

    .line 32
    iput v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->value_:F

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/TimeValueFunctionSamples$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/TimeValueFunctionSamples$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->time_:F

    .line 32
    iput v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->value_:F

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/tts/TimeValueFunctionSamples;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/TimeValueFunctionSamples;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->hasTime:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/TimeValueFunctionSamples;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/TimeValueFunctionSamples;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->time_:F

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/TimeValueFunctionSamples;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/TimeValueFunctionSamples;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->hasValue:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/TimeValueFunctionSamples;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/TimeValueFunctionSamples;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->value_:F

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/TimeValueFunctionSamples;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/TimeValueFunctionSamples;->defaultInstance:Lcom/google/speech/tts/TimeValueFunctionSamples;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
    .locals 1

    .prologue
    .line 140
    # invokes: Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->create()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
    invoke-static {}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->access$100()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/TimeValueFunctionSamples;)Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/TimeValueFunctionSamples;

    .prologue
    .line 143
    invoke-static {}, Lcom/google/speech/tts/TimeValueFunctionSamples;->newBuilder()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;->mergeFrom(Lcom/google/speech/tts/TimeValueFunctionSamples;)Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->getDefaultInstanceForType()Lcom/google/speech/tts/TimeValueFunctionSamples;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/TimeValueFunctionSamples;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/TimeValueFunctionSamples;->defaultInstance:Lcom/google/speech/tts/TimeValueFunctionSamples;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 57
    iget v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->memoizedSerializedSize:I

    .line 58
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 70
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 60
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 61
    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->hasTime()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->getTime()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 65
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->hasValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 66
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->getValue()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 69
    :cond_2
    iput v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->memoizedSerializedSize:I

    move v1, v0

    .line 70
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getTime()F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->time_:F

    return v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->value_:F

    return v0
.end method

.method public hasTime()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->hasTime:Z

    return v0
.end method

.method public hasValue()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->hasValue:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 39
    iget-boolean v1, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->hasTime:Z

    if-nez v1, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v0

    .line 40
    :cond_1
    iget-boolean v1, p0, Lcom/google/speech/tts/TimeValueFunctionSamples;->hasValue:Z

    if-eqz v1, :cond_0

    .line 41
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->toBuilder()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;
    .locals 1

    .prologue
    .line 145
    invoke-static {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->newBuilder(Lcom/google/speech/tts/TimeValueFunctionSamples;)Lcom/google/speech/tts/TimeValueFunctionSamples$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->getSerializedSize()I

    .line 47
    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->getTime()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->getValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 53
    :cond_1
    return-void
.end method
