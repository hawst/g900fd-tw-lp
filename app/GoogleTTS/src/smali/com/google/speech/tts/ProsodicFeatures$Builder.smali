.class public final Lcom/google/speech/tts/ProsodicFeatures$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ProsodicFeatures.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/ProsodicFeatures;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/ProsodicFeatures;",
        "Lcom/google/speech/tts/ProsodicFeatures$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/ProsodicFeatures;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/ProsodicFeatures$Builder;
    .locals 1

    .prologue
    .line 175
    invoke-static {}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->create()Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/ProsodicFeatures$Builder;
    .locals 3

    .prologue
    .line 184
    new-instance v0, Lcom/google/speech/tts/ProsodicFeatures$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;-><init>()V

    .line 185
    .local v0, "builder":Lcom/google/speech/tts/ProsodicFeatures$Builder;
    new-instance v1, Lcom/google/speech/tts/ProsodicFeatures;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/ProsodicFeatures;-><init>(Lcom/google/speech/tts/ProsodicFeatures$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    .line 186
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->build()Lcom/google/speech/tts/ProsodicFeatures;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/ProsodicFeatures;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    invoke-static {v0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 217
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->buildPartial()Lcom/google/speech/tts/ProsodicFeatures;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/ProsodicFeatures;
    .locals 3

    .prologue
    .line 230
    iget-object v1, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    if-nez v1, :cond_0

    .line 231
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    .line 235
    .local v0, "returnMe":Lcom/google/speech/tts/ProsodicFeatures;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    .line 236
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->clone()Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->clone()Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/ProsodicFeatures$Builder;
    .locals 2

    .prologue
    .line 203
    invoke-static {}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->create()Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->mergeFrom(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->clone()Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    invoke-virtual {v0}, Lcom/google/speech/tts/ProsodicFeatures;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 175
    check-cast p1, Lcom/google/speech/tts/ProsodicFeatures;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->mergeFrom(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/ProsodicFeatures$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/ProsodicFeatures$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/ProsodicFeatures;

    .prologue
    .line 240
    invoke-static {}, Lcom/google/speech/tts/ProsodicFeatures;->getDefaultInstance()Lcom/google/speech/tts/ProsodicFeatures;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-object p0

    .line 241
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/ProsodicFeatures;->hasContainsHead()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 242
    invoke-virtual {p1}, Lcom/google/speech/tts/ProsodicFeatures;->getContainsHead()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->setContainsHead(Z)Lcom/google/speech/tts/ProsodicFeatures$Builder;

    .line 244
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/ProsodicFeatures;->hasContainsNucleus()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 245
    invoke-virtual {p1}, Lcom/google/speech/tts/ProsodicFeatures;->getContainsNucleus()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->setContainsNucleus(Z)Lcom/google/speech/tts/ProsodicFeatures$Builder;

    .line 247
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/ProsodicFeatures;->hasIntonationType()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 248
    invoke-virtual {p1}, Lcom/google/speech/tts/ProsodicFeatures;->getIntonationType()Lcom/google/speech/tts/IntonationType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->setIntonationType(Lcom/google/speech/tts/IntonationType;)Lcom/google/speech/tts/ProsodicFeatures$Builder;

    .line 250
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/ProsodicFeatures;->hasProminence()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {p1}, Lcom/google/speech/tts/ProsodicFeatures;->getProminence()Lcom/google/speech/tts/Prominence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/ProsodicFeatures$Builder;->mergeProminence(Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/ProsodicFeatures$Builder;

    goto :goto_0
.end method

.method public mergeProminence(Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/ProsodicFeatures$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Prominence;

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    invoke-virtual {v0}, Lcom/google/speech/tts/ProsodicFeatures;->hasProminence()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    # getter for: Lcom/google/speech/tts/ProsodicFeatures;->prominence_:Lcom/google/speech/tts/Prominence;
    invoke-static {v0}, Lcom/google/speech/tts/ProsodicFeatures;->access$1000(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/Prominence;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Prominence;->getDefaultInstance()Lcom/google/speech/tts/Prominence;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 381
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    iget-object v1, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    # getter for: Lcom/google/speech/tts/ProsodicFeatures;->prominence_:Lcom/google/speech/tts/Prominence;
    invoke-static {v1}, Lcom/google/speech/tts/ProsodicFeatures;->access$1000(Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/Prominence;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Prominence;->newBuilder(Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Prominence$Builder;->mergeFrom(Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Prominence$Builder;->buildPartial()Lcom/google/speech/tts/Prominence;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/ProsodicFeatures;->prominence_:Lcom/google/speech/tts/Prominence;
    invoke-static {v0, v1}, Lcom/google/speech/tts/ProsodicFeatures;->access$1002(Lcom/google/speech/tts/ProsodicFeatures;Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence;

    .line 386
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/ProsodicFeatures;->hasProminence:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/ProsodicFeatures;->access$902(Lcom/google/speech/tts/ProsodicFeatures;Z)Z

    .line 387
    return-object p0

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    # setter for: Lcom/google/speech/tts/ProsodicFeatures;->prominence_:Lcom/google/speech/tts/Prominence;
    invoke-static {v0, p1}, Lcom/google/speech/tts/ProsodicFeatures;->access$1002(Lcom/google/speech/tts/ProsodicFeatures;Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence;

    goto :goto_0
.end method

.method public setContainsHead(Z)Lcom/google/speech/tts/ProsodicFeatures$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/ProsodicFeatures;->hasContainsHead:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/ProsodicFeatures;->access$302(Lcom/google/speech/tts/ProsodicFeatures;Z)Z

    .line 310
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    # setter for: Lcom/google/speech/tts/ProsodicFeatures;->containsHead_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/ProsodicFeatures;->access$402(Lcom/google/speech/tts/ProsodicFeatures;Z)Z

    .line 311
    return-object p0
.end method

.method public setContainsNucleus(Z)Lcom/google/speech/tts/ProsodicFeatures$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/ProsodicFeatures;->hasContainsNucleus:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/ProsodicFeatures;->access$502(Lcom/google/speech/tts/ProsodicFeatures;Z)Z

    .line 328
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    # setter for: Lcom/google/speech/tts/ProsodicFeatures;->containsNucleus_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/ProsodicFeatures;->access$602(Lcom/google/speech/tts/ProsodicFeatures;Z)Z

    .line 329
    return-object p0
.end method

.method public setIntonationType(Lcom/google/speech/tts/IntonationType;)Lcom/google/speech/tts/ProsodicFeatures$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/IntonationType;

    .prologue
    .line 345
    if-nez p1, :cond_0

    .line 346
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/ProsodicFeatures;->hasIntonationType:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/ProsodicFeatures;->access$702(Lcom/google/speech/tts/ProsodicFeatures;Z)Z

    .line 349
    iget-object v0, p0, Lcom/google/speech/tts/ProsodicFeatures$Builder;->result:Lcom/google/speech/tts/ProsodicFeatures;

    # setter for: Lcom/google/speech/tts/ProsodicFeatures;->intonationType_:Lcom/google/speech/tts/IntonationType;
    invoke-static {v0, p1}, Lcom/google/speech/tts/ProsodicFeatures;->access$802(Lcom/google/speech/tts/ProsodicFeatures;Lcom/google/speech/tts/IntonationType;)Lcom/google/speech/tts/IntonationType;

    .line 350
    return-object p0
.end method
