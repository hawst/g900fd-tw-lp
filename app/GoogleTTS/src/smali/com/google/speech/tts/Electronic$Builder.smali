.class public final Lcom/google/speech/tts/Electronic$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Electronic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Electronic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Electronic;",
        "Lcom/google/speech/tts/Electronic$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Electronic;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 301
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Electronic$Builder;
    .locals 1

    .prologue
    .line 295
    invoke-static {}, Lcom/google/speech/tts/Electronic$Builder;->create()Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Electronic$Builder;
    .locals 3

    .prologue
    .line 304
    new-instance v0, Lcom/google/speech/tts/Electronic$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Electronic$Builder;-><init>()V

    .line 305
    .local v0, "builder":Lcom/google/speech/tts/Electronic$Builder;
    new-instance v1, Lcom/google/speech/tts/Electronic;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Electronic;-><init>(Lcom/google/speech/tts/Electronic$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    .line 306
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic$Builder;->build()Lcom/google/speech/tts/Electronic;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Electronic;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    invoke-static {v0}, Lcom/google/speech/tts/Electronic$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 337
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic$Builder;->buildPartial()Lcom/google/speech/tts/Electronic;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Electronic;
    .locals 3

    .prologue
    .line 350
    iget-object v1, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    if-nez v1, :cond_0

    .line 351
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 354
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # getter for: Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Electronic;->access$300(Lcom/google/speech/tts/Electronic;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 355
    iget-object v1, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    iget-object v2, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # getter for: Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Electronic;->access$300(Lcom/google/speech/tts/Electronic;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Electronic;->access$302(Lcom/google/speech/tts/Electronic;Ljava/util/List;)Ljava/util/List;

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    .line 359
    .local v0, "returnMe":Lcom/google/speech/tts/Electronic;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    .line 360
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic$Builder;->clone()Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic$Builder;->clone()Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Electronic$Builder;
    .locals 2

    .prologue
    .line 323
    invoke-static {}, Lcom/google/speech/tts/Electronic$Builder;->create()Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Electronic$Builder;->mergeFrom(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/speech/tts/Electronic$Builder;->clone()Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    invoke-virtual {v0}, Lcom/google/speech/tts/Electronic;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 295
    check-cast p1, Lcom/google/speech/tts/Electronic;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Electronic$Builder;->mergeFrom(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Electronic;

    .prologue
    .line 364
    invoke-static {}, Lcom/google/speech/tts/Electronic;->getDefaultInstance()Lcom/google/speech/tts/Electronic;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 404
    :cond_0
    :goto_0
    return-object p0

    .line 365
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->hasProtocol()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 366
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->getProtocol()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Electronic$Builder;->setProtocol(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 368
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->hasUsername()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 369
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->getUsername()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Electronic$Builder;->setUsername(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 371
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->hasPassword()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 372
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->getPassword()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Electronic$Builder;->setPassword(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 374
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->hasDomain()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 375
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->getDomain()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Electronic$Builder;->setDomain(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 377
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->hasPort()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 378
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->getPort()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Electronic$Builder;->setPort(I)Lcom/google/speech/tts/Electronic$Builder;

    .line 380
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->hasPath()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 381
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Electronic$Builder;->setPath(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 383
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->hasQueryString()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 384
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->getQueryString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Electronic$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 386
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->hasFragmentId()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 387
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->getFragmentId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Electronic$Builder;->setFragmentId(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 389
    :cond_9
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 390
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Electronic$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 392
    :cond_a
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->hasPreserveOrder()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 393
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->getPreserveOrder()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Electronic$Builder;->setPreserveOrder(Z)Lcom/google/speech/tts/Electronic$Builder;

    .line 395
    :cond_b
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 396
    invoke-virtual {p1}, Lcom/google/speech/tts/Electronic;->getCodeSwitch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Electronic$Builder;->setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 398
    :cond_c
    # getter for: Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Electronic;->access$300(Lcom/google/speech/tts/Electronic;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # getter for: Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Electronic;->access$300(Lcom/google/speech/tts/Electronic;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 400
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Electronic;->access$302(Lcom/google/speech/tts/Electronic;Ljava/util/List;)Ljava/util/List;

    .line 402
    :cond_d
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # getter for: Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Electronic;->access$300(Lcom/google/speech/tts/Electronic;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Electronic;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Electronic;->access$300(Lcom/google/speech/tts/Electronic;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 687
    if-nez p1, :cond_0

    .line 688
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 690
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Electronic;->hasCodeSwitch:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Electronic;->access$2402(Lcom/google/speech/tts/Electronic;Z)Z

    .line 691
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # setter for: Lcom/google/speech/tts/Electronic;->codeSwitch_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Electronic;->access$2502(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;

    .line 692
    return-object p0
.end method

.method public setDomain(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 546
    if-nez p1, :cond_0

    .line 547
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 549
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Electronic;->hasDomain:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Electronic;->access$1002(Lcom/google/speech/tts/Electronic;Z)Z

    .line 550
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # setter for: Lcom/google/speech/tts/Electronic;->domain_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Electronic;->access$1102(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;

    .line 551
    return-object p0
.end method

.method public setFragmentId(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 627
    if-nez p1, :cond_0

    .line 628
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 630
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Electronic;->hasFragmentId:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Electronic;->access$1802(Lcom/google/speech/tts/Electronic;Z)Z

    .line 631
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # setter for: Lcom/google/speech/tts/Electronic;->fragmentId_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Electronic;->access$1902(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;

    .line 632
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 648
    if-nez p1, :cond_0

    .line 649
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 651
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Electronic;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Electronic;->access$2002(Lcom/google/speech/tts/Electronic;Z)Z

    .line 652
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # setter for: Lcom/google/speech/tts/Electronic;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Electronic;->access$2102(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;

    .line 653
    return-object p0
.end method

.method public setPassword(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 525
    if-nez p1, :cond_0

    .line 526
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Electronic;->hasPassword:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Electronic;->access$802(Lcom/google/speech/tts/Electronic;Z)Z

    .line 529
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # setter for: Lcom/google/speech/tts/Electronic;->password_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Electronic;->access$902(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;

    .line 530
    return-object p0
.end method

.method public setPath(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 585
    if-nez p1, :cond_0

    .line 586
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 588
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Electronic;->hasPath:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Electronic;->access$1402(Lcom/google/speech/tts/Electronic;Z)Z

    .line 589
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # setter for: Lcom/google/speech/tts/Electronic;->path_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Electronic;->access$1502(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;

    .line 590
    return-object p0
.end method

.method public setPort(I)Lcom/google/speech/tts/Electronic$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Electronic;->hasPort:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Electronic;->access$1202(Lcom/google/speech/tts/Electronic;Z)Z

    .line 568
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # setter for: Lcom/google/speech/tts/Electronic;->port_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Electronic;->access$1302(Lcom/google/speech/tts/Electronic;I)I

    .line 569
    return-object p0
.end method

.method public setPreserveOrder(Z)Lcom/google/speech/tts/Electronic$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 669
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Electronic;->hasPreserveOrder:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Electronic;->access$2202(Lcom/google/speech/tts/Electronic;Z)Z

    .line 670
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # setter for: Lcom/google/speech/tts/Electronic;->preserveOrder_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Electronic;->access$2302(Lcom/google/speech/tts/Electronic;Z)Z

    .line 671
    return-object p0
.end method

.method public setProtocol(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 483
    if-nez p1, :cond_0

    .line 484
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Electronic;->hasProtocol:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Electronic;->access$402(Lcom/google/speech/tts/Electronic;Z)Z

    .line 487
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # setter for: Lcom/google/speech/tts/Electronic;->protocol_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Electronic;->access$502(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;

    .line 488
    return-object p0
.end method

.method public setQueryString(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 606
    if-nez p1, :cond_0

    .line 607
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 609
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Electronic;->hasQueryString:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Electronic;->access$1602(Lcom/google/speech/tts/Electronic;Z)Z

    .line 610
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # setter for: Lcom/google/speech/tts/Electronic;->queryString_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Electronic;->access$1702(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;

    .line 611
    return-object p0
.end method

.method public setUsername(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 504
    if-nez p1, :cond_0

    .line 505
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Electronic;->hasUsername:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Electronic;->access$602(Lcom/google/speech/tts/Electronic;Z)Z

    .line 508
    iget-object v0, p0, Lcom/google/speech/tts/Electronic$Builder;->result:Lcom/google/speech/tts/Electronic;

    # setter for: Lcom/google/speech/tts/Electronic;->username_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Electronic;->access$702(Lcom/google/speech/tts/Electronic;Ljava/lang/String;)Ljava/lang/String;

    .line 509
    return-object p0
.end method
