.class public final Lcom/google/speech/tts/Duplicator$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Duplicator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Duplicator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Duplicator;",
        "Lcom/google/speech/tts/Duplicator$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Duplicator;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Duplicator$Builder;
    .locals 1

    .prologue
    .line 262
    invoke-static {}, Lcom/google/speech/tts/Duplicator$Builder;->create()Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Duplicator$Builder;
    .locals 3

    .prologue
    .line 271
    new-instance v0, Lcom/google/speech/tts/Duplicator$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Duplicator$Builder;-><init>()V

    .line 272
    .local v0, "builder":Lcom/google/speech/tts/Duplicator$Builder;
    new-instance v1, Lcom/google/speech/tts/Duplicator;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Duplicator;-><init>(Lcom/google/speech/tts/Duplicator$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    .line 273
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator$Builder;->build()Lcom/google/speech/tts/Duplicator;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Duplicator;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    invoke-static {v0}, Lcom/google/speech/tts/Duplicator$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 304
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator$Builder;->buildPartial()Lcom/google/speech/tts/Duplicator;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Duplicator;
    .locals 3

    .prologue
    .line 317
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    if-nez v1, :cond_0

    .line 318
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 321
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Duplicator;->access$300(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 322
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    iget-object v2, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Duplicator;->access$300(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Duplicator;->access$302(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 325
    :cond_1
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Duplicator;->access$400(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 326
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    iget-object v2, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Duplicator;->access$400(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Duplicator;->access$402(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 329
    :cond_2
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Duplicator;->access$500(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 330
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    iget-object v2, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Duplicator;->access$500(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Duplicator;->access$502(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 333
    :cond_3
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Duplicator;->access$600(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 334
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    iget-object v2, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Duplicator;->access$600(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Duplicator;->access$602(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 337
    :cond_4
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Duplicator;->access$700(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_5

    .line 338
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    iget-object v2, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Duplicator;->access$700(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Duplicator;->access$702(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 341
    :cond_5
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Duplicator;->access$800(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_6

    .line 342
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    iget-object v2, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Duplicator;->access$800(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Duplicator;->access$802(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 345
    :cond_6
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Duplicator;->access$900(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_7

    .line 346
    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    iget-object v2, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Duplicator;->access$900(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Duplicator;->access$902(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 349
    :cond_7
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    .line 350
    .local v0, "returnMe":Lcom/google/speech/tts/Duplicator;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    .line 351
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator$Builder;->clone()Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator$Builder;->clone()Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Duplicator$Builder;
    .locals 2

    .prologue
    .line 290
    invoke-static {}, Lcom/google/speech/tts/Duplicator$Builder;->create()Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Duplicator$Builder;->mergeFrom(Lcom/google/speech/tts/Duplicator;)Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/speech/tts/Duplicator$Builder;->clone()Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    invoke-virtual {v0}, Lcom/google/speech/tts/Duplicator;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 262
    check-cast p1, Lcom/google/speech/tts/Duplicator;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Duplicator$Builder;->mergeFrom(Lcom/google/speech/tts/Duplicator;)Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Duplicator;)Lcom/google/speech/tts/Duplicator$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Duplicator;

    .prologue
    .line 355
    invoke-static {}, Lcom/google/speech/tts/Duplicator;->getDefaultInstance()Lcom/google/speech/tts/Duplicator;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 398
    :cond_0
    :goto_0
    return-object p0

    .line 356
    :cond_1
    # getter for: Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$300(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 357
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$300(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 358
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Duplicator;->access$302(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 360
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$300(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Duplicator;->date_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$300(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 362
    :cond_3
    # getter for: Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$400(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 363
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$400(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 364
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Duplicator;->access$402(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 366
    :cond_4
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$400(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Duplicator;->decimal_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$400(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 368
    :cond_5
    # getter for: Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$500(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 369
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$500(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 370
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Duplicator;->access$502(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 372
    :cond_6
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$500(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Duplicator;->fraction_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$500(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 374
    :cond_7
    # getter for: Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$600(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 375
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$600(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 376
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Duplicator;->access$602(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 378
    :cond_8
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$600(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Duplicator;->measure_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$600(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 380
    :cond_9
    # getter for: Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$700(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 381
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$700(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 382
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Duplicator;->access$702(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 384
    :cond_a
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$700(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Duplicator;->money_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$700(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 386
    :cond_b
    # getter for: Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$800(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 387
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$800(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 388
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Duplicator;->access$802(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 390
    :cond_c
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$800(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Duplicator;->ordinal_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$800(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 392
    :cond_d
    # getter for: Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$900(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$900(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 394
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Duplicator;->access$902(Lcom/google/speech/tts/Duplicator;Ljava/util/List;)Ljava/util/List;

    .line 396
    :cond_e
    iget-object v0, p0, Lcom/google/speech/tts/Duplicator$Builder;->result:Lcom/google/speech/tts/Duplicator;

    # getter for: Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Duplicator;->access$900(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Duplicator;->time_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Duplicator;->access$900(Lcom/google/speech/tts/Duplicator;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method
