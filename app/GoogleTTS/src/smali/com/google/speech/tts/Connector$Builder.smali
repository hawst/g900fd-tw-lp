.class public final Lcom/google/speech/tts/Connector$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Connector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Connector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Connector;",
        "Lcom/google/speech/tts/Connector$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Connector;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Connector$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-static {}, Lcom/google/speech/tts/Connector$Builder;->create()Lcom/google/speech/tts/Connector$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Connector$Builder;
    .locals 3

    .prologue
    .line 168
    new-instance v0, Lcom/google/speech/tts/Connector$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Connector$Builder;-><init>()V

    .line 169
    .local v0, "builder":Lcom/google/speech/tts/Connector$Builder;
    new-instance v1, Lcom/google/speech/tts/Connector;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Connector;-><init>(Lcom/google/speech/tts/Connector$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    .line 170
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector$Builder;->build()Lcom/google/speech/tts/Connector;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Connector;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Connector$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    invoke-static {v0}, Lcom/google/speech/tts/Connector$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 201
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector$Builder;->buildPartial()Lcom/google/speech/tts/Connector;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Connector;
    .locals 3

    .prologue
    .line 214
    iget-object v1, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    if-nez v1, :cond_0

    .line 215
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    .line 219
    .local v0, "returnMe":Lcom/google/speech/tts/Connector;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    .line 220
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector$Builder;->clone()Lcom/google/speech/tts/Connector$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector$Builder;->clone()Lcom/google/speech/tts/Connector$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Connector$Builder;
    .locals 2

    .prologue
    .line 187
    invoke-static {}, Lcom/google/speech/tts/Connector$Builder;->create()Lcom/google/speech/tts/Connector$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Connector$Builder;->mergeFrom(Lcom/google/speech/tts/Connector;)Lcom/google/speech/tts/Connector$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector$Builder;->clone()Lcom/google/speech/tts/Connector$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    invoke-virtual {v0}, Lcom/google/speech/tts/Connector;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 159
    check-cast p1, Lcom/google/speech/tts/Connector;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Connector$Builder;->mergeFrom(Lcom/google/speech/tts/Connector;)Lcom/google/speech/tts/Connector$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Connector;)Lcom/google/speech/tts/Connector$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/Connector;

    .prologue
    .line 224
    invoke-static {}, Lcom/google/speech/tts/Connector;->getDefaultInstance()Lcom/google/speech/tts/Connector;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 234
    :cond_0
    :goto_0
    return-object p0

    .line 225
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Connector;->hasType()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 226
    invoke-virtual {p1}, Lcom/google/speech/tts/Connector;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Connector$Builder;->setType(Ljava/lang/String;)Lcom/google/speech/tts/Connector$Builder;

    .line 228
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Connector;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 229
    invoke-virtual {p1}, Lcom/google/speech/tts/Connector;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Connector$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Connector$Builder;

    .line 231
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Connector;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p1}, Lcom/google/speech/tts/Connector;->getCodeSwitch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Connector$Builder;->setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Connector$Builder;

    goto :goto_0
.end method

.method public setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Connector$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 319
    if-nez p1, :cond_0

    .line 320
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Connector;->hasCodeSwitch:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Connector;->access$702(Lcom/google/speech/tts/Connector;Z)Z

    .line 323
    iget-object v0, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    # setter for: Lcom/google/speech/tts/Connector;->codeSwitch_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Connector;->access$802(Lcom/google/speech/tts/Connector;Ljava/lang/String;)Ljava/lang/String;

    .line 324
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Connector$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 298
    if-nez p1, :cond_0

    .line 299
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Connector;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Connector;->access$502(Lcom/google/speech/tts/Connector;Z)Z

    .line 302
    iget-object v0, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    # setter for: Lcom/google/speech/tts/Connector;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Connector;->access$602(Lcom/google/speech/tts/Connector;Ljava/lang/String;)Ljava/lang/String;

    .line 303
    return-object p0
.end method

.method public setType(Ljava/lang/String;)Lcom/google/speech/tts/Connector$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 277
    if-nez p1, :cond_0

    .line 278
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Connector;->hasType:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Connector;->access$302(Lcom/google/speech/tts/Connector;Z)Z

    .line 281
    iget-object v0, p0, Lcom/google/speech/tts/Connector$Builder;->result:Lcom/google/speech/tts/Connector;

    # setter for: Lcom/google/speech/tts/Connector;->type_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Connector;->access$402(Lcom/google/speech/tts/Connector;Ljava/lang/String;)Ljava/lang/String;

    .line 282
    return-object p0
.end method
