.class public final Lcom/google/speech/tts/Address;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Address.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Address$1;,
        Lcom/google/speech/tts/Address$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Address;


# instance fields
.field private adminArea_:Ljava/lang/String;

.field private codeSwitch_:Ljava/lang/String;

.field private countryName_:Ljava/lang/String;

.field private dependentLocality_:Ljava/lang/String;

.field private fieldOrder_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasAdminArea:Z

.field private hasCodeSwitch:Z

.field private hasCountryName:Z

.field private hasDependentLocality:Z

.field private hasLocality:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasPoBox:Z

.field private hasPostCode:Z

.field private hasPreserveOrder:Z

.field private hasSubadminArea:Z

.field private hasThoroughfare:Z

.field private locality_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private poBox_:Ljava/lang/String;

.field private postCode_:Ljava/lang/String;

.field private preserveOrder_:Z

.field private subadminArea_:Ljava/lang/String;

.field private thoroughfare_:Lcom/google/speech/tts/Thoroughfare;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 769
    new-instance v0, Lcom/google/speech/tts/Address;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Address;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Address;->defaultInstance:Lcom/google/speech/tts/Address;

    .line 770
    invoke-static {}, Lcom/google/speech/tts/AddressProto;->internalForceInit()V

    .line 771
    sget-object v0, Lcom/google/speech/tts/Address;->defaultInstance:Lcom/google/speech/tts/Address;

    invoke-direct {v0}, Lcom/google/speech/tts/Address;->initFields()V

    .line 772
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->dependentLocality_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->locality_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->subadminArea_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->adminArea_:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->countryName_:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->postCode_:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->poBox_:Ljava/lang/String;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/tts/Address;->preserveOrder_:Z

    .line 95
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->codeSwitch_:Ljava/lang/String;

    .line 101
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;

    .line 159
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Address;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Address;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Address$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Address$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Address;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->dependentLocality_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->locality_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->subadminArea_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->adminArea_:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->countryName_:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->postCode_:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->poBox_:Ljava/lang/String;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/tts/Address;->preserveOrder_:Z

    .line 95
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Address;->codeSwitch_:Ljava/lang/String;

    .line 101
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;

    .line 159
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Address;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Address;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Address;->hasSubadminArea:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Address;->subadminArea_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/Address;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Address;->hasAdminArea:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Address;->adminArea_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/Address;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Address;->hasCountryName:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Address;->countryName_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/Address;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Address;->hasPostCode:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Address;->postCode_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/google/speech/tts/Address;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Address;->hasPoBox:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Address;->poBox_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2002(Lcom/google/speech/tts/Address;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Address;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$2102(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Address;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2202(Lcom/google/speech/tts/Address;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Address;->hasPreserveOrder:Z

    return p1
.end method

.method static synthetic access$2302(Lcom/google/speech/tts/Address;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Address;->preserveOrder_:Z

    return p1
.end method

.method static synthetic access$2402(Lcom/google/speech/tts/Address;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Address;->hasCodeSwitch:Z

    return p1
.end method

.method static synthetic access$2502(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Address;->codeSwitch_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Address;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Address;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Address;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Address;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Address;->hasThoroughfare:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Thoroughfare;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Address;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Address;->thoroughfare_:Lcom/google/speech/tts/Thoroughfare;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Address;Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Thoroughfare;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Lcom/google/speech/tts/Thoroughfare;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Address;->thoroughfare_:Lcom/google/speech/tts/Thoroughfare;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Address;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Address;->hasDependentLocality:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Address;->dependentLocality_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Address;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Address;->hasLocality:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Address;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Address;->locality_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Address;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Address;->defaultInstance:Lcom/google/speech/tts/Address;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 112
    invoke-static {}, Lcom/google/speech/tts/Thoroughfare;->getDefaultInstance()Lcom/google/speech/tts/Thoroughfare;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Address;->thoroughfare_:Lcom/google/speech/tts/Thoroughfare;

    .line 113
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Address$Builder;
    .locals 1

    .prologue
    .line 289
    # invokes: Lcom/google/speech/tts/Address$Builder;->create()Lcom/google/speech/tts/Address$Builder;
    invoke-static {}, Lcom/google/speech/tts/Address$Builder;->access$100()Lcom/google/speech/tts/Address$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Address;

    .prologue
    .line 292
    invoke-static {}, Lcom/google/speech/tts/Address;->newBuilder()Lcom/google/speech/tts/Address$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Address$Builder;->mergeFrom(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAdminArea()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/tts/Address;->adminArea_:Ljava/lang/String;

    return-object v0
.end method

.method public getCodeSwitch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/speech/tts/Address;->codeSwitch_:Ljava/lang/String;

    return-object v0
.end method

.method public getCountryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/speech/tts/Address;->countryName_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getDefaultInstanceForType()Lcom/google/speech/tts/Address;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Address;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Address;->defaultInstance:Lcom/google/speech/tts/Address;

    return-object v0
.end method

.method public getDependentLocality()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/Address;->dependentLocality_:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldOrderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method public getLocality()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/Address;->locality_:Ljava/lang/String;

    return-object v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/speech/tts/Address;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getPoBox()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/speech/tts/Address;->poBox_:Ljava/lang/String;

    return-object v0
.end method

.method public getPostCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/speech/tts/Address;->postCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getPreserveOrder()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/google/speech/tts/Address;->preserveOrder_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 161
    iget v3, p0, Lcom/google/speech/tts/Address;->memoizedSerializedSize:I

    .line 162
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 219
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 164
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 165
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasThoroughfare()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 166
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getThoroughfare()Lcom/google/speech/tts/Thoroughfare;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 169
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasDependentLocality()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 170
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getDependentLocality()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 173
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasLocality()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 174
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getLocality()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 177
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasSubadminArea()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 178
    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getSubadminArea()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 181
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasAdminArea()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 182
    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 185
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasCountryName()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 186
    const/4 v5, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getCountryName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 189
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasPostCode()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 190
    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getPostCode()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 193
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasPoBox()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 194
    const/16 v5, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getPoBox()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 197
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasMorphosyntacticFeatures()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 198
    const/16 v5, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 201
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasPreserveOrder()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 202
    const/16 v5, 0xa

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getPreserveOrder()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 205
    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasCodeSwitch()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 206
    const/16 v5, 0xb

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getCodeSwitch()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 210
    :cond_b
    const/4 v0, 0x0

    .line 211
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 212
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 214
    goto :goto_1

    .line 215
    .end local v1    # "element":Ljava/lang/String;
    :cond_c
    add-int/2addr v3, v0

    .line 216
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 218
    iput v3, p0, Lcom/google/speech/tts/Address;->memoizedSerializedSize:I

    move v4, v3

    .line 219
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getSubadminArea()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/tts/Address;->subadminArea_:Ljava/lang/String;

    return-object v0
.end method

.method public getThoroughfare()Lcom/google/speech/tts/Thoroughfare;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Address;->thoroughfare_:Lcom/google/speech/tts/Thoroughfare;

    return-object v0
.end method

.method public hasAdminArea()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/Address;->hasAdminArea:Z

    return v0
.end method

.method public hasCodeSwitch()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/speech/tts/Address;->hasCodeSwitch:Z

    return v0
.end method

.method public hasCountryName()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/Address;->hasCountryName:Z

    return v0
.end method

.method public hasDependentLocality()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Address;->hasDependentLocality:Z

    return v0
.end method

.method public hasLocality()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/Address;->hasLocality:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/speech/tts/Address;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasPoBox()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/speech/tts/Address;->hasPoBox:Z

    return v0
.end method

.method public hasPostCode()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/Address;->hasPostCode:Z

    return v0
.end method

.method public hasPreserveOrder()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/speech/tts/Address;->hasPreserveOrder:Z

    return v0
.end method

.method public hasSubadminArea()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/Address;->hasSubadminArea:Z

    return v0
.end method

.method public hasThoroughfare()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Address;->hasThoroughfare:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->toBuilder()Lcom/google/speech/tts/Address$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Address$Builder;
    .locals 1

    .prologue
    .line 294
    invoke-static {p0}, Lcom/google/speech/tts/Address;->newBuilder(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getSerializedSize()I

    .line 121
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasThoroughfare()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getThoroughfare()Lcom/google/speech/tts/Thoroughfare;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 124
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasDependentLocality()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 125
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getDependentLocality()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 127
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasLocality()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 128
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getLocality()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 130
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasSubadminArea()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 131
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getSubadminArea()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 133
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasAdminArea()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 134
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 136
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasCountryName()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 137
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getCountryName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 139
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasPostCode()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 140
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getPostCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 142
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasPoBox()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 143
    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getPoBox()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 145
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 146
    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 148
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasPreserveOrder()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 149
    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getPreserveOrder()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 151
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->hasCodeSwitch()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 152
    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getCodeSwitch()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 154
    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/tts/Address;->getFieldOrderList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 155
    .local v0, "element":Ljava/lang/String;
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 157
    .end local v0    # "element":Ljava/lang/String;
    :cond_b
    return-void
.end method
