.class public final enum Lcom/google/speech/tts/IntonationType;
.super Ljava/lang/Enum;
.source "IntonationType.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/IntonationType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/IntonationType;

.field public static final enum CHOICE_QUESTION:Lcom/google/speech/tts/IntonationType;

.field public static final enum COMMAND:Lcom/google/speech/tts/IntonationType;

.field public static final enum ELLIPTICAL_QUESTION:Lcom/google/speech/tts/IntonationType;

.field public static final enum ELLIPTICAL_STATEMENT:Lcom/google/speech/tts/IntonationType;

.field public static final enum FULL_STATEMENT:Lcom/google/speech/tts/IntonationType;

.field public static final enum NON_FINAL:Lcom/google/speech/tts/IntonationType;

.field public static final enum REQUEST_QUESTION:Lcom/google/speech/tts/IntonationType;

.field public static final enum SIMPLY_A_QUESTION:Lcom/google/speech/tts/IntonationType;

.field public static final enum STATEMENT_QUESTION:Lcom/google/speech/tts/IntonationType;

.field public static final enum TAG_QUESTION:Lcom/google/speech/tts/IntonationType;

.field public static final enum UNKNOWN:Lcom/google/speech/tts/IntonationType;

.field public static final enum WH_QUESTION:Lcom/google/speech/tts/IntonationType;

.field public static final enum YES_NO_QUESTION:Lcom/google/speech/tts/IntonationType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/IntonationType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/16 v8, 0xc

    const/16 v7, 0xb

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 7
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->UNKNOWN:Lcom/google/speech/tts/IntonationType;

    .line 8
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "NON_FINAL"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->NON_FINAL:Lcom/google/speech/tts/IntonationType;

    .line 9
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "FULL_STATEMENT"

    invoke-direct {v0, v1, v9, v9, v7}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->FULL_STATEMENT:Lcom/google/speech/tts/IntonationType;

    .line 10
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "ELLIPTICAL_STATEMENT"

    const/4 v2, 0x3

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->ELLIPTICAL_STATEMENT:Lcom/google/speech/tts/IntonationType;

    .line 11
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "COMMAND"

    const/4 v2, 0x4

    const/4 v3, 0x4

    const/16 v4, 0x15

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->COMMAND:Lcom/google/speech/tts/IntonationType;

    .line 12
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "YES_NO_QUESTION"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/16 v4, 0x1f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->YES_NO_QUESTION:Lcom/google/speech/tts/IntonationType;

    .line 13
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "WH_QUESTION"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/16 v4, 0x20

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->WH_QUESTION:Lcom/google/speech/tts/IntonationType;

    .line 14
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "STATEMENT_QUESTION"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/16 v4, 0x21

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->STATEMENT_QUESTION:Lcom/google/speech/tts/IntonationType;

    .line 15
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "REQUEST_QUESTION"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->REQUEST_QUESTION:Lcom/google/speech/tts/IntonationType;

    .line 16
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "ELLIPTICAL_QUESTION"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const/16 v4, 0x23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->ELLIPTICAL_QUESTION:Lcom/google/speech/tts/IntonationType;

    .line 17
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "TAG_QUESTION"

    const/16 v2, 0xa

    const/16 v3, 0xa

    const/16 v4, 0x24

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->TAG_QUESTION:Lcom/google/speech/tts/IntonationType;

    .line 18
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "CHOICE_QUESTION"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v7, v7, v2}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->CHOICE_QUESTION:Lcom/google/speech/tts/IntonationType;

    .line 19
    new-instance v0, Lcom/google/speech/tts/IntonationType;

    const-string v1, "SIMPLY_A_QUESTION"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v8, v8, v2}, Lcom/google/speech/tts/IntonationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->SIMPLY_A_QUESTION:Lcom/google/speech/tts/IntonationType;

    .line 5
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/google/speech/tts/IntonationType;

    sget-object v1, Lcom/google/speech/tts/IntonationType;->UNKNOWN:Lcom/google/speech/tts/IntonationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/speech/tts/IntonationType;->NON_FINAL:Lcom/google/speech/tts/IntonationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/speech/tts/IntonationType;->FULL_STATEMENT:Lcom/google/speech/tts/IntonationType;

    aput-object v1, v0, v9

    const/4 v1, 0x3

    sget-object v2, Lcom/google/speech/tts/IntonationType;->ELLIPTICAL_STATEMENT:Lcom/google/speech/tts/IntonationType;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/speech/tts/IntonationType;->COMMAND:Lcom/google/speech/tts/IntonationType;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/speech/tts/IntonationType;->YES_NO_QUESTION:Lcom/google/speech/tts/IntonationType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/speech/tts/IntonationType;->WH_QUESTION:Lcom/google/speech/tts/IntonationType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/speech/tts/IntonationType;->STATEMENT_QUESTION:Lcom/google/speech/tts/IntonationType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/speech/tts/IntonationType;->REQUEST_QUESTION:Lcom/google/speech/tts/IntonationType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/speech/tts/IntonationType;->ELLIPTICAL_QUESTION:Lcom/google/speech/tts/IntonationType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/speech/tts/IntonationType;->TAG_QUESTION:Lcom/google/speech/tts/IntonationType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/speech/tts/IntonationType;->CHOICE_QUESTION:Lcom/google/speech/tts/IntonationType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/speech/tts/IntonationType;->SIMPLY_A_QUESTION:Lcom/google/speech/tts/IntonationType;

    aput-object v1, v0, v8

    sput-object v0, Lcom/google/speech/tts/IntonationType;->$VALUES:[Lcom/google/speech/tts/IntonationType;

    .line 49
    new-instance v0, Lcom/google/speech/tts/IntonationType$1;

    invoke-direct {v0}, Lcom/google/speech/tts/IntonationType$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/IntonationType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 59
    iput p3, p0, Lcom/google/speech/tts/IntonationType;->index:I

    .line 60
    iput p4, p0, Lcom/google/speech/tts/IntonationType;->value:I

    .line 61
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/IntonationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/google/speech/tts/IntonationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/IntonationType;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/IntonationType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/google/speech/tts/IntonationType;->$VALUES:[Lcom/google/speech/tts/IntonationType;

    invoke-virtual {v0}, [Lcom/google/speech/tts/IntonationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/IntonationType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/google/speech/tts/IntonationType;->value:I

    return v0
.end method
