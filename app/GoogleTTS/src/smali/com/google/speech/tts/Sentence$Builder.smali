.class public final Lcom/google/speech/tts/Sentence$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Sentence.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Sentence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Sentence;",
        "Lcom/google/speech/tts/Sentence$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Sentence;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 211
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Sentence$Builder;
    .locals 1

    .prologue
    .line 205
    invoke-static {}, Lcom/google/speech/tts/Sentence$Builder;->create()Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Sentence$Builder;
    .locals 3

    .prologue
    .line 214
    new-instance v0, Lcom/google/speech/tts/Sentence$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Sentence$Builder;-><init>()V

    .line 215
    .local v0, "builder":Lcom/google/speech/tts/Sentence$Builder;
    new-instance v1, Lcom/google/speech/tts/Sentence;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Sentence;-><init>(Lcom/google/speech/tts/Sentence$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    .line 216
    return-object v0
.end method


# virtual methods
.method public addAllSay(Ljava/lang/Iterable;)Lcom/google/speech/tts/Sentence$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/speech/tts/Say;",
            ">;)",
            "Lcom/google/speech/tts/Sentence$Builder;"
        }
    .end annotation

    .prologue
    .line 397
    .local p1, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lcom/google/speech/tts/Say;>;"
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$300(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Sentence;->access$302(Lcom/google/speech/tts/Sentence;Ljava/util/List;)Ljava/util/List;

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$300(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 401
    return-object p0
.end method

.method public addEntry(Lcom/google/speech/tts/LexiconProto$Entry;)Lcom/google/speech/tts/Sentence$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/LexiconProto$Entry;

    .prologue
    .line 430
    if-nez p1, :cond_0

    .line 431
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$400(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Sentence;->access$402(Lcom/google/speech/tts/Sentence;Ljava/util/List;)Ljava/util/List;

    .line 436
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$400(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 437
    return-object p0
.end method

.method public addSay(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Sentence$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 379
    if-nez p1, :cond_0

    .line 380
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 382
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$300(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Sentence;->access$302(Lcom/google/speech/tts/Sentence;Ljava/util/List;)Ljava/util/List;

    .line 385
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$300(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence$Builder;->build()Lcom/google/speech/tts/Sentence;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Sentence;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    invoke-static {v0}, Lcom/google/speech/tts/Sentence$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 247
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence$Builder;->buildPartial()Lcom/google/speech/tts/Sentence;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Sentence;
    .locals 3

    .prologue
    .line 260
    iget-object v1, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    if-nez v1, :cond_0

    .line 261
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 264
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Sentence;->access$300(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 265
    iget-object v1, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    iget-object v2, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Sentence;->access$300(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Sentence;->access$302(Lcom/google/speech/tts/Sentence;Ljava/util/List;)Ljava/util/List;

    .line 268
    :cond_1
    iget-object v1, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Sentence;->access$400(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 269
    iget-object v1, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    iget-object v2, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Sentence;->access$400(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Sentence;->access$402(Lcom/google/speech/tts/Sentence;Ljava/util/List;)Ljava/util/List;

    .line 272
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    .line 273
    .local v0, "returnMe":Lcom/google/speech/tts/Sentence;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    .line 274
    return-object v0
.end method

.method public clearSay()Lcom/google/speech/tts/Sentence$Builder;
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Sentence;->access$302(Lcom/google/speech/tts/Sentence;Ljava/util/List;)Ljava/util/List;

    .line 405
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence$Builder;->clone()Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence$Builder;->clone()Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Sentence$Builder;
    .locals 2

    .prologue
    .line 233
    invoke-static {}, Lcom/google/speech/tts/Sentence$Builder;->create()Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Sentence$Builder;->mergeFrom(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence$Builder;->clone()Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getEntryCount()I
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    invoke-virtual {v0}, Lcom/google/speech/tts/Sentence;->getEntryCount()I

    move-result v0

    return v0
.end method

.method public getSayList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Say;",
            ">;"
        }
    .end annotation

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$300(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    invoke-virtual {v0}, Lcom/google/speech/tts/Sentence;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 205
    check-cast p1, Lcom/google/speech/tts/Sentence;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Sentence$Builder;->mergeFrom(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Sentence;

    .prologue
    .line 278
    invoke-static {}, Lcom/google/speech/tts/Sentence;->getDefaultInstance()Lcom/google/speech/tts/Sentence;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 300
    :cond_0
    :goto_0
    return-object p0

    .line 279
    :cond_1
    # getter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Sentence;->access$300(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 280
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$300(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 281
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Sentence;->access$302(Lcom/google/speech/tts/Sentence;Ljava/util/List;)Ljava/util/List;

    .line 283
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$300(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Sentence;->access$300(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 285
    :cond_3
    # getter for: Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Sentence;->access$400(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 286
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$400(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 287
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Sentence;->access$402(Lcom/google/speech/tts/Sentence;Ljava/util/List;)Ljava/util/List;

    .line 289
    :cond_4
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$400(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Sentence;->access$400(Lcom/google/speech/tts/Sentence;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 291
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Sentence;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 292
    invoke-virtual {p1}, Lcom/google/speech/tts/Sentence;->getStyle()Lcom/google/speech/tts/Style;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Sentence$Builder;->mergeStyle(Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Sentence$Builder;

    .line 294
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Sentence;->hasDeprecatedExclude()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 295
    invoke-virtual {p1}, Lcom/google/speech/tts/Sentence;->getDeprecatedExclude()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Sentence$Builder;->setDeprecatedExclude(Lcom/google/protobuf/ByteString;)Lcom/google/speech/tts/Sentence$Builder;

    .line 297
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/Sentence;->hasMorphing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    invoke-virtual {p1}, Lcom/google/speech/tts/Sentence;->getMorphing()Lcom/google/speech/tts/SpeechMorphingTargets;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Sentence$Builder;->mergeMorphing(Lcom/google/speech/tts/SpeechMorphingTargets;)Lcom/google/speech/tts/Sentence$Builder;

    goto/16 :goto_0
.end method

.method public mergeMorphing(Lcom/google/speech/tts/SpeechMorphingTargets;)Lcom/google/speech/tts/Sentence$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/SpeechMorphingTargets;

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    invoke-virtual {v0}, Lcom/google/speech/tts/Sentence;->hasMorphing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->morphing_:Lcom/google/speech/tts/SpeechMorphingTargets;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$1000(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/SpeechMorphingTargets;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/SpeechMorphingTargets;->getDefaultInstance()Lcom/google/speech/tts/SpeechMorphingTargets;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 540
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    iget-object v1, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->morphing_:Lcom/google/speech/tts/SpeechMorphingTargets;
    invoke-static {v1}, Lcom/google/speech/tts/Sentence;->access$1000(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/SpeechMorphingTargets;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/SpeechMorphingTargets;->newBuilder(Lcom/google/speech/tts/SpeechMorphingTargets;)Lcom/google/speech/tts/SpeechMorphingTargets$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/SpeechMorphingTargets$Builder;->mergeFrom(Lcom/google/speech/tts/SpeechMorphingTargets;)Lcom/google/speech/tts/SpeechMorphingTargets$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/SpeechMorphingTargets$Builder;->buildPartial()Lcom/google/speech/tts/SpeechMorphingTargets;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Sentence;->morphing_:Lcom/google/speech/tts/SpeechMorphingTargets;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Sentence;->access$1002(Lcom/google/speech/tts/Sentence;Lcom/google/speech/tts/SpeechMorphingTargets;)Lcom/google/speech/tts/SpeechMorphingTargets;

    .line 545
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Sentence;->hasMorphing:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Sentence;->access$902(Lcom/google/speech/tts/Sentence;Z)Z

    .line 546
    return-object p0

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # setter for: Lcom/google/speech/tts/Sentence;->morphing_:Lcom/google/speech/tts/SpeechMorphingTargets;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Sentence;->access$1002(Lcom/google/speech/tts/Sentence;Lcom/google/speech/tts/SpeechMorphingTargets;)Lcom/google/speech/tts/SpeechMorphingTargets;

    goto :goto_0
.end method

.method public mergeStyle(Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Sentence$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Style;

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    invoke-virtual {v0}, Lcom/google/speech/tts/Sentence;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->style_:Lcom/google/speech/tts/Style;
    invoke-static {v0}, Lcom/google/speech/tts/Sentence;->access$600(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Style;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Style;->getDefaultInstance()Lcom/google/speech/tts/Style;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 482
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    iget-object v1, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # getter for: Lcom/google/speech/tts/Sentence;->style_:Lcom/google/speech/tts/Style;
    invoke-static {v1}, Lcom/google/speech/tts/Sentence;->access$600(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Style;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Style;->newBuilder(Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Style$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Style$Builder;->mergeFrom(Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Style$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Style$Builder;->buildPartial()Lcom/google/speech/tts/Style;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Sentence;->style_:Lcom/google/speech/tts/Style;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Sentence;->access$602(Lcom/google/speech/tts/Sentence;Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Style;

    .line 487
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Sentence;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Sentence;->access$502(Lcom/google/speech/tts/Sentence;Z)Z

    .line 488
    return-object p0

    .line 485
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # setter for: Lcom/google/speech/tts/Sentence;->style_:Lcom/google/speech/tts/Style;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Sentence;->access$602(Lcom/google/speech/tts/Sentence;Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Style;

    goto :goto_0
.end method

.method public setDeprecatedExclude(Lcom/google/protobuf/ByteString;)Lcom/google/speech/tts/Sentence$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/protobuf/ByteString;

    .prologue
    .line 504
    if-nez p1, :cond_0

    .line 505
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Sentence;->hasDeprecatedExclude:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Sentence;->access$702(Lcom/google/speech/tts/Sentence;Z)Z

    .line 508
    iget-object v0, p0, Lcom/google/speech/tts/Sentence$Builder;->result:Lcom/google/speech/tts/Sentence;

    # setter for: Lcom/google/speech/tts/Sentence;->deprecatedExclude_:Lcom/google/protobuf/ByteString;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Sentence;->access$802(Lcom/google/speech/tts/Sentence;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    .line 509
    return-object p0
.end method
