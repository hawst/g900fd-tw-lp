.class public final Lcom/google/speech/tts/Prominence;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Prominence.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Prominence$1;,
        Lcom/google/speech/tts/Prominence$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Prominence;


# instance fields
.field private hasProminenceType:Z

.field private hasWordProminence:Z

.field private memoizedSerializedSize:I

.field private prominenceType_:Lcom/google/speech/tts/ProminenceType;

.field private wordProminence_:Lcom/google/speech/tts/ProminenceLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 304
    new-instance v0, Lcom/google/speech/tts/Prominence;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Prominence;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Prominence;->defaultInstance:Lcom/google/speech/tts/Prominence;

    .line 305
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 306
    sget-object v0, Lcom/google/speech/tts/Prominence;->defaultInstance:Lcom/google/speech/tts/Prominence;

    invoke-direct {v0}, Lcom/google/speech/tts/Prominence;->initFields()V

    .line 307
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Prominence;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Prominence;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Prominence$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Prominence$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Prominence;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Prominence;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Prominence;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Prominence;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Prominence;->hasWordProminence:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Prominence;Lcom/google/speech/tts/ProminenceLevel;)Lcom/google/speech/tts/ProminenceLevel;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Prominence;
    .param p1, "x1"    # Lcom/google/speech/tts/ProminenceLevel;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Prominence;->wordProminence_:Lcom/google/speech/tts/ProminenceLevel;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Prominence;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Prominence;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Prominence;->hasProminenceType:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Prominence;Lcom/google/speech/tts/ProminenceType;)Lcom/google/speech/tts/ProminenceType;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Prominence;
    .param p1, "x1"    # Lcom/google/speech/tts/ProminenceType;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Prominence;->prominenceType_:Lcom/google/speech/tts/ProminenceType;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Prominence;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Prominence;->defaultInstance:Lcom/google/speech/tts/Prominence;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/speech/tts/ProminenceLevel;->NONE:Lcom/google/speech/tts/ProminenceLevel;

    iput-object v0, p0, Lcom/google/speech/tts/Prominence;->wordProminence_:Lcom/google/speech/tts/ProminenceLevel;

    .line 38
    sget-object v0, Lcom/google/speech/tts/ProminenceType;->UNDEFINED:Lcom/google/speech/tts/ProminenceType;

    iput-object v0, p0, Lcom/google/speech/tts/Prominence;->prominenceType_:Lcom/google/speech/tts/ProminenceType;

    .line 39
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Prominence$Builder;
    .locals 1

    .prologue
    .line 140
    # invokes: Lcom/google/speech/tts/Prominence$Builder;->create()Lcom/google/speech/tts/Prominence$Builder;
    invoke-static {}, Lcom/google/speech/tts/Prominence$Builder;->access$100()Lcom/google/speech/tts/Prominence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Prominence;

    .prologue
    .line 143
    invoke-static {}, Lcom/google/speech/tts/Prominence;->newBuilder()Lcom/google/speech/tts/Prominence$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Prominence$Builder;->mergeFrom(Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Prominence;->getDefaultInstanceForType()Lcom/google/speech/tts/Prominence;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Prominence;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Prominence;->defaultInstance:Lcom/google/speech/tts/Prominence;

    return-object v0
.end method

.method public getProminenceType()Lcom/google/speech/tts/ProminenceType;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/Prominence;->prominenceType_:Lcom/google/speech/tts/ProminenceType;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 57
    iget v0, p0, Lcom/google/speech/tts/Prominence;->memoizedSerializedSize:I

    .line 58
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 70
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 60
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 61
    invoke-virtual {p0}, Lcom/google/speech/tts/Prominence;->hasWordProminence()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Prominence;->getWordProminence()Lcom/google/speech/tts/ProminenceLevel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/ProminenceLevel;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 65
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Prominence;->hasProminenceType()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 66
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Prominence;->getProminenceType()Lcom/google/speech/tts/ProminenceType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/ProminenceType;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 69
    :cond_2
    iput v0, p0, Lcom/google/speech/tts/Prominence;->memoizedSerializedSize:I

    move v1, v0

    .line 70
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getWordProminence()Lcom/google/speech/tts/ProminenceLevel;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Prominence;->wordProminence_:Lcom/google/speech/tts/ProminenceLevel;

    return-object v0
.end method

.method public hasProminenceType()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Prominence;->hasProminenceType:Z

    return v0
.end method

.method public hasWordProminence()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Prominence;->hasWordProminence:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Prominence;->toBuilder()Lcom/google/speech/tts/Prominence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Prominence$Builder;
    .locals 1

    .prologue
    .line 145
    invoke-static {p0}, Lcom/google/speech/tts/Prominence;->newBuilder(Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/speech/tts/Prominence;->getSerializedSize()I

    .line 47
    invoke-virtual {p0}, Lcom/google/speech/tts/Prominence;->hasWordProminence()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Prominence;->getWordProminence()Lcom/google/speech/tts/ProminenceLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/ProminenceLevel;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Prominence;->hasProminenceType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Prominence;->getProminenceType()Lcom/google/speech/tts/ProminenceType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/ProminenceType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 53
    :cond_1
    return-void
.end method
