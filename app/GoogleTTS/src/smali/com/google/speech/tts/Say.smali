.class public final Lcom/google/speech/tts/Say;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Say.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Say$1;,
        Lcom/google/speech/tts/Say$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Say;


# instance fields
.field private address_:Lcom/google/speech/tts/Address;

.field private cardinal_:Lcom/google/speech/tts/Cardinal;

.field private date_:Lcom/google/speech/tts/Date;

.field private decimal_:Lcom/google/speech/tts/Decimal;

.field private digit_:Ljava/lang/String;

.field private electronic_:Lcom/google/speech/tts/Electronic;

.field private fraction_:Lcom/google/speech/tts/Fraction;

.field private hasAddress:Z

.field private hasCardinal:Z

.field private hasDate:Z

.field private hasDecimal:Z

.field private hasDigit:Z

.field private hasElectronic:Z

.field private hasFraction:Z

.field private hasLetters:Z

.field private hasLocation:Z

.field private hasMeasure:Z

.field private hasMoney:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasOrdinal:Z

.field private hasPause:Z

.field private hasProsodicFeatures:Z

.field private hasTelephone:Z

.field private hasText:Z

.field private hasTime:Z

.field private hasVerbatim:Z

.field private hasVoicemod:Z

.field private hasWords:Z

.field private letters_:Ljava/lang/String;

.field private location_:Llocation/unified/LocationDescriptor;

.field private measure_:Lcom/google/speech/tts/Measure;

.field private memoizedSerializedSize:I

.field private money_:Lcom/google/speech/tts/Money;

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private ordinal_:Lcom/google/speech/tts/Ordinal;

.field private pause_:F

.field private prosodicFeatures_:Lcom/google/speech/tts/ProsodicFeatures;

.field private telephone_:Lcom/google/speech/tts/Telephone;

.field private text_:Ljava/lang/String;

.field private time_:Lcom/google/speech/tts/Time;

.field private verbatim_:Ljava/lang/String;

.field private voicemod_:Lcom/google/speech/tts/VoiceMod;

.field private wordSequence_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/MarkupWord;",
            ">;"
        }
    .end annotation
.end field

.field private words_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1504
    new-instance v0, Lcom/google/speech/tts/Say;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Say;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Say;->defaultInstance:Lcom/google/speech/tts/Say;

    .line 1505
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 1506
    sget-object v0, Lcom/google/speech/tts/Say;->defaultInstance:Lcom/google/speech/tts/Say;

    invoke-direct {v0}, Lcom/google/speech/tts/Say;->initFields()V

    .line 1507
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Say;->text_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Say;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Say;->digit_:Ljava/lang/String;

    .line 123
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Say;->words_:Ljava/lang/String;

    .line 129
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;

    .line 142
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Say;->letters_:Ljava/lang/String;

    .line 149
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Say;->verbatim_:Ljava/lang/String;

    .line 163
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/tts/Say;->pause_:F

    .line 293
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Say;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Say;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Say$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Say$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Say;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Say;->text_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Say;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Say;->digit_:Ljava/lang/String;

    .line 123
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Say;->words_:Ljava/lang/String;

    .line 129
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;

    .line 142
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Say;->letters_:Ljava/lang/String;

    .line 149
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Say;->verbatim_:Ljava/lang/String;

    .line 163
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/tts/Say;->pause_:F

    .line 293
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Say;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasOrdinal:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Ordinal;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->ordinal_:Lcom/google/speech/tts/Ordinal;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/Ordinal;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->ordinal_:Lcom/google/speech/tts/Ordinal;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasDecimal:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Decimal;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->decimal_:Lcom/google/speech/tts/Decimal;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->decimal_:Lcom/google/speech/tts/Decimal;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasFraction:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Fraction;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->fraction_:Lcom/google/speech/tts/Fraction;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/Fraction;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->fraction_:Lcom/google/speech/tts/Fraction;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasDigit:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/Say;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->digit_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasTime:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Time;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->time_:Lcom/google/speech/tts/Time;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/Time;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->time_:Lcom/google/speech/tts/Time;

    return-object p1
.end method

.method static synthetic access$2002(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasMeasure:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Measure;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->measure_:Lcom/google/speech/tts/Measure;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/Measure;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->measure_:Lcom/google/speech/tts/Measure;

    return-object p1
.end method

.method static synthetic access$2202(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasDate:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Date;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->date_:Lcom/google/speech/tts/Date;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/Date;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->date_:Lcom/google/speech/tts/Date;

    return-object p1
.end method

.method static synthetic access$2402(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasAddress:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Address;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->address_:Lcom/google/speech/tts/Address;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/Address;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->address_:Lcom/google/speech/tts/Address;

    return-object p1
.end method

.method static synthetic access$2602(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasTelephone:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Telephone;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->telephone_:Lcom/google/speech/tts/Telephone;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/Telephone;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->telephone_:Lcom/google/speech/tts/Telephone;

    return-object p1
.end method

.method static synthetic access$2802(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasMoney:Z

    return p1
.end method

.method static synthetic access$2900(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Money;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->money_:Lcom/google/speech/tts/Money;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/Money;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->money_:Lcom/google/speech/tts/Money;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Say;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasElectronic:Z

    return p1
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Say;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Electronic;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->electronic_:Lcom/google/speech/tts/Electronic;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/Electronic;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->electronic_:Lcom/google/speech/tts/Electronic;

    return-object p1
.end method

.method static synthetic access$3202(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasWords:Z

    return p1
.end method

.method static synthetic access$3302(Lcom/google/speech/tts/Say;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->words_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3402(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasLetters:Z

    return p1
.end method

.method static synthetic access$3502(Lcom/google/speech/tts/Say;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->letters_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3602(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasVerbatim:Z

    return p1
.end method

.method static synthetic access$3702(Lcom/google/speech/tts/Say;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->verbatim_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3802(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasVoicemod:Z

    return p1
.end method

.method static synthetic access$3900(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/VoiceMod;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/VoiceMod;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;

    return-object p1
.end method

.method static synthetic access$4002(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasPause:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasText:Z

    return p1
.end method

.method static synthetic access$4102(Lcom/google/speech/tts/Say;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Say;->pause_:F

    return p1
.end method

.method static synthetic access$4202(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasProsodicFeatures:Z

    return p1
.end method

.method static synthetic access$4300(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/ProsodicFeatures;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->prosodicFeatures_:Lcom/google/speech/tts/ProsodicFeatures;

    return-object v0
.end method

.method static synthetic access$4302(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/ProsodicFeatures;)Lcom/google/speech/tts/ProsodicFeatures;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/ProsodicFeatures;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->prosodicFeatures_:Lcom/google/speech/tts/ProsodicFeatures;

    return-object p1
.end method

.method static synthetic access$4402(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasLocation:Z

    return p1
.end method

.method static synthetic access$4500(Lcom/google/speech/tts/Say;)Llocation/unified/LocationDescriptor;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->location_:Llocation/unified/LocationDescriptor;

    return-object v0
.end method

.method static synthetic access$4502(Lcom/google/speech/tts/Say;Llocation/unified/LocationDescriptor;)Llocation/unified/LocationDescriptor;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Llocation/unified/LocationDescriptor;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->location_:Llocation/unified/LocationDescriptor;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Say;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->text_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Say;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Say;->hasCardinal:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Cardinal;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Say;->cardinal_:Lcom/google/speech/tts/Cardinal;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Say;Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/Cardinal;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Say;->cardinal_:Lcom/google/speech/tts/Cardinal;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Say;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Say;->defaultInstance:Lcom/google/speech/tts/Say;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 182
    invoke-static {}, Lcom/google/speech/tts/Cardinal;->getDefaultInstance()Lcom/google/speech/tts/Cardinal;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->cardinal_:Lcom/google/speech/tts/Cardinal;

    .line 183
    invoke-static {}, Lcom/google/speech/tts/Ordinal;->getDefaultInstance()Lcom/google/speech/tts/Ordinal;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->ordinal_:Lcom/google/speech/tts/Ordinal;

    .line 184
    invoke-static {}, Lcom/google/speech/tts/Decimal;->getDefaultInstance()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->decimal_:Lcom/google/speech/tts/Decimal;

    .line 185
    invoke-static {}, Lcom/google/speech/tts/Fraction;->getDefaultInstance()Lcom/google/speech/tts/Fraction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->fraction_:Lcom/google/speech/tts/Fraction;

    .line 186
    invoke-static {}, Lcom/google/speech/tts/Time;->getDefaultInstance()Lcom/google/speech/tts/Time;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->time_:Lcom/google/speech/tts/Time;

    .line 187
    invoke-static {}, Lcom/google/speech/tts/Measure;->getDefaultInstance()Lcom/google/speech/tts/Measure;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->measure_:Lcom/google/speech/tts/Measure;

    .line 188
    invoke-static {}, Lcom/google/speech/tts/Date;->getDefaultInstance()Lcom/google/speech/tts/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->date_:Lcom/google/speech/tts/Date;

    .line 189
    invoke-static {}, Lcom/google/speech/tts/Address;->getDefaultInstance()Lcom/google/speech/tts/Address;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->address_:Lcom/google/speech/tts/Address;

    .line 190
    invoke-static {}, Lcom/google/speech/tts/Telephone;->getDefaultInstance()Lcom/google/speech/tts/Telephone;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->telephone_:Lcom/google/speech/tts/Telephone;

    .line 191
    invoke-static {}, Lcom/google/speech/tts/Money;->getDefaultInstance()Lcom/google/speech/tts/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->money_:Lcom/google/speech/tts/Money;

    .line 192
    invoke-static {}, Lcom/google/speech/tts/Electronic;->getDefaultInstance()Lcom/google/speech/tts/Electronic;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->electronic_:Lcom/google/speech/tts/Electronic;

    .line 193
    invoke-static {}, Lcom/google/speech/tts/VoiceMod;->getDefaultInstance()Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;

    .line 194
    invoke-static {}, Lcom/google/speech/tts/ProsodicFeatures;->getDefaultInstance()Lcom/google/speech/tts/ProsodicFeatures;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->prosodicFeatures_:Lcom/google/speech/tts/ProsodicFeatures;

    .line 195
    invoke-static {}, Llocation/unified/LocationDescriptor;->getDefaultInstance()Llocation/unified/LocationDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Say;->location_:Llocation/unified/LocationDescriptor;

    .line 196
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Say$Builder;
    .locals 1

    .prologue
    .line 458
    # invokes: Lcom/google/speech/tts/Say$Builder;->create()Lcom/google/speech/tts/Say$Builder;
    invoke-static {}, Lcom/google/speech/tts/Say$Builder;->access$100()Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Say$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Say;

    .prologue
    .line 461
    invoke-static {}, Lcom/google/speech/tts/Say;->newBuilder()Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Say$Builder;->mergeFrom(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAddress()Lcom/google/speech/tts/Address;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/speech/tts/Say;->address_:Lcom/google/speech/tts/Address;

    return-object v0
.end method

.method public getCardinal()Lcom/google/speech/tts/Cardinal;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/Say;->cardinal_:Lcom/google/speech/tts/Cardinal;

    return-object v0
.end method

.method public getDate()Lcom/google/speech/tts/Date;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/speech/tts/Say;->date_:Lcom/google/speech/tts/Date;

    return-object v0
.end method

.method public getDecimal()Lcom/google/speech/tts/Decimal;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/tts/Say;->decimal_:Lcom/google/speech/tts/Decimal;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getDefaultInstanceForType()Lcom/google/speech/tts/Say;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Say;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Say;->defaultInstance:Lcom/google/speech/tts/Say;

    return-object v0
.end method

.method public getDigit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/speech/tts/Say;->digit_:Ljava/lang/String;

    return-object v0
.end method

.method public getElectronic()Lcom/google/speech/tts/Electronic;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/speech/tts/Say;->electronic_:Lcom/google/speech/tts/Electronic;

    return-object v0
.end method

.method public getFraction()Lcom/google/speech/tts/Fraction;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/speech/tts/Say;->fraction_:Lcom/google/speech/tts/Fraction;

    return-object v0
.end method

.method public getLetters()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/speech/tts/Say;->letters_:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Llocation/unified/LocationDescriptor;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/speech/tts/Say;->location_:Llocation/unified/LocationDescriptor;

    return-object v0
.end method

.method public getMeasure()Lcom/google/speech/tts/Measure;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/speech/tts/Say;->measure_:Lcom/google/speech/tts/Measure;

    return-object v0
.end method

.method public getMoney()Lcom/google/speech/tts/Money;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/speech/tts/Say;->money_:Lcom/google/speech/tts/Money;

    return-object v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/Say;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getOrdinal()Lcom/google/speech/tts/Ordinal;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/tts/Say;->ordinal_:Lcom/google/speech/tts/Ordinal;

    return-object v0
.end method

.method public getPause()F
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/google/speech/tts/Say;->pause_:F

    return v0
.end method

.method public getProsodicFeatures()Lcom/google/speech/tts/ProsodicFeatures;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/speech/tts/Say;->prosodicFeatures_:Lcom/google/speech/tts/ProsodicFeatures;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 295
    iget v2, p0, Lcom/google/speech/tts/Say;->memoizedSerializedSize:I

    .line 296
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 388
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 298
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 299
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasText()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 300
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 303
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasMorphosyntacticFeatures()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 304
    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 307
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasCardinal()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 308
    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getCardinal()Lcom/google/speech/tts/Cardinal;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 311
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasOrdinal()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 312
    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getOrdinal()Lcom/google/speech/tts/Ordinal;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 315
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasDecimal()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 316
    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getDecimal()Lcom/google/speech/tts/Decimal;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 319
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasFraction()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 320
    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getFraction()Lcom/google/speech/tts/Fraction;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 323
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasTime()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 324
    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getTime()Lcom/google/speech/tts/Time;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 327
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasMeasure()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 328
    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getMeasure()Lcom/google/speech/tts/Measure;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 331
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasDate()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 332
    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getDate()Lcom/google/speech/tts/Date;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 335
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasAddress()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 336
    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getAddress()Lcom/google/speech/tts/Address;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 339
    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasTelephone()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 340
    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getTelephone()Lcom/google/speech/tts/Telephone;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 343
    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasMoney()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 344
    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getMoney()Lcom/google/speech/tts/Money;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 347
    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasElectronic()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 348
    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getElectronic()Lcom/google/speech/tts/Electronic;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 351
    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasDigit()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 352
    const/16 v4, 0xf

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getDigit()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 355
    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasWords()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 356
    const/16 v4, 0x10

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getWords()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 359
    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getWordSequenceList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/MarkupWord;

    .line 360
    .local v0, "element":Lcom/google/speech/tts/MarkupWord;
    const/16 v4, 0x11

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 362
    goto :goto_1

    .line 363
    .end local v0    # "element":Lcom/google/speech/tts/MarkupWord;
    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasLetters()Z

    move-result v4

    if-eqz v4, :cond_11

    .line 364
    const/16 v4, 0x12

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getLetters()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 367
    :cond_11
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasVerbatim()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 368
    const/16 v4, 0x13

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getVerbatim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 371
    :cond_12
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasVoicemod()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 372
    const/16 v4, 0x14

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getVoicemod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 375
    :cond_13
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasPause()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 376
    const/16 v4, 0x15

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getPause()F

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v4

    add-int/2addr v2, v4

    .line 379
    :cond_14
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasProsodicFeatures()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 380
    const/16 v4, 0x16

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getProsodicFeatures()Lcom/google/speech/tts/ProsodicFeatures;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 383
    :cond_15
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasLocation()Z

    move-result v4

    if-eqz v4, :cond_16

    .line 384
    const/16 v4, 0x17

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getLocation()Llocation/unified/LocationDescriptor;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 387
    :cond_16
    iput v2, p0, Lcom/google/speech/tts/Say;->memoizedSerializedSize:I

    move v3, v2

    .line 388
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto/16 :goto_0
.end method

.method public getTelephone()Lcom/google/speech/tts/Telephone;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/speech/tts/Say;->telephone_:Lcom/google/speech/tts/Telephone;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Say;->text_:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()Lcom/google/speech/tts/Time;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/speech/tts/Say;->time_:Lcom/google/speech/tts/Time;

    return-object v0
.end method

.method public getVerbatim()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/speech/tts/Say;->verbatim_:Ljava/lang/String;

    return-object v0
.end method

.method public getVoicemod()Lcom/google/speech/tts/VoiceMod;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/speech/tts/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;

    return-object v0
.end method

.method public getWordSequenceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/MarkupWord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/speech/tts/Say;->wordSequence_:Ljava/util/List;

    return-object v0
.end method

.method public getWords()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/speech/tts/Say;->words_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAddress()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasAddress:Z

    return v0
.end method

.method public hasCardinal()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasCardinal:Z

    return v0
.end method

.method public hasDate()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasDate:Z

    return v0
.end method

.method public hasDecimal()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasDecimal:Z

    return v0
.end method

.method public hasDigit()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasDigit:Z

    return v0
.end method

.method public hasElectronic()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasElectronic:Z

    return v0
.end method

.method public hasFraction()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasFraction:Z

    return v0
.end method

.method public hasLetters()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasLetters:Z

    return v0
.end method

.method public hasLocation()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasLocation:Z

    return v0
.end method

.method public hasMeasure()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasMeasure:Z

    return v0
.end method

.method public hasMoney()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasMoney:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasOrdinal()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasOrdinal:Z

    return v0
.end method

.method public hasPause()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasPause:Z

    return v0
.end method

.method public hasProsodicFeatures()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasProsodicFeatures:Z

    return v0
.end method

.method public hasTelephone()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasTelephone:Z

    return v0
.end method

.method public hasText()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasText:Z

    return v0
.end method

.method public hasTime()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasTime:Z

    return v0
.end method

.method public hasVerbatim()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasVerbatim:Z

    return v0
.end method

.method public hasVoicemod()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasVoicemod:Z

    return v0
.end method

.method public hasWords()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/speech/tts/Say;->hasWords:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 198
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasCardinal()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 199
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getCardinal()Lcom/google/speech/tts/Cardinal;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/Cardinal;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1

    .line 219
    :cond_0
    :goto_0
    return v2

    .line 201
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasOrdinal()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 202
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getOrdinal()Lcom/google/speech/tts/Ordinal;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/Ordinal;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 204
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasFraction()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 205
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getFraction()Lcom/google/speech/tts/Fraction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/Fraction;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 207
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasMeasure()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 208
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getMeasure()Lcom/google/speech/tts/Measure;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/Measure;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 210
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasMoney()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 211
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getMoney()Lcom/google/speech/tts/Money;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/Money;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 213
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getWordSequenceList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/MarkupWord;

    .line 214
    .local v0, "element":Lcom/google/speech/tts/MarkupWord;
    invoke-virtual {v0}, Lcom/google/speech/tts/MarkupWord;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_6

    goto :goto_0

    .line 216
    .end local v0    # "element":Lcom/google/speech/tts/MarkupWord;
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasVoicemod()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 217
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getVoicemod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/VoiceMod;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 219
    :cond_8
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->toBuilder()Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Say$Builder;
    .locals 1

    .prologue
    .line 463
    invoke-static {p0}, Lcom/google/speech/tts/Say;->newBuilder(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getSerializedSize()I

    .line 225
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasText()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 226
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 228
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 229
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 231
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasCardinal()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 232
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getCardinal()Lcom/google/speech/tts/Cardinal;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 234
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasOrdinal()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 235
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getOrdinal()Lcom/google/speech/tts/Ordinal;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 237
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasDecimal()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 238
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getDecimal()Lcom/google/speech/tts/Decimal;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 240
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasFraction()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 241
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getFraction()Lcom/google/speech/tts/Fraction;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 243
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasTime()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 244
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getTime()Lcom/google/speech/tts/Time;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 246
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasMeasure()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 247
    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getMeasure()Lcom/google/speech/tts/Measure;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 249
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasDate()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 250
    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getDate()Lcom/google/speech/tts/Date;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 252
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasAddress()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 253
    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getAddress()Lcom/google/speech/tts/Address;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 255
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasTelephone()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 256
    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getTelephone()Lcom/google/speech/tts/Telephone;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 258
    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasMoney()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 259
    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getMoney()Lcom/google/speech/tts/Money;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 261
    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasElectronic()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 262
    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getElectronic()Lcom/google/speech/tts/Electronic;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 264
    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasDigit()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 265
    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getDigit()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 267
    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasWords()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 268
    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getWords()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 270
    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getWordSequenceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/MarkupWord;

    .line 271
    .local v0, "element":Lcom/google/speech/tts/MarkupWord;
    const/16 v2, 0x11

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 273
    .end local v0    # "element":Lcom/google/speech/tts/MarkupWord;
    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasLetters()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 274
    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getLetters()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 276
    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasVerbatim()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 277
    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getVerbatim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 279
    :cond_11
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasVoicemod()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 280
    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getVoicemod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 282
    :cond_12
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasPause()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 283
    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getPause()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 285
    :cond_13
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasProsodicFeatures()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 286
    const/16 v2, 0x16

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getProsodicFeatures()Lcom/google/speech/tts/ProsodicFeatures;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 288
    :cond_14
    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->hasLocation()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 289
    const/16 v2, 0x17

    invoke-virtual {p0}, Lcom/google/speech/tts/Say;->getLocation()Llocation/unified/LocationDescriptor;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 291
    :cond_15
    return-void
.end method
