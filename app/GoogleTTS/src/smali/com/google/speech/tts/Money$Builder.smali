.class public final Lcom/google/speech/tts/Money$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Money.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Money;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Money;",
        "Lcom/google/speech/tts/Money$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Money;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 248
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Money$Builder;
    .locals 1

    .prologue
    .line 242
    invoke-static {}, Lcom/google/speech/tts/Money$Builder;->create()Lcom/google/speech/tts/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Money$Builder;
    .locals 3

    .prologue
    .line 251
    new-instance v0, Lcom/google/speech/tts/Money$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Money$Builder;-><init>()V

    .line 252
    .local v0, "builder":Lcom/google/speech/tts/Money$Builder;
    new-instance v1, Lcom/google/speech/tts/Money;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Money;-><init>(Lcom/google/speech/tts/Money$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    .line 253
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/speech/tts/Money$Builder;->build()Lcom/google/speech/tts/Money;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Money;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Money$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    invoke-static {v0}, Lcom/google/speech/tts/Money$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 284
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Money$Builder;->buildPartial()Lcom/google/speech/tts/Money;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Money;
    .locals 3

    .prologue
    .line 297
    iget-object v1, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    if-nez v1, :cond_0

    .line 298
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 301
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # getter for: Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Money;->access$300(Lcom/google/speech/tts/Money;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 302
    iget-object v1, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    iget-object v2, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # getter for: Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Money;->access$300(Lcom/google/speech/tts/Money;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Money;->access$302(Lcom/google/speech/tts/Money;Ljava/util/List;)Ljava/util/List;

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    .line 306
    .local v0, "returnMe":Lcom/google/speech/tts/Money;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    .line 307
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/speech/tts/Money$Builder;->clone()Lcom/google/speech/tts/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/speech/tts/Money$Builder;->clone()Lcom/google/speech/tts/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Money$Builder;
    .locals 2

    .prologue
    .line 270
    invoke-static {}, Lcom/google/speech/tts/Money$Builder;->create()Lcom/google/speech/tts/Money$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Money$Builder;->mergeFrom(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/speech/tts/Money$Builder;->clone()Lcom/google/speech/tts/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    invoke-virtual {v0}, Lcom/google/speech/tts/Money;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeAmount(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Money$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    invoke-virtual {v0}, Lcom/google/speech/tts/Money;->hasAmount()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # getter for: Lcom/google/speech/tts/Money;->amount_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0}, Lcom/google/speech/tts/Money;->access$500(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Decimal;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Decimal;->getDefaultInstance()Lcom/google/speech/tts/Decimal;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 422
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    iget-object v1, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # getter for: Lcom/google/speech/tts/Money;->amount_:Lcom/google/speech/tts/Decimal;
    invoke-static {v1}, Lcom/google/speech/tts/Money;->access$500(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Decimal;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Decimal;->newBuilder(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Decimal$Builder;->mergeFrom(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Decimal$Builder;->buildPartial()Lcom/google/speech/tts/Decimal;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Money;->amount_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Money;->access$502(Lcom/google/speech/tts/Money;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;

    .line 427
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Money;->hasAmount:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Money;->access$402(Lcom/google/speech/tts/Money;Z)Z

    .line 428
    return-object p0

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # setter for: Lcom/google/speech/tts/Money;->amount_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Money;->access$502(Lcom/google/speech/tts/Money;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 242
    check-cast p1, Lcom/google/speech/tts/Money;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Money$Builder;->mergeFrom(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Money;

    .prologue
    .line 311
    invoke-static {}, Lcom/google/speech/tts/Money;->getDefaultInstance()Lcom/google/speech/tts/Money;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 339
    :cond_0
    :goto_0
    return-object p0

    .line 312
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->hasAmount()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 313
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->getAmount()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Money$Builder;->mergeAmount(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Money$Builder;

    .line 315
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->hasQuantity()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 316
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->getQuantity()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/tts/Money$Builder;->setQuantity(J)Lcom/google/speech/tts/Money$Builder;

    .line 318
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->hasCurrency()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 319
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->getCurrency()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Money$Builder;->setCurrency(Ljava/lang/String;)Lcom/google/speech/tts/Money$Builder;

    .line 321
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 322
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->getStyle()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Money$Builder;->setStyle(I)Lcom/google/speech/tts/Money$Builder;

    .line 324
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 325
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Money$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Money$Builder;

    .line 327
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->hasPreserveOrder()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 328
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->getPreserveOrder()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Money$Builder;->setPreserveOrder(Z)Lcom/google/speech/tts/Money$Builder;

    .line 330
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 331
    invoke-virtual {p1}, Lcom/google/speech/tts/Money;->getCodeSwitch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Money$Builder;->setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Money$Builder;

    .line 333
    :cond_8
    # getter for: Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Money;->access$300(Lcom/google/speech/tts/Money;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # getter for: Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Money;->access$300(Lcom/google/speech/tts/Money;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 335
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Money;->access$302(Lcom/google/speech/tts/Money;Ljava/util/List;)Ljava/util/List;

    .line 337
    :cond_9
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # getter for: Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Money;->access$300(Lcom/google/speech/tts/Money;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Money;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Money;->access$300(Lcom/google/speech/tts/Money;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Money$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 540
    if-nez p1, :cond_0

    .line 541
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Money;->hasCodeSwitch:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Money;->access$1602(Lcom/google/speech/tts/Money;Z)Z

    .line 544
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # setter for: Lcom/google/speech/tts/Money;->codeSwitch_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Money;->access$1702(Lcom/google/speech/tts/Money;Ljava/lang/String;)Ljava/lang/String;

    .line 545
    return-object p0
.end method

.method public setCurrency(Ljava/lang/String;)Lcom/google/speech/tts/Money$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 462
    if-nez p1, :cond_0

    .line 463
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 465
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Money;->hasCurrency:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Money;->access$802(Lcom/google/speech/tts/Money;Z)Z

    .line 466
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # setter for: Lcom/google/speech/tts/Money;->currency_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Money;->access$902(Lcom/google/speech/tts/Money;Ljava/lang/String;)Ljava/lang/String;

    .line 467
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Money$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 501
    if-nez p1, :cond_0

    .line 502
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Money;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Money;->access$1202(Lcom/google/speech/tts/Money;Z)Z

    .line 505
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # setter for: Lcom/google/speech/tts/Money;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Money;->access$1302(Lcom/google/speech/tts/Money;Ljava/lang/String;)Ljava/lang/String;

    .line 506
    return-object p0
.end method

.method public setPreserveOrder(Z)Lcom/google/speech/tts/Money$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Money;->hasPreserveOrder:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Money;->access$1402(Lcom/google/speech/tts/Money;Z)Z

    .line 523
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # setter for: Lcom/google/speech/tts/Money;->preserveOrder_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Money;->access$1502(Lcom/google/speech/tts/Money;Z)Z

    .line 524
    return-object p0
.end method

.method public setQuantity(J)Lcom/google/speech/tts/Money$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Money;->hasQuantity:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Money;->access$602(Lcom/google/speech/tts/Money;Z)Z

    .line 445
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # setter for: Lcom/google/speech/tts/Money;->quantity_:J
    invoke-static {v0, p1, p2}, Lcom/google/speech/tts/Money;->access$702(Lcom/google/speech/tts/Money;J)J

    .line 446
    return-object p0
.end method

.method public setStyle(I)Lcom/google/speech/tts/Money$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Money;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Money;->access$1002(Lcom/google/speech/tts/Money;Z)Z

    .line 484
    iget-object v0, p0, Lcom/google/speech/tts/Money$Builder;->result:Lcom/google/speech/tts/Money;

    # setter for: Lcom/google/speech/tts/Money;->style_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Money;->access$1102(Lcom/google/speech/tts/Money;I)I

    .line 485
    return-object p0
.end method
