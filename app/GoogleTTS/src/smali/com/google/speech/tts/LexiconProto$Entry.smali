.class public final Lcom/google/speech/tts/LexiconProto$Entry;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Entry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/LexiconProto$Entry$Builder;,
        Lcom/google/speech/tts/LexiconProto$Entry$Syntax;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/LexiconProto$Entry;


# instance fields
.field private c_:Ljava/lang/String;

.field private hasC:Z

.field private hasId:Z

.field private hasM:Z

.field private hasSyn:Z

.field private id_:Ljava/lang/String;

.field private m_:Lcom/google/speech/tts/LexiconProto$Morphology;

.field private memoizedSerializedSize:I

.field private p_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Pronunciation;",
            ">;"
        }
    .end annotation
.end field

.field private s_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Spelling;",
            ">;"
        }
    .end annotation
.end field

.field private syn_:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2602
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Entry;

    .line 2603
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 2604
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Entry;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Entry;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->initFields()V

    .line 2605
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1958
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2042
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->id_:Ljava/lang/String;

    .line 2048
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;

    .line 2060
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;

    .line 2073
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->c_:Ljava/lang/String;

    .line 2130
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->memoizedSerializedSize:I

    .line 1959
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->initFields()V

    .line 1960
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/LexiconProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/LexiconProto$1;

    .prologue
    .line 1955
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto$Entry;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 1961
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2042
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->id_:Ljava/lang/String;

    .line 2048
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;

    .line 2060
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;

    .line 2073
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->c_:Ljava/lang/String;

    .line 2130
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->memoizedSerializedSize:I

    .line 1961
    return-void
.end method

.method static synthetic access$4100(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;

    .prologue
    .line 1955
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4102(Lcom/google/speech/tts/LexiconProto$Entry;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 1955
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$4200(Lcom/google/speech/tts/LexiconProto$Entry;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;

    .prologue
    .line 1955
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4202(Lcom/google/speech/tts/LexiconProto$Entry;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 1955
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$4302(Lcom/google/speech/tts/LexiconProto$Entry;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;
    .param p1, "x1"    # Z

    .prologue
    .line 1955
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Entry;->hasId:Z

    return p1
.end method

.method static synthetic access$4402(Lcom/google/speech/tts/LexiconProto$Entry;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 1955
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Entry;->id_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4502(Lcom/google/speech/tts/LexiconProto$Entry;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;
    .param p1, "x1"    # Z

    .prologue
    .line 1955
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Entry;->hasC:Z

    return p1
.end method

.method static synthetic access$4602(Lcom/google/speech/tts/LexiconProto$Entry;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 1955
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Entry;->c_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4702(Lcom/google/speech/tts/LexiconProto$Entry;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;
    .param p1, "x1"    # Z

    .prologue
    .line 1955
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Entry;->hasSyn:Z

    return p1
.end method

.method static synthetic access$4802(Lcom/google/speech/tts/LexiconProto$Entry;Lcom/google/speech/tts/LexiconProto$Entry$Syntax;)Lcom/google/speech/tts/LexiconProto$Entry$Syntax;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;
    .param p1, "x1"    # Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .prologue
    .line 1955
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Entry;->syn_:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    return-object p1
.end method

.method static synthetic access$4902(Lcom/google/speech/tts/LexiconProto$Entry;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;
    .param p1, "x1"    # Z

    .prologue
    .line 1955
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Entry;->hasM:Z

    return p1
.end method

.method static synthetic access$5000(Lcom/google/speech/tts/LexiconProto$Entry;)Lcom/google/speech/tts/LexiconProto$Morphology;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;

    .prologue
    .line 1955
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->m_:Lcom/google/speech/tts/LexiconProto$Morphology;

    return-object v0
.end method

.method static synthetic access$5002(Lcom/google/speech/tts/LexiconProto$Entry;Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Morphology;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Entry;
    .param p1, "x1"    # Lcom/google/speech/tts/LexiconProto$Morphology;

    .prologue
    .line 1955
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Entry;->m_:Lcom/google/speech/tts/LexiconProto$Morphology;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/LexiconProto$Entry;
    .locals 1

    .prologue
    .line 1965
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Entry;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Entry;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 2092
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->UNSPECIFIED:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->syn_:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 2093
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Morphology;->getDefaultInstance()Lcom/google/speech/tts/LexiconProto$Morphology;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->m_:Lcom/google/speech/tts/LexiconProto$Morphology;

    .line 2094
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 1

    .prologue
    .line 2231
    # invokes: Lcom/google/speech/tts/LexiconProto$Entry$Builder;->create()Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->access$3900()Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/LexiconProto$Entry;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/LexiconProto$Entry;

    .prologue
    .line 2234
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Entry;->newBuilder()Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Entry;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2075
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->c_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto$Entry;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto$Entry;
    .locals 1

    .prologue
    .line 1969
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Entry;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Entry;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2044
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getM()Lcom/google/speech/tts/LexiconProto$Morphology;
    .locals 1

    .prologue
    .line 2089
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->m_:Lcom/google/speech/tts/LexiconProto$Morphology;

    return-object v0
.end method

.method public getPList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Pronunciation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2051
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->p_:Ljava/util/List;

    return-object v0
.end method

.method public getSList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Spelling;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2063
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->s_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 2132
    iget v2, p0, Lcom/google/speech/tts/LexiconProto$Entry;->memoizedSerializedSize:I

    .line 2133
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 2161
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 2135
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 2136
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->hasId()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2137
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2140
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getPList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Pronunciation;

    .line 2141
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Pronunciation;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2143
    goto :goto_1

    .line 2144
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Pronunciation;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getSList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Spelling;

    .line 2145
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Spelling;
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2147
    goto :goto_2

    .line 2148
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Spelling;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->hasC()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2149
    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getC()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2152
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->hasSyn()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2153
    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getSyn()Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 2156
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->hasM()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2157
    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getM()Lcom/google/speech/tts/LexiconProto$Morphology;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2160
    :cond_6
    iput v2, p0, Lcom/google/speech/tts/LexiconProto$Entry;->memoizedSerializedSize:I

    move v3, v2

    .line 2161
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getSyn()Lcom/google/speech/tts/LexiconProto$Entry$Syntax;
    .locals 1

    .prologue
    .line 2082
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->syn_:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    return-object v0
.end method

.method public hasC()Z
    .locals 1

    .prologue
    .line 2074
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->hasC:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 2043
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->hasId:Z

    return v0
.end method

.method public hasM()Z
    .locals 1

    .prologue
    .line 2088
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->hasM:Z

    return v0
.end method

.method public hasSyn()Z
    .locals 1

    .prologue
    .line 2081
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Entry;->hasSyn:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2096
    iget-boolean v3, p0, Lcom/google/speech/tts/LexiconProto$Entry;->hasId:Z

    if-nez v3, :cond_1

    .line 2104
    :cond_0
    :goto_0
    return v2

    .line 2097
    :cond_1
    iget-boolean v3, p0, Lcom/google/speech/tts/LexiconProto$Entry;->hasC:Z

    if-eqz v3, :cond_0

    .line 2098
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getPList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Pronunciation;

    .line 2099
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Pronunciation;
    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 2101
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Pronunciation;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getSList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Spelling;

    .line 2102
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Spelling;
    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$Spelling;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_4

    goto :goto_0

    .line 2104
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Spelling;
    :cond_5
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->toBuilder()Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/LexiconProto$Entry$Builder;
    .locals 1

    .prologue
    .line 2236
    invoke-static {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->newBuilder(Lcom/google/speech/tts/LexiconProto$Entry;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2109
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getSerializedSize()I

    .line 2110
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->hasId()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2111
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 2113
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getPList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Pronunciation;

    .line 2114
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Pronunciation;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 2116
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Pronunciation;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getSList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Spelling;

    .line 2117
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Spelling;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 2119
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Spelling;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->hasC()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2120
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getC()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 2122
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->hasSyn()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2123
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getSyn()Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 2125
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->hasM()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2126
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Entry;->getM()Lcom/google/speech/tts/LexiconProto$Morphology;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 2128
    :cond_5
    return-void
.end method
