.class public final Lcom/google/speech/tts/Time;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Time.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Time$1;,
        Lcom/google/speech/tts/Time$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Time;


# instance fields
.field private codeSwitch_:Ljava/lang/String;

.field private fieldOrder_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasCodeSwitch:Z

.field private hasHours:Z

.field private hasMinutes:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasPreserveOrder:Z

.field private hasSeconds:Z

.field private hasSpeakPeriod:Z

.field private hasStyle:Z

.field private hasSuffix:Z

.field private hasZone:Z

.field private hours_:I

.field private memoizedSerializedSize:I

.field private minutes_:I

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private preserveOrder_:Z

.field private seconds_:I

.field private speakPeriod_:Z

.field private style_:I

.field private suffix_:Ljava/lang/String;

.field private zone_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 690
    new-instance v0, Lcom/google/speech/tts/Time;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Time;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Time;->defaultInstance:Lcom/google/speech/tts/Time;

    .line 691
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 692
    sget-object v0, Lcom/google/speech/tts/Time;->defaultInstance:Lcom/google/speech/tts/Time;

    invoke-direct {v0}, Lcom/google/speech/tts/Time;->initFields()V

    .line 693
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v1, p0, Lcom/google/speech/tts/Time;->hours_:I

    .line 32
    iput v1, p0, Lcom/google/speech/tts/Time;->minutes_:I

    .line 39
    iput v1, p0, Lcom/google/speech/tts/Time;->seconds_:I

    .line 46
    iput-boolean v1, p0, Lcom/google/speech/tts/Time;->speakPeriod_:Z

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Time;->suffix_:Ljava/lang/String;

    .line 60
    iput v1, p0, Lcom/google/speech/tts/Time;->style_:I

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Time;->zone_:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Time;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 81
    iput-boolean v1, p0, Lcom/google/speech/tts/Time;->preserveOrder_:Z

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Time;->codeSwitch_:Ljava/lang/String;

    .line 94
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;

    .line 148
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Time;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Time;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Time$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Time$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Time;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v1, p0, Lcom/google/speech/tts/Time;->hours_:I

    .line 32
    iput v1, p0, Lcom/google/speech/tts/Time;->minutes_:I

    .line 39
    iput v1, p0, Lcom/google/speech/tts/Time;->seconds_:I

    .line 46
    iput-boolean v1, p0, Lcom/google/speech/tts/Time;->speakPeriod_:Z

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Time;->suffix_:Ljava/lang/String;

    .line 60
    iput v1, p0, Lcom/google/speech/tts/Time;->style_:I

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Time;->zone_:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Time;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 81
    iput-boolean v1, p0, Lcom/google/speech/tts/Time;->preserveOrder_:Z

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Time;->codeSwitch_:Ljava/lang/String;

    .line 94
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;

    .line 148
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Time;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Time;->hasSpeakPeriod:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Time;->speakPeriod_:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Time;->hasSuffix:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/Time;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Time;->suffix_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Time;->hasStyle:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/Time;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Time;->style_:I

    return p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Time;->hasZone:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/Time;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Time;->zone_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/google/speech/tts/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Time;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/google/speech/tts/Time;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Time;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2002(Lcom/google/speech/tts/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Time;->hasPreserveOrder:Z

    return p1
.end method

.method static synthetic access$2102(Lcom/google/speech/tts/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Time;->preserveOrder_:Z

    return p1
.end method

.method static synthetic access$2202(Lcom/google/speech/tts/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Time;->hasCodeSwitch:Z

    return p1
.end method

.method static synthetic access$2302(Lcom/google/speech/tts/Time;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Time;->codeSwitch_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Time;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Time;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Time;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Time;->hasHours:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Time;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Time;->hours_:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Time;->hasMinutes:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Time;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Time;->minutes_:I

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Time;->hasSeconds:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Time;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Time;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/Time;->seconds_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Time;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Time;->defaultInstance:Lcom/google/speech/tts/Time;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Time$Builder;
    .locals 1

    .prologue
    .line 274
    # invokes: Lcom/google/speech/tts/Time$Builder;->create()Lcom/google/speech/tts/Time$Builder;
    invoke-static {}, Lcom/google/speech/tts/Time$Builder;->access$100()Lcom/google/speech/tts/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Time;

    .prologue
    .line 277
    invoke-static {}, Lcom/google/speech/tts/Time;->newBuilder()Lcom/google/speech/tts/Time$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Time$Builder;->mergeFrom(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCodeSwitch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/speech/tts/Time;->codeSwitch_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getDefaultInstanceForType()Lcom/google/speech/tts/Time;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Time;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Time;->defaultInstance:Lcom/google/speech/tts/Time;

    return-object v0
.end method

.method public getFieldOrderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method public getHours()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/tts/Time;->hours_:I

    return v0
.end method

.method public getMinutes()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/speech/tts/Time;->minutes_:I

    return v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/speech/tts/Time;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getPreserveOrder()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/google/speech/tts/Time;->preserveOrder_:Z

    return v0
.end method

.method public getSeconds()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/speech/tts/Time;->seconds_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 150
    iget v3, p0, Lcom/google/speech/tts/Time;->memoizedSerializedSize:I

    .line 151
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 204
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 153
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 154
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasHours()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 155
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getHours()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 158
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasMinutes()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 159
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getMinutes()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 162
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasSeconds()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 163
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getSeconds()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 166
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasSpeakPeriod()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 167
    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getSpeakPeriod()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 170
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasSuffix()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 171
    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getSuffix()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 174
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasStyle()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 175
    const/4 v5, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getStyle()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 178
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasZone()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 179
    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getZone()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 182
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasMorphosyntacticFeatures()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 183
    const/16 v5, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 186
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasPreserveOrder()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 187
    const/16 v5, 0xa

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getPreserveOrder()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 190
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasCodeSwitch()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 191
    const/16 v5, 0xb

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getCodeSwitch()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 195
    :cond_a
    const/4 v0, 0x0

    .line 196
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 197
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 199
    goto :goto_1

    .line 200
    .end local v1    # "element":Ljava/lang/String;
    :cond_b
    add-int/2addr v3, v0

    .line 201
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 203
    iput v3, p0, Lcom/google/speech/tts/Time;->memoizedSerializedSize:I

    move v4, v3

    .line 204
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getSpeakPeriod()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/speech/tts/Time;->speakPeriod_:Z

    return v0
.end method

.method public getStyle()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/google/speech/tts/Time;->style_:I

    return v0
.end method

.method public getSuffix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/tts/Time;->suffix_:Ljava/lang/String;

    return-object v0
.end method

.method public getZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/speech/tts/Time;->zone_:Ljava/lang/String;

    return-object v0
.end method

.method public hasCodeSwitch()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/speech/tts/Time;->hasCodeSwitch:Z

    return v0
.end method

.method public hasHours()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Time;->hasHours:Z

    return v0
.end method

.method public hasMinutes()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Time;->hasMinutes:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/speech/tts/Time;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasPreserveOrder()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/speech/tts/Time;->hasPreserveOrder:Z

    return v0
.end method

.method public hasSeconds()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/Time;->hasSeconds:Z

    return v0
.end method

.method public hasSpeakPeriod()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/Time;->hasSpeakPeriod:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/Time;->hasStyle:Z

    return v0
.end method

.method public hasSuffix()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/Time;->hasSuffix:Z

    return v0
.end method

.method public hasZone()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/Time;->hasZone:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->toBuilder()Lcom/google/speech/tts/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Time$Builder;
    .locals 1

    .prologue
    .line 279
    invoke-static {p0}, Lcom/google/speech/tts/Time;->newBuilder(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getSerializedSize()I

    .line 113
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasHours()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getHours()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 116
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasMinutes()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 117
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getMinutes()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 119
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasSeconds()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 120
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getSeconds()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 122
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasSpeakPeriod()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 123
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getSpeakPeriod()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 125
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasSuffix()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 126
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getSuffix()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 128
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 129
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getStyle()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 131
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasZone()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 132
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getZone()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 134
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 135
    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 137
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasPreserveOrder()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 138
    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getPreserveOrder()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 140
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->hasCodeSwitch()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 141
    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getCodeSwitch()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 143
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/tts/Time;->getFieldOrderList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 144
    .local v0, "element":Ljava/lang/String;
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 146
    .end local v0    # "element":Ljava/lang/String;
    :cond_a
    return-void
.end method
