.class public final enum Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;
.super Ljava/lang/Enum;
.source "ReadStatusProto.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/ReadStatusProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReadStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

.field public static final enum READ_STATUS_END_OF_STREAM:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

.field public static final enum READ_STATUS_ERROR:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

.field public static final enum READ_STATUS_SUCCESS:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    new-instance v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    const-string v1, "READ_STATUS_ERROR"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->READ_STATUS_ERROR:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    .line 14
    new-instance v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    const-string v1, "READ_STATUS_END_OF_STREAM"

    invoke-direct {v0, v1, v4, v4, v3}, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->READ_STATUS_END_OF_STREAM:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    .line 15
    new-instance v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    const-string v1, "READ_STATUS_SUCCESS"

    invoke-direct {v0, v1, v5, v5, v4}, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->READ_STATUS_SUCCESS:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    .line 11
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    sget-object v1, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->READ_STATUS_ERROR:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->READ_STATUS_END_OF_STREAM:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->READ_STATUS_SUCCESS:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->$VALUES:[Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    .line 35
    new-instance v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus$1;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->index:I

    .line 46
    iput p4, p0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->value:I

    .line 47
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->$VALUES:[Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    invoke-virtual {v0}, [Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->value:I

    return v0
.end method
