.class public final Lcom/google/speech/tts/output_info/Token$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Token.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/output_info/Token;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/output_info/Token;",
        "Lcom/google/speech/tts/output_info/Token$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/output_info/Token;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/output_info/Token$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-static {}, Lcom/google/speech/tts/output_info/Token$Builder;->create()Lcom/google/speech/tts/output_info/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/output_info/Token$Builder;
    .locals 3

    .prologue
    .line 156
    new-instance v0, Lcom/google/speech/tts/output_info/Token$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/output_info/Token$Builder;-><init>()V

    .line 157
    .local v0, "builder":Lcom/google/speech/tts/output_info/Token$Builder;
    new-instance v1, Lcom/google/speech/tts/output_info/Token;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/output_info/Token;-><init>(Lcom/google/speech/tts/output_info/Token$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    .line 158
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token$Builder;->build()Lcom/google/speech/tts/output_info/Token;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/output_info/Token;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    invoke-static {v0}, Lcom/google/speech/tts/output_info/Token$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 189
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token$Builder;->buildPartial()Lcom/google/speech/tts/output_info/Token;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/output_info/Token;
    .locals 3

    .prologue
    .line 202
    iget-object v1, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    if-nez v1, :cond_0

    .line 203
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    .line 207
    .local v0, "returnMe":Lcom/google/speech/tts/output_info/Token;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    .line 208
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token$Builder;->clone()Lcom/google/speech/tts/output_info/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token$Builder;->clone()Lcom/google/speech/tts/output_info/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/output_info/Token$Builder;
    .locals 2

    .prologue
    .line 175
    invoke-static {}, Lcom/google/speech/tts/output_info/Token$Builder;->create()Lcom/google/speech/tts/output_info/Token$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/output_info/Token$Builder;->mergeFrom(Lcom/google/speech/tts/output_info/Token;)Lcom/google/speech/tts/output_info/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Token$Builder;->clone()Lcom/google/speech/tts/output_info/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    invoke-virtual {v0}, Lcom/google/speech/tts/output_info/Token;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeEnd(Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/output_info/InputIndex;

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    invoke-virtual {v0}, Lcom/google/speech/tts/output_info/Token;->hasEnd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    # getter for: Lcom/google/speech/tts/output_info/Token;->end_:Lcom/google/speech/tts/output_info/InputIndex;
    invoke-static {v0}, Lcom/google/speech/tts/output_info/Token;->access$600(Lcom/google/speech/tts/output_info/Token;)Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/output_info/InputIndex;->getDefaultInstance()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 320
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    iget-object v1, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    # getter for: Lcom/google/speech/tts/output_info/Token;->end_:Lcom/google/speech/tts/output_info/InputIndex;
    invoke-static {v1}, Lcom/google/speech/tts/output_info/Token;->access$600(Lcom/google/speech/tts/output_info/Token;)Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/output_info/InputIndex;->newBuilder(Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/output_info/InputIndex$Builder;->mergeFrom(Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/output_info/InputIndex$Builder;->buildPartial()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/output_info/Token;->end_:Lcom/google/speech/tts/output_info/InputIndex;
    invoke-static {v0, v1}, Lcom/google/speech/tts/output_info/Token;->access$602(Lcom/google/speech/tts/output_info/Token;Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex;

    .line 325
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/output_info/Token;->hasEnd:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/output_info/Token;->access$502(Lcom/google/speech/tts/output_info/Token;Z)Z

    .line 326
    return-object p0

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    # setter for: Lcom/google/speech/tts/output_info/Token;->end_:Lcom/google/speech/tts/output_info/InputIndex;
    invoke-static {v0, p1}, Lcom/google/speech/tts/output_info/Token;->access$602(Lcom/google/speech/tts/output_info/Token;Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 147
    check-cast p1, Lcom/google/speech/tts/output_info/Token;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/output_info/Token$Builder;->mergeFrom(Lcom/google/speech/tts/output_info/Token;)Lcom/google/speech/tts/output_info/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/output_info/Token;)Lcom/google/speech/tts/output_info/Token$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/output_info/Token;

    .prologue
    .line 212
    invoke-static {}, Lcom/google/speech/tts/output_info/Token;->getDefaultInstance()Lcom/google/speech/tts/output_info/Token;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-object p0

    .line 213
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/output_info/Token;->hasStart()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    invoke-virtual {p1}, Lcom/google/speech/tts/output_info/Token;->getStart()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/output_info/Token$Builder;->mergeStart(Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/Token$Builder;

    .line 216
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/output_info/Token;->hasEnd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {p1}, Lcom/google/speech/tts/output_info/Token;->getEnd()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/output_info/Token$Builder;->mergeEnd(Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/Token$Builder;

    goto :goto_0
.end method

.method public mergeStart(Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/output_info/InputIndex;

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    invoke-virtual {v0}, Lcom/google/speech/tts/output_info/Token;->hasStart()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    # getter for: Lcom/google/speech/tts/output_info/Token;->start_:Lcom/google/speech/tts/output_info/InputIndex;
    invoke-static {v0}, Lcom/google/speech/tts/output_info/Token;->access$400(Lcom/google/speech/tts/output_info/Token;)Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/output_info/InputIndex;->getDefaultInstance()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    iget-object v1, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    # getter for: Lcom/google/speech/tts/output_info/Token;->start_:Lcom/google/speech/tts/output_info/InputIndex;
    invoke-static {v1}, Lcom/google/speech/tts/output_info/Token;->access$400(Lcom/google/speech/tts/output_info/Token;)Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/output_info/InputIndex;->newBuilder(Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/output_info/InputIndex$Builder;->mergeFrom(Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/output_info/InputIndex$Builder;->buildPartial()Lcom/google/speech/tts/output_info/InputIndex;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/output_info/Token;->start_:Lcom/google/speech/tts/output_info/InputIndex;
    invoke-static {v0, v1}, Lcom/google/speech/tts/output_info/Token;->access$402(Lcom/google/speech/tts/output_info/Token;Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex;

    .line 288
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/output_info/Token;->hasStart:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/output_info/Token;->access$302(Lcom/google/speech/tts/output_info/Token;Z)Z

    .line 289
    return-object p0

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Token$Builder;->result:Lcom/google/speech/tts/output_info/Token;

    # setter for: Lcom/google/speech/tts/output_info/Token;->start_:Lcom/google/speech/tts/output_info/InputIndex;
    invoke-static {v0, p1}, Lcom/google/speech/tts/output_info/Token;->access$402(Lcom/google/speech/tts/output_info/Token;Lcom/google/speech/tts/output_info/InputIndex;)Lcom/google/speech/tts/output_info/InputIndex;

    goto :goto_0
.end method
