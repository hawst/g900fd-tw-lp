.class public final Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto$Morphology;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/LexiconProto$Morphology;",
        "Lcom/google/speech/tts/LexiconProto$Morphology$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/LexiconProto$Morphology;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1732
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$2800()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    .locals 1

    .prologue
    .line 1726
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->create()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    .locals 3

    .prologue
    .line 1735
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;-><init>()V

    .line 1736
    .local v0, "builder":Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    new-instance v1, Lcom/google/speech/tts/LexiconProto$Morphology;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/LexiconProto$Morphology;-><init>(Lcom/google/speech/tts/LexiconProto$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    .line 1737
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1726
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->build()Lcom/google/speech/tts/LexiconProto$Morphology;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/LexiconProto$Morphology;
    .locals 1

    .prologue
    .line 1765
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1766
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1768
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->buildPartial()Lcom/google/speech/tts/LexiconProto$Morphology;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/LexiconProto$Morphology;
    .locals 3

    .prologue
    .line 1781
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    if-nez v1, :cond_0

    .line 1782
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1785
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    .line 1786
    .local v0, "returnMe":Lcom/google/speech/tts/LexiconProto$Morphology;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    .line 1787
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1726
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1726
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    .locals 2

    .prologue
    .line 1754
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->create()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1726
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 1762
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$Morphology;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 1726
    check-cast p1, Lcom/google/speech/tts/LexiconProto$Morphology;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/LexiconProto$Morphology;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/LexiconProto$Morphology;

    .prologue
    .line 1791
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Morphology;->getDefaultInstance()Lcom/google/speech/tts/LexiconProto$Morphology;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1804
    :cond_0
    :goto_0
    return-object p0

    .line 1792
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Morphology;->hasPos()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1793
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Morphology;->getPos()Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->setPos(Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    .line 1795
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Morphology;->hasGender()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1796
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Morphology;->getGender()Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->setGender(Lcom/google/speech/tts/LexiconProto$Morphology$Gender;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    .line 1798
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Morphology;->hasAnimacy()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1799
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Morphology;->getAnimacy()Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->setAnimacy(Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    .line 1801
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Morphology;->hasNumber()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1802
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Morphology;->getNumber()Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->setNumber(Lcom/google/speech/tts/LexiconProto$Morphology$Number;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;

    goto :goto_0
.end method

.method public setAnimacy(Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    .prologue
    .line 1909
    if-nez p1, :cond_0

    .line 1910
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1912
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Morphology;->hasAnimacy:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Morphology;->access$3402(Lcom/google/speech/tts/LexiconProto$Morphology;Z)Z

    .line 1913
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    # setter for: Lcom/google/speech/tts/LexiconProto$Morphology;->animacy_:Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Morphology;->access$3502(Lcom/google/speech/tts/LexiconProto$Morphology;Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;)Lcom/google/speech/tts/LexiconProto$Morphology$Animacy;

    .line 1914
    return-object p0
.end method

.method public setGender(Lcom/google/speech/tts/LexiconProto$Morphology$Gender;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    .prologue
    .line 1888
    if-nez p1, :cond_0

    .line 1889
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1891
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Morphology;->hasGender:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Morphology;->access$3202(Lcom/google/speech/tts/LexiconProto$Morphology;Z)Z

    .line 1892
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    # setter for: Lcom/google/speech/tts/LexiconProto$Morphology;->gender_:Lcom/google/speech/tts/LexiconProto$Morphology$Gender;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Morphology;->access$3302(Lcom/google/speech/tts/LexiconProto$Morphology;Lcom/google/speech/tts/LexiconProto$Morphology$Gender;)Lcom/google/speech/tts/LexiconProto$Morphology$Gender;

    .line 1893
    return-object p0
.end method

.method public setNumber(Lcom/google/speech/tts/LexiconProto$Morphology$Number;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    .prologue
    .line 1930
    if-nez p1, :cond_0

    .line 1931
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1933
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Morphology;->hasNumber:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Morphology;->access$3602(Lcom/google/speech/tts/LexiconProto$Morphology;Z)Z

    .line 1934
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    # setter for: Lcom/google/speech/tts/LexiconProto$Morphology;->number_:Lcom/google/speech/tts/LexiconProto$Morphology$Number;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Morphology;->access$3702(Lcom/google/speech/tts/LexiconProto$Morphology;Lcom/google/speech/tts/LexiconProto$Morphology$Number;)Lcom/google/speech/tts/LexiconProto$Morphology$Number;

    .line 1935
    return-object p0
.end method

.method public setPos(Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;)Lcom/google/speech/tts/LexiconProto$Morphology$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .prologue
    .line 1867
    if-nez p1, :cond_0

    .line 1868
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1870
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Morphology;->hasPos:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Morphology;->access$3002(Lcom/google/speech/tts/LexiconProto$Morphology;Z)Z

    .line 1871
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Morphology$Builder;->result:Lcom/google/speech/tts/LexiconProto$Morphology;

    # setter for: Lcom/google/speech/tts/LexiconProto$Morphology;->pos_:Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Morphology;->access$3102(Lcom/google/speech/tts/LexiconProto$Morphology;Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;)Lcom/google/speech/tts/LexiconProto$Morphology$PartOfSpeech;

    .line 1872
    return-object p0
.end method
