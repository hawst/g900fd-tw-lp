.class public final Lcom/google/speech/tts/PiecewiseModelParams;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PiecewiseModelParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/PiecewiseModelParams$1;,
        Lcom/google/speech/tts/PiecewiseModelParams$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/PiecewiseModelParams;


# instance fields
.field private deviation_:F

.field private hasDeviation:Z

.field private hasTimeAddressing:Z

.field private memoizedSerializedSize:I

.field private sample_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/TimeValueFunctionSamples;",
            ">;"
        }
    .end annotation
.end field

.field private timeAddressing_:Lcom/google/speech/tts/TimeAddressingType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 385
    new-instance v0, Lcom/google/speech/tts/PiecewiseModelParams;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/PiecewiseModelParams;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/PiecewiseModelParams;->defaultInstance:Lcom/google/speech/tts/PiecewiseModelParams;

    .line 386
    invoke-static {}, Lcom/google/speech/tts/AdvancedVoiceModProto;->internalForceInit()V

    .line 387
    sget-object v0, Lcom/google/speech/tts/PiecewiseModelParams;->defaultInstance:Lcom/google/speech/tts/PiecewiseModelParams;

    invoke-direct {v0}, Lcom/google/speech/tts/PiecewiseModelParams;->initFields()V

    .line 388
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;

    .line 37
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->deviation_:F

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/PiecewiseModelParams$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/PiecewiseModelParams$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/PiecewiseModelParams;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;

    .line 37
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->deviation_:F

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lcom/google/speech/tts/PiecewiseModelParams;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/PiecewiseModelParams;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/PiecewiseModelParams;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/PiecewiseModelParams;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/PiecewiseModelParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/PiecewiseModelParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/PiecewiseModelParams;->hasDeviation:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/PiecewiseModelParams;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/PiecewiseModelParams;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/PiecewiseModelParams;->deviation_:F

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/PiecewiseModelParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/PiecewiseModelParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/PiecewiseModelParams;->hasTimeAddressing:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/PiecewiseModelParams;Lcom/google/speech/tts/TimeAddressingType;)Lcom/google/speech/tts/TimeAddressingType;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/PiecewiseModelParams;
    .param p1, "x1"    # Lcom/google/speech/tts/TimeAddressingType;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/PiecewiseModelParams;->timeAddressing_:Lcom/google/speech/tts/TimeAddressingType;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/PiecewiseModelParams;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/PiecewiseModelParams;->defaultInstance:Lcom/google/speech/tts/PiecewiseModelParams;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/speech/tts/TimeAddressingType;->UNIFORM:Lcom/google/speech/tts/TimeAddressingType;

    iput-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->timeAddressing_:Lcom/google/speech/tts/TimeAddressingType;

    .line 50
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/PiecewiseModelParams$Builder;
    .locals 1

    .prologue
    .line 161
    # invokes: Lcom/google/speech/tts/PiecewiseModelParams$Builder;->create()Lcom/google/speech/tts/PiecewiseModelParams$Builder;
    invoke-static {}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->access$100()Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/PiecewiseModelParams$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/PiecewiseModelParams;

    .prologue
    .line 164
    invoke-static {}, Lcom/google/speech/tts/PiecewiseModelParams;->newBuilder()Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->mergeFrom(Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->getDefaultInstanceForType()Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/PiecewiseModelParams;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/PiecewiseModelParams;->defaultInstance:Lcom/google/speech/tts/PiecewiseModelParams;

    return-object v0
.end method

.method public getDeviation()F
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->deviation_:F

    return v0
.end method

.method public getSampleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/TimeValueFunctionSamples;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 74
    iget v2, p0, Lcom/google/speech/tts/PiecewiseModelParams;->memoizedSerializedSize:I

    .line 75
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 91
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 77
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 78
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->getSampleList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/TimeValueFunctionSamples;

    .line 79
    .local v0, "element":Lcom/google/speech/tts/TimeValueFunctionSamples;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 81
    goto :goto_1

    .line 82
    .end local v0    # "element":Lcom/google/speech/tts/TimeValueFunctionSamples;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->hasDeviation()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 83
    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->getDeviation()F

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v4

    add-int/2addr v2, v4

    .line 86
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->hasTimeAddressing()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 87
    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->getTimeAddressing()Lcom/google/speech/tts/TimeAddressingType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/speech/tts/TimeAddressingType;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 90
    :cond_3
    iput v2, p0, Lcom/google/speech/tts/PiecewiseModelParams;->memoizedSerializedSize:I

    move v3, v2

    .line 91
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getTimeAddressing()Lcom/google/speech/tts/TimeAddressingType;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->timeAddressing_:Lcom/google/speech/tts/TimeAddressingType;

    return-object v0
.end method

.method public hasDeviation()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->hasDeviation:Z

    return v0
.end method

.method public hasTimeAddressing()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/speech/tts/PiecewiseModelParams;->hasTimeAddressing:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->getSampleList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/TimeValueFunctionSamples;

    .line 53
    .local v0, "element":Lcom/google/speech/tts/TimeValueFunctionSamples;
    invoke-virtual {v0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 55
    .end local v0    # "element":Lcom/google/speech/tts/TimeValueFunctionSamples;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->toBuilder()Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/PiecewiseModelParams$Builder;
    .locals 1

    .prologue
    .line 166
    invoke-static {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->newBuilder(Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->getSerializedSize()I

    .line 61
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->getSampleList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/TimeValueFunctionSamples;

    .line 62
    .local v0, "element":Lcom/google/speech/tts/TimeValueFunctionSamples;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 64
    .end local v0    # "element":Lcom/google/speech/tts/TimeValueFunctionSamples;
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->hasDeviation()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 65
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->getDeviation()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 67
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->hasTimeAddressing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 68
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams;->getTimeAddressing()Lcom/google/speech/tts/TimeAddressingType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/TimeAddressingType;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 70
    :cond_2
    return-void
.end method
