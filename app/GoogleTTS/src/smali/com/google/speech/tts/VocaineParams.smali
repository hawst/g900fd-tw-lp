.class public final Lcom/google/speech/tts/VocaineParams;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "VocaineParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/VocaineParams$1;,
        Lcom/google/speech/tts/VocaineParams$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/VocaineParams;


# instance fields
.field private aperiodicityBoost_:F

.field private hasAperiodicityBoost:Z

.field private hasMaximumVoicedFrequency:Z

.field private hasModulationFactor:Z

.field private hasPhaseDispersion:Z

.field private hasUnvoicedScaleFactor:Z

.field private hasUseNoiseModulation:Z

.field private maximumVoicedFrequency_:F

.field private memoizedSerializedSize:I

.field private modulationFactor_:F

.field private phaseDispersion_:F

.field private unvoicedScaleFactor_:F

.field private useNoiseModulation_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 444
    new-instance v0, Lcom/google/speech/tts/VocaineParams;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/VocaineParams;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/VocaineParams;->defaultInstance:Lcom/google/speech/tts/VocaineParams;

    .line 445
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 446
    sget-object v0, Lcom/google/speech/tts/VocaineParams;->defaultInstance:Lcom/google/speech/tts/VocaineParams;

    invoke-direct {v0}, Lcom/google/speech/tts/VocaineParams;->initFields()V

    .line 447
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const v0, 0x45dac000    # 7000.0f

    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->maximumVoicedFrequency_:F

    .line 32
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->aperiodicityBoost_:F

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/tts/VocaineParams;->useNoiseModulation_:Z

    .line 46
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->modulationFactor_:F

    .line 53
    const/high16 v0, -0x3fc00000    # -3.0f

    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->unvoicedScaleFactor_:F

    .line 60
    const/high16 v0, 0x3e800000    # 0.25f

    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->phaseDispersion_:F

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/VocaineParams;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/VocaineParams$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/VocaineParams$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/VocaineParams;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const v0, 0x45dac000    # 7000.0f

    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->maximumVoicedFrequency_:F

    .line 32
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->aperiodicityBoost_:F

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/tts/VocaineParams;->useNoiseModulation_:Z

    .line 46
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->modulationFactor_:F

    .line 53
    const/high16 v0, -0x3fc00000    # -3.0f

    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->unvoicedScaleFactor_:F

    .line 60
    const/high16 v0, 0x3e800000    # 0.25f

    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->phaseDispersion_:F

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/VocaineParams;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VocaineParams;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/VocaineParams;->modulationFactor_:F

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/VocaineParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VocaineParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/VocaineParams;->hasUnvoicedScaleFactor:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/VocaineParams;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VocaineParams;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/VocaineParams;->unvoicedScaleFactor_:F

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/VocaineParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VocaineParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/VocaineParams;->hasPhaseDispersion:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/VocaineParams;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VocaineParams;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/VocaineParams;->phaseDispersion_:F

    return p1
.end method

.method static synthetic access$302(Lcom/google/speech/tts/VocaineParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VocaineParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/VocaineParams;->hasMaximumVoicedFrequency:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/VocaineParams;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VocaineParams;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/VocaineParams;->maximumVoicedFrequency_:F

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/VocaineParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VocaineParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/VocaineParams;->hasAperiodicityBoost:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/VocaineParams;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VocaineParams;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/VocaineParams;->aperiodicityBoost_:F

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/VocaineParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VocaineParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/VocaineParams;->hasUseNoiseModulation:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/VocaineParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VocaineParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/VocaineParams;->useNoiseModulation_:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/VocaineParams;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/VocaineParams;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/VocaineParams;->hasModulationFactor:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/VocaineParams;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/VocaineParams;->defaultInstance:Lcom/google/speech/tts/VocaineParams;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/VocaineParams$Builder;
    .locals 1

    .prologue
    .line 194
    # invokes: Lcom/google/speech/tts/VocaineParams$Builder;->create()Lcom/google/speech/tts/VocaineParams$Builder;
    invoke-static {}, Lcom/google/speech/tts/VocaineParams$Builder;->access$100()Lcom/google/speech/tts/VocaineParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/VocaineParams;)Lcom/google/speech/tts/VocaineParams$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/VocaineParams;

    .prologue
    .line 197
    invoke-static {}, Lcom/google/speech/tts/VocaineParams;->newBuilder()Lcom/google/speech/tts/VocaineParams$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/VocaineParams$Builder;->mergeFrom(Lcom/google/speech/tts/VocaineParams;)Lcom/google/speech/tts/VocaineParams$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAperiodicityBoost()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/speech/tts/VocaineParams;->aperiodicityBoost_:F

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getDefaultInstanceForType()Lcom/google/speech/tts/VocaineParams;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/VocaineParams;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/VocaineParams;->defaultInstance:Lcom/google/speech/tts/VocaineParams;

    return-object v0
.end method

.method public getMaximumVoicedFrequency()F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/tts/VocaineParams;->maximumVoicedFrequency_:F

    return v0
.end method

.method public getModulationFactor()F
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/speech/tts/VocaineParams;->modulationFactor_:F

    return v0
.end method

.method public getPhaseDispersion()F
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/google/speech/tts/VocaineParams;->phaseDispersion_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 95
    iget v0, p0, Lcom/google/speech/tts/VocaineParams;->memoizedSerializedSize:I

    .line 96
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 124
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 98
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 99
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->hasMaximumVoicedFrequency()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 100
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getMaximumVoicedFrequency()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 103
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->hasAperiodicityBoost()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 104
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getAperiodicityBoost()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 107
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->hasUseNoiseModulation()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 108
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getUseNoiseModulation()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 111
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->hasModulationFactor()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 112
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getModulationFactor()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 115
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->hasUnvoicedScaleFactor()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 116
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getUnvoicedScaleFactor()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 119
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->hasPhaseDispersion()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 120
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getPhaseDispersion()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 123
    :cond_6
    iput v0, p0, Lcom/google/speech/tts/VocaineParams;->memoizedSerializedSize:I

    move v1, v0

    .line 124
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getUnvoicedScaleFactor()F
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/speech/tts/VocaineParams;->unvoicedScaleFactor_:F

    return v0
.end method

.method public getUseNoiseModulation()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/speech/tts/VocaineParams;->useNoiseModulation_:Z

    return v0
.end method

.method public hasAperiodicityBoost()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/VocaineParams;->hasAperiodicityBoost:Z

    return v0
.end method

.method public hasMaximumVoicedFrequency()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/VocaineParams;->hasMaximumVoicedFrequency:Z

    return v0
.end method

.method public hasModulationFactor()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/VocaineParams;->hasModulationFactor:Z

    return v0
.end method

.method public hasPhaseDispersion()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/VocaineParams;->hasPhaseDispersion:Z

    return v0
.end method

.method public hasUnvoicedScaleFactor()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/VocaineParams;->hasUnvoicedScaleFactor:Z

    return v0
.end method

.method public hasUseNoiseModulation()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/VocaineParams;->hasUseNoiseModulation:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->toBuilder()Lcom/google/speech/tts/VocaineParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/VocaineParams$Builder;
    .locals 1

    .prologue
    .line 199
    invoke-static {p0}, Lcom/google/speech/tts/VocaineParams;->newBuilder(Lcom/google/speech/tts/VocaineParams;)Lcom/google/speech/tts/VocaineParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getSerializedSize()I

    .line 73
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->hasMaximumVoicedFrequency()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getMaximumVoicedFrequency()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->hasAperiodicityBoost()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getAperiodicityBoost()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 79
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->hasUseNoiseModulation()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getUseNoiseModulation()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 82
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->hasModulationFactor()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 83
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getModulationFactor()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 85
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->hasUnvoicedScaleFactor()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 86
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getUnvoicedScaleFactor()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 88
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->hasPhaseDispersion()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 89
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/VocaineParams;->getPhaseDispersion()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 91
    :cond_5
    return-void
.end method
