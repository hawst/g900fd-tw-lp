.class public final Lcom/google/speech/tts/Telephone$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Telephone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Telephone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Telephone;",
        "Lcom/google/speech/tts/Telephone$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Telephone;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Telephone$Builder;
    .locals 1

    .prologue
    .line 249
    invoke-static {}, Lcom/google/speech/tts/Telephone$Builder;->create()Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Telephone$Builder;
    .locals 3

    .prologue
    .line 258
    new-instance v0, Lcom/google/speech/tts/Telephone$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Telephone$Builder;-><init>()V

    .line 259
    .local v0, "builder":Lcom/google/speech/tts/Telephone$Builder;
    new-instance v1, Lcom/google/speech/tts/Telephone;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Telephone;-><init>(Lcom/google/speech/tts/Telephone$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    .line 260
    return-object v0
.end method


# virtual methods
.method public addNumberPart(Ljava/lang/String;)Lcom/google/speech/tts/Telephone$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 447
    if-nez p1, :cond_0

    .line 448
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 450
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # getter for: Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Telephone;->access$300(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 451
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Telephone;->access$302(Lcom/google/speech/tts/Telephone;Ljava/util/List;)Ljava/util/List;

    .line 453
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # getter for: Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Telephone;->access$300(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 454
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone$Builder;->build()Lcom/google/speech/tts/Telephone;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Telephone;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    invoke-static {v0}, Lcom/google/speech/tts/Telephone$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 291
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone$Builder;->buildPartial()Lcom/google/speech/tts/Telephone;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Telephone;
    .locals 3

    .prologue
    .line 304
    iget-object v1, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    if-nez v1, :cond_0

    .line 305
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 308
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # getter for: Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Telephone;->access$300(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 309
    iget-object v1, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    iget-object v2, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # getter for: Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Telephone;->access$300(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Telephone;->access$302(Lcom/google/speech/tts/Telephone;Ljava/util/List;)Ljava/util/List;

    .line 312
    :cond_1
    iget-object v1, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # getter for: Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Telephone;->access$400(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 313
    iget-object v1, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    iget-object v2, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # getter for: Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Telephone;->access$400(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Telephone;->access$402(Lcom/google/speech/tts/Telephone;Ljava/util/List;)Ljava/util/List;

    .line 316
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    .line 317
    .local v0, "returnMe":Lcom/google/speech/tts/Telephone;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    .line 318
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone$Builder;->clone()Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone$Builder;->clone()Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Telephone$Builder;
    .locals 2

    .prologue
    .line 277
    invoke-static {}, Lcom/google/speech/tts/Telephone$Builder;->create()Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Telephone$Builder;->mergeFrom(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/speech/tts/Telephone$Builder;->clone()Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    invoke-virtual {v0}, Lcom/google/speech/tts/Telephone;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 249
    check-cast p1, Lcom/google/speech/tts/Telephone;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Telephone$Builder;->mergeFrom(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Telephone;

    .prologue
    .line 322
    invoke-static {}, Lcom/google/speech/tts/Telephone;->getDefaultInstance()Lcom/google/speech/tts/Telephone;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 353
    :cond_0
    :goto_0
    return-object p0

    .line 323
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Telephone;->hasCountryCode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 324
    invoke-virtual {p1}, Lcom/google/speech/tts/Telephone;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Telephone$Builder;->setCountryCode(Ljava/lang/String;)Lcom/google/speech/tts/Telephone$Builder;

    .line 326
    :cond_2
    # getter for: Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Telephone;->access$300(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 327
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # getter for: Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Telephone;->access$300(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 328
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Telephone;->access$302(Lcom/google/speech/tts/Telephone;Ljava/util/List;)Ljava/util/List;

    .line 330
    :cond_3
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # getter for: Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Telephone;->access$300(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Telephone;->numberPart_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Telephone;->access$300(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 332
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Telephone;->hasExtension()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 333
    invoke-virtual {p1}, Lcom/google/speech/tts/Telephone;->getExtension()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Telephone$Builder;->setExtension(Ljava/lang/String;)Lcom/google/speech/tts/Telephone$Builder;

    .line 335
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Telephone;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 336
    invoke-virtual {p1}, Lcom/google/speech/tts/Telephone;->getStyle()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Telephone$Builder;->setStyle(I)Lcom/google/speech/tts/Telephone$Builder;

    .line 338
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Telephone;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 339
    invoke-virtual {p1}, Lcom/google/speech/tts/Telephone;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Telephone$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Telephone$Builder;

    .line 341
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/Telephone;->hasPreserveOrder()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 342
    invoke-virtual {p1}, Lcom/google/speech/tts/Telephone;->getPreserveOrder()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Telephone$Builder;->setPreserveOrder(Z)Lcom/google/speech/tts/Telephone$Builder;

    .line 344
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/tts/Telephone;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 345
    invoke-virtual {p1}, Lcom/google/speech/tts/Telephone;->getCodeSwitch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Telephone$Builder;->setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Telephone$Builder;

    .line 347
    :cond_9
    # getter for: Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Telephone;->access$400(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # getter for: Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Telephone;->access$400(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 349
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Telephone;->access$402(Lcom/google/speech/tts/Telephone;Ljava/util/List;)Ljava/util/List;

    .line 351
    :cond_a
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # getter for: Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Telephone;->access$400(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Telephone;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Telephone;->access$400(Lcom/google/speech/tts/Telephone;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Telephone$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 555
    if-nez p1, :cond_0

    .line 556
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 558
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Telephone;->hasCodeSwitch:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Telephone;->access$1502(Lcom/google/speech/tts/Telephone;Z)Z

    .line 559
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # setter for: Lcom/google/speech/tts/Telephone;->codeSwitch_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Telephone;->access$1602(Lcom/google/speech/tts/Telephone;Ljava/lang/String;)Ljava/lang/String;

    .line 560
    return-object p0
.end method

.method public setCountryCode(Ljava/lang/String;)Lcom/google/speech/tts/Telephone$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 416
    if-nez p1, :cond_0

    .line 417
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Telephone;->hasCountryCode:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Telephone;->access$502(Lcom/google/speech/tts/Telephone;Z)Z

    .line 420
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # setter for: Lcom/google/speech/tts/Telephone;->countryCode_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Telephone;->access$602(Lcom/google/speech/tts/Telephone;Ljava/lang/String;)Ljava/lang/String;

    .line 421
    return-object p0
.end method

.method public setExtension(Ljava/lang/String;)Lcom/google/speech/tts/Telephone$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 477
    if-nez p1, :cond_0

    .line 478
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Telephone;->hasExtension:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Telephone;->access$702(Lcom/google/speech/tts/Telephone;Z)Z

    .line 481
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # setter for: Lcom/google/speech/tts/Telephone;->extension_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Telephone;->access$802(Lcom/google/speech/tts/Telephone;Ljava/lang/String;)Ljava/lang/String;

    .line 482
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Telephone$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 516
    if-nez p1, :cond_0

    .line 517
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 519
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Telephone;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Telephone;->access$1102(Lcom/google/speech/tts/Telephone;Z)Z

    .line 520
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # setter for: Lcom/google/speech/tts/Telephone;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Telephone;->access$1202(Lcom/google/speech/tts/Telephone;Ljava/lang/String;)Ljava/lang/String;

    .line 521
    return-object p0
.end method

.method public setPreserveOrder(Z)Lcom/google/speech/tts/Telephone$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 537
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Telephone;->hasPreserveOrder:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Telephone;->access$1302(Lcom/google/speech/tts/Telephone;Z)Z

    .line 538
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # setter for: Lcom/google/speech/tts/Telephone;->preserveOrder_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Telephone;->access$1402(Lcom/google/speech/tts/Telephone;Z)Z

    .line 539
    return-object p0
.end method

.method public setStyle(I)Lcom/google/speech/tts/Telephone$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Telephone;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Telephone;->access$902(Lcom/google/speech/tts/Telephone;Z)Z

    .line 499
    iget-object v0, p0, Lcom/google/speech/tts/Telephone$Builder;->result:Lcom/google/speech/tts/Telephone;

    # setter for: Lcom/google/speech/tts/Telephone;->style_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Telephone;->access$1002(Lcom/google/speech/tts/Telephone;I)I

    .line 500
    return-object p0
.end method
