.class public final Lcom/google/speech/tts/Time$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Time.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Time;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Time;",
        "Lcom/google/speech/tts/Time$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Time;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 287
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Time$Builder;
    .locals 1

    .prologue
    .line 281
    invoke-static {}, Lcom/google/speech/tts/Time$Builder;->create()Lcom/google/speech/tts/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Time$Builder;
    .locals 3

    .prologue
    .line 290
    new-instance v0, Lcom/google/speech/tts/Time$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Time$Builder;-><init>()V

    .line 291
    .local v0, "builder":Lcom/google/speech/tts/Time$Builder;
    new-instance v1, Lcom/google/speech/tts/Time;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Time;-><init>(Lcom/google/speech/tts/Time$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    .line 292
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/google/speech/tts/Time$Builder;->build()Lcom/google/speech/tts/Time;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Time;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Time$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    invoke-static {v0}, Lcom/google/speech/tts/Time$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 323
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Time$Builder;->buildPartial()Lcom/google/speech/tts/Time;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Time;
    .locals 3

    .prologue
    .line 336
    iget-object v1, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    if-nez v1, :cond_0

    .line 337
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 340
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # getter for: Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Time;->access$300(Lcom/google/speech/tts/Time;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 341
    iget-object v1, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    iget-object v2, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # getter for: Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Time;->access$300(Lcom/google/speech/tts/Time;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Time;->access$302(Lcom/google/speech/tts/Time;Ljava/util/List;)Ljava/util/List;

    .line 344
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    .line 345
    .local v0, "returnMe":Lcom/google/speech/tts/Time;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    .line 346
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/google/speech/tts/Time$Builder;->clone()Lcom/google/speech/tts/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/google/speech/tts/Time$Builder;->clone()Lcom/google/speech/tts/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Time$Builder;
    .locals 2

    .prologue
    .line 309
    invoke-static {}, Lcom/google/speech/tts/Time$Builder;->create()Lcom/google/speech/tts/Time$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Time$Builder;->mergeFrom(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/google/speech/tts/Time$Builder;->clone()Lcom/google/speech/tts/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    invoke-virtual {v0}, Lcom/google/speech/tts/Time;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 281
    check-cast p1, Lcom/google/speech/tts/Time;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Time$Builder;->mergeFrom(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Time;

    .prologue
    .line 350
    invoke-static {}, Lcom/google/speech/tts/Time;->getDefaultInstance()Lcom/google/speech/tts/Time;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-object p0

    .line 351
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->hasHours()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 352
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->getHours()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Time$Builder;->setHours(I)Lcom/google/speech/tts/Time$Builder;

    .line 354
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->hasMinutes()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 355
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->getMinutes()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Time$Builder;->setMinutes(I)Lcom/google/speech/tts/Time$Builder;

    .line 357
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->hasSeconds()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 358
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->getSeconds()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Time$Builder;->setSeconds(I)Lcom/google/speech/tts/Time$Builder;

    .line 360
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->hasSpeakPeriod()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 361
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->getSpeakPeriod()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Time$Builder;->setSpeakPeriod(Z)Lcom/google/speech/tts/Time$Builder;

    .line 363
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->hasSuffix()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 364
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->getSuffix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Time$Builder;->setSuffix(Ljava/lang/String;)Lcom/google/speech/tts/Time$Builder;

    .line 366
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 367
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->getStyle()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Time$Builder;->setStyle(I)Lcom/google/speech/tts/Time$Builder;

    .line 369
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->hasZone()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 370
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->getZone()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Time$Builder;->setZone(Ljava/lang/String;)Lcom/google/speech/tts/Time$Builder;

    .line 372
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 373
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Time$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Time$Builder;

    .line 375
    :cond_9
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->hasPreserveOrder()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 376
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->getPreserveOrder()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Time$Builder;->setPreserveOrder(Z)Lcom/google/speech/tts/Time$Builder;

    .line 378
    :cond_a
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 379
    invoke-virtual {p1}, Lcom/google/speech/tts/Time;->getCodeSwitch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Time$Builder;->setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Time$Builder;

    .line 381
    :cond_b
    # getter for: Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Time;->access$300(Lcom/google/speech/tts/Time;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # getter for: Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Time;->access$300(Lcom/google/speech/tts/Time;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 383
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Time;->access$302(Lcom/google/speech/tts/Time;Ljava/util/List;)Ljava/util/List;

    .line 385
    :cond_c
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # getter for: Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Time;->access$300(Lcom/google/speech/tts/Time;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Time;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Time;->access$300(Lcom/google/speech/tts/Time;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Time$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 633
    if-nez p1, :cond_0

    .line 634
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 636
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Time;->hasCodeSwitch:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Time;->access$2202(Lcom/google/speech/tts/Time;Z)Z

    .line 637
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # setter for: Lcom/google/speech/tts/Time;->codeSwitch_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Time;->access$2302(Lcom/google/speech/tts/Time;Ljava/lang/String;)Ljava/lang/String;

    .line 638
    return-object p0
.end method

.method public setHours(I)Lcom/google/speech/tts/Time$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Time;->hasHours:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Time;->access$402(Lcom/google/speech/tts/Time;Z)Z

    .line 463
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # setter for: Lcom/google/speech/tts/Time;->hours_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Time;->access$502(Lcom/google/speech/tts/Time;I)I

    .line 464
    return-object p0
.end method

.method public setMinutes(I)Lcom/google/speech/tts/Time$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Time;->hasMinutes:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Time;->access$602(Lcom/google/speech/tts/Time;Z)Z

    .line 481
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # setter for: Lcom/google/speech/tts/Time;->minutes_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Time;->access$702(Lcom/google/speech/tts/Time;I)I

    .line 482
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Time$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 594
    if-nez p1, :cond_0

    .line 595
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 597
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Time;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Time;->access$1802(Lcom/google/speech/tts/Time;Z)Z

    .line 598
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # setter for: Lcom/google/speech/tts/Time;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Time;->access$1902(Lcom/google/speech/tts/Time;Ljava/lang/String;)Ljava/lang/String;

    .line 599
    return-object p0
.end method

.method public setPreserveOrder(Z)Lcom/google/speech/tts/Time$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Time;->hasPreserveOrder:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Time;->access$2002(Lcom/google/speech/tts/Time;Z)Z

    .line 616
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # setter for: Lcom/google/speech/tts/Time;->preserveOrder_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Time;->access$2102(Lcom/google/speech/tts/Time;Z)Z

    .line 617
    return-object p0
.end method

.method public setSeconds(I)Lcom/google/speech/tts/Time$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Time;->hasSeconds:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Time;->access$802(Lcom/google/speech/tts/Time;Z)Z

    .line 499
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # setter for: Lcom/google/speech/tts/Time;->seconds_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Time;->access$902(Lcom/google/speech/tts/Time;I)I

    .line 500
    return-object p0
.end method

.method public setSpeakPeriod(Z)Lcom/google/speech/tts/Time$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Time;->hasSpeakPeriod:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Time;->access$1002(Lcom/google/speech/tts/Time;Z)Z

    .line 517
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # setter for: Lcom/google/speech/tts/Time;->speakPeriod_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Time;->access$1102(Lcom/google/speech/tts/Time;Z)Z

    .line 518
    return-object p0
.end method

.method public setStyle(I)Lcom/google/speech/tts/Time$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Time;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Time;->access$1402(Lcom/google/speech/tts/Time;Z)Z

    .line 556
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # setter for: Lcom/google/speech/tts/Time;->style_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/Time;->access$1502(Lcom/google/speech/tts/Time;I)I

    .line 557
    return-object p0
.end method

.method public setSuffix(Ljava/lang/String;)Lcom/google/speech/tts/Time$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 534
    if-nez p1, :cond_0

    .line 535
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 537
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Time;->hasSuffix:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Time;->access$1202(Lcom/google/speech/tts/Time;Z)Z

    .line 538
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # setter for: Lcom/google/speech/tts/Time;->suffix_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Time;->access$1302(Lcom/google/speech/tts/Time;Ljava/lang/String;)Ljava/lang/String;

    .line 539
    return-object p0
.end method

.method public setZone(Ljava/lang/String;)Lcom/google/speech/tts/Time$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 573
    if-nez p1, :cond_0

    .line 574
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Time;->hasZone:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Time;->access$1602(Lcom/google/speech/tts/Time;Z)Z

    .line 577
    iget-object v0, p0, Lcom/google/speech/tts/Time$Builder;->result:Lcom/google/speech/tts/Time;

    # setter for: Lcom/google/speech/tts/Time;->zone_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Time;->access$1702(Lcom/google/speech/tts/Time;Ljava/lang/String;)Ljava/lang/String;

    .line 578
    return-object p0
.end method
