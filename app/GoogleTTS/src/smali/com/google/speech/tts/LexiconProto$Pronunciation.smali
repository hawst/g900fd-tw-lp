.class public final Lcom/google/speech/tts/LexiconProto$Pronunciation;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Pronunciation"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/LexiconProto$Pronunciation;


# instance fields
.field private hasId:Z

.field private hasV:Z

.field private id_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private v_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 313
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Pronunciation;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/LexiconProto$Pronunciation;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    .line 314
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 315
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->initFields()V

    .line 316
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->id_:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->v_:Ljava/lang/String;

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->memoizedSerializedSize:I

    .line 26
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->initFields()V

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/LexiconProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/LexiconProto$1;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->id_:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->v_:Ljava/lang/String;

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->memoizedSerializedSize:I

    .line 28
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/tts/LexiconProto$Pronunciation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Pronunciation;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasId:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/LexiconProto$Pronunciation;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Pronunciation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->id_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/LexiconProto$Pronunciation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Pronunciation;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasV:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/LexiconProto$Pronunciation;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Pronunciation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->v_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/LexiconProto$Pronunciation;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
    .locals 1

    .prologue
    .line 157
    # invokes: Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->create()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->access$100()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/LexiconProto$Pronunciation;)Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/LexiconProto$Pronunciation;

    .prologue
    .line 160
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->newBuilder()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Pronunciation;)Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto$Pronunciation;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto$Pronunciation;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Pronunciation;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 74
    iget v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->memoizedSerializedSize:I

    .line 75
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 87
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 77
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 78
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 79
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 82
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasV()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 83
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->getV()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 86
    :cond_2
    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->memoizedSerializedSize:I

    move v1, v0

    .line 87
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->v_:Ljava/lang/String;

    return-object v0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasId:Z

    return v0
.end method

.method public hasV()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasV:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 56
    iget-boolean v1, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasId:Z

    if-nez v1, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v0

    .line 57
    :cond_1
    iget-boolean v1, p0, Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasV:Z

    if-eqz v1, :cond_0

    .line 58
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->toBuilder()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;
    .locals 1

    .prologue
    .line 162
    invoke-static {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->newBuilder(Lcom/google/speech/tts/LexiconProto$Pronunciation;)Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->getSerializedSize()I

    .line 64
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 67
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->hasV()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->getV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 70
    :cond_1
    return-void
.end method
