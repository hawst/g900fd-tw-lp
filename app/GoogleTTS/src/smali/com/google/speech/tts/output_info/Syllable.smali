.class public final Lcom/google/speech/tts/output_info/Syllable;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Syllable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/output_info/Syllable$1;,
        Lcom/google/speech/tts/output_info/Syllable$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/output_info/Syllable;


# instance fields
.field private memoizedSerializedSize:I

.field private phoneme_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/output_info/Phoneme;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 296
    new-instance v0, Lcom/google/speech/tts/output_info/Syllable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/output_info/Syllable;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/output_info/Syllable;->defaultInstance:Lcom/google/speech/tts/output_info/Syllable;

    .line 297
    invoke-static {}, Lcom/google/speech/tts/output_info/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 298
    sget-object v0, Lcom/google/speech/tts/output_info/Syllable;->defaultInstance:Lcom/google/speech/tts/output_info/Syllable;

    invoke-direct {v0}, Lcom/google/speech/tts/output_info/Syllable;->initFields()V

    .line 299
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/output_info/Syllable;->phoneme_:Ljava/util/List;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/output_info/Syllable;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/output_info/Syllable;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/output_info/Syllable$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/output_info/Syllable$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/output_info/Syllable;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/output_info/Syllable;->phoneme_:Ljava/util/List;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/output_info/Syllable;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lcom/google/speech/tts/output_info/Syllable;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Syllable;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Syllable;->phoneme_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/output_info/Syllable;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/output_info/Syllable;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/output_info/Syllable;->phoneme_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/output_info/Syllable;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/output_info/Syllable;->defaultInstance:Lcom/google/speech/tts/output_info/Syllable;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/output_info/Syllable$Builder;
    .locals 1

    .prologue
    .line 129
    # invokes: Lcom/google/speech/tts/output_info/Syllable$Builder;->create()Lcom/google/speech/tts/output_info/Syllable$Builder;
    invoke-static {}, Lcom/google/speech/tts/output_info/Syllable$Builder;->access$100()Lcom/google/speech/tts/output_info/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/output_info/Syllable;)Lcom/google/speech/tts/output_info/Syllable$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/output_info/Syllable;

    .prologue
    .line 132
    invoke-static {}, Lcom/google/speech/tts/output_info/Syllable;->newBuilder()Lcom/google/speech/tts/output_info/Syllable$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/output_info/Syllable$Builder;->mergeFrom(Lcom/google/speech/tts/output_info/Syllable;)Lcom/google/speech/tts/output_info/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Syllable;->getDefaultInstanceForType()Lcom/google/speech/tts/output_info/Syllable;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/output_info/Syllable;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/output_info/Syllable;->defaultInstance:Lcom/google/speech/tts/output_info/Syllable;

    return-object v0
.end method

.method public getPhonemeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/output_info/Phoneme;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/output_info/Syllable;->phoneme_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 50
    iget v2, p0, Lcom/google/speech/tts/output_info/Syllable;->memoizedSerializedSize:I

    .line 51
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 59
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 53
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 54
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Syllable;->getPhonemeList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/output_info/Phoneme;

    .line 55
    .local v0, "element":Lcom/google/speech/tts/output_info/Phoneme;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 57
    goto :goto_1

    .line 58
    .end local v0    # "element":Lcom/google/speech/tts/output_info/Phoneme;
    :cond_1
    iput v2, p0, Lcom/google/speech/tts/output_info/Syllable;->memoizedSerializedSize:I

    move v3, v2

    .line 59
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Syllable;->toBuilder()Lcom/google/speech/tts/output_info/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/output_info/Syllable$Builder;
    .locals 1

    .prologue
    .line 134
    invoke-static {p0}, Lcom/google/speech/tts/output_info/Syllable;->newBuilder(Lcom/google/speech/tts/output_info/Syllable;)Lcom/google/speech/tts/output_info/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Syllable;->getSerializedSize()I

    .line 43
    invoke-virtual {p0}, Lcom/google/speech/tts/output_info/Syllable;->getPhonemeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/output_info/Phoneme;

    .line 44
    .local v0, "element":Lcom/google/speech/tts/output_info/Phoneme;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 46
    .end local v0    # "element":Lcom/google/speech/tts/output_info/Phoneme;
    :cond_0
    return-void
.end method
