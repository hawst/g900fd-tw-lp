.class public final Lcom/google/speech/tts/Connector;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Connector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Connector$1;,
        Lcom/google/speech/tts/Connector$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Connector;


# instance fields
.field private codeSwitch_:Ljava/lang/String;

.field private hasCodeSwitch:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasType:Z

.field private memoizedSerializedSize:I

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private type_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 336
    new-instance v0, Lcom/google/speech/tts/Connector;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Connector;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Connector;->defaultInstance:Lcom/google/speech/tts/Connector;

    .line 337
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 338
    sget-object v0, Lcom/google/speech/tts/Connector;->defaultInstance:Lcom/google/speech/tts/Connector;

    invoke-direct {v0}, Lcom/google/speech/tts/Connector;->initFields()V

    .line 339
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Connector;->type_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Connector;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Connector;->codeSwitch_:Ljava/lang/String;

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Connector;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Connector;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Connector$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Connector$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Connector;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Connector;->type_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Connector;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Connector;->codeSwitch_:Ljava/lang/String;

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Connector;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Connector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Connector;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Connector;->hasType:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Connector;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Connector;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Connector;->type_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Connector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Connector;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Connector;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Connector;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Connector;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Connector;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Connector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Connector;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Connector;->hasCodeSwitch:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Connector;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Connector;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Connector;->codeSwitch_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Connector;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Connector;->defaultInstance:Lcom/google/speech/tts/Connector;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Connector$Builder;
    .locals 1

    .prologue
    .line 152
    # invokes: Lcom/google/speech/tts/Connector$Builder;->create()Lcom/google/speech/tts/Connector$Builder;
    invoke-static {}, Lcom/google/speech/tts/Connector$Builder;->access$100()Lcom/google/speech/tts/Connector$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Connector;)Lcom/google/speech/tts/Connector$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Connector;

    .prologue
    .line 155
    invoke-static {}, Lcom/google/speech/tts/Connector;->newBuilder()Lcom/google/speech/tts/Connector$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Connector$Builder;->mergeFrom(Lcom/google/speech/tts/Connector;)Lcom/google/speech/tts/Connector$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCodeSwitch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/Connector;->codeSwitch_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->getDefaultInstanceForType()Lcom/google/speech/tts/Connector;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Connector;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Connector;->defaultInstance:Lcom/google/speech/tts/Connector;

    return-object v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/Connector;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 65
    iget v0, p0, Lcom/google/speech/tts/Connector;->memoizedSerializedSize:I

    .line 66
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 82
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 68
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 69
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->hasType()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 70
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 73
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 74
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 77
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->hasCodeSwitch()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 78
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->getCodeSwitch()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 81
    :cond_3
    iput v0, p0, Lcom/google/speech/tts/Connector;->memoizedSerializedSize:I

    move v1, v0

    .line 82
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Connector;->type_:Ljava/lang/String;

    return-object v0
.end method

.method public hasCodeSwitch()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/Connector;->hasCodeSwitch:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Connector;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Connector;->hasType:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->toBuilder()Lcom/google/speech/tts/Connector$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Connector$Builder;
    .locals 1

    .prologue
    .line 157
    invoke-static {p0}, Lcom/google/speech/tts/Connector;->newBuilder(Lcom/google/speech/tts/Connector;)Lcom/google/speech/tts/Connector$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->getSerializedSize()I

    .line 52
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->hasType()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 58
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 59
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Connector;->getCodeSwitch()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 61
    :cond_2
    return-void
.end method
