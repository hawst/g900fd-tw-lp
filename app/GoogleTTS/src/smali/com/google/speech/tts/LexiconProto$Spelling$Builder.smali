.class public final Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto$Spelling;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/LexiconProto$Spelling;",
        "Lcom/google/speech/tts/LexiconProto$Spelling$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/LexiconProto$Spelling;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 469
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$800()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
    .locals 1

    .prologue
    .line 463
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->create()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
    .locals 3

    .prologue
    .line 472
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;-><init>()V

    .line 473
    .local v0, "builder":Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
    new-instance v1, Lcom/google/speech/tts/LexiconProto$Spelling;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/LexiconProto$Spelling;-><init>(Lcom/google/speech/tts/LexiconProto$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->result:Lcom/google/speech/tts/LexiconProto$Spelling;

    .line 474
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 463
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->build()Lcom/google/speech/tts/LexiconProto$Spelling;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/LexiconProto$Spelling;
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->result:Lcom/google/speech/tts/LexiconProto$Spelling;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->result:Lcom/google/speech/tts/LexiconProto$Spelling;

    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 505
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->buildPartial()Lcom/google/speech/tts/LexiconProto$Spelling;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/LexiconProto$Spelling;
    .locals 3

    .prologue
    .line 518
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->result:Lcom/google/speech/tts/LexiconProto$Spelling;

    if-nez v1, :cond_0

    .line 519
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 522
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->result:Lcom/google/speech/tts/LexiconProto$Spelling;

    .line 523
    .local v0, "returnMe":Lcom/google/speech/tts/LexiconProto$Spelling;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->result:Lcom/google/speech/tts/LexiconProto$Spelling;

    .line 524
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 463
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 463
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
    .locals 2

    .prologue
    .line 491
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->create()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->result:Lcom/google/speech/tts/LexiconProto$Spelling;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Spelling;)Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 463
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->result:Lcom/google/speech/tts/LexiconProto$Spelling;

    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$Spelling;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 463
    check-cast p1, Lcom/google/speech/tts/LexiconProto$Spelling;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Spelling;)Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/LexiconProto$Spelling;)Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/LexiconProto$Spelling;

    .prologue
    .line 528
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Spelling;->getDefaultInstance()Lcom/google/speech/tts/LexiconProto$Spelling;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 535
    :cond_0
    :goto_0
    return-object p0

    .line 529
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Spelling;->hasId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 530
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Spelling;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->setId(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    .line 532
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Spelling;->hasV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 533
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Spelling;->getV()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->setV(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    goto :goto_0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 574
    if-nez p1, :cond_0

    .line 575
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 577
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->result:Lcom/google/speech/tts/LexiconProto$Spelling;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Spelling;->hasId:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Spelling;->access$1002(Lcom/google/speech/tts/LexiconProto$Spelling;Z)Z

    .line 578
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->result:Lcom/google/speech/tts/LexiconProto$Spelling;

    # setter for: Lcom/google/speech/tts/LexiconProto$Spelling;->id_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Spelling;->access$1102(Lcom/google/speech/tts/LexiconProto$Spelling;Ljava/lang/String;)Ljava/lang/String;

    .line 579
    return-object p0
.end method

.method public setV(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Spelling$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 595
    if-nez p1, :cond_0

    .line 596
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 598
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->result:Lcom/google/speech/tts/LexiconProto$Spelling;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Spelling;->hasV:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Spelling;->access$1202(Lcom/google/speech/tts/LexiconProto$Spelling;Z)Z

    .line 599
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->result:Lcom/google/speech/tts/LexiconProto$Spelling;

    # setter for: Lcom/google/speech/tts/LexiconProto$Spelling;->v_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Spelling;->access$1302(Lcom/google/speech/tts/LexiconProto$Spelling;Ljava/lang/String;)Ljava/lang/String;

    .line 600
    return-object p0
.end method
