.class public final Lcom/google/speech/tts/LexiconProto;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/LexiconProto$1;,
        Lcom/google/speech/tts/LexiconProto$Builder;,
        Lcom/google/speech/tts/LexiconProto$Entry;,
        Lcom/google/speech/tts/LexiconProto$Morphology;,
        Lcom/google/speech/tts/LexiconProto$Syllable;,
        Lcom/google/speech/tts/LexiconProto$ExpandedPronunciation;,
        Lcom/google/speech/tts/LexiconProto$Spelling;,
        Lcom/google/speech/tts/LexiconProto$Pronunciation;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/LexiconProto;


# instance fields
.field private hasHeader:Z

.field private hasLegalVowels:Z

.field private header_:Ljava/lang/String;

.field private legalVowels_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private w_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2972
    new-instance v0, Lcom/google/speech/tts/LexiconProto;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/LexiconProto;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto;->defaultInstance:Lcom/google/speech/tts/LexiconProto;

    .line 2973
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 2974
    sget-object v0, Lcom/google/speech/tts/LexiconProto;->defaultInstance:Lcom/google/speech/tts/LexiconProto;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto;->initFields()V

    .line 2975
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2613
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto;->header_:Ljava/lang/String;

    .line 2619
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;

    .line 2632
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto;->legalVowels_:Ljava/lang/String;

    .line 2660
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/LexiconProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/LexiconProto$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2613
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto;->header_:Ljava/lang/String;

    .line 2619
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;

    .line 2632
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto;->legalVowels_:Ljava/lang/String;

    .line 2660
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$5400(Lcom/google/speech/tts/LexiconProto;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5402(Lcom/google/speech/tts/LexiconProto;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$5502(Lcom/google/speech/tts/LexiconProto;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto;->hasHeader:Z

    return p1
.end method

.method static synthetic access$5602(Lcom/google/speech/tts/LexiconProto;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto;->header_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5702(Lcom/google/speech/tts/LexiconProto;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto;->hasLegalVowels:Z

    return p1
.end method

.method static synthetic access$5802(Lcom/google/speech/tts/LexiconProto;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto;->legalVowels_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/LexiconProto;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/LexiconProto;->defaultInstance:Lcom/google/speech/tts/LexiconProto;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 2637
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/LexiconProto$Builder;
    .locals 1

    .prologue
    .line 2749
    # invokes: Lcom/google/speech/tts/LexiconProto$Builder;->create()Lcom/google/speech/tts/LexiconProto$Builder;
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Builder;->access$5200()Lcom/google/speech/tts/LexiconProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/LexiconProto;)Lcom/google/speech/tts/LexiconProto$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/LexiconProto;

    .prologue
    .line 2752
    invoke-static {}, Lcom/google/speech/tts/LexiconProto;->newBuilder()Lcom/google/speech/tts/LexiconProto$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/LexiconProto$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto;)Lcom/google/speech/tts/LexiconProto$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/LexiconProto;->defaultInstance:Lcom/google/speech/tts/LexiconProto;

    return-object v0
.end method

.method public getHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2615
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto;->header_:Ljava/lang/String;

    return-object v0
.end method

.method public getLegalVowels()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2634
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto;->legalVowels_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 2662
    iget v2, p0, Lcom/google/speech/tts/LexiconProto;->memoizedSerializedSize:I

    .line 2663
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 2679
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 2665
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 2666
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->hasHeader()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2667
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->getHeader()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2670
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->getWList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Entry;

    .line 2671
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Entry;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2673
    goto :goto_1

    .line 2674
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Entry;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->hasLegalVowels()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2675
    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->getLegalVowels()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2678
    :cond_3
    iput v2, p0, Lcom/google/speech/tts/LexiconProto;->memoizedSerializedSize:I

    move v3, v2

    .line 2679
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getWList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2622
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto;->w_:Ljava/util/List;

    return-object v0
.end method

.method public hasHeader()Z
    .locals 1

    .prologue
    .line 2614
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto;->hasHeader:Z

    return v0
.end method

.method public hasLegalVowels()Z
    .locals 1

    .prologue
    .line 2633
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto;->hasLegalVowels:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2639
    iget-boolean v3, p0, Lcom/google/speech/tts/LexiconProto;->hasHeader:Z

    if-nez v3, :cond_0

    .line 2643
    :goto_0
    return v2

    .line 2640
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->getWList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Entry;

    .line 2641
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Entry;
    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 2643
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Entry;
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->toBuilder()Lcom/google/speech/tts/LexiconProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/LexiconProto$Builder;
    .locals 1

    .prologue
    .line 2754
    invoke-static {p0}, Lcom/google/speech/tts/LexiconProto;->newBuilder(Lcom/google/speech/tts/LexiconProto;)Lcom/google/speech/tts/LexiconProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2648
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->getSerializedSize()I

    .line 2649
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->hasHeader()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2650
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->getHeader()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 2652
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->getWList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Entry;

    .line 2653
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Entry;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 2655
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Entry;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->hasLegalVowels()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2656
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto;->getLegalVowels()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 2658
    :cond_2
    return-void
.end method
