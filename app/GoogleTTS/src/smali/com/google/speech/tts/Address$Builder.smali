.class public final Lcom/google/speech/tts/Address$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Address.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Address;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Address;",
        "Lcom/google/speech/tts/Address$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Address;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Address$Builder;
    .locals 1

    .prologue
    .line 296
    invoke-static {}, Lcom/google/speech/tts/Address$Builder;->create()Lcom/google/speech/tts/Address$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Address$Builder;
    .locals 3

    .prologue
    .line 305
    new-instance v0, Lcom/google/speech/tts/Address$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Address$Builder;-><init>()V

    .line 306
    .local v0, "builder":Lcom/google/speech/tts/Address$Builder;
    new-instance v1, Lcom/google/speech/tts/Address;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Address;-><init>(Lcom/google/speech/tts/Address$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    .line 307
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/google/speech/tts/Address$Builder;->build()Lcom/google/speech/tts/Address;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Address;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Address$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    invoke-static {v0}, Lcom/google/speech/tts/Address$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 338
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Address$Builder;->buildPartial()Lcom/google/speech/tts/Address;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Address;
    .locals 3

    .prologue
    .line 351
    iget-object v1, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    if-nez v1, :cond_0

    .line 352
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 355
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # getter for: Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/Address;->access$300(Lcom/google/speech/tts/Address;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 356
    iget-object v1, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    iget-object v2, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # getter for: Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/Address;->access$300(Lcom/google/speech/tts/Address;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/Address;->access$302(Lcom/google/speech/tts/Address;Ljava/util/List;)Ljava/util/List;

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    .line 360
    .local v0, "returnMe":Lcom/google/speech/tts/Address;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    .line 361
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/google/speech/tts/Address$Builder;->clone()Lcom/google/speech/tts/Address$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/google/speech/tts/Address$Builder;->clone()Lcom/google/speech/tts/Address$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Address$Builder;
    .locals 2

    .prologue
    .line 324
    invoke-static {}, Lcom/google/speech/tts/Address$Builder;->create()Lcom/google/speech/tts/Address$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Address$Builder;->mergeFrom(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/google/speech/tts/Address$Builder;->clone()Lcom/google/speech/tts/Address$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    invoke-virtual {v0}, Lcom/google/speech/tts/Address;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 296
    check-cast p1, Lcom/google/speech/tts/Address;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Address$Builder;->mergeFrom(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/Address;

    .prologue
    .line 365
    invoke-static {}, Lcom/google/speech/tts/Address;->getDefaultInstance()Lcom/google/speech/tts/Address;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 405
    :cond_0
    :goto_0
    return-object p0

    .line 366
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->hasThoroughfare()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 367
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->getThoroughfare()Lcom/google/speech/tts/Thoroughfare;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Address$Builder;->mergeThoroughfare(Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Address$Builder;

    .line 369
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->hasDependentLocality()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 370
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->getDependentLocality()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Address$Builder;->setDependentLocality(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;

    .line 372
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->hasLocality()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 373
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->getLocality()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Address$Builder;->setLocality(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;

    .line 375
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->hasSubadminArea()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 376
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->getSubadminArea()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Address$Builder;->setSubadminArea(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;

    .line 378
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->hasAdminArea()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 379
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Address$Builder;->setAdminArea(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;

    .line 381
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->hasCountryName()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 382
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->getCountryName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Address$Builder;->setCountryName(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;

    .line 384
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->hasPostCode()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 385
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->getPostCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Address$Builder;->setPostCode(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;

    .line 387
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->hasPoBox()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 388
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->getPoBox()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Address$Builder;->setPoBox(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;

    .line 390
    :cond_9
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 391
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Address$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;

    .line 393
    :cond_a
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->hasPreserveOrder()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 394
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->getPreserveOrder()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Address$Builder;->setPreserveOrder(Z)Lcom/google/speech/tts/Address$Builder;

    .line 396
    :cond_b
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 397
    invoke-virtual {p1}, Lcom/google/speech/tts/Address;->getCodeSwitch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Address$Builder;->setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;

    .line 399
    :cond_c
    # getter for: Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Address;->access$300(Lcom/google/speech/tts/Address;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # getter for: Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Address;->access$300(Lcom/google/speech/tts/Address;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 401
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$302(Lcom/google/speech/tts/Address;Ljava/util/List;)Ljava/util/List;

    .line 403
    :cond_d
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # getter for: Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/Address;->access$300(Lcom/google/speech/tts/Address;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/Address;->fieldOrder_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/Address;->access$300(Lcom/google/speech/tts/Address;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public mergeThoroughfare(Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Address$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Thoroughfare;

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    invoke-virtual {v0}, Lcom/google/speech/tts/Address;->hasThoroughfare()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # getter for: Lcom/google/speech/tts/Address;->thoroughfare_:Lcom/google/speech/tts/Thoroughfare;
    invoke-static {v0}, Lcom/google/speech/tts/Address;->access$500(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Thoroughfare;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Thoroughfare;->getDefaultInstance()Lcom/google/speech/tts/Thoroughfare;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 504
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    iget-object v1, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # getter for: Lcom/google/speech/tts/Address;->thoroughfare_:Lcom/google/speech/tts/Thoroughfare;
    invoke-static {v1}, Lcom/google/speech/tts/Address;->access$500(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Thoroughfare;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Thoroughfare;->newBuilder(Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Thoroughfare$Builder;->mergeFrom(Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Thoroughfare$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Thoroughfare$Builder;->buildPartial()Lcom/google/speech/tts/Thoroughfare;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/Address;->thoroughfare_:Lcom/google/speech/tts/Thoroughfare;
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$502(Lcom/google/speech/tts/Address;Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Thoroughfare;

    .line 509
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Address;->hasThoroughfare:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$402(Lcom/google/speech/tts/Address;Z)Z

    .line 510
    return-object p0

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # setter for: Lcom/google/speech/tts/Address;->thoroughfare_:Lcom/google/speech/tts/Thoroughfare;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Address;->access$502(Lcom/google/speech/tts/Address;Lcom/google/speech/tts/Thoroughfare;)Lcom/google/speech/tts/Thoroughfare;

    goto :goto_0
.end method

.method public setAdminArea(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 589
    if-nez p1, :cond_0

    .line 590
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Address;->hasAdminArea:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$1202(Lcom/google/speech/tts/Address;Z)Z

    .line 593
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # setter for: Lcom/google/speech/tts/Address;->adminArea_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Address;->access$1302(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;

    .line 594
    return-object p0
.end method

.method public setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 712
    if-nez p1, :cond_0

    .line 713
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 715
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Address;->hasCodeSwitch:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$2402(Lcom/google/speech/tts/Address;Z)Z

    .line 716
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # setter for: Lcom/google/speech/tts/Address;->codeSwitch_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Address;->access$2502(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;

    .line 717
    return-object p0
.end method

.method public setCountryName(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 610
    if-nez p1, :cond_0

    .line 611
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 613
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Address;->hasCountryName:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$1402(Lcom/google/speech/tts/Address;Z)Z

    .line 614
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # setter for: Lcom/google/speech/tts/Address;->countryName_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Address;->access$1502(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;

    .line 615
    return-object p0
.end method

.method public setDependentLocality(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 526
    if-nez p1, :cond_0

    .line 527
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Address;->hasDependentLocality:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$602(Lcom/google/speech/tts/Address;Z)Z

    .line 530
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # setter for: Lcom/google/speech/tts/Address;->dependentLocality_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Address;->access$702(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;

    .line 531
    return-object p0
.end method

.method public setLocality(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 547
    if-nez p1, :cond_0

    .line 548
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 550
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Address;->hasLocality:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$802(Lcom/google/speech/tts/Address;Z)Z

    .line 551
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # setter for: Lcom/google/speech/tts/Address;->locality_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Address;->access$902(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;

    .line 552
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 673
    if-nez p1, :cond_0

    .line 674
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 676
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Address;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$2002(Lcom/google/speech/tts/Address;Z)Z

    .line 677
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # setter for: Lcom/google/speech/tts/Address;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Address;->access$2102(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;

    .line 678
    return-object p0
.end method

.method public setPoBox(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 652
    if-nez p1, :cond_0

    .line 653
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 655
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Address;->hasPoBox:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$1802(Lcom/google/speech/tts/Address;Z)Z

    .line 656
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # setter for: Lcom/google/speech/tts/Address;->poBox_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Address;->access$1902(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;

    .line 657
    return-object p0
.end method

.method public setPostCode(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 631
    if-nez p1, :cond_0

    .line 632
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 634
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Address;->hasPostCode:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$1602(Lcom/google/speech/tts/Address;Z)Z

    .line 635
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # setter for: Lcom/google/speech/tts/Address;->postCode_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Address;->access$1702(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;

    .line 636
    return-object p0
.end method

.method public setPreserveOrder(Z)Lcom/google/speech/tts/Address$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Address;->hasPreserveOrder:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$2202(Lcom/google/speech/tts/Address;Z)Z

    .line 695
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # setter for: Lcom/google/speech/tts/Address;->preserveOrder_:Z
    invoke-static {v0, p1}, Lcom/google/speech/tts/Address;->access$2302(Lcom/google/speech/tts/Address;Z)Z

    .line 696
    return-object p0
.end method

.method public setSubadminArea(Ljava/lang/String;)Lcom/google/speech/tts/Address$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 568
    if-nez p1, :cond_0

    .line 569
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Address;->hasSubadminArea:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Address;->access$1002(Lcom/google/speech/tts/Address;Z)Z

    .line 572
    iget-object v0, p0, Lcom/google/speech/tts/Address$Builder;->result:Lcom/google/speech/tts/Address;

    # setter for: Lcom/google/speech/tts/Address;->subadminArea_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Address;->access$1102(Lcom/google/speech/tts/Address;Ljava/lang/String;)Ljava/lang/String;

    .line 573
    return-object p0
.end method
