.class public final Lcom/google/speech/tts/PiecewiseModelParams$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PiecewiseModelParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/PiecewiseModelParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/PiecewiseModelParams;",
        "Lcom/google/speech/tts/PiecewiseModelParams$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/PiecewiseModelParams;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/PiecewiseModelParams$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-static {}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->create()Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/PiecewiseModelParams$Builder;
    .locals 3

    .prologue
    .line 177
    new-instance v0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;-><init>()V

    .line 178
    .local v0, "builder":Lcom/google/speech/tts/PiecewiseModelParams$Builder;
    new-instance v1, Lcom/google/speech/tts/PiecewiseModelParams;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/PiecewiseModelParams;-><init>(Lcom/google/speech/tts/PiecewiseModelParams$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    .line 179
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->build()Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/PiecewiseModelParams;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    invoke-static {v0}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 210
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->buildPartial()Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/PiecewiseModelParams;
    .locals 3

    .prologue
    .line 223
    iget-object v1, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    if-nez v1, :cond_0

    .line 224
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 227
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    # getter for: Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/PiecewiseModelParams;->access$300(Lcom/google/speech/tts/PiecewiseModelParams;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 228
    iget-object v1, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    iget-object v2, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    # getter for: Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/PiecewiseModelParams;->access$300(Lcom/google/speech/tts/PiecewiseModelParams;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/PiecewiseModelParams;->access$302(Lcom/google/speech/tts/PiecewiseModelParams;Ljava/util/List;)Ljava/util/List;

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    .line 232
    .local v0, "returnMe":Lcom/google/speech/tts/PiecewiseModelParams;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    .line 233
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->clone()Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->clone()Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/PiecewiseModelParams$Builder;
    .locals 2

    .prologue
    .line 196
    invoke-static {}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->create()Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->mergeFrom(Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->clone()Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    invoke-virtual {v0}, Lcom/google/speech/tts/PiecewiseModelParams;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 168
    check-cast p1, Lcom/google/speech/tts/PiecewiseModelParams;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->mergeFrom(Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/PiecewiseModelParams$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/PiecewiseModelParams;

    .prologue
    .line 237
    invoke-static {}, Lcom/google/speech/tts/PiecewiseModelParams;->getDefaultInstance()Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-object p0

    .line 238
    :cond_1
    # getter for: Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/PiecewiseModelParams;->access$300(Lcom/google/speech/tts/PiecewiseModelParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 239
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    # getter for: Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/PiecewiseModelParams;->access$300(Lcom/google/speech/tts/PiecewiseModelParams;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 240
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/PiecewiseModelParams;->access$302(Lcom/google/speech/tts/PiecewiseModelParams;Ljava/util/List;)Ljava/util/List;

    .line 242
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    # getter for: Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/PiecewiseModelParams;->access$300(Lcom/google/speech/tts/PiecewiseModelParams;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/PiecewiseModelParams;->sample_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/PiecewiseModelParams;->access$300(Lcom/google/speech/tts/PiecewiseModelParams;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 244
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/PiecewiseModelParams;->hasDeviation()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 245
    invoke-virtual {p1}, Lcom/google/speech/tts/PiecewiseModelParams;->getDeviation()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->setDeviation(F)Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    .line 247
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/PiecewiseModelParams;->hasTimeAddressing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    invoke-virtual {p1}, Lcom/google/speech/tts/PiecewiseModelParams;->getTimeAddressing()Lcom/google/speech/tts/TimeAddressingType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->setTimeAddressing(Lcom/google/speech/tts/TimeAddressingType;)Lcom/google/speech/tts/PiecewiseModelParams$Builder;

    goto :goto_0
.end method

.method public setDeviation(F)Lcom/google/speech/tts/PiecewiseModelParams$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/PiecewiseModelParams;->hasDeviation:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/PiecewiseModelParams;->access$402(Lcom/google/speech/tts/PiecewiseModelParams;Z)Z

    .line 351
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    # setter for: Lcom/google/speech/tts/PiecewiseModelParams;->deviation_:F
    invoke-static {v0, p1}, Lcom/google/speech/tts/PiecewiseModelParams;->access$502(Lcom/google/speech/tts/PiecewiseModelParams;F)F

    .line 352
    return-object p0
.end method

.method public setTimeAddressing(Lcom/google/speech/tts/TimeAddressingType;)Lcom/google/speech/tts/PiecewiseModelParams$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/TimeAddressingType;

    .prologue
    .line 368
    if-nez p1, :cond_0

    .line 369
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/PiecewiseModelParams;->hasTimeAddressing:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/PiecewiseModelParams;->access$602(Lcom/google/speech/tts/PiecewiseModelParams;Z)Z

    .line 372
    iget-object v0, p0, Lcom/google/speech/tts/PiecewiseModelParams$Builder;->result:Lcom/google/speech/tts/PiecewiseModelParams;

    # setter for: Lcom/google/speech/tts/PiecewiseModelParams;->timeAddressing_:Lcom/google/speech/tts/TimeAddressingType;
    invoke-static {v0, p1}, Lcom/google/speech/tts/PiecewiseModelParams;->access$702(Lcom/google/speech/tts/PiecewiseModelParams;Lcom/google/speech/tts/TimeAddressingType;)Lcom/google/speech/tts/TimeAddressingType;

    .line 373
    return-object p0
.end method
