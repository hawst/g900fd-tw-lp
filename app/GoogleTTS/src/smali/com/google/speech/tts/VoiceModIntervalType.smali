.class public final enum Lcom/google/speech/tts/VoiceModIntervalType;
.super Ljava/lang/Enum;
.source "VoiceModIntervalType.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/VoiceModIntervalType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/VoiceModIntervalType;

.field public static final enum LINGUISTIC_ITEM:Lcom/google/speech/tts/VoiceModIntervalType;

.field public static final enum SPAN_END:Lcom/google/speech/tts/VoiceModIntervalType;

.field public static final enum SPAN_START:Lcom/google/speech/tts/VoiceModIntervalType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/VoiceModIntervalType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lcom/google/speech/tts/VoiceModIntervalType;

    const-string v1, "LINGUISTIC_ITEM"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/speech/tts/VoiceModIntervalType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModIntervalType;->LINGUISTIC_ITEM:Lcom/google/speech/tts/VoiceModIntervalType;

    .line 8
    new-instance v0, Lcom/google/speech/tts/VoiceModIntervalType;

    const-string v1, "SPAN_START"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/speech/tts/VoiceModIntervalType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModIntervalType;->SPAN_START:Lcom/google/speech/tts/VoiceModIntervalType;

    .line 9
    new-instance v0, Lcom/google/speech/tts/VoiceModIntervalType;

    const-string v1, "SPAN_END"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/google/speech/tts/VoiceModIntervalType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModIntervalType;->SPAN_END:Lcom/google/speech/tts/VoiceModIntervalType;

    .line 5
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/speech/tts/VoiceModIntervalType;

    sget-object v1, Lcom/google/speech/tts/VoiceModIntervalType;->LINGUISTIC_ITEM:Lcom/google/speech/tts/VoiceModIntervalType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/speech/tts/VoiceModIntervalType;->SPAN_START:Lcom/google/speech/tts/VoiceModIntervalType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/speech/tts/VoiceModIntervalType;->SPAN_END:Lcom/google/speech/tts/VoiceModIntervalType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/speech/tts/VoiceModIntervalType;->$VALUES:[Lcom/google/speech/tts/VoiceModIntervalType;

    .line 29
    new-instance v0, Lcom/google/speech/tts/VoiceModIntervalType$1;

    invoke-direct {v0}, Lcom/google/speech/tts/VoiceModIntervalType$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/VoiceModIntervalType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput p3, p0, Lcom/google/speech/tts/VoiceModIntervalType;->index:I

    .line 40
    iput p4, p0, Lcom/google/speech/tts/VoiceModIntervalType;->value:I

    .line 41
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/VoiceModIntervalType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/google/speech/tts/VoiceModIntervalType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/VoiceModIntervalType;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/VoiceModIntervalType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/google/speech/tts/VoiceModIntervalType;->$VALUES:[Lcom/google/speech/tts/VoiceModIntervalType;

    invoke-virtual {v0}, [Lcom/google/speech/tts/VoiceModIntervalType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/VoiceModIntervalType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/google/speech/tts/VoiceModIntervalType;->value:I

    return v0
.end method
