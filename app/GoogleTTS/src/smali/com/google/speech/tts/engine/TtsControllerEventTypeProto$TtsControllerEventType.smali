.class public final enum Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;
.super Ljava/lang/Enum;
.source "TtsControllerEventTypeProto.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/TtsControllerEventTypeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TtsControllerEventType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

.field public static final enum TTS_EVENT_AUDIO_BUFFER_READY:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

.field public static final enum TTS_EVENT_AUDIO_FINISHED:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

.field public static final enum TTS_EVENT_PRONUNCIATIONS_READY:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

.field public static final enum TTS_EVENT_SIGNAL:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    const-string v1, "TTS_EVENT_SIGNAL"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->TTS_EVENT_SIGNAL:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    .line 14
    new-instance v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    const-string v1, "TTS_EVENT_PRONUNCIATIONS_READY"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->TTS_EVENT_PRONUNCIATIONS_READY:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    .line 15
    new-instance v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    const-string v1, "TTS_EVENT_AUDIO_BUFFER_READY"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->TTS_EVENT_AUDIO_BUFFER_READY:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    .line 16
    new-instance v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    const-string v1, "TTS_EVENT_AUDIO_FINISHED"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->TTS_EVENT_AUDIO_FINISHED:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    .line 11
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    sget-object v1, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->TTS_EVENT_SIGNAL:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->TTS_EVENT_PRONUNCIATIONS_READY:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->TTS_EVENT_AUDIO_BUFFER_READY:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->TTS_EVENT_AUDIO_FINISHED:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->$VALUES:[Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    .line 37
    new-instance v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType$1;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput p3, p0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->index:I

    .line 48
    iput p4, p0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->value:I

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->$VALUES:[Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    invoke-virtual {v0}, [Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->value:I

    return v0
.end method
