.class public final Lcom/google/speech/tts/Sentence;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Sentence.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Sentence$1;,
        Lcom/google/speech/tts/Sentence$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Sentence;


# instance fields
.field private deprecatedExclude_:Lcom/google/protobuf/ByteString;

.field private entry_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private hasDeprecatedExclude:Z

.field private hasMorphing:Z

.field private hasStyle:Z

.field private memoizedSerializedSize:I

.field private morphing_:Lcom/google/speech/tts/SpeechMorphingTargets;

.field private say_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Say;",
            ">;"
        }
    .end annotation
.end field

.field private style_:Lcom/google/speech/tts/Style;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 558
    new-instance v0, Lcom/google/speech/tts/Sentence;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Sentence;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Sentence;->defaultInstance:Lcom/google/speech/tts/Sentence;

    .line 559
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 560
    sget-object v0, Lcom/google/speech/tts/Sentence;->defaultInstance:Lcom/google/speech/tts/Sentence;

    invoke-direct {v0}, Lcom/google/speech/tts/Sentence;->initFields()V

    .line 561
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;

    .line 56
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/speech/tts/Sentence;->deprecatedExclude_:Lcom/google/protobuf/ByteString;

    .line 101
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Sentence;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Sentence;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Sentence$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Sentence$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Sentence;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;

    .line 56
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/speech/tts/Sentence;->deprecatedExclude_:Lcom/google/protobuf/ByteString;

    .line 101
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Sentence;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1000(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/SpeechMorphingTargets;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Sentence;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Sentence;->morphing_:Lcom/google/speech/tts/SpeechMorphingTargets;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Sentence;Lcom/google/speech/tts/SpeechMorphingTargets;)Lcom/google/speech/tts/SpeechMorphingTargets;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Sentence;
    .param p1, "x1"    # Lcom/google/speech/tts/SpeechMorphingTargets;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Sentence;->morphing_:Lcom/google/speech/tts/SpeechMorphingTargets;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Sentence;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Sentence;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Sentence;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Sentence;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/speech/tts/Sentence;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Sentence;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Sentence;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Sentence;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Sentence;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Sentence;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Sentence;->hasStyle:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Style;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Sentence;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Sentence;->style_:Lcom/google/speech/tts/Style;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Sentence;Lcom/google/speech/tts/Style;)Lcom/google/speech/tts/Style;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Sentence;
    .param p1, "x1"    # Lcom/google/speech/tts/Style;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Sentence;->style_:Lcom/google/speech/tts/Style;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Sentence;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Sentence;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Sentence;->hasDeprecatedExclude:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Sentence;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Sentence;
    .param p1, "x1"    # Lcom/google/protobuf/ByteString;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Sentence;->deprecatedExclude_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Sentence;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Sentence;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Sentence;->hasMorphing:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Sentence;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Sentence;->defaultInstance:Lcom/google/speech/tts/Sentence;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/speech/tts/Style;->getDefaultInstance()Lcom/google/speech/tts/Style;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Sentence;->style_:Lcom/google/speech/tts/Style;

    .line 69
    invoke-static {}, Lcom/google/speech/tts/SpeechMorphingTargets;->getDefaultInstance()Lcom/google/speech/tts/SpeechMorphingTargets;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Sentence;->morphing_:Lcom/google/speech/tts/SpeechMorphingTargets;

    .line 70
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Sentence$Builder;
    .locals 1

    .prologue
    .line 198
    # invokes: Lcom/google/speech/tts/Sentence$Builder;->create()Lcom/google/speech/tts/Sentence$Builder;
    invoke-static {}, Lcom/google/speech/tts/Sentence$Builder;->access$100()Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Sentence;

    .prologue
    .line 201
    invoke-static {}, Lcom/google/speech/tts/Sentence;->newBuilder()Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Sentence$Builder;->mergeFrom(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getDefaultInstanceForType()Lcom/google/speech/tts/Sentence;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Sentence;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Sentence;->defaultInstance:Lcom/google/speech/tts/Sentence;

    return-object v0
.end method

.method public getDeprecatedExclude()Lcom/google/protobuf/ByteString;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/speech/tts/Sentence;->deprecatedExclude_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getEntryCount()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/speech/tts/Sentence;->entry_:Ljava/util/List;

    return-object v0
.end method

.method public getMorphing()Lcom/google/speech/tts/SpeechMorphingTargets;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/speech/tts/Sentence;->morphing_:Lcom/google/speech/tts/SpeechMorphingTargets;

    return-object v0
.end method

.method public getSay(I)Lcom/google/speech/tts/Say;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Say;

    return-object v0
.end method

.method public getSayCount()I
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSayList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Say;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Sentence;->say_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 103
    iget v2, p0, Lcom/google/speech/tts/Sentence;->memoizedSerializedSize:I

    .line 104
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 128
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 106
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 107
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getSayList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Say;

    .line 108
    .local v0, "element":Lcom/google/speech/tts/Say;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 110
    goto :goto_1

    .line 111
    .end local v0    # "element":Lcom/google/speech/tts/Say;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getEntryList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Entry;

    .line 112
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Entry;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 114
    goto :goto_2

    .line 115
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Entry;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->hasStyle()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 116
    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getStyle()Lcom/google/speech/tts/Style;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 119
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->hasDeprecatedExclude()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 120
    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getDeprecatedExclude()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    .line 123
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->hasMorphing()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 124
    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getMorphing()Lcom/google/speech/tts/SpeechMorphingTargets;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 127
    :cond_5
    iput v2, p0, Lcom/google/speech/tts/Sentence;->memoizedSerializedSize:I

    move v3, v2

    .line 128
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getStyle()Lcom/google/speech/tts/Style;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/speech/tts/Sentence;->style_:Lcom/google/speech/tts/Style;

    return-object v0
.end method

.method public hasDeprecatedExclude()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/speech/tts/Sentence;->hasDeprecatedExclude:Z

    return v0
.end method

.method public hasMorphing()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/google/speech/tts/Sentence;->hasMorphing:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/speech/tts/Sentence;->hasStyle:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getSayList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Say;

    .line 73
    .local v0, "element":Lcom/google/speech/tts/Say;
    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 78
    .end local v0    # "element":Lcom/google/speech/tts/Say;
    :goto_0
    return v2

    .line 75
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getEntryList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Entry;

    .line 76
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Entry;
    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 78
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Entry;
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->toBuilder()Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Sentence$Builder;
    .locals 1

    .prologue
    .line 203
    invoke-static {p0}, Lcom/google/speech/tts/Sentence;->newBuilder(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getSerializedSize()I

    .line 84
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getSayList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/Say;

    .line 85
    .local v0, "element":Lcom/google/speech/tts/Say;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 87
    .end local v0    # "element":Lcom/google/speech/tts/Say;
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getEntryList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Entry;

    .line 88
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Entry;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 90
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Entry;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 91
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getStyle()Lcom/google/speech/tts/Style;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 93
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->hasDeprecatedExclude()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 94
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getDeprecatedExclude()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 96
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->hasMorphing()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 97
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getMorphing()Lcom/google/speech/tts/SpeechMorphingTargets;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 99
    :cond_4
    return-void
.end method
