.class public final Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "TtsControllerEventProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/TtsControllerEventProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TtsControllerEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

.field public static final logId:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension",
            "<",
            "Lcom/google/speech/recognizer/Loggable;",
            "Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private audio_:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

.field private eventType_:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

.field private hasAudio:Z

.field private hasEventType:Z

.field private hasReadStatus:Z

.field private hasTimestamp:Z

.field private hasUtterance:Z

.field private memoizedSerializedSize:I

.field private readStatus_:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

.field private timestamp_:J

.field private utterance_:Lcom/google/speech/patts/UtteranceP;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->newGeneratedExtension()Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    move-result-object v0

    sput-object v0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->logId:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    .line 489
    new-instance v0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->defaultInstance:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    .line 490
    invoke-static {}, Lcom/google/speech/tts/engine/TtsControllerEventProto;->internalForceInit()V

    .line 491
    sget-object v0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->defaultInstance:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->initFields()V

    .line 492
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 46
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->timestamp_:J

    .line 105
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->memoizedSerializedSize:I

    .line 16
    invoke-direct {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->initFields()V

    .line 17
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/engine/TtsControllerEventProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$1;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 46
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->timestamp_:J

    .line 105
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->memoizedSerializedSize:I

    .line 18
    return-void
.end method

.method static synthetic access$1000(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->audio_:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .param p1, "x1"    # Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->audio_:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasReadStatus:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;)Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .param p1, "x1"    # Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->readStatus_:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasEventType:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;)Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .param p1, "x1"    # Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->eventType_:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasTimestamp:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .param p1, "x1"    # J

    .prologue
    .line 12
    iput-wide p1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->timestamp_:J

    return-wide p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasUtterance:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;)Lcom/google/speech/patts/UtteranceP;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->utterance_:Lcom/google/speech/patts/UtteranceP;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/patts/UtteranceP;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .param p1, "x1"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->utterance_:Lcom/google/speech/patts/UtteranceP;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasAudio:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->defaultInstance:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->TTS_EVENT_SIGNAL:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    iput-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->eventType_:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    .line 73
    invoke-static {}, Lcom/google/speech/patts/UtteranceP;->getDefaultInstance()Lcom/google/speech/patts/UtteranceP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->utterance_:Lcom/google/speech/patts/UtteranceP;

    .line 74
    invoke-static {}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->getDefaultInstance()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->audio_:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    .line 75
    sget-object v0, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->READ_STATUS_ERROR:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    iput-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->readStatus_:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    .line 76
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    .locals 1

    .prologue
    .line 202
    # invokes: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->create()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    invoke-static {}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->access$100()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    .prologue
    .line 205
    invoke-static {}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->newBuilder()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->mergeFrom(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAudio()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->audio_:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getDefaultInstanceForType()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->defaultInstance:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    return-object v0
.end method

.method public getEventType()Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->eventType_:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    return-object v0
.end method

.method public getReadStatus()Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->readStatus_:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 107
    iget v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->memoizedSerializedSize:I

    .line 108
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 132
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 110
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 111
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasEventType()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 112
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getEventType()Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 115
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasTimestamp()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 116
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getTimestamp()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 119
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasUtterance()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 120
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getUtterance()Lcom/google/speech/patts/UtteranceP;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 123
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasAudio()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 124
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getAudio()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 127
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasReadStatus()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 128
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getReadStatus()Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 131
    :cond_5
    iput v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->memoizedSerializedSize:I

    move v1, v0

    .line 132
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->timestamp_:J

    return-wide v0
.end method

.method public getUtterance()Lcom/google/speech/patts/UtteranceP;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->utterance_:Lcom/google/speech/patts/UtteranceP;

    return-object v0
.end method

.method public hasAudio()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasAudio:Z

    return v0
.end method

.method public hasEventType()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasEventType:Z

    return v0
.end method

.method public hasReadStatus()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasReadStatus:Z

    return v0
.end method

.method public hasTimestamp()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasTimestamp:Z

    return v0
.end method

.method public hasUtterance()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasUtterance:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 78
    iget-boolean v1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasEventType:Z

    if-nez v1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 79
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasUtterance()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 80
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getUtterance()Lcom/google/speech/patts/UtteranceP;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/UtteranceP;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->toBuilder()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    .locals 1

    .prologue
    .line 207
    invoke-static {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->newBuilder(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getSerializedSize()I

    .line 88
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasEventType()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getEventType()Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasTimestamp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getTimestamp()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 94
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasUtterance()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getUtterance()Lcom/google/speech/patts/UtteranceP;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 97
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasAudio()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 98
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getAudio()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 100
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasReadStatus()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 101
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getReadStatus()Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 103
    :cond_4
    return-void
.end method
