.class public final Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "TtsControllerEventProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;",
        "Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    .locals 1

    .prologue
    .line 209
    invoke-static {}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->create()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    .locals 3

    .prologue
    .line 218
    new-instance v0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;-><init>()V

    .line 219
    .local v0, "builder":Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    new-instance v1, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;-><init>(Lcom/google/speech/tts/engine/TtsControllerEventProto$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    .line 220
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->build()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    invoke-static {v0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 251
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->buildPartial()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    .locals 3

    .prologue
    .line 264
    iget-object v1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    if-nez v1, :cond_0

    .line 265
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    .line 269
    .local v0, "returnMe":Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    .line 270
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->clone()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->clone()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    .locals 2

    .prologue
    .line 237
    invoke-static {}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->create()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->mergeFrom(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->clone()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    invoke-virtual {v0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeAudio(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    invoke-virtual {v0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasAudio()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    # getter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->audio_:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$1000(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->getDefaultInstance()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 450
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    iget-object v1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    # getter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->audio_:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    invoke-static {v1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$1000(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->newBuilder(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->mergeFrom(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->buildPartial()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->audio_:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$1002(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    .line 455
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasAudio:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$902(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Z)Z

    .line 456
    return-object p0

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    # setter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->audio_:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$1002(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 209
    check-cast p1, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->mergeFrom(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    .prologue
    .line 274
    invoke-static {}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getDefaultInstance()Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 290
    :cond_0
    :goto_0
    return-object p0

    .line 275
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasEventType()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 276
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getEventType()Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->setEventType(Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    .line 278
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasTimestamp()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 279
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getTimestamp()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->setTimestamp(J)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    .line 281
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasUtterance()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 282
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getUtterance()Lcom/google/speech/patts/UtteranceP;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->mergeUtterance(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    .line 284
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasAudio()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 285
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getAudio()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->mergeAudio(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    .line 287
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasReadStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->getReadStatus()Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->setReadStatus(Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;

    goto :goto_0
.end method

.method public mergeUtterance(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    invoke-virtual {v0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasUtterance()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    # getter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->utterance_:Lcom/google/speech/patts/UtteranceP;
    invoke-static {v0}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$800(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;)Lcom/google/speech/patts/UtteranceP;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/UtteranceP;->getDefaultInstance()Lcom/google/speech/patts/UtteranceP;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 413
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    iget-object v1, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    # getter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->utterance_:Lcom/google/speech/patts/UtteranceP;
    invoke-static {v1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$800(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;)Lcom/google/speech/patts/UtteranceP;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/UtteranceP;->newBuilder(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/UtteranceP$Builder;->mergeFrom(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/UtteranceP$Builder;->buildPartial()Lcom/google/speech/patts/UtteranceP;

    move-result-object v1

    # setter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->utterance_:Lcom/google/speech/patts/UtteranceP;
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$802(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/patts/UtteranceP;

    .line 418
    :goto_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasUtterance:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$702(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Z)Z

    .line 419
    return-object p0

    .line 416
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    # setter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->utterance_:Lcom/google/speech/patts/UtteranceP;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$802(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/patts/UtteranceP;

    goto :goto_0
.end method

.method public setEventType(Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    .prologue
    .line 359
    if-nez p1, :cond_0

    .line 360
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasEventType:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$302(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Z)Z

    .line 363
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    # setter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->eventType_:Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$402(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;)Lcom/google/speech/tts/engine/TtsControllerEventTypeProto$TtsControllerEventType;

    .line 364
    return-object p0
.end method

.method public setReadStatus(Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    .prologue
    .line 472
    if-nez p1, :cond_0

    .line 473
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 475
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasReadStatus:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$1102(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Z)Z

    .line 476
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    # setter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->readStatus_:Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$1202(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;)Lcom/google/speech/tts/engine/ReadStatusProto$ReadStatus;

    .line 477
    return-object p0
.end method

.method public setTimestamp(J)Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->hasTimestamp:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$502(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;Z)Z

    .line 381
    iget-object v0, p0, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent$Builder;->result:Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;

    # setter for: Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->timestamp_:J
    invoke-static {v0, p1, p2}, Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;->access$602(Lcom/google/speech/tts/engine/TtsControllerEventProto$TtsControllerEvent;J)J

    .line 382
    return-object p0
.end method
