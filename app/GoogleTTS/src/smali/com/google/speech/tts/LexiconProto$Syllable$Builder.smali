.class public final Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto$Syllable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/LexiconProto$Syllable;",
        "Lcom/google/speech/tts/LexiconProto$Syllable$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/LexiconProto$Syllable;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1118
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1900()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
    .locals 1

    .prologue
    .line 1112
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->create()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
    .locals 3

    .prologue
    .line 1121
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;-><init>()V

    .line 1122
    .local v0, "builder":Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
    new-instance v1, Lcom/google/speech/tts/LexiconProto$Syllable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/LexiconProto$Syllable;-><init>(Lcom/google/speech/tts/LexiconProto$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    .line 1123
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1112
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->build()Lcom/google/speech/tts/LexiconProto$Syllable;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/LexiconProto$Syllable;
    .locals 1

    .prologue
    .line 1151
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1152
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1154
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->buildPartial()Lcom/google/speech/tts/LexiconProto$Syllable;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/LexiconProto$Syllable;
    .locals 3

    .prologue
    .line 1167
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    if-nez v1, :cond_0

    .line 1168
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1171
    :cond_0
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    # getter for: Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2100(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 1172
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    iget-object v2, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    # getter for: Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2100(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2102(Lcom/google/speech/tts/LexiconProto$Syllable;Ljava/util/List;)Ljava/util/List;

    .line 1175
    :cond_1
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    # getter for: Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2200(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 1176
    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    iget-object v2, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    # getter for: Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2200(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2202(Lcom/google/speech/tts/LexiconProto$Syllable;Ljava/util/List;)Ljava/util/List;

    .line 1179
    :cond_2
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    .line 1180
    .local v0, "returnMe":Lcom/google/speech/tts/LexiconProto$Syllable;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    .line 1181
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1112
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1112
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
    .locals 2

    .prologue
    .line 1140
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->create()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Syllable;)Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1112
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->clone()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 1148
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$Syllable;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 1112
    check-cast p1, Lcom/google/speech/tts/LexiconProto$Syllable;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Syllable;)Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/LexiconProto$Syllable;)Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/tts/LexiconProto$Syllable;

    .prologue
    .line 1185
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Syllable;->getDefaultInstance()Lcom/google/speech/tts/LexiconProto$Syllable;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1204
    :cond_0
    :goto_0
    return-object p0

    .line 1186
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Syllable;->hasStress()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1187
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Syllable;->getStress()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->setStress(I)Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    .line 1189
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Syllable;->hasVowel()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1190
    invoke-virtual {p1}, Lcom/google/speech/tts/LexiconProto$Syllable;->getVowel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->setVowel(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    .line 1192
    :cond_3
    # getter for: Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2100(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1193
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    # getter for: Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2100(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1194
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2102(Lcom/google/speech/tts/LexiconProto$Syllable;Ljava/util/List;)Ljava/util/List;

    .line 1196
    :cond_4
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    # getter for: Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2100(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2100(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1198
    :cond_5
    # getter for: Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2200(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1199
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    # getter for: Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2200(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1200
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2202(Lcom/google/speech/tts/LexiconProto$Syllable;Ljava/util/List;)Ljava/util/List;

    .line 1202
    :cond_6
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    # getter for: Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2200(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2200(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public setStress(I)Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1251
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Syllable;->hasStress:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2302(Lcom/google/speech/tts/LexiconProto$Syllable;Z)Z

    .line 1252
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    # setter for: Lcom/google/speech/tts/LexiconProto$Syllable;->stress_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2402(Lcom/google/speech/tts/LexiconProto$Syllable;I)I

    .line 1253
    return-object p0
.end method

.method public setVowel(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1269
    if-nez p1, :cond_0

    .line 1270
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1272
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/LexiconProto$Syllable;->hasVowel:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2502(Lcom/google/speech/tts/LexiconProto$Syllable;Z)Z

    .line 1273
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->result:Lcom/google/speech/tts/LexiconProto$Syllable;

    # setter for: Lcom/google/speech/tts/LexiconProto$Syllable;->vowel_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/LexiconProto$Syllable;->access$2602(Lcom/google/speech/tts/LexiconProto$Syllable;Ljava/lang/String;)Ljava/lang/String;

    .line 1274
    return-object p0
.end method
