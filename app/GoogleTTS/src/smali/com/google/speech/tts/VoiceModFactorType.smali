.class public final enum Lcom/google/speech/tts/VoiceModFactorType;
.super Ljava/lang/Enum;
.source "VoiceModFactorType.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/VoiceModFactorType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/VoiceModFactorType;

.field public static final enum DIPLOPHONIA:Lcom/google/speech/tts/VoiceModFactorType;

.field public static final enum DURATION:Lcom/google/speech/tts/VoiceModFactorType;

.field public static final enum F0:Lcom/google/speech/tts/VoiceModFactorType;

.field public static final enum JITTER:Lcom/google/speech/tts/VoiceModFactorType;

.field public static final enum POWER:Lcom/google/speech/tts/VoiceModFactorType;

.field public static final enum SHIMMER:Lcom/google/speech/tts/VoiceModFactorType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/VoiceModFactorType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 7
    new-instance v0, Lcom/google/speech/tts/VoiceModFactorType;

    const-string v1, "F0"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/speech/tts/VoiceModFactorType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModFactorType;->F0:Lcom/google/speech/tts/VoiceModFactorType;

    .line 8
    new-instance v0, Lcom/google/speech/tts/VoiceModFactorType;

    const-string v1, "DURATION"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/google/speech/tts/VoiceModFactorType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModFactorType;->DURATION:Lcom/google/speech/tts/VoiceModFactorType;

    .line 9
    new-instance v0, Lcom/google/speech/tts/VoiceModFactorType;

    const-string v1, "POWER"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/google/speech/tts/VoiceModFactorType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModFactorType;->POWER:Lcom/google/speech/tts/VoiceModFactorType;

    .line 10
    new-instance v0, Lcom/google/speech/tts/VoiceModFactorType;

    const-string v1, "JITTER"

    invoke-direct {v0, v1, v8, v8, v8}, Lcom/google/speech/tts/VoiceModFactorType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModFactorType;->JITTER:Lcom/google/speech/tts/VoiceModFactorType;

    .line 11
    new-instance v0, Lcom/google/speech/tts/VoiceModFactorType;

    const-string v1, "SHIMMER"

    invoke-direct {v0, v1, v9, v9, v9}, Lcom/google/speech/tts/VoiceModFactorType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModFactorType;->SHIMMER:Lcom/google/speech/tts/VoiceModFactorType;

    .line 12
    new-instance v0, Lcom/google/speech/tts/VoiceModFactorType;

    const-string v1, "DIPLOPHONIA"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/VoiceModFactorType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/VoiceModFactorType;->DIPLOPHONIA:Lcom/google/speech/tts/VoiceModFactorType;

    .line 5
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/speech/tts/VoiceModFactorType;

    sget-object v1, Lcom/google/speech/tts/VoiceModFactorType;->F0:Lcom/google/speech/tts/VoiceModFactorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/speech/tts/VoiceModFactorType;->DURATION:Lcom/google/speech/tts/VoiceModFactorType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/speech/tts/VoiceModFactorType;->POWER:Lcom/google/speech/tts/VoiceModFactorType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/speech/tts/VoiceModFactorType;->JITTER:Lcom/google/speech/tts/VoiceModFactorType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/speech/tts/VoiceModFactorType;->SHIMMER:Lcom/google/speech/tts/VoiceModFactorType;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/speech/tts/VoiceModFactorType;->DIPLOPHONIA:Lcom/google/speech/tts/VoiceModFactorType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/speech/tts/VoiceModFactorType;->$VALUES:[Lcom/google/speech/tts/VoiceModFactorType;

    .line 35
    new-instance v0, Lcom/google/speech/tts/VoiceModFactorType$1;

    invoke-direct {v0}, Lcom/google/speech/tts/VoiceModFactorType$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/VoiceModFactorType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lcom/google/speech/tts/VoiceModFactorType;->index:I

    .line 46
    iput p4, p0, Lcom/google/speech/tts/VoiceModFactorType;->value:I

    .line 47
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/VoiceModFactorType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/google/speech/tts/VoiceModFactorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/VoiceModFactorType;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/VoiceModFactorType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/google/speech/tts/VoiceModFactorType;->$VALUES:[Lcom/google/speech/tts/VoiceModFactorType;

    invoke-virtual {v0}, [Lcom/google/speech/tts/VoiceModFactorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/VoiceModFactorType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/google/speech/tts/VoiceModFactorType;->value:I

    return v0
.end method
