.class public final enum Lcom/google/speech/tts/LexiconProto$Entry$Syntax;
.super Ljava/lang/Enum;
.source "LexiconProto.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto$Entry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Syntax"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/tts/LexiconProto$Entry$Syntax;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum CONTENT:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum FIRST_NAME:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum FOREIGN:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum FOREIGN_FUNCTION_AR:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum FOREIGN_LETTER:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum FUNCTION:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum FUNCTION_AR:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum FUNCTION_NR:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum LAST_NAME:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum LETTER:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum LSEQ_EXCEPTION:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum NAME:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum PLACE:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum SILENCE:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum SPELLING_EXCEPTION:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field public static final enum UNSPECIFIED:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Entry$Syntax;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1974
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "UNSPECIFIED"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->UNSPECIFIED:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1975
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "FUNCTION"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FUNCTION:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1976
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "CONTENT"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->CONTENT:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1977
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v8, v8, v8}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->NAME:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1978
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "PLACE"

    invoke-direct {v0, v1, v9, v9, v9}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->PLACE:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1979
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "FOREIGN"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FOREIGN:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1980
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "FIRST_NAME"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FIRST_NAME:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1981
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "LAST_NAME"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->LAST_NAME:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1982
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "FUNCTION_NR"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FUNCTION_NR:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1983
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "FUNCTION_AR"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FUNCTION_AR:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1984
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "FOREIGN_FUNCTION_AR"

    const/16 v2, 0xa

    const/16 v3, 0xa

    const/16 v4, 0xa

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FOREIGN_FUNCTION_AR:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1985
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "SILENCE"

    const/16 v2, 0xb

    const/16 v3, 0xb

    const/16 v4, 0xb

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->SILENCE:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1986
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "SPELLING_EXCEPTION"

    const/16 v2, 0xc

    const/16 v3, 0xc

    const/16 v4, 0xc

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->SPELLING_EXCEPTION:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1987
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "LSEQ_EXCEPTION"

    const/16 v2, 0xd

    const/16 v3, 0xd

    const/16 v4, 0xd

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->LSEQ_EXCEPTION:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1988
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "LETTER"

    const/16 v2, 0xe

    const/16 v3, 0xe

    const/16 v4, 0xe

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->LETTER:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1989
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    const-string v1, "FOREIGN_LETTER"

    const/16 v2, 0xf

    const/16 v3, 0xf

    const/16 v4, 0xf

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FOREIGN_LETTER:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 1972
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->UNSPECIFIED:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FUNCTION:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->CONTENT:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->NAME:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->PLACE:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FOREIGN:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FIRST_NAME:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->LAST_NAME:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FUNCTION_NR:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FUNCTION_AR:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FOREIGN_FUNCTION_AR:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->SILENCE:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->SPELLING_EXCEPTION:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->LSEQ_EXCEPTION:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->LETTER:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->FOREIGN_LETTER:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->$VALUES:[Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 2022
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax$1;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax$1;-><init>()V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2031
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2032
    iput p3, p0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->index:I

    .line 2033
    iput p4, p0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->value:I

    .line 2034
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Entry$Syntax;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1972
    const-class v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/tts/LexiconProto$Entry$Syntax;
    .locals 1

    .prologue
    .line 1972
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->$VALUES:[Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    invoke-virtual {v0}, [Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 1993
    iget v0, p0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->value:I

    return v0
.end method
