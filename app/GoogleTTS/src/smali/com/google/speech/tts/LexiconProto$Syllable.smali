.class public final Lcom/google/speech/tts/LexiconProto$Syllable;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LexiconProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/LexiconProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Syllable"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/LexiconProto$Syllable;


# instance fields
.field private coda_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasStress:Z

.field private hasVowel:Z

.field private memoizedSerializedSize:I

.field private onset_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private stress_:I

.field private vowel_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1366
    new-instance v0, Lcom/google/speech/tts/LexiconProto$Syllable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/LexiconProto$Syllable;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/LexiconProto$Syllable;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Syllable;

    .line 1367
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 1368
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Syllable;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Syllable;

    invoke-direct {v0}, Lcom/google/speech/tts/LexiconProto$Syllable;->initFields()V

    .line 1369
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 925
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 942
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->stress_:I

    .line 949
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->vowel_:Ljava/lang/String;

    .line 955
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;

    .line 967
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;

    .line 1002
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->memoizedSerializedSize:I

    .line 926
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->initFields()V

    .line 927
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/LexiconProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/LexiconProto$1;

    .prologue
    .line 922
    invoke-direct {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 928
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 942
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->stress_:I

    .line 949
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->vowel_:Ljava/lang/String;

    .line 955
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;

    .line 967
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;

    .line 1002
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->memoizedSerializedSize:I

    .line 928
    return-void
.end method

.method static synthetic access$2100(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Syllable;

    .prologue
    .line 922
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/google/speech/tts/LexiconProto$Syllable;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Syllable;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 922
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/google/speech/tts/LexiconProto$Syllable;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Syllable;

    .prologue
    .line 922
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/speech/tts/LexiconProto$Syllable;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Syllable;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 922
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2302(Lcom/google/speech/tts/LexiconProto$Syllable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Syllable;
    .param p1, "x1"    # Z

    .prologue
    .line 922
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->hasStress:Z

    return p1
.end method

.method static synthetic access$2402(Lcom/google/speech/tts/LexiconProto$Syllable;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Syllable;
    .param p1, "x1"    # I

    .prologue
    .line 922
    iput p1, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->stress_:I

    return p1
.end method

.method static synthetic access$2502(Lcom/google/speech/tts/LexiconProto$Syllable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Syllable;
    .param p1, "x1"    # Z

    .prologue
    .line 922
    iput-boolean p1, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->hasVowel:Z

    return p1
.end method

.method static synthetic access$2602(Lcom/google/speech/tts/LexiconProto$Syllable;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/LexiconProto$Syllable;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 922
    iput-object p1, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->vowel_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/LexiconProto$Syllable;
    .locals 1

    .prologue
    .line 932
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Syllable;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Syllable;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 978
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
    .locals 1

    .prologue
    .line 1105
    # invokes: Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->create()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->access$1900()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/LexiconProto$Syllable;)Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/LexiconProto$Syllable;

    .prologue
    .line 1108
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Syllable;->newBuilder()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/LexiconProto$Syllable$Builder;->mergeFrom(Lcom/google/speech/tts/LexiconProto$Syllable;)Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCodaList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 970
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->coda_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 922
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto$Syllable;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/LexiconProto$Syllable;
    .locals 1

    .prologue
    .line 936
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Syllable;->defaultInstance:Lcom/google/speech/tts/LexiconProto$Syllable;

    return-object v0
.end method

.method public getOnsetList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 958
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->onset_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 1004
    iget v3, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->memoizedSerializedSize:I

    .line 1005
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 1035
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 1007
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 1008
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->hasStress()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1009
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->getStress()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 1012
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->hasVowel()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1013
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->getVowel()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1017
    :cond_2
    const/4 v0, 0x0

    .line 1018
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->getOnsetList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1019
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1021
    goto :goto_1

    .line 1022
    .end local v1    # "element":Ljava/lang/String;
    :cond_3
    add-int/2addr v3, v0

    .line 1023
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->getOnsetList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 1026
    const/4 v0, 0x0

    .line 1027
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->getCodaList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1028
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1030
    goto :goto_2

    .line 1031
    .end local v1    # "element":Ljava/lang/String;
    :cond_4
    add-int/2addr v3, v0

    .line 1032
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->getCodaList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 1034
    iput v3, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->memoizedSerializedSize:I

    move v4, v3

    .line 1035
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto :goto_0
.end method

.method public getStress()I
    .locals 1

    .prologue
    .line 944
    iget v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->stress_:I

    return v0
.end method

.method public getVowel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 951
    iget-object v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->vowel_:Ljava/lang/String;

    return-object v0
.end method

.method public hasStress()Z
    .locals 1

    .prologue
    .line 943
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->hasStress:Z

    return v0
.end method

.method public hasVowel()Z
    .locals 1

    .prologue
    .line 950
    iget-boolean v0, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->hasVowel:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 980
    iget-boolean v1, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->hasStress:Z

    if-nez v1, :cond_1

    .line 982
    :cond_0
    :goto_0
    return v0

    .line 981
    :cond_1
    iget-boolean v1, p0, Lcom/google/speech/tts/LexiconProto$Syllable;->hasVowel:Z

    if-eqz v1, :cond_0

    .line 982
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 922
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->toBuilder()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/LexiconProto$Syllable$Builder;
    .locals 1

    .prologue
    .line 1110
    invoke-static {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->newBuilder(Lcom/google/speech/tts/LexiconProto$Syllable;)Lcom/google/speech/tts/LexiconProto$Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 987
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->getSerializedSize()I

    .line 988
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->hasStress()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 989
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->getStress()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 991
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->hasVowel()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 992
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->getVowel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 994
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->getOnsetList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 995
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 997
    .end local v0    # "element":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/LexiconProto$Syllable;->getCodaList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 998
    .restart local v0    # "element":Ljava/lang/String;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_1

    .line 1000
    .end local v0    # "element":Ljava/lang/String;
    :cond_3
    return-void
.end method
