.class public final Lcom/google/speech/tts/Abbreviation$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Abbreviation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/Abbreviation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/Abbreviation;",
        "Lcom/google/speech/tts/Abbreviation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/Abbreviation;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/Abbreviation$Builder;
    .locals 1

    .prologue
    .line 160
    invoke-static {}, Lcom/google/speech/tts/Abbreviation$Builder;->create()Lcom/google/speech/tts/Abbreviation$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/Abbreviation$Builder;
    .locals 3

    .prologue
    .line 169
    new-instance v0, Lcom/google/speech/tts/Abbreviation$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/Abbreviation$Builder;-><init>()V

    .line 170
    .local v0, "builder":Lcom/google/speech/tts/Abbreviation$Builder;
    new-instance v1, Lcom/google/speech/tts/Abbreviation;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/Abbreviation;-><init>(Lcom/google/speech/tts/Abbreviation$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    .line 171
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/speech/tts/Abbreviation$Builder;->build()Lcom/google/speech/tts/Abbreviation;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/Abbreviation;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/Abbreviation$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    invoke-static {v0}, Lcom/google/speech/tts/Abbreviation$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 202
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Abbreviation$Builder;->buildPartial()Lcom/google/speech/tts/Abbreviation;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/Abbreviation;
    .locals 3

    .prologue
    .line 215
    iget-object v1, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    if-nez v1, :cond_0

    .line 216
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    .line 220
    .local v0, "returnMe":Lcom/google/speech/tts/Abbreviation;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    .line 221
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/speech/tts/Abbreviation$Builder;->clone()Lcom/google/speech/tts/Abbreviation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/speech/tts/Abbreviation$Builder;->clone()Lcom/google/speech/tts/Abbreviation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/Abbreviation$Builder;
    .locals 2

    .prologue
    .line 188
    invoke-static {}, Lcom/google/speech/tts/Abbreviation$Builder;->create()Lcom/google/speech/tts/Abbreviation$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Abbreviation$Builder;->mergeFrom(Lcom/google/speech/tts/Abbreviation;)Lcom/google/speech/tts/Abbreviation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/speech/tts/Abbreviation$Builder;->clone()Lcom/google/speech/tts/Abbreviation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    invoke-virtual {v0}, Lcom/google/speech/tts/Abbreviation;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 160
    check-cast p1, Lcom/google/speech/tts/Abbreviation;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/Abbreviation$Builder;->mergeFrom(Lcom/google/speech/tts/Abbreviation;)Lcom/google/speech/tts/Abbreviation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/Abbreviation;)Lcom/google/speech/tts/Abbreviation$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/Abbreviation;

    .prologue
    .line 225
    invoke-static {}, Lcom/google/speech/tts/Abbreviation;->getDefaultInstance()Lcom/google/speech/tts/Abbreviation;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-object p0

    .line 226
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/Abbreviation;->hasText()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 227
    invoke-virtual {p1}, Lcom/google/speech/tts/Abbreviation;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Abbreviation$Builder;->setText(Ljava/lang/String;)Lcom/google/speech/tts/Abbreviation$Builder;

    .line 229
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/Abbreviation;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 230
    invoke-virtual {p1}, Lcom/google/speech/tts/Abbreviation;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Abbreviation$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Abbreviation$Builder;

    .line 232
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/tts/Abbreviation;->hasCodeSwitch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p1}, Lcom/google/speech/tts/Abbreviation;->getCodeSwitch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/Abbreviation$Builder;->setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Abbreviation$Builder;

    goto :goto_0
.end method

.method public setCodeSwitch(Ljava/lang/String;)Lcom/google/speech/tts/Abbreviation$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 320
    if-nez p1, :cond_0

    .line 321
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Abbreviation;->hasCodeSwitch:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Abbreviation;->access$702(Lcom/google/speech/tts/Abbreviation;Z)Z

    .line 324
    iget-object v0, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    # setter for: Lcom/google/speech/tts/Abbreviation;->codeSwitch_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Abbreviation;->access$802(Lcom/google/speech/tts/Abbreviation;Ljava/lang/String;)Ljava/lang/String;

    .line 325
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Abbreviation$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 299
    if-nez p1, :cond_0

    .line 300
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Abbreviation;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Abbreviation;->access$502(Lcom/google/speech/tts/Abbreviation;Z)Z

    .line 303
    iget-object v0, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    # setter for: Lcom/google/speech/tts/Abbreviation;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Abbreviation;->access$602(Lcom/google/speech/tts/Abbreviation;Ljava/lang/String;)Ljava/lang/String;

    .line 304
    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/google/speech/tts/Abbreviation$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 278
    if-nez p1, :cond_0

    .line 279
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/Abbreviation;->hasText:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/Abbreviation;->access$302(Lcom/google/speech/tts/Abbreviation;Z)Z

    .line 282
    iget-object v0, p0, Lcom/google/speech/tts/Abbreviation$Builder;->result:Lcom/google/speech/tts/Abbreviation;

    # setter for: Lcom/google/speech/tts/Abbreviation;->text_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/tts/Abbreviation;->access$402(Lcom/google/speech/tts/Abbreviation;Ljava/lang/String;)Ljava/lang/String;

    .line 283
    return-object p0
.end method
