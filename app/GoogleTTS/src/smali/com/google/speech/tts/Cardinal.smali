.class public final Lcom/google/speech/tts/Cardinal;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Cardinal.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/Cardinal$1;,
        Lcom/google/speech/tts/Cardinal$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/Cardinal;


# instance fields
.field private codeSwitch_:Ljava/lang/String;

.field private fieldOrder_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasCodeSwitch:Z

.field private hasInteger:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasPreserveOrder:Z

.field private integer_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private preserveOrder_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 454
    new-instance v0, Lcom/google/speech/tts/Cardinal;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/Cardinal;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/Cardinal;->defaultInstance:Lcom/google/speech/tts/Cardinal;

    .line 455
    invoke-static {}, Lcom/google/speech/tts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 456
    sget-object v0, Lcom/google/speech/tts/Cardinal;->defaultInstance:Lcom/google/speech/tts/Cardinal;

    invoke-direct {v0}, Lcom/google/speech/tts/Cardinal;->initFields()V

    .line 457
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Cardinal;->integer_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Cardinal;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/speech/tts/Cardinal;->preserveOrder_:Z

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Cardinal;->codeSwitch_:Ljava/lang/String;

    .line 52
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Cardinal;->fieldOrder_:Ljava/util/List;

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Cardinal;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/Cardinal;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/Cardinal$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/Cardinal$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/Cardinal;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Cardinal;->integer_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Cardinal;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/speech/tts/Cardinal;->preserveOrder_:Z

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/tts/Cardinal;->codeSwitch_:Ljava/lang/String;

    .line 52
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/Cardinal;->fieldOrder_:Ljava/util/List;

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/tts/Cardinal;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/Cardinal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Cardinal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Cardinal;->hasCodeSwitch:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/Cardinal;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Cardinal;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Cardinal;->codeSwitch_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/Cardinal;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/Cardinal;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/Cardinal;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/Cardinal;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Cardinal;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Cardinal;->fieldOrder_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/Cardinal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Cardinal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Cardinal;->hasInteger:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/Cardinal;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Cardinal;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Cardinal;->integer_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/Cardinal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Cardinal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Cardinal;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/Cardinal;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Cardinal;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/Cardinal;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/Cardinal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Cardinal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Cardinal;->hasPreserveOrder:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/Cardinal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/Cardinal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/Cardinal;->preserveOrder_:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/Cardinal;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/Cardinal;->defaultInstance:Lcom/google/speech/tts/Cardinal;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/Cardinal$Builder;
    .locals 1

    .prologue
    .line 191
    # invokes: Lcom/google/speech/tts/Cardinal$Builder;->create()Lcom/google/speech/tts/Cardinal$Builder;
    invoke-static {}, Lcom/google/speech/tts/Cardinal$Builder;->access$100()Lcom/google/speech/tts/Cardinal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/Cardinal;

    .prologue
    .line 194
    invoke-static {}, Lcom/google/speech/tts/Cardinal;->newBuilder()Lcom/google/speech/tts/Cardinal$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Cardinal$Builder;->mergeFrom(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCodeSwitch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/tts/Cardinal;->codeSwitch_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getDefaultInstanceForType()Lcom/google/speech/tts/Cardinal;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/Cardinal;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/Cardinal;->defaultInstance:Lcom/google/speech/tts/Cardinal;

    return-object v0
.end method

.method public getFieldOrderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/tts/Cardinal;->fieldOrder_:Ljava/util/List;

    return-object v0
.end method

.method public getInteger()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/tts/Cardinal;->integer_:Ljava/lang/String;

    return-object v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/Cardinal;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getPreserveOrder()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/speech/tts/Cardinal;->preserveOrder_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 91
    iget v3, p0, Lcom/google/speech/tts/Cardinal;->memoizedSerializedSize:I

    .line 92
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 121
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 94
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 95
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->hasInteger()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 96
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getInteger()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 99
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->hasMorphosyntacticFeatures()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 100
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 103
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->hasPreserveOrder()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 104
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getPreserveOrder()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 107
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->hasCodeSwitch()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 108
    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getCodeSwitch()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 112
    :cond_4
    const/4 v0, 0x0

    .line 113
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 114
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 116
    goto :goto_1

    .line 117
    .end local v1    # "element":Ljava/lang/String;
    :cond_5
    add-int/2addr v3, v0

    .line 118
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getFieldOrderList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 120
    iput v3, p0, Lcom/google/speech/tts/Cardinal;->memoizedSerializedSize:I

    move v4, v3

    .line 121
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto :goto_0
.end method

.method public hasCodeSwitch()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/Cardinal;->hasCodeSwitch:Z

    return v0
.end method

.method public hasInteger()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/Cardinal;->hasInteger:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/Cardinal;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasPreserveOrder()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/Cardinal;->hasPreserveOrder:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/speech/tts/Cardinal;->hasInteger:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 66
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->toBuilder()Lcom/google/speech/tts/Cardinal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/Cardinal$Builder;
    .locals 1

    .prologue
    .line 196
    invoke-static {p0}, Lcom/google/speech/tts/Cardinal;->newBuilder(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getSerializedSize()I

    .line 72
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->hasInteger()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getInteger()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 76
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->hasPreserveOrder()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 79
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getPreserveOrder()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 81
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->hasCodeSwitch()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 82
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getCodeSwitch()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 84
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/Cardinal;->getFieldOrderList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 87
    .end local v0    # "element":Ljava/lang/String;
    :cond_4
    return-void
.end method
