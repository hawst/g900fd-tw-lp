.class public final Lcom/google/speech/tts/AdvancedVoiceMod;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AdvancedVoiceMod.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/tts/AdvancedVoiceMod$1;,
        Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/tts/AdvancedVoiceMod;


# instance fields
.field private factor_:Lcom/google/speech/tts/VoiceModFactorType;

.field private hasFactor:Z

.field private hasInterval:Z

.field private hasItem:Z

.field private hasItemIndex:Z

.field private hasMeanParams:Z

.field private hasPiecewiseParams:Z

.field private hasSpanId:Z

.field private hasTransform:Z

.field private hasWordIndex:Z

.field private interval_:Lcom/google/speech/tts/VoiceModIntervalType;

.field private itemIndex_:I

.field private item_:Lcom/google/speech/tts/VoiceModLinguisticItemType;

.field private meanParams_:Lcom/google/speech/tts/MeanModelParams;

.field private memoizedSerializedSize:I

.field private operatorParams_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/TimeValueFunctionSamples;",
            ">;"
        }
    .end annotation
.end field

.field private piecewiseParams_:Lcom/google/speech/tts/PiecewiseModelParams;

.field private spanId_:I

.field private transform_:Lcom/google/speech/tts/VoiceModTransformType;

.field private wordIndex_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 739
    new-instance v0, Lcom/google/speech/tts/AdvancedVoiceMod;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/tts/AdvancedVoiceMod;-><init>(Z)V

    sput-object v0, Lcom/google/speech/tts/AdvancedVoiceMod;->defaultInstance:Lcom/google/speech/tts/AdvancedVoiceMod;

    .line 740
    invoke-static {}, Lcom/google/speech/tts/AdvancedVoiceModProto;->internalForceInit()V

    .line 741
    sget-object v0, Lcom/google/speech/tts/AdvancedVoiceMod;->defaultInstance:Lcom/google/speech/tts/AdvancedVoiceMod;

    invoke-direct {v0}, Lcom/google/speech/tts/AdvancedVoiceMod;->initFields()V

    .line 742
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->wordIndex_:I

    .line 39
    iput v1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->itemIndex_:I

    .line 53
    iput v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->spanId_:I

    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;

    .line 154
    iput v1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/tts/AdvancedVoiceMod$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->wordIndex_:I

    .line 39
    iput v1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->itemIndex_:I

    .line 53
    iput v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->spanId_:I

    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;

    .line 154
    iput v1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasInterval:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/VoiceModIntervalType;)Lcom/google/speech/tts/VoiceModIntervalType;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Lcom/google/speech/tts/VoiceModIntervalType;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->interval_:Lcom/google/speech/tts/VoiceModIntervalType;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasSpanId:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/tts/AdvancedVoiceMod;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->spanId_:I

    return p1
.end method

.method static synthetic access$1402(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasTransform:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/VoiceModTransformType;)Lcom/google/speech/tts/VoiceModTransformType;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Lcom/google/speech/tts/VoiceModTransformType;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->transform_:Lcom/google/speech/tts/VoiceModTransformType;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasFactor:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/VoiceModFactorType;)Lcom/google/speech/tts/VoiceModFactorType;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Lcom/google/speech/tts/VoiceModFactorType;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->factor_:Lcom/google/speech/tts/VoiceModFactorType;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasMeanParams:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/google/speech/tts/AdvancedVoiceMod;)Lcom/google/speech/tts/MeanModelParams;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->meanParams_:Lcom/google/speech/tts/MeanModelParams;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/MeanModelParams;)Lcom/google/speech/tts/MeanModelParams;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Lcom/google/speech/tts/MeanModelParams;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->meanParams_:Lcom/google/speech/tts/MeanModelParams;

    return-object p1
.end method

.method static synthetic access$2002(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasPiecewiseParams:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/google/speech/tts/AdvancedVoiceMod;)Lcom/google/speech/tts/PiecewiseModelParams;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->piecewiseParams_:Lcom/google/speech/tts/PiecewiseModelParams;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/PiecewiseModelParams;)Lcom/google/speech/tts/PiecewiseModelParams;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Lcom/google/speech/tts/PiecewiseModelParams;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->piecewiseParams_:Lcom/google/speech/tts/PiecewiseModelParams;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/tts/AdvancedVoiceMod;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/tts/AdvancedVoiceMod;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasWordIndex:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/tts/AdvancedVoiceMod;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->wordIndex_:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasItem:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/tts/AdvancedVoiceMod;Lcom/google/speech/tts/VoiceModLinguisticItemType;)Lcom/google/speech/tts/VoiceModLinguisticItemType;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Lcom/google/speech/tts/VoiceModLinguisticItemType;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->item_:Lcom/google/speech/tts/VoiceModLinguisticItemType;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/speech/tts/AdvancedVoiceMod;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasItemIndex:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/tts/AdvancedVoiceMod;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/tts/AdvancedVoiceMod;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->itemIndex_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/tts/AdvancedVoiceMod;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/tts/AdvancedVoiceMod;->defaultInstance:Lcom/google/speech/tts/AdvancedVoiceMod;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lcom/google/speech/tts/VoiceModLinguisticItemType;->WORD:Lcom/google/speech/tts/VoiceModLinguisticItemType;

    iput-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->item_:Lcom/google/speech/tts/VoiceModLinguisticItemType;

    .line 99
    sget-object v0, Lcom/google/speech/tts/VoiceModIntervalType;->LINGUISTIC_ITEM:Lcom/google/speech/tts/VoiceModIntervalType;

    iput-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->interval_:Lcom/google/speech/tts/VoiceModIntervalType;

    .line 100
    sget-object v0, Lcom/google/speech/tts/VoiceModTransformType;->OPERATOR:Lcom/google/speech/tts/VoiceModTransformType;

    iput-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->transform_:Lcom/google/speech/tts/VoiceModTransformType;

    .line 101
    sget-object v0, Lcom/google/speech/tts/VoiceModFactorType;->F0:Lcom/google/speech/tts/VoiceModFactorType;

    iput-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->factor_:Lcom/google/speech/tts/VoiceModFactorType;

    .line 102
    invoke-static {}, Lcom/google/speech/tts/MeanModelParams;->getDefaultInstance()Lcom/google/speech/tts/MeanModelParams;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->meanParams_:Lcom/google/speech/tts/MeanModelParams;

    .line 103
    invoke-static {}, Lcom/google/speech/tts/PiecewiseModelParams;->getDefaultInstance()Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->piecewiseParams_:Lcom/google/speech/tts/PiecewiseModelParams;

    .line 104
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 1

    .prologue
    .line 271
    # invokes: Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->create()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    invoke-static {}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->access$100()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/tts/AdvancedVoiceMod;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/tts/AdvancedVoiceMod;

    .prologue
    .line 274
    invoke-static {}, Lcom/google/speech/tts/AdvancedVoiceMod;->newBuilder()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/AdvancedVoiceMod$Builder;->mergeFrom(Lcom/google/speech/tts/AdvancedVoiceMod;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getDefaultInstanceForType()Lcom/google/speech/tts/AdvancedVoiceMod;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/tts/AdvancedVoiceMod;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/tts/AdvancedVoiceMod;->defaultInstance:Lcom/google/speech/tts/AdvancedVoiceMod;

    return-object v0
.end method

.method public getFactor()Lcom/google/speech/tts/VoiceModFactorType;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->factor_:Lcom/google/speech/tts/VoiceModFactorType;

    return-object v0
.end method

.method public getInterval()Lcom/google/speech/tts/VoiceModIntervalType;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->interval_:Lcom/google/speech/tts/VoiceModIntervalType;

    return-object v0
.end method

.method public getItem()Lcom/google/speech/tts/VoiceModLinguisticItemType;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->item_:Lcom/google/speech/tts/VoiceModLinguisticItemType;

    return-object v0
.end method

.method public getItemIndex()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->itemIndex_:I

    return v0
.end method

.method public getMeanParams()Lcom/google/speech/tts/MeanModelParams;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->meanParams_:Lcom/google/speech/tts/MeanModelParams;

    return-object v0
.end method

.method public getOperatorParamsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/TimeValueFunctionSamples;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->operatorParams_:Ljava/util/List;

    return-object v0
.end method

.method public getPiecewiseParams()Lcom/google/speech/tts/PiecewiseModelParams;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->piecewiseParams_:Lcom/google/speech/tts/PiecewiseModelParams;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 156
    iget v2, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->memoizedSerializedSize:I

    .line 157
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 201
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 159
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 160
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasWordIndex()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 161
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getWordIndex()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 164
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasItem()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 165
    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getItem()Lcom/google/speech/tts/VoiceModLinguisticItemType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/speech/tts/VoiceModLinguisticItemType;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 168
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasItemIndex()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 169
    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getItemIndex()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 172
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasInterval()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 173
    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getInterval()Lcom/google/speech/tts/VoiceModIntervalType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/speech/tts/VoiceModIntervalType;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 176
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasSpanId()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 177
    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getSpanId()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 180
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasTransform()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 181
    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getTransform()Lcom/google/speech/tts/VoiceModTransformType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/speech/tts/VoiceModTransformType;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 184
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasFactor()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 185
    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getFactor()Lcom/google/speech/tts/VoiceModFactorType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/speech/tts/VoiceModFactorType;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 188
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getOperatorParamsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/TimeValueFunctionSamples;

    .line 189
    .local v0, "element":Lcom/google/speech/tts/TimeValueFunctionSamples;
    const/16 v4, 0x8

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 191
    goto :goto_1

    .line 192
    .end local v0    # "element":Lcom/google/speech/tts/TimeValueFunctionSamples;
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasMeanParams()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 193
    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getMeanParams()Lcom/google/speech/tts/MeanModelParams;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 196
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasPiecewiseParams()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 197
    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getPiecewiseParams()Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 200
    :cond_a
    iput v2, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->memoizedSerializedSize:I

    move v3, v2

    .line 201
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto/16 :goto_0
.end method

.method public getSpanId()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->spanId_:I

    return v0
.end method

.method public getTransform()Lcom/google/speech/tts/VoiceModTransformType;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->transform_:Lcom/google/speech/tts/VoiceModTransformType;

    return-object v0
.end method

.method public getWordIndex()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->wordIndex_:I

    return v0
.end method

.method public hasFactor()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasFactor:Z

    return v0
.end method

.method public hasInterval()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasInterval:Z

    return v0
.end method

.method public hasItem()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasItem:Z

    return v0
.end method

.method public hasItemIndex()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasItemIndex:Z

    return v0
.end method

.method public hasMeanParams()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasMeanParams:Z

    return v0
.end method

.method public hasPiecewiseParams()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasPiecewiseParams:Z

    return v0
.end method

.method public hasSpanId()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasSpanId:Z

    return v0
.end method

.method public hasTransform()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasTransform:Z

    return v0
.end method

.method public hasWordIndex()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasWordIndex:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 106
    iget-boolean v3, p0, Lcom/google/speech/tts/AdvancedVoiceMod;->hasWordIndex:Z

    if-nez v3, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v2

    .line 107
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getOperatorParamsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/TimeValueFunctionSamples;

    .line 108
    .local v0, "element":Lcom/google/speech/tts/TimeValueFunctionSamples;
    invoke-virtual {v0}, Lcom/google/speech/tts/TimeValueFunctionSamples;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 110
    .end local v0    # "element":Lcom/google/speech/tts/TimeValueFunctionSamples;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasMeanParams()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 111
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getMeanParams()Lcom/google/speech/tts/MeanModelParams;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/MeanModelParams;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 113
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasPiecewiseParams()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 114
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getPiecewiseParams()Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/PiecewiseModelParams;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 116
    :cond_5
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->toBuilder()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/tts/AdvancedVoiceMod$Builder;
    .locals 1

    .prologue
    .line 276
    invoke-static {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->newBuilder(Lcom/google/speech/tts/AdvancedVoiceMod;)Lcom/google/speech/tts/AdvancedVoiceMod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getSerializedSize()I

    .line 122
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasWordIndex()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 123
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getWordIndex()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasItem()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 126
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getItem()Lcom/google/speech/tts/VoiceModLinguisticItemType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/VoiceModLinguisticItemType;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 128
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasItemIndex()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 129
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getItemIndex()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 131
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasInterval()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 132
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getInterval()Lcom/google/speech/tts/VoiceModIntervalType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/VoiceModIntervalType;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 134
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasSpanId()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 135
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getSpanId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 137
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasTransform()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 138
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getTransform()Lcom/google/speech/tts/VoiceModTransformType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/VoiceModTransformType;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 140
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasFactor()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 141
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getFactor()Lcom/google/speech/tts/VoiceModFactorType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/VoiceModFactorType;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 143
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getOperatorParamsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/TimeValueFunctionSamples;

    .line 144
    .local v0, "element":Lcom/google/speech/tts/TimeValueFunctionSamples;
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 146
    .end local v0    # "element":Lcom/google/speech/tts/TimeValueFunctionSamples;
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasMeanParams()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 147
    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getMeanParams()Lcom/google/speech/tts/MeanModelParams;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 149
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->hasPiecewiseParams()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 150
    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/tts/AdvancedVoiceMod;->getPiecewiseParams()Lcom/google/speech/tts/PiecewiseModelParams;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 152
    :cond_9
    return-void
.end method
