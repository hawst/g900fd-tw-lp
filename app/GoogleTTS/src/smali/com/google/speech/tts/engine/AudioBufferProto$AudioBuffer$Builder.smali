.class public final Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AudioBufferProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;",
        "Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
    .locals 1

    .prologue
    .line 151
    invoke-static {}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->create()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
    .locals 3

    .prologue
    .line 160
    new-instance v0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    invoke-direct {v0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;-><init>()V

    .line 161
    .local v0, "builder":Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
    new-instance v1, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;-><init>(Lcom/google/speech/tts/engine/AudioBufferProto$1;)V

    iput-object v1, v0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    .line 162
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->build()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    invoke-static {v0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 193
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->buildPartial()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    .locals 3

    .prologue
    .line 206
    iget-object v1, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    if-nez v1, :cond_0

    .line 207
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    .line 211
    .local v0, "returnMe":Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    .line 212
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->clone()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->clone()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
    .locals 2

    .prologue
    .line 179
    invoke-static {}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->create()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->mergeFrom(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->clone()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    invoke-virtual {v0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 151
    check-cast p1, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->mergeFrom(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    .prologue
    .line 216
    invoke-static {}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->getDefaultInstance()Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-object p0

    .line 217
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->hasSamples()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->getSamples()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->setSamples(Lcom/google/protobuf/ByteString;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    .line 220
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->hasSampleRate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {p1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->getSampleRate()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->setSampleRate(I)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;

    goto :goto_0
.end method

.method public setSampleRate(I)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->hasSampleRate:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->access$502(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;Z)Z

    .line 284
    iget-object v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    # setter for: Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->sampleRate_:I
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->access$602(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;I)I

    .line 285
    return-object p0
.end method

.method public setSamples(Lcom/google/protobuf/ByteString;)Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/protobuf/ByteString;

    .prologue
    .line 262
    if-nez p1, :cond_0

    .line 263
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->hasSamples:Z
    invoke-static {v0, v1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->access$302(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;Z)Z

    .line 266
    iget-object v0, p0, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer$Builder;->result:Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;

    # setter for: Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->samples_:Lcom/google/protobuf/ByteString;
    invoke-static {v0, p1}, Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;->access$402(Lcom/google/speech/tts/engine/AudioBufferProto$AudioBuffer;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    .line 267
    return-object p0
.end method
