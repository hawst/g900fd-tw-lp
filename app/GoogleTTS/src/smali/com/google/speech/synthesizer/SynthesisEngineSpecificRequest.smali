.class public final Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
.source "SynthesisEngineSpecificRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$1;,
        Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage",
        "<",
        "Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;",
        ">;"
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;


# instance fields
.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 217
    new-instance v0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;-><init>(Z)V

    sput-object v0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->defaultInstance:Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    .line 218
    invoke-static {}, Lcom/google/speech/synthesizer/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 219
    sget-object v0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->defaultInstance:Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    invoke-direct {v0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->initFields()V

    .line 220
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->memoizedSerializedSize:I

    .line 10
    invoke-direct {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->initFields()V

    .line 11
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->memoizedSerializedSize:I

    .line 12
    return-void
.end method

.method public static getDefaultInstance()Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->defaultInstance:Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;
    .locals 1

    .prologue
    .line 116
    # invokes: Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;->create()Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;
    invoke-static {}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;->access$100()Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;)Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    .prologue
    .line 119
    invoke-static {}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->newBuilder()Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;->mergeFrom(Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;)Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->getDefaultInstanceForType()Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->defaultInstance:Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 40
    iget v0, p0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->memoizedSerializedSize:I

    .line 41
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 46
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 43
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 44
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->extensionsSerializedSize()I

    move-result v2

    add-int/2addr v0, v2

    .line 45
    iput v0, p0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->memoizedSerializedSize:I

    move v1, v0

    .line 46
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->extensionsAreInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 27
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->toBuilder()Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;
    .locals 1

    .prologue
    .line 121
    invoke-static {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->newBuilder(Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;)Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->getSerializedSize()I

    .line 34
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->newExtensionWriter()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;

    move-result-object v0

    .line 35
    .local v0, "extensionWriter":Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;->writeUntil(ILcom/google/protobuf/CodedOutputStream;)V

    .line 36
    return-void
.end method
