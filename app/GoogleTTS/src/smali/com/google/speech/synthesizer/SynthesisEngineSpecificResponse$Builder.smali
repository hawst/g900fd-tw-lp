.class public final Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
.source "SynthesisEngineSpecificResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
        "<",
        "Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;",
        "Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;
    .locals 1

    .prologue
    .line 123
    invoke-static {}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->create()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;
    .locals 3

    .prologue
    .line 132
    new-instance v0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;

    invoke-direct {v0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;-><init>()V

    .line 133
    .local v0, "builder":Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;
    new-instance v1, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;-><init>(Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$1;)V

    iput-object v1, v0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->result:Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    .line 134
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->build()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->result:Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->result:Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    invoke-static {v0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 165
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->buildPartial()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;
    .locals 3

    .prologue
    .line 178
    iget-object v1, p0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->result:Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    if-nez v1, :cond_0

    .line 179
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->result:Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    .line 183
    .local v0, "returnMe":Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->result:Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    .line 184
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->clone()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->clone()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->clone()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;
    .locals 2

    .prologue
    .line 151
    invoke-static {}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->create()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->result:Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    invoke-virtual {v0, v1}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->mergeFrom(Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;)Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->clone()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic internalGetResult()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->internalGetResult()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    move-result-object v0

    return-object v0
.end method

.method protected internalGetResult()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->result:Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->result:Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    invoke-virtual {v0}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 123
    check-cast p1, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->mergeFrom(Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;)Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;)Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    .prologue
    .line 188
    invoke-static {}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;->getDefaultInstance()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 190
    :goto_0
    return-object p0

    .line 189
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse$Builder;->mergeExtensionFields(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;)V

    goto :goto_0
.end method
