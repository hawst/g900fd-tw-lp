.class public final Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PipelineDef.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/recognizer/pipeline/PipelineDef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/recognizer/pipeline/PipelineDef;",
        "Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/recognizer/pipeline/PipelineDef;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
    .locals 1

    .prologue
    .line 249
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->create()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
    .locals 3

    .prologue
    .line 258
    new-instance v0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    invoke-direct {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;-><init>()V

    .line 259
    .local v0, "builder":Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
    new-instance v1, Lcom/google/speech/recognizer/pipeline/PipelineDef;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/recognizer/pipeline/PipelineDef;-><init>(Lcom/google/speech/recognizer/pipeline/PipelineDef$1;)V

    iput-object v1, v0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    .line 260
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->build()Lcom/google/speech/recognizer/pipeline/PipelineDef;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 291
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->buildPartial()Lcom/google/speech/recognizer/pipeline/PipelineDef;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .locals 3

    .prologue
    .line 304
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    if-nez v1, :cond_0

    .line 305
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 308
    :cond_0
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$300(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 309
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    iget-object v2, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$300(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$302(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;

    .line 312
    :cond_1
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$400(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 313
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    iget-object v2, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$400(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$402(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;

    .line 316
    :cond_2
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$500(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 317
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    iget-object v2, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$500(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$502(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;

    .line 320
    :cond_3
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$600(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 321
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    iget-object v2, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$600(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$602(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;

    .line 324
    :cond_4
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$700(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_5

    .line 325
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    iget-object v2, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$700(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$702(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;

    .line 328
    :cond_5
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    .line 329
    .local v0, "returnMe":Lcom/google/speech/recognizer/pipeline/PipelineDef;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    .line 330
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->clone()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->clone()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
    .locals 2

    .prologue
    .line 277
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->create()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    invoke-virtual {v0, v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->mergeFrom(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->clone()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    invoke-virtual {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 249
    check-cast p1, Lcom/google/speech/recognizer/pipeline/PipelineDef;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->mergeFrom(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;

    .prologue
    .line 334
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getDefaultInstance()Lcom/google/speech/recognizer/pipeline/PipelineDef;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 371
    :cond_0
    :goto_0
    return-object p0

    .line 335
    :cond_1
    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$300(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 336
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$300(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$302(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;

    .line 339
    :cond_2
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$300(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$300(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 341
    :cond_3
    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$400(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 342
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$400(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 343
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$402(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;

    .line 345
    :cond_4
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$400(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$400(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 347
    :cond_5
    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$500(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 348
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$500(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 349
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$502(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;

    .line 351
    :cond_6
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$500(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$500(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 353
    :cond_7
    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$600(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 354
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$600(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 355
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$602(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;

    .line 357
    :cond_8
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$600(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$600(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 359
    :cond_9
    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$700(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 360
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$700(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 361
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$702(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;

    .line 363
    :cond_a
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$700(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$700(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 365
    :cond_b
    invoke-virtual {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->hasTracerRef()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 366
    invoke-virtual {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getTracerRef()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->setTracerRef(Ljava/lang/String;)Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    .line 368
    :cond_c
    invoke-virtual {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    invoke-virtual {p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->setName(Ljava/lang/String;)Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    goto/16 :goto_0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 716
    if-nez p1, :cond_0

    .line 717
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 719
    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$1002(Lcom/google/speech/recognizer/pipeline/PipelineDef;Z)Z

    .line 720
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$1102(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/lang/String;)Ljava/lang/String;

    .line 721
    return-object p0
.end method

.method public setTracerRef(Ljava/lang/String;)Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 695
    if-nez p1, :cond_0

    .line 696
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 698
    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->hasTracerRef:Z
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$802(Lcom/google/speech/recognizer/pipeline/PipelineDef;Z)Z

    .line 699
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    # setter for: Lcom/google/speech/recognizer/pipeline/PipelineDef;->tracerRef_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->access$902(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/lang/String;)Ljava/lang/String;

    .line 700
    return-object p0
.end method
