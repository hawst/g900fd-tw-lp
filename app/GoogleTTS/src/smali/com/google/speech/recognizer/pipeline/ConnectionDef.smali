.class public final Lcom/google/speech/recognizer/pipeline/ConnectionDef;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "ConnectionDef.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/recognizer/pipeline/ConnectionDef$1;,
        Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/recognizer/pipeline/ConnectionDef;


# instance fields
.field private from_:Ljava/lang/String;

.field private hasFrom:Z

.field private hasTo:Z

.field private memoizedSerializedSize:I

.field private to_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 294
    new-instance v0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;-><init>(Z)V

    sput-object v0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->defaultInstance:Lcom/google/speech/recognizer/pipeline/ConnectionDef;

    .line 295
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 296
    sget-object v0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->defaultInstance:Lcom/google/speech/recognizer/pipeline/ConnectionDef;

    invoke-direct {v0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->initFields()V

    .line 297
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->from_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->to_:Ljava/lang/String;

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/recognizer/pipeline/ConnectionDef$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/recognizer/pipeline/ConnectionDef$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->from_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->to_:Ljava/lang/String;

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/recognizer/pipeline/ConnectionDef;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/ConnectionDef;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->hasFrom:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/recognizer/pipeline/ConnectionDef;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/ConnectionDef;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->from_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/recognizer/pipeline/ConnectionDef;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/ConnectionDef;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->hasTo:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/recognizer/pipeline/ConnectionDef;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/ConnectionDef;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->to_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/recognizer/pipeline/ConnectionDef;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->defaultInstance:Lcom/google/speech/recognizer/pipeline/ConnectionDef;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;
    .locals 1

    .prologue
    .line 138
    # invokes: Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;->create()Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;->access$100()Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/recognizer/pipeline/ConnectionDef;)Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/recognizer/pipeline/ConnectionDef;

    .prologue
    .line 141
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->newBuilder()Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;->mergeFrom(Lcom/google/speech/recognizer/pipeline/ConnectionDef;)Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->getDefaultInstanceForType()Lcom/google/speech/recognizer/pipeline/ConnectionDef;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/recognizer/pipeline/ConnectionDef;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->defaultInstance:Lcom/google/speech/recognizer/pipeline/ConnectionDef;

    return-object v0
.end method

.method public getFrom()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->from_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 55
    iget v0, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->memoizedSerializedSize:I

    .line 56
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 68
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 58
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 59
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->hasFrom()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->getFrom()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 63
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->hasTo()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 64
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->getTo()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 67
    :cond_2
    iput v0, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->memoizedSerializedSize:I

    move v1, v0

    .line 68
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getTo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->to_:Ljava/lang/String;

    return-object v0
.end method

.method public hasFrom()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->hasFrom:Z

    return v0
.end method

.method public hasTo()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->hasTo:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->toBuilder()Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;
    .locals 1

    .prologue
    .line 143
    invoke-static {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->newBuilder(Lcom/google/speech/recognizer/pipeline/ConnectionDef;)Lcom/google/speech/recognizer/pipeline/ConnectionDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->getSerializedSize()I

    .line 45
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->hasFrom()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->getFrom()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->hasTo()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ConnectionDef;->getTo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 51
    :cond_1
    return-void
.end method
