.class public final Lcom/google/speech/recognizer/pipeline/StreamDef;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
.source "StreamDef.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/recognizer/pipeline/StreamDef$1;,
        Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage",
        "<",
        "Lcom/google/speech/recognizer/pipeline/StreamDef;",
        ">;"
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/recognizer/pipeline/StreamDef;


# instance fields
.field private classname_:Ljava/lang/String;

.field private hasClassname:Z

.field private hasName:Z

.field private hasParams:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private paramsRef_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private params_:Lgreco/Params;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 446
    new-instance v0, Lcom/google/speech/recognizer/pipeline/StreamDef;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/recognizer/pipeline/StreamDef;-><init>(Z)V

    sput-object v0, Lcom/google/speech/recognizer/pipeline/StreamDef;->defaultInstance:Lcom/google/speech/recognizer/pipeline/StreamDef;

    .line 447
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 448
    sget-object v0, Lcom/google/speech/recognizer/pipeline/StreamDef;->defaultInstance:Lcom/google/speech/recognizer/pipeline/StreamDef;

    invoke-direct {v0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->initFields()V

    .line 449
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->name_:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->classname_:Ljava/lang/String;

    .line 46
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;

    .line 87
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->memoizedSerializedSize:I

    .line 10
    invoke-direct {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->initFields()V

    .line 11
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/recognizer/pipeline/StreamDef$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/recognizer/pipeline/StreamDef$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->name_:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->classname_:Ljava/lang/String;

    .line 46
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;

    .line 87
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->memoizedSerializedSize:I

    .line 12
    return-void
.end method

.method static synthetic access$300(Lcom/google/speech/recognizer/pipeline/StreamDef;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/StreamDef;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/recognizer/pipeline/StreamDef;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/StreamDef;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/recognizer/pipeline/StreamDef;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/StreamDef;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasName:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/recognizer/pipeline/StreamDef;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/StreamDef;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/speech/recognizer/pipeline/StreamDef;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/StreamDef;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasClassname:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/recognizer/pipeline/StreamDef;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/StreamDef;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->classname_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/speech/recognizer/pipeline/StreamDef;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/StreamDef;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasParams:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/speech/recognizer/pipeline/StreamDef;)Lgreco/Params;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/StreamDef;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->params_:Lgreco/Params;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/speech/recognizer/pipeline/StreamDef;Lgreco/Params;)Lgreco/Params;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/StreamDef;
    .param p1, "x1"    # Lgreco/Params;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->params_:Lgreco/Params;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/recognizer/pipeline/StreamDef;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/speech/recognizer/pipeline/StreamDef;->defaultInstance:Lcom/google/speech/recognizer/pipeline/StreamDef;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lgreco/Params;->getDefaultInstance()Lgreco/Params;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->params_:Lgreco/Params;

    .line 58
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    .locals 1

    .prologue
    .line 186
    # invokes: Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->create()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->access$100()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/recognizer/pipeline/StreamDef;)Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/recognizer/pipeline/StreamDef;

    .prologue
    .line 189
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/StreamDef;->newBuilder()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->mergeFrom(Lcom/google/speech/recognizer/pipeline/StreamDef;)Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassname()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->classname_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getDefaultInstanceForType()Lcom/google/speech/recognizer/pipeline/StreamDef;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/recognizer/pipeline/StreamDef;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/speech/recognizer/pipeline/StreamDef;->defaultInstance:Lcom/google/speech/recognizer/pipeline/StreamDef;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getParams()Lgreco/Params;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->params_:Lgreco/Params;

    return-object v0
.end method

.method public getParamsRefList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 89
    iget v3, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->memoizedSerializedSize:I

    .line 90
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 116
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 92
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 93
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasName()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 94
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 97
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasClassname()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 98
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getClassname()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 101
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasParams()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 102
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getParams()Lgreco/Params;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 106
    :cond_3
    const/4 v0, 0x0

    .line 107
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getParamsRefList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 108
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 110
    goto :goto_1

    .line 111
    .end local v1    # "element":Ljava/lang/String;
    :cond_4
    add-int/2addr v3, v0

    .line 112
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getParamsRefList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 114
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->extensionsSerializedSize()I

    move-result v5

    add-int/2addr v3, v5

    .line 115
    iput v3, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->memoizedSerializedSize:I

    move v4, v3

    .line 116
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto :goto_0
.end method

.method public hasClassname()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasClassname:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasName:Z

    return v0
.end method

.method public hasParams()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasParams:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasParams()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getParams()Lgreco/Params;

    move-result-object v1

    invoke-virtual {v1}, Lgreco/Params;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v0

    .line 63
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->extensionsAreInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->toBuilder()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    .locals 1

    .prologue
    .line 191
    invoke-static {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->newBuilder(Lcom/google/speech/recognizer/pipeline/StreamDef;)Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getSerializedSize()I

    .line 71
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->newExtensionWriter()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;

    move-result-object v1

    .line 72
    .local v1, "extensionWriter":Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasName()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasClassname()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 76
    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getClassname()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasParams()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 79
    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getParams()Lgreco/Params;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 81
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getParamsRefList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 82
    .local v0, "element":Ljava/lang/String;
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 84
    .end local v0    # "element":Ljava/lang/String;
    :cond_3
    const/high16 v3, 0x20000000

    invoke-virtual {v1, v3, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;->writeUntil(ILcom/google/protobuf/CodedOutputStream;)V

    .line 85
    return-void
.end method
