.class public final Lcom/google/speech/recognizer/pipeline/PipelineDef;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PipelineDef.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/recognizer/pipeline/PipelineDef$1;,
        Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/recognizer/pipeline/PipelineDef;


# instance fields
.field private connect_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/recognizer/pipeline/ConnectionDef;",
            ">;"
        }
    .end annotation
.end field

.field private hasName:Z

.field private hasTracerRef:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private resource_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgreco/ResourceDef;",
            ">;"
        }
    .end annotation
.end field

.field private sessionResource_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgreco/ResourceDef;",
            ">;"
        }
    .end annotation
.end field

.field private stream_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/recognizer/pipeline/StreamDef;",
            ">;"
        }
    .end annotation
.end field

.field private thread_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/recognizer/pipeline/ThreadDef;",
            ">;"
        }
    .end annotation
.end field

.field private tracerRef_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 733
    new-instance v0, Lcom/google/speech/recognizer/pipeline/PipelineDef;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/recognizer/pipeline/PipelineDef;-><init>(Z)V

    sput-object v0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->defaultInstance:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    .line 734
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 735
    sget-object v0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->defaultInstance:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    invoke-direct {v0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->initFields()V

    .line 736
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;

    .line 72
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;

    .line 85
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->tracerRef_:Ljava/lang/String;

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->name_:Ljava/lang/String;

    .line 137
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/recognizer/pipeline/PipelineDef$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;

    .line 72
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;

    .line 85
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->tracerRef_:Ljava/lang/String;

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->name_:Ljava/lang/String;

    .line 137
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/recognizer/pipeline/PipelineDef;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->hasName:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/speech/recognizer/pipeline/PipelineDef;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->hasTracerRef:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/recognizer/pipeline/PipelineDef;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->tracerRef_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->defaultInstance:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
    .locals 1

    .prologue
    .line 242
    # invokes: Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->create()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->access$100()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/recognizer/pipeline/PipelineDef;

    .prologue
    .line 245
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->newBuilder()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;->mergeFrom(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getConnectList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/recognizer/pipeline/ConnectionDef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->connect_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getDefaultInstanceForType()Lcom/google/speech/recognizer/pipeline/PipelineDef;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/recognizer/pipeline/PipelineDef;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->defaultInstance:Lcom/google/speech/recognizer/pipeline/PipelineDef;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getResourceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgreco/ResourceDef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->resource_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 139
    iget v2, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->memoizedSerializedSize:I

    .line 140
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 172
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 142
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 143
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getStreamList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/pipeline/StreamDef;

    .line 144
    .local v0, "element":Lcom/google/speech/recognizer/pipeline/StreamDef;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 146
    goto :goto_1

    .line 147
    .end local v0    # "element":Lcom/google/speech/recognizer/pipeline/StreamDef;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getConnectList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;

    .line 148
    .local v0, "element":Lcom/google/speech/recognizer/pipeline/ConnectionDef;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 150
    goto :goto_2

    .line 151
    .end local v0    # "element":Lcom/google/speech/recognizer/pipeline/ConnectionDef;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getResourceList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgreco/ResourceDef;

    .line 152
    .local v0, "element":Lgreco/ResourceDef;
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 154
    goto :goto_3

    .line 155
    .end local v0    # "element":Lgreco/ResourceDef;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getThreadList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/pipeline/ThreadDef;

    .line 156
    .local v0, "element":Lcom/google/speech/recognizer/pipeline/ThreadDef;
    const/4 v4, 0x4

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 158
    goto :goto_4

    .line 159
    .end local v0    # "element":Lcom/google/speech/recognizer/pipeline/ThreadDef;
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getSessionResourceList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgreco/ResourceDef;

    .line 160
    .local v0, "element":Lgreco/ResourceDef;
    const/4 v4, 0x5

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 162
    goto :goto_5

    .line 163
    .end local v0    # "element":Lgreco/ResourceDef;
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->hasTracerRef()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 164
    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getTracerRef()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 167
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->hasName()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 168
    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 171
    :cond_7
    iput v2, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->memoizedSerializedSize:I

    move v3, v2

    .line 172
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto/16 :goto_0
.end method

.method public getSessionResourceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgreco/ResourceDef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->sessionResource_:Ljava/util/List;

    return-object v0
.end method

.method public getStreamList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/recognizer/pipeline/StreamDef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->stream_:Ljava/util/List;

    return-object v0
.end method

.method public getThreadList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/recognizer/pipeline/ThreadDef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->thread_:Ljava/util/List;

    return-object v0
.end method

.method public getTracerRef()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->tracerRef_:Ljava/lang/String;

    return-object v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->hasName:Z

    return v0
.end method

.method public hasTracerRef()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/google/speech/recognizer/pipeline/PipelineDef;->hasTracerRef:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 99
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getStreamList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/pipeline/StreamDef;

    .line 100
    .local v0, "element":Lcom/google/speech/recognizer/pipeline/StreamDef;
    invoke-virtual {v0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 108
    .end local v0    # "element":Lcom/google/speech/recognizer/pipeline/StreamDef;
    :goto_0
    return v2

    .line 102
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getResourceList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgreco/ResourceDef;

    .line 103
    .local v0, "element":Lgreco/ResourceDef;
    invoke-virtual {v0}, Lgreco/ResourceDef;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 105
    .end local v0    # "element":Lgreco/ResourceDef;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getSessionResourceList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgreco/ResourceDef;

    .line 106
    .restart local v0    # "element":Lgreco/ResourceDef;
    invoke-virtual {v0}, Lgreco/ResourceDef;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_4

    goto :goto_0

    .line 108
    .end local v0    # "element":Lgreco/ResourceDef;
    :cond_5
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->toBuilder()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;
    .locals 1

    .prologue
    .line 247
    invoke-static {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->newBuilder(Lcom/google/speech/recognizer/pipeline/PipelineDef;)Lcom/google/speech/recognizer/pipeline/PipelineDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getSerializedSize()I

    .line 114
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getStreamList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/pipeline/StreamDef;

    .line 115
    .local v0, "element":Lcom/google/speech/recognizer/pipeline/StreamDef;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 117
    .end local v0    # "element":Lcom/google/speech/recognizer/pipeline/StreamDef;
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getConnectList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/pipeline/ConnectionDef;

    .line 118
    .local v0, "element":Lcom/google/speech/recognizer/pipeline/ConnectionDef;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 120
    .end local v0    # "element":Lcom/google/speech/recognizer/pipeline/ConnectionDef;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getResourceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgreco/ResourceDef;

    .line 121
    .local v0, "element":Lgreco/ResourceDef;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    .line 123
    .end local v0    # "element":Lgreco/ResourceDef;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getThreadList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/pipeline/ThreadDef;

    .line 124
    .local v0, "element":Lcom/google/speech/recognizer/pipeline/ThreadDef;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    .line 126
    .end local v0    # "element":Lcom/google/speech/recognizer/pipeline/ThreadDef;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getSessionResourceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgreco/ResourceDef;

    .line 127
    .local v0, "element":Lgreco/ResourceDef;
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_4

    .line 129
    .end local v0    # "element":Lgreco/ResourceDef;
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->hasTracerRef()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 130
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getTracerRef()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 132
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->hasName()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 133
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/PipelineDef;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 135
    :cond_6
    return-void
.end method
