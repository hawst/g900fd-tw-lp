.class public final Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
.source "StreamDef.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/recognizer/pipeline/StreamDef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
        "<",
        "Lcom/google/speech/recognizer/pipeline/StreamDef;",
        "Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/recognizer/pipeline/StreamDef;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    .locals 1

    .prologue
    .line 193
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->create()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    .locals 3

    .prologue
    .line 202
    new-instance v0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    invoke-direct {v0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;-><init>()V

    .line 203
    .local v0, "builder":Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    new-instance v1, Lcom/google/speech/recognizer/pipeline/StreamDef;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/recognizer/pipeline/StreamDef;-><init>(Lcom/google/speech/recognizer/pipeline/StreamDef$1;)V

    iput-object v1, v0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    .line 204
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->build()Lcom/google/speech/recognizer/pipeline/StreamDef;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/recognizer/pipeline/StreamDef;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 235
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->buildPartial()Lcom/google/speech/recognizer/pipeline/StreamDef;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/recognizer/pipeline/StreamDef;
    .locals 3

    .prologue
    .line 248
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    if-nez v1, :cond_0

    .line 249
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$300(Lcom/google/speech/recognizer/pipeline/StreamDef;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 253
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    iget-object v2, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$300(Lcom/google/speech/recognizer/pipeline/StreamDef;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$302(Lcom/google/speech/recognizer/pipeline/StreamDef;Ljava/util/List;)Ljava/util/List;

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    .line 257
    .local v0, "returnMe":Lcom/google/speech/recognizer/pipeline/StreamDef;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    .line 258
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->clone()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->clone()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->clone()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    .locals 2

    .prologue
    .line 221
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->create()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    invoke-virtual {v0, v1}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->mergeFrom(Lcom/google/speech/recognizer/pipeline/StreamDef;)Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->clone()Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic internalGetResult()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->internalGetResult()Lcom/google/speech/recognizer/pipeline/StreamDef;

    move-result-object v0

    return-object v0
.end method

.method protected internalGetResult()Lcom/google/speech/recognizer/pipeline/StreamDef;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    invoke-virtual {v0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 193
    check-cast p1, Lcom/google/speech/recognizer/pipeline/StreamDef;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->mergeFrom(Lcom/google/speech/recognizer/pipeline/StreamDef;)Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/recognizer/pipeline/StreamDef;)Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/recognizer/pipeline/StreamDef;

    .prologue
    .line 262
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getDefaultInstance()Lcom/google/speech/recognizer/pipeline/StreamDef;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 279
    :goto_0
    return-object p0

    .line 263
    :cond_0
    invoke-virtual {p1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasName()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    invoke-virtual {p1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->setName(Ljava/lang/String;)Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    .line 266
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasClassname()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 267
    invoke-virtual {p1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getClassname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->setClassname(Ljava/lang/String;)Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    .line 269
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasParams()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 270
    invoke-virtual {p1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->getParams()Lgreco/Params;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->mergeParams(Lgreco/Params;)Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;

    .line 272
    :cond_3
    # getter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$300(Lcom/google/speech/recognizer/pipeline/StreamDef;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 273
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$300(Lcom/google/speech/recognizer/pipeline/StreamDef;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 274
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$302(Lcom/google/speech/recognizer/pipeline/StreamDef;Ljava/util/List;)Ljava/util/List;

    .line 276
    :cond_4
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$300(Lcom/google/speech/recognizer/pipeline/StreamDef;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->paramsRef_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$300(Lcom/google/speech/recognizer/pipeline/StreamDef;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 278
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->mergeExtensionFields(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;)V

    goto :goto_0
.end method

.method public mergeParams(Lgreco/Params;)Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    .locals 2
    .param p1, "value"    # Lgreco/Params;

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    invoke-virtual {v0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->hasParams()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->params_:Lgreco/Params;
    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$900(Lcom/google/speech/recognizer/pipeline/StreamDef;)Lgreco/Params;

    move-result-object v0

    invoke-static {}, Lgreco/Params;->getDefaultInstance()Lgreco/Params;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 388
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    # getter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->params_:Lgreco/Params;
    invoke-static {v1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$900(Lcom/google/speech/recognizer/pipeline/StreamDef;)Lgreco/Params;

    move-result-object v1

    invoke-static {v1}, Lgreco/Params;->newBuilder(Lgreco/Params;)Lgreco/Params$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lgreco/Params$Builder;->mergeFrom(Lgreco/Params;)Lgreco/Params$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lgreco/Params$Builder;->buildPartial()Lgreco/Params;

    move-result-object v1

    # setter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->params_:Lgreco/Params;
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$902(Lcom/google/speech/recognizer/pipeline/StreamDef;Lgreco/Params;)Lgreco/Params;

    .line 393
    :goto_0
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->hasParams:Z
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$802(Lcom/google/speech/recognizer/pipeline/StreamDef;Z)Z

    .line 394
    return-object p0

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    # setter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->params_:Lgreco/Params;
    invoke-static {v0, p1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$902(Lcom/google/speech/recognizer/pipeline/StreamDef;Lgreco/Params;)Lgreco/Params;

    goto :goto_0
.end method

.method public setClassname(Ljava/lang/String;)Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 352
    if-nez p1, :cond_0

    .line 353
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->hasClassname:Z
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$602(Lcom/google/speech/recognizer/pipeline/StreamDef;Z)Z

    .line 356
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    # setter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->classname_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$702(Lcom/google/speech/recognizer/pipeline/StreamDef;Ljava/lang/String;)Ljava/lang/String;

    .line 357
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 331
    if-nez p1, :cond_0

    .line 332
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$402(Lcom/google/speech/recognizer/pipeline/StreamDef;Z)Z

    .line 335
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/StreamDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/StreamDef;

    # setter for: Lcom/google/speech/recognizer/pipeline/StreamDef;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/recognizer/pipeline/StreamDef;->access$502(Lcom/google/speech/recognizer/pipeline/StreamDef;Ljava/lang/String;)Ljava/lang/String;

    .line 336
    return-object p0
.end method
