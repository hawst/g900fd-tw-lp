.class public final Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ThreadDef.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/recognizer/pipeline/ThreadDef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/recognizer/pipeline/ThreadDef;",
        "Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/recognizer/pipeline/ThreadDef;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;
    .locals 1

    .prologue
    .line 131
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->create()Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;
    .locals 3

    .prologue
    .line 140
    new-instance v0, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;

    invoke-direct {v0}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;-><init>()V

    .line 141
    .local v0, "builder":Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;
    new-instance v1, Lcom/google/speech/recognizer/pipeline/ThreadDef;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/recognizer/pipeline/ThreadDef;-><init>(Lcom/google/speech/recognizer/pipeline/ThreadDef$1;)V

    iput-object v1, v0, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/ThreadDef;

    .line 142
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->build()Lcom/google/speech/recognizer/pipeline/ThreadDef;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/recognizer/pipeline/ThreadDef;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/ThreadDef;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/ThreadDef;

    invoke-static {v0}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 173
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->buildPartial()Lcom/google/speech/recognizer/pipeline/ThreadDef;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/recognizer/pipeline/ThreadDef;
    .locals 3

    .prologue
    .line 186
    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/ThreadDef;

    if-nez v1, :cond_0

    .line 187
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/ThreadDef;

    .line 191
    .local v0, "returnMe":Lcom/google/speech/recognizer/pipeline/ThreadDef;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/ThreadDef;

    .line 192
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->clone()Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->clone()Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;
    .locals 2

    .prologue
    .line 159
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->create()Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/ThreadDef;

    invoke-virtual {v0, v1}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->mergeFrom(Lcom/google/speech/recognizer/pipeline/ThreadDef;)Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->clone()Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/ThreadDef;

    invoke-virtual {v0}, Lcom/google/speech/recognizer/pipeline/ThreadDef;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 131
    check-cast p1, Lcom/google/speech/recognizer/pipeline/ThreadDef;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->mergeFrom(Lcom/google/speech/recognizer/pipeline/ThreadDef;)Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/recognizer/pipeline/ThreadDef;)Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/recognizer/pipeline/ThreadDef;

    .prologue
    .line 196
    invoke-static {}, Lcom/google/speech/recognizer/pipeline/ThreadDef;->getDefaultInstance()Lcom/google/speech/recognizer/pipeline/ThreadDef;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 200
    :cond_0
    :goto_0
    return-object p0

    .line 197
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/recognizer/pipeline/ThreadDef;->hasStream()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {p1}, Lcom/google/speech/recognizer/pipeline/ThreadDef;->getStream()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->setStream(Ljava/lang/String;)Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;

    goto :goto_0
.end method

.method public setStream(Ljava/lang/String;)Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 235
    if-nez p1, :cond_0

    .line 236
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/ThreadDef;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/recognizer/pipeline/ThreadDef;->hasStream:Z
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/pipeline/ThreadDef;->access$302(Lcom/google/speech/recognizer/pipeline/ThreadDef;Z)Z

    .line 239
    iget-object v0, p0, Lcom/google/speech/recognizer/pipeline/ThreadDef$Builder;->result:Lcom/google/speech/recognizer/pipeline/ThreadDef;

    # setter for: Lcom/google/speech/recognizer/pipeline/ThreadDef;->stream_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/recognizer/pipeline/ThreadDef;->access$402(Lcom/google/speech/recognizer/pipeline/ThreadDef;Ljava/lang/String;)Ljava/lang/String;

    .line 240
    return-object p0
.end method
