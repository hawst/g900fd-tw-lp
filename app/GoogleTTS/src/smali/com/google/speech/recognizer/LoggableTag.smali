.class public final Lcom/google/speech/recognizer/LoggableTag;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LoggableTag.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/recognizer/LoggableTag$1;,
        Lcom/google/speech/recognizer/LoggableTag$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/recognizer/LoggableTag;

.field public static final logId:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension",
            "<",
            "Lcom/google/speech/recognizer/Loggable;",
            "Lcom/google/speech/recognizer/LoggableTag;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private hasTag:Z

.field private memoizedSerializedSize:I

.field private tag_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->newGeneratedExtension()Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    move-result-object v0

    sput-object v0, Lcom/google/speech/recognizer/LoggableTag;->logId:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    .line 259
    new-instance v0, Lcom/google/speech/recognizer/LoggableTag;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/recognizer/LoggableTag;-><init>(Z)V

    sput-object v0, Lcom/google/speech/recognizer/LoggableTag;->defaultInstance:Lcom/google/speech/recognizer/LoggableTag;

    .line 260
    invoke-static {}, Lcom/google/speech/recognizer/LoggableTagProto;->internalForceInit()V

    .line 261
    sget-object v0, Lcom/google/speech/recognizer/LoggableTag;->defaultInstance:Lcom/google/speech/recognizer/LoggableTag;

    invoke-direct {v0}, Lcom/google/speech/recognizer/LoggableTag;->initFields()V

    .line 262
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/LoggableTag;->tag_:Ljava/lang/String;

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/LoggableTag;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/recognizer/LoggableTag;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/recognizer/LoggableTag$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/recognizer/LoggableTag$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/recognizer/LoggableTag;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/LoggableTag;->tag_:Ljava/lang/String;

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/LoggableTag;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/recognizer/LoggableTag;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/LoggableTag;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/recognizer/LoggableTag;->hasTag:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/recognizer/LoggableTag;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/recognizer/LoggableTag;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/recognizer/LoggableTag;->tag_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/recognizer/LoggableTag;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/recognizer/LoggableTag;->defaultInstance:Lcom/google/speech/recognizer/LoggableTag;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/recognizer/LoggableTag$Builder;
    .locals 1

    .prologue
    .line 131
    # invokes: Lcom/google/speech/recognizer/LoggableTag$Builder;->create()Lcom/google/speech/recognizer/LoggableTag$Builder;
    invoke-static {}, Lcom/google/speech/recognizer/LoggableTag$Builder;->access$100()Lcom/google/speech/recognizer/LoggableTag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/recognizer/LoggableTag;)Lcom/google/speech/recognizer/LoggableTag$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/recognizer/LoggableTag;

    .prologue
    .line 134
    invoke-static {}, Lcom/google/speech/recognizer/LoggableTag;->newBuilder()Lcom/google/speech/recognizer/LoggableTag$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/recognizer/LoggableTag$Builder;->mergeFrom(Lcom/google/speech/recognizer/LoggableTag;)Lcom/google/speech/recognizer/LoggableTag$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag;->getDefaultInstanceForType()Lcom/google/speech/recognizer/LoggableTag;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/recognizer/LoggableTag;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/recognizer/LoggableTag;->defaultInstance:Lcom/google/speech/recognizer/LoggableTag;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 52
    iget v0, p0, Lcom/google/speech/recognizer/LoggableTag;->memoizedSerializedSize:I

    .line 53
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 61
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 55
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 56
    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag;->hasTag()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 57
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 60
    :cond_1
    iput v0, p0, Lcom/google/speech/recognizer/LoggableTag;->memoizedSerializedSize:I

    move v1, v0

    .line 61
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/recognizer/LoggableTag;->tag_:Ljava/lang/String;

    return-object v0
.end method

.method public hasTag()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/recognizer/LoggableTag;->hasTag:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag;->toBuilder()Lcom/google/speech/recognizer/LoggableTag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/recognizer/LoggableTag$Builder;
    .locals 1

    .prologue
    .line 136
    invoke-static {p0}, Lcom/google/speech/recognizer/LoggableTag;->newBuilder(Lcom/google/speech/recognizer/LoggableTag;)Lcom/google/speech/recognizer/LoggableTag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag;->getSerializedSize()I

    .line 45
    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag;->hasTag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 48
    :cond_0
    return-void
.end method
