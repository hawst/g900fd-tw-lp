.class public final Lcom/google/speech/recognizer/LoggableTag$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LoggableTag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/recognizer/LoggableTag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/recognizer/LoggableTag;",
        "Lcom/google/speech/recognizer/LoggableTag$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/recognizer/LoggableTag;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/recognizer/LoggableTag$Builder;
    .locals 1

    .prologue
    .line 138
    invoke-static {}, Lcom/google/speech/recognizer/LoggableTag$Builder;->create()Lcom/google/speech/recognizer/LoggableTag$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/recognizer/LoggableTag$Builder;
    .locals 3

    .prologue
    .line 147
    new-instance v0, Lcom/google/speech/recognizer/LoggableTag$Builder;

    invoke-direct {v0}, Lcom/google/speech/recognizer/LoggableTag$Builder;-><init>()V

    .line 148
    .local v0, "builder":Lcom/google/speech/recognizer/LoggableTag$Builder;
    new-instance v1, Lcom/google/speech/recognizer/LoggableTag;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/recognizer/LoggableTag;-><init>(Lcom/google/speech/recognizer/LoggableTag$1;)V

    iput-object v1, v0, Lcom/google/speech/recognizer/LoggableTag$Builder;->result:Lcom/google/speech/recognizer/LoggableTag;

    .line 149
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag$Builder;->build()Lcom/google/speech/recognizer/LoggableTag;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/recognizer/LoggableTag;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/speech/recognizer/LoggableTag$Builder;->result:Lcom/google/speech/recognizer/LoggableTag;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/speech/recognizer/LoggableTag$Builder;->result:Lcom/google/speech/recognizer/LoggableTag;

    invoke-static {v0}, Lcom/google/speech/recognizer/LoggableTag$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag$Builder;->buildPartial()Lcom/google/speech/recognizer/LoggableTag;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/recognizer/LoggableTag;
    .locals 3

    .prologue
    .line 193
    iget-object v1, p0, Lcom/google/speech/recognizer/LoggableTag$Builder;->result:Lcom/google/speech/recognizer/LoggableTag;

    if-nez v1, :cond_0

    .line 194
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/LoggableTag$Builder;->result:Lcom/google/speech/recognizer/LoggableTag;

    .line 198
    .local v0, "returnMe":Lcom/google/speech/recognizer/LoggableTag;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/recognizer/LoggableTag$Builder;->result:Lcom/google/speech/recognizer/LoggableTag;

    .line 199
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag$Builder;->clone()Lcom/google/speech/recognizer/LoggableTag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag$Builder;->clone()Lcom/google/speech/recognizer/LoggableTag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/recognizer/LoggableTag$Builder;
    .locals 2

    .prologue
    .line 166
    invoke-static {}, Lcom/google/speech/recognizer/LoggableTag$Builder;->create()Lcom/google/speech/recognizer/LoggableTag$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/recognizer/LoggableTag$Builder;->result:Lcom/google/speech/recognizer/LoggableTag;

    invoke-virtual {v0, v1}, Lcom/google/speech/recognizer/LoggableTag$Builder;->mergeFrom(Lcom/google/speech/recognizer/LoggableTag;)Lcom/google/speech/recognizer/LoggableTag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/speech/recognizer/LoggableTag$Builder;->clone()Lcom/google/speech/recognizer/LoggableTag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/speech/recognizer/LoggableTag$Builder;->result:Lcom/google/speech/recognizer/LoggableTag;

    invoke-virtual {v0}, Lcom/google/speech/recognizer/LoggableTag;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 138
    check-cast p1, Lcom/google/speech/recognizer/LoggableTag;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/recognizer/LoggableTag$Builder;->mergeFrom(Lcom/google/speech/recognizer/LoggableTag;)Lcom/google/speech/recognizer/LoggableTag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/recognizer/LoggableTag;)Lcom/google/speech/recognizer/LoggableTag$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/recognizer/LoggableTag;

    .prologue
    .line 203
    invoke-static {}, Lcom/google/speech/recognizer/LoggableTag;->getDefaultInstance()Lcom/google/speech/recognizer/LoggableTag;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-object p0

    .line 204
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/recognizer/LoggableTag;->hasTag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {p1}, Lcom/google/speech/recognizer/LoggableTag;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/recognizer/LoggableTag$Builder;->setTag(Ljava/lang/String;)Lcom/google/speech/recognizer/LoggableTag$Builder;

    goto :goto_0
.end method

.method public setTag(Ljava/lang/String;)Lcom/google/speech/recognizer/LoggableTag$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 242
    if-nez p1, :cond_0

    .line 243
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/LoggableTag$Builder;->result:Lcom/google/speech/recognizer/LoggableTag;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/recognizer/LoggableTag;->hasTag:Z
    invoke-static {v0, v1}, Lcom/google/speech/recognizer/LoggableTag;->access$302(Lcom/google/speech/recognizer/LoggableTag;Z)Z

    .line 246
    iget-object v0, p0, Lcom/google/speech/recognizer/LoggableTag$Builder;->result:Lcom/google/speech/recognizer/LoggableTag;

    # setter for: Lcom/google/speech/recognizer/LoggableTag;->tag_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/recognizer/LoggableTag;->access$402(Lcom/google/speech/recognizer/LoggableTag;Ljava/lang/String;)Ljava/lang/String;

    .line 247
    return-object p0
.end method
