.class public final Lcom/google/speech/patts/PattsRequest$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PattsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/PattsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/PattsRequest;",
        "Lcom/google/speech/patts/PattsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/PattsRequest;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/PattsRequest$Builder;
    .locals 1

    .prologue
    .line 153
    invoke-static {}, Lcom/google/speech/patts/PattsRequest$Builder;->create()Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/PattsRequest$Builder;
    .locals 3

    .prologue
    .line 162
    new-instance v0, Lcom/google/speech/patts/PattsRequest$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/PattsRequest$Builder;-><init>()V

    .line 163
    .local v0, "builder":Lcom/google/speech/patts/PattsRequest$Builder;
    new-instance v1, Lcom/google/speech/patts/PattsRequest;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/PattsRequest;-><init>(Lcom/google/speech/patts/PattsRequest$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    .line 164
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest$Builder;->build()Lcom/google/speech/patts/PattsRequest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/PattsRequest;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    invoke-static {v0}, Lcom/google/speech/patts/PattsRequest$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 195
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest$Builder;->buildPartial()Lcom/google/speech/patts/PattsRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/PattsRequest;
    .locals 3

    .prologue
    .line 208
    iget-object v1, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    if-nez v1, :cond_0

    .line 209
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    .line 213
    .local v0, "returnMe":Lcom/google/speech/patts/PattsRequest;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    .line 214
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest$Builder;->clone()Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest$Builder;->clone()Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/PattsRequest$Builder;
    .locals 2

    .prologue
    .line 181
    invoke-static {}, Lcom/google/speech/patts/PattsRequest$Builder;->create()Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/PattsRequest$Builder;->mergeFrom(Lcom/google/speech/patts/PattsRequest;)Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest$Builder;->clone()Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    invoke-virtual {v0}, Lcom/google/speech/patts/PattsRequest;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 153
    check-cast p1, Lcom/google/speech/patts/PattsRequest;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/PattsRequest$Builder;->mergeFrom(Lcom/google/speech/patts/PattsRequest;)Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/PattsRequest;)Lcom/google/speech/patts/PattsRequest$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/patts/PattsRequest;

    .prologue
    .line 218
    invoke-static {}, Lcom/google/speech/patts/PattsRequest;->getDefaultInstance()Lcom/google/speech/patts/PattsRequest;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-object p0

    .line 219
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/PattsRequest;->hasSentence()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 220
    invoke-virtual {p1}, Lcom/google/speech/patts/PattsRequest;->getSentence()Lcom/google/speech/patts/markup/Sentence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/PattsRequest$Builder;->mergeSentence(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/PattsRequest$Builder;

    .line 222
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/PattsRequest;->hasTtsSentence()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {p1}, Lcom/google/speech/patts/PattsRequest;->getTtsSentence()Lcom/google/speech/tts/Sentence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/PattsRequest$Builder;->mergeTtsSentence(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/patts/PattsRequest$Builder;

    goto :goto_0
.end method

.method public mergeSentence(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/PattsRequest$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/Sentence;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    invoke-virtual {v0}, Lcom/google/speech/patts/PattsRequest;->hasSentence()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    # getter for: Lcom/google/speech/patts/PattsRequest;->sentence_:Lcom/google/speech/patts/markup/Sentence;
    invoke-static {v0}, Lcom/google/speech/patts/PattsRequest;->access$400(Lcom/google/speech/patts/PattsRequest;)Lcom/google/speech/patts/markup/Sentence;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/Sentence;->getDefaultInstance()Lcom/google/speech/patts/markup/Sentence;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    iget-object v1, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    # getter for: Lcom/google/speech/patts/PattsRequest;->sentence_:Lcom/google/speech/patts/markup/Sentence;
    invoke-static {v1}, Lcom/google/speech/patts/PattsRequest;->access$400(Lcom/google/speech/patts/PattsRequest;)Lcom/google/speech/patts/markup/Sentence;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/Sentence;->newBuilder(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/Sentence$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Sentence$Builder;->buildPartial()Lcom/google/speech/patts/markup/Sentence;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/PattsRequest;->sentence_:Lcom/google/speech/patts/markup/Sentence;
    invoke-static {v0, v1}, Lcom/google/speech/patts/PattsRequest;->access$402(Lcom/google/speech/patts/PattsRequest;Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Sentence;

    .line 294
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/PattsRequest;->hasSentence:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/PattsRequest;->access$302(Lcom/google/speech/patts/PattsRequest;Z)Z

    .line 295
    return-object p0

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    # setter for: Lcom/google/speech/patts/PattsRequest;->sentence_:Lcom/google/speech/patts/markup/Sentence;
    invoke-static {v0, p1}, Lcom/google/speech/patts/PattsRequest;->access$402(Lcom/google/speech/patts/PattsRequest;Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Sentence;

    goto :goto_0
.end method

.method public mergeTtsSentence(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/patts/PattsRequest$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Sentence;

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    invoke-virtual {v0}, Lcom/google/speech/patts/PattsRequest;->hasTtsSentence()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    # getter for: Lcom/google/speech/patts/PattsRequest;->ttsSentence_:Lcom/google/speech/tts/Sentence;
    invoke-static {v0}, Lcom/google/speech/patts/PattsRequest;->access$600(Lcom/google/speech/patts/PattsRequest;)Lcom/google/speech/tts/Sentence;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Sentence;->getDefaultInstance()Lcom/google/speech/tts/Sentence;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 326
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    iget-object v1, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    # getter for: Lcom/google/speech/patts/PattsRequest;->ttsSentence_:Lcom/google/speech/tts/Sentence;
    invoke-static {v1}, Lcom/google/speech/patts/PattsRequest;->access$600(Lcom/google/speech/patts/PattsRequest;)Lcom/google/speech/tts/Sentence;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Sentence;->newBuilder(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Sentence$Builder;->mergeFrom(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Sentence$Builder;->buildPartial()Lcom/google/speech/tts/Sentence;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/PattsRequest;->ttsSentence_:Lcom/google/speech/tts/Sentence;
    invoke-static {v0, v1}, Lcom/google/speech/patts/PattsRequest;->access$602(Lcom/google/speech/patts/PattsRequest;Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence;

    .line 331
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/PattsRequest;->hasTtsSentence:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/PattsRequest;->access$502(Lcom/google/speech/patts/PattsRequest;Z)Z

    .line 332
    return-object p0

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    # setter for: Lcom/google/speech/patts/PattsRequest;->ttsSentence_:Lcom/google/speech/tts/Sentence;
    invoke-static {v0, p1}, Lcom/google/speech/patts/PattsRequest;->access$602(Lcom/google/speech/patts/PattsRequest;Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence;

    goto :goto_0
.end method

.method public setTtsSentence(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/patts/PattsRequest$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Sentence;

    .prologue
    .line 311
    if-nez p1, :cond_0

    .line 312
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/PattsRequest;->hasTtsSentence:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/PattsRequest;->access$502(Lcom/google/speech/patts/PattsRequest;Z)Z

    .line 315
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest$Builder;->result:Lcom/google/speech/patts/PattsRequest;

    # setter for: Lcom/google/speech/patts/PattsRequest;->ttsSentence_:Lcom/google/speech/tts/Sentence;
    invoke-static {v0, p1}, Lcom/google/speech/patts/PattsRequest;->access$602(Lcom/google/speech/patts/PattsRequest;Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence;

    .line 316
    return-object p0
.end method
