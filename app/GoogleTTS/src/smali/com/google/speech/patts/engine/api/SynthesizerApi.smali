.class public final Lcom/google/speech/patts/engine/api/SynthesizerApi;
.super Lcom/google/speech/patts/engine/api/ApiBase;
.source "SynthesizerApi.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/engine/api/ApiBase;-><init>()V

    return-void
.end method

.method public static native createSynthesizer(JJ)J
.end method

.method public static native deleteSynthesizer(J)V
.end method

.method public static native generateOutputInfo(JJ)Lcom/google/speech/tts/output_info/Utterance;
.end method

.method public static native getVoiceSampleRate(J)I
.end method

.method public static native initBufferedSynthesis(JJ)J
.end method

.method public static native isAudiable(JJ)Z
.end method

.method public static native waveGenerationBuffered(JJ[SJ)I
.end method

.method public static native waveGenerationToByteBuffer(JJLjava/nio/ByteBuffer;J)I
.end method

.method public static native waveGenerationWhole(JJ)Z
.end method

.method public static native wordIdsToUnits(JJ)Z
.end method
