.class public final Lcom/google/speech/patts/Syllable$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Syllable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/Syllable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/Syllable;",
        "Lcom/google/speech/patts/Syllable$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/Syllable;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/Syllable$Builder;
    .locals 1

    .prologue
    .line 229
    invoke-static {}, Lcom/google/speech/patts/Syllable$Builder;->create()Lcom/google/speech/patts/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/Syllable$Builder;
    .locals 3

    .prologue
    .line 238
    new-instance v0, Lcom/google/speech/patts/Syllable$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/Syllable$Builder;-><init>()V

    .line 239
    .local v0, "builder":Lcom/google/speech/patts/Syllable$Builder;
    new-instance v1, Lcom/google/speech/patts/Syllable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/Syllable;-><init>(Lcom/google/speech/patts/Syllable$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    .line 240
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable$Builder;->build()Lcom/google/speech/patts/Syllable;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/Syllable;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    invoke-static {v0}, Lcom/google/speech/patts/Syllable$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 271
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable$Builder;->buildPartial()Lcom/google/speech/patts/Syllable;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/Syllable;
    .locals 3

    .prologue
    .line 284
    iget-object v1, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    if-nez v1, :cond_0

    .line 285
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    .line 289
    .local v0, "returnMe":Lcom/google/speech/patts/Syllable;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    .line 290
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable$Builder;->clone()Lcom/google/speech/patts/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable$Builder;->clone()Lcom/google/speech/patts/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/Syllable$Builder;
    .locals 2

    .prologue
    .line 257
    invoke-static {}, Lcom/google/speech/patts/Syllable$Builder;->create()Lcom/google/speech/patts/Syllable$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/Syllable$Builder;->mergeFrom(Lcom/google/speech/patts/Syllable;)Lcom/google/speech/patts/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable$Builder;->clone()Lcom/google/speech/patts/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    invoke-virtual {v0}, Lcom/google/speech/patts/Syllable;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 229
    check-cast p1, Lcom/google/speech/patts/Syllable;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/Syllable$Builder;->mergeFrom(Lcom/google/speech/patts/Syllable;)Lcom/google/speech/patts/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/Syllable;)Lcom/google/speech/patts/Syllable$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/patts/Syllable;

    .prologue
    .line 294
    invoke-static {}, Lcom/google/speech/patts/Syllable;->getDefaultInstance()Lcom/google/speech/patts/Syllable;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 319
    :cond_0
    :goto_0
    return-object p0

    .line 295
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->hasParent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 296
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->getParent()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Syllable$Builder;->setParent(I)Lcom/google/speech/patts/Syllable$Builder;

    .line 298
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->hasFirstDaughter()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 299
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->getFirstDaughter()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Syllable$Builder;->setFirstDaughter(I)Lcom/google/speech/patts/Syllable$Builder;

    .line 301
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->hasLastDaughter()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 302
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->getLastDaughter()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Syllable$Builder;->setLastDaughter(I)Lcom/google/speech/patts/Syllable$Builder;

    .line 304
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->hasContour()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 305
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->getContour()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Syllable$Builder;->setContour(Ljava/lang/String;)Lcom/google/speech/patts/Syllable$Builder;

    .line 307
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->hasStress()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 308
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->getStress()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Syllable$Builder;->setStress(I)Lcom/google/speech/patts/Syllable$Builder;

    .line 310
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->hasTone()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 311
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->getTone()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Syllable$Builder;->setTone(I)Lcom/google/speech/patts/Syllable$Builder;

    .line 313
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->hasName()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 314
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Syllable$Builder;->setName(Ljava/lang/String;)Lcom/google/speech/patts/Syllable$Builder;

    .line 316
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->hasSupraSegmentalModelName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    invoke-virtual {p1}, Lcom/google/speech/patts/Syllable;->getSupraSegmentalModelName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Syllable$Builder;->setSupraSegmentalModelName(Ljava/lang/String;)Lcom/google/speech/patts/Syllable$Builder;

    goto :goto_0
.end method

.method public setContour(Ljava/lang/String;)Lcom/google/speech/patts/Syllable$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 436
    if-nez p1, :cond_0

    .line 437
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Syllable;->hasContour:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Syllable;->access$902(Lcom/google/speech/patts/Syllable;Z)Z

    .line 440
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    # setter for: Lcom/google/speech/patts/Syllable;->contour_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/Syllable;->access$1002(Lcom/google/speech/patts/Syllable;Ljava/lang/String;)Ljava/lang/String;

    .line 441
    return-object p0
.end method

.method public setFirstDaughter(I)Lcom/google/speech/patts/Syllable$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Syllable;->hasFirstDaughter:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Syllable;->access$502(Lcom/google/speech/patts/Syllable;Z)Z

    .line 401
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    # setter for: Lcom/google/speech/patts/Syllable;->firstDaughter_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Syllable;->access$602(Lcom/google/speech/patts/Syllable;I)I

    .line 402
    return-object p0
.end method

.method public setLastDaughter(I)Lcom/google/speech/patts/Syllable$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Syllable;->hasLastDaughter:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Syllable;->access$702(Lcom/google/speech/patts/Syllable;Z)Z

    .line 419
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    # setter for: Lcom/google/speech/patts/Syllable;->lastDaughter_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Syllable;->access$802(Lcom/google/speech/patts/Syllable;I)I

    .line 420
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/patts/Syllable$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 493
    if-nez p1, :cond_0

    .line 494
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Syllable;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Syllable;->access$1502(Lcom/google/speech/patts/Syllable;Z)Z

    .line 497
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    # setter for: Lcom/google/speech/patts/Syllable;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/Syllable;->access$1602(Lcom/google/speech/patts/Syllable;Ljava/lang/String;)Ljava/lang/String;

    .line 498
    return-object p0
.end method

.method public setParent(I)Lcom/google/speech/patts/Syllable$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Syllable;->hasParent:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Syllable;->access$302(Lcom/google/speech/patts/Syllable;Z)Z

    .line 383
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    # setter for: Lcom/google/speech/patts/Syllable;->parent_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Syllable;->access$402(Lcom/google/speech/patts/Syllable;I)I

    .line 384
    return-object p0
.end method

.method public setStress(I)Lcom/google/speech/patts/Syllable$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Syllable;->hasStress:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Syllable;->access$1102(Lcom/google/speech/patts/Syllable;Z)Z

    .line 458
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    # setter for: Lcom/google/speech/patts/Syllable;->stress_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Syllable;->access$1202(Lcom/google/speech/patts/Syllable;I)I

    .line 459
    return-object p0
.end method

.method public setSupraSegmentalModelName(Ljava/lang/String;)Lcom/google/speech/patts/Syllable$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 514
    if-nez p1, :cond_0

    .line 515
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Syllable;->hasSupraSegmentalModelName:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Syllable;->access$1702(Lcom/google/speech/patts/Syllable;Z)Z

    .line 518
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    # setter for: Lcom/google/speech/patts/Syllable;->supraSegmentalModelName_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/Syllable;->access$1802(Lcom/google/speech/patts/Syllable;Ljava/lang/String;)Ljava/lang/String;

    .line 519
    return-object p0
.end method

.method public setTone(I)Lcom/google/speech/patts/Syllable$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Syllable;->hasTone:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Syllable;->access$1302(Lcom/google/speech/patts/Syllable;Z)Z

    .line 476
    iget-object v0, p0, Lcom/google/speech/patts/Syllable$Builder;->result:Lcom/google/speech/patts/Syllable;

    # setter for: Lcom/google/speech/patts/Syllable;->tone_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Syllable;->access$1402(Lcom/google/speech/patts/Syllable;I)I

    .line 477
    return-object p0
.end method
