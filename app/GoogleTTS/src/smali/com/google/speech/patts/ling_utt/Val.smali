.class public final Lcom/google/speech/patts/ling_utt/Val;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Val.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/ling_utt/Val$1;,
        Lcom/google/speech/patts/ling_utt/Val$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/ling_utt/Val;


# instance fields
.field private bval_:Z

.field private fval_:F

.field private fvval_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private hasBval:Z

.field private hasFval:Z

.field private hasIval:Z

.field private hasName:Z

.field private hasSval:Z

.field private ival_:I

.field private ivval_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private sval_:Ljava/lang/String;

.field private svval_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private tmval_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/ling_utt/Val;",
            ">;"
        }
    .end annotation
.end field

.field private vvval_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/ling_utt/Val;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 827
    new-instance v0, Lcom/google/speech/patts/ling_utt/Val;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/ling_utt/Val;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/ling_utt/Val;->defaultInstance:Lcom/google/speech/patts/ling_utt/Val;

    .line 828
    invoke-static {}, Lcom/google/speech/patts/ling_utt/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 829
    sget-object v0, Lcom/google/speech/patts/ling_utt/Val;->defaultInstance:Lcom/google/speech/patts/ling_utt/Val;

    invoke-direct {v0}, Lcom/google/speech/patts/ling_utt/Val;->initFields()V

    .line 830
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->name_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->sval_:Ljava/lang/String;

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;

    .line 62
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;

    .line 74
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;

    .line 87
    iput v1, p0, Lcom/google/speech/patts/ling_utt/Val;->ival_:I

    .line 94
    iput-boolean v1, p0, Lcom/google/speech/patts/ling_utt/Val;->bval_:Z

    .line 101
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/patts/ling_utt/Val;->fval_:F

    .line 107
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;

    .line 165
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/ling_utt/Val;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/ling_utt/Val;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/ling_utt/Val$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/ling_utt/Val$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/ling_utt/Val;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->name_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->sval_:Ljava/lang/String;

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;

    .line 62
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;

    .line 74
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;

    .line 87
    iput v1, p0, Lcom/google/speech/patts/ling_utt/Val;->ival_:I

    .line 94
    iput-boolean v1, p0, Lcom/google/speech/patts/ling_utt/Val;->bval_:Z

    .line 101
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/patts/ling_utt/Val;->fval_:F

    .line 107
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;

    .line 165
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/ling_utt/Val;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/ling_utt/Val;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Val;->hasSval:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/patts/ling_utt/Val;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Val;->sval_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/speech/patts/ling_utt/Val;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Val;->hasIval:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/patts/ling_utt/Val;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/ling_utt/Val;->ival_:I

    return p1
.end method

.method static synthetic access$1402(Lcom/google/speech/patts/ling_utt/Val;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Val;->hasBval:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/patts/ling_utt/Val;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Val;->bval_:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/google/speech/patts/ling_utt/Val;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Val;->hasFval:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/patts/ling_utt/Val;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/ling_utt/Val;->fval_:F

    return p1
.end method

.method static synthetic access$300(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/ling_utt/Val;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Val;->hasName:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/ling_utt/Val;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Val;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Val;->name_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/ling_utt/Val;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/ling_utt/Val;->defaultInstance:Lcom/google/speech/patts/ling_utt/Val;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 118
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/ling_utt/Val$Builder;
    .locals 1

    .prologue
    .line 294
    # invokes: Lcom/google/speech/patts/ling_utt/Val$Builder;->create()Lcom/google/speech/patts/ling_utt/Val$Builder;
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Val$Builder;->access$100()Lcom/google/speech/patts/ling_utt/Val$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/ling_utt/Val;)Lcom/google/speech/patts/ling_utt/Val$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/ling_utt/Val;

    .prologue
    .line 297
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Val;->newBuilder()Lcom/google/speech/patts/ling_utt/Val$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->mergeFrom(Lcom/google/speech/patts/ling_utt/Val;)Lcom/google/speech/patts/ling_utt/Val$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBval()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Val;->bval_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getDefaultInstanceForType()Lcom/google/speech/patts/ling_utt/Val;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/ling_utt/Val;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/ling_utt/Val;->defaultInstance:Lcom/google/speech/patts/ling_utt/Val;

    return-object v0
.end method

.method public getFval()F
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/google/speech/patts/ling_utt/Val;->fval_:F

    return v0
.end method

.method public getFvvalList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;

    return-object v0
.end method

.method public getIval()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/speech/patts/ling_utt/Val;->ival_:I

    return v0
.end method

.method public getIvvalList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 167
    iget v3, p0, Lcom/google/speech/patts/ling_utt/Val;->memoizedSerializedSize:I

    .line 168
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 224
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 170
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 171
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->hasName()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 172
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 175
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->hasSval()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 176
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getSval()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 180
    :cond_2
    const/4 v0, 0x0

    .line 181
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getSvvalList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 182
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 184
    goto :goto_1

    .line 185
    .end local v1    # "element":Ljava/lang/String;
    :cond_3
    add-int/2addr v3, v0

    .line 186
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getSvvalList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 189
    const/4 v0, 0x0

    .line 190
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getFvvalList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v0, v5, 0x4

    .line 191
    add-int/2addr v3, v0

    .line 192
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getFvvalList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 195
    const/4 v0, 0x0

    .line 196
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getIvvalList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 197
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v0, v5

    .line 199
    goto :goto_2

    .line 200
    .end local v1    # "element":I
    :cond_4
    add-int/2addr v3, v0

    .line 201
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getIvvalList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 203
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getVvvalList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/patts/ling_utt/Val;

    .line 204
    .local v1, "element":Lcom/google/speech/patts/ling_utt/Val;
    const/4 v5, 0x6

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 206
    goto :goto_3

    .line 207
    .end local v1    # "element":Lcom/google/speech/patts/ling_utt/Val;
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->hasIval()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 208
    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getIval()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 211
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->hasBval()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 212
    const/16 v5, 0x8

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getBval()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 215
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->hasFval()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 216
    const/16 v5, 0x9

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getFval()F

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v5

    add-int/2addr v3, v5

    .line 219
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getTmvalList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/patts/ling_utt/Val;

    .line 220
    .restart local v1    # "element":Lcom/google/speech/patts/ling_utt/Val;
    const/16 v5, 0xa

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 222
    goto :goto_4

    .line 223
    .end local v1    # "element":Lcom/google/speech/patts/ling_utt/Val;
    :cond_9
    iput v3, p0, Lcom/google/speech/patts/ling_utt/Val;->memoizedSerializedSize:I

    move v4, v3

    .line 224
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getSval()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->sval_:Ljava/lang/String;

    return-object v0
.end method

.method public getSvvalList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;

    return-object v0
.end method

.method public getTmvalList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/ling_utt/Val;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;

    return-object v0
.end method

.method public getVvvalList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/ling_utt/Val;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;

    return-object v0
.end method

.method public hasBval()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Val;->hasBval:Z

    return v0
.end method

.method public hasFval()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Val;->hasFval:Z

    return v0
.end method

.method public hasIval()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Val;->hasIval:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Val;->hasName:Z

    return v0
.end method

.method public hasSval()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Val;->hasSval:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 120
    iget-boolean v3, p0, Lcom/google/speech/patts/ling_utt/Val;->hasName:Z

    if-nez v3, :cond_0

    .line 127
    :goto_0
    return v2

    .line 121
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getVvvalList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Val;

    .line 122
    .local v0, "element":Lcom/google/speech/patts/ling_utt/Val;
    invoke-virtual {v0}, Lcom/google/speech/patts/ling_utt/Val;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 124
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Val;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getTmvalList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Val;

    .line 125
    .restart local v0    # "element":Lcom/google/speech/patts/ling_utt/Val;
    invoke-virtual {v0}, Lcom/google/speech/patts/ling_utt/Val;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_0

    .line 127
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Val;
    :cond_4
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->toBuilder()Lcom/google/speech/patts/ling_utt/Val$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/ling_utt/Val$Builder;
    .locals 1

    .prologue
    .line 299
    invoke-static {p0}, Lcom/google/speech/patts/ling_utt/Val;->newBuilder(Lcom/google/speech/patts/ling_utt/Val;)Lcom/google/speech/patts/ling_utt/Val$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getSerializedSize()I

    .line 133
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->hasName()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 134
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 136
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->hasSval()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 137
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getSval()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 139
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getSvvalList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 140
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 142
    .end local v0    # "element":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getFvvalList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 143
    .local v0, "element":F
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    goto :goto_1

    .line 145
    .end local v0    # "element":F
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getIvvalList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 146
    .local v0, "element":I
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    goto :goto_2

    .line 148
    .end local v0    # "element":I
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getVvvalList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Val;

    .line 149
    .local v0, "element":Lcom/google/speech/patts/ling_utt/Val;
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    .line 151
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Val;
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->hasIval()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 152
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getIval()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 154
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->hasBval()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 155
    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getBval()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 157
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->hasFval()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 158
    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getFval()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 160
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val;->getTmvalList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Val;

    .line 161
    .restart local v0    # "element":Lcom/google/speech/patts/ling_utt/Val;
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_4

    .line 163
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Val;
    :cond_9
    return-void
.end method
