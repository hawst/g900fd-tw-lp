.class public final Lcom/google/speech/patts/UtteranceP$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
.source "UtteranceP.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/UtteranceP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
        "<",
        "Lcom/google/speech/patts/UtteranceP;",
        "Lcom/google/speech/patts/UtteranceP$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/UtteranceP;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/UtteranceP$Builder;
    .locals 1

    .prologue
    .line 325
    invoke-static {}, Lcom/google/speech/patts/UtteranceP$Builder;->create()Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/UtteranceP$Builder;
    .locals 3

    .prologue
    .line 334
    new-instance v0, Lcom/google/speech/patts/UtteranceP$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/UtteranceP$Builder;-><init>()V

    .line 335
    .local v0, "builder":Lcom/google/speech/patts/UtteranceP$Builder;
    new-instance v1, Lcom/google/speech/patts/UtteranceP;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/UtteranceP;-><init>(Lcom/google/speech/patts/UtteranceP$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    .line 336
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP$Builder;->build()Lcom/google/speech/patts/UtteranceP;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/UtteranceP;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 367
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP$Builder;->buildPartial()Lcom/google/speech/patts/UtteranceP;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/UtteranceP;
    .locals 3

    .prologue
    .line 380
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    if-nez v1, :cond_0

    .line 381
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 384
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/UtteranceP;->access$300(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 385
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    iget-object v2, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/UtteranceP;->access$300(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/UtteranceP;->access$302(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;

    .line 388
    :cond_1
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/UtteranceP;->access$400(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 389
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    iget-object v2, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/UtteranceP;->access$400(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/UtteranceP;->access$402(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;

    .line 392
    :cond_2
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/UtteranceP;->access$500(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 393
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    iget-object v2, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/UtteranceP;->access$500(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/UtteranceP;->access$502(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;

    .line 396
    :cond_3
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/UtteranceP;->access$600(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 397
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    iget-object v2, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/UtteranceP;->access$600(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/UtteranceP;->access$602(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;

    .line 400
    :cond_4
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/UtteranceP;->access$700(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_5

    .line 401
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    iget-object v2, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/UtteranceP;->access$700(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/UtteranceP;->access$702(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;

    .line 404
    :cond_5
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/UtteranceP;->access$800(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_6

    .line 405
    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    iget-object v2, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/UtteranceP;->access$800(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/UtteranceP;->access$802(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;

    .line 408
    :cond_6
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    .line 409
    .local v0, "returnMe":Lcom/google/speech/patts/UtteranceP;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    .line 410
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP$Builder;->clone()Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP$Builder;->clone()Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP$Builder;->clone()Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/UtteranceP$Builder;
    .locals 2

    .prologue
    .line 353
    invoke-static {}, Lcom/google/speech/patts/UtteranceP$Builder;->create()Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/UtteranceP$Builder;->mergeFrom(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP$Builder;->clone()Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic internalGetResult()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
    .locals 1

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP$Builder;->internalGetResult()Lcom/google/speech/patts/UtteranceP;

    move-result-object v0

    return-object v0
.end method

.method protected internalGetResult()Lcom/google/speech/patts/UtteranceP;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    invoke-virtual {v0}, Lcom/google/speech/patts/UtteranceP;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 325
    check-cast p1, Lcom/google/speech/patts/UtteranceP;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/UtteranceP$Builder;->mergeFrom(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/patts/UtteranceP$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 414
    invoke-static {}, Lcom/google/speech/patts/UtteranceP;->getDefaultInstance()Lcom/google/speech/patts/UtteranceP;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 467
    :goto_0
    return-object p0

    .line 415
    :cond_0
    invoke-virtual {p1}, Lcom/google/speech/patts/UtteranceP;->hasId()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416
    invoke-virtual {p1}, Lcom/google/speech/patts/UtteranceP;->getId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/UtteranceP$Builder;->setId(J)Lcom/google/speech/patts/UtteranceP$Builder;

    .line 418
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/UtteranceP;->hasInput()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 419
    invoke-virtual {p1}, Lcom/google/speech/patts/UtteranceP;->getInput()Lcom/google/speech/tts/Sentence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/UtteranceP$Builder;->mergeInput(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/patts/UtteranceP$Builder;

    .line 421
    :cond_2
    # getter for: Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/UtteranceP;->access$300(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 422
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$300(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 423
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$302(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;

    .line 425
    :cond_3
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$300(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/UtteranceP;->access$300(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 427
    :cond_4
    # getter for: Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/UtteranceP;->access$400(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 428
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$400(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 429
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$402(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;

    .line 431
    :cond_5
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$400(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/UtteranceP;->access$400(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 433
    :cond_6
    # getter for: Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/UtteranceP;->access$500(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 434
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$500(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 435
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$502(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;

    .line 437
    :cond_7
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$500(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/UtteranceP;->access$500(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 439
    :cond_8
    # getter for: Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/UtteranceP;->access$600(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 440
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$600(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 441
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$602(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;

    .line 443
    :cond_9
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$600(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/UtteranceP;->access$600(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 445
    :cond_a
    # getter for: Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/UtteranceP;->access$700(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 446
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$700(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 447
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$702(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;

    .line 449
    :cond_b
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$700(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/UtteranceP;->access$700(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 451
    :cond_c
    # getter for: Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/UtteranceP;->access$800(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 452
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$800(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 453
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$802(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;

    .line 455
    :cond_d
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$800(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/UtteranceP;->access$800(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 457
    :cond_e
    invoke-virtual {p1}, Lcom/google/speech/patts/UtteranceP;->hasLattice()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 458
    invoke-virtual {p1}, Lcom/google/speech/patts/UtteranceP;->getLattice()Lspeech/patts/Lattice;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/UtteranceP$Builder;->mergeLattice(Lspeech/patts/Lattice;)Lcom/google/speech/patts/UtteranceP$Builder;

    .line 460
    :cond_f
    invoke-virtual {p1}, Lcom/google/speech/patts/UtteranceP;->hasFilename()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 461
    invoke-virtual {p1}, Lcom/google/speech/patts/UtteranceP;->getFilename()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/UtteranceP$Builder;->setFilename(Ljava/lang/String;)Lcom/google/speech/patts/UtteranceP$Builder;

    .line 463
    :cond_10
    invoke-virtual {p1}, Lcom/google/speech/patts/UtteranceP;->hasStats()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 464
    invoke-virtual {p1}, Lcom/google/speech/patts/UtteranceP;->getStats()Lspeech/patts/UtteranceStats;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/UtteranceP$Builder;->mergeStats(Lspeech/patts/UtteranceStats;)Lcom/google/speech/patts/UtteranceP$Builder;

    .line 466
    :cond_11
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/UtteranceP$Builder;->mergeExtensionFields(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;)V

    goto/16 :goto_0
.end method

.method public mergeInput(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/patts/UtteranceP$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Sentence;

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    invoke-virtual {v0}, Lcom/google/speech/patts/UtteranceP;->hasInput()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->input_:Lcom/google/speech/tts/Sentence;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$1200(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/tts/Sentence;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Sentence;->getDefaultInstance()Lcom/google/speech/tts/Sentence;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 602
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->input_:Lcom/google/speech/tts/Sentence;
    invoke-static {v1}, Lcom/google/speech/patts/UtteranceP;->access$1200(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/tts/Sentence;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Sentence;->newBuilder(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Sentence$Builder;->mergeFrom(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Sentence$Builder;->buildPartial()Lcom/google/speech/tts/Sentence;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/UtteranceP;->input_:Lcom/google/speech/tts/Sentence;
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$1202(Lcom/google/speech/patts/UtteranceP;Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence;

    .line 607
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/UtteranceP;->hasInput:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$1102(Lcom/google/speech/patts/UtteranceP;Z)Z

    .line 608
    return-object p0

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # setter for: Lcom/google/speech/patts/UtteranceP;->input_:Lcom/google/speech/tts/Sentence;
    invoke-static {v0, p1}, Lcom/google/speech/patts/UtteranceP;->access$1202(Lcom/google/speech/patts/UtteranceP;Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence;

    goto :goto_0
.end method

.method public mergeLattice(Lspeech/patts/Lattice;)Lcom/google/speech/patts/UtteranceP$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/Lattice;

    .prologue
    .line 943
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    invoke-virtual {v0}, Lcom/google/speech/patts/UtteranceP;->hasLattice()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->lattice_:Lspeech/patts/Lattice;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$1400(Lcom/google/speech/patts/UtteranceP;)Lspeech/patts/Lattice;

    move-result-object v0

    invoke-static {}, Lspeech/patts/Lattice;->getDefaultInstance()Lspeech/patts/Lattice;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 945
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->lattice_:Lspeech/patts/Lattice;
    invoke-static {v1}, Lcom/google/speech/patts/UtteranceP;->access$1400(Lcom/google/speech/patts/UtteranceP;)Lspeech/patts/Lattice;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/Lattice;->newBuilder(Lspeech/patts/Lattice;)Lspeech/patts/Lattice$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/Lattice$Builder;->mergeFrom(Lspeech/patts/Lattice;)Lspeech/patts/Lattice$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/Lattice$Builder;->buildPartial()Lspeech/patts/Lattice;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/UtteranceP;->lattice_:Lspeech/patts/Lattice;
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$1402(Lcom/google/speech/patts/UtteranceP;Lspeech/patts/Lattice;)Lspeech/patts/Lattice;

    .line 950
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/UtteranceP;->hasLattice:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$1302(Lcom/google/speech/patts/UtteranceP;Z)Z

    .line 951
    return-object p0

    .line 948
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # setter for: Lcom/google/speech/patts/UtteranceP;->lattice_:Lspeech/patts/Lattice;
    invoke-static {v0, p1}, Lcom/google/speech/patts/UtteranceP;->access$1402(Lcom/google/speech/patts/UtteranceP;Lspeech/patts/Lattice;)Lspeech/patts/Lattice;

    goto :goto_0
.end method

.method public mergeStats(Lspeech/patts/UtteranceStats;)Lcom/google/speech/patts/UtteranceP$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/UtteranceStats;

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    invoke-virtual {v0}, Lcom/google/speech/patts/UtteranceP;->hasStats()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->stats_:Lspeech/patts/UtteranceStats;
    invoke-static {v0}, Lcom/google/speech/patts/UtteranceP;->access$1800(Lcom/google/speech/patts/UtteranceP;)Lspeech/patts/UtteranceStats;

    move-result-object v0

    invoke-static {}, Lspeech/patts/UtteranceStats;->getDefaultInstance()Lspeech/patts/UtteranceStats;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1003
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    iget-object v1, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # getter for: Lcom/google/speech/patts/UtteranceP;->stats_:Lspeech/patts/UtteranceStats;
    invoke-static {v1}, Lcom/google/speech/patts/UtteranceP;->access$1800(Lcom/google/speech/patts/UtteranceP;)Lspeech/patts/UtteranceStats;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/UtteranceStats;->newBuilder(Lspeech/patts/UtteranceStats;)Lspeech/patts/UtteranceStats$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/UtteranceStats$Builder;->mergeFrom(Lspeech/patts/UtteranceStats;)Lspeech/patts/UtteranceStats$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/UtteranceStats$Builder;->buildPartial()Lspeech/patts/UtteranceStats;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/UtteranceP;->stats_:Lspeech/patts/UtteranceStats;
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$1802(Lcom/google/speech/patts/UtteranceP;Lspeech/patts/UtteranceStats;)Lspeech/patts/UtteranceStats;

    .line 1008
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/UtteranceP;->hasStats:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$1702(Lcom/google/speech/patts/UtteranceP;Z)Z

    .line 1009
    return-object p0

    .line 1006
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # setter for: Lcom/google/speech/patts/UtteranceP;->stats_:Lspeech/patts/UtteranceStats;
    invoke-static {v0, p1}, Lcom/google/speech/patts/UtteranceP;->access$1802(Lcom/google/speech/patts/UtteranceP;Lspeech/patts/UtteranceStats;)Lspeech/patts/UtteranceStats;

    goto :goto_0
.end method

.method public setFilename(Ljava/lang/String;)Lcom/google/speech/patts/UtteranceP$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 967
    if-nez p1, :cond_0

    .line 968
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 970
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/UtteranceP;->hasFilename:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$1502(Lcom/google/speech/patts/UtteranceP;Z)Z

    .line 971
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # setter for: Lcom/google/speech/patts/UtteranceP;->filename_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/UtteranceP;->access$1602(Lcom/google/speech/patts/UtteranceP;Ljava/lang/String;)Ljava/lang/String;

    .line 972
    return-object p0
.end method

.method public setId(J)Lcom/google/speech/patts/UtteranceP$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/UtteranceP;->hasId:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/UtteranceP;->access$902(Lcom/google/speech/patts/UtteranceP;Z)Z

    .line 570
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP$Builder;->result:Lcom/google/speech/patts/UtteranceP;

    # setter for: Lcom/google/speech/patts/UtteranceP;->id_:J
    invoke-static {v0, p1, p2}, Lcom/google/speech/patts/UtteranceP;->access$1002(Lcom/google/speech/patts/UtteranceP;J)J

    .line 571
    return-object p0
.end method
