.class public final Lcom/google/speech/patts/hmm/LinearTransformProto;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LinearTransformProto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/hmm/LinearTransformProto$1;,
        Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;,
        Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/hmm/LinearTransformProto;


# instance fields
.field private biasMemoizedSerializedSize:I

.field private bias_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private blocks_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;",
            ">;"
        }
    .end annotation
.end field

.field private det_:F

.field private hasDet:Z

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 724
    new-instance v0, Lcom/google/speech/patts/hmm/LinearTransformProto;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/hmm/LinearTransformProto;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/hmm/LinearTransformProto;->defaultInstance:Lcom/google/speech/patts/hmm/LinearTransformProto;

    .line 725
    invoke-static {}, Lcom/google/speech/patts/hmm/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 726
    sget-object v0, Lcom/google/speech/patts/hmm/LinearTransformProto;->defaultInstance:Lcom/google/speech/patts/hmm/LinearTransformProto;

    invoke-direct {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->initFields()V

    .line 727
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 325
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;

    .line 337
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;

    .line 346
    iput v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->biasMemoizedSerializedSize:I

    .line 351
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->det_:F

    .line 379
    iput v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/hmm/LinearTransformProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/hmm/LinearTransformProto$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, -0x1

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 325
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;

    .line 337
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;

    .line 346
    iput v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->biasMemoizedSerializedSize:I

    .line 351
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->det_:F

    .line 379
    iput v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/hmm/LinearTransformProto;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/LinearTransformProto;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->det_:F

    return p1
.end method

.method static synthetic access$700(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/LinearTransformProto;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/speech/patts/hmm/LinearTransformProto;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/LinearTransformProto;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/LinearTransformProto;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/speech/patts/hmm/LinearTransformProto;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/LinearTransformProto;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/hmm/LinearTransformProto;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/LinearTransformProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->hasDet:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/hmm/LinearTransformProto;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/hmm/LinearTransformProto;->defaultInstance:Lcom/google/speech/patts/hmm/LinearTransformProto;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 356
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;
    .locals 1

    .prologue
    .line 475
    # invokes: Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->create()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;
    invoke-static {}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->access$500()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/hmm/LinearTransformProto;)Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/hmm/LinearTransformProto;

    .prologue
    .line 478
    invoke-static {}, Lcom/google/speech/patts/hmm/LinearTransformProto;->newBuilder()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/LinearTransformProto;)Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBiasList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;

    return-object v0
.end method

.method public getBlocksList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->getDefaultInstanceForType()Lcom/google/speech/patts/hmm/LinearTransformProto;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/hmm/LinearTransformProto;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/hmm/LinearTransformProto;->defaultInstance:Lcom/google/speech/patts/hmm/LinearTransformProto;

    return-object v0
.end method

.method public getDet()F
    .locals 1

    .prologue
    .line 353
    iget v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->det_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 381
    iget v3, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->memoizedSerializedSize:I

    .line 382
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 405
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 384
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 385
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->getBlocksList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    .line 386
    .local v1, "element":Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
    const/4 v5, 0x1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 388
    goto :goto_1

    .line 390
    .end local v1    # "element":Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
    :cond_1
    const/4 v0, 0x0

    .line 391
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->getBiasList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v0, v5, 0x4

    .line 392
    add-int/2addr v3, v0

    .line 393
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->getBiasList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 394
    add-int/lit8 v3, v3, 0x1

    .line 395
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v3, v5

    .line 398
    :cond_2
    iput v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->biasMemoizedSerializedSize:I

    .line 400
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->hasDet()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 401
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->getDet()F

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v5

    add-int/2addr v3, v5

    .line 404
    :cond_3
    iput v3, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->memoizedSerializedSize:I

    move v4, v3

    .line 405
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto :goto_0
.end method

.method public hasDet()Z
    .locals 1

    .prologue
    .line 352
    iget-boolean v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->hasDet:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 358
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->toBuilder()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;
    .locals 1

    .prologue
    .line 480
    invoke-static {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->newBuilder(Lcom/google/speech/patts/hmm/LinearTransformProto;)Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 363
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->getSerializedSize()I

    .line 364
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->getBlocksList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    .line 365
    .local v0, "element":Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 367
    .end local v0    # "element":Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->getBiasList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 368
    const/16 v2, 0x12

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 369
    iget v2, p0, Lcom/google/speech/patts/hmm/LinearTransformProto;->biasMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 371
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->getBiasList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 372
    .local v0, "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_1

    .line 374
    .end local v0    # "element":F
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->hasDet()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 375
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->getDet()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 377
    :cond_3
    return-void
.end method
