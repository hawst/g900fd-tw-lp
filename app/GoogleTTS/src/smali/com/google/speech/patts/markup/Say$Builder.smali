.class public final Lcom/google/speech/patts/markup/Say$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Say.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Say;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/markup/Say;",
        "Lcom/google/speech/patts/markup/Say$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/markup/Say;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 441
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/markup/Say$Builder;
    .locals 1

    .prologue
    .line 435
    invoke-static {}, Lcom/google/speech/patts/markup/Say$Builder;->create()Lcom/google/speech/patts/markup/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/markup/Say$Builder;
    .locals 3

    .prologue
    .line 444
    new-instance v0, Lcom/google/speech/patts/markup/Say$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Say$Builder;-><init>()V

    .line 445
    .local v0, "builder":Lcom/google/speech/patts/markup/Say$Builder;
    new-instance v1, Lcom/google/speech/patts/markup/Say;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/markup/Say;-><init>(Lcom/google/speech/patts/markup/Say$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    .line 446
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say$Builder;->build()Lcom/google/speech/patts/markup/Say;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/markup/Say;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-static {v0}, Lcom/google/speech/patts/markup/Say$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 477
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say$Builder;->buildPartial()Lcom/google/speech/patts/markup/Say;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/markup/Say;
    .locals 3

    .prologue
    .line 490
    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    if-nez v1, :cond_0

    .line 491
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    .line 495
    .local v0, "returnMe":Lcom/google/speech/patts/markup/Say;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    .line 496
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say$Builder;->clone()Lcom/google/speech/patts/markup/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say$Builder;->clone()Lcom/google/speech/patts/markup/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2

    .prologue
    .line 463
    invoke-static {}, Lcom/google/speech/patts/markup/Say$Builder;->create()Lcom/google/speech/patts/markup/Say$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/markup/Say$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say$Builder;->clone()Lcom/google/speech/patts/markup/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeAlternative(Lcom/google/speech/patts/markup/Alternative;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/Alternative;

    .prologue
    .line 1269
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->hasAlternative()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->alternative_:Lcom/google/speech/patts/markup/Alternative;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Say;->access$4000(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Alternative;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/Alternative;->getDefaultInstance()Lcom/google/speech/patts/markup/Alternative;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1271
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->alternative_:Lcom/google/speech/patts/markup/Alternative;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Say;->access$4000(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Alternative;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/Alternative;->newBuilder(Lcom/google/speech/patts/markup/Alternative;)Lcom/google/speech/patts/markup/Alternative$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/Alternative$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Alternative;)Lcom/google/speech/patts/markup/Alternative$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Alternative$Builder;->buildPartial()Lcom/google/speech/patts/markup/Alternative;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Say;->alternative_:Lcom/google/speech/patts/markup/Alternative;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$4002(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Alternative;)Lcom/google/speech/patts/markup/Alternative;

    .line 1276
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasAlternative:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$3902(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 1277
    return-object p0

    .line 1274
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->alternative_:Lcom/google/speech/patts/markup/Alternative;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$4002(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Alternative;)Lcom/google/speech/patts/markup/Alternative;

    goto :goto_0
.end method

.method public mergeDate(Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/Date;

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->date_:Lcom/google/speech/patts/markup/Date;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Say;->access$2400(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Date;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/Date;->getDefaultInstance()Lcom/google/speech/patts/markup/Date;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1042
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->date_:Lcom/google/speech/patts/markup/Date;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Say;->access$2400(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/Date;->newBuilder(Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/Date$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Date$Builder;->buildPartial()Lcom/google/speech/patts/markup/Date;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Say;->date_:Lcom/google/speech/patts/markup/Date;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$2402(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Date;

    .line 1047
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasDate:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$2302(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 1048
    return-object p0

    .line 1045
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->date_:Lcom/google/speech/patts/markup/Date;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$2402(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Date;

    goto :goto_0
.end method

.method public mergeDecimal(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/Decimal;

    .prologue
    .line 818
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->hasDecimal()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->decimal_:Lcom/google/speech/patts/markup/Decimal;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Say;->access$1200(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Decimal;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/Decimal;->getDefaultInstance()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 820
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->decimal_:Lcom/google/speech/patts/markup/Decimal;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Say;->access$1200(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Decimal;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/Decimal;->newBuilder(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/Decimal$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Decimal$Builder;->buildPartial()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Say;->decimal_:Lcom/google/speech/patts/markup/Decimal;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$1202(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal;

    .line 825
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasDecimal:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$1102(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 826
    return-object p0

    .line 823
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->decimal_:Lcom/google/speech/patts/markup/Decimal;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$1202(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal;

    goto :goto_0
.end method

.method public mergeFraction(Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/Fraction;

    .prologue
    .line 855
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->fraction_:Lcom/google/speech/patts/markup/Fraction;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Say;->access$1400(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Fraction;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/Fraction;->getDefaultInstance()Lcom/google/speech/patts/markup/Fraction;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 857
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->fraction_:Lcom/google/speech/patts/markup/Fraction;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Say;->access$1400(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Fraction;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/Fraction;->newBuilder(Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/Fraction$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Fraction$Builder;->buildPartial()Lcom/google/speech/patts/markup/Fraction;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Say;->fraction_:Lcom/google/speech/patts/markup/Fraction;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$1402(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Fraction;

    .line 862
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasFraction:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$1302(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 863
    return-object p0

    .line 860
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->fraction_:Lcom/google/speech/patts/markup/Fraction;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$1402(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Fraction;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 435
    check-cast p1, Lcom/google/speech/patts/markup/Say;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/markup/Say$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 500
    invoke-static {}, Lcom/google/speech/patts/markup/Say;->getDefaultInstance()Lcom/google/speech/patts/markup/Say;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 561
    :cond_0
    :goto_0
    return-object p0

    .line 501
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasText()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 502
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->setText(Ljava/lang/String;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 504
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasCardinal()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 505
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getCardinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->setCardinal(I)Lcom/google/speech/patts/markup/Say$Builder;

    .line 507
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasOrdinal()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 508
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getOrdinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->setOrdinal(I)Lcom/google/speech/patts/markup/Say$Builder;

    .line 510
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasDigit()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 511
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getDigit()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->setDigit(Ljava/lang/String;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 513
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasDecimal()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 514
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getDecimal()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->mergeDecimal(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 516
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 517
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getFraction()Lcom/google/speech/patts/markup/Fraction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->mergeFraction(Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 519
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 520
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getTime()Lcom/google/speech/patts/markup/Time;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->mergeTime(Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 522
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasMoney()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 523
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getMoney()Lcom/google/speech/patts/markup/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->mergeMoney(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 525
    :cond_9
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasMeasure()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 526
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getMeasure()Lcom/google/speech/patts/markup/Measure;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->mergeMeasure(Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 528
    :cond_a
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasTelephone()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 529
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getTelephone()Lcom/google/speech/patts/markup/Telephone;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->mergeTelephone(Lcom/google/speech/patts/markup/Telephone;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 531
    :cond_b
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 532
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getDate()Lcom/google/speech/patts/markup/Date;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->mergeDate(Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 534
    :cond_c
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasWords()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 535
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getWords()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->setWords(Ljava/lang/String;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 537
    :cond_d
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasWordsequence()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 538
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getWordsequence()Lcom/google/speech/patts/markup/WordSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->mergeWordsequence(Lcom/google/speech/patts/markup/WordSequence;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 540
    :cond_e
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasVoicemod()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 541
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getVoicemod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->mergeVoicemod(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 543
    :cond_f
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasPercent()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 544
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getPercent()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->mergePercent(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 546
    :cond_10
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasPause()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 547
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getPause()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->setPause(F)Lcom/google/speech/patts/markup/Say$Builder;

    .line 549
    :cond_11
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasLetters()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 550
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getLetters()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->setLetters(Ljava/lang/String;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 552
    :cond_12
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasVerbatim()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 553
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getVerbatim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->setVerbatim(Ljava/lang/String;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 555
    :cond_13
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasAlternative()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 556
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getAlternative()Lcom/google/speech/patts/markup/Alternative;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->mergeAlternative(Lcom/google/speech/patts/markup/Alternative;)Lcom/google/speech/patts/markup/Say$Builder;

    .line 558
    :cond_14
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->hasForeignLanguage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Say;->getForeignLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Say$Builder;->setForeignLanguage(Ljava/lang/String;)Lcom/google/speech/patts/markup/Say$Builder;

    goto/16 :goto_0
.end method

.method public mergeMeasure(Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/Measure;

    .prologue
    .line 966
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->hasMeasure()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->measure_:Lcom/google/speech/patts/markup/Measure;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Say;->access$2000(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Measure;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/Measure;->getDefaultInstance()Lcom/google/speech/patts/markup/Measure;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 968
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->measure_:Lcom/google/speech/patts/markup/Measure;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Say;->access$2000(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Measure;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/Measure;->newBuilder(Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/Measure$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Measure$Builder;->buildPartial()Lcom/google/speech/patts/markup/Measure;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Say;->measure_:Lcom/google/speech/patts/markup/Measure;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$2002(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Measure;

    .line 973
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasMeasure:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$1902(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 974
    return-object p0

    .line 971
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->measure_:Lcom/google/speech/patts/markup/Measure;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$2002(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Measure;

    goto :goto_0
.end method

.method public mergeMoney(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/Money;

    .prologue
    .line 929
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->hasMoney()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->money_:Lcom/google/speech/patts/markup/Money;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Say;->access$1800(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Money;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/Money;->getDefaultInstance()Lcom/google/speech/patts/markup/Money;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 931
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->money_:Lcom/google/speech/patts/markup/Money;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Say;->access$1800(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Money;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/Money;->newBuilder(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/Money$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Money$Builder;->buildPartial()Lcom/google/speech/patts/markup/Money;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Say;->money_:Lcom/google/speech/patts/markup/Money;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$1802(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Money;

    .line 936
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasMoney:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$1702(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 937
    return-object p0

    .line 934
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->money_:Lcom/google/speech/patts/markup/Money;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$1802(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Money;

    goto :goto_0
.end method

.method public mergePercent(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/Decimal;

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->hasPercent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->percent_:Lcom/google/speech/patts/markup/Decimal;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Say;->access$3200(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Decimal;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/Decimal;->getDefaultInstance()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1174
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->percent_:Lcom/google/speech/patts/markup/Decimal;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Say;->access$3200(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Decimal;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/Decimal;->newBuilder(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/Decimal$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Decimal$Builder;->buildPartial()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Say;->percent_:Lcom/google/speech/patts/markup/Decimal;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$3202(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal;

    .line 1179
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasPercent:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$3102(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 1180
    return-object p0

    .line 1177
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->percent_:Lcom/google/speech/patts/markup/Decimal;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$3202(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal;

    goto :goto_0
.end method

.method public mergeTelephone(Lcom/google/speech/patts/markup/Telephone;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/Telephone;

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->hasTelephone()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->telephone_:Lcom/google/speech/patts/markup/Telephone;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Say;->access$2200(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Telephone;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/Telephone;->getDefaultInstance()Lcom/google/speech/patts/markup/Telephone;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1005
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->telephone_:Lcom/google/speech/patts/markup/Telephone;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Say;->access$2200(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Telephone;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/Telephone;->newBuilder(Lcom/google/speech/patts/markup/Telephone;)Lcom/google/speech/patts/markup/Telephone$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/Telephone$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Telephone;)Lcom/google/speech/patts/markup/Telephone$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Telephone$Builder;->buildPartial()Lcom/google/speech/patts/markup/Telephone;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Say;->telephone_:Lcom/google/speech/patts/markup/Telephone;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$2202(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Telephone;)Lcom/google/speech/patts/markup/Telephone;

    .line 1010
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasTelephone:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$2102(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 1011
    return-object p0

    .line 1008
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->telephone_:Lcom/google/speech/patts/markup/Telephone;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$2202(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Telephone;)Lcom/google/speech/patts/markup/Telephone;

    goto :goto_0
.end method

.method public mergeTime(Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/Time;

    .prologue
    .line 892
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->time_:Lcom/google/speech/patts/markup/Time;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Say;->access$1600(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Time;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/Time;->getDefaultInstance()Lcom/google/speech/patts/markup/Time;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 894
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->time_:Lcom/google/speech/patts/markup/Time;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Say;->access$1600(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Time;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/Time;->newBuilder(Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/Time$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Time$Builder;->buildPartial()Lcom/google/speech/patts/markup/Time;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Say;->time_:Lcom/google/speech/patts/markup/Time;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$1602(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Time;

    .line 899
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasTime:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$1502(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 900
    return-object p0

    .line 897
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->time_:Lcom/google/speech/patts/markup/Time;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$1602(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Time;

    goto :goto_0
.end method

.method public mergeVoicemod(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/VoiceMod;

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->hasVoicemod()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Say;->access$3000(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/VoiceMod;->getDefaultInstance()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1137
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Say;->access$3000(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/VoiceMod;->newBuilder(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/VoiceMod$Builder;->mergeFrom(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/VoiceMod$Builder;->buildPartial()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$3002(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;

    .line 1142
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasVoicemod:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$2902(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 1143
    return-object p0

    .line 1140
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$3002(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;

    goto :goto_0
.end method

.method public mergeWordsequence(Lcom/google/speech/patts/markup/WordSequence;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/WordSequence;

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->hasWordsequence()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->wordsequence_:Lcom/google/speech/patts/markup/WordSequence;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Say;->access$2800(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/WordSequence;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/WordSequence;->getDefaultInstance()Lcom/google/speech/patts/markup/WordSequence;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1100
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # getter for: Lcom/google/speech/patts/markup/Say;->wordsequence_:Lcom/google/speech/patts/markup/WordSequence;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Say;->access$2800(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/WordSequence;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/WordSequence;->newBuilder(Lcom/google/speech/patts/markup/WordSequence;)Lcom/google/speech/patts/markup/WordSequence$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/WordSequence$Builder;->mergeFrom(Lcom/google/speech/patts/markup/WordSequence;)Lcom/google/speech/patts/markup/WordSequence$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/WordSequence$Builder;->buildPartial()Lcom/google/speech/patts/markup/WordSequence;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Say;->wordsequence_:Lcom/google/speech/patts/markup/WordSequence;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$2802(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/WordSequence;)Lcom/google/speech/patts/markup/WordSequence;

    .line 1105
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasWordsequence:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$2702(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 1106
    return-object p0

    .line 1103
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->wordsequence_:Lcom/google/speech/patts/markup/WordSequence;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$2802(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/WordSequence;)Lcom/google/speech/patts/markup/WordSequence;

    goto :goto_0
.end method

.method public setCardinal(I)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 748
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasCardinal:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$502(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 749
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->cardinal_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$602(Lcom/google/speech/patts/markup/Say;I)I

    .line 750
    return-object p0
.end method

.method public setDigit(Ljava/lang/String;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 784
    if-nez p1, :cond_0

    .line 785
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 787
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasDigit:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$902(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 788
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->digit_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$1002(Lcom/google/speech/patts/markup/Say;Ljava/lang/String;)Ljava/lang/String;

    .line 789
    return-object p0
.end method

.method public setForeignLanguage(Ljava/lang/String;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1293
    if-nez p1, :cond_0

    .line 1294
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1296
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasForeignLanguage:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$4102(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 1297
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->foreignLanguage_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$4202(Lcom/google/speech/patts/markup/Say;Ljava/lang/String;)Ljava/lang/String;

    .line 1298
    return-object p0
.end method

.method public setLetters(Ljava/lang/String;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1214
    if-nez p1, :cond_0

    .line 1215
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1217
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasLetters:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$3502(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 1218
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->letters_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$3602(Lcom/google/speech/patts/markup/Say;Ljava/lang/String;)Ljava/lang/String;

    .line 1219
    return-object p0
.end method

.method public setOrdinal(I)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 766
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasOrdinal:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$702(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 767
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->ordinal_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$802(Lcom/google/speech/patts/markup/Say;I)I

    .line 768
    return-object p0
.end method

.method public setPause(F)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 1196
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasPause:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$3302(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 1197
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->pause_:F
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$3402(Lcom/google/speech/patts/markup/Say;F)F

    .line 1198
    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 727
    if-nez p1, :cond_0

    .line 728
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 730
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasText:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$302(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 731
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->text_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$402(Lcom/google/speech/patts/markup/Say;Ljava/lang/String;)Ljava/lang/String;

    .line 732
    return-object p0
.end method

.method public setVerbatim(Ljava/lang/String;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1235
    if-nez p1, :cond_0

    .line 1236
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1238
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasVerbatim:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$3702(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 1239
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->verbatim_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$3802(Lcom/google/speech/patts/markup/Say;Ljava/lang/String;)Ljava/lang/String;

    .line 1240
    return-object p0
.end method

.method public setWords(Ljava/lang/String;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1064
    if-nez p1, :cond_0

    .line 1065
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1067
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Say;->hasWords:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Say;->access$2502(Lcom/google/speech/patts/markup/Say;Z)Z

    .line 1068
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say$Builder;->result:Lcom/google/speech/patts/markup/Say;

    # setter for: Lcom/google/speech/patts/markup/Say;->words_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Say;->access$2602(Lcom/google/speech/patts/markup/Say;Ljava/lang/String;)Ljava/lang/String;

    .line 1069
    return-object p0
.end method
