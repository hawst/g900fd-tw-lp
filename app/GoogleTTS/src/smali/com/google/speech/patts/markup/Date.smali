.class public final Lcom/google/speech/patts/markup/Date;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Date.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/markup/Date$1;,
        Lcom/google/speech/patts/markup/Date$Builder;,
        Lcom/google/speech/patts/markup/Date$LegalMonth;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/markup/Date;


# instance fields
.field private day_:I

.field private hasDay:Z

.field private hasMonth:Z

.field private hasStyle:Z

.field private hasText:Z

.field private hasYear:Z

.field private memoizedSerializedSize:I

.field private month_:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field private style_:Ljava/lang/String;

.field private text_:Ljava/lang/String;

.field private year_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 478
    new-instance v0, Lcom/google/speech/patts/markup/Date;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/markup/Date;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/markup/Date;->defaultInstance:Lcom/google/speech/patts/markup/Date;

    .line 479
    invoke-static {}, Lcom/google/speech/patts/markup/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 480
    sget-object v0, Lcom/google/speech/patts/markup/Date;->defaultInstance:Lcom/google/speech/patts/markup/Date;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Date;->initFields()V

    .line 481
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 84
    iput v0, p0, Lcom/google/speech/patts/markup/Date;->day_:I

    .line 98
    iput v0, p0, Lcom/google/speech/patts/markup/Date;->year_:I

    .line 105
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Date;->style_:Ljava/lang/String;

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Date;->text_:Ljava/lang/String;

    .line 143
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Date;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Date;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/markup/Date$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/markup/Date$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Date;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 84
    iput v0, p0, Lcom/google/speech/patts/markup/Date;->day_:I

    .line 98
    iput v0, p0, Lcom/google/speech/patts/markup/Date;->year_:I

    .line 105
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Date;->style_:Ljava/lang/String;

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Date;->text_:Ljava/lang/String;

    .line 143
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Date;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/markup/Date;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Date;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Date;->style_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/speech/patts/markup/Date;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Date;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Date;->hasText:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/patts/markup/Date;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Date;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Date;->text_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/speech/patts/markup/Date;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Date;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Date;->hasDay:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/markup/Date;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Date;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/markup/Date;->day_:I

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/markup/Date;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Date;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Date;->hasMonth:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/markup/Date;Lcom/google/speech/patts/markup/Date$LegalMonth;)Lcom/google/speech/patts/markup/Date$LegalMonth;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Date;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/Date$LegalMonth;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Date;->month_:Lcom/google/speech/patts/markup/Date$LegalMonth;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/markup/Date;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Date;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Date;->hasYear:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/markup/Date;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Date;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/markup/Date;->year_:I

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/markup/Date;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Date;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Date;->hasStyle:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/markup/Date;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/markup/Date;->defaultInstance:Lcom/google/speech/patts/markup/Date;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH1:Lcom/google/speech/patts/markup/Date$LegalMonth;

    iput-object v0, p0, Lcom/google/speech/patts/markup/Date;->month_:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 118
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/markup/Date$Builder;
    .locals 1

    .prologue
    .line 240
    # invokes: Lcom/google/speech/patts/markup/Date$Builder;->create()Lcom/google/speech/patts/markup/Date$Builder;
    invoke-static {}, Lcom/google/speech/patts/markup/Date$Builder;->access$100()Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Date$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/markup/Date;

    .prologue
    .line 243
    invoke-static {}, Lcom/google/speech/patts/markup/Date;->newBuilder()Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/markup/Date$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDay()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/google/speech/patts/markup/Date;->day_:I

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->getDefaultInstanceForType()Lcom/google/speech/patts/markup/Date;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/markup/Date;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/markup/Date;->defaultInstance:Lcom/google/speech/patts/markup/Date;

    return-object v0
.end method

.method public getMonth()Lcom/google/speech/patts/markup/Date$LegalMonth;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date;->month_:Lcom/google/speech/patts/markup/Date$LegalMonth;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 145
    iget v0, p0, Lcom/google/speech/patts/markup/Date;->memoizedSerializedSize:I

    .line 146
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 170
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 148
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 149
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->hasDay()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 150
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->getDay()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 153
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->hasMonth()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 154
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->getMonth()Lcom/google/speech/patts/markup/Date$LegalMonth;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/patts/markup/Date$LegalMonth;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 157
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->hasYear()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 158
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->getYear()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 161
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 162
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->getStyle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 165
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->hasText()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 166
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 169
    :cond_5
    iput v0, p0, Lcom/google/speech/patts/markup/Date;->memoizedSerializedSize:I

    move v1, v0

    .line 170
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date;->style_:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date;->text_:Ljava/lang/String;

    return-object v0
.end method

.method public getYear()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/google/speech/patts/markup/Date;->year_:I

    return v0
.end method

.method public hasDay()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Date;->hasDay:Z

    return v0
.end method

.method public hasMonth()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Date;->hasMonth:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Date;->hasStyle:Z

    return v0
.end method

.method public hasText()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Date;->hasText:Z

    return v0
.end method

.method public hasYear()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Date;->hasYear:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->toBuilder()Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/markup/Date$Builder;
    .locals 1

    .prologue
    .line 245
    invoke-static {p0}, Lcom/google/speech/patts/markup/Date;->newBuilder(Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->getSerializedSize()I

    .line 126
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->hasDay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->getDay()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 129
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->hasMonth()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->getMonth()Lcom/google/speech/patts/markup/Date$LegalMonth;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Date$LegalMonth;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 132
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->hasYear()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->getYear()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 135
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 136
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->getStyle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 138
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->hasText()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 139
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 141
    :cond_4
    return-void
.end method
