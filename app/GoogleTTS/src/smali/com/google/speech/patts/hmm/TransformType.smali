.class public final enum Lcom/google/speech/patts/hmm/TransformType;
.super Ljava/lang/Enum;
.source "TransformType.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/patts/hmm/TransformType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/patts/hmm/TransformType;

.field public static final enum TRANSFORM_CMLLR_MEANCOV:Lcom/google/speech/patts/hmm/TransformType;

.field public static final enum TRANSFORM_MAP:Lcom/google/speech/patts/hmm/TransformType;

.field public static final enum TRANSFORM_MLLR_COV:Lcom/google/speech/patts/hmm/TransformType;

.field public static final enum TRANSFORM_MLLR_MEAN:Lcom/google/speech/patts/hmm/TransformType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/patts/hmm/TransformType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lcom/google/speech/patts/hmm/TransformType;

    const-string v1, "TRANSFORM_MLLR_MEAN"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/speech/patts/hmm/TransformType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/hmm/TransformType;->TRANSFORM_MLLR_MEAN:Lcom/google/speech/patts/hmm/TransformType;

    .line 8
    new-instance v0, Lcom/google/speech/patts/hmm/TransformType;

    const-string v1, "TRANSFORM_MLLR_COV"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/speech/patts/hmm/TransformType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/hmm/TransformType;->TRANSFORM_MLLR_COV:Lcom/google/speech/patts/hmm/TransformType;

    .line 9
    new-instance v0, Lcom/google/speech/patts/hmm/TransformType;

    const-string v1, "TRANSFORM_CMLLR_MEANCOV"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/google/speech/patts/hmm/TransformType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/hmm/TransformType;->TRANSFORM_CMLLR_MEANCOV:Lcom/google/speech/patts/hmm/TransformType;

    .line 10
    new-instance v0, Lcom/google/speech/patts/hmm/TransformType;

    const-string v1, "TRANSFORM_MAP"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/speech/patts/hmm/TransformType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/hmm/TransformType;->TRANSFORM_MAP:Lcom/google/speech/patts/hmm/TransformType;

    .line 5
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/speech/patts/hmm/TransformType;

    sget-object v1, Lcom/google/speech/patts/hmm/TransformType;->TRANSFORM_MLLR_MEAN:Lcom/google/speech/patts/hmm/TransformType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/speech/patts/hmm/TransformType;->TRANSFORM_MLLR_COV:Lcom/google/speech/patts/hmm/TransformType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/speech/patts/hmm/TransformType;->TRANSFORM_CMLLR_MEANCOV:Lcom/google/speech/patts/hmm/TransformType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/speech/patts/hmm/TransformType;->TRANSFORM_MAP:Lcom/google/speech/patts/hmm/TransformType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/speech/patts/hmm/TransformType;->$VALUES:[Lcom/google/speech/patts/hmm/TransformType;

    .line 31
    new-instance v0, Lcom/google/speech/patts/hmm/TransformType$1;

    invoke-direct {v0}, Lcom/google/speech/patts/hmm/TransformType$1;-><init>()V

    sput-object v0, Lcom/google/speech/patts/hmm/TransformType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/google/speech/patts/hmm/TransformType;->index:I

    .line 42
    iput p4, p0, Lcom/google/speech/patts/hmm/TransformType;->value:I

    .line 43
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/patts/hmm/TransformType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/google/speech/patts/hmm/TransformType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/hmm/TransformType;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/patts/hmm/TransformType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/google/speech/patts/hmm/TransformType;->$VALUES:[Lcom/google/speech/patts/hmm/TransformType;

    invoke-virtual {v0}, [Lcom/google/speech/patts/hmm/TransformType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/patts/hmm/TransformType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/google/speech/patts/hmm/TransformType;->value:I

    return v0
.end method
