.class public final Lcom/google/speech/patts/markup/Telephone$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Telephone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Telephone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/markup/Telephone;",
        "Lcom/google/speech/patts/markup/Telephone$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/markup/Telephone;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/markup/Telephone$Builder;
    .locals 1

    .prologue
    .line 167
    invoke-static {}, Lcom/google/speech/patts/markup/Telephone$Builder;->create()Lcom/google/speech/patts/markup/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/markup/Telephone$Builder;
    .locals 3

    .prologue
    .line 176
    new-instance v0, Lcom/google/speech/patts/markup/Telephone$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Telephone$Builder;-><init>()V

    .line 177
    .local v0, "builder":Lcom/google/speech/patts/markup/Telephone$Builder;
    new-instance v1, Lcom/google/speech/patts/markup/Telephone;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/markup/Telephone;-><init>(Lcom/google/speech/patts/markup/Telephone$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    .line 178
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Telephone$Builder;->build()Lcom/google/speech/patts/markup/Telephone;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/markup/Telephone;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Telephone$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    invoke-static {v0}, Lcom/google/speech/patts/markup/Telephone$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Telephone$Builder;->buildPartial()Lcom/google/speech/patts/markup/Telephone;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/markup/Telephone;
    .locals 3

    .prologue
    .line 222
    iget-object v1, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    if-nez v1, :cond_0

    .line 223
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 226
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    # getter for: Lcom/google/speech/patts/markup/Telephone;->part_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Telephone;->access$300(Lcom/google/speech/patts/markup/Telephone;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 227
    iget-object v1, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    iget-object v2, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    # getter for: Lcom/google/speech/patts/markup/Telephone;->part_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/markup/Telephone;->access$300(Lcom/google/speech/patts/markup/Telephone;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/markup/Telephone;->part_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/markup/Telephone;->access$302(Lcom/google/speech/patts/markup/Telephone;Ljava/util/List;)Ljava/util/List;

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    .line 231
    .local v0, "returnMe":Lcom/google/speech/patts/markup/Telephone;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    .line 232
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Telephone$Builder;->clone()Lcom/google/speech/patts/markup/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Telephone$Builder;->clone()Lcom/google/speech/patts/markup/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/markup/Telephone$Builder;
    .locals 2

    .prologue
    .line 195
    invoke-static {}, Lcom/google/speech/patts/markup/Telephone$Builder;->create()Lcom/google/speech/patts/markup/Telephone$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/markup/Telephone$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Telephone;)Lcom/google/speech/patts/markup/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Telephone$Builder;->clone()Lcom/google/speech/patts/markup/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Telephone;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 167
    check-cast p1, Lcom/google/speech/patts/markup/Telephone;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/markup/Telephone$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Telephone;)Lcom/google/speech/patts/markup/Telephone$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/markup/Telephone;)Lcom/google/speech/patts/markup/Telephone$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/markup/Telephone;

    .prologue
    .line 236
    invoke-static {}, Lcom/google/speech/patts/markup/Telephone;->getDefaultInstance()Lcom/google/speech/patts/markup/Telephone;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 249
    :cond_0
    :goto_0
    return-object p0

    .line 237
    :cond_1
    # getter for: Lcom/google/speech/patts/markup/Telephone;->part_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/markup/Telephone;->access$300(Lcom/google/speech/patts/markup/Telephone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 238
    iget-object v0, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    # getter for: Lcom/google/speech/patts/markup/Telephone;->part_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Telephone;->access$300(Lcom/google/speech/patts/markup/Telephone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 239
    iget-object v0, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/markup/Telephone;->part_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Telephone;->access$302(Lcom/google/speech/patts/markup/Telephone;Ljava/util/List;)Ljava/util/List;

    .line 241
    :cond_2
    iget-object v0, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    # getter for: Lcom/google/speech/patts/markup/Telephone;->part_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Telephone;->access$300(Lcom/google/speech/patts/markup/Telephone;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/markup/Telephone;->part_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/markup/Telephone;->access$300(Lcom/google/speech/patts/markup/Telephone;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 243
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Telephone;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 244
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Telephone;->getStyle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Telephone$Builder;->setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Telephone$Builder;

    .line 246
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Telephone;->hasCountryCode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Telephone;->getCountryCode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Telephone$Builder;->setCountryCode(I)Lcom/google/speech/patts/markup/Telephone$Builder;

    goto :goto_0
.end method

.method public setCountryCode(I)Lcom/google/speech/patts/markup/Telephone$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Telephone;->hasCountryCode:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Telephone;->access$602(Lcom/google/speech/patts/markup/Telephone;Z)Z

    .line 367
    iget-object v0, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    # setter for: Lcom/google/speech/patts/markup/Telephone;->countryCode_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Telephone;->access$702(Lcom/google/speech/patts/markup/Telephone;I)I

    .line 368
    return-object p0
.end method

.method public setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Telephone$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 345
    if-nez p1, :cond_0

    .line 346
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Telephone;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Telephone;->access$402(Lcom/google/speech/patts/markup/Telephone;Z)Z

    .line 349
    iget-object v0, p0, Lcom/google/speech/patts/markup/Telephone$Builder;->result:Lcom/google/speech/patts/markup/Telephone;

    # setter for: Lcom/google/speech/patts/markup/Telephone;->style_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Telephone;->access$502(Lcom/google/speech/patts/markup/Telephone;Ljava/lang/String;)Ljava/lang/String;

    .line 350
    return-object p0
.end method
