.class public final Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SpeechMorphingTransform.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/hmm/SpeechMorphingTransform$1;,
        Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;


# instance fields
.field private frequencyWarpWeight_:Lspeech/patts/FrequencyWarpAndWeightTransforms;

.field private hasFrequencyWarpWeight:Z

.field private hasHmmXforms:Z

.field private hasSpeakerId:Z

.field private hmmXforms_:Lcom/google/speech/patts/hmm/HmmTransform;

.field private memoizedSerializedSize:I

.field private speakerId_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 383
    new-instance v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->defaultInstance:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .line 384
    invoke-static {}, Lcom/google/speech/patts/hmm/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 385
    sget-object v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->defaultInstance:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    invoke-direct {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->initFields()V

    .line 386
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->speakerId_:Ljava/lang/String;

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/hmm/SpeechMorphingTransform$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->speakerId_:Ljava/lang/String;

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasHmmXforms:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/HmmTransform;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hmmXforms_:Lcom/google/speech/patts/hmm/HmmTransform;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/HmmTransform;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .param p1, "x1"    # Lcom/google/speech/patts/hmm/HmmTransform;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hmmXforms_:Lcom/google/speech/patts/hmm/HmmTransform;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasFrequencyWarpWeight:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lspeech/patts/FrequencyWarpAndWeightTransforms;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->frequencyWarpWeight_:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransforms;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .param p1, "x1"    # Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->frequencyWarpWeight_:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasSpeakerId:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->speakerId_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->defaultInstance:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/google/speech/patts/hmm/HmmTransform;->getDefaultInstance()Lcom/google/speech/patts/hmm/HmmTransform;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hmmXforms_:Lcom/google/speech/patts/hmm/HmmTransform;

    .line 45
    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getDefaultInstance()Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->frequencyWarpWeight_:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .line 46
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    .locals 1

    .prologue
    .line 157
    # invokes: Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->create()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    invoke-static {}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->access$100()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .prologue
    .line 160
    invoke-static {}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->newBuilder()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getDefaultInstanceForType()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->defaultInstance:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    return-object v0
.end method

.method public getFrequencyWarpWeight()Lspeech/patts/FrequencyWarpAndWeightTransforms;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->frequencyWarpWeight_:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    return-object v0
.end method

.method public getHmmXforms()Lcom/google/speech/patts/hmm/HmmTransform;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hmmXforms_:Lcom/google/speech/patts/hmm/HmmTransform;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 70
    iget v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->memoizedSerializedSize:I

    .line 71
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 87
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 73
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 74
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasHmmXforms()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 75
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getHmmXforms()Lcom/google/speech/patts/hmm/HmmTransform;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasFrequencyWarpWeight()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 79
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getFrequencyWarpWeight()Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 82
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasSpeakerId()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 83
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getSpeakerId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 86
    :cond_3
    iput v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->memoizedSerializedSize:I

    move v1, v0

    .line 87
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getSpeakerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->speakerId_:Ljava/lang/String;

    return-object v0
.end method

.method public hasFrequencyWarpWeight()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasFrequencyWarpWeight:Z

    return v0
.end method

.method public hasHmmXforms()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasHmmXforms:Z

    return v0
.end method

.method public hasSpeakerId()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasSpeakerId:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasFrequencyWarpWeight()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getFrequencyWarpWeight()Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v0

    invoke-virtual {v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 51
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->toBuilder()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    .locals 1

    .prologue
    .line 162
    invoke-static {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->newBuilder(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getSerializedSize()I

    .line 57
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasHmmXforms()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getHmmXforms()Lcom/google/speech/patts/hmm/HmmTransform;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasFrequencyWarpWeight()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getFrequencyWarpWeight()Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 63
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasSpeakerId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 64
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getSpeakerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 66
    :cond_2
    return-void
.end method
