.class public Lcom/google/speech/patts/api/android/StaticJniLoader;
.super Ljava/lang/Object;
.source "StaticJniLoader.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static loadJni()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 28
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/google/speech/patts/engine/api/ApiBase;->jniLibraryName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_ub"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .local v1, "ule":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return v3

    .line 30
    .end local v1    # "ule":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v1

    .line 33
    .restart local v1    # "ule":Ljava/lang/UnsatisfiedLinkError;
    :try_start_1
    sget-object v4, Lcom/google/speech/patts/engine/api/ApiBase;->jniLibraryName:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 35
    :catch_1
    move-exception v2

    .line 36
    .local v2, "ule2":Ljava/lang/UnsatisfiedLinkError;
    const-string v3, "patts"

    const-string v4, "Shared object load error: "

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 43
    .end local v2    # "ule2":Ljava/lang/UnsatisfiedLinkError;
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 38
    :catch_2
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "patts"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to load JNI library: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
