.class public final Lcom/google/speech/patts/markup/Decimal$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Decimal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Decimal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/markup/Decimal;",
        "Lcom/google/speech/patts/markup/Decimal$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/markup/Decimal;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/markup/Decimal$Builder;
    .locals 1

    .prologue
    .line 174
    invoke-static {}, Lcom/google/speech/patts/markup/Decimal$Builder;->create()Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/markup/Decimal$Builder;
    .locals 3

    .prologue
    .line 183
    new-instance v0, Lcom/google/speech/patts/markup/Decimal$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Decimal$Builder;-><init>()V

    .line 184
    .local v0, "builder":Lcom/google/speech/patts/markup/Decimal$Builder;
    new-instance v1, Lcom/google/speech/patts/markup/Decimal;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/markup/Decimal;-><init>(Lcom/google/speech/patts/markup/Decimal$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    .line 185
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal$Builder;->build()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/markup/Decimal;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    invoke-static {v0}, Lcom/google/speech/patts/markup/Decimal$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 216
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal$Builder;->buildPartial()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/markup/Decimal;
    .locals 3

    .prologue
    .line 229
    iget-object v1, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    if-nez v1, :cond_0

    .line 230
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    .line 234
    .local v0, "returnMe":Lcom/google/speech/patts/markup/Decimal;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    .line 235
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal$Builder;->clone()Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal$Builder;->clone()Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/markup/Decimal$Builder;
    .locals 2

    .prologue
    .line 202
    invoke-static {}, Lcom/google/speech/patts/markup/Decimal$Builder;->create()Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/markup/Decimal$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal$Builder;->clone()Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Decimal;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 174
    check-cast p1, Lcom/google/speech/patts/markup/Decimal;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/markup/Decimal$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/markup/Decimal;

    .prologue
    .line 239
    invoke-static {}, Lcom/google/speech/patts/markup/Decimal;->getDefaultInstance()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 252
    :cond_0
    :goto_0
    return-object p0

    .line 240
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Decimal;->hasNumber()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 241
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Decimal;->getNumber()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/markup/Decimal$Builder;->setNumber(J)Lcom/google/speech/patts/markup/Decimal$Builder;

    .line 243
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Decimal;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 244
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Decimal;->getFraction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Decimal$Builder;->setFraction(Ljava/lang/String;)Lcom/google/speech/patts/markup/Decimal$Builder;

    .line 246
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Decimal;->hasNegative()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 247
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Decimal;->getNegative()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Decimal$Builder;->setNegative(Z)Lcom/google/speech/patts/markup/Decimal$Builder;

    .line 249
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Decimal;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Decimal;->getStyle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Decimal$Builder;->setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Decimal$Builder;

    goto :goto_0
.end method

.method public setFraction(Ljava/lang/String;)Lcom/google/speech/patts/markup/Decimal$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 317
    if-nez p1, :cond_0

    .line 318
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Decimal;->hasFraction:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Decimal;->access$502(Lcom/google/speech/patts/markup/Decimal;Z)Z

    .line 321
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    # setter for: Lcom/google/speech/patts/markup/Decimal;->fraction_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Decimal;->access$602(Lcom/google/speech/patts/markup/Decimal;Ljava/lang/String;)Ljava/lang/String;

    .line 322
    return-object p0
.end method

.method public setNegative(Z)Lcom/google/speech/patts/markup/Decimal$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Decimal;->hasNegative:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Decimal;->access$702(Lcom/google/speech/patts/markup/Decimal;Z)Z

    .line 339
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    # setter for: Lcom/google/speech/patts/markup/Decimal;->negative_:Z
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Decimal;->access$802(Lcom/google/speech/patts/markup/Decimal;Z)Z

    .line 340
    return-object p0
.end method

.method public setNumber(J)Lcom/google/speech/patts/markup/Decimal$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Decimal;->hasNumber:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Decimal;->access$302(Lcom/google/speech/patts/markup/Decimal;Z)Z

    .line 300
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    # setter for: Lcom/google/speech/patts/markup/Decimal;->number_:J
    invoke-static {v0, p1, p2}, Lcom/google/speech/patts/markup/Decimal;->access$402(Lcom/google/speech/patts/markup/Decimal;J)J

    .line 301
    return-object p0
.end method

.method public setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Decimal$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 356
    if-nez p1, :cond_0

    .line 357
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Decimal;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Decimal;->access$902(Lcom/google/speech/patts/markup/Decimal;Z)Z

    .line 360
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal$Builder;->result:Lcom/google/speech/patts/markup/Decimal;

    # setter for: Lcom/google/speech/patts/markup/Decimal;->style_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Decimal;->access$1002(Lcom/google/speech/patts/markup/Decimal;Ljava/lang/String;)Ljava/lang/String;

    .line 361
    return-object p0
.end method
