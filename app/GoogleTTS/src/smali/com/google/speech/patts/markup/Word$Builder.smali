.class public final Lcom/google/speech/patts/markup/Word$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Word.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Word;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/markup/Word;",
        "Lcom/google/speech/patts/markup/Word$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/markup/Word;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/markup/Word$Builder;
    .locals 1

    .prologue
    .line 146
    invoke-static {}, Lcom/google/speech/patts/markup/Word$Builder;->create()Lcom/google/speech/patts/markup/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/markup/Word$Builder;
    .locals 3

    .prologue
    .line 155
    new-instance v0, Lcom/google/speech/patts/markup/Word$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Word$Builder;-><init>()V

    .line 156
    .local v0, "builder":Lcom/google/speech/patts/markup/Word$Builder;
    new-instance v1, Lcom/google/speech/patts/markup/Word;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/markup/Word;-><init>(Lcom/google/speech/patts/markup/Word$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/markup/Word$Builder;->result:Lcom/google/speech/patts/markup/Word;

    .line 157
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Word$Builder;->build()Lcom/google/speech/patts/markup/Word;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/markup/Word;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/speech/patts/markup/Word$Builder;->result:Lcom/google/speech/patts/markup/Word;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Word$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/speech/patts/markup/Word$Builder;->result:Lcom/google/speech/patts/markup/Word;

    invoke-static {v0}, Lcom/google/speech/patts/markup/Word$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Word$Builder;->buildPartial()Lcom/google/speech/patts/markup/Word;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/markup/Word;
    .locals 3

    .prologue
    .line 201
    iget-object v1, p0, Lcom/google/speech/patts/markup/Word$Builder;->result:Lcom/google/speech/patts/markup/Word;

    if-nez v1, :cond_0

    .line 202
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Word$Builder;->result:Lcom/google/speech/patts/markup/Word;

    .line 206
    .local v0, "returnMe":Lcom/google/speech/patts/markup/Word;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/markup/Word$Builder;->result:Lcom/google/speech/patts/markup/Word;

    .line 207
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Word$Builder;->clone()Lcom/google/speech/patts/markup/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Word$Builder;->clone()Lcom/google/speech/patts/markup/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/markup/Word$Builder;
    .locals 2

    .prologue
    .line 174
    invoke-static {}, Lcom/google/speech/patts/markup/Word$Builder;->create()Lcom/google/speech/patts/markup/Word$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/markup/Word$Builder;->result:Lcom/google/speech/patts/markup/Word;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/markup/Word$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Word;)Lcom/google/speech/patts/markup/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Word$Builder;->clone()Lcom/google/speech/patts/markup/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/speech/patts/markup/Word$Builder;->result:Lcom/google/speech/patts/markup/Word;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Word;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 146
    check-cast p1, Lcom/google/speech/patts/markup/Word;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/markup/Word$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Word;)Lcom/google/speech/patts/markup/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/markup/Word;)Lcom/google/speech/patts/markup/Word$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/patts/markup/Word;

    .prologue
    .line 211
    invoke-static {}, Lcom/google/speech/patts/markup/Word;->getDefaultInstance()Lcom/google/speech/patts/markup/Word;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-object p0

    .line 212
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Word;->hasId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Word;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Word$Builder;->setId(Ljava/lang/String;)Lcom/google/speech/patts/markup/Word$Builder;

    .line 215
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Word;->hasVariant()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Word;->getVariant()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Word$Builder;->setVariant(Ljava/lang/String;)Lcom/google/speech/patts/markup/Word$Builder;

    goto :goto_0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/speech/patts/markup/Word$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 257
    if-nez p1, :cond_0

    .line 258
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Word$Builder;->result:Lcom/google/speech/patts/markup/Word;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Word;->hasId:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Word;->access$302(Lcom/google/speech/patts/markup/Word;Z)Z

    .line 261
    iget-object v0, p0, Lcom/google/speech/patts/markup/Word$Builder;->result:Lcom/google/speech/patts/markup/Word;

    # setter for: Lcom/google/speech/patts/markup/Word;->id_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Word;->access$402(Lcom/google/speech/patts/markup/Word;Ljava/lang/String;)Ljava/lang/String;

    .line 262
    return-object p0
.end method

.method public setVariant(Ljava/lang/String;)Lcom/google/speech/patts/markup/Word$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 278
    if-nez p1, :cond_0

    .line 279
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Word$Builder;->result:Lcom/google/speech/patts/markup/Word;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Word;->hasVariant:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Word;->access$502(Lcom/google/speech/patts/markup/Word;Z)Z

    .line 282
    iget-object v0, p0, Lcom/google/speech/patts/markup/Word$Builder;->result:Lcom/google/speech/patts/markup/Word;

    # setter for: Lcom/google/speech/patts/markup/Word;->variant_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Word;->access$602(Lcom/google/speech/patts/markup/Word;Ljava/lang/String;)Ljava/lang/String;

    .line 283
    return-object p0
.end method
