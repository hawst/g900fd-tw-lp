.class public final Lcom/google/speech/patts/ling_utt/Val$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Val.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/ling_utt/Val;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/ling_utt/Val;",
        "Lcom/google/speech/patts/ling_utt/Val$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/ling_utt/Val;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 307
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/ling_utt/Val$Builder;
    .locals 1

    .prologue
    .line 301
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Val$Builder;->create()Lcom/google/speech/patts/ling_utt/Val$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/ling_utt/Val$Builder;
    .locals 3

    .prologue
    .line 310
    new-instance v0, Lcom/google/speech/patts/ling_utt/Val$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/ling_utt/Val$Builder;-><init>()V

    .line 311
    .local v0, "builder":Lcom/google/speech/patts/ling_utt/Val$Builder;
    new-instance v1, Lcom/google/speech/patts/ling_utt/Val;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/ling_utt/Val;-><init>(Lcom/google/speech/patts/ling_utt/Val$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    .line 312
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->build()Lcom/google/speech/patts/ling_utt/Val;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/ling_utt/Val;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 343
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->buildPartial()Lcom/google/speech/patts/ling_utt/Val;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/ling_utt/Val;
    .locals 3

    .prologue
    .line 356
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    if-nez v1, :cond_0

    .line 357
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 360
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/ling_utt/Val;->access$300(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 361
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    iget-object v2, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/ling_utt/Val;->access$300(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/ling_utt/Val;->access$302(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;

    .line 364
    :cond_1
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/ling_utt/Val;->access$400(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 365
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    iget-object v2, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/ling_utt/Val;->access$400(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/ling_utt/Val;->access$402(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;

    .line 368
    :cond_2
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/ling_utt/Val;->access$500(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 369
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    iget-object v2, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/ling_utt/Val;->access$500(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/ling_utt/Val;->access$502(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;

    .line 372
    :cond_3
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/ling_utt/Val;->access$600(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 373
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    iget-object v2, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/ling_utt/Val;->access$600(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/ling_utt/Val;->access$602(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;

    .line 376
    :cond_4
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/ling_utt/Val;->access$700(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_5

    .line 377
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    iget-object v2, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/ling_utt/Val;->access$700(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/ling_utt/Val;->access$702(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;

    .line 380
    :cond_5
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    .line 381
    .local v0, "returnMe":Lcom/google/speech/patts/ling_utt/Val;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    .line 382
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->clone()Lcom/google/speech/patts/ling_utt/Val$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->clone()Lcom/google/speech/patts/ling_utt/Val$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/ling_utt/Val$Builder;
    .locals 2

    .prologue
    .line 329
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Val$Builder;->create()Lcom/google/speech/patts/ling_utt/Val$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/ling_utt/Val$Builder;->mergeFrom(Lcom/google/speech/patts/ling_utt/Val;)Lcom/google/speech/patts/ling_utt/Val$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->clone()Lcom/google/speech/patts/ling_utt/Val$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    invoke-virtual {v0}, Lcom/google/speech/patts/ling_utt/Val;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 301
    check-cast p1, Lcom/google/speech/patts/ling_utt/Val;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/ling_utt/Val$Builder;->mergeFrom(Lcom/google/speech/patts/ling_utt/Val;)Lcom/google/speech/patts/ling_utt/Val$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/ling_utt/Val;)Lcom/google/speech/patts/ling_utt/Val$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/ling_utt/Val;

    .prologue
    .line 386
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Val;->getDefaultInstance()Lcom/google/speech/patts/ling_utt/Val;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 432
    :cond_0
    :goto_0
    return-object p0

    .line 387
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Val;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 388
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Val;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->setName(Ljava/lang/String;)Lcom/google/speech/patts/ling_utt/Val$Builder;

    .line 390
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Val;->hasSval()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 391
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Val;->getSval()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->setSval(Ljava/lang/String;)Lcom/google/speech/patts/ling_utt/Val$Builder;

    .line 393
    :cond_3
    # getter for: Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Val;->access$300(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 394
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Val;->access$300(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 395
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Val;->access$302(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;

    .line 397
    :cond_4
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Val;->access$300(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->svval_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Val;->access$300(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 399
    :cond_5
    # getter for: Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Val;->access$400(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 400
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Val;->access$400(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 401
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Val;->access$402(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;

    .line 403
    :cond_6
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Val;->access$400(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->fvval_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Val;->access$400(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 405
    :cond_7
    # getter for: Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Val;->access$500(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 406
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Val;->access$500(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 407
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Val;->access$502(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;

    .line 409
    :cond_8
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Val;->access$500(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->ivval_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Val;->access$500(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 411
    :cond_9
    # getter for: Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Val;->access$600(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 412
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Val;->access$600(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 413
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Val;->access$602(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;

    .line 415
    :cond_a
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Val;->access$600(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->vvval_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Val;->access$600(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 417
    :cond_b
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Val;->hasIval()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 418
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Val;->getIval()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->setIval(I)Lcom/google/speech/patts/ling_utt/Val$Builder;

    .line 420
    :cond_c
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Val;->hasBval()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 421
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Val;->getBval()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->setBval(Z)Lcom/google/speech/patts/ling_utt/Val$Builder;

    .line 423
    :cond_d
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Val;->hasFval()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 424
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Val;->getFval()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/ling_utt/Val$Builder;->setFval(F)Lcom/google/speech/patts/ling_utt/Val$Builder;

    .line 426
    :cond_e
    # getter for: Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Val;->access$700(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 427
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Val;->access$700(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 428
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Val;->access$702(Lcom/google/speech/patts/ling_utt/Val;Ljava/util/List;)Ljava/util/List;

    .line 430
    :cond_f
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Val;->access$700(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/ling_utt/Val;->tmval_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Val;->access$700(Lcom/google/speech/patts/ling_utt/Val;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setBval(Z)Lcom/google/speech/patts/ling_utt/Val$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 744
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->hasBval:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Val;->access$1402(Lcom/google/speech/patts/ling_utt/Val;Z)Z

    .line 745
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->bval_:Z
    invoke-static {v0, p1}, Lcom/google/speech/patts/ling_utt/Val;->access$1502(Lcom/google/speech/patts/ling_utt/Val;Z)Z

    .line 746
    return-object p0
.end method

.method public setFval(F)Lcom/google/speech/patts/ling_utt/Val$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 762
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->hasFval:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Val;->access$1602(Lcom/google/speech/patts/ling_utt/Val;Z)Z

    .line 763
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->fval_:F
    invoke-static {v0, p1}, Lcom/google/speech/patts/ling_utt/Val;->access$1702(Lcom/google/speech/patts/ling_utt/Val;F)F

    .line 764
    return-object p0
.end method

.method public setIval(I)Lcom/google/speech/patts/ling_utt/Val$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 726
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->hasIval:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Val;->access$1202(Lcom/google/speech/patts/ling_utt/Val;Z)Z

    .line 727
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->ival_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/ling_utt/Val;->access$1302(Lcom/google/speech/patts/ling_utt/Val;I)I

    .line 728
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/patts/ling_utt/Val$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 525
    if-nez p1, :cond_0

    .line 526
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Val;->access$802(Lcom/google/speech/patts/ling_utt/Val;Z)Z

    .line 529
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/ling_utt/Val;->access$902(Lcom/google/speech/patts/ling_utt/Val;Ljava/lang/String;)Ljava/lang/String;

    .line 530
    return-object p0
.end method

.method public setSval(Ljava/lang/String;)Lcom/google/speech/patts/ling_utt/Val$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 546
    if-nez p1, :cond_0

    .line 547
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 549
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->hasSval:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Val;->access$1002(Lcom/google/speech/patts/ling_utt/Val;Z)Z

    .line 550
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Val$Builder;->result:Lcom/google/speech/patts/ling_utt/Val;

    # setter for: Lcom/google/speech/patts/ling_utt/Val;->sval_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/ling_utt/Val;->access$1102(Lcom/google/speech/patts/ling_utt/Val;Ljava/lang/String;)Ljava/lang/String;

    .line 551
    return-object p0
.end method
