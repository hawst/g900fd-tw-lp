.class public final Lcom/google/speech/patts/markup/Email$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Email.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Email;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/markup/Email;",
        "Lcom/google/speech/patts/markup/Email$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/markup/Email;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/markup/Email$Builder;
    .locals 1

    .prologue
    .line 145
    invoke-static {}, Lcom/google/speech/patts/markup/Email$Builder;->create()Lcom/google/speech/patts/markup/Email$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/markup/Email$Builder;
    .locals 3

    .prologue
    .line 154
    new-instance v0, Lcom/google/speech/patts/markup/Email$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Email$Builder;-><init>()V

    .line 155
    .local v0, "builder":Lcom/google/speech/patts/markup/Email$Builder;
    new-instance v1, Lcom/google/speech/patts/markup/Email;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/markup/Email;-><init>(Lcom/google/speech/patts/markup/Email$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/markup/Email$Builder;->result:Lcom/google/speech/patts/markup/Email;

    .line 156
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Email$Builder;->build()Lcom/google/speech/patts/markup/Email;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/markup/Email;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/speech/patts/markup/Email$Builder;->result:Lcom/google/speech/patts/markup/Email;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Email$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/speech/patts/markup/Email$Builder;->result:Lcom/google/speech/patts/markup/Email;

    invoke-static {v0}, Lcom/google/speech/patts/markup/Email$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 187
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Email$Builder;->buildPartial()Lcom/google/speech/patts/markup/Email;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/markup/Email;
    .locals 3

    .prologue
    .line 200
    iget-object v1, p0, Lcom/google/speech/patts/markup/Email$Builder;->result:Lcom/google/speech/patts/markup/Email;

    if-nez v1, :cond_0

    .line 201
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Email$Builder;->result:Lcom/google/speech/patts/markup/Email;

    .line 205
    .local v0, "returnMe":Lcom/google/speech/patts/markup/Email;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/markup/Email$Builder;->result:Lcom/google/speech/patts/markup/Email;

    .line 206
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Email$Builder;->clone()Lcom/google/speech/patts/markup/Email$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Email$Builder;->clone()Lcom/google/speech/patts/markup/Email$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/markup/Email$Builder;
    .locals 2

    .prologue
    .line 173
    invoke-static {}, Lcom/google/speech/patts/markup/Email$Builder;->create()Lcom/google/speech/patts/markup/Email$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/markup/Email$Builder;->result:Lcom/google/speech/patts/markup/Email;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/markup/Email$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Email;)Lcom/google/speech/patts/markup/Email$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Email$Builder;->clone()Lcom/google/speech/patts/markup/Email$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/speech/patts/markup/Email$Builder;->result:Lcom/google/speech/patts/markup/Email;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Email;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 145
    check-cast p1, Lcom/google/speech/patts/markup/Email;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/markup/Email$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Email;)Lcom/google/speech/patts/markup/Email$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/markup/Email;)Lcom/google/speech/patts/markup/Email$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/patts/markup/Email;

    .prologue
    .line 210
    invoke-static {}, Lcom/google/speech/patts/markup/Email;->getDefaultInstance()Lcom/google/speech/patts/markup/Email;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-object p0

    .line 211
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Email;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 212
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Email;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Email$Builder;->setName(Ljava/lang/String;)Lcom/google/speech/patts/markup/Email$Builder;

    .line 214
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Email;->hasDomain()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Email;->getDomain()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Email$Builder;->setDomain(Ljava/lang/String;)Lcom/google/speech/patts/markup/Email$Builder;

    goto :goto_0
.end method

.method public setDomain(Ljava/lang/String;)Lcom/google/speech/patts/markup/Email$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 277
    if-nez p1, :cond_0

    .line 278
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Email$Builder;->result:Lcom/google/speech/patts/markup/Email;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Email;->hasDomain:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Email;->access$502(Lcom/google/speech/patts/markup/Email;Z)Z

    .line 281
    iget-object v0, p0, Lcom/google/speech/patts/markup/Email$Builder;->result:Lcom/google/speech/patts/markup/Email;

    # setter for: Lcom/google/speech/patts/markup/Email;->domain_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Email;->access$602(Lcom/google/speech/patts/markup/Email;Ljava/lang/String;)Ljava/lang/String;

    .line 282
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/patts/markup/Email$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 256
    if-nez p1, :cond_0

    .line 257
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Email$Builder;->result:Lcom/google/speech/patts/markup/Email;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Email;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Email;->access$302(Lcom/google/speech/patts/markup/Email;Z)Z

    .line 260
    iget-object v0, p0, Lcom/google/speech/patts/markup/Email$Builder;->result:Lcom/google/speech/patts/markup/Email;

    # setter for: Lcom/google/speech/patts/markup/Email;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Email;->access$402(Lcom/google/speech/patts/markup/Email;Ljava/lang/String;)Ljava/lang/String;

    .line 261
    return-object p0
.end method
