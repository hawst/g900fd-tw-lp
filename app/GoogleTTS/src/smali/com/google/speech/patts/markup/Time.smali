.class public final Lcom/google/speech/patts/markup/Time;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Time.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/markup/Time$1;,
        Lcom/google/speech/patts/markup/Time$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/markup/Time;


# instance fields
.field private hasHours:Z

.field private hasMinutes:Z

.field private hasSeconds:Z

.field private hasStyle:Z

.field private hasZone:Z

.field private hours_:I

.field private memoizedSerializedSize:I

.field private minutes_:I

.field private seconds_:I

.field private style_:Ljava/lang/String;

.field private zone_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 411
    new-instance v0, Lcom/google/speech/patts/markup/Time;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/markup/Time;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/markup/Time;->defaultInstance:Lcom/google/speech/patts/markup/Time;

    .line 412
    invoke-static {}, Lcom/google/speech/patts/markup/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 413
    sget-object v0, Lcom/google/speech/patts/markup/Time;->defaultInstance:Lcom/google/speech/patts/markup/Time;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Time;->initFields()V

    .line 414
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/patts/markup/Time;->hours_:I

    .line 32
    iput v0, p0, Lcom/google/speech/patts/markup/Time;->minutes_:I

    .line 39
    iput v0, p0, Lcom/google/speech/patts/markup/Time;->seconds_:I

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Time;->style_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Time;->zone_:Ljava/lang/String;

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Time;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Time;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/markup/Time$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/markup/Time$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Time;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/patts/markup/Time;->hours_:I

    .line 32
    iput v0, p0, Lcom/google/speech/patts/markup/Time;->minutes_:I

    .line 39
    iput v0, p0, Lcom/google/speech/patts/markup/Time;->seconds_:I

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Time;->style_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Time;->zone_:Ljava/lang/String;

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Time;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/markup/Time;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Time;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Time;->style_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/speech/patts/markup/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Time;->hasZone:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/patts/markup/Time;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Time;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Time;->zone_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/speech/patts/markup/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Time;->hasHours:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/markup/Time;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Time;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/markup/Time;->hours_:I

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/markup/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Time;->hasMinutes:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/markup/Time;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Time;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/markup/Time;->minutes_:I

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/markup/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Time;->hasSeconds:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/markup/Time;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Time;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/markup/Time;->seconds_:I

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/markup/Time;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Time;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Time;->hasStyle:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/markup/Time;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/markup/Time;->defaultInstance:Lcom/google/speech/patts/markup/Time;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/markup/Time$Builder;
    .locals 1

    .prologue
    .line 180
    # invokes: Lcom/google/speech/patts/markup/Time$Builder;->create()Lcom/google/speech/patts/markup/Time$Builder;
    invoke-static {}, Lcom/google/speech/patts/markup/Time$Builder;->access$100()Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Time$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/markup/Time;

    .prologue
    .line 183
    invoke-static {}, Lcom/google/speech/patts/markup/Time;->newBuilder()Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/markup/Time$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->getDefaultInstanceForType()Lcom/google/speech/patts/markup/Time;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/markup/Time;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/markup/Time;->defaultInstance:Lcom/google/speech/patts/markup/Time;

    return-object v0
.end method

.method public getHours()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/patts/markup/Time;->hours_:I

    return v0
.end method

.method public getMinutes()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/speech/patts/markup/Time;->minutes_:I

    return v0
.end method

.method public getSeconds()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/speech/patts/markup/Time;->seconds_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 85
    iget v0, p0, Lcom/google/speech/patts/markup/Time;->memoizedSerializedSize:I

    .line 86
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 110
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 88
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 89
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->hasHours()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->getHours()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 93
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->hasMinutes()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 94
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->getMinutes()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 97
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->hasSeconds()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 98
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->getSeconds()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 101
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 102
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->getStyle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 105
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->hasZone()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 106
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->getZone()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 109
    :cond_5
    iput v0, p0, Lcom/google/speech/patts/markup/Time;->memoizedSerializedSize:I

    move v1, v0

    .line 110
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time;->style_:Ljava/lang/String;

    return-object v0
.end method

.method public getZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time;->zone_:Ljava/lang/String;

    return-object v0
.end method

.method public hasHours()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Time;->hasHours:Z

    return v0
.end method

.method public hasMinutes()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Time;->hasMinutes:Z

    return v0
.end method

.method public hasSeconds()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Time;->hasSeconds:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Time;->hasStyle:Z

    return v0
.end method

.method public hasZone()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Time;->hasZone:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->toBuilder()Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/markup/Time$Builder;
    .locals 1

    .prologue
    .line 185
    invoke-static {p0}, Lcom/google/speech/patts/markup/Time;->newBuilder(Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->getSerializedSize()I

    .line 66
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->hasHours()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->getHours()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->hasMinutes()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->getMinutes()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 72
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->hasSeconds()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->getSeconds()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 75
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 76
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->getStyle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 78
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->hasZone()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 79
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time;->getZone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 81
    :cond_4
    return-void
.end method
