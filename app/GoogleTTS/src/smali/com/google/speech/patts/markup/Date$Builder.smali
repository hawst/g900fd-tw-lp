.class public final Lcom/google/speech/patts/markup/Date$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Date.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Date;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/markup/Date;",
        "Lcom/google/speech/patts/markup/Date$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/markup/Date;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 253
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/markup/Date$Builder;
    .locals 1

    .prologue
    .line 247
    invoke-static {}, Lcom/google/speech/patts/markup/Date$Builder;->create()Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/markup/Date$Builder;
    .locals 3

    .prologue
    .line 256
    new-instance v0, Lcom/google/speech/patts/markup/Date$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Date$Builder;-><init>()V

    .line 257
    .local v0, "builder":Lcom/google/speech/patts/markup/Date$Builder;
    new-instance v1, Lcom/google/speech/patts/markup/Date;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/markup/Date;-><init>(Lcom/google/speech/patts/markup/Date$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    .line 258
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date$Builder;->build()Lcom/google/speech/patts/markup/Date;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/markup/Date;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    invoke-static {v0}, Lcom/google/speech/patts/markup/Date$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 289
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date$Builder;->buildPartial()Lcom/google/speech/patts/markup/Date;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/markup/Date;
    .locals 3

    .prologue
    .line 302
    iget-object v1, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    if-nez v1, :cond_0

    .line 303
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    .line 307
    .local v0, "returnMe":Lcom/google/speech/patts/markup/Date;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    .line 308
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date$Builder;->clone()Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date$Builder;->clone()Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/markup/Date$Builder;
    .locals 2

    .prologue
    .line 275
    invoke-static {}, Lcom/google/speech/patts/markup/Date$Builder;->create()Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/markup/Date$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Date$Builder;->clone()Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Date;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 247
    check-cast p1, Lcom/google/speech/patts/markup/Date;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/markup/Date$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Date$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Date$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/patts/markup/Date;

    .prologue
    .line 312
    invoke-static {}, Lcom/google/speech/patts/markup/Date;->getDefaultInstance()Lcom/google/speech/patts/markup/Date;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-object p0

    .line 313
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Date;->hasDay()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 314
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Date;->getDay()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Date$Builder;->setDay(I)Lcom/google/speech/patts/markup/Date$Builder;

    .line 316
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Date;->hasMonth()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 317
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Date;->getMonth()Lcom/google/speech/patts/markup/Date$LegalMonth;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Date$Builder;->setMonth(Lcom/google/speech/patts/markup/Date$LegalMonth;)Lcom/google/speech/patts/markup/Date$Builder;

    .line 319
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Date;->hasYear()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 320
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Date;->getYear()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Date$Builder;->setYear(I)Lcom/google/speech/patts/markup/Date$Builder;

    .line 322
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Date;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 323
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Date;->getStyle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Date$Builder;->setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Date$Builder;

    .line 325
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Date;->hasText()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Date;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Date$Builder;->setText(Ljava/lang/String;)Lcom/google/speech/patts/markup/Date$Builder;

    goto :goto_0
.end method

.method public setDay(I)Lcom/google/speech/patts/markup/Date$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Date;->hasDay:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Date;->access$302(Lcom/google/speech/patts/markup/Date;Z)Z

    .line 384
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    # setter for: Lcom/google/speech/patts/markup/Date;->day_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Date;->access$402(Lcom/google/speech/patts/markup/Date;I)I

    .line 385
    return-object p0
.end method

.method public setMonth(Lcom/google/speech/patts/markup/Date$LegalMonth;)Lcom/google/speech/patts/markup/Date$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/Date$LegalMonth;

    .prologue
    .line 401
    if-nez p1, :cond_0

    .line 402
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Date;->hasMonth:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Date;->access$502(Lcom/google/speech/patts/markup/Date;Z)Z

    .line 405
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    # setter for: Lcom/google/speech/patts/markup/Date;->month_:Lcom/google/speech/patts/markup/Date$LegalMonth;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Date;->access$602(Lcom/google/speech/patts/markup/Date;Lcom/google/speech/patts/markup/Date$LegalMonth;)Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 406
    return-object p0
.end method

.method public setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Date$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 440
    if-nez p1, :cond_0

    .line 441
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 443
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Date;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Date;->access$902(Lcom/google/speech/patts/markup/Date;Z)Z

    .line 444
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    # setter for: Lcom/google/speech/patts/markup/Date;->style_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Date;->access$1002(Lcom/google/speech/patts/markup/Date;Ljava/lang/String;)Ljava/lang/String;

    .line 445
    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/google/speech/patts/markup/Date$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 461
    if-nez p1, :cond_0

    .line 462
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Date;->hasText:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Date;->access$1102(Lcom/google/speech/patts/markup/Date;Z)Z

    .line 465
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    # setter for: Lcom/google/speech/patts/markup/Date;->text_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Date;->access$1202(Lcom/google/speech/patts/markup/Date;Ljava/lang/String;)Ljava/lang/String;

    .line 466
    return-object p0
.end method

.method public setYear(I)Lcom/google/speech/patts/markup/Date$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Date;->hasYear:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Date;->access$702(Lcom/google/speech/patts/markup/Date;Z)Z

    .line 423
    iget-object v0, p0, Lcom/google/speech/patts/markup/Date$Builder;->result:Lcom/google/speech/patts/markup/Date;

    # setter for: Lcom/google/speech/patts/markup/Date;->year_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Date;->access$802(Lcom/google/speech/patts/markup/Date;I)I

    .line 424
    return-object p0
.end method
