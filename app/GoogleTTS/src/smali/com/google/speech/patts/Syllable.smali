.class public final Lcom/google/speech/patts/Syllable;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Syllable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/Syllable$1;,
        Lcom/google/speech/patts/Syllable$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/Syllable;


# instance fields
.field private contour_:Ljava/lang/String;

.field private firstDaughter_:I

.field private hasContour:Z

.field private hasFirstDaughter:Z

.field private hasLastDaughter:Z

.field private hasName:Z

.field private hasParent:Z

.field private hasStress:Z

.field private hasSupraSegmentalModelName:Z

.field private hasTone:Z

.field private lastDaughter_:I

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private parent_:I

.field private stress_:I

.field private supraSegmentalModelName_:Ljava/lang/String;

.field private tone_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 531
    new-instance v0, Lcom/google/speech/patts/Syllable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/Syllable;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/Syllable;->defaultInstance:Lcom/google/speech/patts/Syllable;

    .line 532
    invoke-static {}, Lcom/google/speech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 533
    sget-object v0, Lcom/google/speech/patts/Syllable;->defaultInstance:Lcom/google/speech/patts/Syllable;

    invoke-direct {v0}, Lcom/google/speech/patts/Syllable;->initFields()V

    .line 534
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v1, p0, Lcom/google/speech/patts/Syllable;->parent_:I

    .line 32
    iput v1, p0, Lcom/google/speech/patts/Syllable;->firstDaughter_:I

    .line 39
    iput v1, p0, Lcom/google/speech/patts/Syllable;->lastDaughter_:I

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Syllable;->contour_:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/google/speech/patts/Syllable;->stress_:I

    .line 60
    iput v1, p0, Lcom/google/speech/patts/Syllable;->tone_:I

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Syllable;->name_:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Syllable;->supraSegmentalModelName_:Ljava/lang/String;

    .line 113
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/Syllable;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/Syllable;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/Syllable$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/Syllable$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/Syllable;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v1, p0, Lcom/google/speech/patts/Syllable;->parent_:I

    .line 32
    iput v1, p0, Lcom/google/speech/patts/Syllable;->firstDaughter_:I

    .line 39
    iput v1, p0, Lcom/google/speech/patts/Syllable;->lastDaughter_:I

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Syllable;->contour_:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/google/speech/patts/Syllable;->stress_:I

    .line 60
    iput v1, p0, Lcom/google/speech/patts/Syllable;->tone_:I

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Syllable;->name_:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Syllable;->supraSegmentalModelName_:Ljava/lang/String;

    .line 113
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/Syllable;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/Syllable;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Syllable;->contour_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/speech/patts/Syllable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Syllable;->hasStress:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/patts/Syllable;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Syllable;->stress_:I

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/patts/Syllable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Syllable;->hasTone:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/speech/patts/Syllable;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Syllable;->tone_:I

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/patts/Syllable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Syllable;->hasName:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/google/speech/patts/Syllable;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Syllable;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1702(Lcom/google/speech/patts/Syllable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Syllable;->hasSupraSegmentalModelName:Z

    return p1
.end method

.method static synthetic access$1802(Lcom/google/speech/patts/Syllable;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Syllable;->supraSegmentalModelName_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/speech/patts/Syllable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Syllable;->hasParent:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/Syllable;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Syllable;->parent_:I

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/Syllable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Syllable;->hasFirstDaughter:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/Syllable;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Syllable;->firstDaughter_:I

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/Syllable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Syllable;->hasLastDaughter:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/Syllable;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Syllable;->lastDaughter_:I

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/Syllable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Syllable;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Syllable;->hasContour:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/Syllable;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/Syllable;->defaultInstance:Lcom/google/speech/patts/Syllable;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/Syllable$Builder;
    .locals 1

    .prologue
    .line 222
    # invokes: Lcom/google/speech/patts/Syllable$Builder;->create()Lcom/google/speech/patts/Syllable$Builder;
    invoke-static {}, Lcom/google/speech/patts/Syllable$Builder;->access$100()Lcom/google/speech/patts/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/Syllable;)Lcom/google/speech/patts/Syllable$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/Syllable;

    .prologue
    .line 225
    invoke-static {}, Lcom/google/speech/patts/Syllable;->newBuilder()Lcom/google/speech/patts/Syllable$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/Syllable$Builder;->mergeFrom(Lcom/google/speech/patts/Syllable;)Lcom/google/speech/patts/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getContour()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/patts/Syllable;->contour_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getDefaultInstanceForType()Lcom/google/speech/patts/Syllable;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/Syllable;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/Syllable;->defaultInstance:Lcom/google/speech/patts/Syllable;

    return-object v0
.end method

.method public getFirstDaughter()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/speech/patts/Syllable;->firstDaughter_:I

    return v0
.end method

.method public getLastDaughter()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/speech/patts/Syllable;->lastDaughter_:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/speech/patts/Syllable;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getParent()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/patts/Syllable;->parent_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 115
    iget v0, p0, Lcom/google/speech/patts/Syllable;->memoizedSerializedSize:I

    .line 116
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 152
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 118
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 119
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasParent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 120
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getParent()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 123
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasFirstDaughter()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 124
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getFirstDaughter()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 127
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasLastDaughter()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 128
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getLastDaughter()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 131
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasContour()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 132
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getContour()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 135
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasStress()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 136
    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getStress()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 139
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasTone()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 140
    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getTone()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 143
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasName()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 144
    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 147
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasSupraSegmentalModelName()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 148
    const/16 v2, 0x3e8

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getSupraSegmentalModelName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 151
    :cond_8
    iput v0, p0, Lcom/google/speech/patts/Syllable;->memoizedSerializedSize:I

    move v1, v0

    .line 152
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto/16 :goto_0
.end method

.method public getStress()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/speech/patts/Syllable;->stress_:I

    return v0
.end method

.method public getSupraSegmentalModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/speech/patts/Syllable;->supraSegmentalModelName_:Ljava/lang/String;

    return-object v0
.end method

.method public getTone()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/google/speech/patts/Syllable;->tone_:I

    return v0
.end method

.method public hasContour()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/patts/Syllable;->hasContour:Z

    return v0
.end method

.method public hasFirstDaughter()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/Syllable;->hasFirstDaughter:Z

    return v0
.end method

.method public hasLastDaughter()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/patts/Syllable;->hasLastDaughter:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/patts/Syllable;->hasName:Z

    return v0
.end method

.method public hasParent()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/Syllable;->hasParent:Z

    return v0
.end method

.method public hasStress()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/patts/Syllable;->hasStress:Z

    return v0
.end method

.method public hasSupraSegmentalModelName()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/speech/patts/Syllable;->hasSupraSegmentalModelName:Z

    return v0
.end method

.method public hasTone()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/patts/Syllable;->hasTone:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->toBuilder()Lcom/google/speech/patts/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/Syllable$Builder;
    .locals 1

    .prologue
    .line 227
    invoke-static {p0}, Lcom/google/speech/patts/Syllable;->newBuilder(Lcom/google/speech/patts/Syllable;)Lcom/google/speech/patts/Syllable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getSerializedSize()I

    .line 87
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasParent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getParent()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 90
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasFirstDaughter()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getFirstDaughter()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 93
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasLastDaughter()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getLastDaughter()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 96
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasContour()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 97
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getContour()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 99
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasStress()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 100
    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getStress()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 102
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasTone()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 103
    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getTone()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 105
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasName()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 106
    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 108
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->hasSupraSegmentalModelName()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 109
    const/16 v0, 0x3e8

    invoke-virtual {p0}, Lcom/google/speech/patts/Syllable;->getSupraSegmentalModelName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 111
    :cond_7
    return-void
.end method
