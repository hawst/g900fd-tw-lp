.class public final Lcom/google/speech/patts/hmm/HmmTransform$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "HmmTransform.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/hmm/HmmTransform;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/hmm/HmmTransform;",
        "Lcom/google/speech/patts/hmm/HmmTransform$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/hmm/HmmTransform;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    .locals 1

    .prologue
    .line 213
    invoke-static {}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->create()Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    .locals 3

    .prologue
    .line 222
    new-instance v0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;-><init>()V

    .line 223
    .local v0, "builder":Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    new-instance v1, Lcom/google/speech/patts/hmm/HmmTransform;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/hmm/HmmTransform;-><init>(Lcom/google/speech/patts/hmm/HmmTransform$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    .line 224
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->build()Lcom/google/speech/patts/hmm/HmmTransform;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/hmm/HmmTransform;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    invoke-static {v0}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 255
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->buildPartial()Lcom/google/speech/patts/hmm/HmmTransform;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/hmm/HmmTransform;
    .locals 3

    .prologue
    .line 268
    iget-object v1, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    if-nez v1, :cond_0

    .line 269
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 272
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    # getter for: Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$300(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 273
    iget-object v1, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    iget-object v2, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    # getter for: Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/hmm/HmmTransform;->access$300(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/hmm/HmmTransform;->access$302(Lcom/google/speech/patts/hmm/HmmTransform;Ljava/util/List;)Ljava/util/List;

    .line 276
    :cond_1
    iget-object v1, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    # getter for: Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$400(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 277
    iget-object v1, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    iget-object v2, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    # getter for: Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/hmm/HmmTransform;->access$400(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/hmm/HmmTransform;->access$402(Lcom/google/speech/patts/hmm/HmmTransform;Ljava/util/List;)Ljava/util/List;

    .line 280
    :cond_2
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    .line 281
    .local v0, "returnMe":Lcom/google/speech/patts/hmm/HmmTransform;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    .line 282
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->clone()Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->clone()Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    .locals 2

    .prologue
    .line 241
    invoke-static {}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->create()Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->clone()Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    invoke-virtual {v0}, Lcom/google/speech/patts/hmm/HmmTransform;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 213
    check-cast p1, Lcom/google/speech/patts/hmm/HmmTransform;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/hmm/HmmTransform;

    .prologue
    .line 286
    invoke-static {}, Lcom/google/speech/patts/hmm/HmmTransform;->getDefaultInstance()Lcom/google/speech/patts/hmm/HmmTransform;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 308
    :cond_0
    :goto_0
    return-object p0

    .line 287
    :cond_1
    # getter for: Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$300(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 288
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    # getter for: Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/HmmTransform;->access$300(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 289
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$302(Lcom/google/speech/patts/hmm/HmmTransform;Ljava/util/List;)Ljava/util/List;

    .line 291
    :cond_2
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    # getter for: Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/HmmTransform;->access$300(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$300(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 293
    :cond_3
    # getter for: Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$400(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 294
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    # getter for: Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/HmmTransform;->access$400(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 295
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$402(Lcom/google/speech/patts/hmm/HmmTransform;Ljava/util/List;)Ljava/util/List;

    .line 297
    :cond_4
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    # getter for: Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/HmmTransform;->access$400(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$400(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 299
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/HmmTransform;->hasXformType()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 300
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/HmmTransform;->getXformType()Lcom/google/speech/patts/hmm/TransformType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->setXformType(Lcom/google/speech/patts/hmm/TransformType;)Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    .line 302
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/HmmTransform;->hasInvertedBlocks()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 303
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/HmmTransform;->getInvertedBlocks()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->setInvertedBlocks(Z)Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    .line 305
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/HmmTransform;->hasVoiceChecksum()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/HmmTransform;->getVoiceChecksum()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->setVoiceChecksum(I)Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    goto/16 :goto_0
.end method

.method public setInvertedBlocks(Z)Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/hmm/HmmTransform;->hasInvertedBlocks:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$702(Lcom/google/speech/patts/hmm/HmmTransform;Z)Z

    .line 481
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    # setter for: Lcom/google/speech/patts/hmm/HmmTransform;->invertedBlocks_:Z
    invoke-static {v0, p1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$802(Lcom/google/speech/patts/hmm/HmmTransform;Z)Z

    .line 482
    return-object p0
.end method

.method public setVoiceChecksum(I)Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/hmm/HmmTransform;->hasVoiceChecksum:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$902(Lcom/google/speech/patts/hmm/HmmTransform;Z)Z

    .line 499
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    # setter for: Lcom/google/speech/patts/hmm/HmmTransform;->voiceChecksum_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$1002(Lcom/google/speech/patts/hmm/HmmTransform;I)I

    .line 500
    return-object p0
.end method

.method public setXformType(Lcom/google/speech/patts/hmm/TransformType;)Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/hmm/TransformType;

    .prologue
    .line 459
    if-nez p1, :cond_0

    .line 460
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/hmm/HmmTransform;->hasXformType:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$502(Lcom/google/speech/patts/hmm/HmmTransform;Z)Z

    .line 463
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->result:Lcom/google/speech/patts/hmm/HmmTransform;

    # setter for: Lcom/google/speech/patts/hmm/HmmTransform;->xformType_:Lcom/google/speech/patts/hmm/TransformType;
    invoke-static {v0, p1}, Lcom/google/speech/patts/hmm/HmmTransform;->access$602(Lcom/google/speech/patts/hmm/HmmTransform;Lcom/google/speech/patts/hmm/TransformType;)Lcom/google/speech/patts/hmm/TransformType;

    .line 464
    return-object p0
.end method
