.class public final Lcom/google/speech/patts/ling_utt/Stream;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Stream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/ling_utt/Stream$1;,
        Lcom/google/speech/patts/ling_utt/Stream$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/ling_utt/Stream;


# instance fields
.field private hasName:Z

.field private items_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/ling_utt/Item;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 342
    new-instance v0, Lcom/google/speech/patts/ling_utt/Stream;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/ling_utt/Stream;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/ling_utt/Stream;->defaultInstance:Lcom/google/speech/patts/ling_utt/Stream;

    .line 343
    invoke-static {}, Lcom/google/speech/patts/ling_utt/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 344
    sget-object v0, Lcom/google/speech/patts/ling_utt/Stream;->defaultInstance:Lcom/google/speech/patts/ling_utt/Stream;

    invoke-direct {v0}, Lcom/google/speech/patts/ling_utt/Stream;->initFields()V

    .line 345
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream;->name_:Ljava/lang/String;

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/ling_utt/Stream;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/ling_utt/Stream;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/ling_utt/Stream$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/ling_utt/Stream$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/ling_utt/Stream;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream;->name_:Ljava/lang/String;

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/ling_utt/Stream;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lcom/google/speech/patts/ling_utt/Stream;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Stream;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/patts/ling_utt/Stream;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Stream;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/ling_utt/Stream;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Stream;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Stream;->hasName:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/ling_utt/Stream;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Stream;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Stream;->name_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/ling_utt/Stream;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/ling_utt/Stream;->defaultInstance:Lcom/google/speech/patts/ling_utt/Stream;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/ling_utt/Stream$Builder;
    .locals 1

    .prologue
    .line 147
    # invokes: Lcom/google/speech/patts/ling_utt/Stream$Builder;->create()Lcom/google/speech/patts/ling_utt/Stream$Builder;
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->access$100()Lcom/google/speech/patts/ling_utt/Stream$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/ling_utt/Stream;)Lcom/google/speech/patts/ling_utt/Stream$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/ling_utt/Stream;

    .prologue
    .line 150
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Stream;->newBuilder()Lcom/google/speech/patts/ling_utt/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->mergeFrom(Lcom/google/speech/patts/ling_utt/Stream;)Lcom/google/speech/patts/ling_utt/Stream$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream;->getDefaultInstanceForType()Lcom/google/speech/patts/ling_utt/Stream;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/ling_utt/Stream;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/ling_utt/Stream;->defaultInstance:Lcom/google/speech/patts/ling_utt/Stream;

    return-object v0
.end method

.method public getItemsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/ling_utt/Item;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 64
    iget v2, p0, Lcom/google/speech/patts/ling_utt/Stream;->memoizedSerializedSize:I

    .line 65
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 77
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 67
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 68
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream;->getItemsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Item;

    .line 69
    .local v0, "element":Lcom/google/speech/patts/ling_utt/Item;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 71
    goto :goto_1

    .line 72
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Item;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream;->hasName()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 73
    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 76
    :cond_2
    iput v2, p0, Lcom/google/speech/patts/ling_utt/Stream;->memoizedSerializedSize:I

    move v3, v2

    .line 77
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Stream;->hasName:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 44
    iget-boolean v3, p0, Lcom/google/speech/patts/ling_utt/Stream;->hasName:Z

    if-nez v3, :cond_0

    .line 48
    :goto_0
    return v2

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream;->getItemsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Item;

    .line 46
    .local v0, "element":Lcom/google/speech/patts/ling_utt/Item;
    invoke-virtual {v0}, Lcom/google/speech/patts/ling_utt/Item;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 48
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Item;
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream;->toBuilder()Lcom/google/speech/patts/ling_utt/Stream$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/ling_utt/Stream$Builder;
    .locals 1

    .prologue
    .line 152
    invoke-static {p0}, Lcom/google/speech/patts/ling_utt/Stream;->newBuilder(Lcom/google/speech/patts/ling_utt/Stream;)Lcom/google/speech/patts/ling_utt/Stream$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream;->getSerializedSize()I

    .line 54
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream;->getItemsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Item;

    .line 55
    .local v0, "element":Lcom/google/speech/patts/ling_utt/Item;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 57
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Item;
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 60
    :cond_1
    return-void
.end method
