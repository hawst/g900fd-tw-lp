.class public final Lcom/google/speech/patts/markup/Fraction$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Fraction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Fraction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/markup/Fraction;",
        "Lcom/google/speech/patts/markup/Fraction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/markup/Fraction;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/markup/Fraction$Builder;
    .locals 1

    .prologue
    .line 161
    invoke-static {}, Lcom/google/speech/patts/markup/Fraction$Builder;->create()Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/markup/Fraction$Builder;
    .locals 3

    .prologue
    .line 170
    new-instance v0, Lcom/google/speech/patts/markup/Fraction$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Fraction$Builder;-><init>()V

    .line 171
    .local v0, "builder":Lcom/google/speech/patts/markup/Fraction$Builder;
    new-instance v1, Lcom/google/speech/patts/markup/Fraction;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/markup/Fraction;-><init>(Lcom/google/speech/patts/markup/Fraction$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    .line 172
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction$Builder;->build()Lcom/google/speech/patts/markup/Fraction;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/markup/Fraction;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    invoke-static {v0}, Lcom/google/speech/patts/markup/Fraction$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 203
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction$Builder;->buildPartial()Lcom/google/speech/patts/markup/Fraction;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/markup/Fraction;
    .locals 3

    .prologue
    .line 216
    iget-object v1, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    if-nez v1, :cond_0

    .line 217
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    .line 221
    .local v0, "returnMe":Lcom/google/speech/patts/markup/Fraction;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    .line 222
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction$Builder;->clone()Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction$Builder;->clone()Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/markup/Fraction$Builder;
    .locals 2

    .prologue
    .line 189
    invoke-static {}, Lcom/google/speech/patts/markup/Fraction$Builder;->create()Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/markup/Fraction$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction$Builder;->clone()Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Fraction;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 161
    check-cast p1, Lcom/google/speech/patts/markup/Fraction;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/markup/Fraction$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Fraction$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/patts/markup/Fraction;

    .prologue
    .line 226
    invoke-static {}, Lcom/google/speech/patts/markup/Fraction;->getDefaultInstance()Lcom/google/speech/patts/markup/Fraction;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-object p0

    .line 227
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Fraction;->hasNumerator()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 228
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Fraction;->getNumerator()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Fraction$Builder;->setNumerator(I)Lcom/google/speech/patts/markup/Fraction$Builder;

    .line 230
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Fraction;->hasDenominator()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 231
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Fraction;->getDenominator()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Fraction$Builder;->setDenominator(I)Lcom/google/speech/patts/markup/Fraction$Builder;

    .line 233
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Fraction;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Fraction;->getStyle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Fraction$Builder;->setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Fraction$Builder;

    goto :goto_0
.end method

.method public setDenominator(I)Lcom/google/speech/patts/markup/Fraction$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Fraction;->hasDenominator:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Fraction;->access$502(Lcom/google/speech/patts/markup/Fraction;Z)Z

    .line 298
    iget-object v0, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    # setter for: Lcom/google/speech/patts/markup/Fraction;->denominator_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Fraction;->access$602(Lcom/google/speech/patts/markup/Fraction;I)I

    .line 299
    return-object p0
.end method

.method public setNumerator(I)Lcom/google/speech/patts/markup/Fraction$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Fraction;->hasNumerator:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Fraction;->access$302(Lcom/google/speech/patts/markup/Fraction;Z)Z

    .line 280
    iget-object v0, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    # setter for: Lcom/google/speech/patts/markup/Fraction;->numerator_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Fraction;->access$402(Lcom/google/speech/patts/markup/Fraction;I)I

    .line 281
    return-object p0
.end method

.method public setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Fraction$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 315
    if-nez p1, :cond_0

    .line 316
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Fraction;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Fraction;->access$702(Lcom/google/speech/patts/markup/Fraction;Z)Z

    .line 319
    iget-object v0, p0, Lcom/google/speech/patts/markup/Fraction$Builder;->result:Lcom/google/speech/patts/markup/Fraction;

    # setter for: Lcom/google/speech/patts/markup/Fraction;->style_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Fraction;->access$802(Lcom/google/speech/patts/markup/Fraction;Ljava/lang/String;)Ljava/lang/String;

    .line 320
    return-object p0
.end method
