.class public final Lcom/google/speech/patts/Frame$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Frame.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/Frame;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/Frame;",
        "Lcom/google/speech/patts/Frame$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/Frame;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/Frame$Builder;
    .locals 1

    .prologue
    .line 193
    invoke-static {}, Lcom/google/speech/patts/Frame$Builder;->create()Lcom/google/speech/patts/Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/Frame$Builder;
    .locals 3

    .prologue
    .line 202
    new-instance v0, Lcom/google/speech/patts/Frame$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/Frame$Builder;-><init>()V

    .line 203
    .local v0, "builder":Lcom/google/speech/patts/Frame$Builder;
    new-instance v1, Lcom/google/speech/patts/Frame;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/Frame;-><init>(Lcom/google/speech/patts/Frame$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    .line 204
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame$Builder;->build()Lcom/google/speech/patts/Frame;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/Frame;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/Frame$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    invoke-static {v0}, Lcom/google/speech/patts/Frame$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 235
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame$Builder;->buildPartial()Lcom/google/speech/patts/Frame;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/Frame;
    .locals 3

    .prologue
    .line 248
    iget-object v1, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    if-nez v1, :cond_0

    .line 249
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    # getter for: Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/Frame;->access$300(Lcom/google/speech/patts/Frame;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 253
    iget-object v1, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    iget-object v2, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    # getter for: Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/Frame;->access$300(Lcom/google/speech/patts/Frame;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/Frame;->access$302(Lcom/google/speech/patts/Frame;Ljava/util/List;)Ljava/util/List;

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    .line 257
    .local v0, "returnMe":Lcom/google/speech/patts/Frame;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    .line 258
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame$Builder;->clone()Lcom/google/speech/patts/Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame$Builder;->clone()Lcom/google/speech/patts/Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/Frame$Builder;
    .locals 2

    .prologue
    .line 221
    invoke-static {}, Lcom/google/speech/patts/Frame$Builder;->create()Lcom/google/speech/patts/Frame$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/Frame$Builder;->mergeFrom(Lcom/google/speech/patts/Frame;)Lcom/google/speech/patts/Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame$Builder;->clone()Lcom/google/speech/patts/Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    invoke-virtual {v0}, Lcom/google/speech/patts/Frame;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 193
    check-cast p1, Lcom/google/speech/patts/Frame;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/Frame$Builder;->mergeFrom(Lcom/google/speech/patts/Frame;)Lcom/google/speech/patts/Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/Frame;)Lcom/google/speech/patts/Frame$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/Frame;

    .prologue
    .line 262
    invoke-static {}, Lcom/google/speech/patts/Frame;->getDefaultInstance()Lcom/google/speech/patts/Frame;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 281
    :cond_0
    :goto_0
    return-object p0

    .line 263
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/Frame;->hasParent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264
    invoke-virtual {p1}, Lcom/google/speech/patts/Frame;->getParent()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Frame$Builder;->setParent(I)Lcom/google/speech/patts/Frame$Builder;

    .line 266
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/Frame;->hasFirstDaughter()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 267
    invoke-virtual {p1}, Lcom/google/speech/patts/Frame;->getFirstDaughter()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Frame$Builder;->setFirstDaughter(I)Lcom/google/speech/patts/Frame$Builder;

    .line 269
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/Frame;->hasLastDaughter()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 270
    invoke-virtual {p1}, Lcom/google/speech/patts/Frame;->getLastDaughter()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Frame$Builder;->setLastDaughter(I)Lcom/google/speech/patts/Frame$Builder;

    .line 272
    :cond_4
    # getter for: Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/Frame;->access$300(Lcom/google/speech/patts/Frame;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 273
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    # getter for: Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/Frame;->access$300(Lcom/google/speech/patts/Frame;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 274
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/Frame;->access$302(Lcom/google/speech/patts/Frame;Ljava/util/List;)Ljava/util/List;

    .line 276
    :cond_5
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    # getter for: Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/Frame;->access$300(Lcom/google/speech/patts/Frame;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/Frame;->access$300(Lcom/google/speech/patts/Frame;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 278
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/patts/Frame;->hasVocoderInputs()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    invoke-virtual {p1}, Lcom/google/speech/patts/Frame;->getVocoderInputs()Lspeech/patts/VocoderInput;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Frame$Builder;->mergeVocoderInputs(Lspeech/patts/VocoderInput;)Lcom/google/speech/patts/Frame$Builder;

    goto :goto_0
.end method

.method public mergeVocoderInputs(Lspeech/patts/VocoderInput;)Lcom/google/speech/patts/Frame$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/VocoderInput;

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    invoke-virtual {v0}, Lcom/google/speech/patts/Frame;->hasVocoderInputs()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    # getter for: Lcom/google/speech/patts/Frame;->vocoderInputs_:Lspeech/patts/VocoderInput;
    invoke-static {v0}, Lcom/google/speech/patts/Frame;->access$1100(Lcom/google/speech/patts/Frame;)Lspeech/patts/VocoderInput;

    move-result-object v0

    invoke-static {}, Lspeech/patts/VocoderInput;->getDefaultInstance()Lspeech/patts/VocoderInput;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 459
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    iget-object v1, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    # getter for: Lcom/google/speech/patts/Frame;->vocoderInputs_:Lspeech/patts/VocoderInput;
    invoke-static {v1}, Lcom/google/speech/patts/Frame;->access$1100(Lcom/google/speech/patts/Frame;)Lspeech/patts/VocoderInput;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/VocoderInput;->newBuilder(Lspeech/patts/VocoderInput;)Lspeech/patts/VocoderInput$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/VocoderInput$Builder;->mergeFrom(Lspeech/patts/VocoderInput;)Lspeech/patts/VocoderInput$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/VocoderInput$Builder;->buildPartial()Lspeech/patts/VocoderInput;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/Frame;->vocoderInputs_:Lspeech/patts/VocoderInput;
    invoke-static {v0, v1}, Lcom/google/speech/patts/Frame;->access$1102(Lcom/google/speech/patts/Frame;Lspeech/patts/VocoderInput;)Lspeech/patts/VocoderInput;

    .line 464
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Frame;->hasVocoderInputs:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Frame;->access$1002(Lcom/google/speech/patts/Frame;Z)Z

    .line 465
    return-object p0

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    # setter for: Lcom/google/speech/patts/Frame;->vocoderInputs_:Lspeech/patts/VocoderInput;
    invoke-static {v0, p1}, Lcom/google/speech/patts/Frame;->access$1102(Lcom/google/speech/patts/Frame;Lspeech/patts/VocoderInput;)Lspeech/patts/VocoderInput;

    goto :goto_0
.end method

.method public setFirstDaughter(I)Lcom/google/speech/patts/Frame$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Frame;->hasFirstDaughter:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Frame;->access$602(Lcom/google/speech/patts/Frame;Z)Z

    .line 358
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    # setter for: Lcom/google/speech/patts/Frame;->firstDaughter_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Frame;->access$702(Lcom/google/speech/patts/Frame;I)I

    .line 359
    return-object p0
.end method

.method public setLastDaughter(I)Lcom/google/speech/patts/Frame$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Frame;->hasLastDaughter:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Frame;->access$802(Lcom/google/speech/patts/Frame;Z)Z

    .line 376
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    # setter for: Lcom/google/speech/patts/Frame;->lastDaughter_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Frame;->access$902(Lcom/google/speech/patts/Frame;I)I

    .line 377
    return-object p0
.end method

.method public setParent(I)Lcom/google/speech/patts/Frame$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Frame;->hasParent:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Frame;->access$402(Lcom/google/speech/patts/Frame;Z)Z

    .line 340
    iget-object v0, p0, Lcom/google/speech/patts/Frame$Builder;->result:Lcom/google/speech/patts/Frame;

    # setter for: Lcom/google/speech/patts/Frame;->parent_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Frame;->access$502(Lcom/google/speech/patts/Frame;I)I

    .line 341
    return-object p0
.end method
