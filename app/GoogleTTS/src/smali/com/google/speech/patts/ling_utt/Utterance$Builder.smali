.class public final Lcom/google/speech/patts/ling_utt/Utterance$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Utterance.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/ling_utt/Utterance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/ling_utt/Utterance;",
        "Lcom/google/speech/patts/ling_utt/Utterance$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/ling_utt/Utterance;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/ling_utt/Utterance$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->create()Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/ling_utt/Utterance$Builder;
    .locals 3

    .prologue
    .line 177
    new-instance v0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;-><init>()V

    .line 178
    .local v0, "builder":Lcom/google/speech/patts/ling_utt/Utterance$Builder;
    new-instance v1, Lcom/google/speech/patts/ling_utt/Utterance;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/ling_utt/Utterance;-><init>(Lcom/google/speech/patts/ling_utt/Utterance$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    .line 179
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->build()Lcom/google/speech/patts/ling_utt/Utterance;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/ling_utt/Utterance;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 210
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->buildPartial()Lcom/google/speech/patts/ling_utt/Utterance;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/ling_utt/Utterance;
    .locals 3

    .prologue
    .line 223
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    if-nez v1, :cond_0

    .line 224
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 227
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    # getter for: Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/ling_utt/Utterance;->access$300(Lcom/google/speech/patts/ling_utt/Utterance;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 228
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    iget-object v2, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    # getter for: Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/ling_utt/Utterance;->access$300(Lcom/google/speech/patts/ling_utt/Utterance;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/ling_utt/Utterance;->access$302(Lcom/google/speech/patts/ling_utt/Utterance;Ljava/util/List;)Ljava/util/List;

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    .line 232
    .local v0, "returnMe":Lcom/google/speech/patts/ling_utt/Utterance;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    .line 233
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->clone()Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->clone()Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/ling_utt/Utterance$Builder;
    .locals 2

    .prologue
    .line 196
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->create()Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->mergeFrom(Lcom/google/speech/patts/ling_utt/Utterance;)Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->clone()Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    invoke-virtual {v0}, Lcom/google/speech/patts/ling_utt/Utterance;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 168
    check-cast p1, Lcom/google/speech/patts/ling_utt/Utterance;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->mergeFrom(Lcom/google/speech/patts/ling_utt/Utterance;)Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/ling_utt/Utterance;)Lcom/google/speech/patts/ling_utt/Utterance$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/ling_utt/Utterance;

    .prologue
    .line 237
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Utterance;->getDefaultInstance()Lcom/google/speech/patts/ling_utt/Utterance;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-object p0

    .line 238
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Utterance;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 239
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Utterance;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->setName(Ljava/lang/String;)Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    .line 241
    :cond_2
    # getter for: Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Utterance;->access$300(Lcom/google/speech/patts/ling_utt/Utterance;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 242
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    # getter for: Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Utterance;->access$300(Lcom/google/speech/patts/ling_utt/Utterance;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 243
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Utterance;->access$302(Lcom/google/speech/patts/ling_utt/Utterance;Ljava/util/List;)Ljava/util/List;

    .line 245
    :cond_3
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    # getter for: Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Utterance;->access$300(Lcom/google/speech/patts/ling_utt/Utterance;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Utterance;->access$300(Lcom/google/speech/patts/ling_utt/Utterance;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 247
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Utterance;->hasRuntimeId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Utterance;->getRuntimeId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->setRuntimeId(J)Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/patts/ling_utt/Utterance$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 295
    if-nez p1, :cond_0

    .line 296
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/ling_utt/Utterance;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Utterance;->access$402(Lcom/google/speech/patts/ling_utt/Utterance;Z)Z

    .line 299
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    # setter for: Lcom/google/speech/patts/ling_utt/Utterance;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/ling_utt/Utterance;->access$502(Lcom/google/speech/patts/ling_utt/Utterance;Ljava/lang/String;)Ljava/lang/String;

    .line 300
    return-object p0
.end method

.method public setRuntimeId(J)Lcom/google/speech/patts/ling_utt/Utterance$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/ling_utt/Utterance;->hasRuntimeId:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Utterance;->access$602(Lcom/google/speech/patts/ling_utt/Utterance;Z)Z

    .line 368
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->result:Lcom/google/speech/patts/ling_utt/Utterance;

    # setter for: Lcom/google/speech/patts/ling_utt/Utterance;->runtimeId_:J
    invoke-static {v0, p1, p2}, Lcom/google/speech/patts/ling_utt/Utterance;->access$702(Lcom/google/speech/patts/ling_utt/Utterance;J)J

    .line 369
    return-object p0
.end method
