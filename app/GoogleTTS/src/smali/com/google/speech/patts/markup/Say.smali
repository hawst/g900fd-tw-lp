.class public final Lcom/google/speech/patts/markup/Say;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Say.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/markup/Say$1;,
        Lcom/google/speech/patts/markup/Say$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/markup/Say;


# instance fields
.field private alternative_:Lcom/google/speech/patts/markup/Alternative;

.field private cardinal_:I

.field private date_:Lcom/google/speech/patts/markup/Date;

.field private decimal_:Lcom/google/speech/patts/markup/Decimal;

.field private digit_:Ljava/lang/String;

.field private foreignLanguage_:Ljava/lang/String;

.field private fraction_:Lcom/google/speech/patts/markup/Fraction;

.field private hasAlternative:Z

.field private hasCardinal:Z

.field private hasDate:Z

.field private hasDecimal:Z

.field private hasDigit:Z

.field private hasForeignLanguage:Z

.field private hasFraction:Z

.field private hasLetters:Z

.field private hasMeasure:Z

.field private hasMoney:Z

.field private hasOrdinal:Z

.field private hasPause:Z

.field private hasPercent:Z

.field private hasTelephone:Z

.field private hasText:Z

.field private hasTime:Z

.field private hasVerbatim:Z

.field private hasVoicemod:Z

.field private hasWords:Z

.field private hasWordsequence:Z

.field private letters_:Ljava/lang/String;

.field private measure_:Lcom/google/speech/patts/markup/Measure;

.field private memoizedSerializedSize:I

.field private money_:Lcom/google/speech/patts/markup/Money;

.field private ordinal_:I

.field private pause_:F

.field private percent_:Lcom/google/speech/patts/markup/Decimal;

.field private telephone_:Lcom/google/speech/patts/markup/Telephone;

.field private text_:Ljava/lang/String;

.field private time_:Lcom/google/speech/patts/markup/Time;

.field private verbatim_:Ljava/lang/String;

.field private voicemod_:Lcom/google/speech/tts/VoiceMod;

.field private words_:Ljava/lang/String;

.field private wordsequence_:Lcom/google/speech/patts/markup/WordSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1310
    new-instance v0, Lcom/google/speech/patts/markup/Say;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/markup/Say;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/markup/Say;->defaultInstance:Lcom/google/speech/patts/markup/Say;

    .line 1311
    invoke-static {}, Lcom/google/speech/patts/markup/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 1312
    sget-object v0, Lcom/google/speech/patts/markup/Say;->defaultInstance:Lcom/google/speech/patts/markup/Say;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Say;->initFields()V

    .line 1313
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->text_:Ljava/lang/String;

    .line 32
    iput v1, p0, Lcom/google/speech/patts/markup/Say;->cardinal_:I

    .line 39
    iput v1, p0, Lcom/google/speech/patts/markup/Say;->ordinal_:I

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->digit_:Ljava/lang/String;

    .line 102
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->words_:Ljava/lang/String;

    .line 130
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/patts/markup/Say;->pause_:F

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->letters_:Ljava/lang/String;

    .line 144
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->verbatim_:Ljava/lang/String;

    .line 158
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->foreignLanguage_:Ljava/lang/String;

    .line 271
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Say;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Say;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/markup/Say$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/markup/Say$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Say;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->text_:Ljava/lang/String;

    .line 32
    iput v1, p0, Lcom/google/speech/patts/markup/Say;->cardinal_:I

    .line 39
    iput v1, p0, Lcom/google/speech/patts/markup/Say;->ordinal_:I

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->digit_:Ljava/lang/String;

    .line 102
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->words_:Ljava/lang/String;

    .line 130
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/patts/markup/Say;->pause_:F

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->letters_:Ljava/lang/String;

    .line 144
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->verbatim_:Ljava/lang/String;

    .line 158
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->foreignLanguage_:Ljava/lang/String;

    .line 271
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Say;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/markup/Say;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->digit_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasDecimal:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Decimal;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->decimal_:Lcom/google/speech/patts/markup/Decimal;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/Decimal;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->decimal_:Lcom/google/speech/patts/markup/Decimal;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasFraction:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Fraction;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->fraction_:Lcom/google/speech/patts/markup/Fraction;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Fraction;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/Fraction;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->fraction_:Lcom/google/speech/patts/markup/Fraction;

    return-object p1
.end method

.method static synthetic access$1502(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasTime:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Time;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->time_:Lcom/google/speech/patts/markup/Time;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Time;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/Time;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->time_:Lcom/google/speech/patts/markup/Time;

    return-object p1
.end method

.method static synthetic access$1702(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasMoney:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Money;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->money_:Lcom/google/speech/patts/markup/Money;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Money;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/Money;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->money_:Lcom/google/speech/patts/markup/Money;

    return-object p1
.end method

.method static synthetic access$1902(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasMeasure:Z

    return p1
.end method

.method static synthetic access$2000(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Measure;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->measure_:Lcom/google/speech/patts/markup/Measure;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Measure;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/Measure;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->measure_:Lcom/google/speech/patts/markup/Measure;

    return-object p1
.end method

.method static synthetic access$2102(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasTelephone:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Telephone;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->telephone_:Lcom/google/speech/patts/markup/Telephone;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Telephone;)Lcom/google/speech/patts/markup/Telephone;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/Telephone;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->telephone_:Lcom/google/speech/patts/markup/Telephone;

    return-object p1
.end method

.method static synthetic access$2302(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasDate:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Date;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->date_:Lcom/google/speech/patts/markup/Date;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Date;)Lcom/google/speech/patts/markup/Date;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/Date;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->date_:Lcom/google/speech/patts/markup/Date;

    return-object p1
.end method

.method static synthetic access$2502(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasWords:Z

    return p1
.end method

.method static synthetic access$2602(Lcom/google/speech/patts/markup/Say;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->words_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2702(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasWordsequence:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/WordSequence;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->wordsequence_:Lcom/google/speech/patts/markup/WordSequence;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/WordSequence;)Lcom/google/speech/patts/markup/WordSequence;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/WordSequence;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->wordsequence_:Lcom/google/speech/patts/markup/WordSequence;

    return-object p1
.end method

.method static synthetic access$2902(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasVoicemod:Z

    return p1
.end method

.method static synthetic access$3000(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/tts/VoiceMod;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Lcom/google/speech/tts/VoiceMod;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasText:Z

    return p1
.end method

.method static synthetic access$3102(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasPercent:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Decimal;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->percent_:Lcom/google/speech/patts/markup/Decimal;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/Decimal;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->percent_:Lcom/google/speech/patts/markup/Decimal;

    return-object p1
.end method

.method static synthetic access$3302(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasPause:Z

    return p1
.end method

.method static synthetic access$3402(Lcom/google/speech/patts/markup/Say;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/markup/Say;->pause_:F

    return p1
.end method

.method static synthetic access$3502(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasLetters:Z

    return p1
.end method

.method static synthetic access$3602(Lcom/google/speech/patts/markup/Say;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->letters_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3702(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasVerbatim:Z

    return p1
.end method

.method static synthetic access$3802(Lcom/google/speech/patts/markup/Say;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->verbatim_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3902(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasAlternative:Z

    return p1
.end method

.method static synthetic access$4000(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Alternative;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->alternative_:Lcom/google/speech/patts/markup/Alternative;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/google/speech/patts/markup/Say;Lcom/google/speech/patts/markup/Alternative;)Lcom/google/speech/patts/markup/Alternative;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/Alternative;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->alternative_:Lcom/google/speech/patts/markup/Alternative;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/markup/Say;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->text_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4102(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasForeignLanguage:Z

    return p1
.end method

.method static synthetic access$4202(Lcom/google/speech/patts/markup/Say;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Say;->foreignLanguage_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasCardinal:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/markup/Say;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/markup/Say;->cardinal_:I

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasOrdinal:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/markup/Say;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/markup/Say;->ordinal_:I

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/markup/Say;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Say;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Say;->hasDigit:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/markup/Say;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/markup/Say;->defaultInstance:Lcom/google/speech/patts/markup/Say;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 163
    invoke-static {}, Lcom/google/speech/patts/markup/Decimal;->getDefaultInstance()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->decimal_:Lcom/google/speech/patts/markup/Decimal;

    .line 164
    invoke-static {}, Lcom/google/speech/patts/markup/Fraction;->getDefaultInstance()Lcom/google/speech/patts/markup/Fraction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->fraction_:Lcom/google/speech/patts/markup/Fraction;

    .line 165
    invoke-static {}, Lcom/google/speech/patts/markup/Time;->getDefaultInstance()Lcom/google/speech/patts/markup/Time;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->time_:Lcom/google/speech/patts/markup/Time;

    .line 166
    invoke-static {}, Lcom/google/speech/patts/markup/Money;->getDefaultInstance()Lcom/google/speech/patts/markup/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->money_:Lcom/google/speech/patts/markup/Money;

    .line 167
    invoke-static {}, Lcom/google/speech/patts/markup/Measure;->getDefaultInstance()Lcom/google/speech/patts/markup/Measure;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->measure_:Lcom/google/speech/patts/markup/Measure;

    .line 168
    invoke-static {}, Lcom/google/speech/patts/markup/Telephone;->getDefaultInstance()Lcom/google/speech/patts/markup/Telephone;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->telephone_:Lcom/google/speech/patts/markup/Telephone;

    .line 169
    invoke-static {}, Lcom/google/speech/patts/markup/Date;->getDefaultInstance()Lcom/google/speech/patts/markup/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->date_:Lcom/google/speech/patts/markup/Date;

    .line 170
    invoke-static {}, Lcom/google/speech/patts/markup/WordSequence;->getDefaultInstance()Lcom/google/speech/patts/markup/WordSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->wordsequence_:Lcom/google/speech/patts/markup/WordSequence;

    .line 171
    invoke-static {}, Lcom/google/speech/tts/VoiceMod;->getDefaultInstance()Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;

    .line 172
    invoke-static {}, Lcom/google/speech/patts/markup/Decimal;->getDefaultInstance()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->percent_:Lcom/google/speech/patts/markup/Decimal;

    .line 173
    invoke-static {}, Lcom/google/speech/patts/markup/Alternative;->getDefaultInstance()Lcom/google/speech/patts/markup/Alternative;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Say;->alternative_:Lcom/google/speech/patts/markup/Alternative;

    .line 174
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/markup/Say$Builder;
    .locals 1

    .prologue
    .line 428
    # invokes: Lcom/google/speech/patts/markup/Say$Builder;->create()Lcom/google/speech/patts/markup/Say$Builder;
    invoke-static {}, Lcom/google/speech/patts/markup/Say$Builder;->access$100()Lcom/google/speech/patts/markup/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Say$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/markup/Say;

    .prologue
    .line 431
    invoke-static {}, Lcom/google/speech/patts/markup/Say;->newBuilder()Lcom/google/speech/patts/markup/Say$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/markup/Say$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Say$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAlternative()Lcom/google/speech/patts/markup/Alternative;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->alternative_:Lcom/google/speech/patts/markup/Alternative;

    return-object v0
.end method

.method public getCardinal()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/speech/patts/markup/Say;->cardinal_:I

    return v0
.end method

.method public getDate()Lcom/google/speech/patts/markup/Date;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->date_:Lcom/google/speech/patts/markup/Date;

    return-object v0
.end method

.method public getDecimal()Lcom/google/speech/patts/markup/Decimal;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->decimal_:Lcom/google/speech/patts/markup/Decimal;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getDefaultInstanceForType()Lcom/google/speech/patts/markup/Say;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/markup/Say;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/markup/Say;->defaultInstance:Lcom/google/speech/patts/markup/Say;

    return-object v0
.end method

.method public getDigit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->digit_:Ljava/lang/String;

    return-object v0
.end method

.method public getForeignLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->foreignLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public getFraction()Lcom/google/speech/patts/markup/Fraction;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->fraction_:Lcom/google/speech/patts/markup/Fraction;

    return-object v0
.end method

.method public getLetters()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->letters_:Ljava/lang/String;

    return-object v0
.end method

.method public getMeasure()Lcom/google/speech/patts/markup/Measure;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->measure_:Lcom/google/speech/patts/markup/Measure;

    return-object v0
.end method

.method public getMoney()Lcom/google/speech/patts/markup/Money;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->money_:Lcom/google/speech/patts/markup/Money;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/speech/patts/markup/Say;->ordinal_:I

    return v0
.end method

.method public getPause()F
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/google/speech/patts/markup/Say;->pause_:F

    return v0
.end method

.method public getPercent()Lcom/google/speech/patts/markup/Decimal;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->percent_:Lcom/google/speech/patts/markup/Decimal;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 273
    iget v0, p0, Lcom/google/speech/patts/markup/Say;->memoizedSerializedSize:I

    .line 274
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 358
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 276
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 277
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasText()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 278
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 281
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasCardinal()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 282
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getCardinal()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 285
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasOrdinal()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 286
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getOrdinal()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 289
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasDigit()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 290
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getDigit()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 293
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasDecimal()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 294
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getDecimal()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 297
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasFraction()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 298
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getFraction()Lcom/google/speech/patts/markup/Fraction;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 301
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasTime()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 302
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getTime()Lcom/google/speech/patts/markup/Time;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 305
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasMoney()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 306
    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getMoney()Lcom/google/speech/patts/markup/Money;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 309
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasMeasure()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 310
    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getMeasure()Lcom/google/speech/patts/markup/Measure;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 313
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasTelephone()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 314
    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getTelephone()Lcom/google/speech/patts/markup/Telephone;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 317
    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasDate()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 318
    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getDate()Lcom/google/speech/patts/markup/Date;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 321
    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasWords()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 322
    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getWords()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 325
    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasWordsequence()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 326
    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getWordsequence()Lcom/google/speech/patts/markup/WordSequence;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 329
    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasVoicemod()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 330
    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getVoicemod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 333
    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasPercent()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 334
    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getPercent()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 337
    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasPause()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 338
    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getPause()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 341
    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasLetters()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 342
    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getLetters()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 345
    :cond_11
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasVerbatim()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 346
    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getVerbatim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 349
    :cond_12
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasAlternative()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 350
    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getAlternative()Lcom/google/speech/patts/markup/Alternative;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 353
    :cond_13
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasForeignLanguage()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 354
    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getForeignLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 357
    :cond_14
    iput v0, p0, Lcom/google/speech/patts/markup/Say;->memoizedSerializedSize:I

    move v1, v0

    .line 358
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto/16 :goto_0
.end method

.method public getTelephone()Lcom/google/speech/patts/markup/Telephone;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->telephone_:Lcom/google/speech/patts/markup/Telephone;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->text_:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()Lcom/google/speech/patts/markup/Time;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->time_:Lcom/google/speech/patts/markup/Time;

    return-object v0
.end method

.method public getVerbatim()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->verbatim_:Ljava/lang/String;

    return-object v0
.end method

.method public getVoicemod()Lcom/google/speech/tts/VoiceMod;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->voicemod_:Lcom/google/speech/tts/VoiceMod;

    return-object v0
.end method

.method public getWords()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->words_:Ljava/lang/String;

    return-object v0
.end method

.method public getWordsequence()Lcom/google/speech/patts/markup/WordSequence;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/speech/patts/markup/Say;->wordsequence_:Lcom/google/speech/patts/markup/WordSequence;

    return-object v0
.end method

.method public hasAlternative()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasAlternative:Z

    return v0
.end method

.method public hasCardinal()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasCardinal:Z

    return v0
.end method

.method public hasDate()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasDate:Z

    return v0
.end method

.method public hasDecimal()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasDecimal:Z

    return v0
.end method

.method public hasDigit()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasDigit:Z

    return v0
.end method

.method public hasForeignLanguage()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasForeignLanguage:Z

    return v0
.end method

.method public hasFraction()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasFraction:Z

    return v0
.end method

.method public hasLetters()Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasLetters:Z

    return v0
.end method

.method public hasMeasure()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasMeasure:Z

    return v0
.end method

.method public hasMoney()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasMoney:Z

    return v0
.end method

.method public hasOrdinal()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasOrdinal:Z

    return v0
.end method

.method public hasPause()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasPause:Z

    return v0
.end method

.method public hasPercent()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasPercent:Z

    return v0
.end method

.method public hasTelephone()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasTelephone:Z

    return v0
.end method

.method public hasText()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasText:Z

    return v0
.end method

.method public hasTime()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasTime:Z

    return v0
.end method

.method public hasVerbatim()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasVerbatim:Z

    return v0
.end method

.method public hasVoicemod()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasVoicemod:Z

    return v0
.end method

.method public hasWords()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasWords:Z

    return v0
.end method

.method public hasWordsequence()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Say;->hasWordsequence:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 176
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasDecimal()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 177
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getDecimal()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Decimal;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 203
    :cond_0
    :goto_0
    return v0

    .line 179
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasFraction()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 180
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getFraction()Lcom/google/speech/patts/markup/Fraction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Fraction;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 182
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasMoney()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 183
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getMoney()Lcom/google/speech/patts/markup/Money;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Money;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasMeasure()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 186
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getMeasure()Lcom/google/speech/patts/markup/Measure;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Measure;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasTelephone()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 189
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getTelephone()Lcom/google/speech/patts/markup/Telephone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Telephone;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasWordsequence()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 192
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getWordsequence()Lcom/google/speech/patts/markup/WordSequence;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/WordSequence;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasVoicemod()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 195
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getVoicemod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/VoiceMod;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasPercent()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 198
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getPercent()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Decimal;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasAlternative()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 201
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getAlternative()Lcom/google/speech/patts/markup/Alternative;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Alternative;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    :cond_9
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->toBuilder()Lcom/google/speech/patts/markup/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/markup/Say$Builder;
    .locals 1

    .prologue
    .line 433
    invoke-static {p0}, Lcom/google/speech/patts/markup/Say;->newBuilder(Lcom/google/speech/patts/markup/Say;)Lcom/google/speech/patts/markup/Say$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getSerializedSize()I

    .line 209
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasText()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 212
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasCardinal()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getCardinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 215
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasOrdinal()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 216
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getOrdinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 218
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasDigit()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 219
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getDigit()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 221
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasDecimal()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 222
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getDecimal()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 224
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 225
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getFraction()Lcom/google/speech/patts/markup/Fraction;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 227
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 228
    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getTime()Lcom/google/speech/patts/markup/Time;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 230
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasMoney()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 231
    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getMoney()Lcom/google/speech/patts/markup/Money;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 233
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasMeasure()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 234
    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getMeasure()Lcom/google/speech/patts/markup/Measure;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 236
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasTelephone()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 237
    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getTelephone()Lcom/google/speech/patts/markup/Telephone;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 239
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 240
    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getDate()Lcom/google/speech/patts/markup/Date;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 242
    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasWords()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 243
    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getWords()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 245
    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasWordsequence()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 246
    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getWordsequence()Lcom/google/speech/patts/markup/WordSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 248
    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasVoicemod()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 249
    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getVoicemod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 251
    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasPercent()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 252
    const/16 v0, 0xf

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getPercent()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 254
    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasPause()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 255
    const/16 v0, 0x10

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getPause()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 257
    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasLetters()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 258
    const/16 v0, 0x11

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getLetters()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 260
    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasVerbatim()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 261
    const/16 v0, 0x12

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getVerbatim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 263
    :cond_11
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasAlternative()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 264
    const/16 v0, 0x13

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getAlternative()Lcom/google/speech/patts/markup/Alternative;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 266
    :cond_12
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->hasForeignLanguage()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 267
    const/16 v0, 0x14

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Say;->getForeignLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 269
    :cond_13
    return-void
.end method
