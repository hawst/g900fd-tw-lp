.class public final Lcom/google/speech/patts/ling_utt/Utterance;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Utterance.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/ling_utt/Utterance$1;,
        Lcom/google/speech/patts/ling_utt/Utterance$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/ling_utt/Utterance;


# instance fields
.field private hasName:Z

.field private hasRuntimeId:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private runtimeId_:J

.field private streams_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/ling_utt/Stream;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 381
    new-instance v0, Lcom/google/speech/patts/ling_utt/Utterance;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/ling_utt/Utterance;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/ling_utt/Utterance;->defaultInstance:Lcom/google/speech/patts/ling_utt/Utterance;

    .line 382
    invoke-static {}, Lcom/google/speech/patts/ling_utt/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 383
    sget-object v0, Lcom/google/speech/patts/ling_utt/Utterance;->defaultInstance:Lcom/google/speech/patts/ling_utt/Utterance;

    invoke-direct {v0}, Lcom/google/speech/patts/ling_utt/Utterance;->initFields()V

    .line 384
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->name_:Ljava/lang/String;

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;

    .line 44
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->runtimeId_:J

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/ling_utt/Utterance$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/ling_utt/Utterance$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/ling_utt/Utterance;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->name_:Ljava/lang/String;

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;

    .line 44
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->runtimeId_:J

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lcom/google/speech/patts/ling_utt/Utterance;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Utterance;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/patts/ling_utt/Utterance;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Utterance;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/ling_utt/Utterance;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Utterance;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Utterance;->hasName:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/ling_utt/Utterance;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Utterance;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Utterance;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/ling_utt/Utterance;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Utterance;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Utterance;->hasRuntimeId:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/ling_utt/Utterance;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Utterance;
    .param p1, "x1"    # J

    .prologue
    .line 5
    iput-wide p1, p0, Lcom/google/speech/patts/ling_utt/Utterance;->runtimeId_:J

    return-wide p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/ling_utt/Utterance;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/ling_utt/Utterance;->defaultInstance:Lcom/google/speech/patts/ling_utt/Utterance;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/ling_utt/Utterance$Builder;
    .locals 1

    .prologue
    .line 161
    # invokes: Lcom/google/speech/patts/ling_utt/Utterance$Builder;->create()Lcom/google/speech/patts/ling_utt/Utterance$Builder;
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->access$100()Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/ling_utt/Utterance;)Lcom/google/speech/patts/ling_utt/Utterance$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/ling_utt/Utterance;

    .prologue
    .line 164
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Utterance;->newBuilder()Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/ling_utt/Utterance$Builder;->mergeFrom(Lcom/google/speech/patts/ling_utt/Utterance;)Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->getDefaultInstanceForType()Lcom/google/speech/patts/ling_utt/Utterance;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/ling_utt/Utterance;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/ling_utt/Utterance;->defaultInstance:Lcom/google/speech/patts/ling_utt/Utterance;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getRuntimeId()J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->runtimeId_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    .line 74
    iget v2, p0, Lcom/google/speech/patts/ling_utt/Utterance;->memoizedSerializedSize:I

    .line 75
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 91
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 77
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 78
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->hasName()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 79
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 82
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->getStreamsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Stream;

    .line 83
    .local v0, "element":Lcom/google/speech/patts/ling_utt/Stream;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 85
    goto :goto_1

    .line 86
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Stream;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->hasRuntimeId()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 87
    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->getRuntimeId()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 90
    :cond_3
    iput v2, p0, Lcom/google/speech/patts/ling_utt/Utterance;->memoizedSerializedSize:I

    move v3, v2

    .line 91
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getStreamsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/ling_utt/Stream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->streams_:Ljava/util/List;

    return-object v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->hasName:Z

    return v0
.end method

.method public hasRuntimeId()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Utterance;->hasRuntimeId:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 51
    iget-boolean v3, p0, Lcom/google/speech/patts/ling_utt/Utterance;->hasName:Z

    if-nez v3, :cond_0

    .line 55
    :goto_0
    return v2

    .line 52
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->getStreamsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Stream;

    .line 53
    .local v0, "element":Lcom/google/speech/patts/ling_utt/Stream;
    invoke-virtual {v0}, Lcom/google/speech/patts/ling_utt/Stream;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 55
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Stream;
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->toBuilder()Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/ling_utt/Utterance$Builder;
    .locals 1

    .prologue
    .line 166
    invoke-static {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->newBuilder(Lcom/google/speech/patts/ling_utt/Utterance;)Lcom/google/speech/patts/ling_utt/Utterance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->getSerializedSize()I

    .line 61
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->hasName()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 64
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->getStreamsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Stream;

    .line 65
    .local v0, "element":Lcom/google/speech/patts/ling_utt/Stream;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 67
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Stream;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->hasRuntimeId()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 68
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Utterance;->getRuntimeId()J

    move-result-wide v4

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 70
    :cond_2
    return-void
.end method
