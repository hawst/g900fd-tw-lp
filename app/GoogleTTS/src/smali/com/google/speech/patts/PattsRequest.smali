.class public final Lcom/google/speech/patts/PattsRequest;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PattsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/PattsRequest$1;,
        Lcom/google/speech/patts/PattsRequest$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/PattsRequest;


# instance fields
.field private hasSentence:Z

.field private hasTtsSentence:Z

.field private memoizedSerializedSize:I

.field private sentence_:Lcom/google/speech/patts/markup/Sentence;

.field private ttsSentence_:Lcom/google/speech/tts/Sentence;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 344
    new-instance v0, Lcom/google/speech/patts/PattsRequest;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/PattsRequest;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/PattsRequest;->defaultInstance:Lcom/google/speech/patts/PattsRequest;

    .line 345
    invoke-static {}, Lcom/google/speech/patts/PattsRequestProto;->internalForceInit()V

    .line 346
    sget-object v0, Lcom/google/speech/patts/PattsRequest;->defaultInstance:Lcom/google/speech/patts/PattsRequest;

    invoke-direct {v0}, Lcom/google/speech/patts/PattsRequest;->initFields()V

    .line 347
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/PattsRequest;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/PattsRequest;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/PattsRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/PattsRequest$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/PattsRequest;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/PattsRequest;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/patts/PattsRequest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/PattsRequest;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/PattsRequest;->hasSentence:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/speech/patts/PattsRequest;)Lcom/google/speech/patts/markup/Sentence;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/PattsRequest;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest;->sentence_:Lcom/google/speech/patts/markup/Sentence;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/speech/patts/PattsRequest;Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Sentence;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/PattsRequest;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/Sentence;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/PattsRequest;->sentence_:Lcom/google/speech/patts/markup/Sentence;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/PattsRequest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/PattsRequest;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/PattsRequest;->hasTtsSentence:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/speech/patts/PattsRequest;)Lcom/google/speech/tts/Sentence;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/PattsRequest;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest;->ttsSentence_:Lcom/google/speech/tts/Sentence;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/speech/patts/PattsRequest;Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/PattsRequest;
    .param p1, "x1"    # Lcom/google/speech/tts/Sentence;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/PattsRequest;->ttsSentence_:Lcom/google/speech/tts/Sentence;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/PattsRequest;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/PattsRequest;->defaultInstance:Lcom/google/speech/patts/PattsRequest;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Lcom/google/speech/patts/markup/Sentence;->getDefaultInstance()Lcom/google/speech/patts/markup/Sentence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/PattsRequest;->sentence_:Lcom/google/speech/patts/markup/Sentence;

    .line 38
    invoke-static {}, Lcom/google/speech/tts/Sentence;->getDefaultInstance()Lcom/google/speech/tts/Sentence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/PattsRequest;->ttsSentence_:Lcom/google/speech/tts/Sentence;

    .line 39
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/PattsRequest$Builder;
    .locals 1

    .prologue
    .line 146
    # invokes: Lcom/google/speech/patts/PattsRequest$Builder;->create()Lcom/google/speech/patts/PattsRequest$Builder;
    invoke-static {}, Lcom/google/speech/patts/PattsRequest$Builder;->access$100()Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/PattsRequest;)Lcom/google/speech/patts/PattsRequest$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/PattsRequest;

    .prologue
    .line 149
    invoke-static {}, Lcom/google/speech/patts/PattsRequest;->newBuilder()Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/PattsRequest$Builder;->mergeFrom(Lcom/google/speech/patts/PattsRequest;)Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->getDefaultInstanceForType()Lcom/google/speech/patts/PattsRequest;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/PattsRequest;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/PattsRequest;->defaultInstance:Lcom/google/speech/patts/PattsRequest;

    return-object v0
.end method

.method public getSentence()Lcom/google/speech/patts/markup/Sentence;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest;->sentence_:Lcom/google/speech/patts/markup/Sentence;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 63
    iget v0, p0, Lcom/google/speech/patts/PattsRequest;->memoizedSerializedSize:I

    .line 64
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 76
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 66
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->hasSentence()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 68
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->getSentence()Lcom/google/speech/patts/markup/Sentence;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 71
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->hasTtsSentence()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 72
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->getTtsSentence()Lcom/google/speech/tts/Sentence;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 75
    :cond_2
    iput v0, p0, Lcom/google/speech/patts/PattsRequest;->memoizedSerializedSize:I

    move v1, v0

    .line 76
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getTtsSentence()Lcom/google/speech/tts/Sentence;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/patts/PattsRequest;->ttsSentence_:Lcom/google/speech/tts/Sentence;

    return-object v0
.end method

.method public hasSentence()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/PattsRequest;->hasSentence:Z

    return v0
.end method

.method public hasTtsSentence()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/PattsRequest;->hasTtsSentence:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->hasSentence()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 42
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->getSentence()Lcom/google/speech/patts/markup/Sentence;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Sentence;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 47
    :cond_0
    :goto_0
    return v0

    .line 44
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->hasTtsSentence()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 45
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->getTtsSentence()Lcom/google/speech/tts/Sentence;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Sentence;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->toBuilder()Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/PattsRequest$Builder;
    .locals 1

    .prologue
    .line 151
    invoke-static {p0}, Lcom/google/speech/patts/PattsRequest;->newBuilder(Lcom/google/speech/patts/PattsRequest;)Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->getSerializedSize()I

    .line 53
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->hasSentence()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->getSentence()Lcom/google/speech/patts/markup/Sentence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 56
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->hasTtsSentence()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/PattsRequest;->getTtsSentence()Lcom/google/speech/tts/Sentence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 59
    :cond_1
    return-void
.end method
