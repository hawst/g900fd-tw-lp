.class public final Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SpeechMorphingTransformPalette.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;",
        "Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;
    .locals 1

    .prologue
    .line 153
    invoke-static {}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->create()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;
    .locals 3

    .prologue
    .line 162
    new-instance v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;-><init>()V

    .line 163
    .local v0, "builder":Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;
    new-instance v1, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;-><init>(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    .line 164
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->build()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    invoke-static {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 195
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->buildPartial()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;
    .locals 3

    .prologue
    .line 208
    iget-object v1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    if-nez v1, :cond_0

    .line 209
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 212
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    # getter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->access$300(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 213
    iget-object v1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    iget-object v2, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    # getter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->access$300(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->access$302(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;Ljava/util/List;)Ljava/util/List;

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    .line 217
    .local v0, "returnMe":Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    .line 218
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->clone()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->clone()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;
    .locals 2

    .prologue
    .line 181
    invoke-static {}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->create()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->clone()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    invoke-virtual {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 153
    check-cast p1, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    .prologue
    .line 222
    invoke-static {}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->getDefaultInstance()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-object p0

    .line 223
    :cond_1
    # getter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->access$300(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 224
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    # getter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->access$300(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 225
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->access$302(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;Ljava/util/List;)Ljava/util/List;

    .line 227
    :cond_2
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    # getter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->access$300(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->access$300(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 229
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->hasDefaultSpeakerReference()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->getDefaultSpeakerReference()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->setDefaultSpeakerReference(Ljava/lang/String;)Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    goto :goto_0
.end method

.method public setDefaultSpeakerReference(Ljava/lang/String;)Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 324
    if-nez p1, :cond_0

    .line 325
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->hasDefaultSpeakerReference:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->access$402(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;Z)Z

    .line 328
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    # setter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->defaultSpeakerReference_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->access$502(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;Ljava/lang/String;)Ljava/lang/String;

    .line 329
    return-object p0
.end method
