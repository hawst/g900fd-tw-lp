.class public final Lcom/google/speech/patts/Phoneme$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
.source "Phoneme.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/Phoneme;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
        "<",
        "Lcom/google/speech/patts/Phoneme;",
        "Lcom/google/speech/patts/Phoneme$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/Phoneme;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/Phoneme$Builder;
    .locals 1

    .prologue
    .line 257
    invoke-static {}, Lcom/google/speech/patts/Phoneme$Builder;->create()Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/Phoneme$Builder;
    .locals 3

    .prologue
    .line 266
    new-instance v0, Lcom/google/speech/patts/Phoneme$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/Phoneme$Builder;-><init>()V

    .line 267
    .local v0, "builder":Lcom/google/speech/patts/Phoneme$Builder;
    new-instance v1, Lcom/google/speech/patts/Phoneme;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/Phoneme;-><init>(Lcom/google/speech/patts/Phoneme$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    .line 268
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme$Builder;->build()Lcom/google/speech/patts/Phoneme;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/Phoneme;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    invoke-static {v0}, Lcom/google/speech/patts/Phoneme$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 299
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme$Builder;->buildPartial()Lcom/google/speech/patts/Phoneme;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/Phoneme;
    .locals 3

    .prologue
    .line 312
    iget-object v1, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    if-nez v1, :cond_0

    .line 313
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 316
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # getter for: Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/Phoneme;->access$300(Lcom/google/speech/patts/Phoneme;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 317
    iget-object v1, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    iget-object v2, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # getter for: Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/Phoneme;->access$300(Lcom/google/speech/patts/Phoneme;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/Phoneme;->access$302(Lcom/google/speech/patts/Phoneme;Ljava/util/List;)Ljava/util/List;

    .line 320
    :cond_1
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    .line 321
    .local v0, "returnMe":Lcom/google/speech/patts/Phoneme;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    .line 322
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme$Builder;->clone()Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme$Builder;->clone()Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme$Builder;->clone()Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/Phoneme$Builder;
    .locals 2

    .prologue
    .line 285
    invoke-static {}, Lcom/google/speech/patts/Phoneme$Builder;->create()Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/Phoneme$Builder;->mergeFrom(Lcom/google/speech/patts/Phoneme;)Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme$Builder;->clone()Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic internalGetResult()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme$Builder;->internalGetResult()Lcom/google/speech/patts/Phoneme;

    move-result-object v0

    return-object v0
.end method

.method protected internalGetResult()Lcom/google/speech/patts/Phoneme;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    invoke-virtual {v0}, Lcom/google/speech/patts/Phoneme;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 257
    check-cast p1, Lcom/google/speech/patts/Phoneme;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/Phoneme$Builder;->mergeFrom(Lcom/google/speech/patts/Phoneme;)Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/Phoneme;)Lcom/google/speech/patts/Phoneme$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/Phoneme;

    .prologue
    .line 326
    invoke-static {}, Lcom/google/speech/patts/Phoneme;->getDefaultInstance()Lcom/google/speech/patts/Phoneme;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 358
    :goto_0
    return-object p0

    .line 327
    :cond_0
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->hasParent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 328
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->getParent()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Phoneme$Builder;->setParent(I)Lcom/google/speech/patts/Phoneme$Builder;

    .line 330
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->hasFirstDaughter()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 331
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->getFirstDaughter()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Phoneme$Builder;->setFirstDaughter(I)Lcom/google/speech/patts/Phoneme$Builder;

    .line 333
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->hasLastDaughter()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 334
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->getLastDaughter()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Phoneme$Builder;->setLastDaughter(I)Lcom/google/speech/patts/Phoneme$Builder;

    .line 336
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->hasName()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 337
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Phoneme$Builder;->setName(Ljava/lang/String;)Lcom/google/speech/patts/Phoneme$Builder;

    .line 339
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->hasVowel()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 340
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->getVowel()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Phoneme$Builder;->setVowel(Z)Lcom/google/speech/patts/Phoneme$Builder;

    .line 342
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->hasStart()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 343
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->getStart()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Phoneme$Builder;->setStart(F)Lcom/google/speech/patts/Phoneme$Builder;

    .line 345
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->hasEnd()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 346
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->getEnd()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Phoneme$Builder;->setEnd(F)Lcom/google/speech/patts/Phoneme$Builder;

    .line 348
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->hasStats()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 349
    invoke-virtual {p1}, Lcom/google/speech/patts/Phoneme;->getStats()Lspeech/patts/PhoneStats;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Phoneme$Builder;->mergeStats(Lspeech/patts/PhoneStats;)Lcom/google/speech/patts/Phoneme$Builder;

    .line 351
    :cond_8
    # getter for: Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/Phoneme;->access$300(Lcom/google/speech/patts/Phoneme;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 352
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # getter for: Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/Phoneme;->access$300(Lcom/google/speech/patts/Phoneme;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 353
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/Phoneme;->access$302(Lcom/google/speech/patts/Phoneme;Ljava/util/List;)Ljava/util/List;

    .line 355
    :cond_9
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # getter for: Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/Phoneme;->access$300(Lcom/google/speech/patts/Phoneme;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/Phoneme;->access$300(Lcom/google/speech/patts/Phoneme;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 357
    :cond_a
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/Phoneme$Builder;->mergeExtensionFields(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;)V

    goto/16 :goto_0
.end method

.method public mergeStats(Lspeech/patts/PhoneStats;)Lcom/google/speech/patts/Phoneme$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/PhoneStats;

    .prologue
    .line 581
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    invoke-virtual {v0}, Lcom/google/speech/patts/Phoneme;->hasStats()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # getter for: Lcom/google/speech/patts/Phoneme;->stats_:Lspeech/patts/PhoneStats;
    invoke-static {v0}, Lcom/google/speech/patts/Phoneme;->access$1900(Lcom/google/speech/patts/Phoneme;)Lspeech/patts/PhoneStats;

    move-result-object v0

    invoke-static {}, Lspeech/patts/PhoneStats;->getDefaultInstance()Lspeech/patts/PhoneStats;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 583
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    iget-object v1, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # getter for: Lcom/google/speech/patts/Phoneme;->stats_:Lspeech/patts/PhoneStats;
    invoke-static {v1}, Lcom/google/speech/patts/Phoneme;->access$1900(Lcom/google/speech/patts/Phoneme;)Lspeech/patts/PhoneStats;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/PhoneStats;->newBuilder(Lspeech/patts/PhoneStats;)Lspeech/patts/PhoneStats$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/PhoneStats$Builder;->mergeFrom(Lspeech/patts/PhoneStats;)Lspeech/patts/PhoneStats$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/PhoneStats$Builder;->buildPartial()Lspeech/patts/PhoneStats;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/Phoneme;->stats_:Lspeech/patts/PhoneStats;
    invoke-static {v0, v1}, Lcom/google/speech/patts/Phoneme;->access$1902(Lcom/google/speech/patts/Phoneme;Lspeech/patts/PhoneStats;)Lspeech/patts/PhoneStats;

    .line 588
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Phoneme;->hasStats:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Phoneme;->access$1802(Lcom/google/speech/patts/Phoneme;Z)Z

    .line 589
    return-object p0

    .line 586
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # setter for: Lcom/google/speech/patts/Phoneme;->stats_:Lspeech/patts/PhoneStats;
    invoke-static {v0, p1}, Lcom/google/speech/patts/Phoneme;->access$1902(Lcom/google/speech/patts/Phoneme;Lspeech/patts/PhoneStats;)Lspeech/patts/PhoneStats;

    goto :goto_0
.end method

.method public setEnd(F)Lcom/google/speech/patts/Phoneme$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Phoneme;->hasEnd:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Phoneme;->access$1602(Lcom/google/speech/patts/Phoneme;Z)Z

    .line 551
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # setter for: Lcom/google/speech/patts/Phoneme;->end_:F
    invoke-static {v0, p1}, Lcom/google/speech/patts/Phoneme;->access$1702(Lcom/google/speech/patts/Phoneme;F)F

    .line 552
    return-object p0
.end method

.method public setFirstDaughter(I)Lcom/google/speech/patts/Phoneme$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Phoneme;->hasFirstDaughter:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Phoneme;->access$602(Lcom/google/speech/patts/Phoneme;Z)Z

    .line 458
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # setter for: Lcom/google/speech/patts/Phoneme;->firstDaughter_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Phoneme;->access$702(Lcom/google/speech/patts/Phoneme;I)I

    .line 459
    return-object p0
.end method

.method public setLastDaughter(I)Lcom/google/speech/patts/Phoneme$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Phoneme;->hasLastDaughter:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Phoneme;->access$802(Lcom/google/speech/patts/Phoneme;Z)Z

    .line 476
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # setter for: Lcom/google/speech/patts/Phoneme;->lastDaughter_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Phoneme;->access$902(Lcom/google/speech/patts/Phoneme;I)I

    .line 477
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/patts/Phoneme$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 493
    if-nez p1, :cond_0

    .line 494
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Phoneme;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Phoneme;->access$1002(Lcom/google/speech/patts/Phoneme;Z)Z

    .line 497
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # setter for: Lcom/google/speech/patts/Phoneme;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/Phoneme;->access$1102(Lcom/google/speech/patts/Phoneme;Ljava/lang/String;)Ljava/lang/String;

    .line 498
    return-object p0
.end method

.method public setParent(I)Lcom/google/speech/patts/Phoneme$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Phoneme;->hasParent:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Phoneme;->access$402(Lcom/google/speech/patts/Phoneme;Z)Z

    .line 440
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # setter for: Lcom/google/speech/patts/Phoneme;->parent_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Phoneme;->access$502(Lcom/google/speech/patts/Phoneme;I)I

    .line 441
    return-object p0
.end method

.method public setStart(F)Lcom/google/speech/patts/Phoneme$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Phoneme;->hasStart:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Phoneme;->access$1402(Lcom/google/speech/patts/Phoneme;Z)Z

    .line 533
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # setter for: Lcom/google/speech/patts/Phoneme;->start_:F
    invoke-static {v0, p1}, Lcom/google/speech/patts/Phoneme;->access$1502(Lcom/google/speech/patts/Phoneme;F)F

    .line 534
    return-object p0
.end method

.method public setVowel(Z)Lcom/google/speech/patts/Phoneme$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 514
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Phoneme;->hasVowel:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Phoneme;->access$1202(Lcom/google/speech/patts/Phoneme;Z)Z

    .line 515
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme$Builder;->result:Lcom/google/speech/patts/Phoneme;

    # setter for: Lcom/google/speech/patts/Phoneme;->vowel_:Z
    invoke-static {v0, p1}, Lcom/google/speech/patts/Phoneme;->access$1302(Lcom/google/speech/patts/Phoneme;Z)Z

    .line 516
    return-object p0
.end method
