.class public final Lcom/google/speech/patts/markup/Measure$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Measure.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Measure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/markup/Measure;",
        "Lcom/google/speech/patts/markup/Measure$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/markup/Measure;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/markup/Measure$Builder;
    .locals 1

    .prologue
    .line 189
    invoke-static {}, Lcom/google/speech/patts/markup/Measure$Builder;->create()Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/markup/Measure$Builder;
    .locals 3

    .prologue
    .line 198
    new-instance v0, Lcom/google/speech/patts/markup/Measure$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Measure$Builder;-><init>()V

    .line 199
    .local v0, "builder":Lcom/google/speech/patts/markup/Measure$Builder;
    new-instance v1, Lcom/google/speech/patts/markup/Measure;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/markup/Measure;-><init>(Lcom/google/speech/patts/markup/Measure$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    .line 200
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure$Builder;->build()Lcom/google/speech/patts/markup/Measure;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/markup/Measure;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    invoke-static {v0}, Lcom/google/speech/patts/markup/Measure$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 231
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure$Builder;->buildPartial()Lcom/google/speech/patts/markup/Measure;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/markup/Measure;
    .locals 3

    .prologue
    .line 244
    iget-object v1, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    if-nez v1, :cond_0

    .line 245
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    .line 249
    .local v0, "returnMe":Lcom/google/speech/patts/markup/Measure;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    .line 250
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure$Builder;->clone()Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure$Builder;->clone()Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/markup/Measure$Builder;
    .locals 2

    .prologue
    .line 217
    invoke-static {}, Lcom/google/speech/patts/markup/Measure$Builder;->create()Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/markup/Measure$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure$Builder;->clone()Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Measure;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 189
    check-cast p1, Lcom/google/speech/patts/markup/Measure;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/markup/Measure$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Measure$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/patts/markup/Measure;

    .prologue
    .line 254
    invoke-static {}, Lcom/google/speech/patts/markup/Measure;->getDefaultInstance()Lcom/google/speech/patts/markup/Measure;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 270
    :cond_0
    :goto_0
    return-object p0

    .line 255
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Measure;->hasNumber()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Measure;->getNumber()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Measure$Builder;->setNumber(I)Lcom/google/speech/patts/markup/Measure$Builder;

    .line 258
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Measure;->hasUnits()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 259
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Measure;->getUnits()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Measure$Builder;->setUnits(Ljava/lang/String;)Lcom/google/speech/patts/markup/Measure$Builder;

    .line 261
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Measure;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 262
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Measure;->getFraction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Measure$Builder;->setFraction(Ljava/lang/String;)Lcom/google/speech/patts/markup/Measure$Builder;

    .line 264
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Measure;->hasUnitsPrefix()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 265
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Measure;->getUnitsPrefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Measure$Builder;->setUnitsPrefix(Ljava/lang/String;)Lcom/google/speech/patts/markup/Measure$Builder;

    .line 267
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Measure;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Measure;->getStyle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Measure$Builder;->setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Measure$Builder;

    goto :goto_0
.end method

.method public setFraction(Ljava/lang/String;)Lcom/google/speech/patts/markup/Measure$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 360
    if-nez p1, :cond_0

    .line 361
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Measure;->hasFraction:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Measure;->access$702(Lcom/google/speech/patts/markup/Measure;Z)Z

    .line 364
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    # setter for: Lcom/google/speech/patts/markup/Measure;->fraction_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Measure;->access$802(Lcom/google/speech/patts/markup/Measure;Ljava/lang/String;)Ljava/lang/String;

    .line 365
    return-object p0
.end method

.method public setNumber(I)Lcom/google/speech/patts/markup/Measure$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Measure;->hasNumber:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Measure;->access$302(Lcom/google/speech/patts/markup/Measure;Z)Z

    .line 322
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    # setter for: Lcom/google/speech/patts/markup/Measure;->number_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Measure;->access$402(Lcom/google/speech/patts/markup/Measure;I)I

    .line 323
    return-object p0
.end method

.method public setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Measure$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 402
    if-nez p1, :cond_0

    .line 403
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Measure;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Measure;->access$1102(Lcom/google/speech/patts/markup/Measure;Z)Z

    .line 406
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    # setter for: Lcom/google/speech/patts/markup/Measure;->style_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Measure;->access$1202(Lcom/google/speech/patts/markup/Measure;Ljava/lang/String;)Ljava/lang/String;

    .line 407
    return-object p0
.end method

.method public setUnits(Ljava/lang/String;)Lcom/google/speech/patts/markup/Measure$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 339
    if-nez p1, :cond_0

    .line 340
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Measure;->hasUnits:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Measure;->access$502(Lcom/google/speech/patts/markup/Measure;Z)Z

    .line 343
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    # setter for: Lcom/google/speech/patts/markup/Measure;->units_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Measure;->access$602(Lcom/google/speech/patts/markup/Measure;Ljava/lang/String;)Ljava/lang/String;

    .line 344
    return-object p0
.end method

.method public setUnitsPrefix(Ljava/lang/String;)Lcom/google/speech/patts/markup/Measure$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 381
    if-nez p1, :cond_0

    .line 382
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Measure;->hasUnitsPrefix:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Measure;->access$902(Lcom/google/speech/patts/markup/Measure;Z)Z

    .line 385
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure$Builder;->result:Lcom/google/speech/patts/markup/Measure;

    # setter for: Lcom/google/speech/patts/markup/Measure;->unitsPrefix_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Measure;->access$1002(Lcom/google/speech/patts/markup/Measure;Ljava/lang/String;)Ljava/lang/String;

    .line 386
    return-object p0
.end method
