.class public final Lcom/google/speech/patts/markup/Decimal;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Decimal.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/markup/Decimal$1;,
        Lcom/google/speech/patts/markup/Decimal$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/markup/Decimal;


# instance fields
.field private fraction_:Ljava/lang/String;

.field private hasFraction:Z

.field private hasNegative:Z

.field private hasNumber:Z

.field private hasStyle:Z

.field private memoizedSerializedSize:I

.field private negative_:Z

.field private number_:J

.field private style_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 373
    new-instance v0, Lcom/google/speech/patts/markup/Decimal;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/markup/Decimal;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/markup/Decimal;->defaultInstance:Lcom/google/speech/patts/markup/Decimal;

    .line 374
    invoke-static {}, Lcom/google/speech/patts/markup/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 375
    sget-object v0, Lcom/google/speech/patts/markup/Decimal;->defaultInstance:Lcom/google/speech/patts/markup/Decimal;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Decimal;->initFields()V

    .line 376
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/patts/markup/Decimal;->number_:J

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Decimal;->fraction_:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/speech/patts/markup/Decimal;->negative_:Z

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Decimal;->style_:Ljava/lang/String;

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Decimal;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Decimal;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/markup/Decimal$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/markup/Decimal$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Decimal;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/patts/markup/Decimal;->number_:J

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Decimal;->fraction_:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/speech/patts/markup/Decimal;->negative_:Z

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Decimal;->style_:Ljava/lang/String;

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Decimal;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/markup/Decimal;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Decimal;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Decimal;->style_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/speech/patts/markup/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Decimal;->hasNumber:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/markup/Decimal;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Decimal;
    .param p1, "x1"    # J

    .prologue
    .line 5
    iput-wide p1, p0, Lcom/google/speech/patts/markup/Decimal;->number_:J

    return-wide p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/markup/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Decimal;->hasFraction:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/markup/Decimal;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Decimal;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Decimal;->fraction_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/markup/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Decimal;->hasNegative:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/markup/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Decimal;->negative_:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/markup/Decimal;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Decimal;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Decimal;->hasStyle:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/markup/Decimal;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/markup/Decimal;->defaultInstance:Lcom/google/speech/patts/markup/Decimal;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/markup/Decimal$Builder;
    .locals 1

    .prologue
    .line 167
    # invokes: Lcom/google/speech/patts/markup/Decimal$Builder;->create()Lcom/google/speech/patts/markup/Decimal$Builder;
    invoke-static {}, Lcom/google/speech/patts/markup/Decimal$Builder;->access$100()Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/markup/Decimal;

    .prologue
    .line 170
    invoke-static {}, Lcom/google/speech/patts/markup/Decimal;->newBuilder()Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/markup/Decimal$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->getDefaultInstanceForType()Lcom/google/speech/patts/markup/Decimal;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/markup/Decimal;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/markup/Decimal;->defaultInstance:Lcom/google/speech/patts/markup/Decimal;

    return-object v0
.end method

.method public getFraction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal;->fraction_:Ljava/lang/String;

    return-object v0
.end method

.method public getNegative()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Decimal;->negative_:Z

    return v0
.end method

.method public getNumber()J
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, Lcom/google/speech/patts/markup/Decimal;->number_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 76
    iget v0, p0, Lcom/google/speech/patts/markup/Decimal;->memoizedSerializedSize:I

    .line 77
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 97
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 79
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 80
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->hasNumber()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 81
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->getNumber()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 84
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->hasFraction()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 85
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->getFraction()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 88
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->hasNegative()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 89
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->getNegative()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 92
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 93
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->getStyle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 96
    :cond_4
    iput v0, p0, Lcom/google/speech/patts/markup/Decimal;->memoizedSerializedSize:I

    move v1, v0

    .line 97
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/patts/markup/Decimal;->style_:Ljava/lang/String;

    return-object v0
.end method

.method public hasFraction()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Decimal;->hasFraction:Z

    return v0
.end method

.method public hasNegative()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Decimal;->hasNegative:Z

    return v0
.end method

.method public hasNumber()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Decimal;->hasNumber:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Decimal;->hasStyle:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Decimal;->hasNumber:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 54
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->toBuilder()Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/markup/Decimal$Builder;
    .locals 1

    .prologue
    .line 172
    invoke-static {p0}, Lcom/google/speech/patts/markup/Decimal;->newBuilder(Lcom/google/speech/patts/markup/Decimal;)Lcom/google/speech/patts/markup/Decimal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->getSerializedSize()I

    .line 60
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->hasNumber()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->getNumber()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    .line 63
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->getFraction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 66
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->hasNegative()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 67
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->getNegative()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 69
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 70
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Decimal;->getStyle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 72
    :cond_3
    return-void
.end method
