.class public final Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LinearTransformProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/hmm/LinearTransformProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/hmm/LinearTransformProto;",
        "Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/hmm/LinearTransformProto;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 488
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$500()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;
    .locals 1

    .prologue
    .line 482
    invoke-static {}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->create()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;
    .locals 3

    .prologue
    .line 491
    new-instance v0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;-><init>()V

    .line 492
    .local v0, "builder":Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;
    new-instance v1, Lcom/google/speech/patts/hmm/LinearTransformProto;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/hmm/LinearTransformProto;-><init>(Lcom/google/speech/patts/hmm/LinearTransformProto$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    .line 493
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->build()Lcom/google/speech/patts/hmm/LinearTransformProto;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/hmm/LinearTransformProto;
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 522
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    invoke-static {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 524
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->buildPartial()Lcom/google/speech/patts/hmm/LinearTransformProto;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/hmm/LinearTransformProto;
    .locals 3

    .prologue
    .line 537
    iget-object v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    if-nez v1, :cond_0

    .line 538
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 541
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$700(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 542
    iget-object v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    iget-object v2, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$700(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$702(Lcom/google/speech/patts/hmm/LinearTransformProto;Ljava/util/List;)Ljava/util/List;

    .line 545
    :cond_1
    iget-object v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$800(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 546
    iget-object v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    iget-object v2, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$800(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$802(Lcom/google/speech/patts/hmm/LinearTransformProto;Ljava/util/List;)Ljava/util/List;

    .line 549
    :cond_2
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    .line 550
    .local v0, "returnMe":Lcom/google/speech/patts/hmm/LinearTransformProto;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    .line 551
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->clone()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->clone()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;
    .locals 2

    .prologue
    .line 510
    invoke-static {}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->create()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/LinearTransformProto;)Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->clone()Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    invoke-virtual {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 482
    check-cast p1, Lcom/google/speech/patts/hmm/LinearTransformProto;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/LinearTransformProto;)Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/hmm/LinearTransformProto;)Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/hmm/LinearTransformProto;

    .prologue
    .line 555
    invoke-static {}, Lcom/google/speech/patts/hmm/LinearTransformProto;->getDefaultInstance()Lcom/google/speech/patts/hmm/LinearTransformProto;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 571
    :cond_0
    :goto_0
    return-object p0

    .line 556
    :cond_1
    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$700(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 557
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$700(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 558
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$702(Lcom/google/speech/patts/hmm/LinearTransformProto;Ljava/util/List;)Ljava/util/List;

    .line 560
    :cond_2
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$700(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->blocks_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$700(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 562
    :cond_3
    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$800(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 563
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$800(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 564
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$802(Lcom/google/speech/patts/hmm/LinearTransformProto;Ljava/util/List;)Ljava/util/List;

    .line 566
    :cond_4
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$800(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->bias_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$800(Lcom/google/speech/patts/hmm/LinearTransformProto;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 568
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/LinearTransformProto;->hasDet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/LinearTransformProto;->getDet()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->setDet(F)Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;

    goto :goto_0
.end method

.method public setDet(F)Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 710
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->hasDet:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$902(Lcom/google/speech/patts/hmm/LinearTransformProto;Z)Z

    .line 711
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto;

    # setter for: Lcom/google/speech/patts/hmm/LinearTransformProto;->det_:F
    invoke-static {v0, p1}, Lcom/google/speech/patts/hmm/LinearTransformProto;->access$1002(Lcom/google/speech/patts/hmm/LinearTransformProto;F)F

    .line 712
    return-object p0
.end method
