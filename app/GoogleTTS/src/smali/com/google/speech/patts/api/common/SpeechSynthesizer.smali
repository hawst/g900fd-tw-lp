.class public abstract Lcom/google/speech/patts/api/common/SpeechSynthesizer;
.super Ljava/lang/Object;
.source "SpeechSynthesizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/api/common/SpeechSynthesizer$NonAudiableInputException;
    }
.end annotation


# instance fields
.field private numRequests:J

.field protected projectFileHandle:J

.field protected projectResourceHolderHandle:J

.field protected synthesizerHandle:J

.field private textNormEnabled:Z

.field protected textNormHandle:J


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    .line 119
    iput-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    .line 120
    iput-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    .line 121
    iput-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    .line 122
    iput-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->numRequests:J

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormEnabled:Z

    .line 124
    return-void
.end method

.method private appendFileBytes(Ljava/io/InputStream;Ljava/io/ByteArrayOutputStream;)V
    .locals 3
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "output"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 567
    const/16 v2, 0x800

    new-array v1, v2, [B

    .line 568
    .local v1, "readBuffer":[B
    const/4 v0, 0x0

    .line 569
    .local v0, "bytesRead":I
    :goto_0
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    if-lez v0, :cond_0

    .line 570
    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 572
    :cond_0
    return-void
.end method

.method private initBufferedSynthesisImpl(J)Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;
    .locals 9
    .param p1, "hInOut"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/speech/patts/api/common/SpeechSynthesizer$NonAudiableInputException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 430
    iget-wide v4, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-static {v4, v5, p1, p2}, Lcom/google/speech/patts/engine/api/SynthesizerApi;->wordIdsToUnits(JJ)Z

    move-result v3

    if-nez v3, :cond_0

    .line 431
    const-string v3, "patts"

    const-string v4, "Failed to synthesize"

    invoke-virtual {p0, v3, v4}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    :goto_0
    return-object v2

    .line 435
    :cond_0
    iget-wide v4, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-static {v4, v5, p1, p2}, Lcom/google/speech/patts/engine/api/SynthesizerApi;->isAudiable(JJ)Z

    move-result v3

    if-nez v3, :cond_1

    .line 436
    const-string v3, "patts"

    const-string v4, "Is not audiable (output is only silence)"

    invoke-virtual {p0, v3, v4}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    new-instance v3, Lcom/google/speech/patts/api/common/SpeechSynthesizer$NonAudiableInputException;

    invoke-direct {v3}, Lcom/google/speech/patts/api/common/SpeechSynthesizer$NonAudiableInputException;-><init>()V

    throw v3

    .line 440
    :cond_1
    iget-wide v4, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-static {v4, v5, p1, p2}, Lcom/google/speech/patts/engine/api/SynthesizerApi;->initBufferedSynthesis(JJ)J

    move-result-wide v0

    .line 442
    .local v0, "hState":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 443
    const-string v3, "patts"

    const-string v4, "Failed to inialize buffered synthesizer"

    invoke-virtual {p0, v3, v4}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 446
    :cond_2
    new-instance v2, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;

    iget-wide v4, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-direct {v2, v4, v5}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;-><init>(J)V

    .line 448
    .local v2, "synth":Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;
    iget-wide v4, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->numRequests:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->numRequests:J

    invoke-virtual {v2, v4, v5}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setId(J)V

    .line 449
    invoke-virtual {v2, p1, p2}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setOutputHandle(J)V

    .line 450
    invoke-virtual {v2, v0, v1}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setStateHandle(J)V

    .line 451
    sget-object v3, Lcom/google/speech/patts/engine/api/SynthesisStatus;->STATUS_INITIAL:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    invoke-virtual {v2, v3}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setStatus(Lcom/google/speech/patts/engine/api/SynthesisStatus;)V

    goto :goto_0
.end method

.method private prepareText(Lcom/google/speech/tts/Sentence;)J
    .locals 12
    .param p1, "sentence"    # Lcom/google/speech/tts/Sentence;

    .prologue
    const/4 v2, 0x0

    const-wide/16 v10, 0x0

    .line 492
    const-wide/16 v8, 0x0

    .line 493
    .local v8, "hOut":J
    iget-boolean v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormEnabled:Z

    if-nez v0, :cond_0

    .line 494
    invoke-virtual {p1, v2}, Lcom/google/speech/tts/Sentence;->getSay(I)Lcom/google/speech/tts/Say;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->getText()Ljava/lang/String;

    move-result-object v7

    .line 495
    .local v7, "text":Ljava/lang/String;
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-static {v0, v1, v7}, Lcom/google/speech/patts/engine/api/TextAnalysisApi;->wlStringToWordIds(JLjava/lang/String;)J

    move-result-wide v8

    cmp-long v0, v8, v10

    if-nez v0, :cond_3

    .line 497
    const-string v0, "patts"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to convert in-vocabulary (wl) words \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" to utterance"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v10

    .line 532
    .end local v7    # "text":Ljava/lang/String;
    :goto_0
    return-wide v0

    .line 505
    :cond_0
    invoke-virtual {p1}, Lcom/google/speech/tts/Sentence;->getSayCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p1, v2}, Lcom/google/speech/tts/Sentence;->getSay(I)Lcom/google/speech/tts/Say;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->hasText()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v2}, Lcom/google/speech/tts/Sentence;->getSay(I)Lcom/google/speech/tts/Say;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "say {"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 510
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, v0}, Lcom/google/speech/tts/Sentence;->getSay(I)Lcom/google/speech/tts/Say;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/speech/tts/Say;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    .line 511
    .local v4, "textualProtoBytes":[B
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    iget-wide v2, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    invoke-virtual {p1}, Lcom/google/speech/tts/Sentence;->toByteArray()[B

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/speech/patts/engine/api/TextAnalysisApi;->textSentenceTextualProtoV2ToWordIds(JJ[B[B)J
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    .line 516
    .end local v4    # "textualProtoBytes":[B
    :goto_1
    cmp-long v0, v8, v10

    if-nez v0, :cond_1

    .line 517
    const-string v0, "patts"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to normalize textual proto, falling back to literal interpretation of the text: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    :cond_1
    cmp-long v0, v8, v10

    if-nez v0, :cond_2

    .line 524
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    iget-wide v2, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    invoke-virtual {p1}, Lcom/google/speech/tts/Sentence;->toByteArray()[B

    move-result-object v5

    invoke-static {v0, v1, v2, v3, v5}, Lcom/google/speech/patts/engine/api/TextAnalysisApi;->textSentenceProtoV2ToWordIds(JJ[B)J

    move-result-wide v8

    .line 527
    :cond_2
    cmp-long v0, v8, v10

    if-nez v0, :cond_3

    .line 528
    const-string v0, "patts"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to normalize proto: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v10

    .line 529
    goto/16 :goto_0

    .line 513
    :catch_0
    move-exception v6

    .line 514
    .local v6, "e":Ljava/io/UnsupportedEncodingException;
    const-string v0, "patts"

    const-string v1, "Unsupported charset: UTF8"

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .end local v6    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_3
    move-wide v0, v8

    .line 532
    goto/16 :goto_0
.end method

.method private varargs readFileListToByteArray([Ljava/io/InputStream;)[B
    .locals 6
    .param p1, "streams"    # [Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 543
    array-length v5, p1

    if-nez v5, :cond_0

    .line 544
    const/4 v5, 0x0

    .line 555
    :goto_0
    return-object v5

    .line 547
    :cond_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    const v5, 0x8000

    invoke-direct {v3, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 549
    .local v3, "output":Ljava/io/ByteArrayOutputStream;
    move-object v0, p1

    .local v0, "arr$":[Ljava/io/InputStream;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 550
    .local v4, "stream":Ljava/io/InputStream;
    invoke-direct {p0, v4, v3}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->appendFileBytes(Ljava/io/InputStream;Ljava/io/ByteArrayOutputStream;)V

    .line 549
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 555
    .end local v4    # "stream":Ljava/io/InputStream;
    :cond_1
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    goto :goto_0
.end method


# virtual methods
.method public final getSampleRate()I
    .locals 2

    .prologue
    .line 389
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/SynthesizerApi;->getVoiceSampleRate(J)I

    move-result v0

    return v0
.end method

.method public initBufferedSynthesis(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;
    .locals 4
    .param p1, "sentence"    # Lcom/google/speech/tts/Sentence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/speech/patts/api/common/SpeechSynthesizer$NonAudiableInputException;
        }
    .end annotation

    .prologue
    .line 274
    invoke-direct {p0, p1}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->prepareText(Lcom/google/speech/tts/Sentence;)J

    move-result-wide v0

    .local v0, "hOut":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 275
    const-string v2, "patts"

    const-string v3, "Failed to prepare text"

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const/4 v2, 0x0

    .line 278
    :goto_0
    return-object v2

    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->initBufferedSynthesisImpl(J)Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;

    move-result-object v2

    goto :goto_0
.end method

.method public initialize(Ljava/io/InputStream;Ljava/lang/String;)Z
    .locals 12
    .param p1, "projectFile"    # Ljava/io/InputStream;
    .param p2, "projectPrefix"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const-wide/16 v10, 0x0

    const/4 v1, 0x0

    .line 136
    invoke-virtual {p0}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->loadJni()Z

    move-result v5

    if-nez v5, :cond_0

    .line 137
    const-string v4, "patts"

    const-string v5, "Failed to load JNI"

    invoke-virtual {p0, v4, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :goto_0
    return v1

    .line 142
    :cond_0
    const-wide/16 v2, -0x1

    .line 144
    .local v2, "projectFileResource":J
    const/4 v5, 0x1

    :try_start_0
    new-array v5, v5, [Ljava/io/InputStream;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-direct {p0, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->readFileListToByteArray([Ljava/io/InputStream;)[B

    move-result-object v5

    invoke-static {v5}, Lcom/google/speech/patts/engine/api/FileResourceApi;->memoryResourceFromBuffer([B)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 150
    cmp-long v5, v2, v10

    if-nez v5, :cond_1

    .line 151
    const-string v4, "patts"

    const-string v5, "Failed to read project file to memory buffer"

    invoke-virtual {p0, v4, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "patts"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to open project file: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 154
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    invoke-virtual {p0, v2, v3}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->initializeProject(J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    .line 155
    invoke-static {v2, v3}, Lcom/google/speech/patts/engine/api/FileResourceApi;->deleteFileResource(J)V

    .line 156
    iget-wide v6, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    cmp-long v5, v6, v10

    if-nez v5, :cond_2

    .line 157
    const-string v4, "patts"

    const-string v5, "Failed to initialize project file"

    invoke-virtual {p0, v4, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 160
    :cond_2
    if-eqz p2, :cond_3

    .line 161
    iget-wide v6, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    invoke-static {v6, v7, p2}, Lcom/google/speech/patts/engine/api/ProjectFileApi;->projectFileSetPrefix(JLjava/lang/String;)V

    .line 163
    :cond_3
    invoke-static {}, Lcom/google/speech/patts/engine/api/ProjectFileApi;->newProjectResourceHolder()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    .line 166
    iget-wide v6, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    iget-wide v8, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    invoke-static {v6, v7, v8, v9}, Lcom/google/speech/patts/engine/api/TextAnalysisApi;->createTextNormalizer(JJ)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    .line 168
    iget-wide v6, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    cmp-long v5, v6, v10

    if-nez v5, :cond_4

    .line 169
    const-string v4, "patts"

    const-string v5, "Failed to load and initialize text normalizer"

    invoke-virtual {p0, v4, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 172
    :cond_4
    iget-wide v6, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    iget-wide v8, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    invoke-static {v6, v7, v8, v9}, Lcom/google/speech/patts/engine/api/SynthesizerApi;->createSynthesizer(JJ)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    .line 174
    iget-wide v6, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    cmp-long v5, v6, v10

    if-nez v5, :cond_5

    .line 175
    const-string v4, "patts"

    const-string v5, "Failed to load and initialize synthesizer"

    invoke-virtual {p0, v4, v5}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v1, v4

    .line 178
    goto/16 :goto_0
.end method

.method protected abstract initializeProject(J)J
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 205
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 209
    const/4 v0, 0x1

    .line 211
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract loadJni()Z
.end method

.method protected abstract logError(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract logInfo(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public shutdown()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 366
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 367
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/TextAnalysisApi;->deleteTextNormalizer(J)V

    .line 368
    iput-wide v2, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->textNormHandle:J

    .line 370
    :cond_0
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 371
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/SynthesizerApi;->deleteSynthesizer(J)V

    .line 372
    iput-wide v2, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    .line 374
    :cond_1
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 375
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/ProjectFileApi;->deleteProjectFile(J)V

    .line 376
    iput-wide v2, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectFileHandle:J

    .line 378
    :cond_2
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 379
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/ProjectFileApi;->deleteProjectResourceHolder(J)V

    .line 381
    iput-wide v2, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->projectResourceHolderHandle:J

    .line 383
    :cond_3
    return-void
.end method

.method public synthesizeToDirectBuffer(Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;Ljava/nio/ByteBuffer;)I
    .locals 10
    .param p1, "synth"    # Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;
    .param p2, "audioBuf"    # Ljava/nio/ByteBuffer;

    .prologue
    const/4 v9, -0x1

    .line 330
    if-nez p1, :cond_0

    .line 331
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid buffered synthesis handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335
    :cond_0
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 336
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->synthesizerHandle:J

    invoke-virtual {p1}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->getOutputHandle()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->getStateHandle()J

    move-result-wide v5

    move-object v4, p2

    invoke-static/range {v0 .. v6}, Lcom/google/speech/patts/engine/api/SynthesizerApi;->waveGenerationToByteBuffer(JJLjava/nio/ByteBuffer;J)I

    move-result v7

    .line 340
    .local v7, "numSamplesReady":I
    invoke-virtual {p1}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->getStateHandle()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/CommonApi;->getSynthesisStatus(J)Lcom/google/speech/patts/engine/api/SynthesisStatus;

    move-result-object v8

    .line 342
    .local v8, "status":Lcom/google/speech/patts/engine/api/SynthesisStatus;
    sget-object v0, Lcom/google/speech/patts/engine/api/SynthesisStatus;->STATUS_FAILED:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    if-ne v8, v0, :cond_1

    .line 343
    const-string v0, "patts"

    const-string v1, "Buffered synthesis failed"

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    sget-object v0, Lcom/google/speech/patts/engine/api/SynthesisStatus;->STATUS_FAILED:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    invoke-virtual {p1, v0}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setStatus(Lcom/google/speech/patts/engine/api/SynthesisStatus;)V

    move v7, v9

    .line 359
    .end local v7    # "numSamplesReady":I
    :goto_0
    return v7

    .line 347
    .restart local v7    # "numSamplesReady":I
    :cond_1
    if-gez v7, :cond_2

    .line 348
    const-string v0, "patts"

    const-string v1, "Buffered synthesis failed: invalid number of samples returned"

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;->logError(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    sget-object v0, Lcom/google/speech/patts/engine/api/SynthesisStatus;->STATUS_FAILED:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    invoke-virtual {p1, v0}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setStatus(Lcom/google/speech/patts/engine/api/SynthesisStatus;)V

    move v7, v9

    .line 352
    goto :goto_0

    .line 354
    :cond_2
    invoke-virtual {p1, v8}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->setStatus(Lcom/google/speech/patts/engine/api/SynthesisStatus;)V

    .line 357
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 358
    invoke-virtual {p2, v7}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_0
.end method
