.class public final Lcom/google/speech/patts/markup/Measure;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Measure.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/markup/Measure$1;,
        Lcom/google/speech/patts/markup/Measure$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/markup/Measure;


# instance fields
.field private fraction_:Ljava/lang/String;

.field private hasFraction:Z

.field private hasNumber:Z

.field private hasStyle:Z

.field private hasUnits:Z

.field private hasUnitsPrefix:Z

.field private memoizedSerializedSize:I

.field private number_:I

.field private style_:Ljava/lang/String;

.field private unitsPrefix_:Ljava/lang/String;

.field private units_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 419
    new-instance v0, Lcom/google/speech/patts/markup/Measure;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/markup/Measure;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/markup/Measure;->defaultInstance:Lcom/google/speech/patts/markup/Measure;

    .line 420
    invoke-static {}, Lcom/google/speech/patts/markup/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 421
    sget-object v0, Lcom/google/speech/patts/markup/Measure;->defaultInstance:Lcom/google/speech/patts/markup/Measure;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Measure;->initFields()V

    .line 422
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/patts/markup/Measure;->number_:I

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Measure;->units_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Measure;->fraction_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Measure;->unitsPrefix_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Measure;->style_:Ljava/lang/String;

    .line 85
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Measure;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Measure;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/markup/Measure$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/markup/Measure$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Measure;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/patts/markup/Measure;->number_:I

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Measure;->units_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Measure;->fraction_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Measure;->unitsPrefix_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Measure;->style_:Ljava/lang/String;

    .line 85
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Measure;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/markup/Measure;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Measure;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Measure;->unitsPrefix_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/speech/patts/markup/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Measure;->hasStyle:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/patts/markup/Measure;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Measure;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Measure;->style_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/speech/patts/markup/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Measure;->hasNumber:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/markup/Measure;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Measure;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/markup/Measure;->number_:I

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/markup/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Measure;->hasUnits:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/markup/Measure;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Measure;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Measure;->units_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/markup/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Measure;->hasFraction:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/markup/Measure;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Measure;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Measure;->fraction_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/markup/Measure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Measure;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Measure;->hasUnitsPrefix:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/markup/Measure;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/markup/Measure;->defaultInstance:Lcom/google/speech/patts/markup/Measure;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/markup/Measure$Builder;
    .locals 1

    .prologue
    .line 182
    # invokes: Lcom/google/speech/patts/markup/Measure$Builder;->create()Lcom/google/speech/patts/markup/Measure$Builder;
    invoke-static {}, Lcom/google/speech/patts/markup/Measure$Builder;->access$100()Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Measure$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/markup/Measure;

    .prologue
    .line 185
    invoke-static {}, Lcom/google/speech/patts/markup/Measure;->newBuilder()Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/markup/Measure$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->getDefaultInstanceForType()Lcom/google/speech/patts/markup/Measure;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/markup/Measure;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/markup/Measure;->defaultInstance:Lcom/google/speech/patts/markup/Measure;

    return-object v0
.end method

.method public getFraction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure;->fraction_:Ljava/lang/String;

    return-object v0
.end method

.method public getNumber()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/patts/markup/Measure;->number_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 87
    iget v0, p0, Lcom/google/speech/patts/markup/Measure;->memoizedSerializedSize:I

    .line 88
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 112
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 90
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 91
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->hasNumber()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 92
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 95
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->hasUnits()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->getUnits()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 99
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->hasFraction()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 100
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->getFraction()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 103
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->hasUnitsPrefix()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 104
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->getUnitsPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 107
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 108
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->getStyle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 111
    :cond_5
    iput v0, p0, Lcom/google/speech/patts/markup/Measure;->memoizedSerializedSize:I

    move v1, v0

    .line 112
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure;->style_:Ljava/lang/String;

    return-object v0
.end method

.method public getUnits()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure;->units_:Ljava/lang/String;

    return-object v0
.end method

.method public getUnitsPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/patts/markup/Measure;->unitsPrefix_:Ljava/lang/String;

    return-object v0
.end method

.method public hasFraction()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Measure;->hasFraction:Z

    return v0
.end method

.method public hasNumber()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Measure;->hasNumber:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Measure;->hasStyle:Z

    return v0
.end method

.method public hasUnits()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Measure;->hasUnits:Z

    return v0
.end method

.method public hasUnitsPrefix()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Measure;->hasUnitsPrefix:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 60
    iget-boolean v1, p0, Lcom/google/speech/patts/markup/Measure;->hasNumber:Z

    if-nez v1, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v0

    .line 61
    :cond_1
    iget-boolean v1, p0, Lcom/google/speech/patts/markup/Measure;->hasUnits:Z

    if-eqz v1, :cond_0

    .line 62
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->toBuilder()Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/markup/Measure$Builder;
    .locals 1

    .prologue
    .line 187
    invoke-static {p0}, Lcom/google/speech/patts/markup/Measure;->newBuilder(Lcom/google/speech/patts/markup/Measure;)Lcom/google/speech/patts/markup/Measure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->getSerializedSize()I

    .line 68
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->hasNumber()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->hasUnits()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->getUnits()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 74
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->getFraction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 77
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->hasUnitsPrefix()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 78
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->getUnitsPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 80
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 81
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Measure;->getStyle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 83
    :cond_4
    return-void
.end method
