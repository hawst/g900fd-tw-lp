.class public final Lcom/google/speech/patts/hmm/HmmTransform;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "HmmTransform.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/hmm/HmmTransform$1;,
        Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/hmm/HmmTransform;


# instance fields
.field private classesToXformsMemoizedSerializedSize:I

.field private classesToXforms_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private hasInvertedBlocks:Z

.field private hasVoiceChecksum:Z

.field private hasXformType:Z

.field private invertedBlocks_:Z

.field private memoizedSerializedSize:I

.field private transforms_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/hmm/LinearTransformProto;",
            ">;"
        }
    .end annotation
.end field

.field private voiceChecksum_:I

.field private xformType_:Lcom/google/speech/patts/hmm/TransformType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 512
    new-instance v0, Lcom/google/speech/patts/hmm/HmmTransform;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/hmm/HmmTransform;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/hmm/HmmTransform;->defaultInstance:Lcom/google/speech/patts/hmm/HmmTransform;

    .line 513
    invoke-static {}, Lcom/google/speech/patts/hmm/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 514
    sget-object v0, Lcom/google/speech/patts/hmm/HmmTransform;->defaultInstance:Lcom/google/speech/patts/hmm/HmmTransform;

    invoke-direct {v0}, Lcom/google/speech/patts/hmm/HmmTransform;->initFields()V

    .line 515
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;

    .line 45
    iput v1, p0, Lcom/google/speech/patts/hmm/HmmTransform;->classesToXformsMemoizedSerializedSize:I

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->invertedBlocks_:Z

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->voiceChecksum_:I

    .line 99
    iput v1, p0, Lcom/google/speech/patts/hmm/HmmTransform;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/hmm/HmmTransform$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/hmm/HmmTransform$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/hmm/HmmTransform;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, -0x1

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;

    .line 45
    iput v1, p0, Lcom/google/speech/patts/hmm/HmmTransform;->classesToXformsMemoizedSerializedSize:I

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->invertedBlocks_:Z

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->voiceChecksum_:I

    .line 99
    iput v1, p0, Lcom/google/speech/patts/hmm/HmmTransform;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/hmm/HmmTransform;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/HmmTransform;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/hmm/HmmTransform;->voiceChecksum_:I

    return p1
.end method

.method static synthetic access$300(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/HmmTransform;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/patts/hmm/HmmTransform;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/HmmTransform;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/speech/patts/hmm/HmmTransform;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/HmmTransform;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/speech/patts/hmm/HmmTransform;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/HmmTransform;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/hmm/HmmTransform;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/HmmTransform;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/hmm/HmmTransform;->hasXformType:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/hmm/HmmTransform;Lcom/google/speech/patts/hmm/TransformType;)Lcom/google/speech/patts/hmm/TransformType;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/HmmTransform;
    .param p1, "x1"    # Lcom/google/speech/patts/hmm/TransformType;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/hmm/HmmTransform;->xformType_:Lcom/google/speech/patts/hmm/TransformType;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/hmm/HmmTransform;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/HmmTransform;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/hmm/HmmTransform;->hasInvertedBlocks:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/hmm/HmmTransform;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/HmmTransform;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/hmm/HmmTransform;->invertedBlocks_:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/hmm/HmmTransform;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/HmmTransform;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/hmm/HmmTransform;->hasVoiceChecksum:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/hmm/HmmTransform;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/hmm/HmmTransform;->defaultInstance:Lcom/google/speech/patts/hmm/HmmTransform;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/google/speech/patts/hmm/TransformType;->TRANSFORM_CMLLR_MEANCOV:Lcom/google/speech/patts/hmm/TransformType;

    iput-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->xformType_:Lcom/google/speech/patts/hmm/TransformType;

    .line 70
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    .locals 1

    .prologue
    .line 206
    # invokes: Lcom/google/speech/patts/hmm/HmmTransform$Builder;->create()Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    invoke-static {}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->access$100()Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/hmm/HmmTransform;

    .prologue
    .line 209
    invoke-static {}, Lcom/google/speech/patts/hmm/HmmTransform;->newBuilder()Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassesToXformsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->classesToXforms_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getDefaultInstanceForType()Lcom/google/speech/patts/hmm/HmmTransform;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/hmm/HmmTransform;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/hmm/HmmTransform;->defaultInstance:Lcom/google/speech/patts/hmm/HmmTransform;

    return-object v0
.end method

.method public getInvertedBlocks()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->invertedBlocks_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 101
    iget v3, p0, Lcom/google/speech/patts/hmm/HmmTransform;->memoizedSerializedSize:I

    .line 102
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 136
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 104
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 105
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getTransformsList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/patts/hmm/LinearTransformProto;

    .line 106
    .local v1, "element":Lcom/google/speech/patts/hmm/LinearTransformProto;
    const/4 v5, 0x1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 108
    goto :goto_1

    .line 110
    .end local v1    # "element":Lcom/google/speech/patts/hmm/LinearTransformProto;
    :cond_1
    const/4 v0, 0x0

    .line 111
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getClassesToXformsList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 112
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v0, v5

    .line 114
    goto :goto_2

    .line 115
    .end local v1    # "element":I
    :cond_2
    add-int/2addr v3, v0

    .line 116
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getClassesToXformsList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 117
    add-int/lit8 v3, v3, 0x1

    .line 118
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v3, v5

    .line 121
    :cond_3
    iput v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->classesToXformsMemoizedSerializedSize:I

    .line 123
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->hasXformType()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 124
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getXformType()Lcom/google/speech/patts/hmm/TransformType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/speech/patts/hmm/TransformType;->getNumber()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 127
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->hasInvertedBlocks()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 128
    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getInvertedBlocks()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 131
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->hasVoiceChecksum()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 132
    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getVoiceChecksum()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 135
    :cond_6
    iput v3, p0, Lcom/google/speech/patts/hmm/HmmTransform;->memoizedSerializedSize:I

    move v4, v3

    .line 136
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getTransformsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/hmm/LinearTransformProto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->transforms_:Ljava/util/List;

    return-object v0
.end method

.method public getVoiceChecksum()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->voiceChecksum_:I

    return v0
.end method

.method public getXformType()Lcom/google/speech/patts/hmm/TransformType;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->xformType_:Lcom/google/speech/patts/hmm/TransformType;

    return-object v0
.end method

.method public hasInvertedBlocks()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->hasInvertedBlocks:Z

    return v0
.end method

.method public hasVoiceChecksum()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->hasVoiceChecksum:Z

    return v0
.end method

.method public hasXformType()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/speech/patts/hmm/HmmTransform;->hasXformType:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->toBuilder()Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/hmm/HmmTransform$Builder;
    .locals 1

    .prologue
    .line 211
    invoke-static {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->newBuilder(Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getSerializedSize()I

    .line 78
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getTransformsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/hmm/LinearTransformProto;

    .line 79
    .local v0, "element":Lcom/google/speech/patts/hmm/LinearTransformProto;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 81
    .end local v0    # "element":Lcom/google/speech/patts/hmm/LinearTransformProto;
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getClassesToXformsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 82
    const/16 v2, 0x12

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 83
    iget v2, p0, Lcom/google/speech/patts/hmm/HmmTransform;->classesToXformsMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 85
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getClassesToXformsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 86
    .local v0, "element":I
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32NoTag(I)V

    goto :goto_1

    .line 88
    .end local v0    # "element":I
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->hasXformType()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 89
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getXformType()Lcom/google/speech/patts/hmm/TransformType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/patts/hmm/TransformType;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 91
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->hasInvertedBlocks()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 92
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getInvertedBlocks()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 94
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->hasVoiceChecksum()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 95
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/HmmTransform;->getVoiceChecksum()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 97
    :cond_5
    return-void
.end method
