.class public final enum Lcom/google/speech/patts/markup/Date$LegalMonth;
.super Ljava/lang/Enum;
.source "Date.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Date;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LegalMonth"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/speech/patts/markup/Date$LegalMonth;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/speech/patts/markup/Date$LegalMonth;

.field public static final enum MONTH1:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field public static final enum MONTH10:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field public static final enum MONTH11:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field public static final enum MONTH12:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field public static final enum MONTH2:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field public static final enum MONTH3:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field public static final enum MONTH4:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field public static final enum MONTH5:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field public static final enum MONTH6:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field public static final enum MONTH7:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field public static final enum MONTH8:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field public static final enum MONTH9:Lcom/google/speech/patts/markup/Date$LegalMonth;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/speech/patts/markup/Date$LegalMonth;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 24
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    const-string v1, "MONTH1"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/speech/patts/markup/Date$LegalMonth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH1:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 25
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    const-string v1, "MONTH2"

    invoke-direct {v0, v1, v5, v5, v6}, Lcom/google/speech/patts/markup/Date$LegalMonth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH2:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 26
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    const-string v1, "MONTH3"

    invoke-direct {v0, v1, v6, v6, v7}, Lcom/google/speech/patts/markup/Date$LegalMonth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH3:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 27
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    const-string v1, "MONTH4"

    invoke-direct {v0, v1, v7, v7, v8}, Lcom/google/speech/patts/markup/Date$LegalMonth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH4:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 28
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    const-string v1, "MONTH5"

    invoke-direct {v0, v1, v8, v8, v9}, Lcom/google/speech/patts/markup/Date$LegalMonth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH5:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 29
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    const-string v1, "MONTH6"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v9, v9, v2}, Lcom/google/speech/patts/markup/Date$LegalMonth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH6:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 30
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    const-string v1, "MONTH7"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/patts/markup/Date$LegalMonth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH7:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 31
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    const-string v1, "MONTH8"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/patts/markup/Date$LegalMonth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH8:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 32
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    const-string v1, "MONTH9"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/patts/markup/Date$LegalMonth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH9:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 33
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    const-string v1, "MONTH10"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const/16 v4, 0xa

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/patts/markup/Date$LegalMonth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH10:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 34
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    const-string v1, "MONTH11"

    const/16 v2, 0xa

    const/16 v3, 0xa

    const/16 v4, 0xb

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/patts/markup/Date$LegalMonth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH11:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 35
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    const-string v1, "MONTH12"

    const/16 v2, 0xb

    const/16 v3, 0xb

    const/16 v4, 0xc

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/speech/patts/markup/Date$LegalMonth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH12:Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 22
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/speech/patts/markup/Date$LegalMonth;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH1:Lcom/google/speech/patts/markup/Date$LegalMonth;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH2:Lcom/google/speech/patts/markup/Date$LegalMonth;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH3:Lcom/google/speech/patts/markup/Date$LegalMonth;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH4:Lcom/google/speech/patts/markup/Date$LegalMonth;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH5:Lcom/google/speech/patts/markup/Date$LegalMonth;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH6:Lcom/google/speech/patts/markup/Date$LegalMonth;

    aput-object v1, v0, v9

    const/4 v1, 0x6

    sget-object v2, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH7:Lcom/google/speech/patts/markup/Date$LegalMonth;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH8:Lcom/google/speech/patts/markup/Date$LegalMonth;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH9:Lcom/google/speech/patts/markup/Date$LegalMonth;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH10:Lcom/google/speech/patts/markup/Date$LegalMonth;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH11:Lcom/google/speech/patts/markup/Date$LegalMonth;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/speech/patts/markup/Date$LegalMonth;->MONTH12:Lcom/google/speech/patts/markup/Date$LegalMonth;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->$VALUES:[Lcom/google/speech/patts/markup/Date$LegalMonth;

    .line 64
    new-instance v0, Lcom/google/speech/patts/markup/Date$LegalMonth$1;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Date$LegalMonth$1;-><init>()V

    sput-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 74
    iput p3, p0, Lcom/google/speech/patts/markup/Date$LegalMonth;->index:I

    .line 75
    iput p4, p0, Lcom/google/speech/patts/markup/Date$LegalMonth;->value:I

    .line 76
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/speech/patts/markup/Date$LegalMonth;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/markup/Date$LegalMonth;

    return-object v0
.end method

.method public static values()[Lcom/google/speech/patts/markup/Date$LegalMonth;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/speech/patts/markup/Date$LegalMonth;->$VALUES:[Lcom/google/speech/patts/markup/Date$LegalMonth;

    invoke-virtual {v0}, [Lcom/google/speech/patts/markup/Date$LegalMonth;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/speech/patts/markup/Date$LegalMonth;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/speech/patts/markup/Date$LegalMonth;->value:I

    return v0
.end method
