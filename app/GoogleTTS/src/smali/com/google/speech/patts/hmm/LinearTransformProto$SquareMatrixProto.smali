.class public final Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LinearTransformProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/hmm/LinearTransformProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SquareMatrixProto"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;


# instance fields
.field private memoizedSerializedSize:I

.field private valsMemoizedSerializedSize:I

.field private vals_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 315
    new-instance v0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->defaultInstance:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    .line 316
    invoke-static {}, Lcom/google/speech/patts/hmm/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 317
    sget-object v0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->defaultInstance:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    invoke-direct {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->initFields()V

    .line 318
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 25
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 41
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;

    .line 50
    iput v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->valsMemoizedSerializedSize:I

    .line 70
    iput v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->memoizedSerializedSize:I

    .line 26
    invoke-direct {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->initFields()V

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/hmm/LinearTransformProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/hmm/LinearTransformProto$1;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, -0x1

    .line 28
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 41
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;

    .line 50
    iput v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->valsMemoizedSerializedSize:I

    .line 70
    iput v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->memoizedSerializedSize:I

    .line 28
    return-void
.end method

.method static synthetic access$300(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->defaultInstance:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;
    .locals 1

    .prologue
    .line 158
    # invokes: Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->create()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;
    invoke-static {}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->access$100()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    .prologue
    .line 161
    invoke-static {}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->newBuilder()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->getDefaultInstanceForType()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->defaultInstance:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 72
    iget v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->memoizedSerializedSize:I

    .line 73
    .local v1, "size":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 88
    .end local v1    # "size":I
    .local v2, "size":I
    :goto_0
    return v2

    .line 75
    .end local v2    # "size":I
    .restart local v1    # "size":I
    :cond_0
    const/4 v1, 0x0

    .line 77
    const/4 v0, 0x0

    .line 78
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->getValsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 79
    add-int/2addr v1, v0

    .line 80
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->getValsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 81
    add-int/lit8 v1, v1, 0x1

    .line 82
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 85
    :cond_1
    iput v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->valsMemoizedSerializedSize:I

    .line 87
    iput v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->memoizedSerializedSize:I

    move v2, v1

    .line 88
    .end local v1    # "size":I
    .restart local v2    # "size":I
    goto :goto_0
.end method

.method public getValsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->toBuilder()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;
    .locals 1

    .prologue
    .line 163
    invoke-static {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->newBuilder(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->getSerializedSize()I

    .line 61
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->getValsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 62
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 63
    iget v2, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->valsMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->getValsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 66
    .local v0, "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_0

    .line 68
    .end local v0    # "element":F
    :cond_1
    return-void
.end method
