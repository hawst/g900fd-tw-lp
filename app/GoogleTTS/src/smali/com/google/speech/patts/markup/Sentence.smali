.class public final Lcom/google/speech/patts/markup/Sentence;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Sentence.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/markup/Sentence$1;,
        Lcom/google/speech/patts/markup/Sentence$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/markup/Sentence;


# instance fields
.field private entry_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private hasParams:Z

.field private hasStyle:Z

.field private hasTransform:Z

.field private memoizedSerializedSize:I

.field private params_:Lcom/google/speech/patts/ling_utt/TreeMap;

.field private say_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/markup/Say;",
            ">;"
        }
    .end annotation
.end field

.field private style_:Lcom/google/speech/patts/markup/Style;

.field private transform_:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 586
    new-instance v0, Lcom/google/speech/patts/markup/Sentence;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/markup/Sentence;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/markup/Sentence;->defaultInstance:Lcom/google/speech/patts/markup/Sentence;

    .line 587
    invoke-static {}, Lcom/google/speech/patts/markup/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 588
    sget-object v0, Lcom/google/speech/patts/markup/Sentence;->defaultInstance:Lcom/google/speech/patts/markup/Sentence;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Sentence;->initFields()V

    .line 589
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;

    .line 108
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Sentence;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Sentence;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/markup/Sentence$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/markup/Sentence$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Sentence;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;

    .line 108
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Sentence;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1000(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->transform_:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/markup/Sentence;Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;
    .param p1, "x1"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Sentence;->transform_:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/patts/markup/Sentence;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/speech/patts/markup/Sentence;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/markup/Sentence;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Sentence;->hasParams:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/ling_utt/TreeMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->params_:Lcom/google/speech/patts/ling_utt/TreeMap;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/speech/patts/markup/Sentence;Lcom/google/speech/patts/ling_utt/TreeMap;)Lcom/google/speech/patts/ling_utt/TreeMap;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;
    .param p1, "x1"    # Lcom/google/speech/patts/ling_utt/TreeMap;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Sentence;->params_:Lcom/google/speech/patts/ling_utt/TreeMap;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/markup/Sentence;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Sentence;->hasStyle:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Style;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->style_:Lcom/google/speech/patts/markup/Style;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/speech/patts/markup/Sentence;Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Style;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/Style;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Sentence;->style_:Lcom/google/speech/patts/markup/Style;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/markup/Sentence;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Sentence;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Sentence;->hasTransform:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/markup/Sentence;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/markup/Sentence;->defaultInstance:Lcom/google/speech/patts/markup/Sentence;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/speech/patts/ling_utt/TreeMap;->getDefaultInstance()Lcom/google/speech/patts/ling_utt/TreeMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->params_:Lcom/google/speech/patts/ling_utt/TreeMap;

    .line 69
    invoke-static {}, Lcom/google/speech/patts/markup/Style;->getDefaultInstance()Lcom/google/speech/patts/markup/Style;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->style_:Lcom/google/speech/patts/markup/Style;

    .line 70
    invoke-static {}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getDefaultInstance()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->transform_:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .line 71
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/markup/Sentence$Builder;
    .locals 1

    .prologue
    .line 205
    # invokes: Lcom/google/speech/patts/markup/Sentence$Builder;->create()Lcom/google/speech/patts/markup/Sentence$Builder;
    invoke-static {}, Lcom/google/speech/patts/markup/Sentence$Builder;->access$100()Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Sentence$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/markup/Sentence;

    .prologue
    .line 208
    invoke-static {}, Lcom/google/speech/patts/markup/Sentence;->newBuilder()Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/markup/Sentence$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getDefaultInstanceForType()Lcom/google/speech/patts/markup/Sentence;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/markup/Sentence;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/markup/Sentence;->defaultInstance:Lcom/google/speech/patts/markup/Sentence;

    return-object v0
.end method

.method public getEntryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/LexiconProto$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;

    return-object v0
.end method

.method public getParams()Lcom/google/speech/patts/ling_utt/TreeMap;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->params_:Lcom/google/speech/patts/ling_utt/TreeMap;

    return-object v0
.end method

.method public getSayList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/markup/Say;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 110
    iget v2, p0, Lcom/google/speech/patts/markup/Sentence;->memoizedSerializedSize:I

    .line 111
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 135
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 113
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 114
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getSayList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/markup/Say;

    .line 115
    .local v0, "element":Lcom/google/speech/patts/markup/Say;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 117
    goto :goto_1

    .line 118
    .end local v0    # "element":Lcom/google/speech/patts/markup/Say;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getEntryList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Entry;

    .line 119
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Entry;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 121
    goto :goto_2

    .line 122
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Entry;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->hasParams()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 123
    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getParams()Lcom/google/speech/patts/ling_utt/TreeMap;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 126
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->hasStyle()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 127
    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getStyle()Lcom/google/speech/patts/markup/Style;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 130
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->hasTransform()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 131
    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getTransform()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 134
    :cond_5
    iput v2, p0, Lcom/google/speech/patts/markup/Sentence;->memoizedSerializedSize:I

    move v3, v2

    .line 135
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getStyle()Lcom/google/speech/patts/markup/Style;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->style_:Lcom/google/speech/patts/markup/Style;

    return-object v0
.end method

.method public getTransform()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence;->transform_:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    return-object v0
.end method

.method public hasParams()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Sentence;->hasParams:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Sentence;->hasStyle:Z

    return v0
.end method

.method public hasTransform()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Sentence;->hasTransform:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 73
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getSayList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/markup/Say;

    .line 74
    .local v0, "element":Lcom/google/speech/patts/markup/Say;
    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Say;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 85
    .end local v0    # "element":Lcom/google/speech/patts/markup/Say;
    :cond_1
    :goto_0
    return v2

    .line 76
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getEntryList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Entry;

    .line 77
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Entry;
    invoke-virtual {v0}, Lcom/google/speech/tts/LexiconProto$Entry;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_0

    .line 79
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Entry;
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->hasParams()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 80
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getParams()Lcom/google/speech/patts/ling_utt/TreeMap;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/patts/ling_utt/TreeMap;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 82
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->hasTransform()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 83
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getTransform()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 85
    :cond_6
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->toBuilder()Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/markup/Sentence$Builder;
    .locals 1

    .prologue
    .line 210
    invoke-static {p0}, Lcom/google/speech/patts/markup/Sentence;->newBuilder(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getSerializedSize()I

    .line 91
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getSayList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/markup/Say;

    .line 92
    .local v0, "element":Lcom/google/speech/patts/markup/Say;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 94
    .end local v0    # "element":Lcom/google/speech/patts/markup/Say;
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getEntryList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/tts/LexiconProto$Entry;

    .line 95
    .local v0, "element":Lcom/google/speech/tts/LexiconProto$Entry;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 97
    .end local v0    # "element":Lcom/google/speech/tts/LexiconProto$Entry;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->hasParams()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 98
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getParams()Lcom/google/speech/patts/ling_utt/TreeMap;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 100
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 101
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getStyle()Lcom/google/speech/patts/markup/Style;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 103
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->hasTransform()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 104
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence;->getTransform()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 106
    :cond_4
    return-void
.end method
