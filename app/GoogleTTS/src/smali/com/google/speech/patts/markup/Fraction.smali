.class public final Lcom/google/speech/patts/markup/Fraction;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Fraction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/markup/Fraction$1;,
        Lcom/google/speech/patts/markup/Fraction$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/markup/Fraction;


# instance fields
.field private denominator_:I

.field private hasDenominator:Z

.field private hasNumerator:Z

.field private hasStyle:Z

.field private memoizedSerializedSize:I

.field private numerator_:I

.field private style_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 332
    new-instance v0, Lcom/google/speech/patts/markup/Fraction;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/markup/Fraction;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/markup/Fraction;->defaultInstance:Lcom/google/speech/patts/markup/Fraction;

    .line 333
    invoke-static {}, Lcom/google/speech/patts/markup/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 334
    sget-object v0, Lcom/google/speech/patts/markup/Fraction;->defaultInstance:Lcom/google/speech/patts/markup/Fraction;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Fraction;->initFields()V

    .line 335
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/patts/markup/Fraction;->numerator_:I

    .line 32
    iput v0, p0, Lcom/google/speech/patts/markup/Fraction;->denominator_:I

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Fraction;->style_:Ljava/lang/String;

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Fraction;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Fraction;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/markup/Fraction$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/markup/Fraction$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Fraction;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/patts/markup/Fraction;->numerator_:I

    .line 32
    iput v0, p0, Lcom/google/speech/patts/markup/Fraction;->denominator_:I

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Fraction;->style_:Ljava/lang/String;

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Fraction;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/patts/markup/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Fraction;->hasNumerator:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/markup/Fraction;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Fraction;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/markup/Fraction;->numerator_:I

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/markup/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Fraction;->hasDenominator:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/markup/Fraction;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Fraction;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/markup/Fraction;->denominator_:I

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/markup/Fraction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Fraction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Fraction;->hasStyle:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/markup/Fraction;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Fraction;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Fraction;->style_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/markup/Fraction;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/markup/Fraction;->defaultInstance:Lcom/google/speech/patts/markup/Fraction;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/markup/Fraction$Builder;
    .locals 1

    .prologue
    .line 154
    # invokes: Lcom/google/speech/patts/markup/Fraction$Builder;->create()Lcom/google/speech/patts/markup/Fraction$Builder;
    invoke-static {}, Lcom/google/speech/patts/markup/Fraction$Builder;->access$100()Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Fraction$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/markup/Fraction;

    .prologue
    .line 157
    invoke-static {}, Lcom/google/speech/patts/markup/Fraction;->newBuilder()Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/markup/Fraction$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->getDefaultInstanceForType()Lcom/google/speech/patts/markup/Fraction;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/markup/Fraction;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/markup/Fraction;->defaultInstance:Lcom/google/speech/patts/markup/Fraction;

    return-object v0
.end method

.method public getDenominator()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/speech/patts/markup/Fraction;->denominator_:I

    return v0
.end method

.method public getNumerator()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/patts/markup/Fraction;->numerator_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 67
    iget v0, p0, Lcom/google/speech/patts/markup/Fraction;->memoizedSerializedSize:I

    .line 68
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 84
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 70
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 71
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->hasNumerator()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 72
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->getNumerator()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 75
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->hasDenominator()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 76
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->getDenominator()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 79
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 80
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->getStyle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 83
    :cond_3
    iput v0, p0, Lcom/google/speech/patts/markup/Fraction;->memoizedSerializedSize:I

    move v1, v0

    .line 84
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/patts/markup/Fraction;->style_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDenominator()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Fraction;->hasDenominator:Z

    return v0
.end method

.method public hasNumerator()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Fraction;->hasNumerator:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Fraction;->hasStyle:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 46
    iget-boolean v1, p0, Lcom/google/speech/patts/markup/Fraction;->hasNumerator:Z

    if-nez v1, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v0

    .line 47
    :cond_1
    iget-boolean v1, p0, Lcom/google/speech/patts/markup/Fraction;->hasDenominator:Z

    if-eqz v1, :cond_0

    .line 48
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->toBuilder()Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/markup/Fraction$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-static {p0}, Lcom/google/speech/patts/markup/Fraction;->newBuilder(Lcom/google/speech/patts/markup/Fraction;)Lcom/google/speech/patts/markup/Fraction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->getSerializedSize()I

    .line 54
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->hasNumerator()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->getNumerator()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->hasDenominator()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->getDenominator()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 60
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Fraction;->getStyle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 63
    :cond_2
    return-void
.end method
