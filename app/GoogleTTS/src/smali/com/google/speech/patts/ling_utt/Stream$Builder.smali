.class public final Lcom/google/speech/patts/ling_utt/Stream$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Stream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/ling_utt/Stream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/ling_utt/Stream;",
        "Lcom/google/speech/patts/ling_utt/Stream$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/ling_utt/Stream;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/ling_utt/Stream$Builder;
    .locals 1

    .prologue
    .line 154
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->create()Lcom/google/speech/patts/ling_utt/Stream$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/ling_utt/Stream$Builder;
    .locals 3

    .prologue
    .line 163
    new-instance v0, Lcom/google/speech/patts/ling_utt/Stream$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/ling_utt/Stream$Builder;-><init>()V

    .line 164
    .local v0, "builder":Lcom/google/speech/patts/ling_utt/Stream$Builder;
    new-instance v1, Lcom/google/speech/patts/ling_utt/Stream;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/ling_utt/Stream;-><init>(Lcom/google/speech/patts/ling_utt/Stream$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    .line 165
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->build()Lcom/google/speech/patts/ling_utt/Stream;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/ling_utt/Stream;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 196
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->buildPartial()Lcom/google/speech/patts/ling_utt/Stream;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/ling_utt/Stream;
    .locals 3

    .prologue
    .line 209
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    if-nez v1, :cond_0

    .line 210
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 213
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    # getter for: Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/ling_utt/Stream;->access$300(Lcom/google/speech/patts/ling_utt/Stream;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 214
    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    iget-object v2, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    # getter for: Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/ling_utt/Stream;->access$300(Lcom/google/speech/patts/ling_utt/Stream;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/ling_utt/Stream;->access$302(Lcom/google/speech/patts/ling_utt/Stream;Ljava/util/List;)Ljava/util/List;

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    .line 218
    .local v0, "returnMe":Lcom/google/speech/patts/ling_utt/Stream;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    .line 219
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->clone()Lcom/google/speech/patts/ling_utt/Stream$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->clone()Lcom/google/speech/patts/ling_utt/Stream$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/ling_utt/Stream$Builder;
    .locals 2

    .prologue
    .line 182
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->create()Lcom/google/speech/patts/ling_utt/Stream$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->mergeFrom(Lcom/google/speech/patts/ling_utt/Stream;)Lcom/google/speech/patts/ling_utt/Stream$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->clone()Lcom/google/speech/patts/ling_utt/Stream$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    invoke-virtual {v0}, Lcom/google/speech/patts/ling_utt/Stream;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 154
    check-cast p1, Lcom/google/speech/patts/ling_utt/Stream;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->mergeFrom(Lcom/google/speech/patts/ling_utt/Stream;)Lcom/google/speech/patts/ling_utt/Stream$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/ling_utt/Stream;)Lcom/google/speech/patts/ling_utt/Stream$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/ling_utt/Stream;

    .prologue
    .line 223
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Stream;->getDefaultInstance()Lcom/google/speech/patts/ling_utt/Stream;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-object p0

    .line 224
    :cond_1
    # getter for: Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Stream;->access$300(Lcom/google/speech/patts/ling_utt/Stream;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 225
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    # getter for: Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Stream;->access$300(Lcom/google/speech/patts/ling_utt/Stream;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 226
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Stream;->access$302(Lcom/google/speech/patts/ling_utt/Stream;Ljava/util/List;)Ljava/util/List;

    .line 228
    :cond_2
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    # getter for: Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/ling_utt/Stream;->access$300(Lcom/google/speech/patts/ling_utt/Stream;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/ling_utt/Stream;->items_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/ling_utt/Stream;->access$300(Lcom/google/speech/patts/ling_utt/Stream;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 230
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Stream;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    invoke-virtual {p1}, Lcom/google/speech/patts/ling_utt/Stream;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/ling_utt/Stream$Builder;->setName(Ljava/lang/String;)Lcom/google/speech/patts/ling_utt/Stream$Builder;

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/patts/ling_utt/Stream$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 325
    if-nez p1, :cond_0

    .line 326
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/ling_utt/Stream;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/ling_utt/Stream;->access$402(Lcom/google/speech/patts/ling_utt/Stream;Z)Z

    .line 329
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Stream$Builder;->result:Lcom/google/speech/patts/ling_utt/Stream;

    # setter for: Lcom/google/speech/patts/ling_utt/Stream;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/ling_utt/Stream;->access$502(Lcom/google/speech/patts/ling_utt/Stream;Ljava/lang/String;)Ljava/lang/String;

    .line 330
    return-object p0
.end method
