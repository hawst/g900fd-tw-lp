.class public final Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SpeechMorphingTransform.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/hmm/SpeechMorphingTransform;",
        "Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    .locals 1

    .prologue
    .line 164
    invoke-static {}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->create()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    .locals 3

    .prologue
    .line 173
    new-instance v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;-><init>()V

    .line 174
    .local v0, "builder":Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    new-instance v1, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;-><init>(Lcom/google/speech/patts/hmm/SpeechMorphingTransform$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .line 175
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->build()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    invoke-static {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->buildPartial()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    .locals 3

    .prologue
    .line 219
    iget-object v1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    if-nez v1, :cond_0

    .line 220
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .line 224
    .local v0, "returnMe":Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .line 225
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->clone()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->clone()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    .locals 2

    .prologue
    .line 192
    invoke-static {}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->create()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->clone()Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    invoke-virtual {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrequencyWarpWeight(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    invoke-virtual {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasFrequencyWarpWeight()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    # getter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->frequencyWarpWeight_:Lspeech/patts/FrequencyWarpAndWeightTransforms;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->access$600(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v0

    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getDefaultInstance()Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 344
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    iget-object v1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    # getter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->frequencyWarpWeight_:Lspeech/patts/FrequencyWarpAndWeightTransforms;
    invoke-static {v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->access$600(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->newBuilder(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->mergeFrom(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->buildPartial()Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->frequencyWarpWeight_:Lspeech/patts/FrequencyWarpAndWeightTransforms;
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->access$602(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .line 349
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasFrequencyWarpWeight:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->access$502(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Z)Z

    .line 350
    return-object p0

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    # setter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->frequencyWarpWeight_:Lspeech/patts/FrequencyWarpAndWeightTransforms;
    invoke-static {v0, p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->access$602(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransforms;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 164
    check-cast p1, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .prologue
    .line 229
    invoke-static {}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getDefaultInstance()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 239
    :cond_0
    :goto_0
    return-object p0

    .line 230
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasHmmXforms()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getHmmXforms()Lcom/google/speech/patts/hmm/HmmTransform;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->mergeHmmXforms(Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    .line 233
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasFrequencyWarpWeight()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 234
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getFrequencyWarpWeight()Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->mergeFrequencyWarpWeight(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    .line 236
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasSpeakerId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    invoke-virtual {p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getSpeakerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->setSpeakerId(Ljava/lang/String;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    goto :goto_0
.end method

.method public mergeHmmXforms(Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/hmm/HmmTransform;

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    invoke-virtual {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasHmmXforms()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    # getter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hmmXforms_:Lcom/google/speech/patts/hmm/HmmTransform;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->access$400(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/HmmTransform;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/hmm/HmmTransform;->getDefaultInstance()Lcom/google/speech/patts/hmm/HmmTransform;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 307
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    iget-object v1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    # getter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hmmXforms_:Lcom/google/speech/patts/hmm/HmmTransform;
    invoke-static {v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->access$400(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/HmmTransform;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/hmm/HmmTransform;->newBuilder(Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/HmmTransform$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/hmm/HmmTransform$Builder;->buildPartial()Lcom/google/speech/patts/hmm/HmmTransform;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hmmXforms_:Lcom/google/speech/patts/hmm/HmmTransform;
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->access$402(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/HmmTransform;

    .line 312
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasHmmXforms:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->access$302(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Z)Z

    .line 313
    return-object p0

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    # setter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hmmXforms_:Lcom/google/speech/patts/hmm/HmmTransform;
    invoke-static {v0, p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->access$402(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Lcom/google/speech/patts/hmm/HmmTransform;)Lcom/google/speech/patts/hmm/HmmTransform;

    goto :goto_0
.end method

.method public setSpeakerId(Ljava/lang/String;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 366
    if-nez p1, :cond_0

    .line 367
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->hasSpeakerId:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->access$702(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Z)Z

    .line 370
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->result:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    # setter for: Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->speakerId_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->access$802(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;Ljava/lang/String;)Ljava/lang/String;

    .line 371
    return-object p0
.end method
