.class public final Lcom/google/speech/patts/ling_utt/Item;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Item.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/ling_utt/Item$1;,
        Lcom/google/speech/patts/ling_utt/Item$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/ling_utt/Item;


# instance fields
.field private firstDaughter_:I

.field private hasFirstDaughter:Z

.field private hasLastDaughter:Z

.field private hasParent:Z

.field private hasStreamName:Z

.field private lastDaughter_:I

.field private memoizedSerializedSize:I

.field private parent_:I

.field private streamName_:Ljava/lang/String;

.field private val_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/ling_utt/Val;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 458
    new-instance v0, Lcom/google/speech/patts/ling_utt/Item;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/ling_utt/Item;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/ling_utt/Item;->defaultInstance:Lcom/google/speech/patts/ling_utt/Item;

    .line 459
    invoke-static {}, Lcom/google/speech/patts/ling_utt/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 460
    sget-object v0, Lcom/google/speech/patts/ling_utt/Item;->defaultInstance:Lcom/google/speech/patts/ling_utt/Item;

    invoke-direct {v0}, Lcom/google/speech/patts/ling_utt/Item;->initFields()V

    .line 461
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Item;->val_:Ljava/util/List;

    .line 37
    iput v1, p0, Lcom/google/speech/patts/ling_utt/Item;->firstDaughter_:I

    .line 44
    iput v1, p0, Lcom/google/speech/patts/ling_utt/Item;->lastDaughter_:I

    .line 51
    iput v1, p0, Lcom/google/speech/patts/ling_utt/Item;->parent_:I

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Item;->streamName_:Ljava/lang/String;

    .line 91
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/ling_utt/Item;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/ling_utt/Item;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/ling_utt/Item$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/ling_utt/Item$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/ling_utt/Item;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Item;->val_:Ljava/util/List;

    .line 37
    iput v1, p0, Lcom/google/speech/patts/ling_utt/Item;->firstDaughter_:I

    .line 44
    iput v1, p0, Lcom/google/speech/patts/ling_utt/Item;->lastDaughter_:I

    .line 51
    iput v1, p0, Lcom/google/speech/patts/ling_utt/Item;->parent_:I

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/ling_utt/Item;->streamName_:Ljava/lang/String;

    .line 91
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/ling_utt/Item;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/ling_utt/Item;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Item;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Item;->hasStreamName:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/patts/ling_utt/Item;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Item;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Item;->streamName_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/patts/ling_utt/Item;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Item;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Item;->val_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/patts/ling_utt/Item;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Item;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/ling_utt/Item;->val_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/ling_utt/Item;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Item;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Item;->hasFirstDaughter:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/ling_utt/Item;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Item;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/ling_utt/Item;->firstDaughter_:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/ling_utt/Item;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Item;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Item;->hasLastDaughter:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/ling_utt/Item;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Item;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/ling_utt/Item;->lastDaughter_:I

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/ling_utt/Item;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Item;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/ling_utt/Item;->hasParent:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/ling_utt/Item;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/ling_utt/Item;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/ling_utt/Item;->parent_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/ling_utt/Item;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/ling_utt/Item;->defaultInstance:Lcom/google/speech/patts/ling_utt/Item;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/ling_utt/Item$Builder;
    .locals 1

    .prologue
    .line 188
    # invokes: Lcom/google/speech/patts/ling_utt/Item$Builder;->create()Lcom/google/speech/patts/ling_utt/Item$Builder;
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Item$Builder;->access$100()Lcom/google/speech/patts/ling_utt/Item$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/ling_utt/Item;)Lcom/google/speech/patts/ling_utt/Item$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/ling_utt/Item;

    .prologue
    .line 191
    invoke-static {}, Lcom/google/speech/patts/ling_utt/Item;->newBuilder()Lcom/google/speech/patts/ling_utt/Item$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/ling_utt/Item$Builder;->mergeFrom(Lcom/google/speech/patts/ling_utt/Item;)Lcom/google/speech/patts/ling_utt/Item$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getDefaultInstanceForType()Lcom/google/speech/patts/ling_utt/Item;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/ling_utt/Item;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/ling_utt/Item;->defaultInstance:Lcom/google/speech/patts/ling_utt/Item;

    return-object v0
.end method

.method public getFirstDaughter()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/speech/patts/ling_utt/Item;->firstDaughter_:I

    return v0
.end method

.method public getLastDaughter()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/google/speech/patts/ling_utt/Item;->lastDaughter_:I

    return v0
.end method

.method public getParent()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/speech/patts/ling_utt/Item;->parent_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 93
    iget v2, p0, Lcom/google/speech/patts/ling_utt/Item;->memoizedSerializedSize:I

    .line 94
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 118
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 96
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 97
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getValList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Val;

    .line 98
    .local v0, "element":Lcom/google/speech/patts/ling_utt/Val;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 100
    goto :goto_1

    .line 101
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Val;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->hasFirstDaughter()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 102
    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getFirstDaughter()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 105
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->hasLastDaughter()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 106
    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getLastDaughter()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 109
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->hasParent()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 110
    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getParent()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 113
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->hasStreamName()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 114
    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getStreamName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 117
    :cond_5
    iput v2, p0, Lcom/google/speech/patts/ling_utt/Item;->memoizedSerializedSize:I

    move v3, v2

    .line 118
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getStreamName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Item;->streamName_:Ljava/lang/String;

    return-object v0
.end method

.method public getValList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/ling_utt/Val;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/ling_utt/Item;->val_:Ljava/util/List;

    return-object v0
.end method

.method public hasFirstDaughter()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Item;->hasFirstDaughter:Z

    return v0
.end method

.method public hasLastDaughter()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Item;->hasLastDaughter:Z

    return v0
.end method

.method public hasParent()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Item;->hasParent:Z

    return v0
.end method

.method public hasStreamName()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/speech/patts/ling_utt/Item;->hasStreamName:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getValList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Val;

    .line 66
    .local v0, "element":Lcom/google/speech/patts/ling_utt/Val;
    invoke-virtual {v0}, Lcom/google/speech/patts/ling_utt/Val;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 68
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Val;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->toBuilder()Lcom/google/speech/patts/ling_utt/Item$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/ling_utt/Item$Builder;
    .locals 1

    .prologue
    .line 193
    invoke-static {p0}, Lcom/google/speech/patts/ling_utt/Item;->newBuilder(Lcom/google/speech/patts/ling_utt/Item;)Lcom/google/speech/patts/ling_utt/Item$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getSerializedSize()I

    .line 74
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getValList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/ling_utt/Val;

    .line 75
    .local v0, "element":Lcom/google/speech/patts/ling_utt/Val;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 77
    .end local v0    # "element":Lcom/google/speech/patts/ling_utt/Val;
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->hasFirstDaughter()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 78
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getFirstDaughter()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 80
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->hasLastDaughter()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 81
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getLastDaughter()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 83
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->hasParent()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 84
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getParent()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 86
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->hasStreamName()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 87
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/ling_utt/Item;->getStreamName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 89
    :cond_4
    return-void
.end method
