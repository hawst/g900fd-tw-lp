.class public final Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SpeechMorphingTransformPalette.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$1;,
        Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;


# instance fields
.field private defaultSpeakerReference_:Ljava/lang/String;

.field private hasDefaultSpeakerReference:Z

.field private memoizedSerializedSize:I

.field private speaker_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/hmm/SpeechMorphingTransform;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 341
    new-instance v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->defaultInstance:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    .line 342
    invoke-static {}, Lcom/google/speech/patts/hmm/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 343
    sget-object v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->defaultInstance:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    invoke-direct {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->initFields()V

    .line 344
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->defaultSpeakerReference_:Ljava/lang/String;

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->defaultSpeakerReference_:Ljava/lang/String;

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->hasDefaultSpeakerReference:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->defaultSpeakerReference_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->defaultInstance:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;
    .locals 1

    .prologue
    .line 146
    # invokes: Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->create()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;
    invoke-static {}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->access$100()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    .prologue
    .line 149
    invoke-static {}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->newBuilder()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->getDefaultInstanceForType()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->defaultInstance:Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;

    return-object v0
.end method

.method public getDefaultSpeakerReference()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->defaultSpeakerReference_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 63
    iget v2, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->memoizedSerializedSize:I

    .line 64
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 76
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 66
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->getSpeakerList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .line 68
    .local v0, "element":Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 70
    goto :goto_1

    .line 71
    .end local v0    # "element":Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->hasDefaultSpeakerReference()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 72
    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->getDefaultSpeakerReference()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 75
    :cond_2
    iput v2, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->memoizedSerializedSize:I

    move v3, v2

    .line 76
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getSpeakerList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/hmm/SpeechMorphingTransform;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->speaker_:Ljava/util/List;

    return-object v0
.end method

.method public hasDefaultSpeakerReference()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->hasDefaultSpeakerReference:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->getSpeakerList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .line 45
    .local v0, "element":Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    invoke-virtual {v0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 47
    .end local v0    # "element":Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->toBuilder()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;
    .locals 1

    .prologue
    .line 151
    invoke-static {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->newBuilder(Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;)Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->getSerializedSize()I

    .line 53
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->getSpeakerList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .line 54
    .local v0, "element":Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 56
    .end local v0    # "element":Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->hasDefaultSpeakerReference()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 57
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/SpeechMorphingTransformPalette;->getDefaultSpeakerReference()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 59
    :cond_1
    return-void
.end method
