.class public final Lcom/google/speech/patts/Frames$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Frames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/Frames;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/Frames;",
        "Lcom/google/speech/patts/Frames$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/Frames;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/Frames$Builder;
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Lcom/google/speech/patts/Frames$Builder;->create()Lcom/google/speech/patts/Frames$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/Frames$Builder;
    .locals 3

    .prologue
    .line 145
    new-instance v0, Lcom/google/speech/patts/Frames$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/Frames$Builder;-><init>()V

    .line 146
    .local v0, "builder":Lcom/google/speech/patts/Frames$Builder;
    new-instance v1, Lcom/google/speech/patts/Frames;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/Frames;-><init>(Lcom/google/speech/patts/Frames$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    .line 147
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/speech/patts/Frames$Builder;->build()Lcom/google/speech/patts/Frames;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/Frames;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/Frames$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    invoke-static {v0}, Lcom/google/speech/patts/Frames$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 178
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/Frames$Builder;->buildPartial()Lcom/google/speech/patts/Frames;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/Frames;
    .locals 3

    .prologue
    .line 191
    iget-object v1, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    if-nez v1, :cond_0

    .line 192
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 195
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    # getter for: Lcom/google/speech/patts/Frames;->frames_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/Frames;->access$300(Lcom/google/speech/patts/Frames;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 196
    iget-object v1, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    iget-object v2, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    # getter for: Lcom/google/speech/patts/Frames;->frames_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/Frames;->access$300(Lcom/google/speech/patts/Frames;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/Frames;->frames_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/Frames;->access$302(Lcom/google/speech/patts/Frames;Ljava/util/List;)Ljava/util/List;

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    .line 200
    .local v0, "returnMe":Lcom/google/speech/patts/Frames;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    .line 201
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/speech/patts/Frames$Builder;->clone()Lcom/google/speech/patts/Frames$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/speech/patts/Frames$Builder;->clone()Lcom/google/speech/patts/Frames$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/Frames$Builder;
    .locals 2

    .prologue
    .line 164
    invoke-static {}, Lcom/google/speech/patts/Frames$Builder;->create()Lcom/google/speech/patts/Frames$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/Frames$Builder;->mergeFrom(Lcom/google/speech/patts/Frames;)Lcom/google/speech/patts/Frames$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/speech/patts/Frames$Builder;->clone()Lcom/google/speech/patts/Frames$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    invoke-virtual {v0}, Lcom/google/speech/patts/Frames;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 136
    check-cast p1, Lcom/google/speech/patts/Frames;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/Frames$Builder;->mergeFrom(Lcom/google/speech/patts/Frames;)Lcom/google/speech/patts/Frames$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/Frames;)Lcom/google/speech/patts/Frames$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/Frames;

    .prologue
    .line 205
    invoke-static {}, Lcom/google/speech/patts/Frames;->getDefaultInstance()Lcom/google/speech/patts/Frames;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-object p0

    .line 206
    :cond_1
    # getter for: Lcom/google/speech/patts/Frames;->frames_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/Frames;->access$300(Lcom/google/speech/patts/Frames;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    # getter for: Lcom/google/speech/patts/Frames;->frames_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/Frames;->access$300(Lcom/google/speech/patts/Frames;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 208
    iget-object v0, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/Frames;->frames_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/Frames;->access$302(Lcom/google/speech/patts/Frames;Ljava/util/List;)Ljava/util/List;

    .line 210
    :cond_2
    iget-object v0, p0, Lcom/google/speech/patts/Frames$Builder;->result:Lcom/google/speech/patts/Frames;

    # getter for: Lcom/google/speech/patts/Frames;->frames_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/Frames;->access$300(Lcom/google/speech/patts/Frames;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/Frames;->frames_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/Frames;->access$300(Lcom/google/speech/patts/Frames;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
