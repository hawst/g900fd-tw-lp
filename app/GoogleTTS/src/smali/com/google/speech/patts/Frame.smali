.class public final Lcom/google/speech/patts/Frame;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Frame.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/Frame$1;,
        Lcom/google/speech/patts/Frame$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/Frame;


# instance fields
.field private firstDaughter_:I

.field private hasFirstDaughter:Z

.field private hasLastDaughter:Z

.field private hasParent:Z

.field private hasVocoderInputs:Z

.field private lastDaughter_:I

.field private memoizedSerializedSize:I

.field private parent_:I

.field private pdfs_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/ProbabilityDistribution;",
            ">;"
        }
    .end annotation
.end field

.field private vocoderInputs_:Lspeech/patts/VocoderInput;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 477
    new-instance v0, Lcom/google/speech/patts/Frame;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/Frame;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/Frame;->defaultInstance:Lcom/google/speech/patts/Frame;

    .line 478
    invoke-static {}, Lcom/google/speech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 479
    sget-object v0, Lcom/google/speech/patts/Frame;->defaultInstance:Lcom/google/speech/patts/Frame;

    invoke-direct {v0}, Lcom/google/speech/patts/Frame;->initFields()V

    .line 480
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/patts/Frame;->parent_:I

    .line 32
    iput v0, p0, Lcom/google/speech/patts/Frame;->firstDaughter_:I

    .line 39
    iput v0, p0, Lcom/google/speech/patts/Frame;->lastDaughter_:I

    .line 45
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/Frame;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/Frame;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/Frame$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/Frame$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/Frame;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/patts/Frame;->parent_:I

    .line 32
    iput v0, p0, Lcom/google/speech/patts/Frame;->firstDaughter_:I

    .line 39
    iput v0, p0, Lcom/google/speech/patts/Frame;->lastDaughter_:I

    .line 45
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/Frame;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/Frame;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Frame;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Frame;->hasVocoderInputs:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/speech/patts/Frame;)Lspeech/patts/VocoderInput;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/Frame;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/Frame;->vocoderInputs_:Lspeech/patts/VocoderInput;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/speech/patts/Frame;Lspeech/patts/VocoderInput;)Lspeech/patts/VocoderInput;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Frame;
    .param p1, "x1"    # Lspeech/patts/VocoderInput;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Frame;->vocoderInputs_:Lspeech/patts/VocoderInput;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/patts/Frame;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/Frame;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/patts/Frame;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Frame;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/Frame;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Frame;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Frame;->hasParent:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/Frame;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Frame;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Frame;->parent_:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/Frame;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Frame;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Frame;->hasFirstDaughter:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/Frame;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Frame;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Frame;->firstDaughter_:I

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/Frame;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Frame;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Frame;->hasLastDaughter:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/Frame;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Frame;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Frame;->lastDaughter_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/Frame;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/Frame;->defaultInstance:Lcom/google/speech/patts/Frame;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 63
    invoke-static {}, Lspeech/patts/VocoderInput;->getDefaultInstance()Lspeech/patts/VocoderInput;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/Frame;->vocoderInputs_:Lspeech/patts/VocoderInput;

    .line 64
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/Frame$Builder;
    .locals 1

    .prologue
    .line 186
    # invokes: Lcom/google/speech/patts/Frame$Builder;->create()Lcom/google/speech/patts/Frame$Builder;
    invoke-static {}, Lcom/google/speech/patts/Frame$Builder;->access$100()Lcom/google/speech/patts/Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/Frame;)Lcom/google/speech/patts/Frame$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/Frame;

    .prologue
    .line 189
    invoke-static {}, Lcom/google/speech/patts/Frame;->newBuilder()Lcom/google/speech/patts/Frame$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/Frame$Builder;->mergeFrom(Lcom/google/speech/patts/Frame;)Lcom/google/speech/patts/Frame$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->getDefaultInstanceForType()Lcom/google/speech/patts/Frame;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/Frame;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/Frame;->defaultInstance:Lcom/google/speech/patts/Frame;

    return-object v0
.end method

.method public getFirstDaughter()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/speech/patts/Frame;->firstDaughter_:I

    return v0
.end method

.method public getLastDaughter()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/speech/patts/Frame;->lastDaughter_:I

    return v0
.end method

.method public getParent()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/patts/Frame;->parent_:I

    return v0
.end method

.method public getPdfsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/ProbabilityDistribution;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/patts/Frame;->pdfs_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 91
    iget v2, p0, Lcom/google/speech/patts/Frame;->memoizedSerializedSize:I

    .line 92
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 116
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 94
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 95
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->hasParent()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 96
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->getParent()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 99
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->hasFirstDaughter()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 100
    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->getFirstDaughter()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 103
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->hasLastDaughter()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 104
    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->getLastDaughter()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 107
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->getPdfsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/ProbabilityDistribution;

    .line 108
    .local v0, "element":Lspeech/patts/ProbabilityDistribution;
    const/16 v4, 0xa

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 110
    goto :goto_1

    .line 111
    .end local v0    # "element":Lspeech/patts/ProbabilityDistribution;
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->hasVocoderInputs()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 112
    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->getVocoderInputs()Lspeech/patts/VocoderInput;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 115
    :cond_5
    iput v2, p0, Lcom/google/speech/patts/Frame;->memoizedSerializedSize:I

    move v3, v2

    .line 116
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getVocoderInputs()Lspeech/patts/VocoderInput;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/speech/patts/Frame;->vocoderInputs_:Lspeech/patts/VocoderInput;

    return-object v0
.end method

.method public hasFirstDaughter()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/Frame;->hasFirstDaughter:Z

    return v0
.end method

.method public hasLastDaughter()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/patts/Frame;->hasLastDaughter:Z

    return v0
.end method

.method public hasParent()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/Frame;->hasParent:Z

    return v0
.end method

.method public hasVocoderInputs()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/speech/patts/Frame;->hasVocoderInputs:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->toBuilder()Lcom/google/speech/patts/Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/Frame$Builder;
    .locals 1

    .prologue
    .line 191
    invoke-static {p0}, Lcom/google/speech/patts/Frame;->newBuilder(Lcom/google/speech/patts/Frame;)Lcom/google/speech/patts/Frame$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->getSerializedSize()I

    .line 72
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->hasParent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->getParent()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->hasFirstDaughter()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 76
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->getFirstDaughter()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->hasLastDaughter()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 79
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->getLastDaughter()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 81
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->getPdfsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/ProbabilityDistribution;

    .line 82
    .local v0, "element":Lspeech/patts/ProbabilityDistribution;
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 84
    .end local v0    # "element":Lspeech/patts/ProbabilityDistribution;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->hasVocoderInputs()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 85
    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/patts/Frame;->getVocoderInputs()Lspeech/patts/VocoderInput;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 87
    :cond_4
    return-void
.end method
