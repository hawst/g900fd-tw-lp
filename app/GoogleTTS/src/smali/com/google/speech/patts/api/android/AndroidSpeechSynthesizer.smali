.class public Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;
.super Lcom/google/speech/patts/api/common/SpeechSynthesizer;
.source "AndroidSpeechSynthesizer.java"


# instance fields
.field private mAssetManager:Landroid/content/res/AssetManager;

.field private mAssetPackageName:Ljava/lang/String;

.field private mLoadingFromApk:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/speech/patts/api/common/SpeechSynthesizer;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method protected initializeProject(J)J
    .locals 3
    .param p1, "fileResource"    # J

    .prologue
    .line 89
    const-string v0, "patts"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loading project file (apk mode is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->mLoadingFromApk:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "assets from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->mAssetPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v0, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->mAssetManager:Landroid/content/res/AssetManager;

    iget-boolean v1, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->mLoadingFromApk:Z

    invoke-static {p1, p2, v0, v1}, Lcom/google/speech/patts/engine/api/ProjectFileApi;->newProjectFile(JLjava/lang/Object;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method protected loadJni()Z
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Lcom/google/speech/patts/api/android/StaticJniLoader;->loadJni()Z

    move-result v0

    return v0
.end method

.method protected logError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "where"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    return-void
.end method

.method protected logInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "where"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 78
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    return-void
.end method

.method public setAssetManager(Ljava/lang/String;Landroid/content/res/AssetManager;Z)V
    .locals 0
    .param p1, "assetPackageName"    # Ljava/lang/String;
    .param p2, "assetManager"    # Landroid/content/res/AssetManager;
    .param p3, "loadingFromApk"    # Z

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->mAssetPackageName:Ljava/lang/String;

    .line 47
    iput-object p2, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->mAssetManager:Landroid/content/res/AssetManager;

    .line 48
    iput-boolean p3, p0, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->mLoadingFromApk:Z

    .line 49
    return-void
.end method
