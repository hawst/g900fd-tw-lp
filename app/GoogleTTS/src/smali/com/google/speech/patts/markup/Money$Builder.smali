.class public final Lcom/google/speech/patts/markup/Money$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Money.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Money;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/markup/Money;",
        "Lcom/google/speech/patts/markup/Money$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/markup/Money;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 183
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/markup/Money$Builder;
    .locals 1

    .prologue
    .line 177
    invoke-static {}, Lcom/google/speech/patts/markup/Money$Builder;->create()Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/markup/Money$Builder;
    .locals 3

    .prologue
    .line 186
    new-instance v0, Lcom/google/speech/patts/markup/Money$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Money$Builder;-><init>()V

    .line 187
    .local v0, "builder":Lcom/google/speech/patts/markup/Money$Builder;
    new-instance v1, Lcom/google/speech/patts/markup/Money;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/markup/Money;-><init>(Lcom/google/speech/patts/markup/Money$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    .line 188
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money$Builder;->build()Lcom/google/speech/patts/markup/Money;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/markup/Money;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    invoke-static {v0}, Lcom/google/speech/patts/markup/Money$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 219
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money$Builder;->buildPartial()Lcom/google/speech/patts/markup/Money;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/markup/Money;
    .locals 3

    .prologue
    .line 232
    iget-object v1, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    if-nez v1, :cond_0

    .line 233
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    .line 237
    .local v0, "returnMe":Lcom/google/speech/patts/markup/Money;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    .line 238
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money$Builder;->clone()Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money$Builder;->clone()Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/markup/Money$Builder;
    .locals 2

    .prologue
    .line 205
    invoke-static {}, Lcom/google/speech/patts/markup/Money$Builder;->create()Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/markup/Money$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money$Builder;->clone()Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Money;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 177
    check-cast p1, Lcom/google/speech/patts/markup/Money;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/markup/Money$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Money$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/markup/Money;

    .prologue
    .line 242
    invoke-static {}, Lcom/google/speech/patts/markup/Money;->getDefaultInstance()Lcom/google/speech/patts/markup/Money;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 255
    :cond_0
    :goto_0
    return-object p0

    .line 243
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Money;->hasMajor()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Money;->getMajor()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/markup/Money$Builder;->setMajor(J)Lcom/google/speech/patts/markup/Money$Builder;

    .line 246
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Money;->hasMinor()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 247
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Money;->getMinor()Lcom/google/speech/patts/markup/LeadingZeroInt;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Money$Builder;->mergeMinor(Lcom/google/speech/patts/markup/LeadingZeroInt;)Lcom/google/speech/patts/markup/Money$Builder;

    .line 249
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Money;->hasCurrency()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 250
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Money;->getCurrency()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Money$Builder;->setCurrency(Ljava/lang/String;)Lcom/google/speech/patts/markup/Money$Builder;

    .line 252
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Money;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Money;->getStyle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Money$Builder;->setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Money$Builder;

    goto :goto_0
.end method

.method public mergeMinor(Lcom/google/speech/patts/markup/LeadingZeroInt;)Lcom/google/speech/patts/markup/Money$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/LeadingZeroInt;

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Money;->hasMinor()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    # getter for: Lcom/google/speech/patts/markup/Money;->minor_:Lcom/google/speech/patts/markup/LeadingZeroInt;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Money;->access$600(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/LeadingZeroInt;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/LeadingZeroInt;->getDefaultInstance()Lcom/google/speech/patts/markup/LeadingZeroInt;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 340
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    # getter for: Lcom/google/speech/patts/markup/Money;->minor_:Lcom/google/speech/patts/markup/LeadingZeroInt;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Money;->access$600(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/LeadingZeroInt;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/LeadingZeroInt;->newBuilder(Lcom/google/speech/patts/markup/LeadingZeroInt;)Lcom/google/speech/patts/markup/LeadingZeroInt$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/LeadingZeroInt$Builder;->mergeFrom(Lcom/google/speech/patts/markup/LeadingZeroInt;)Lcom/google/speech/patts/markup/LeadingZeroInt$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/LeadingZeroInt$Builder;->buildPartial()Lcom/google/speech/patts/markup/LeadingZeroInt;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Money;->minor_:Lcom/google/speech/patts/markup/LeadingZeroInt;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Money;->access$602(Lcom/google/speech/patts/markup/Money;Lcom/google/speech/patts/markup/LeadingZeroInt;)Lcom/google/speech/patts/markup/LeadingZeroInt;

    .line 345
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Money;->hasMinor:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Money;->access$502(Lcom/google/speech/patts/markup/Money;Z)Z

    .line 346
    return-object p0

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    # setter for: Lcom/google/speech/patts/markup/Money;->minor_:Lcom/google/speech/patts/markup/LeadingZeroInt;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Money;->access$602(Lcom/google/speech/patts/markup/Money;Lcom/google/speech/patts/markup/LeadingZeroInt;)Lcom/google/speech/patts/markup/LeadingZeroInt;

    goto :goto_0
.end method

.method public setCurrency(Ljava/lang/String;)Lcom/google/speech/patts/markup/Money$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 362
    if-nez p1, :cond_0

    .line 363
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Money;->hasCurrency:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Money;->access$702(Lcom/google/speech/patts/markup/Money;Z)Z

    .line 366
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    # setter for: Lcom/google/speech/patts/markup/Money;->currency_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Money;->access$802(Lcom/google/speech/patts/markup/Money;Ljava/lang/String;)Ljava/lang/String;

    .line 367
    return-object p0
.end method

.method public setMajor(J)Lcom/google/speech/patts/markup/Money$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Money;->hasMajor:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Money;->access$302(Lcom/google/speech/patts/markup/Money;Z)Z

    .line 308
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    # setter for: Lcom/google/speech/patts/markup/Money;->major_:J
    invoke-static {v0, p1, p2}, Lcom/google/speech/patts/markup/Money;->access$402(Lcom/google/speech/patts/markup/Money;J)J

    .line 309
    return-object p0
.end method

.method public setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Money$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 383
    if-nez p1, :cond_0

    .line 384
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Money;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Money;->access$902(Lcom/google/speech/patts/markup/Money;Z)Z

    .line 387
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money$Builder;->result:Lcom/google/speech/patts/markup/Money;

    # setter for: Lcom/google/speech/patts/markup/Money;->style_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Money;->access$1002(Lcom/google/speech/patts/markup/Money;Ljava/lang/String;)Ljava/lang/String;

    .line 388
    return-object p0
.end method
