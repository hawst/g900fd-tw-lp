.class public final Lcom/google/speech/patts/Phoneme;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
.source "Phoneme.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/Phoneme$1;,
        Lcom/google/speech/patts/Phoneme$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage",
        "<",
        "Lcom/google/speech/patts/Phoneme;",
        ">;"
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/Phoneme;


# instance fields
.field private alignerStateBoundaries_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private end_:F

.field private firstDaughter_:I

.field private hasEnd:Z

.field private hasFirstDaughter:Z

.field private hasLastDaughter:Z

.field private hasName:Z

.field private hasParent:Z

.field private hasStart:Z

.field private hasStats:Z

.field private hasVowel:Z

.field private lastDaughter_:I

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private parent_:I

.field private start_:F

.field private stats_:Lspeech/patts/PhoneStats;

.field private vowel_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 635
    new-instance v0, Lcom/google/speech/patts/Phoneme;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/Phoneme;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/Phoneme;->defaultInstance:Lcom/google/speech/patts/Phoneme;

    .line 636
    invoke-static {}, Lcom/google/speech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 637
    sget-object v0, Lcom/google/speech/patts/Phoneme;->defaultInstance:Lcom/google/speech/patts/Phoneme;

    invoke-direct {v0}, Lcom/google/speech/patts/Phoneme;->initFields()V

    .line 638
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 26
    iput v1, p0, Lcom/google/speech/patts/Phoneme;->parent_:I

    .line 33
    iput v1, p0, Lcom/google/speech/patts/Phoneme;->firstDaughter_:I

    .line 40
    iput v1, p0, Lcom/google/speech/patts/Phoneme;->lastDaughter_:I

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Phoneme;->name_:Ljava/lang/String;

    .line 54
    iput-boolean v1, p0, Lcom/google/speech/patts/Phoneme;->vowel_:Z

    .line 61
    iput v2, p0, Lcom/google/speech/patts/Phoneme;->start_:F

    .line 68
    iput v2, p0, Lcom/google/speech/patts/Phoneme;->end_:F

    .line 81
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;

    .line 134
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/Phoneme;->memoizedSerializedSize:I

    .line 10
    invoke-direct {p0}, Lcom/google/speech/patts/Phoneme;->initFields()V

    .line 11
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/Phoneme$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/Phoneme$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/Phoneme;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3
    .param p1, "noInit"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 12
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 26
    iput v1, p0, Lcom/google/speech/patts/Phoneme;->parent_:I

    .line 33
    iput v1, p0, Lcom/google/speech/patts/Phoneme;->firstDaughter_:I

    .line 40
    iput v1, p0, Lcom/google/speech/patts/Phoneme;->lastDaughter_:I

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Phoneme;->name_:Ljava/lang/String;

    .line 54
    iput-boolean v1, p0, Lcom/google/speech/patts/Phoneme;->vowel_:Z

    .line 61
    iput v2, p0, Lcom/google/speech/patts/Phoneme;->start_:F

    .line 68
    iput v2, p0, Lcom/google/speech/patts/Phoneme;->end_:F

    .line 81
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;

    .line 134
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/Phoneme;->memoizedSerializedSize:I

    .line 12
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/Phoneme;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Phoneme;->hasName:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/patts/Phoneme;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Phoneme;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/speech/patts/Phoneme;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Phoneme;->hasVowel:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/speech/patts/Phoneme;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Phoneme;->vowel_:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/speech/patts/Phoneme;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Phoneme;->hasStart:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/patts/Phoneme;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Phoneme;->start_:F

    return p1
.end method

.method static synthetic access$1602(Lcom/google/speech/patts/Phoneme;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Phoneme;->hasEnd:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/patts/Phoneme;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Phoneme;->end_:F

    return p1
.end method

.method static synthetic access$1802(Lcom/google/speech/patts/Phoneme;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Phoneme;->hasStats:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/google/speech/patts/Phoneme;)Lspeech/patts/PhoneStats;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme;->stats_:Lspeech/patts/PhoneStats;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/google/speech/patts/Phoneme;Lspeech/patts/PhoneStats;)Lspeech/patts/PhoneStats;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # Lspeech/patts/PhoneStats;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Phoneme;->stats_:Lspeech/patts/PhoneStats;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/patts/Phoneme;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/patts/Phoneme;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/Phoneme;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Phoneme;->hasParent:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/Phoneme;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Phoneme;->parent_:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/Phoneme;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Phoneme;->hasFirstDaughter:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/Phoneme;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Phoneme;->firstDaughter_:I

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/Phoneme;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Phoneme;->hasLastDaughter:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/Phoneme;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Phoneme;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Phoneme;->lastDaughter_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/Phoneme;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/speech/patts/Phoneme;->defaultInstance:Lcom/google/speech/patts/Phoneme;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 92
    invoke-static {}, Lspeech/patts/PhoneStats;->getDefaultInstance()Lspeech/patts/PhoneStats;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/Phoneme;->stats_:Lspeech/patts/PhoneStats;

    .line 93
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/Phoneme$Builder;
    .locals 1

    .prologue
    .line 250
    # invokes: Lcom/google/speech/patts/Phoneme$Builder;->create()Lcom/google/speech/patts/Phoneme$Builder;
    invoke-static {}, Lcom/google/speech/patts/Phoneme$Builder;->access$100()Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/Phoneme;)Lcom/google/speech/patts/Phoneme$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/Phoneme;

    .prologue
    .line 253
    invoke-static {}, Lcom/google/speech/patts/Phoneme;->newBuilder()Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/Phoneme$Builder;->mergeFrom(Lcom/google/speech/patts/Phoneme;)Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAlignerStateBoundariesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme;->alignerStateBoundaries_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getDefaultInstanceForType()Lcom/google/speech/patts/Phoneme;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/Phoneme;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/speech/patts/Phoneme;->defaultInstance:Lcom/google/speech/patts/Phoneme;

    return-object v0
.end method

.method public getEnd()F
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/google/speech/patts/Phoneme;->end_:F

    return v0
.end method

.method public getFirstDaughter()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/google/speech/patts/Phoneme;->firstDaughter_:I

    return v0
.end method

.method public getLastDaughter()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/speech/patts/Phoneme;->lastDaughter_:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getParent()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/speech/patts/Phoneme;->parent_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 136
    iget v1, p0, Lcom/google/speech/patts/Phoneme;->memoizedSerializedSize:I

    .line 137
    .local v1, "size":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 180
    .end local v1    # "size":I
    .local v2, "size":I
    :goto_0
    return v2

    .line 139
    .end local v2    # "size":I
    .restart local v1    # "size":I
    :cond_0
    const/4 v1, 0x0

    .line 140
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasParent()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 141
    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getParent()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 144
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasFirstDaughter()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 145
    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getFirstDaughter()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 148
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasLastDaughter()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 149
    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getLastDaughter()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 152
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasName()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 153
    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 156
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasVowel()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 157
    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getVowel()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v1, v3

    .line 160
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasStart()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 161
    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getStart()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v1, v3

    .line 164
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasEnd()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 165
    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getEnd()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v1, v3

    .line 168
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasStats()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 169
    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getStats()Lspeech/patts/PhoneStats;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 173
    :cond_8
    const/4 v0, 0x0

    .line 174
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getAlignerStateBoundariesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 175
    add-int/2addr v1, v0

    .line 176
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getAlignerStateBoundariesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v1, v3

    .line 178
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->extensionsSerializedSize()I

    move-result v3

    add-int/2addr v1, v3

    .line 179
    iput v1, p0, Lcom/google/speech/patts/Phoneme;->memoizedSerializedSize:I

    move v2, v1

    .line 180
    .end local v1    # "size":I
    .restart local v2    # "size":I
    goto/16 :goto_0
.end method

.method public getStart()F
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/google/speech/patts/Phoneme;->start_:F

    return v0
.end method

.method public getStats()Lspeech/patts/PhoneStats;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/speech/patts/Phoneme;->stats_:Lspeech/patts/PhoneStats;

    return-object v0
.end method

.method public getVowel()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/speech/patts/Phoneme;->vowel_:Z

    return v0
.end method

.method public hasEnd()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/speech/patts/Phoneme;->hasEnd:Z

    return v0
.end method

.method public hasFirstDaughter()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/speech/patts/Phoneme;->hasFirstDaughter:Z

    return v0
.end method

.method public hasLastDaughter()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/speech/patts/Phoneme;->hasLastDaughter:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/speech/patts/Phoneme;->hasName:Z

    return v0
.end method

.method public hasParent()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/speech/patts/Phoneme;->hasParent:Z

    return v0
.end method

.method public hasStart()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/speech/patts/Phoneme;->hasStart:Z

    return v0
.end method

.method public hasStats()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/speech/patts/Phoneme;->hasStats:Z

    return v0
.end method

.method public hasVowel()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/speech/patts/Phoneme;->hasVowel:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->extensionsAreInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 96
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->toBuilder()Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/Phoneme$Builder;
    .locals 1

    .prologue
    .line 255
    invoke-static {p0}, Lcom/google/speech/patts/Phoneme;->newBuilder(Lcom/google/speech/patts/Phoneme;)Lcom/google/speech/patts/Phoneme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getSerializedSize()I

    .line 103
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->newExtensionWriter()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;

    move-result-object v1

    .line 104
    .local v1, "extensionWriter":Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasParent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 105
    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getParent()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 107
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasFirstDaughter()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 108
    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getFirstDaughter()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 110
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasLastDaughter()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 111
    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getLastDaughter()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 113
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasName()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 114
    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 116
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasVowel()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 117
    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getVowel()Z

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 119
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasStart()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 120
    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getStart()F

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 122
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasEnd()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 123
    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getEnd()F

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 125
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->hasStats()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 126
    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getStats()Lspeech/patts/PhoneStats;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 128
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/Phoneme;->getAlignerStateBoundariesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 129
    .local v0, "element":F
    const/16 v3, 0x9

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    goto :goto_0

    .line 131
    .end local v0    # "element":F
    :cond_8
    const/high16 v3, 0x20000000

    invoke-virtual {v1, v3, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;->writeUntil(ILcom/google/protobuf/CodedOutputStream;)V

    .line 132
    return-void
.end method
