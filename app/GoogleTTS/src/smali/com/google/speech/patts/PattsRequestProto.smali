.class public final Lcom/google/speech/patts/PattsRequestProto;
.super Ljava/lang/Object;
.source "PattsRequestProto.java"


# static fields
.field public static final pattsRequest:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension",
            "<",
            "Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;",
            "Lcom/google/speech/patts/PattsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final pattsResponse:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension",
            "<",
            "Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;",
            "Lcom/google/speech/patts/PattsResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 17
    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->newGeneratedExtension()Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    move-result-object v0

    sput-object v0, Lcom/google/speech/patts/PattsRequestProto;->pattsRequest:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    .line 24
    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->newGeneratedExtension()Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    move-result-object v0

    sput-object v0, Lcom/google/speech/patts/PattsRequestProto;->pattsResponse:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    .line 29
    sget-object v0, Lcom/google/speech/patts/PattsRequestProto;->pattsRequest:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    invoke-static {}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->getDefaultInstance()Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    move-result-object v1

    invoke-static {}, Lcom/google/speech/patts/PattsRequest;->getDefaultInstance()Lcom/google/speech/patts/PattsRequest;

    move-result-object v2

    invoke-static {}, Lcom/google/speech/patts/PattsRequest;->getDefaultInstance()Lcom/google/speech/patts/PattsRequest;

    move-result-object v3

    const v5, 0x199c701

    sget-object v6, Lcom/google/protobuf/WireFormat$FieldType;->MESSAGE:Lcom/google/protobuf/WireFormat$FieldType;

    invoke-virtual/range {v0 .. v6}, Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;->internalInitSingular(Lcom/google/protobuf/MessageLite;Ljava/lang/Object;Lcom/google/protobuf/MessageLite;Lcom/google/protobuf/Internal$EnumLiteMap;ILcom/google/protobuf/WireFormat$FieldType;)V

    .line 36
    sget-object v0, Lcom/google/speech/patts/PattsRequestProto;->pattsResponse:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    invoke-static {}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;->getDefaultInstance()Lcom/google/speech/synthesizer/SynthesisEngineSpecificResponse;

    move-result-object v1

    invoke-static {}, Lcom/google/speech/patts/PattsResponse;->getDefaultInstance()Lcom/google/speech/patts/PattsResponse;

    move-result-object v2

    invoke-static {}, Lcom/google/speech/patts/PattsResponse;->getDefaultInstance()Lcom/google/speech/patts/PattsResponse;

    move-result-object v3

    const v5, 0x4a28e2c

    sget-object v6, Lcom/google/protobuf/WireFormat$FieldType;->MESSAGE:Lcom/google/protobuf/WireFormat$FieldType;

    invoke-virtual/range {v0 .. v6}, Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;->internalInitSingular(Lcom/google/protobuf/MessageLite;Ljava/lang/Object;Lcom/google/protobuf/MessageLite;Lcom/google/protobuf/Internal$EnumLiteMap;ILcom/google/protobuf/WireFormat$FieldType;)V

    .line 43
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static internalForceInit()V
    .locals 0

    .prologue
    .line 45
    return-void
.end method
