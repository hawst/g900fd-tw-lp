.class public final Lcom/google/speech/patts/markup/Url;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Url.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/markup/Url$1;,
        Lcom/google/speech/patts/markup/Url$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/markup/Url;


# instance fields
.field private domain_:Ljava/lang/String;

.field private hasDomain:Z

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 252
    new-instance v0, Lcom/google/speech/patts/markup/Url;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/markup/Url;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/markup/Url;->defaultInstance:Lcom/google/speech/patts/markup/Url;

    .line 253
    invoke-static {}, Lcom/google/speech/patts/markup/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 254
    sget-object v0, Lcom/google/speech/patts/markup/Url;->defaultInstance:Lcom/google/speech/patts/markup/Url;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Url;->initFields()V

    .line 255
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Url;->domain_:Ljava/lang/String;

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Url;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Url;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/markup/Url$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/markup/Url$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Url;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Url;->domain_:Ljava/lang/String;

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Url;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/patts/markup/Url;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Url;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Url;->hasDomain:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/markup/Url;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Url;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Url;->domain_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/markup/Url;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/markup/Url;->defaultInstance:Lcom/google/speech/patts/markup/Url;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/markup/Url$Builder;
    .locals 1

    .prologue
    .line 124
    # invokes: Lcom/google/speech/patts/markup/Url$Builder;->create()Lcom/google/speech/patts/markup/Url$Builder;
    invoke-static {}, Lcom/google/speech/patts/markup/Url$Builder;->access$100()Lcom/google/speech/patts/markup/Url$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/markup/Url;)Lcom/google/speech/patts/markup/Url$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/markup/Url;

    .prologue
    .line 127
    invoke-static {}, Lcom/google/speech/patts/markup/Url;->newBuilder()Lcom/google/speech/patts/markup/Url$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/markup/Url$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Url;)Lcom/google/speech/patts/markup/Url$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Url;->getDefaultInstanceForType()Lcom/google/speech/patts/markup/Url;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/markup/Url;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/markup/Url;->defaultInstance:Lcom/google/speech/patts/markup/Url;

    return-object v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/markup/Url;->domain_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 45
    iget v0, p0, Lcom/google/speech/patts/markup/Url;->memoizedSerializedSize:I

    .line 46
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 54
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 48
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Url;->hasDomain()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 50
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Url;->getDomain()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53
    :cond_1
    iput v0, p0, Lcom/google/speech/patts/markup/Url;->memoizedSerializedSize:I

    move v1, v0

    .line 54
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasDomain()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Url;->hasDomain:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Url;->toBuilder()Lcom/google/speech/patts/markup/Url$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/markup/Url$Builder;
    .locals 1

    .prologue
    .line 129
    invoke-static {p0}, Lcom/google/speech/patts/markup/Url;->newBuilder(Lcom/google/speech/patts/markup/Url;)Lcom/google/speech/patts/markup/Url$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Url;->getSerializedSize()I

    .line 38
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Url;->hasDomain()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Url;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 41
    :cond_0
    return-void
.end method
