.class public final Lcom/google/speech/patts/UtteranceP;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
.source "UtteranceP.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/UtteranceP$1;,
        Lcom/google/speech/patts/UtteranceP$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage",
        "<",
        "Lcom/google/speech/patts/UtteranceP;",
        ">;"
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/UtteranceP;


# instance fields
.field private filename_:Ljava/lang/String;

.field private frames_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/Frame;",
            ">;"
        }
    .end annotation
.end field

.field private hasFilename:Z

.field private hasId:Z

.field private hasInput:Z

.field private hasLattice:Z

.field private hasStats:Z

.field private id_:J

.field private input_:Lcom/google/speech/tts/Sentence;

.field private lattice_:Lspeech/patts/Lattice;

.field private memoizedSerializedSize:I

.field private phonemes_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/Phoneme;",
            ">;"
        }
    .end annotation
.end field

.field private specifications_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/Specification;",
            ">;"
        }
    .end annotation
.end field

.field private stats_:Lspeech/patts/UtteranceStats;

.field private syllables_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/Syllable;",
            ">;"
        }
    .end annotation
.end field

.field private tokens_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Token;",
            ">;"
        }
    .end annotation
.end field

.field private words_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Word;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1021
    new-instance v0, Lcom/google/speech/patts/UtteranceP;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/UtteranceP;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/UtteranceP;->defaultInstance:Lcom/google/speech/patts/UtteranceP;

    .line 1022
    invoke-static {}, Lcom/google/speech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 1023
    sget-object v0, Lcom/google/speech/patts/UtteranceP;->defaultInstance:Lcom/google/speech/patts/UtteranceP;

    invoke-direct {v0}, Lcom/google/speech/patts/UtteranceP;->initFields()V

    .line 1024
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 26
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/patts/UtteranceP;->id_:J

    .line 39
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;

    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;

    .line 63
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;

    .line 75
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;

    .line 87
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;

    .line 99
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;

    .line 119
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->filename_:Ljava/lang/String;

    .line 196
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/UtteranceP;->memoizedSerializedSize:I

    .line 10
    invoke-direct {p0}, Lcom/google/speech/patts/UtteranceP;->initFields()V

    .line 11
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/UtteranceP$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/UtteranceP$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/UtteranceP;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 26
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/patts/UtteranceP;->id_:J

    .line 39
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;

    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;

    .line 63
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;

    .line 75
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;

    .line 87
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;

    .line 99
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;

    .line 119
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->filename_:Ljava/lang/String;

    .line 196
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/UtteranceP;->memoizedSerializedSize:I

    .line 12
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/UtteranceP;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # J

    .prologue
    .line 5
    iput-wide p1, p0, Lcom/google/speech/patts/UtteranceP;->id_:J

    return-wide p1
.end method

.method static synthetic access$1102(Lcom/google/speech/patts/UtteranceP;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/UtteranceP;->hasInput:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/tts/Sentence;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->input_:Lcom/google/speech/tts/Sentence;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/speech/patts/UtteranceP;Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Lcom/google/speech/tts/Sentence;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/UtteranceP;->input_:Lcom/google/speech/tts/Sentence;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/google/speech/patts/UtteranceP;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/UtteranceP;->hasLattice:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/google/speech/patts/UtteranceP;)Lspeech/patts/Lattice;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->lattice_:Lspeech/patts/Lattice;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/google/speech/patts/UtteranceP;Lspeech/patts/Lattice;)Lspeech/patts/Lattice;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Lspeech/patts/Lattice;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/UtteranceP;->lattice_:Lspeech/patts/Lattice;

    return-object p1
.end method

.method static synthetic access$1502(Lcom/google/speech/patts/UtteranceP;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/UtteranceP;->hasFilename:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/google/speech/patts/UtteranceP;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/UtteranceP;->filename_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1702(Lcom/google/speech/patts/UtteranceP;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/UtteranceP;->hasStats:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/google/speech/patts/UtteranceP;)Lspeech/patts/UtteranceStats;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->stats_:Lspeech/patts/UtteranceStats;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/google/speech/patts/UtteranceP;Lspeech/patts/UtteranceStats;)Lspeech/patts/UtteranceStats;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Lspeech/patts/UtteranceStats;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/UtteranceP;->stats_:Lspeech/patts/UtteranceStats;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/speech/patts/UtteranceP;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/speech/patts/UtteranceP;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/UtteranceP;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/UtteranceP;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/UtteranceP;->hasId:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/UtteranceP;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/speech/patts/UtteranceP;->defaultInstance:Lcom/google/speech/patts/UtteranceP;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 131
    invoke-static {}, Lcom/google/speech/tts/Sentence;->getDefaultInstance()Lcom/google/speech/tts/Sentence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->input_:Lcom/google/speech/tts/Sentence;

    .line 132
    invoke-static {}, Lspeech/patts/Lattice;->getDefaultInstance()Lspeech/patts/Lattice;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->lattice_:Lspeech/patts/Lattice;

    .line 133
    invoke-static {}, Lspeech/patts/UtteranceStats;->getDefaultInstance()Lspeech/patts/UtteranceStats;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/UtteranceP;->stats_:Lspeech/patts/UtteranceStats;

    .line 134
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/UtteranceP$Builder;
    .locals 1

    .prologue
    .line 318
    # invokes: Lcom/google/speech/patts/UtteranceP$Builder;->create()Lcom/google/speech/patts/UtteranceP$Builder;
    invoke-static {}, Lcom/google/speech/patts/UtteranceP$Builder;->access$100()Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/patts/UtteranceP$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/UtteranceP;

    .prologue
    .line 321
    invoke-static {}, Lcom/google/speech/patts/UtteranceP;->newBuilder()Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/UtteranceP$Builder;->mergeFrom(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getDefaultInstanceForType()Lcom/google/speech/patts/UtteranceP;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/UtteranceP;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/speech/patts/UtteranceP;->defaultInstance:Lcom/google/speech/patts/UtteranceP;

    return-object v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->filename_:Ljava/lang/String;

    return-object v0
.end method

.method public getFramesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/Frame;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->frames_:Ljava/util/List;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/google/speech/patts/UtteranceP;->id_:J

    return-wide v0
.end method

.method public getInput()Lcom/google/speech/tts/Sentence;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->input_:Lcom/google/speech/tts/Sentence;

    return-object v0
.end method

.method public getLattice()Lspeech/patts/Lattice;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->lattice_:Lspeech/patts/Lattice;

    return-object v0
.end method

.method public getPhonemesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/Phoneme;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->phonemes_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    .line 198
    iget v2, p0, Lcom/google/speech/patts/UtteranceP;->memoizedSerializedSize:I

    .line 199
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 248
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 201
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 202
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->hasId()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 203
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getId()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 206
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->hasInput()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 207
    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getInput()Lcom/google/speech/tts/Sentence;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 210
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getTokensList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Token;

    .line 211
    .local v0, "element":Lspeech/patts/Token;
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 213
    goto :goto_1

    .line 214
    .end local v0    # "element":Lspeech/patts/Token;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getWordsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Word;

    .line 215
    .local v0, "element":Lspeech/patts/Word;
    const/4 v4, 0x4

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 217
    goto :goto_2

    .line 218
    .end local v0    # "element":Lspeech/patts/Word;
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getSyllablesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/Syllable;

    .line 219
    .local v0, "element":Lcom/google/speech/patts/Syllable;
    const/4 v4, 0x5

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 221
    goto :goto_3

    .line 222
    .end local v0    # "element":Lcom/google/speech/patts/Syllable;
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getPhonemesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/Phoneme;

    .line 223
    .local v0, "element":Lcom/google/speech/patts/Phoneme;
    const/4 v4, 0x6

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 225
    goto :goto_4

    .line 226
    .end local v0    # "element":Lcom/google/speech/patts/Phoneme;
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getSpecificationsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/Specification;

    .line 227
    .local v0, "element":Lcom/google/speech/patts/Specification;
    const/4 v4, 0x7

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 229
    goto :goto_5

    .line 230
    .end local v0    # "element":Lcom/google/speech/patts/Specification;
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getFramesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/Frame;

    .line 231
    .local v0, "element":Lcom/google/speech/patts/Frame;
    const/16 v4, 0x8

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 233
    goto :goto_6

    .line 234
    .end local v0    # "element":Lcom/google/speech/patts/Frame;
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->hasLattice()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 235
    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getLattice()Lspeech/patts/Lattice;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 238
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->hasFilename()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 239
    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getFilename()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 242
    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->hasStats()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 243
    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getStats()Lspeech/patts/UtteranceStats;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 246
    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->extensionsSerializedSize()I

    move-result v4

    add-int/2addr v2, v4

    .line 247
    iput v2, p0, Lcom/google/speech/patts/UtteranceP;->memoizedSerializedSize:I

    move v3, v2

    .line 248
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto/16 :goto_0
.end method

.method public getSpecificationsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/Specification;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->specifications_:Ljava/util/List;

    return-object v0
.end method

.method public getStats()Lspeech/patts/UtteranceStats;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->stats_:Lspeech/patts/UtteranceStats;

    return-object v0
.end method

.method public getSyllablesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/patts/Syllable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->syllables_:Ljava/util/List;

    return-object v0
.end method

.method public getTokensList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Token;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->tokens_:Ljava/util/List;

    return-object v0
.end method

.method public getWordsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Word;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/speech/patts/UtteranceP;->words_:Ljava/util/List;

    return-object v0
.end method

.method public hasFilename()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/google/speech/patts/UtteranceP;->hasFilename:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/speech/patts/UtteranceP;->hasId:Z

    return v0
.end method

.method public hasInput()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/speech/patts/UtteranceP;->hasInput:Z

    return v0
.end method

.method public hasLattice()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/google/speech/patts/UtteranceP;->hasLattice:Z

    return v0
.end method

.method public hasStats()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/google/speech/patts/UtteranceP;->hasStats:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 136
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->hasInput()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 137
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getInput()Lcom/google/speech/tts/Sentence;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/Sentence;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1

    .line 152
    :cond_0
    :goto_0
    return v2

    .line 139
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getTokensList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Token;

    .line 140
    .local v0, "element":Lspeech/patts/Token;
    invoke-virtual {v0}, Lspeech/patts/Token;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 142
    .end local v0    # "element":Lspeech/patts/Token;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getWordsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Word;

    .line 143
    .local v0, "element":Lspeech/patts/Word;
    invoke-virtual {v0}, Lspeech/patts/Word;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_4

    goto :goto_0

    .line 145
    .end local v0    # "element":Lspeech/patts/Word;
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getPhonemesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/Phoneme;

    .line 146
    .local v0, "element":Lcom/google/speech/patts/Phoneme;
    invoke-virtual {v0}, Lcom/google/speech/patts/Phoneme;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_6

    goto :goto_0

    .line 148
    .end local v0    # "element":Lcom/google/speech/patts/Phoneme;
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getSpecificationsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/Specification;

    .line 149
    .local v0, "element":Lcom/google/speech/patts/Specification;
    invoke-virtual {v0}, Lcom/google/speech/patts/Specification;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_8

    goto :goto_0

    .line 151
    .end local v0    # "element":Lcom/google/speech/patts/Specification;
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->extensionsAreInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 152
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->toBuilder()Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/UtteranceP$Builder;
    .locals 1

    .prologue
    .line 323
    invoke-static {p0}, Lcom/google/speech/patts/UtteranceP;->newBuilder(Lcom/google/speech/patts/UtteranceP;)Lcom/google/speech/patts/UtteranceP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getSerializedSize()I

    .line 159
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->newExtensionWriter()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;

    move-result-object v1

    .line 160
    .local v1, "extensionWriter":Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->hasId()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 161
    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getId()J

    move-result-wide v4

    invoke-virtual {p1, v3, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 163
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->hasInput()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 164
    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getInput()Lcom/google/speech/tts/Sentence;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 166
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getTokensList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Token;

    .line 167
    .local v0, "element":Lspeech/patts/Token;
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 169
    .end local v0    # "element":Lspeech/patts/Token;
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getWordsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Word;

    .line 170
    .local v0, "element":Lspeech/patts/Word;
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 172
    .end local v0    # "element":Lspeech/patts/Word;
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getSyllablesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/Syllable;

    .line 173
    .local v0, "element":Lcom/google/speech/patts/Syllable;
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    .line 175
    .end local v0    # "element":Lcom/google/speech/patts/Syllable;
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getPhonemesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/Phoneme;

    .line 176
    .local v0, "element":Lcom/google/speech/patts/Phoneme;
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    .line 178
    .end local v0    # "element":Lcom/google/speech/patts/Phoneme;
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getSpecificationsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/Specification;

    .line 179
    .local v0, "element":Lcom/google/speech/patts/Specification;
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_4

    .line 181
    .end local v0    # "element":Lcom/google/speech/patts/Specification;
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getFramesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/patts/Frame;

    .line 182
    .local v0, "element":Lcom/google/speech/patts/Frame;
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_5

    .line 184
    .end local v0    # "element":Lcom/google/speech/patts/Frame;
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->hasLattice()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 185
    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getLattice()Lspeech/patts/Lattice;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 187
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->hasFilename()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 188
    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 190
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->hasStats()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 191
    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/speech/patts/UtteranceP;->getStats()Lspeech/patts/UtteranceStats;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 193
    :cond_a
    const/high16 v3, 0x20000000

    invoke-virtual {v1, v3, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;->writeUntil(ILcom/google/protobuf/CodedOutputStream;)V

    .line 194
    return-void
.end method
