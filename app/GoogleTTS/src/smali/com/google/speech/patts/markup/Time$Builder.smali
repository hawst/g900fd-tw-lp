.class public final Lcom/google/speech/patts/markup/Time$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Time.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Time;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/markup/Time;",
        "Lcom/google/speech/patts/markup/Time$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/markup/Time;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/markup/Time$Builder;
    .locals 1

    .prologue
    .line 187
    invoke-static {}, Lcom/google/speech/patts/markup/Time$Builder;->create()Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/markup/Time$Builder;
    .locals 3

    .prologue
    .line 196
    new-instance v0, Lcom/google/speech/patts/markup/Time$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Time$Builder;-><init>()V

    .line 197
    .local v0, "builder":Lcom/google/speech/patts/markup/Time$Builder;
    new-instance v1, Lcom/google/speech/patts/markup/Time;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/markup/Time;-><init>(Lcom/google/speech/patts/markup/Time$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    .line 198
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time$Builder;->build()Lcom/google/speech/patts/markup/Time;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/markup/Time;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    invoke-static {v0}, Lcom/google/speech/patts/markup/Time$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 229
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time$Builder;->buildPartial()Lcom/google/speech/patts/markup/Time;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/markup/Time;
    .locals 3

    .prologue
    .line 242
    iget-object v1, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    if-nez v1, :cond_0

    .line 243
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    .line 247
    .local v0, "returnMe":Lcom/google/speech/patts/markup/Time;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    .line 248
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time$Builder;->clone()Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time$Builder;->clone()Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/markup/Time$Builder;
    .locals 2

    .prologue
    .line 215
    invoke-static {}, Lcom/google/speech/patts/markup/Time$Builder;->create()Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/markup/Time$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Time$Builder;->clone()Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Time;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 187
    check-cast p1, Lcom/google/speech/patts/markup/Time;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/markup/Time$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Time$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/markup/Time;)Lcom/google/speech/patts/markup/Time$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/patts/markup/Time;

    .prologue
    .line 252
    invoke-static {}, Lcom/google/speech/patts/markup/Time;->getDefaultInstance()Lcom/google/speech/patts/markup/Time;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-object p0

    .line 253
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Time;->hasHours()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 254
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Time;->getHours()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Time$Builder;->setHours(I)Lcom/google/speech/patts/markup/Time$Builder;

    .line 256
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Time;->hasMinutes()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 257
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Time;->getMinutes()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Time$Builder;->setMinutes(I)Lcom/google/speech/patts/markup/Time$Builder;

    .line 259
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Time;->hasSeconds()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 260
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Time;->getSeconds()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Time$Builder;->setSeconds(I)Lcom/google/speech/patts/markup/Time$Builder;

    .line 262
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Time;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 263
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Time;->getStyle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Time$Builder;->setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Time$Builder;

    .line 265
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Time;->hasZone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Time;->getZone()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Time$Builder;->setZone(Ljava/lang/String;)Lcom/google/speech/patts/markup/Time$Builder;

    goto :goto_0
.end method

.method public setHours(I)Lcom/google/speech/patts/markup/Time$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Time;->hasHours:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Time;->access$302(Lcom/google/speech/patts/markup/Time;Z)Z

    .line 320
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    # setter for: Lcom/google/speech/patts/markup/Time;->hours_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Time;->access$402(Lcom/google/speech/patts/markup/Time;I)I

    .line 321
    return-object p0
.end method

.method public setMinutes(I)Lcom/google/speech/patts/markup/Time$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Time;->hasMinutes:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Time;->access$502(Lcom/google/speech/patts/markup/Time;Z)Z

    .line 338
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    # setter for: Lcom/google/speech/patts/markup/Time;->minutes_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Time;->access$602(Lcom/google/speech/patts/markup/Time;I)I

    .line 339
    return-object p0
.end method

.method public setSeconds(I)Lcom/google/speech/patts/markup/Time$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Time;->hasSeconds:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Time;->access$702(Lcom/google/speech/patts/markup/Time;Z)Z

    .line 356
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    # setter for: Lcom/google/speech/patts/markup/Time;->seconds_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Time;->access$802(Lcom/google/speech/patts/markup/Time;I)I

    .line 357
    return-object p0
.end method

.method public setStyle(Ljava/lang/String;)Lcom/google/speech/patts/markup/Time$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 373
    if-nez p1, :cond_0

    .line 374
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Time;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Time;->access$902(Lcom/google/speech/patts/markup/Time;Z)Z

    .line 377
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    # setter for: Lcom/google/speech/patts/markup/Time;->style_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Time;->access$1002(Lcom/google/speech/patts/markup/Time;Ljava/lang/String;)Ljava/lang/String;

    .line 378
    return-object p0
.end method

.method public setZone(Ljava/lang/String;)Lcom/google/speech/patts/markup/Time$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 394
    if-nez p1, :cond_0

    .line 395
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 397
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Time;->hasZone:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Time;->access$1102(Lcom/google/speech/patts/markup/Time;Z)Z

    .line 398
    iget-object v0, p0, Lcom/google/speech/patts/markup/Time$Builder;->result:Lcom/google/speech/patts/markup/Time;

    # setter for: Lcom/google/speech/patts/markup/Time;->zone_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Time;->access$1202(Lcom/google/speech/patts/markup/Time;Ljava/lang/String;)Ljava/lang/String;

    .line 399
    return-object p0
.end method
