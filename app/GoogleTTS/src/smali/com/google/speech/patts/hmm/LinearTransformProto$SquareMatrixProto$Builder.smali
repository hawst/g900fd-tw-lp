.class public final Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LinearTransformProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;",
        "Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;
    .locals 1

    .prologue
    .line 165
    invoke-static {}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->create()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;
    .locals 3

    .prologue
    .line 174
    new-instance v0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;-><init>()V

    .line 175
    .local v0, "builder":Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;
    new-instance v1, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;-><init>(Lcom/google/speech/patts/hmm/LinearTransformProto$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    .line 176
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->build()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    invoke-static {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 207
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->buildPartial()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
    .locals 3

    .prologue
    .line 220
    iget-object v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    if-nez v1, :cond_0

    .line 221
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 224
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->access$300(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 225
    iget-object v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    iget-object v2, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->access$300(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->access$302(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;Ljava/util/List;)Ljava/util/List;

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    .line 229
    .local v0, "returnMe":Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    .line 230
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->clone()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->clone()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;
    .locals 2

    .prologue
    .line 193
    invoke-static {}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->create()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->clone()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    invoke-virtual {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 165
    check-cast p1, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    .prologue
    .line 234
    invoke-static {}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->getDefaultInstance()Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-object p0

    .line 235
    :cond_1
    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->access$300(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->access$300(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 237
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->access$302(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;Ljava/util/List;)Ljava/util/List;

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto$Builder;->result:Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->access$300(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->vals_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;->access$300(Lcom/google/speech/patts/hmm/LinearTransformProto$SquareMatrixProto;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
