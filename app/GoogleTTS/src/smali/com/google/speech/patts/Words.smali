.class public final Lcom/google/speech/patts/Words;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Words.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/Words$1;,
        Lcom/google/speech/patts/Words$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/Words;


# instance fields
.field private memoizedSerializedSize:I

.field private words_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Word;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 299
    new-instance v0, Lcom/google/speech/patts/Words;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/Words;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/Words;->defaultInstance:Lcom/google/speech/patts/Words;

    .line 300
    invoke-static {}, Lcom/google/speech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 301
    sget-object v0, Lcom/google/speech/patts/Words;->defaultInstance:Lcom/google/speech/patts/Words;

    invoke-direct {v0}, Lcom/google/speech/patts/Words;->initFields()V

    .line 302
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/Words;->words_:Ljava/util/List;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/Words;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/Words;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/Words$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/Words$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/Words;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/Words;->words_:Ljava/util/List;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/Words;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lcom/google/speech/patts/Words;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/Words;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/Words;->words_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/speech/patts/Words;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Words;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Words;->words_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/Words;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/Words;->defaultInstance:Lcom/google/speech/patts/Words;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/Words$Builder;
    .locals 1

    .prologue
    .line 132
    # invokes: Lcom/google/speech/patts/Words$Builder;->create()Lcom/google/speech/patts/Words$Builder;
    invoke-static {}, Lcom/google/speech/patts/Words$Builder;->access$100()Lcom/google/speech/patts/Words$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/Words;)Lcom/google/speech/patts/Words$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/Words;

    .prologue
    .line 135
    invoke-static {}, Lcom/google/speech/patts/Words;->newBuilder()Lcom/google/speech/patts/Words$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/Words$Builder;->mergeFrom(Lcom/google/speech/patts/Words;)Lcom/google/speech/patts/Words$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/Words;->getDefaultInstanceForType()Lcom/google/speech/patts/Words;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/Words;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/Words;->defaultInstance:Lcom/google/speech/patts/Words;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 53
    iget v2, p0, Lcom/google/speech/patts/Words;->memoizedSerializedSize:I

    .line 54
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 62
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 56
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 57
    invoke-virtual {p0}, Lcom/google/speech/patts/Words;->getWordsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Word;

    .line 58
    .local v0, "element":Lspeech/patts/Word;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 60
    goto :goto_1

    .line 61
    .end local v0    # "element":Lspeech/patts/Word;
    :cond_1
    iput v2, p0, Lcom/google/speech/patts/Words;->memoizedSerializedSize:I

    move v3, v2

    .line 62
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getWordsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Word;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/Words;->words_:Ljava/util/List;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/speech/patts/Words;->getWordsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Word;

    .line 38
    .local v0, "element":Lspeech/patts/Word;
    invoke-virtual {v0}, Lspeech/patts/Word;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 40
    .end local v0    # "element":Lspeech/patts/Word;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/Words;->toBuilder()Lcom/google/speech/patts/Words$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/Words$Builder;
    .locals 1

    .prologue
    .line 137
    invoke-static {p0}, Lcom/google/speech/patts/Words;->newBuilder(Lcom/google/speech/patts/Words;)Lcom/google/speech/patts/Words$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/speech/patts/Words;->getSerializedSize()I

    .line 46
    invoke-virtual {p0}, Lcom/google/speech/patts/Words;->getWordsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Word;

    .line 47
    .local v0, "element":Lspeech/patts/Word;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 49
    .end local v0    # "element":Lspeech/patts/Word;
    :cond_0
    return-void
.end method
