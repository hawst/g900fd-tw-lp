.class public final Lcom/google/speech/patts/markup/Style;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Style.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/markup/Style$1;,
        Lcom/google/speech/patts/markup/Style$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/markup/Style;


# instance fields
.field private date_:Ljava/lang/String;

.field private fraction_:Ljava/lang/String;

.field private hasDate:Z

.field private hasFraction:Z

.field private hasMeasure:Z

.field private hasMoney:Z

.field private hasTelephone:Z

.field private hasTime:Z

.field private measure_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private money_:Ljava/lang/String;

.field private telephone_:Ljava/lang/String;

.field private time_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 462
    new-instance v0, Lcom/google/speech/patts/markup/Style;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/markup/Style;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/markup/Style;->defaultInstance:Lcom/google/speech/patts/markup/Style;

    .line 463
    invoke-static {}, Lcom/google/speech/patts/markup/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 464
    sget-object v0, Lcom/google/speech/patts/markup/Style;->defaultInstance:Lcom/google/speech/patts/markup/Style;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Style;->initFields()V

    .line 465
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Style;->fraction_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Style;->time_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Style;->money_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Style;->measure_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Style;->telephone_:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Style;->date_:Ljava/lang/String;

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Style;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Style;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/markup/Style$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/markup/Style$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Style;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Style;->fraction_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Style;->time_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Style;->money_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Style;->measure_:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Style;->telephone_:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Style;->date_:Ljava/lang/String;

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Style;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/markup/Style;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Style;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Style;->measure_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/speech/patts/markup/Style;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Style;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Style;->hasTelephone:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/speech/patts/markup/Style;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Style;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Style;->telephone_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/google/speech/patts/markup/Style;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Style;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Style;->hasDate:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/speech/patts/markup/Style;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Style;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Style;->date_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/speech/patts/markup/Style;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Style;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Style;->hasFraction:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/markup/Style;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Style;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Style;->fraction_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/markup/Style;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Style;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Style;->hasTime:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/markup/Style;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Style;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Style;->time_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/markup/Style;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Style;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Style;->hasMoney:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/markup/Style;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Style;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Style;->money_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/markup/Style;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Style;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Style;->hasMeasure:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/markup/Style;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/markup/Style;->defaultInstance:Lcom/google/speech/patts/markup/Style;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/markup/Style$Builder;
    .locals 1

    .prologue
    .line 194
    # invokes: Lcom/google/speech/patts/markup/Style$Builder;->create()Lcom/google/speech/patts/markup/Style$Builder;
    invoke-static {}, Lcom/google/speech/patts/markup/Style$Builder;->access$100()Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Style$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/markup/Style;

    .prologue
    .line 197
    invoke-static {}, Lcom/google/speech/patts/markup/Style;->newBuilder()Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/markup/Style$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style;->date_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getDefaultInstanceForType()Lcom/google/speech/patts/markup/Style;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/markup/Style;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/markup/Style;->defaultInstance:Lcom/google/speech/patts/markup/Style;

    return-object v0
.end method

.method public getFraction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style;->fraction_:Ljava/lang/String;

    return-object v0
.end method

.method public getMeasure()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style;->measure_:Ljava/lang/String;

    return-object v0
.end method

.method public getMoney()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style;->money_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 95
    iget v0, p0, Lcom/google/speech/patts/markup/Style;->memoizedSerializedSize:I

    .line 96
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 124
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 98
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 99
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->hasFraction()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 100
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getFraction()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 103
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->hasTime()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 104
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getTime()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 107
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->hasMoney()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 108
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getMoney()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 111
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->hasMeasure()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 112
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getMeasure()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 115
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->hasTelephone()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 116
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getTelephone()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 119
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->hasDate()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 120
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 123
    :cond_6
    iput v0, p0, Lcom/google/speech/patts/markup/Style;->memoizedSerializedSize:I

    move v1, v0

    .line 124
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getTelephone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style;->telephone_:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style;->time_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDate()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Style;->hasDate:Z

    return v0
.end method

.method public hasFraction()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Style;->hasFraction:Z

    return v0
.end method

.method public hasMeasure()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Style;->hasMeasure:Z

    return v0
.end method

.method public hasMoney()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Style;->hasMoney:Z

    return v0
.end method

.method public hasTelephone()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Style;->hasTelephone:Z

    return v0
.end method

.method public hasTime()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Style;->hasTime:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->toBuilder()Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/markup/Style$Builder;
    .locals 1

    .prologue
    .line 199
    invoke-static {p0}, Lcom/google/speech/patts/markup/Style;->newBuilder(Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getSerializedSize()I

    .line 73
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getFraction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 79
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->hasMoney()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getMoney()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 82
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->hasMeasure()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 83
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getMeasure()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 85
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->hasTelephone()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 86
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getTelephone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 88
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 89
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style;->getDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 91
    :cond_5
    return-void
.end method
