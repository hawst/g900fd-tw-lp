.class public Lcom/google/speech/patts/api/common/SpeechSynthesizer$NonAudiableInputException;
.super Ljava/lang/Exception;
.source "SpeechSynthesizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/api/common/SpeechSynthesizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NonAudiableInputException"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 107
    const-string v0, "Input can\'t be synthesized, output consists of only silence"

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 108
    return-void
.end method
