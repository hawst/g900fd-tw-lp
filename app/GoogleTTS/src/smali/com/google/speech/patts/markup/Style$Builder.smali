.class public final Lcom/google/speech/patts/markup/Style$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Style.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Style;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/markup/Style;",
        "Lcom/google/speech/patts/markup/Style$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/markup/Style;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/markup/Style$Builder;
    .locals 1

    .prologue
    .line 201
    invoke-static {}, Lcom/google/speech/patts/markup/Style$Builder;->create()Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/markup/Style$Builder;
    .locals 3

    .prologue
    .line 210
    new-instance v0, Lcom/google/speech/patts/markup/Style$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Style$Builder;-><init>()V

    .line 211
    .local v0, "builder":Lcom/google/speech/patts/markup/Style$Builder;
    new-instance v1, Lcom/google/speech/patts/markup/Style;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/markup/Style;-><init>(Lcom/google/speech/patts/markup/Style$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    .line 212
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style$Builder;->build()Lcom/google/speech/patts/markup/Style;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/markup/Style;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    invoke-static {v0}, Lcom/google/speech/patts/markup/Style$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 243
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style$Builder;->buildPartial()Lcom/google/speech/patts/markup/Style;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/markup/Style;
    .locals 3

    .prologue
    .line 256
    iget-object v1, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    if-nez v1, :cond_0

    .line 257
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    .line 261
    .local v0, "returnMe":Lcom/google/speech/patts/markup/Style;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    .line 262
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style$Builder;->clone()Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style$Builder;->clone()Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/markup/Style$Builder;
    .locals 2

    .prologue
    .line 229
    invoke-static {}, Lcom/google/speech/patts/markup/Style$Builder;->create()Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/markup/Style$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Style$Builder;->clone()Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Style;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 201
    check-cast p1, Lcom/google/speech/patts/markup/Style;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/markup/Style$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Style$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/speech/patts/markup/Style;

    .prologue
    .line 266
    invoke-static {}, Lcom/google/speech/patts/markup/Style;->getDefaultInstance()Lcom/google/speech/patts/markup/Style;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 285
    :cond_0
    :goto_0
    return-object p0

    .line 267
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Style;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 268
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Style;->getFraction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Style$Builder;->setFraction(Ljava/lang/String;)Lcom/google/speech/patts/markup/Style$Builder;

    .line 270
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Style;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 271
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Style;->getTime()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Style$Builder;->setTime(Ljava/lang/String;)Lcom/google/speech/patts/markup/Style$Builder;

    .line 273
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Style;->hasMoney()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 274
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Style;->getMoney()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Style$Builder;->setMoney(Ljava/lang/String;)Lcom/google/speech/patts/markup/Style$Builder;

    .line 276
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Style;->hasMeasure()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 277
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Style;->getMeasure()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Style$Builder;->setMeasure(Ljava/lang/String;)Lcom/google/speech/patts/markup/Style$Builder;

    .line 279
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Style;->hasTelephone()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 280
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Style;->getTelephone()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Style$Builder;->setTelephone(Ljava/lang/String;)Lcom/google/speech/patts/markup/Style$Builder;

    .line 282
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Style;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Style;->getDate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Style$Builder;->setDate(Ljava/lang/String;)Lcom/google/speech/patts/markup/Style$Builder;

    goto :goto_0
.end method

.method public setDate(Ljava/lang/String;)Lcom/google/speech/patts/markup/Style$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 445
    if-nez p1, :cond_0

    .line 446
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Style;->hasDate:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Style;->access$1302(Lcom/google/speech/patts/markup/Style;Z)Z

    .line 449
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    # setter for: Lcom/google/speech/patts/markup/Style;->date_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Style;->access$1402(Lcom/google/speech/patts/markup/Style;Ljava/lang/String;)Ljava/lang/String;

    .line 450
    return-object p0
.end method

.method public setFraction(Ljava/lang/String;)Lcom/google/speech/patts/markup/Style$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 340
    if-nez p1, :cond_0

    .line 341
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Style;->hasFraction:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Style;->access$302(Lcom/google/speech/patts/markup/Style;Z)Z

    .line 344
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    # setter for: Lcom/google/speech/patts/markup/Style;->fraction_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Style;->access$402(Lcom/google/speech/patts/markup/Style;Ljava/lang/String;)Ljava/lang/String;

    .line 345
    return-object p0
.end method

.method public setMeasure(Ljava/lang/String;)Lcom/google/speech/patts/markup/Style$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 403
    if-nez p1, :cond_0

    .line 404
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Style;->hasMeasure:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Style;->access$902(Lcom/google/speech/patts/markup/Style;Z)Z

    .line 407
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    # setter for: Lcom/google/speech/patts/markup/Style;->measure_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Style;->access$1002(Lcom/google/speech/patts/markup/Style;Ljava/lang/String;)Ljava/lang/String;

    .line 408
    return-object p0
.end method

.method public setMoney(Ljava/lang/String;)Lcom/google/speech/patts/markup/Style$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 382
    if-nez p1, :cond_0

    .line 383
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Style;->hasMoney:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Style;->access$702(Lcom/google/speech/patts/markup/Style;Z)Z

    .line 386
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    # setter for: Lcom/google/speech/patts/markup/Style;->money_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Style;->access$802(Lcom/google/speech/patts/markup/Style;Ljava/lang/String;)Ljava/lang/String;

    .line 387
    return-object p0
.end method

.method public setTelephone(Ljava/lang/String;)Lcom/google/speech/patts/markup/Style$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 424
    if-nez p1, :cond_0

    .line 425
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Style;->hasTelephone:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Style;->access$1102(Lcom/google/speech/patts/markup/Style;Z)Z

    .line 428
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    # setter for: Lcom/google/speech/patts/markup/Style;->telephone_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Style;->access$1202(Lcom/google/speech/patts/markup/Style;Ljava/lang/String;)Ljava/lang/String;

    .line 429
    return-object p0
.end method

.method public setTime(Ljava/lang/String;)Lcom/google/speech/patts/markup/Style$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 361
    if-nez p1, :cond_0

    .line 362
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Style;->hasTime:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Style;->access$502(Lcom/google/speech/patts/markup/Style;Z)Z

    .line 365
    iget-object v0, p0, Lcom/google/speech/patts/markup/Style$Builder;->result:Lcom/google/speech/patts/markup/Style;

    # setter for: Lcom/google/speech/patts/markup/Style;->time_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Style;->access$602(Lcom/google/speech/patts/markup/Style;Ljava/lang/String;)Ljava/lang/String;

    .line 366
    return-object p0
.end method
