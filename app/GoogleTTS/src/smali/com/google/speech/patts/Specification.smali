.class public final Lcom/google/speech/patts/Specification;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
.source "Specification.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/Specification$1;,
        Lcom/google/speech/patts/Specification$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage",
        "<",
        "Lcom/google/speech/patts/Specification;",
        ">;"
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/Specification;


# instance fields
.field private bestUnit_:I

.field private durationsMemoizedSerializedSize:I

.field private durations_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private end_:F

.field private f_:Lspeech/patts/FeatureData;

.field private filenameId_:I

.field private filename_:Ljava/lang/String;

.field private firstDaughter_:I

.field private hasBestUnit:Z

.field private hasEnd:Z

.field private hasF:Z

.field private hasFilename:Z

.field private hasFilenameId:Z

.field private hasFirstDaughter:Z

.field private hasLastDaughter:Z

.field private hasMid:Z

.field private hasModelName:Z

.field private hasName:Z

.field private hasNumPmToEnd:Z

.field private hasNumPmToMid:Z

.field private hasParent:Z

.field private hasPatts1Diagnostics:Z

.field private hasPreviousUnit:Z

.field private hasRunningTotal:Z

.field private hasStart:Z

.field private hasStartPm:Z

.field private hasSupraSegmentalModelName:Z

.field private hasTargetCost:Z

.field private lastDaughter_:I

.field private memoizedSerializedSize:I

.field private mid_:F

.field private modelName_:Ljava/lang/String;

.field private name_:Ljava/lang/String;

.field private numPmToEnd_:I

.field private numPmToMid_:I

.field private parent_:I

.field private patts1Diagnostics_:Ljava/lang/String;

.field private previousUnit_:I

.field private runningTotal_:F

.field private startPm_:I

.field private start_:F

.field private supraSegmentalModelName_:Ljava/lang/String;

.field private targetCost_:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1125
    new-instance v0, Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/Specification;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/Specification;->defaultInstance:Lcom/google/speech/patts/Specification;

    .line 1126
    invoke-static {}, Lcom/google/speech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 1127
    sget-object v0, Lcom/google/speech/patts/Specification;->defaultInstance:Lcom/google/speech/patts/Specification;

    invoke-direct {v0}, Lcom/google/speech/patts/Specification;->initFields()V

    .line 1128
    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 26
    iput v1, p0, Lcom/google/speech/patts/Specification;->parent_:I

    .line 33
    iput v1, p0, Lcom/google/speech/patts/Specification;->firstDaughter_:I

    .line 40
    iput v1, p0, Lcom/google/speech/patts/Specification;->lastDaughter_:I

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->name_:Ljava/lang/String;

    .line 61
    iput v2, p0, Lcom/google/speech/patts/Specification;->start_:F

    .line 68
    iput v2, p0, Lcom/google/speech/patts/Specification;->mid_:F

    .line 75
    iput v2, p0, Lcom/google/speech/patts/Specification;->end_:F

    .line 82
    iput v1, p0, Lcom/google/speech/patts/Specification;->startPm_:I

    .line 89
    iput v1, p0, Lcom/google/speech/patts/Specification;->numPmToMid_:I

    .line 96
    iput v1, p0, Lcom/google/speech/patts/Specification;->numPmToEnd_:I

    .line 103
    iput v1, p0, Lcom/google/speech/patts/Specification;->bestUnit_:I

    .line 110
    iput v1, p0, Lcom/google/speech/patts/Specification;->filenameId_:I

    .line 117
    iput v2, p0, Lcom/google/speech/patts/Specification;->targetCost_:F

    .line 124
    iput v2, p0, Lcom/google/speech/patts/Specification;->runningTotal_:F

    .line 131
    iput v1, p0, Lcom/google/speech/patts/Specification;->previousUnit_:I

    .line 138
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->filename_:Ljava/lang/String;

    .line 144
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;

    .line 153
    iput v3, p0, Lcom/google/speech/patts/Specification;->durationsMemoizedSerializedSize:I

    .line 158
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->modelName_:Ljava/lang/String;

    .line 165
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->supraSegmentalModelName_:Ljava/lang/String;

    .line 172
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->patts1Diagnostics_:Ljava/lang/String;

    .line 259
    iput v3, p0, Lcom/google/speech/patts/Specification;->memoizedSerializedSize:I

    .line 10
    invoke-direct {p0}, Lcom/google/speech/patts/Specification;->initFields()V

    .line 11
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/Specification$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/Specification$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/Specification;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 4
    .param p1, "noInit"    # Z

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 12
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    .line 26
    iput v1, p0, Lcom/google/speech/patts/Specification;->parent_:I

    .line 33
    iput v1, p0, Lcom/google/speech/patts/Specification;->firstDaughter_:I

    .line 40
    iput v1, p0, Lcom/google/speech/patts/Specification;->lastDaughter_:I

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->name_:Ljava/lang/String;

    .line 61
    iput v2, p0, Lcom/google/speech/patts/Specification;->start_:F

    .line 68
    iput v2, p0, Lcom/google/speech/patts/Specification;->mid_:F

    .line 75
    iput v2, p0, Lcom/google/speech/patts/Specification;->end_:F

    .line 82
    iput v1, p0, Lcom/google/speech/patts/Specification;->startPm_:I

    .line 89
    iput v1, p0, Lcom/google/speech/patts/Specification;->numPmToMid_:I

    .line 96
    iput v1, p0, Lcom/google/speech/patts/Specification;->numPmToEnd_:I

    .line 103
    iput v1, p0, Lcom/google/speech/patts/Specification;->bestUnit_:I

    .line 110
    iput v1, p0, Lcom/google/speech/patts/Specification;->filenameId_:I

    .line 117
    iput v2, p0, Lcom/google/speech/patts/Specification;->targetCost_:F

    .line 124
    iput v2, p0, Lcom/google/speech/patts/Specification;->runningTotal_:F

    .line 131
    iput v1, p0, Lcom/google/speech/patts/Specification;->previousUnit_:I

    .line 138
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->filename_:Ljava/lang/String;

    .line 144
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;

    .line 153
    iput v3, p0, Lcom/google/speech/patts/Specification;->durationsMemoizedSerializedSize:I

    .line 158
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->modelName_:Ljava/lang/String;

    .line 165
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->supraSegmentalModelName_:Ljava/lang/String;

    .line 172
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->patts1Diagnostics_:Ljava/lang/String;

    .line 259
    iput v3, p0, Lcom/google/speech/patts/Specification;->memoizedSerializedSize:I

    .line 12
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasName:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/speech/patts/Specification;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Specification;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasF:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/google/speech/patts/Specification;)Lspeech/patts/FeatureData;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/Specification;->f_:Lspeech/patts/FeatureData;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/speech/patts/Specification;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Lspeech/patts/FeatureData;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Specification;->f_:Lspeech/patts/FeatureData;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasStart:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/speech/patts/Specification;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->start_:F

    return p1
.end method

.method static synthetic access$1602(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasMid:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/speech/patts/Specification;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->mid_:F

    return p1
.end method

.method static synthetic access$1802(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasEnd:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/google/speech/patts/Specification;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->end_:F

    return p1
.end method

.method static synthetic access$2002(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasStartPm:Z

    return p1
.end method

.method static synthetic access$2102(Lcom/google/speech/patts/Specification;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->startPm_:I

    return p1
.end method

.method static synthetic access$2202(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasNumPmToMid:Z

    return p1
.end method

.method static synthetic access$2302(Lcom/google/speech/patts/Specification;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->numPmToMid_:I

    return p1
.end method

.method static synthetic access$2402(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasNumPmToEnd:Z

    return p1
.end method

.method static synthetic access$2502(Lcom/google/speech/patts/Specification;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->numPmToEnd_:I

    return p1
.end method

.method static synthetic access$2602(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasBestUnit:Z

    return p1
.end method

.method static synthetic access$2702(Lcom/google/speech/patts/Specification;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->bestUnit_:I

    return p1
.end method

.method static synthetic access$2802(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasFilenameId:Z

    return p1
.end method

.method static synthetic access$2902(Lcom/google/speech/patts/Specification;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->filenameId_:I

    return p1
.end method

.method static synthetic access$300(Lcom/google/speech/patts/Specification;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasTargetCost:Z

    return p1
.end method

.method static synthetic access$302(Lcom/google/speech/patts/Specification;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3102(Lcom/google/speech/patts/Specification;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->targetCost_:F

    return p1
.end method

.method static synthetic access$3202(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasRunningTotal:Z

    return p1
.end method

.method static synthetic access$3302(Lcom/google/speech/patts/Specification;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->runningTotal_:F

    return p1
.end method

.method static synthetic access$3402(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasPreviousUnit:Z

    return p1
.end method

.method static synthetic access$3502(Lcom/google/speech/patts/Specification;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->previousUnit_:I

    return p1
.end method

.method static synthetic access$3602(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasFilename:Z

    return p1
.end method

.method static synthetic access$3702(Lcom/google/speech/patts/Specification;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Specification;->filename_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3802(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasModelName:Z

    return p1
.end method

.method static synthetic access$3902(Lcom/google/speech/patts/Specification;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Specification;->modelName_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4002(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasSupraSegmentalModelName:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasParent:Z

    return p1
.end method

.method static synthetic access$4102(Lcom/google/speech/patts/Specification;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Specification;->supraSegmentalModelName_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4202(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasPatts1Diagnostics:Z

    return p1
.end method

.method static synthetic access$4302(Lcom/google/speech/patts/Specification;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/Specification;->patts1Diagnostics_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/Specification;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->parent_:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasFirstDaughter:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/Specification;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->firstDaughter_:I

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/Specification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/Specification;->hasLastDaughter:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/Specification;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/Specification;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/Specification;->lastDaughter_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/Specification;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/speech/patts/Specification;->defaultInstance:Lcom/google/speech/patts/Specification;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 177
    invoke-static {}, Lspeech/patts/FeatureData;->getDefaultInstance()Lspeech/patts/FeatureData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/Specification;->f_:Lspeech/patts/FeatureData;

    .line 178
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/Specification$Builder;
    .locals 1

    .prologue
    .line 428
    # invokes: Lcom/google/speech/patts/Specification$Builder;->create()Lcom/google/speech/patts/Specification$Builder;
    invoke-static {}, Lcom/google/speech/patts/Specification$Builder;->access$100()Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/Specification;)Lcom/google/speech/patts/Specification$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/Specification;

    .prologue
    .line 431
    invoke-static {}, Lcom/google/speech/patts/Specification;->newBuilder()Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/Specification$Builder;->mergeFrom(Lcom/google/speech/patts/Specification;)Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBestUnit()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/google/speech/patts/Specification;->bestUnit_:I

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getDefaultInstanceForType()Lcom/google/speech/patts/Specification;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/Specification;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/speech/patts/Specification;->defaultInstance:Lcom/google/speech/patts/Specification;

    return-object v0
.end method

.method public getDurationsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;

    return-object v0
.end method

.method public getEnd()F
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/google/speech/patts/Specification;->end_:F

    return v0
.end method

.method public getF()Lspeech/patts/FeatureData;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/speech/patts/Specification;->f_:Lspeech/patts/FeatureData;

    return-object v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/speech/patts/Specification;->filename_:Ljava/lang/String;

    return-object v0
.end method

.method public getFilenameId()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/google/speech/patts/Specification;->filenameId_:I

    return v0
.end method

.method public getFirstDaughter()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/google/speech/patts/Specification;->firstDaughter_:I

    return v0
.end method

.method public getLastDaughter()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/speech/patts/Specification;->lastDaughter_:I

    return v0
.end method

.method public getMid()F
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/google/speech/patts/Specification;->mid_:F

    return v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/speech/patts/Specification;->modelName_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/speech/patts/Specification;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getNumPmToEnd()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/google/speech/patts/Specification;->numPmToEnd_:I

    return v0
.end method

.method public getNumPmToMid()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/google/speech/patts/Specification;->numPmToMid_:I

    return v0
.end method

.method public getParent()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/speech/patts/Specification;->parent_:I

    return v0
.end method

.method public getPatts1Diagnostics()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/speech/patts/Specification;->patts1Diagnostics_:Ljava/lang/String;

    return-object v0
.end method

.method public getPreviousUnit()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/google/speech/patts/Specification;->previousUnit_:I

    return v0
.end method

.method public getRunningTotal()F
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/google/speech/patts/Specification;->runningTotal_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 261
    iget v1, p0, Lcom/google/speech/patts/Specification;->memoizedSerializedSize:I

    .line 262
    .local v1, "size":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 358
    .end local v1    # "size":I
    .local v2, "size":I
    :goto_0
    return v2

    .line 264
    .end local v2    # "size":I
    .restart local v1    # "size":I
    :cond_0
    const/4 v1, 0x0

    .line 265
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasParent()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 266
    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getParent()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 269
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasFirstDaughter()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 270
    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getFirstDaughter()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 273
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasLastDaughter()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 274
    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getLastDaughter()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 277
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasF()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 278
    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getF()Lspeech/patts/FeatureData;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 281
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasName()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 282
    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 285
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasStart()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 286
    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getStart()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v1, v3

    .line 289
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasMid()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 290
    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getMid()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v1, v3

    .line 293
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasEnd()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 294
    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getEnd()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v1, v3

    .line 297
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasStartPm()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 298
    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getStartPm()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 301
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasNumPmToMid()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 302
    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getNumPmToMid()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 305
    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasNumPmToEnd()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 306
    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getNumPmToEnd()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 309
    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasBestUnit()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 310
    const/16 v3, 0x14

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getBestUnit()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 313
    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasFilenameId()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 314
    const/16 v3, 0x15

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getFilenameId()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 317
    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasFilename()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 318
    const/16 v3, 0x16

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 321
    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasTargetCost()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 322
    const/16 v3, 0x17

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getTargetCost()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v1, v3

    .line 325
    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasRunningTotal()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 326
    const/16 v3, 0x18

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getRunningTotal()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v1, v3

    .line 329
    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasPreviousUnit()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 330
    const/16 v3, 0x1d

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getPreviousUnit()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 334
    :cond_11
    const/4 v0, 0x0

    .line 335
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getDurationsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 336
    add-int/2addr v1, v0

    .line 337
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getDurationsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_12

    .line 338
    add-int/lit8 v1, v1, 0x2

    .line 339
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 342
    :cond_12
    iput v0, p0, Lcom/google/speech/patts/Specification;->durationsMemoizedSerializedSize:I

    .line 344
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasModelName()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 345
    const/16 v3, 0x1f

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getModelName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 348
    :cond_13
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasSupraSegmentalModelName()Z

    move-result v3

    if-eqz v3, :cond_14

    .line 349
    const/16 v3, 0x20

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getSupraSegmentalModelName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 352
    :cond_14
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasPatts1Diagnostics()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 353
    const/16 v3, 0x21

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getPatts1Diagnostics()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 356
    :cond_15
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->extensionsSerializedSize()I

    move-result v3

    add-int/2addr v1, v3

    .line 357
    iput v1, p0, Lcom/google/speech/patts/Specification;->memoizedSerializedSize:I

    move v2, v1

    .line 358
    .end local v1    # "size":I
    .restart local v2    # "size":I
    goto/16 :goto_0
.end method

.method public getStart()F
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/google/speech/patts/Specification;->start_:F

    return v0
.end method

.method public getStartPm()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/google/speech/patts/Specification;->startPm_:I

    return v0
.end method

.method public getSupraSegmentalModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/speech/patts/Specification;->supraSegmentalModelName_:Ljava/lang/String;

    return-object v0
.end method

.method public getTargetCost()F
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/google/speech/patts/Specification;->targetCost_:F

    return v0
.end method

.method public hasBestUnit()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasBestUnit:Z

    return v0
.end method

.method public hasEnd()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasEnd:Z

    return v0
.end method

.method public hasF()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasF:Z

    return v0
.end method

.method public hasFilename()Z
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasFilename:Z

    return v0
.end method

.method public hasFilenameId()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasFilenameId:Z

    return v0
.end method

.method public hasFirstDaughter()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasFirstDaughter:Z

    return v0
.end method

.method public hasLastDaughter()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasLastDaughter:Z

    return v0
.end method

.method public hasMid()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasMid:Z

    return v0
.end method

.method public hasModelName()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasModelName:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasName:Z

    return v0
.end method

.method public hasNumPmToEnd()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasNumPmToEnd:Z

    return v0
.end method

.method public hasNumPmToMid()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasNumPmToMid:Z

    return v0
.end method

.method public hasParent()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasParent:Z

    return v0
.end method

.method public hasPatts1Diagnostics()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasPatts1Diagnostics:Z

    return v0
.end method

.method public hasPreviousUnit()Z
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasPreviousUnit:Z

    return v0
.end method

.method public hasRunningTotal()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasRunningTotal:Z

    return v0
.end method

.method public hasStart()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasStart:Z

    return v0
.end method

.method public hasStartPm()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasStartPm:Z

    return v0
.end method

.method public hasSupraSegmentalModelName()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasSupraSegmentalModelName:Z

    return v0
.end method

.method public hasTargetCost()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/google/speech/patts/Specification;->hasTargetCost:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->extensionsAreInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 181
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->toBuilder()Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/Specification$Builder;
    .locals 1

    .prologue
    .line 433
    invoke-static {p0}, Lcom/google/speech/patts/Specification;->newBuilder(Lcom/google/speech/patts/Specification;)Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getSerializedSize()I

    .line 188
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->newExtensionWriter()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;

    move-result-object v1

    .line 189
    .local v1, "extensionWriter":Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasParent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 190
    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getParent()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 192
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasFirstDaughter()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 193
    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getFirstDaughter()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 195
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasLastDaughter()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 196
    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getLastDaughter()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 198
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasF()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 199
    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getF()Lspeech/patts/FeatureData;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 201
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasName()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 202
    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 204
    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasStart()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 205
    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getStart()F

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 207
    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasMid()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 208
    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getMid()F

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 210
    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasEnd()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 211
    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getEnd()F

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 213
    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasStartPm()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 214
    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getStartPm()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 216
    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasNumPmToMid()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 217
    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getNumPmToMid()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 219
    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasNumPmToEnd()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 220
    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getNumPmToEnd()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 222
    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasBestUnit()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 223
    const/16 v3, 0x14

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getBestUnit()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 225
    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasFilenameId()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 226
    const/16 v3, 0x15

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getFilenameId()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 228
    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasFilename()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 229
    const/16 v3, 0x16

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 231
    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasTargetCost()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 232
    const/16 v3, 0x17

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getTargetCost()F

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 234
    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasRunningTotal()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 235
    const/16 v3, 0x18

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getRunningTotal()F

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 237
    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasPreviousUnit()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 238
    const/16 v3, 0x1d

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getPreviousUnit()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 240
    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getDurationsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_11

    .line 241
    const/16 v3, 0xf2

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 242
    iget v3, p0, Lcom/google/speech/patts/Specification;->durationsMemoizedSerializedSize:I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 244
    :cond_11
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getDurationsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 245
    .local v0, "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_0

    .line 247
    .end local v0    # "element":F
    :cond_12
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasModelName()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 248
    const/16 v3, 0x1f

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getModelName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 250
    :cond_13
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasSupraSegmentalModelName()Z

    move-result v3

    if-eqz v3, :cond_14

    .line 251
    const/16 v3, 0x20

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getSupraSegmentalModelName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 253
    :cond_14
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->hasPatts1Diagnostics()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 254
    const/16 v3, 0x21

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification;->getPatts1Diagnostics()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 256
    :cond_15
    const/16 v3, 0x7d1

    invoke-virtual {v1, v3, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;->writeUntil(ILcom/google/protobuf/CodedOutputStream;)V

    .line 257
    return-void
.end method
