.class public final Lcom/google/speech/patts/markup/Money;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Money.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/markup/Money$1;,
        Lcom/google/speech/patts/markup/Money$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/markup/Money;


# instance fields
.field private currency_:Ljava/lang/String;

.field private hasCurrency:Z

.field private hasMajor:Z

.field private hasMinor:Z

.field private hasStyle:Z

.field private major_:J

.field private memoizedSerializedSize:I

.field private minor_:Lcom/google/speech/patts/markup/LeadingZeroInt;

.field private style_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 400
    new-instance v0, Lcom/google/speech/patts/markup/Money;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/markup/Money;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/markup/Money;->defaultInstance:Lcom/google/speech/patts/markup/Money;

    .line 401
    invoke-static {}, Lcom/google/speech/patts/markup/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 402
    sget-object v0, Lcom/google/speech/patts/markup/Money;->defaultInstance:Lcom/google/speech/patts/markup/Money;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Money;->initFields()V

    .line 403
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/patts/markup/Money;->major_:J

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Money;->currency_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Money;->style_:Ljava/lang/String;

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Money;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Money;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/markup/Money$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/markup/Money$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Money;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/patts/markup/Money;->major_:J

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Money;->currency_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/patts/markup/Money;->style_:Ljava/lang/String;

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Money;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lcom/google/speech/patts/markup/Money;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Money;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Money;->style_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/speech/patts/markup/Money;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Money;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Money;->hasMajor:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/markup/Money;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Money;
    .param p1, "x1"    # J

    .prologue
    .line 5
    iput-wide p1, p0, Lcom/google/speech/patts/markup/Money;->major_:J

    return-wide p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/markup/Money;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Money;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Money;->hasMinor:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/LeadingZeroInt;
    .locals 1
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Money;

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money;->minor_:Lcom/google/speech/patts/markup/LeadingZeroInt;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/speech/patts/markup/Money;Lcom/google/speech/patts/markup/LeadingZeroInt;)Lcom/google/speech/patts/markup/LeadingZeroInt;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Money;
    .param p1, "x1"    # Lcom/google/speech/patts/markup/LeadingZeroInt;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Money;->minor_:Lcom/google/speech/patts/markup/LeadingZeroInt;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/speech/patts/markup/Money;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Money;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Money;->hasCurrency:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/speech/patts/markup/Money;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Money;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/speech/patts/markup/Money;->currency_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/speech/patts/markup/Money;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Money;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Money;->hasStyle:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/markup/Money;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/markup/Money;->defaultInstance:Lcom/google/speech/patts/markup/Money;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lcom/google/speech/patts/markup/LeadingZeroInt;->getDefaultInstance()Lcom/google/speech/patts/markup/LeadingZeroInt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/patts/markup/Money;->minor_:Lcom/google/speech/patts/markup/LeadingZeroInt;

    .line 52
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/markup/Money$Builder;
    .locals 1

    .prologue
    .line 170
    # invokes: Lcom/google/speech/patts/markup/Money$Builder;->create()Lcom/google/speech/patts/markup/Money$Builder;
    invoke-static {}, Lcom/google/speech/patts/markup/Money$Builder;->access$100()Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Money$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/markup/Money;

    .prologue
    .line 173
    invoke-static {}, Lcom/google/speech/patts/markup/Money;->newBuilder()Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/markup/Money$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCurrency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money;->currency_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->getDefaultInstanceForType()Lcom/google/speech/patts/markup/Money;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/markup/Money;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/markup/Money;->defaultInstance:Lcom/google/speech/patts/markup/Money;

    return-object v0
.end method

.method public getMajor()J
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, Lcom/google/speech/patts/markup/Money;->major_:J

    return-wide v0
.end method

.method public getMinor()Lcom/google/speech/patts/markup/LeadingZeroInt;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money;->minor_:Lcom/google/speech/patts/markup/LeadingZeroInt;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 79
    iget v0, p0, Lcom/google/speech/patts/markup/Money;->memoizedSerializedSize:I

    .line 80
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 100
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 82
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 83
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->hasMajor()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 84
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->getMajor()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 87
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->hasMinor()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 88
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->getMinor()Lcom/google/speech/patts/markup/LeadingZeroInt;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 91
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->hasCurrency()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 92
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->getCurrency()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 95
    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 96
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->getStyle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 99
    :cond_4
    iput v0, p0, Lcom/google/speech/patts/markup/Money;->memoizedSerializedSize:I

    move v1, v0

    .line 100
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/speech/patts/markup/Money;->style_:Ljava/lang/String;

    return-object v0
.end method

.method public hasCurrency()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Money;->hasCurrency:Z

    return v0
.end method

.method public hasMajor()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Money;->hasMajor:Z

    return v0
.end method

.method public hasMinor()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Money;->hasMinor:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Money;->hasStyle:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->hasMinor()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->getMinor()Lcom/google/speech/patts/markup/LeadingZeroInt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/LeadingZeroInt;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->toBuilder()Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/markup/Money$Builder;
    .locals 1

    .prologue
    .line 175
    invoke-static {p0}, Lcom/google/speech/patts/markup/Money;->newBuilder(Lcom/google/speech/patts/markup/Money;)Lcom/google/speech/patts/markup/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->getSerializedSize()I

    .line 63
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->hasMajor()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->getMajor()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 66
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->hasMinor()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->getMinor()Lcom/google/speech/patts/markup/LeadingZeroInt;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 69
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->hasCurrency()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 70
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->getCurrency()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 72
    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 73
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Money;->getStyle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 75
    :cond_3
    return-void
.end method
