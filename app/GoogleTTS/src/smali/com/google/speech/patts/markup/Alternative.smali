.class public final Lcom/google/speech/patts/markup/Alternative;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Alternative.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/speech/patts/markup/Alternative$1;,
        Lcom/google/speech/patts/markup/Alternative$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/speech/patts/markup/Alternative;


# instance fields
.field private dsp_:Z

.field private hasDsp:Z

.field private hasKey:Z

.field private key_:I

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 289
    new-instance v0, Lcom/google/speech/patts/markup/Alternative;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/speech/patts/markup/Alternative;-><init>(Z)V

    sput-object v0, Lcom/google/speech/patts/markup/Alternative;->defaultInstance:Lcom/google/speech/patts/markup/Alternative;

    .line 290
    invoke-static {}, Lcom/google/speech/patts/markup/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 291
    sget-object v0, Lcom/google/speech/patts/markup/Alternative;->defaultInstance:Lcom/google/speech/patts/markup/Alternative;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Alternative;->initFields()V

    .line 292
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/patts/markup/Alternative;->key_:I

    .line 32
    iput-boolean v0, p0, Lcom/google/speech/patts/markup/Alternative;->dsp_:Z

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Alternative;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Alternative;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/speech/patts/markup/Alternative$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/speech/patts/markup/Alternative$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/speech/patts/markup/Alternative;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/speech/patts/markup/Alternative;->key_:I

    .line 32
    iput-boolean v0, p0, Lcom/google/speech/patts/markup/Alternative;->dsp_:Z

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/patts/markup/Alternative;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lcom/google/speech/patts/markup/Alternative;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Alternative;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Alternative;->hasKey:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/speech/patts/markup/Alternative;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Alternative;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lcom/google/speech/patts/markup/Alternative;->key_:I

    return p1
.end method

.method static synthetic access$502(Lcom/google/speech/patts/markup/Alternative;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Alternative;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Alternative;->hasDsp:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/speech/patts/markup/Alternative;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/speech/patts/markup/Alternative;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lcom/google/speech/patts/markup/Alternative;->dsp_:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/speech/patts/markup/Alternative;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/speech/patts/markup/Alternative;->defaultInstance:Lcom/google/speech/patts/markup/Alternative;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public static newBuilder()Lcom/google/speech/patts/markup/Alternative$Builder;
    .locals 1

    .prologue
    .line 139
    # invokes: Lcom/google/speech/patts/markup/Alternative$Builder;->create()Lcom/google/speech/patts/markup/Alternative$Builder;
    invoke-static {}, Lcom/google/speech/patts/markup/Alternative$Builder;->access$100()Lcom/google/speech/patts/markup/Alternative$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/speech/patts/markup/Alternative;)Lcom/google/speech/patts/markup/Alternative$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/speech/patts/markup/Alternative;

    .prologue
    .line 142
    invoke-static {}, Lcom/google/speech/patts/markup/Alternative;->newBuilder()Lcom/google/speech/patts/markup/Alternative$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/patts/markup/Alternative$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Alternative;)Lcom/google/speech/patts/markup/Alternative$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Alternative;->getDefaultInstanceForType()Lcom/google/speech/patts/markup/Alternative;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/speech/patts/markup/Alternative;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/speech/patts/markup/Alternative;->defaultInstance:Lcom/google/speech/patts/markup/Alternative;

    return-object v0
.end method

.method public getDsp()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Alternative;->dsp_:Z

    return v0
.end method

.method public getKey()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/speech/patts/markup/Alternative;->key_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 56
    iget v0, p0, Lcom/google/speech/patts/markup/Alternative;->memoizedSerializedSize:I

    .line 57
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 69
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 59
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Alternative;->hasKey()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 61
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Alternative;->getKey()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 64
    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Alternative;->hasDsp()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 65
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Alternative;->getDsp()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 68
    :cond_2
    iput v0, p0, Lcom/google/speech/patts/markup/Alternative;->memoizedSerializedSize:I

    move v1, v0

    .line 69
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasDsp()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Alternative;->hasDsp:Z

    return v0
.end method

.method public hasKey()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Alternative;->hasKey:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/speech/patts/markup/Alternative;->hasKey:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 40
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Alternative;->toBuilder()Lcom/google/speech/patts/markup/Alternative$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/speech/patts/markup/Alternative$Builder;
    .locals 1

    .prologue
    .line 144
    invoke-static {p0}, Lcom/google/speech/patts/markup/Alternative;->newBuilder(Lcom/google/speech/patts/markup/Alternative;)Lcom/google/speech/patts/markup/Alternative$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Alternative;->getSerializedSize()I

    .line 46
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Alternative;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Alternative;->getKey()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 49
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Alternative;->hasDsp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Alternative;->getDsp()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 52
    :cond_1
    return-void
.end method
