.class public final Lcom/google/speech/patts/markup/Sentence$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Sentence.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/markup/Sentence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/speech/patts/markup/Sentence;",
        "Lcom/google/speech/patts/markup/Sentence$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/markup/Sentence;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/markup/Sentence$Builder;
    .locals 1

    .prologue
    .line 212
    invoke-static {}, Lcom/google/speech/patts/markup/Sentence$Builder;->create()Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/markup/Sentence$Builder;
    .locals 3

    .prologue
    .line 221
    new-instance v0, Lcom/google/speech/patts/markup/Sentence$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/markup/Sentence$Builder;-><init>()V

    .line 222
    .local v0, "builder":Lcom/google/speech/patts/markup/Sentence$Builder;
    new-instance v1, Lcom/google/speech/patts/markup/Sentence;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/markup/Sentence;-><init>(Lcom/google/speech/patts/markup/Sentence$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    .line 223
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence$Builder;->build()Lcom/google/speech/patts/markup/Sentence;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/markup/Sentence;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    invoke-static {v0}, Lcom/google/speech/patts/markup/Sentence$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 254
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence$Builder;->buildPartial()Lcom/google/speech/patts/markup/Sentence;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/markup/Sentence;
    .locals 3

    .prologue
    .line 267
    iget-object v1, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    if-nez v1, :cond_0

    .line 268
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 271
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Sentence;->access$300(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 272
    iget-object v1, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    iget-object v2, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/markup/Sentence;->access$300(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/markup/Sentence;->access$302(Lcom/google/speech/patts/markup/Sentence;Ljava/util/List;)Ljava/util/List;

    .line 275
    :cond_1
    iget-object v1, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Sentence;->access$400(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 276
    iget-object v1, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    iget-object v2, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/markup/Sentence;->access$400(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/markup/Sentence;->access$402(Lcom/google/speech/patts/markup/Sentence;Ljava/util/List;)Ljava/util/List;

    .line 279
    :cond_2
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    .line 280
    .local v0, "returnMe":Lcom/google/speech/patts/markup/Sentence;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    .line 281
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence$Builder;->clone()Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence$Builder;->clone()Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/markup/Sentence$Builder;
    .locals 2

    .prologue
    .line 240
    invoke-static {}, Lcom/google/speech/patts/markup/Sentence$Builder;->create()Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/markup/Sentence$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/google/speech/patts/markup/Sentence$Builder;->clone()Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Sentence;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 212
    check-cast p1, Lcom/google/speech/patts/markup/Sentence;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/markup/Sentence$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Sentence$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/markup/Sentence;

    .prologue
    .line 285
    invoke-static {}, Lcom/google/speech/patts/markup/Sentence;->getDefaultInstance()Lcom/google/speech/patts/markup/Sentence;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 307
    :cond_0
    :goto_0
    return-object p0

    .line 286
    :cond_1
    # getter for: Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/markup/Sentence;->access$300(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 287
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Sentence;->access$300(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 288
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Sentence;->access$302(Lcom/google/speech/patts/markup/Sentence;Ljava/util/List;)Ljava/util/List;

    .line 290
    :cond_2
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Sentence;->access$300(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/markup/Sentence;->say_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/markup/Sentence;->access$300(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 292
    :cond_3
    # getter for: Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/markup/Sentence;->access$400(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 293
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Sentence;->access$400(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 294
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Sentence;->access$402(Lcom/google/speech/patts/markup/Sentence;Ljava/util/List;)Ljava/util/List;

    .line 296
    :cond_4
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Sentence;->access$400(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/markup/Sentence;->entry_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/markup/Sentence;->access$400(Lcom/google/speech/patts/markup/Sentence;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 298
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Sentence;->hasParams()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 299
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Sentence;->getParams()Lcom/google/speech/patts/ling_utt/TreeMap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Sentence$Builder;->mergeParams(Lcom/google/speech/patts/ling_utt/TreeMap;)Lcom/google/speech/patts/markup/Sentence$Builder;

    .line 301
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Sentence;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 302
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Sentence;->getStyle()Lcom/google/speech/patts/markup/Style;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Sentence$Builder;->mergeStyle(Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Sentence$Builder;

    .line 304
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Sentence;->hasTransform()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    invoke-virtual {p1}, Lcom/google/speech/patts/markup/Sentence;->getTransform()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/markup/Sentence$Builder;->mergeTransform(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/markup/Sentence$Builder;

    goto/16 :goto_0
.end method

.method public mergeParams(Lcom/google/speech/patts/ling_utt/TreeMap;)Lcom/google/speech/patts/markup/Sentence$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/ling_utt/TreeMap;

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Sentence;->hasParams()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->params_:Lcom/google/speech/patts/ling_utt/TreeMap;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Sentence;->access$600(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/ling_utt/TreeMap;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/ling_utt/TreeMap;->getDefaultInstance()Lcom/google/speech/patts/ling_utt/TreeMap;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 494
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->params_:Lcom/google/speech/patts/ling_utt/TreeMap;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Sentence;->access$600(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/ling_utt/TreeMap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/ling_utt/TreeMap;->newBuilder(Lcom/google/speech/patts/ling_utt/TreeMap;)Lcom/google/speech/patts/ling_utt/TreeMap$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/ling_utt/TreeMap$Builder;->mergeFrom(Lcom/google/speech/patts/ling_utt/TreeMap;)Lcom/google/speech/patts/ling_utt/TreeMap$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/ling_utt/TreeMap$Builder;->buildPartial()Lcom/google/speech/patts/ling_utt/TreeMap;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Sentence;->params_:Lcom/google/speech/patts/ling_utt/TreeMap;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Sentence;->access$602(Lcom/google/speech/patts/markup/Sentence;Lcom/google/speech/patts/ling_utt/TreeMap;)Lcom/google/speech/patts/ling_utt/TreeMap;

    .line 499
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Sentence;->hasParams:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Sentence;->access$502(Lcom/google/speech/patts/markup/Sentence;Z)Z

    .line 500
    return-object p0

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # setter for: Lcom/google/speech/patts/markup/Sentence;->params_:Lcom/google/speech/patts/ling_utt/TreeMap;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Sentence;->access$602(Lcom/google/speech/patts/markup/Sentence;Lcom/google/speech/patts/ling_utt/TreeMap;)Lcom/google/speech/patts/ling_utt/TreeMap;

    goto :goto_0
.end method

.method public mergeStyle(Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Sentence$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/markup/Style;

    .prologue
    .line 529
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Sentence;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->style_:Lcom/google/speech/patts/markup/Style;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Sentence;->access$800(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Style;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/markup/Style;->getDefaultInstance()Lcom/google/speech/patts/markup/Style;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 531
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->style_:Lcom/google/speech/patts/markup/Style;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Sentence;->access$800(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/markup/Style;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/markup/Style;->newBuilder(Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/markup/Style$Builder;->mergeFrom(Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Style$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/markup/Style$Builder;->buildPartial()Lcom/google/speech/patts/markup/Style;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Sentence;->style_:Lcom/google/speech/patts/markup/Style;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Sentence;->access$802(Lcom/google/speech/patts/markup/Sentence;Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Style;

    .line 536
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Sentence;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Sentence;->access$702(Lcom/google/speech/patts/markup/Sentence;Z)Z

    .line 537
    return-object p0

    .line 534
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # setter for: Lcom/google/speech/patts/markup/Sentence;->style_:Lcom/google/speech/patts/markup/Style;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Sentence;->access$802(Lcom/google/speech/patts/markup/Sentence;Lcom/google/speech/patts/markup/Style;)Lcom/google/speech/patts/markup/Style;

    goto :goto_0
.end method

.method public mergeTransform(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/markup/Sentence$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .prologue
    .line 566
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    invoke-virtual {v0}, Lcom/google/speech/patts/markup/Sentence;->hasTransform()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->transform_:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    invoke-static {v0}, Lcom/google/speech/patts/markup/Sentence;->access$1000(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->getDefaultInstance()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 568
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    iget-object v1, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # getter for: Lcom/google/speech/patts/markup/Sentence;->transform_:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    invoke-static {v1}, Lcom/google/speech/patts/markup/Sentence;->access$1000(Lcom/google/speech/patts/markup/Sentence;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform;->newBuilder(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->mergeFrom(Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/patts/hmm/SpeechMorphingTransform$Builder;->buildPartial()Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/markup/Sentence;->transform_:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Sentence;->access$1002(Lcom/google/speech/patts/markup/Sentence;Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    .line 573
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/markup/Sentence;->hasTransform:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/markup/Sentence;->access$902(Lcom/google/speech/patts/markup/Sentence;Z)Z

    .line 574
    return-object p0

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/markup/Sentence$Builder;->result:Lcom/google/speech/patts/markup/Sentence;

    # setter for: Lcom/google/speech/patts/markup/Sentence;->transform_:Lcom/google/speech/patts/hmm/SpeechMorphingTransform;
    invoke-static {v0, p1}, Lcom/google/speech/patts/markup/Sentence;->access$1002(Lcom/google/speech/patts/markup/Sentence;Lcom/google/speech/patts/hmm/SpeechMorphingTransform;)Lcom/google/speech/patts/hmm/SpeechMorphingTransform;

    goto :goto_0
.end method
