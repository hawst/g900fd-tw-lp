.class public final Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;
.super Ljava/lang/Object;
.source "BufferedSynthesisHandle.java"


# instance fields
.field private id:J

.field private outputHandle:J

.field private stateHandle:J

.field private status:Lcom/google/speech/patts/engine/api/SynthesisStatus;

.field private synthesizerHandle:J


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->initialize(J)V

    .line 150
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "synthesizerHandle"    # J

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-virtual {p0, p1, p2}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->initialize(J)V

    .line 46
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 52
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->outputHandle:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 53
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->synthesizerHandle:J

    iget-wide v2, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->outputHandle:J

    invoke-static {v0, v1, v2, v3}, Lcom/google/speech/patts/engine/api/CommonApi;->deleteSynthesisOutput(JJ)V

    .line 54
    iput-wide v4, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->outputHandle:J

    .line 56
    :cond_0
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->stateHandle:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 57
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->stateHandle:J

    invoke-static {v0, v1}, Lcom/google/speech/patts/engine/api/CommonApi;->deleteSynthesisState(J)V

    .line 58
    iput-wide v4, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->stateHandle:J

    .line 60
    :cond_1
    iput-wide v4, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->synthesizerHandle:J

    .line 61
    return-void
.end method

.method public getOutputHandle()J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->outputHandle:J

    return-wide v0
.end method

.method public getStateHandle()J
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->stateHandle:J

    return-wide v0
.end method

.method protected initialize(J)V
    .locals 5
    .param p1, "synthHandle"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 158
    iput-wide p1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->synthesizerHandle:J

    .line 159
    iput-wide v2, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->outputHandle:J

    .line 160
    iput-wide v2, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->stateHandle:J

    .line 161
    sget-object v0, Lcom/google/speech/patts/engine/api/SynthesisStatus;->STATUS_INITIAL:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    iput-object v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->status:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    .line 162
    iput-wide v2, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->id:J

    .line 163
    return-void
.end method

.method public isInProgress()Z
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->status:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    sget-object v1, Lcom/google/speech/patts/engine/api/SynthesisStatus;->STATUS_IN_PROGRESS:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setId(J)V
    .locals 1
    .param p1, "newId"    # J

    .prologue
    .line 132
    iput-wide p1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->id:J

    .line 133
    return-void
.end method

.method public setOutputHandle(J)V
    .locals 1
    .param p1, "handle"    # J

    .prologue
    .line 78
    iput-wide p1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->outputHandle:J

    .line 79
    return-void
.end method

.method public setStateHandle(J)V
    .locals 1
    .param p1, "handle"    # J

    .prologue
    .line 96
    iput-wide p1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->stateHandle:J

    .line 97
    return-void
.end method

.method public setStatus(Lcom/google/speech/patts/engine/api/SynthesisStatus;)V
    .locals 0
    .param p1, "newStatus"    # Lcom/google/speech/patts/engine/api/SynthesisStatus;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->status:Lcom/google/speech/patts/engine/api/SynthesisStatus;

    .line 115
    return-void
.end method
