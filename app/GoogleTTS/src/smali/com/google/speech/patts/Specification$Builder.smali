.class public final Lcom/google/speech/patts/Specification$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
.source "Specification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/patts/Specification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder",
        "<",
        "Lcom/google/speech/patts/Specification;",
        "Lcom/google/speech/patts/Specification$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/speech/patts/Specification;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 441
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/speech/patts/Specification$Builder;
    .locals 1

    .prologue
    .line 435
    invoke-static {}, Lcom/google/speech/patts/Specification$Builder;->create()Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/speech/patts/Specification$Builder;
    .locals 3

    .prologue
    .line 444
    new-instance v0, Lcom/google/speech/patts/Specification$Builder;

    invoke-direct {v0}, Lcom/google/speech/patts/Specification$Builder;-><init>()V

    .line 445
    .local v0, "builder":Lcom/google/speech/patts/Specification$Builder;
    new-instance v1, Lcom/google/speech/patts/Specification;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/speech/patts/Specification;-><init>(Lcom/google/speech/patts/Specification$1;)V

    iput-object v1, v0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    .line 446
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification$Builder;->build()Lcom/google/speech/patts/Specification;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/speech/patts/Specification;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/patts/Specification$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    invoke-static {v0}, Lcom/google/speech/patts/Specification$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 477
    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification$Builder;->buildPartial()Lcom/google/speech/patts/Specification;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/speech/patts/Specification;
    .locals 3

    .prologue
    .line 490
    iget-object v1, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    if-nez v1, :cond_0

    .line 491
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 494
    :cond_0
    iget-object v1, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # getter for: Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/speech/patts/Specification;->access$300(Lcom/google/speech/patts/Specification;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 495
    iget-object v1, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    iget-object v2, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # getter for: Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/speech/patts/Specification;->access$300(Lcom/google/speech/patts/Specification;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/speech/patts/Specification;->access$302(Lcom/google/speech/patts/Specification;Ljava/util/List;)Ljava/util/List;

    .line 498
    :cond_1
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    .line 499
    .local v0, "returnMe":Lcom/google/speech/patts/Specification;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    .line 500
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification$Builder;->clone()Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification$Builder;->clone()Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;
    .locals 1

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification$Builder;->clone()Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/speech/patts/Specification$Builder;
    .locals 2

    .prologue
    .line 463
    invoke-static {}, Lcom/google/speech/patts/Specification$Builder;->create()Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    invoke-virtual {v0, v1}, Lcom/google/speech/patts/Specification$Builder;->mergeFrom(Lcom/google/speech/patts/Specification;)Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification$Builder;->clone()Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic internalGetResult()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
    .locals 1

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/google/speech/patts/Specification$Builder;->internalGetResult()Lcom/google/speech/patts/Specification;

    move-result-object v0

    return-object v0
.end method

.method protected internalGetResult()Lcom/google/speech/patts/Specification;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    invoke-virtual {v0}, Lcom/google/speech/patts/Specification;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeF(Lspeech/patts/FeatureData;)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/FeatureData;

    .prologue
    .line 789
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    invoke-virtual {v0}, Lcom/google/speech/patts/Specification;->hasF()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # getter for: Lcom/google/speech/patts/Specification;->f_:Lspeech/patts/FeatureData;
    invoke-static {v0}, Lcom/google/speech/patts/Specification;->access$1300(Lcom/google/speech/patts/Specification;)Lspeech/patts/FeatureData;

    move-result-object v0

    invoke-static {}, Lspeech/patts/FeatureData;->getDefaultInstance()Lspeech/patts/FeatureData;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 791
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    iget-object v1, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # getter for: Lcom/google/speech/patts/Specification;->f_:Lspeech/patts/FeatureData;
    invoke-static {v1}, Lcom/google/speech/patts/Specification;->access$1300(Lcom/google/speech/patts/Specification;)Lspeech/patts/FeatureData;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/FeatureData;->newBuilder(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/FeatureData$Builder;->mergeFrom(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/FeatureData$Builder;->buildPartial()Lspeech/patts/FeatureData;

    move-result-object v1

    # setter for: Lcom/google/speech/patts/Specification;->f_:Lspeech/patts/FeatureData;
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$1302(Lcom/google/speech/patts/Specification;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;

    .line 796
    :goto_0
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasF:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$1202(Lcom/google/speech/patts/Specification;Z)Z

    .line 797
    return-object p0

    .line 794
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->f_:Lspeech/patts/FeatureData;
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$1302(Lcom/google/speech/patts/Specification;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 435
    check-cast p1, Lcom/google/speech/patts/Specification;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/Specification$Builder;->mergeFrom(Lcom/google/speech/patts/Specification;)Lcom/google/speech/patts/Specification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/speech/patts/Specification;)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/speech/patts/Specification;

    .prologue
    .line 504
    invoke-static {}, Lcom/google/speech/patts/Specification;->getDefaultInstance()Lcom/google/speech/patts/Specification;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 572
    :goto_0
    return-object p0

    .line 505
    :cond_0
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasParent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 506
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getParent()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setParent(I)Lcom/google/speech/patts/Specification$Builder;

    .line 508
    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasFirstDaughter()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 509
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getFirstDaughter()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setFirstDaughter(I)Lcom/google/speech/patts/Specification$Builder;

    .line 511
    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasLastDaughter()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 512
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getLastDaughter()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setLastDaughter(I)Lcom/google/speech/patts/Specification$Builder;

    .line 514
    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasName()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 515
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setName(Ljava/lang/String;)Lcom/google/speech/patts/Specification$Builder;

    .line 517
    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasF()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 518
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getF()Lspeech/patts/FeatureData;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->mergeF(Lspeech/patts/FeatureData;)Lcom/google/speech/patts/Specification$Builder;

    .line 520
    :cond_5
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasStart()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 521
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getStart()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setStart(F)Lcom/google/speech/patts/Specification$Builder;

    .line 523
    :cond_6
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasMid()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 524
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getMid()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setMid(F)Lcom/google/speech/patts/Specification$Builder;

    .line 526
    :cond_7
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasEnd()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 527
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getEnd()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setEnd(F)Lcom/google/speech/patts/Specification$Builder;

    .line 529
    :cond_8
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasStartPm()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 530
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getStartPm()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setStartPm(I)Lcom/google/speech/patts/Specification$Builder;

    .line 532
    :cond_9
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasNumPmToMid()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 533
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getNumPmToMid()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setNumPmToMid(I)Lcom/google/speech/patts/Specification$Builder;

    .line 535
    :cond_a
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasNumPmToEnd()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 536
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getNumPmToEnd()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setNumPmToEnd(I)Lcom/google/speech/patts/Specification$Builder;

    .line 538
    :cond_b
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasBestUnit()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 539
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getBestUnit()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setBestUnit(I)Lcom/google/speech/patts/Specification$Builder;

    .line 541
    :cond_c
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasFilenameId()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 542
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getFilenameId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setFilenameId(I)Lcom/google/speech/patts/Specification$Builder;

    .line 544
    :cond_d
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasTargetCost()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 545
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getTargetCost()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setTargetCost(F)Lcom/google/speech/patts/Specification$Builder;

    .line 547
    :cond_e
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasRunningTotal()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 548
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getRunningTotal()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setRunningTotal(F)Lcom/google/speech/patts/Specification$Builder;

    .line 550
    :cond_f
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasPreviousUnit()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 551
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getPreviousUnit()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setPreviousUnit(I)Lcom/google/speech/patts/Specification$Builder;

    .line 553
    :cond_10
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasFilename()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 554
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getFilename()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setFilename(Ljava/lang/String;)Lcom/google/speech/patts/Specification$Builder;

    .line 556
    :cond_11
    # getter for: Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/Specification;->access$300(Lcom/google/speech/patts/Specification;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_13

    .line 557
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # getter for: Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/Specification;->access$300(Lcom/google/speech/patts/Specification;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 558
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$302(Lcom/google/speech/patts/Specification;Ljava/util/List;)Ljava/util/List;

    .line 560
    :cond_12
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # getter for: Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/speech/patts/Specification;->access$300(Lcom/google/speech/patts/Specification;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/speech/patts/Specification;->durations_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/speech/patts/Specification;->access$300(Lcom/google/speech/patts/Specification;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 562
    :cond_13
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasModelName()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 563
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getModelName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setModelName(Ljava/lang/String;)Lcom/google/speech/patts/Specification$Builder;

    .line 565
    :cond_14
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasSupraSegmentalModelName()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 566
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getSupraSegmentalModelName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setSupraSegmentalModelName(Ljava/lang/String;)Lcom/google/speech/patts/Specification$Builder;

    .line 568
    :cond_15
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->hasPatts1Diagnostics()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 569
    invoke-virtual {p1}, Lcom/google/speech/patts/Specification;->getPatts1Diagnostics()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/speech/patts/Specification$Builder;->setPatts1Diagnostics(Ljava/lang/String;)Lcom/google/speech/patts/Specification$Builder;

    .line 571
    :cond_16
    invoke-virtual {p0, p1}, Lcom/google/speech/patts/Specification$Builder;->mergeExtensionFields(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;)V

    goto/16 :goto_0
.end method

.method public setBestUnit(I)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 921
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasBestUnit:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$2602(Lcom/google/speech/patts/Specification;Z)Z

    .line 922
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->bestUnit_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$2702(Lcom/google/speech/patts/Specification;I)I

    .line 923
    return-object p0
.end method

.method public setEnd(F)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 849
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasEnd:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$1802(Lcom/google/speech/patts/Specification;Z)Z

    .line 850
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->end_:F
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$1902(Lcom/google/speech/patts/Specification;F)F

    .line 851
    return-object p0
.end method

.method public setFilename(Ljava/lang/String;)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1011
    if-nez p1, :cond_0

    .line 1012
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1014
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasFilename:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$3602(Lcom/google/speech/patts/Specification;Z)Z

    .line 1015
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->filename_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$3702(Lcom/google/speech/patts/Specification;Ljava/lang/String;)Ljava/lang/String;

    .line 1016
    return-object p0
.end method

.method public setFilenameId(I)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 939
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasFilenameId:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$2802(Lcom/google/speech/patts/Specification;Z)Z

    .line 940
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->filenameId_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$2902(Lcom/google/speech/patts/Specification;I)I

    .line 941
    return-object p0
.end method

.method public setFirstDaughter(I)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasFirstDaughter:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$602(Lcom/google/speech/patts/Specification;Z)Z

    .line 720
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->firstDaughter_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$702(Lcom/google/speech/patts/Specification;I)I

    .line 721
    return-object p0
.end method

.method public setLastDaughter(I)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 737
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasLastDaughter:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$802(Lcom/google/speech/patts/Specification;Z)Z

    .line 738
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->lastDaughter_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$902(Lcom/google/speech/patts/Specification;I)I

    .line 739
    return-object p0
.end method

.method public setMid(F)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 831
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasMid:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$1602(Lcom/google/speech/patts/Specification;Z)Z

    .line 832
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->mid_:F
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$1702(Lcom/google/speech/patts/Specification;F)F

    .line 833
    return-object p0
.end method

.method public setModelName(Ljava/lang/String;)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1066
    if-nez p1, :cond_0

    .line 1067
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1069
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasModelName:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$3802(Lcom/google/speech/patts/Specification;Z)Z

    .line 1070
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->modelName_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$3902(Lcom/google/speech/patts/Specification;Ljava/lang/String;)Ljava/lang/String;

    .line 1071
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 755
    if-nez p1, :cond_0

    .line 756
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 758
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$1002(Lcom/google/speech/patts/Specification;Z)Z

    .line 759
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$1102(Lcom/google/speech/patts/Specification;Ljava/lang/String;)Ljava/lang/String;

    .line 760
    return-object p0
.end method

.method public setNumPmToEnd(I)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 903
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasNumPmToEnd:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$2402(Lcom/google/speech/patts/Specification;Z)Z

    .line 904
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->numPmToEnd_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$2502(Lcom/google/speech/patts/Specification;I)I

    .line 905
    return-object p0
.end method

.method public setNumPmToMid(I)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 885
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasNumPmToMid:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$2202(Lcom/google/speech/patts/Specification;Z)Z

    .line 886
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->numPmToMid_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$2302(Lcom/google/speech/patts/Specification;I)I

    .line 887
    return-object p0
.end method

.method public setParent(I)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 701
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasParent:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$402(Lcom/google/speech/patts/Specification;Z)Z

    .line 702
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->parent_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$502(Lcom/google/speech/patts/Specification;I)I

    .line 703
    return-object p0
.end method

.method public setPatts1Diagnostics(Ljava/lang/String;)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1108
    if-nez p1, :cond_0

    .line 1109
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1111
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasPatts1Diagnostics:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$4202(Lcom/google/speech/patts/Specification;Z)Z

    .line 1112
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->patts1Diagnostics_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$4302(Lcom/google/speech/patts/Specification;Ljava/lang/String;)Ljava/lang/String;

    .line 1113
    return-object p0
.end method

.method public setPreviousUnit(I)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 993
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasPreviousUnit:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$3402(Lcom/google/speech/patts/Specification;Z)Z

    .line 994
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->previousUnit_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$3502(Lcom/google/speech/patts/Specification;I)I

    .line 995
    return-object p0
.end method

.method public setRunningTotal(F)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 975
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasRunningTotal:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$3202(Lcom/google/speech/patts/Specification;Z)Z

    .line 976
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->runningTotal_:F
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$3302(Lcom/google/speech/patts/Specification;F)F

    .line 977
    return-object p0
.end method

.method public setStart(F)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 813
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasStart:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$1402(Lcom/google/speech/patts/Specification;Z)Z

    .line 814
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->start_:F
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$1502(Lcom/google/speech/patts/Specification;F)F

    .line 815
    return-object p0
.end method

.method public setStartPm(I)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 867
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasStartPm:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$2002(Lcom/google/speech/patts/Specification;Z)Z

    .line 868
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->startPm_:I
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$2102(Lcom/google/speech/patts/Specification;I)I

    .line 869
    return-object p0
.end method

.method public setSupraSegmentalModelName(Ljava/lang/String;)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1087
    if-nez p1, :cond_0

    .line 1088
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1090
    :cond_0
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasSupraSegmentalModelName:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$4002(Lcom/google/speech/patts/Specification;Z)Z

    .line 1091
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->supraSegmentalModelName_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$4102(Lcom/google/speech/patts/Specification;Ljava/lang/String;)Ljava/lang/String;

    .line 1092
    return-object p0
.end method

.method public setTargetCost(F)Lcom/google/speech/patts/Specification$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 957
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    const/4 v1, 0x1

    # setter for: Lcom/google/speech/patts/Specification;->hasTargetCost:Z
    invoke-static {v0, v1}, Lcom/google/speech/patts/Specification;->access$3002(Lcom/google/speech/patts/Specification;Z)Z

    .line 958
    iget-object v0, p0, Lcom/google/speech/patts/Specification$Builder;->result:Lcom/google/speech/patts/Specification;

    # setter for: Lcom/google/speech/patts/Specification;->targetCost_:F
    invoke-static {v0, p1}, Lcom/google/speech/patts/Specification;->access$3102(Lcom/google/speech/patts/Specification;F)F

    .line 959
    return-object p0
.end method
