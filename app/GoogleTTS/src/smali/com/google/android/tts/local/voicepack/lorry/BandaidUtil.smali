.class public Lcom/google/android/tts/local/voicepack/lorry/BandaidUtil;
.super Ljava/lang/Object;
.source "BandaidUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFallbackURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "bandaidUrl"    # Ljava/lang/String;

    .prologue
    .line 13
    const-string v0, "redirector.gvt1.com/edgedl"

    const-string v1, "dl.google.com"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isBandaidURL(Ljava/lang/String;)Z
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 6
    const-string v0, "redirector.gvt1.com/edgedl"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
