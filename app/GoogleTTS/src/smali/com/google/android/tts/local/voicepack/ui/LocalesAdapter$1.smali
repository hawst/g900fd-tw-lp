.class Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter$1;
.super Ljava/lang/Object;
.source "LocalesAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->getEntryView(Ljava/util/Locale;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;

.field final synthetic val$locale:Ljava/util/Locale;


# direct methods
.method constructor <init>(Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;Ljava/util/Locale;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter$1;->this$0:Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;

    iput-object p2, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter$1;->val$locale:Ljava/util/Locale;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 73
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter$1;->this$0:Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;

    # getter for: Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->access$000(Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;)Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 74
    .local v0, "intent":Landroid/content/Intent;
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter$1;->val$locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter$1;->val$locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 75
    .local v1, "localeStr":[Ljava/lang/String;
    const-string v2, "locale"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter$1;->this$0:Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;

    # getter for: Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->access$000(Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 77
    return-void
.end method
