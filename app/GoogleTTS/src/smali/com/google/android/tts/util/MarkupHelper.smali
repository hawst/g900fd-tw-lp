.class public Lcom/google/android/tts/util/MarkupHelper;
.super Ljava/lang/Object;
.source "MarkupHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static LogWarning(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "type"    # Ljava/lang/String;
    .param p1, "arg"    # Ljava/lang/String;

    .prologue
    .line 235
    const-string v0, "MarkupHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TtsSpan of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not have a value for argument "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    return-void
.end method

.method public static createSay(Landroid/text/style/TtsSpan;Lcom/google/speech/tts/Sentence$Builder;Ljava/lang/CharSequence;Ljava/util/Locale;)Lcom/google/speech/tts/Say;
    .locals 39
    .param p0, "span"    # Landroid/text/style/TtsSpan;
    .param p1, "sentence"    # Lcom/google/speech/tts/Sentence$Builder;
    .param p2, "spannedText"    # Ljava/lang/CharSequence;
    .param p3, "locale"    # Ljava/util/Locale;

    .prologue
    .line 55
    invoke-static {}, Lcom/google/speech/tts/Say;->newBuilder()Lcom/google/speech/tts/Say$Builder;

    move-result-object v4

    .line 56
    .local v4, "builder":Lcom/google/speech/tts/Say$Builder;
    invoke-virtual/range {p0 .. p0}, Landroid/text/style/TtsSpan;->getType()Ljava/lang/String;

    move-result-object v33

    .line 57
    .local v33, "type":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/text/style/TtsSpan;->getArgs()Landroid/os/PersistableBundle;

    move-result-object v2

    .line 59
    .local v2, "args":Landroid/os/PersistableBundle;
    const/16 v23, 0x0

    .line 60
    .local v23, "morphosyntacticFeatures":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v36

    const-string v37, "rus"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_0

    .line 61
    invoke-static {v2}, Lcom/google/android/tts/util/MarkupHelper;->russianMorphosyntacticString(Landroid/os/PersistableBundle;)Ljava/lang/String;

    move-result-object v23

    .line 64
    :cond_0
    const-string v36, "android.type.text"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_2

    .line 65
    const-string v36, "android.arg.text"

    move-object/from16 v0, v36

    invoke-static {v2, v0}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 66
    .local v31, "text":Ljava/lang/String;
    if-eqz v31, :cond_1

    .line 67
    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/google/speech/tts/Say$Builder;->setText(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    .line 231
    .end local v31    # "text":Ljava/lang/String;
    :goto_0
    invoke-virtual {v4}, Lcom/google/speech/tts/Say$Builder;->build()Lcom/google/speech/tts/Say;

    move-result-object v36

    :goto_1
    return-object v36

    .line 70
    .restart local v31    # "text":Ljava/lang/String;
    :cond_1
    invoke-interface/range {p2 .. p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-virtual {v4, v0}, Lcom/google/speech/tts/Say$Builder;->setText(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    goto :goto_0

    .line 72
    .end local v31    # "text":Ljava/lang/String;
    :cond_2
    const-string v36, "android.type.cardinal"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_4

    .line 73
    move-object/from16 v0, v23

    invoke-static {v2, v0}, Lcom/google/android/tts/util/MarkupHelper;->makeCardinal(Landroid/os/PersistableBundle;Ljava/lang/String;)Lcom/google/speech/tts/Cardinal;

    move-result-object v5

    .line 74
    .local v5, "cardinal":Lcom/google/speech/tts/Cardinal;
    if-nez v5, :cond_3

    .line 75
    const/16 v36, 0x0

    goto :goto_1

    .line 77
    :cond_3
    invoke-virtual {v4, v5}, Lcom/google/speech/tts/Say$Builder;->setCardinal(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Say$Builder;

    goto :goto_0

    .line 78
    .end local v5    # "cardinal":Lcom/google/speech/tts/Cardinal;
    :cond_4
    const-string v36, "android.type.decimal"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_6

    .line 79
    invoke-static {v2}, Lcom/google/android/tts/util/MarkupHelper;->makeDecimal(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Decimal;

    move-result-object v8

    .line 80
    .local v8, "decimal":Lcom/google/speech/tts/Decimal;
    if-nez v8, :cond_5

    .line 81
    const/16 v36, 0x0

    goto :goto_1

    .line 83
    :cond_5
    invoke-virtual {v4, v8}, Lcom/google/speech/tts/Say$Builder;->setDecimal(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Say$Builder;

    goto :goto_0

    .line 84
    .end local v8    # "decimal":Lcom/google/speech/tts/Decimal;
    :cond_6
    const-string v36, "android.type.fraction"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_8

    .line 85
    invoke-static {v2}, Lcom/google/android/tts/util/MarkupHelper;->makeFraction(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Fraction;

    move-result-object v14

    .line 86
    .local v14, "fraction":Lcom/google/speech/tts/Fraction;
    if-nez v14, :cond_7

    .line 87
    const/16 v36, 0x0

    goto :goto_1

    .line 89
    :cond_7
    invoke-virtual {v4, v14}, Lcom/google/speech/tts/Say$Builder;->setFraction(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Say$Builder;

    goto :goto_0

    .line 90
    .end local v14    # "fraction":Lcom/google/speech/tts/Fraction;
    :cond_8
    const-string v36, "android.type.ordinal"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_a

    .line 91
    invoke-static {v2}, Lcom/google/android/tts/util/MarkupHelper;->makeOrdinal(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Ordinal;

    move-result-object v26

    .line 92
    .local v26, "ordinal":Lcom/google/speech/tts/Ordinal;
    if-nez v26, :cond_9

    .line 93
    const/16 v36, 0x0

    goto :goto_1

    .line 95
    :cond_9
    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Lcom/google/speech/tts/Say$Builder;->setOrdinal(Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Say$Builder;

    goto :goto_0

    .line 96
    .end local v26    # "ordinal":Lcom/google/speech/tts/Ordinal;
    :cond_a
    const-string v36, "android.type.measure"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_c

    .line 97
    invoke-static {v2}, Lcom/google/android/tts/util/MarkupHelper;->makeMeasure(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Measure;

    move-result-object v22

    .line 98
    .local v22, "measure":Lcom/google/speech/tts/Measure;
    if-nez v22, :cond_b

    .line 99
    const/16 v36, 0x0

    goto/16 :goto_1

    .line 101
    :cond_b
    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lcom/google/speech/tts/Say$Builder;->setMeasure(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Say$Builder;

    goto/16 :goto_0

    .line 102
    .end local v22    # "measure":Lcom/google/speech/tts/Measure;
    :cond_c
    const-string v36, "android.type.time"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_e

    .line 103
    invoke-static {v2}, Lcom/google/android/tts/util/MarkupHelper;->makeTime(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Time;

    move-result-object v32

    .line 104
    .local v32, "time":Lcom/google/speech/tts/Time;
    if-nez v32, :cond_d

    .line 105
    const/16 v36, 0x0

    goto/16 :goto_1

    .line 107
    :cond_d
    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Lcom/google/speech/tts/Say$Builder;->setTime(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Say$Builder;

    goto/16 :goto_0

    .line 108
    .end local v32    # "time":Lcom/google/speech/tts/Time;
    :cond_e
    const-string v36, "android.type.date"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_10

    .line 109
    invoke-static {v2}, Lcom/google/android/tts/util/MarkupHelper;->makeDate(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Date;

    move-result-object v7

    .line 110
    .local v7, "date":Lcom/google/speech/tts/Date;
    if-nez v7, :cond_f

    .line 111
    const/16 v36, 0x0

    goto/16 :goto_1

    .line 113
    :cond_f
    invoke-virtual {v4, v7}, Lcom/google/speech/tts/Say$Builder;->setDate(Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Say$Builder;

    goto/16 :goto_0

    .line 114
    .end local v7    # "date":Lcom/google/speech/tts/Date;
    :cond_10
    const-string v36, "android.type.telephone"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_12

    .line 115
    invoke-static {v2}, Lcom/google/android/tts/util/MarkupHelper;->makeTelephone(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Telephone;

    move-result-object v30

    .line 116
    .local v30, "telephone":Lcom/google/speech/tts/Telephone;
    if-nez v30, :cond_11

    .line 117
    const/16 v36, 0x0

    goto/16 :goto_1

    .line 119
    :cond_11
    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Lcom/google/speech/tts/Say$Builder;->setTelephone(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Say$Builder;

    goto/16 :goto_0

    .line 120
    .end local v30    # "telephone":Lcom/google/speech/tts/Telephone;
    :cond_12
    const-string v36, "android.type.electronic"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_14

    .line 121
    invoke-static {v2}, Lcom/google/android/tts/util/MarkupHelper;->makeElectronic(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Electronic;

    move-result-object v10

    .line 122
    .local v10, "electronic":Lcom/google/speech/tts/Electronic;
    if-nez v10, :cond_13

    .line 123
    const/16 v36, 0x0

    goto/16 :goto_1

    .line 125
    :cond_13
    invoke-virtual {v4, v10}, Lcom/google/speech/tts/Say$Builder;->setElectronic(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Say$Builder;

    goto/16 :goto_0

    .line 126
    .end local v10    # "electronic":Lcom/google/speech/tts/Electronic;
    :cond_14
    const-string v36, "android.type.digits"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_17

    .line 127
    const-string v36, "android.arg.digits"

    move-object/from16 v0, v36

    invoke-static {v2, v0}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 128
    .local v9, "digits":Ljava/lang/String;
    if-eqz v9, :cond_15

    .line 129
    invoke-virtual {v4, v9}, Lcom/google/speech/tts/Say$Builder;->setDigit(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    goto/16 :goto_0

    .line 131
    :cond_15
    invoke-interface/range {p2 .. p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v36

    const-string v37, "[0-9]+"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v36

    if-eqz v36, :cond_16

    .line 132
    invoke-interface/range {p2 .. p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-virtual {v4, v0}, Lcom/google/speech/tts/Say$Builder;->setDigit(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    goto/16 :goto_0

    .line 134
    :cond_16
    const/16 v36, 0x0

    goto/16 :goto_1

    .line 136
    .end local v9    # "digits":Ljava/lang/String;
    :cond_17
    const-string v36, "android.type.verbatim"

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_19

    .line 137
    const-string v36, "android.arg.verbatim"

    move-object/from16 v0, v36

    invoke-static {v2, v0}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 138
    .local v34, "verbatim":Ljava/lang/String;
    if-eqz v34, :cond_18

    .line 139
    move-object/from16 v0, v34

    invoke-virtual {v4, v0}, Lcom/google/speech/tts/Say$Builder;->setVerbatim(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    goto/16 :goto_0

    .line 141
    :cond_18
    invoke-interface/range {p2 .. p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-virtual {v4, v0}, Lcom/google/speech/tts/Say$Builder;->setVerbatim(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    goto/16 :goto_0

    .line 146
    .end local v34    # "verbatim":Ljava/lang/String;
    :cond_19
    invoke-virtual/range {p0 .. p0}, Landroid/text/style/TtsSpan;->getType()Ljava/lang/String;

    move-result-object v36

    sget-object v37, Lcom/google/android/tts/util/TtsSpanFeatures;->TYPE_PRONUNCIATION:Ljava/lang/String;

    invoke-virtual/range {v36 .. v37}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_1c

    .line 147
    sget-object v36, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_PRONUNCIATION:Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-static {v2, v0}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 148
    .local v27, "pron":Ljava/lang/String;
    if-nez v27, :cond_1a

    .line 150
    sget-object v36, Lcom/google/android/tts/util/TtsSpanFeatures;->TYPE_PRONUNCIATION:Ljava/lang/String;

    sget-object v37, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_PRONUNCIATION:Ljava/lang/String;

    invoke-static/range {v36 .. v37}, Lcom/google/android/tts/util/MarkupHelper;->LogWarning(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const/16 v36, 0x0

    goto/16 :goto_1

    .line 155
    :cond_1a
    invoke-static/range {p3 .. p3}, Lcom/google/android/tts/util/LocalesHelper;->normalizeTTSLocale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v25

    .line 157
    .local v25, "normalizedLocale":Ljava/util/Locale;
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Spelling;->newBuilder()Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v36

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v25 .. v25}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ".0"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->setId(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v36

    invoke-interface/range {p2 .. p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->setV(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Spelling$Builder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Lcom/google/speech/tts/LexiconProto$Spelling$Builder;->build()Lcom/google/speech/tts/LexiconProto$Spelling;

    move-result-object v29

    .line 163
    .local v29, "spelling":Lcom/google/speech/tts/LexiconProto$Spelling;
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Pronunciation;->newBuilder()Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v36

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v25 .. v25}, Ljava/util/Locale;->toLanguageTag()Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ".0"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->setId(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->setV(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Lcom/google/speech/tts/LexiconProto$Pronunciation$Builder;->build()Lcom/google/speech/tts/LexiconProto$Pronunciation;

    move-result-object v28

    .line 172
    .local v28, "pronunciation":Lcom/google/speech/tts/LexiconProto$Pronunciation;
    const-string v35, "pronunciationspanwordid_"

    .line 174
    .local v35, "wordId":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/speech/tts/Sentence$Builder;->getEntryCount()I

    move-result v36

    invoke-static/range {v36 .. v36}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .local v3, "arr$":[C
    array-length v0, v3

    move/from16 v18, v0

    .local v18, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_2
    move/from16 v0, v18

    if-ge v15, v0, :cond_1b

    aget-char v24, v3, v15

    .line 175
    .local v24, "n":C
    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-static/range {v24 .. v24}, Ljava/lang/Character;->getNumericValue(C)I

    move-result v37

    add-int/lit8 v37, v37, 0x61

    move/from16 v0, v37

    int-to-char v0, v0

    move/from16 v37, v0

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    .line 174
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 178
    .end local v24    # "n":C
    :cond_1b
    invoke-static {}, Lcom/google/speech/tts/LexiconProto$Entry;->newBuilder()Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->setId(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->addS(Lcom/google/speech/tts/LexiconProto$Spelling;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->addP(Lcom/google/speech/tts/LexiconProto$Pronunciation;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v36

    const-string v37, "from TYPE_PRONUNCIATION span"

    invoke-virtual/range {v36 .. v37}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->setC(Ljava/lang/String;)Lcom/google/speech/tts/LexiconProto$Entry$Builder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Lcom/google/speech/tts/LexiconProto$Entry$Builder;->build()Lcom/google/speech/tts/LexiconProto$Entry;

    move-result-object v11

    .line 186
    .local v11, "entry":Lcom/google/speech/tts/LexiconProto$Entry;
    invoke-static {}, Lcom/google/speech/tts/MarkupWord;->newBuilder()Lcom/google/speech/tts/MarkupWord$Builder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/speech/tts/MarkupWord$Builder;->setId(Ljava/lang/String;)Lcom/google/speech/tts/MarkupWord$Builder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Lcom/google/speech/tts/MarkupWord$Builder;->build()Lcom/google/speech/tts/MarkupWord;

    move-result-object v21

    .line 191
    .local v21, "markupWord":Lcom/google/speech/tts/MarkupWord;
    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/google/speech/tts/Say$Builder;->addWordSequence(Lcom/google/speech/tts/MarkupWord;)Lcom/google/speech/tts/Say$Builder;

    .line 192
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/speech/tts/Sentence$Builder;->addEntry(Lcom/google/speech/tts/LexiconProto$Entry;)Lcom/google/speech/tts/Sentence$Builder;

    goto/16 :goto_0

    .line 197
    .end local v3    # "arr$":[C
    .end local v11    # "entry":Lcom/google/speech/tts/LexiconProto$Entry;
    .end local v15    # "i$":I
    .end local v18    # "len$":I
    .end local v21    # "markupWord":Lcom/google/speech/tts/MarkupWord;
    .end local v25    # "normalizedLocale":Ljava/util/Locale;
    .end local v27    # "pron":Ljava/lang/String;
    .end local v28    # "pronunciation":Lcom/google/speech/tts/LexiconProto$Pronunciation;
    .end local v29    # "spelling":Lcom/google/speech/tts/LexiconProto$Spelling;
    .end local v35    # "wordId":Ljava/lang/String;
    :cond_1c
    invoke-virtual/range {p0 .. p0}, Landroid/text/style/TtsSpan;->getType()Ljava/lang/String;

    move-result-object v36

    sget-object v37, Lcom/google/android/tts/util/TtsSpanFeatures;->TYPE_LOCATION:Ljava/lang/String;

    invoke-virtual/range {v36 .. v37}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_21

    .line 198
    invoke-static {}, Llocation/unified/LocationDescriptor;->newBuilder()Llocation/unified/LocationDescriptor$Builder;

    move-result-object v19

    .line 200
    .local v19, "location":Llocation/unified/LocationDescriptor$Builder;
    sget-object v36, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_LATITUDE_E7:Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-static {v2, v0}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsInt(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v17

    .line 202
    .local v17, "lattiude":Ljava/lang/Integer;
    sget-object v36, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_LONGITUDE_E7:Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-static {v2, v0}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsInt(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v20

    .line 204
    .local v20, "longitude":Ljava/lang/Integer;
    if-eqz v17, :cond_1d

    if-eqz v20, :cond_1d

    .line 205
    invoke-static {}, Llocation/unified/LatLng;->newBuilder()Llocation/unified/LatLng$Builder;

    move-result-object v36

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v37

    invoke-virtual/range {v36 .. v37}, Llocation/unified/LatLng$Builder;->setLatitudeE7(I)Llocation/unified/LatLng$Builder;

    move-result-object v36

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v37

    invoke-virtual/range {v36 .. v37}, Llocation/unified/LatLng$Builder;->setLongitudeE7(I)Llocation/unified/LatLng$Builder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Llocation/unified/LatLng$Builder;->build()Llocation/unified/LatLng;

    move-result-object v16

    .line 209
    .local v16, "latlng":Llocation/unified/LatLng;
    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Llocation/unified/LocationDescriptor$Builder;->setLatlng(Llocation/unified/LatLng;)Llocation/unified/LocationDescriptor$Builder;

    .line 211
    .end local v16    # "latlng":Llocation/unified/LatLng;
    :cond_1d
    sget-object v36, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_CELL_ID:Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v36

    if-nez v36, :cond_1e

    sget-object v36, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_FPRINT:Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v36

    if-eqz v36, :cond_20

    .line 213
    :cond_1e
    invoke-static {}, Llocation/unified/FeatureIdProto;->newBuilder()Llocation/unified/FeatureIdProto$Builder;

    move-result-object v12

    .line 214
    .local v12, "feature_id":Llocation/unified/FeatureIdProto$Builder;
    sget-object v36, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_CELL_ID:Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-static {v2, v0}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsLong(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    .line 216
    .local v6, "cell_id":Ljava/lang/Long;
    if-eqz v6, :cond_1f

    .line 217
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v36

    move-wide/from16 v0, v36

    invoke-virtual {v12, v0, v1}, Llocation/unified/FeatureIdProto$Builder;->setCellId(J)Llocation/unified/FeatureIdProto$Builder;

    .line 219
    :cond_1f
    sget-object v36, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_FPRINT:Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-static {v2, v0}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsLong(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v13

    .line 221
    .local v13, "fprint":Ljava/lang/Long;
    if-eqz v13, :cond_20

    .line 222
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v36

    move-wide/from16 v0, v36

    invoke-virtual {v12, v0, v1}, Llocation/unified/FeatureIdProto$Builder;->setFprint(J)Llocation/unified/FeatureIdProto$Builder;

    .line 225
    .end local v6    # "cell_id":Ljava/lang/Long;
    .end local v12    # "feature_id":Llocation/unified/FeatureIdProto$Builder;
    .end local v13    # "fprint":Ljava/lang/Long;
    :cond_20
    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/google/speech/tts/Say$Builder;->setLocation(Llocation/unified/LocationDescriptor$Builder;)Lcom/google/speech/tts/Say$Builder;

    goto/16 :goto_0

    .line 228
    .end local v17    # "lattiude":Ljava/lang/Integer;
    .end local v19    # "location":Llocation/unified/LocationDescriptor$Builder;
    .end local v20    # "longitude":Ljava/lang/Integer;
    :cond_21
    const-string v36, "MarkupHelper"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "TtsSpan of type "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " is not recognized."

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const/16 v36, 0x0

    goto/16 :goto_1
.end method

.method public static createSayPause(FLcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/Say;
    .locals 2
    .param p0, "duration"    # F
    .param p1, "voicemod"    # Lcom/google/speech/tts/VoiceMod;

    .prologue
    .line 680
    invoke-static {}, Lcom/google/speech/tts/Say;->newBuilder()Lcom/google/speech/tts/Say$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/speech/tts/Say$Builder;->setPause(F)Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    .line 681
    .local v0, "builder":Lcom/google/speech/tts/Say$Builder;
    if-eqz p1, :cond_0

    .line 682
    invoke-virtual {v0, p1}, Lcom/google/speech/tts/Say$Builder;->setVoicemod(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/Say$Builder;

    .line 684
    :cond_0
    invoke-virtual {v0}, Lcom/google/speech/tts/Say$Builder;->build()Lcom/google/speech/tts/Say;

    move-result-object v1

    return-object v1
.end method

.method public static createSayText(Ljava/lang/String;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/Say;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "voicemod"    # Lcom/google/speech/tts/VoiceMod;

    .prologue
    .line 671
    invoke-static {}, Lcom/google/speech/tts/Say;->newBuilder()Lcom/google/speech/tts/Say$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/speech/tts/Say$Builder;->setText(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    move-result-object v0

    .line 672
    .local v0, "builder":Lcom/google/speech/tts/Say$Builder;
    if-eqz p1, :cond_0

    .line 673
    invoke-virtual {v0, p1}, Lcom/google/speech/tts/Say$Builder;->setVoicemod(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/Say$Builder;

    .line 675
    :cond_0
    invoke-virtual {v0}, Lcom/google/speech/tts/Say$Builder;->build()Lcom/google/speech/tts/Say;

    move-result-object v1

    return-object v1
.end method

.method public static createSentenceBuilder(Ljava/util/List;)Lcom/google/speech/tts/Sentence$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Say;",
            ">;)",
            "Lcom/google/speech/tts/Sentence$Builder;"
        }
    .end annotation

    .prologue
    .line 694
    .local p0, "sayList":Ljava/util/List;, "Ljava/util/List<Lcom/google/speech/tts/Say;>;"
    invoke-static {}, Lcom/google/speech/tts/Sentence;->newBuilder()Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/tts/Sentence$Builder;->addAllSay(Ljava/lang/Iterable;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "args"    # Landroid/os/PersistableBundle;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 604
    invoke-virtual {p0, p1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 605
    .local v0, "obj":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 606
    check-cast v0, Ljava/lang/String;

    .line 612
    .end local v0    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 607
    .restart local v0    # "obj":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Integer;

    if-nez v1, :cond_1

    instance-of v1, v0, Ljava/lang/Double;

    if-nez v1, :cond_1

    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 610
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 612
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getNumberAsInt(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 10
    .param p0, "args"    # Landroid/os/PersistableBundle;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 517
    invoke-virtual {p0, p1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 518
    .local v3, "obj":Ljava/lang/Object;
    instance-of v6, v3, Ljava/lang/String;

    if-eqz v6, :cond_1

    move-object v2, v3

    .line 519
    check-cast v2, Ljava/lang/String;

    .line 520
    .local v2, "number":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x2b

    if-ne v6, v7, :cond_0

    .line 521
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 524
    :cond_0
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 551
    .end local v2    # "number":Ljava/lang/String;
    .end local v3    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v3

    .line 525
    .restart local v2    # "number":Ljava/lang/String;
    .restart local v3    # "obj":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 526
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v6, "MarkupHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The value of argument "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " could not be parsed as an integer."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v5

    .line 528
    goto :goto_0

    .line 530
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    .end local v2    # "number":Ljava/lang/String;
    :cond_1
    instance-of v6, v3, Ljava/lang/Integer;

    if-eqz v6, :cond_2

    .line 531
    check-cast v3, Ljava/lang/Integer;

    goto :goto_0

    .line 532
    :cond_2
    instance-of v6, v3, Ljava/lang/Double;

    if-eqz v6, :cond_5

    move-object v0, v3

    .line 533
    check-cast v0, Ljava/lang/Double;

    .line 534
    .local v0, "d":Ljava/lang/Double;
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide v8, 0x41dfffffffc00000L    # 2.147483647E9

    cmpl-double v6, v6, v8

    if-gtz v6, :cond_3

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide/high16 v8, -0x3e20000000000000L    # -2.147483648E9

    cmpg-double v6, v6, v8

    if-gez v6, :cond_4

    .line 535
    :cond_3
    const-string v6, "MarkupHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The value of the argument "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is not in integer range: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v5

    .line 537
    goto :goto_0

    .line 539
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    .line 540
    .end local v0    # "d":Ljava/lang/Double;
    :cond_5
    instance-of v6, v3, Ljava/lang/Long;

    if-eqz v6, :cond_8

    move-object v4, v3

    .line 541
    check-cast v4, Ljava/lang/Long;

    .line 542
    .local v4, "value":Ljava/lang/Long;
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/32 v8, 0x7fffffff

    cmp-long v6, v6, v8

    if-gtz v6, :cond_6

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/32 v8, -0x80000000

    cmp-long v6, v6, v8

    if-gez v6, :cond_7

    .line 543
    :cond_6
    const-string v6, "MarkupHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The value of the argument "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is not in integer range: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v5

    .line 545
    goto/16 :goto_0

    .line 547
    :cond_7
    check-cast v3, Ljava/lang/Long;

    .end local v3    # "obj":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Long;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_0

    .line 549
    .end local v4    # "value":Ljava/lang/Long;
    .restart local v3    # "obj":Ljava/lang/Object;
    :cond_8
    const-string v6, "MarkupHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The value of argument "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " should not be passed as an object."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v5

    .line 551
    goto/16 :goto_0
.end method

.method private static getNumberAsLong(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/Long;
    .locals 10
    .param p0, "args"    # Landroid/os/PersistableBundle;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 564
    invoke-virtual {p0, p1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 565
    .local v3, "obj":Ljava/lang/Object;
    instance-of v5, v3, Ljava/lang/Long;

    if-eqz v5, :cond_0

    .line 566
    check-cast v3, Ljava/lang/Long;

    .line 592
    .end local v3    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v3

    .line 567
    .restart local v3    # "obj":Ljava/lang/Object;
    :cond_0
    instance-of v5, v3, Ljava/lang/String;

    if-eqz v5, :cond_2

    move-object v2, v3

    .line 568
    check-cast v2, Ljava/lang/String;

    .line 569
    .local v2, "number":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2b

    if-ne v5, v6, :cond_1

    .line 570
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 573
    :cond_1
    :try_start_0
    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 574
    :catch_0
    move-exception v1

    .line 575
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v5, "MarkupHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "The value of argument "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " could not be parsed as a long."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    .line 577
    goto :goto_0

    .line 579
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    .end local v2    # "number":Ljava/lang/String;
    :cond_2
    instance-of v5, v3, Ljava/lang/Integer;

    if-eqz v5, :cond_3

    .line 580
    check-cast v3, Ljava/lang/Long;

    goto :goto_0

    .line 581
    :cond_3
    instance-of v5, v3, Ljava/lang/Double;

    if-eqz v5, :cond_6

    move-object v0, v3

    .line 582
    check-cast v0, Ljava/lang/Double;

    .line 583
    .local v0, "d":Ljava/lang/Double;
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide/high16 v8, 0x43e0000000000000L    # 9.223372036854776E18

    cmpl-double v5, v6, v8

    if-gtz v5, :cond_4

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide/high16 v8, -0x3c20000000000000L    # -9.223372036854776E18

    cmpg-double v5, v6, v8

    if-gez v5, :cond_5

    .line 584
    :cond_4
    const-string v5, "MarkupHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "The value of the argument "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is not in long range: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    .line 586
    goto/16 :goto_0

    .line 588
    :cond_5
    invoke-virtual {v0}, Ljava/lang/Double;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto/16 :goto_0

    .line 590
    .end local v0    # "d":Ljava/lang/Double;
    :cond_6
    const-string v5, "MarkupHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "The value of argument "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " should not be passed as an object."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    .line 592
    goto/16 :goto_0
.end method

.method private static getNumberAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "args"    # Landroid/os/PersistableBundle;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 492
    invoke-virtual {p0, p1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 493
    .local v1, "obj":Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 494
    check-cast v0, Ljava/lang/String;

    .line 495
    .local v0, "number":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2b

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 504
    .end local v0    # "number":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 497
    :cond_1
    instance-of v2, v1, Ljava/lang/Integer;

    if-nez v2, :cond_2

    instance-of v2, v1, Ljava/lang/Double;

    if-nez v2, :cond_2

    instance-of v2, v1, Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 500
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 502
    :cond_3
    const-string v2, "MarkupHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The value of argument "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " should not be passed as an object."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static makeCardinal(Landroid/os/PersistableBundle;Ljava/lang/String;)Lcom/google/speech/tts/Cardinal;
    .locals 4
    .param p0, "args"    # Landroid/os/PersistableBundle;
    .param p1, "morphosyntacticFeatures"    # Ljava/lang/String;

    .prologue
    .line 240
    invoke-static {}, Lcom/google/speech/tts/Cardinal;->newBuilder()Lcom/google/speech/tts/Cardinal$Builder;

    move-result-object v0

    .line 241
    .local v0, "cardinal":Lcom/google/speech/tts/Cardinal$Builder;
    const-string v2, "android.arg.number"

    invoke-static {p0, v2}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, "integer":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 243
    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Cardinal$Builder;->setInteger(Ljava/lang/String;)Lcom/google/speech/tts/Cardinal$Builder;

    .line 251
    if-eqz p1, :cond_0

    .line 252
    invoke-virtual {v0, p1}, Lcom/google/speech/tts/Cardinal$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lcom/google/speech/tts/Cardinal$Builder;

    .line 254
    :cond_0
    invoke-virtual {v0}, Lcom/google/speech/tts/Cardinal$Builder;->build()Lcom/google/speech/tts/Cardinal;

    move-result-object v2

    :goto_0
    return-object v2

    .line 248
    :cond_1
    const-string v2, "android.type.cardinal"

    const-string v3, "android.arg.number"

    invoke-static {v2, v3}, Lcom/google/android/tts/util/MarkupHelper;->LogWarning(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static makeDate(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Date;
    .locals 7
    .param p0, "args"    # Landroid/os/PersistableBundle;

    .prologue
    .line 377
    const/4 v3, 0x0

    .line 378
    .local v3, "valid":Z
    invoke-static {}, Lcom/google/speech/tts/Date;->newBuilder()Lcom/google/speech/tts/Date$Builder;

    move-result-object v0

    .line 379
    .local v0, "date":Lcom/google/speech/tts/Date$Builder;
    const-string v6, "android.arg.day"

    invoke-static {p0, v6}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 380
    .local v1, "day":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 381
    const/4 v3, 0x1

    .line 382
    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Date$Builder;->setDay(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;

    .line 384
    :cond_0
    const-string v6, "android.arg.weekday"

    invoke-static {p0, v6}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 385
    .local v4, "weekday":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 386
    const/4 v3, 0x1

    .line 387
    invoke-virtual {v0, v4}, Lcom/google/speech/tts/Date$Builder;->setWeekday(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;

    .line 389
    :cond_1
    const-string v6, "android.arg.month"

    invoke-static {p0, v6}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 390
    .local v2, "month":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 391
    const/4 v3, 0x1

    .line 392
    invoke-virtual {v0, v2}, Lcom/google/speech/tts/Date$Builder;->setMonth(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;

    .line 394
    :cond_2
    const-string v6, "android.arg.year"

    invoke-static {p0, v6}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 395
    .local v5, "year":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 396
    const/4 v3, 0x1

    .line 397
    invoke-virtual {v0, v5}, Lcom/google/speech/tts/Date$Builder;->setYear(Ljava/lang/String;)Lcom/google/speech/tts/Date$Builder;

    .line 400
    :cond_3
    if-eqz v3, :cond_4

    .line 401
    invoke-virtual {v0}, Lcom/google/speech/tts/Date$Builder;->build()Lcom/google/speech/tts/Date;

    move-result-object v6

    .line 403
    :goto_0
    return-object v6

    :cond_4
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private static makeDecimal(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Decimal;
    .locals 5
    .param p0, "args"    # Landroid/os/PersistableBundle;

    .prologue
    .line 283
    invoke-static {}, Lcom/google/speech/tts/Decimal;->newBuilder()Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v0

    .line 284
    .local v0, "decimal":Lcom/google/speech/tts/Decimal$Builder;
    const-string v3, "android.arg.integer_part"

    invoke-static {p0, v3}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 285
    .local v2, "integerPart":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 286
    invoke-virtual {v0, v2}, Lcom/google/speech/tts/Decimal$Builder;->setIntegerPart(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;

    .line 293
    const-string v3, "android.arg.fractional_part"

    invoke-static {p0, v3}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 295
    .local v1, "fractionalPart":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 296
    invoke-virtual {v0, v1}, Lcom/google/speech/tts/Decimal$Builder;->setFractionalPart(Ljava/lang/String;)Lcom/google/speech/tts/Decimal$Builder;

    .line 298
    :cond_0
    invoke-virtual {v0}, Lcom/google/speech/tts/Decimal$Builder;->build()Lcom/google/speech/tts/Decimal;

    move-result-object v3

    .end local v1    # "fractionalPart":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 290
    :cond_1
    const-string v3, "android.type.decimal"

    const-string v4, "android.arg.integer_part"

    invoke-static {v3, v4}, Lcom/google/android/tts/util/MarkupHelper;->LogWarning(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static makeElectronic(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Electronic;
    .locals 11
    .param p0, "args"    # Landroid/os/PersistableBundle;

    .prologue
    .line 433
    const/4 v9, 0x0

    .line 434
    .local v9, "valid":Z
    invoke-static {}, Lcom/google/speech/tts/Electronic;->newBuilder()Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v1

    .line 435
    .local v1, "electronic":Lcom/google/speech/tts/Electronic$Builder;
    const-string v10, "android.arg.protocol"

    invoke-static {p0, v10}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 436
    .local v6, "protocol":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 437
    const/4 v9, 0x1

    .line 438
    invoke-virtual {v1, v6}, Lcom/google/speech/tts/Electronic$Builder;->setProtocol(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 440
    :cond_0
    const-string v10, "android.arg.username"

    invoke-static {p0, v10}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 441
    .local v8, "username":Ljava/lang/String;
    if-eqz v8, :cond_1

    .line 442
    const/4 v9, 0x1

    .line 443
    invoke-virtual {v1, v8}, Lcom/google/speech/tts/Electronic$Builder;->setUsername(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 445
    :cond_1
    const-string v10, "android.arg.password"

    invoke-static {p0, v10}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 446
    .local v3, "password":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 447
    invoke-virtual {v1, v3}, Lcom/google/speech/tts/Electronic$Builder;->setPassword(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 449
    :cond_2
    const-string v10, "android.arg.domain"

    invoke-static {p0, v10}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 450
    .local v0, "domain":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 451
    const/4 v9, 0x1

    .line 452
    invoke-virtual {v1, v0}, Lcom/google/speech/tts/Electronic$Builder;->setDomain(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 454
    :cond_3
    const-string v10, "android.arg.port"

    invoke-static {p0, v10}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsInt(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    .line 455
    .local v5, "port":Ljava/lang/Integer;
    if-eqz v5, :cond_4

    .line 456
    const/4 v9, 0x1

    .line 457
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v1, v10}, Lcom/google/speech/tts/Electronic$Builder;->setPort(I)Lcom/google/speech/tts/Electronic$Builder;

    .line 459
    :cond_4
    const-string v10, "android.arg.path"

    invoke-static {p0, v10}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 460
    .local v4, "path":Ljava/lang/String;
    if-eqz v4, :cond_5

    .line 461
    const/4 v9, 0x1

    .line 462
    invoke-virtual {v1, v4}, Lcom/google/speech/tts/Electronic$Builder;->setPath(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 464
    :cond_5
    const-string v10, "android.arg.query_string"

    invoke-static {p0, v10}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 465
    .local v7, "queryString":Ljava/lang/String;
    if-eqz v7, :cond_6

    .line 466
    const/4 v9, 0x1

    .line 467
    invoke-virtual {v1, v7}, Lcom/google/speech/tts/Electronic$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 469
    :cond_6
    const-string v10, "android.arg.fragment_id"

    invoke-static {p0, v10}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 470
    .local v2, "fragmentId":Ljava/lang/String;
    if-eqz v2, :cond_7

    .line 471
    const/4 v9, 0x1

    .line 472
    invoke-virtual {v1, v2}, Lcom/google/speech/tts/Electronic$Builder;->setFragmentId(Ljava/lang/String;)Lcom/google/speech/tts/Electronic$Builder;

    .line 474
    :cond_7
    if-eqz v9, :cond_8

    .line 475
    invoke-virtual {v1}, Lcom/google/speech/tts/Electronic$Builder;->build()Lcom/google/speech/tts/Electronic;

    move-result-object v10

    .line 479
    :goto_0
    return-object v10

    :cond_8
    const/4 v10, 0x0

    goto :goto_0
.end method

.method private static makeFraction(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Fraction;
    .locals 7
    .param p0, "args"    # Landroid/os/PersistableBundle;

    .prologue
    const/4 v4, 0x0

    .line 258
    invoke-static {}, Lcom/google/speech/tts/Fraction;->newBuilder()Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v1

    .line 259
    .local v1, "fraction":Lcom/google/speech/tts/Fraction$Builder;
    const-string v5, "android.arg.integer_part"

    invoke-static {p0, v5}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 260
    .local v2, "integerPart":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 261
    invoke-virtual {v1, v2}, Lcom/google/speech/tts/Fraction$Builder;->setIntegerPart(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;

    .line 263
    :cond_0
    const-string v5, "android.arg.numerator"

    invoke-static {p0, v5}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 264
    .local v3, "numerator":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 265
    invoke-virtual {v1, v3}, Lcom/google/speech/tts/Fraction$Builder;->setNumerator(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;

    .line 271
    const-string v5, "android.arg.denominator"

    invoke-static {p0, v5}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 272
    .local v0, "denominator":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 273
    invoke-virtual {v1, v0}, Lcom/google/speech/tts/Fraction$Builder;->setDenominator(Ljava/lang/String;)Lcom/google/speech/tts/Fraction$Builder;

    .line 279
    invoke-virtual {v1}, Lcom/google/speech/tts/Fraction$Builder;->build()Lcom/google/speech/tts/Fraction;

    move-result-object v4

    .end local v0    # "denominator":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 268
    :cond_1
    const-string v5, "android.type.fraction"

    const-string v6, "android.arg.numerator"

    invoke-static {v5, v6}, Lcom/google/android/tts/util/MarkupHelper;->LogWarning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 276
    .restart local v0    # "denominator":Ljava/lang/String;
    :cond_2
    const-string v5, "android.type.fraction"

    const-string v6, "android.arg.denominator"

    invoke-static {v5, v6}, Lcom/google/android/tts/util/MarkupHelper;->LogWarning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static makeMeasure(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Measure;
    .locals 8
    .param p0, "args"    # Landroid/os/PersistableBundle;

    .prologue
    const/4 v5, 0x0

    .line 318
    invoke-static {}, Lcom/google/speech/tts/Measure;->newBuilder()Lcom/google/speech/tts/Measure$Builder;

    move-result-object v3

    .line 319
    .local v3, "measure":Lcom/google/speech/tts/Measure$Builder;
    const-string v6, "android.arg.denominator"

    invoke-virtual {p0, v6}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 320
    invoke-static {p0}, Lcom/google/android/tts/util/MarkupHelper;->makeFraction(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Fraction;

    move-result-object v2

    .line 321
    .local v2, "fraction":Lcom/google/speech/tts/Fraction;
    if-eqz v2, :cond_0

    .line 322
    invoke-virtual {v3, v2}, Lcom/google/speech/tts/Measure$Builder;->setFraction(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Measure$Builder;

    .line 325
    .end local v2    # "fraction":Lcom/google/speech/tts/Fraction;
    :cond_0
    const-string v6, "android.arg.fractional_part"

    invoke-virtual {p0, v6}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 326
    invoke-static {p0}, Lcom/google/android/tts/util/MarkupHelper;->makeDecimal(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Decimal;

    move-result-object v1

    .line 327
    .local v1, "decimal":Lcom/google/speech/tts/Decimal;
    if-eqz v1, :cond_1

    .line 328
    invoke-virtual {v3, v1}, Lcom/google/speech/tts/Measure$Builder;->setDecimal(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Measure$Builder;

    .line 331
    .end local v1    # "decimal":Lcom/google/speech/tts/Decimal;
    :cond_1
    const-string v6, "android.arg.number"

    invoke-virtual {p0, v6}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 333
    invoke-static {p0, v5}, Lcom/google/android/tts/util/MarkupHelper;->makeCardinal(Landroid/os/PersistableBundle;Ljava/lang/String;)Lcom/google/speech/tts/Cardinal;

    move-result-object v0

    .line 334
    .local v0, "cardinal":Lcom/google/speech/tts/Cardinal;
    if-eqz v0, :cond_2

    .line 335
    invoke-virtual {v3, v0}, Lcom/google/speech/tts/Measure$Builder;->setCardinal(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Measure$Builder;

    .line 338
    .end local v0    # "cardinal":Lcom/google/speech/tts/Cardinal;
    :cond_2
    const-string v6, "android.arg.unit"

    invoke-static {p0, v6}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 339
    .local v4, "unit":Ljava/lang/String;
    if-eqz v4, :cond_3

    .line 340
    invoke-virtual {v3, v4}, Lcom/google/speech/tts/Measure$Builder;->setUnits(Ljava/lang/String;)Lcom/google/speech/tts/Measure$Builder;

    .line 347
    invoke-virtual {v3}, Lcom/google/speech/tts/Measure$Builder;->build()Lcom/google/speech/tts/Measure;

    move-result-object v5

    :goto_0
    return-object v5

    .line 344
    :cond_3
    const-string v6, "android.type.measure"

    const-string v7, "android.arg.unit"

    invoke-static {v6, v7}, Lcom/google/android/tts/util/MarkupHelper;->LogWarning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static makeOrdinal(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Ordinal;
    .locals 4
    .param p0, "args"    # Landroid/os/PersistableBundle;

    .prologue
    .line 302
    invoke-static {}, Lcom/google/speech/tts/Ordinal;->newBuilder()Lcom/google/speech/tts/Ordinal$Builder;

    move-result-object v1

    .line 303
    .local v1, "ordinal":Lcom/google/speech/tts/Ordinal$Builder;
    const-string v2, "android.arg.number"

    invoke-static {p0, v2}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 304
    .local v0, "number":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 305
    invoke-virtual {v1, v0}, Lcom/google/speech/tts/Ordinal$Builder;->setInteger(Ljava/lang/String;)Lcom/google/speech/tts/Ordinal$Builder;

    .line 314
    invoke-virtual {v1}, Lcom/google/speech/tts/Ordinal$Builder;->build()Lcom/google/speech/tts/Ordinal;

    move-result-object v2

    :goto_0
    return-object v2

    .line 311
    :cond_0
    const-string v2, "android.type.ordinal"

    const-string v3, "android.arg.number"

    invoke-static {v2, v3}, Lcom/google/android/tts/util/MarkupHelper;->LogWarning(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static makeTelephone(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Telephone;
    .locals 10
    .param p0, "args"    # Landroid/os/PersistableBundle;

    .prologue
    .line 409
    invoke-static {}, Lcom/google/speech/tts/Telephone;->newBuilder()Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v7

    .line 410
    .local v7, "telephone":Lcom/google/speech/tts/Telephone$Builder;
    const-string v8, "android.arg.country_code"

    invoke-static {p0, v8}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 411
    .local v1, "countryCode":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 412
    invoke-virtual {v7, v1}, Lcom/google/speech/tts/Telephone$Builder;->setCountryCode(Ljava/lang/String;)Lcom/google/speech/tts/Telephone$Builder;

    .line 414
    :cond_0
    const-string v8, "android.arg.number_parts"

    invoke-static {p0, v8}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 415
    .local v5, "number":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 416
    const-string v8, "[- \\\\/\\.]"

    invoke-virtual {v5, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v6, v0, v3

    .line 417
    .local v6, "numberPart":Ljava/lang/String;
    invoke-virtual {v7, v6}, Lcom/google/speech/tts/Telephone$Builder;->addNumberPart(Ljava/lang/String;)Lcom/google/speech/tts/Telephone$Builder;

    .line 416
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 422
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v6    # "numberPart":Ljava/lang/String;
    :cond_1
    const-string v8, "android.type.telephone"

    const-string v9, "android.arg.number_parts"

    invoke-static {v8, v9}, Lcom/google/android/tts/util/MarkupHelper;->LogWarning(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const/4 v8, 0x0

    .line 429
    :goto_1
    return-object v8

    .line 425
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    :cond_2
    const-string v8, "android.arg.extension"

    invoke-static {p0, v8}, Lcom/google/android/tts/util/MarkupHelper;->getAsString(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 426
    .local v2, "extension":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 427
    invoke-virtual {v7, v2}, Lcom/google/speech/tts/Telephone$Builder;->setExtension(Ljava/lang/String;)Lcom/google/speech/tts/Telephone$Builder;

    .line 429
    :cond_3
    invoke-virtual {v7}, Lcom/google/speech/tts/Telephone$Builder;->build()Lcom/google/speech/tts/Telephone;

    move-result-object v8

    goto :goto_1
.end method

.method private static makeTime(Landroid/os/PersistableBundle;)Lcom/google/speech/tts/Time;
    .locals 6
    .param p0, "args"    # Landroid/os/PersistableBundle;

    .prologue
    const/4 v3, 0x0

    .line 351
    invoke-static {}, Lcom/google/speech/tts/Time;->newBuilder()Lcom/google/speech/tts/Time$Builder;

    move-result-object v2

    .line 352
    .local v2, "time":Lcom/google/speech/tts/Time$Builder;
    const-string v4, "android.arg.hours"

    invoke-static {p0, v4}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsInt(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 353
    .local v0, "hours":Ljava/lang/Integer;
    if-eqz v0, :cond_2

    .line 354
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ltz v4, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x19

    if-ge v4, v5, :cond_1

    .line 355
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/speech/tts/Time$Builder;->setHours(I)Lcom/google/speech/tts/Time$Builder;

    .line 365
    const-string v4, "android.arg.minutes"

    invoke-static {p0, v4}, Lcom/google/android/tts/util/MarkupHelper;->getNumberAsInt(Landroid/os/PersistableBundle;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 366
    .local v1, "minutes":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 367
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ltz v4, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x3c

    if-ge v4, v5, :cond_1

    .line 368
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/speech/tts/Time$Builder;->setMinutes(I)Lcom/google/speech/tts/Time$Builder;

    .line 373
    :cond_0
    invoke-virtual {v2}, Lcom/google/speech/tts/Time$Builder;->build()Lcom/google/speech/tts/Time;

    move-result-object v3

    .end local v1    # "minutes":Ljava/lang/Integer;
    :cond_1
    :goto_0
    return-object v3

    .line 362
    :cond_2
    const-string v4, "android.type.time"

    const-string v5, "android.arg.hours"

    invoke-static {v4, v5}, Lcom/google/android/tts/util/MarkupHelper;->LogWarning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static russianMorphosyntacticString(Landroid/os/PersistableBundle;)Ljava/lang/String;
    .locals 3
    .param p0, "args"    # Landroid/os/PersistableBundle;

    .prologue
    .line 625
    const-string v1, "android.arg.case"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "android.arg.multiplicity"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "android.arg.gender"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 628
    const/4 v0, 0x0

    .line 666
    :cond_0
    :goto_0
    return-object v0

    .line 631
    :cond_1
    const-string v0, "__"

    .line 633
    .local v0, "features":Ljava/lang/String;
    const-string v1, "android.arg.multiplicity"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "android.arg.multiplicity"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "android.plural"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 635
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "PLU"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 648
    :cond_2
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 650
    const-string v1, "android.arg.case"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 651
    const-string v1, "android.arg.case"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "android.nominative"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 652
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "NOM"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 636
    :cond_3
    const-string v1, "android.arg.gender"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 637
    const-string v1, "android.arg.gender"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 638
    const-string v1, "android.arg.gender"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "android.male"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 639
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MAS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 640
    :cond_4
    const-string v1, "android.arg.gender"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "android.female"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 641
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "FEM"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 642
    :cond_5
    const-string v1, "android.arg.gender"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "android.neutral"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 643
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "NEU"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 653
    :cond_6
    const-string v1, "android.arg.case"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "android.genitive"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 654
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "GEN"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 655
    :cond_7
    const-string v1, "android.arg.case"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "android.dative"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 656
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "DAT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 657
    :cond_8
    const-string v1, "android.arg.case"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "android.accusative"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 658
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ACC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 659
    :cond_9
    const-string v1, "android.arg.case"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "android.instrumental"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 660
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "INS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 661
    :cond_a
    const-string v1, "android.arg.case"

    invoke-virtual {p0, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "android.locative"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 663
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "PRE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
