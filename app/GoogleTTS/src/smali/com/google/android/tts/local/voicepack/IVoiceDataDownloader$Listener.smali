.class public interface abstract Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;
.super Ljava/lang/Object;
.source "IVoiceDataDownloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onVoiceDownloadFail(Ljava/lang/String;)V
.end method

.method public abstract onVoiceDownloadStart(Ljava/lang/String;)V
.end method

.method public abstract onVoiceDownloadSuccess(Ljava/lang/String;)V
.end method
