.class public Lcom/google/android/tts/local/voicepack/IntegrityChecker;
.super Ljava/lang/Object;
.source "IntegrityChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/local/voicepack/IntegrityChecker$IntegrityChecksCache;,
        Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mChecksumDB:Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;

.field private mContext:Landroid/content/Context;

.field private mIntegrityChecksCache:Lcom/google/android/tts/local/voicepack/IntegrityChecker$IntegrityChecksCache;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 209
    const-class v0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 218
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "integrity_cache"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/tts/local/voicepack/IntegrityChecker;-><init>(Landroid/content/Context;Ljava/io/File;Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;)V

    .line 219
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/io/File;Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "integrityCache"    # Ljava/io/File;
    .param p3, "checksumDb"    # Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;

    .prologue
    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->mContext:Landroid/content/Context;

    .line 225
    new-instance v0, Lcom/google/android/tts/local/voicepack/IntegrityChecker$IntegrityChecksCache;

    invoke-direct {v0, p2}, Lcom/google/android/tts/local/voicepack/IntegrityChecker$IntegrityChecksCache;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->mIntegrityChecksCache:Lcom/google/android/tts/local/voicepack/IntegrityChecker$IntegrityChecksCache;

    .line 226
    iput-object p3, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->mChecksumDB:Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;

    .line 227
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private ensureIntegrityDB()V
    .locals 5

    .prologue
    .line 231
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->mChecksumDB:Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;

    if-nez v1, :cond_0

    .line 233
    :try_start_0
    new-instance v1, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->mContext:Landroid/content/Context;

    const-string v3, "checksums.csv"

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v1, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->mChecksumDB:Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 234
    :catch_0
    move-exception v0

    .line 235
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->TAG:Ljava/lang/String;

    const-string v2, "Failed to load voice data checksums"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static fromHexString(Ljava/lang/String;)[B
    .locals 7
    .param p0, "encoded"    # Ljava/lang/String;

    .prologue
    .line 295
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    rem-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_0

    .line 296
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Input string must contain an even number of characters"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 299
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    new-array v3, v4, [B

    .line 300
    .local v3, "result":[B
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 301
    .local v1, "enc":[C
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    if-ge v2, v4, :cond_1

    .line 302
    new-instance v0, Ljava/lang/StringBuilder;

    const/4 v4, 0x2

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 303
    .local v0, "curr":Ljava/lang/StringBuilder;
    aget-char v4, v1, v2

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v2, 0x1

    aget-char v5, v1, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 304
    div-int/lit8 v4, v2, 0x2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 301
    add-int/lit8 v2, v2, 0x2

    goto :goto_0

    .line 306
    .end local v0    # "curr":Ljava/lang/StringBuilder;
    :cond_1
    return-object v3
.end method


# virtual methods
.method public isConsistent(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Ljava/io/File;)Z
    .locals 13
    .param p1, "metadata"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .param p2, "voiceDataDir"    # Ljava/io/File;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 242
    iget-object v10, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->mIntegrityChecksCache:Lcom/google/android/tts/local/voicepack/IntegrityChecker$IntegrityChecksCache;

    invoke-virtual {v10, p2}, Lcom/google/android/tts/local/voicepack/IntegrityChecker$IntegrityChecksCache;->isConsistent(Ljava/io/File;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 290
    :cond_0
    :goto_0
    return v8

    .line 246
    :cond_1
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->ensureIntegrityDB()V

    .line 247
    iget-object v10, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->mChecksumDB:Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;

    if-eqz v10, :cond_0

    .line 251
    sget-object v10, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Checking integrity of voice: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", revision: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    iget-object v10, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->mChecksumDB:Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v12

    invoke-virtual {v10, v11, v12}, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;->getFiles(Ljava/lang/String;I)Ljava/util/Map;

    move-result-object v5

    .line 256
    .local v5, "expectedFiles":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    if-nez v5, :cond_2

    .line 257
    sget-object v9, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "No checksums for voice: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", revision: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 262
    :cond_2
    invoke-virtual {p2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v7, :cond_7

    aget-object v1, v0, v6

    .line 263
    .local v1, "child":Ljava/io/File;
    const-string v10, "."

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, ".."

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 262
    :cond_3
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 266
    :cond_4
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    .line 267
    .local v4, "expectedChecksum":[B
    if-nez v4, :cond_5

    .line 268
    sget-object v8, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Directory "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " contains unexpected file: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 269
    goto/16 :goto_0

    .line 272
    :cond_5
    :try_start_0
    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v4, v10}, Lcom/google/android/tts/util/HashHelper;->verifyMD5Checksum([BLjava/io/InputStream;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 273
    sget-object v10, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Directory "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " file "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ": checksum mismatch"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v8, v9

    .line 274
    goto/16 :goto_0

    .line 276
    :catch_0
    move-exception v2

    .line 277
    .local v2, "e":Ljava/io/IOException;
    sget-object v10, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception while checking checksums of "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 279
    .end local v2    # "e":Ljava/io/IOException;
    :cond_6
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 281
    .end local v1    # "child":Ljava/io/File;
    .end local v4    # "expectedChecksum":[B
    :cond_7
    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_9

    .line 282
    sget-object v8, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Directory "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " is missing files"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 284
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[B>;"
    sget-object v10, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Missing file: "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v10, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[B>;"
    :cond_8
    move v8, v9

    .line 286
    goto/16 :goto_0

    .line 289
    .local v6, "i$":I
    :cond_9
    iget-object v9, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->mIntegrityChecksCache:Lcom/google/android/tts/local/voicepack/IntegrityChecker$IntegrityChecksCache;

    invoke-virtual {v9, p2}, Lcom/google/android/tts/local/voicepack/IntegrityChecker$IntegrityChecksCache;->markConsistent(Ljava/io/File;)V

    goto/16 :goto_0
.end method
