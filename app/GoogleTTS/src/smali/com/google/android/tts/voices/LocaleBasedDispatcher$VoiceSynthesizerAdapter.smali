.class Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;
.super Ljava/lang/Object;
.source "LocaleBasedDispatcher.java"

# interfaces
.implements Lcom/google/android/tts/voices/VoiceSynthesizer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/voices/LocaleBasedDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "VoiceSynthesizerAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/tts/voices/LocaleBasedDispatcher;


# direct methods
.method constructor <init>(Lcom/google/android/tts/voices/LocaleBasedDispatcher;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;->this$0:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLanguage()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;->this$0:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v0}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->getLanguage()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPreferedLocaleFor(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;
    .locals 1
    .param p1, "iso3Language"    # Ljava/lang/String;
    .param p2, "iso3Country"    # Ljava/lang/String;

    .prologue
    .line 292
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getSupportedLocales()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getVoices(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voices/InternalVoice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "voicesList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/voices/InternalVoice;>;"
    const/16 v3, 0x12c

    .line 253
    iget-object v1, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;->this$0:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    # getter for: Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLocalSynthesizer:Lcom/google/android/tts/voices/Synthesizer;
    invoke-static {v1}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->access$000(Lcom/google/android/tts/voices/LocaleBasedDispatcher;)Lcom/google/android/tts/voices/Synthesizer;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/tts/voices/Synthesizer;->getSupportedLocales()Ljava/util/Set;

    move-result-object v10

    .line 254
    .local v10, "localeLocales":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Locale;>;"
    iget-object v1, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;->this$0:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    # getter for: Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkSynthesizer:Lcom/google/android/tts/voices/Synthesizer;
    invoke-static {v1}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->access$100(Lcom/google/android/tts/voices/LocaleBasedDispatcher;)Lcom/google/android/tts/voices/Synthesizer;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/tts/voices/Synthesizer;->getSupportedLocales()Ljava/util/Set;

    move-result-object v11

    .line 256
    .local v11, "networkLocales":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Locale;>;"
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 257
    .local v8, "all":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/util/Locale;>;"
    invoke-virtual {v8, v10}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 258
    invoke-virtual {v8, v11}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 260
    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;

    .line 261
    .local v2, "locale":Ljava/util/Locale;
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 262
    .local v6, "features":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-interface {v10, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    const-string v1, "embeddedTts"

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 265
    :cond_0
    const/4 v5, 0x0

    .line 266
    .local v5, "requiresNetwork":Z
    invoke-interface {v11, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 267
    const-string v1, "networkTts"

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 268
    const/4 v5, 0x1

    .line 271
    :cond_1
    new-instance v0, Lcom/google/android/tts/voices/InternalVoice;

    iget-object v1, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;->this$0:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    # invokes: Lcom/google/android/tts/voices/LocaleBasedDispatcher;->getVoiceNameForLocale(Ljava/util/Locale;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->access$200(Lcom/google/android/tts/voices/LocaleBasedDispatcher;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    move v4, v3

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/tts/voices/InternalVoice;-><init>(Ljava/lang/String;Ljava/util/Locale;IIZLjava/util/Set;Lcom/google/android/tts/voices/VoiceSynthesizer;)V

    .line 274
    .local v0, "voice":Lcom/google/android/tts/voices/InternalVoice;
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 276
    .end local v0    # "voice":Lcom/google/android/tts/voices/InternalVoice;
    .end local v2    # "locale":Ljava/util/Locale;
    .end local v5    # "requiresNetwork":Z
    .end local v6    # "features":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_2
    return-void
.end method

.method public isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;->this$0:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;->this$0:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public onLoadVoice(Lcom/google/android/tts/voices/InternalVoice;)Z
    .locals 4
    .param p1, "voice"    # Lcom/google/android/tts/voices/InternalVoice;

    .prologue
    .line 280
    invoke-virtual {p1}, Lcom/google/android/tts/voices/InternalVoice;->getLocale()Ljava/util/Locale;

    move-result-object v0

    .line 281
    .local v0, "locale":Ljava/util/Locale;
    iget-object v1, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;->this$0:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;->this$0:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v0}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->onStop()V

    .line 234
    return-void
.end method

.method public onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/tts/service/GoogleTTSRequest;
    .param p2, "callback"    # Landroid/speech/tts/SynthesisCallback;

    .prologue
    .line 227
    invoke-static {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->cloneWithoutVoiceName(Lcom/google/android/tts/service/GoogleTTSRequest;)Lcom/google/android/tts/service/GoogleTTSRequest;

    move-result-object v0

    .line 228
    .local v0, "voicelessRequest":Lcom/google/android/tts/service/GoogleTTSRequest;
    iget-object v1, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;->this$0:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V

    .line 229
    return-void
.end method
