.class public Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;
.super Ljava/lang/Object;
.source "ProcessVoicePackTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;,
        Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

.field private final mCleanup:Ljava/lang/Runnable;

.field private final mInput:Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;

.field private final mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/tts/service/GoogleTTSApplication;Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "app"    # Lcom/google/android/tts/service/GoogleTTSApplication;
    .param p2, "input"    # Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;
    .param p3, "cleanup"    # Ljava/lang/Runnable;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    .line 63
    iput-object p2, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mInput:Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;

    .line 64
    iput-object p3, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mCleanup:Ljava/lang/Runnable;

    .line 65
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataDownloader()Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;

    .line 66
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;)Lcom/google/android/tts/service/GoogleTTSApplication;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    return-object v0
.end method

.method private deleteDownloadedFile(Ljava/lang/String;)V
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 268
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 269
    .local v0, "downloaded":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 270
    sget-object v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to delete downloaded file:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_0
    return-void
.end method

.method private handleCompletedDownload(Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;)Z
    .locals 10
    .param p1, "input"    # Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;
    .param p2, "unpackState"    # Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;

    .prologue
    const/4 v5, 0x0

    .line 141
    iget-wide v6, p1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;->downloadId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-gez v6, :cond_1

    .line 142
    sget-object v6, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Received intent without download ID :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;->downloadId:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_0
    :goto_0
    return v5

    .line 146
    :cond_1
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;

    iget-wide v8, p1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;->downloadId:J

    invoke-virtual {v6, v8, v9}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->getDownloadInfo(J)Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    move-result-object v6

    iput-object v6, p2, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    .line 147
    iget-object v6, p2, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    if-eqz v6, :cond_0

    .line 152
    iget-object v1, p2, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    .line 153
    .local v1, "downloadInfo":Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;
    iget v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mStatus:I

    const/16 v7, 0x8

    if-eq v6, v7, :cond_2

    .line 154
    sget-object v6, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Download of voice pack from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p2, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    iget-object v8, v8, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mUri:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " was unsuccessful"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 162
    :cond_2
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v6

    iget-object v7, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getVoiceDataDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 165
    .local v0, "dataDir":Ljava/io/File;
    :try_start_0
    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->getExpectedMD5Checksum(Ljava/lang/String;)[B

    move-result-object v4

    .line 166
    .local v4, "md5Checksum":[B
    if-eqz v4, :cond_4

    new-instance v6, Ljava/io/FileInputStream;

    iget-object v7, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v6}, Lcom/google/android/tts/util/HashHelper;->verifyMD5Checksum([BLjava/io/InputStream;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 169
    sget-object v6, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Bad checksum, ignoring "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 197
    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->deleteDownloadedFile(Ljava/lang/String;)V

    .line 199
    :cond_3
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v6

    iget-object v7, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->voicePackInstallEnd(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 173
    :cond_4
    :try_start_1
    iget v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mStatus:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    and-int/lit8 v6, v6, 0x8

    if-nez v6, :cond_6

    .line 196
    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 197
    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->deleteDownloadedFile(Ljava/lang/String;)V

    .line 199
    :cond_5
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v6

    iget-object v7, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->voicePackInstallEnd(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 177
    :cond_6
    :try_start_2
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v6

    iget-object v7, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->voicePackInstallBegin(Ljava/lang/String;)V

    .line 178
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v0, v7}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->deleteVoiceDataFiles(Ljava/io/File;Z)Z

    .line 179
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 180
    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v6, v0}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->processDownloadedFile(Ljava/lang/String;Ljava/io/File;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v5

    .line 196
    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 197
    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->deleteDownloadedFile(Ljava/lang/String;)V

    .line 199
    :cond_7
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v6

    iget-object v7, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->voicePackInstallEnd(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 181
    .end local v4    # "md5Checksum":[B
    :catch_0
    move-exception v2

    .line 182
    .local v2, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v6, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->TAG:Ljava/lang/String;

    const-string v7, "Failed to process downloaded voice pack"

    invoke-static {v6, v7, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 183
    new-instance v3, Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 184
    .local v3, "handler":Landroid/os/Handler;
    new-instance v6, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$1;

    invoke-direct {v6, p0}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$1;-><init>(Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;)V

    invoke-virtual {v3, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 193
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v0, v7}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->deleteVoiceDataFiles(Ljava/io/File;Z)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 196
    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 197
    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->deleteDownloadedFile(Ljava/lang/String;)V

    .line 199
    :cond_8
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v6

    iget-object v7, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->voicePackInstallEnd(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 196
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "handler":Landroid/os/Handler;
    :catchall_0
    move-exception v5

    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 197
    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->deleteDownloadedFile(Ljava/lang/String;)V

    .line 199
    :cond_9
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v6

    iget-object v7, v1, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->voicePackInstallEnd(Ljava/lang/String;)V

    throw v5
.end method

.method private processDownloadedFile(Ljava/lang/String;Ljava/io/File;)Z
    .locals 17
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "dataDir"    # Ljava/io/File;

    .prologue
    .line 217
    :try_start_0
    new-instance v6, Ljava/util/zip/ZipFile;

    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v15, 0x1

    invoke-direct {v6, v14, v15}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;I)V

    .line 219
    .local v6, "file":Ljava/util/zip/ZipFile;
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v3

    .line 220
    .local v3, "entries":Ljava/util/Enumeration;, "Ljava/util/Enumeration<+Ljava/util/zip/ZipEntry;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 221
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/zip/ZipEntry;

    .line 223
    .local v4, "entry":Ljava/util/zip/ZipEntry;
    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v5

    .line 224
    .local v5, "entryFileName":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v11, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 225
    .local v11, "outputFile":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    .line 226
    .local v12, "outputFileName":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 227
    invoke-virtual {v11}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 261
    .end local v3    # "entries":Ljava/util/Enumeration;, "Ljava/util/Enumeration<+Ljava/util/zip/ZipEntry;>;"
    .end local v4    # "entry":Ljava/util/zip/ZipEntry;
    .end local v5    # "entryFileName":Ljava/lang/String;
    .end local v6    # "file":Ljava/util/zip/ZipFile;
    .end local v11    # "outputFile":Ljava/io/File;
    .end local v12    # "outputFileName":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 262
    .local v7, "ioe":Ljava/io/IOException;
    sget-object v14, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Exception processing downloaded file :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const/4 v14, 0x0

    .end local v7    # "ioe":Ljava/io/IOException;
    :goto_1
    return v14

    .line 237
    .restart local v3    # "entries":Ljava/util/Enumeration;, "Ljava/util/Enumeration<+Ljava/util/zip/ZipEntry;>;"
    .restart local v4    # "entry":Ljava/util/zip/ZipEntry;
    .restart local v5    # "entryFileName":Ljava/lang/String;
    .restart local v6    # "file":Ljava/util/zip/ZipFile;
    .restart local v11    # "outputFile":Ljava/io/File;
    .restart local v12    # "outputFileName":Ljava/lang/String;
    :cond_0
    const/4 v8, 0x0

    .line 238
    .local v8, "is":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 240
    .local v9, "os":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v6, v4}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v8

    .line 241
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 242
    .end local v9    # "os":Ljava/io/FileOutputStream;
    .local v10, "os":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 243
    .local v2, "count":I
    const/4 v13, 0x0

    .line 244
    .local v13, "total":I
    const/high16 v14, 0x10000

    :try_start_2
    new-array v1, v14, [B

    .line 245
    .local v1, "buffer":[B
    :goto_2
    invoke-virtual {v8, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-lez v2, :cond_1

    .line 246
    const/4 v14, 0x0

    invoke-virtual {v10, v1, v14, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 247
    add-int/2addr v13, v2

    goto :goto_2

    .line 251
    :cond_1
    if-eqz v8, :cond_2

    :try_start_3
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    .line 252
    :cond_2
    if-eqz v10, :cond_3

    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 255
    :cond_3
    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_0

    .line 251
    .end local v1    # "buffer":[B
    .end local v2    # "count":I
    .end local v10    # "os":Ljava/io/FileOutputStream;
    .end local v13    # "total":I
    .restart local v9    # "os":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v14

    :goto_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    .line 252
    :cond_4
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    :cond_5
    throw v14

    .line 258
    .end local v4    # "entry":Ljava/util/zip/ZipEntry;
    .end local v5    # "entryFileName":Ljava/lang/String;
    .end local v8    # "is":Ljava/io/InputStream;
    .end local v9    # "os":Ljava/io/FileOutputStream;
    .end local v11    # "outputFile":Ljava/io/File;
    .end local v12    # "outputFileName":Ljava/lang/String;
    :cond_6
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 260
    const/4 v14, 0x1

    goto :goto_1

    .line 251
    .restart local v2    # "count":I
    .restart local v4    # "entry":Ljava/util/zip/ZipEntry;
    .restart local v5    # "entryFileName":Ljava/lang/String;
    .restart local v8    # "is":Ljava/io/InputStream;
    .restart local v10    # "os":Ljava/io/FileOutputStream;
    .restart local v11    # "outputFile":Ljava/io/File;
    .restart local v12    # "outputFileName":Ljava/lang/String;
    .restart local v13    # "total":I
    :catchall_1
    move-exception v14

    move-object v9, v10

    .end local v10    # "os":Ljava/io/FileOutputStream;
    .restart local v9    # "os":Ljava/io/FileOutputStream;
    goto :goto_3
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 44
    return-void
.end method

.method getExpectedMD5Checksum(Ljava/lang/String;)[B
    .locals 2
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 205
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v1}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceMetadataListManager()Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->getVoiceMetadataByName(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v0

    .line 207
    .local v0, "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasMd5Checksum()Z

    move-result v1

    if-nez v1, :cond_1

    .line 208
    :cond_0
    const/4 v1, 0x0

    .line 210
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getMd5Checksum()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/micro/ByteStringMicro;->toByteArray()[B

    move-result-object v1

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 71
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->unpack()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mCleanup:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mCleanup:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 77
    :cond_0
    return-void

    .line 73
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mCleanup:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    .line 74
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mCleanup:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    :cond_1
    throw v0
.end method

.method public unpack()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 80
    new-instance v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;

    const/4 v4, 0x0

    invoke-direct {v1, v4}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;-><init>(Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$1;)V

    .line 83
    .local v1, "unpackState":Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mInput:Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;

    invoke-direct {p0, v4, v1}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->handleCompletedDownload(Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;)Z

    move-result v4

    iput-boolean v4, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->success:Z

    .line 84
    iget-object v4, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    if-nez v4, :cond_0

    .line 86
    sget-object v3, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->TAG:Ljava/lang/String;

    const-string v4, "No active download info for the voice pack, ignoring"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v4}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceMetadataListManager()Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    iget-object v5, v5, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->getVoiceMetadataByName(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v2

    .line 93
    .local v2, "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    if-nez v2, :cond_1

    .line 94
    sget-object v3, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No voice metadata entry in voice metadata list for the voice named "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    iget-object v5, v5, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 99
    :cond_1
    iget-boolean v4, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->success:Z

    if-nez v4, :cond_2

    iget-object v4, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    iget-object v4, v4, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mUri:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/tts/local/voicepack/lorry/BandaidUtil;->isBandaidURL(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 100
    sget-object v3, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retrying download of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    iget-object v5, v5, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " without bandaid"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;

    iget-object v4, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    iget-wide v4, v4, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mDownloadId:J

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->markCompleted(JZ)V

    .line 106
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;

    iget-object v4, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    iget-boolean v4, v4, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mUserInitiated:Z

    iget-object v5, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    iget-object v5, v5, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mUri:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/tts/local/voicepack/lorry/BandaidUtil;->getFallbackURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v2, v4, v5, v6}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;ZLjava/lang/String;Z)Z

    goto :goto_0

    .line 115
    :cond_2
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;

    iget-object v5, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    iget-wide v6, v5, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mDownloadId:J

    invoke-virtual {v4, v6, v7, v3}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->markCompleted(JZ)V

    .line 117
    iget-boolean v4, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->success:Z

    if-nez v4, :cond_3

    .line 118
    sget-object v3, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Voice unpacking failed for voice named "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    iget-object v5, v5, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 123
    :cond_3
    iget-object v4, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    iget-boolean v4, v4, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mUserInitiated:Z

    if-eqz v4, :cond_4

    .line 125
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v4}, Lcom/google/android/tts/service/GoogleTTSApplication;->getTtsConfig()Lcom/google/android/tts/settings/TtsConfig;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->downloadInfo:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    iget-object v6, v6, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-interface {v4, v5, v6}, Lcom/google/android/tts/settings/TtsConfig;->setDefaultVoiceName(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_4
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v4}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->checkData()V

    .line 133
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.speech.tts.engine.TTS_DATA_INSTALLED"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 135
    .local v0, "intent":Landroid/content/Intent;
    const-string v4, "dataInstalled"

    iget-boolean v5, v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$UnpackingState;->success:Z

    if-eqz v5, :cond_5

    :goto_1
    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 137
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->mApp:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v3, v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 135
    :cond_5
    const/4 v3, -0x1

    goto :goto_1
.end method
