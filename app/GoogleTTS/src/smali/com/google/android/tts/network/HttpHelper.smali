.class Lcom/google/android/tts/network/HttpHelper;
.super Ljava/lang/Object;
.source "HttpHelper.java"


# static fields
.field private static final SPEECH_DOGFOOD_REQUEST_STR:Ljava/lang/String;


# instance fields
.field private final mLangCountryHelper:Lcom/google/android/tts/network/NetworkVoicesManager;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mUrlRewriter:Lcom/google/android/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://www.google.com/speech-api/v1/synthesize?client=%s&lang=%s&rate=16000&speed=%f&pitch=%f&enc=speex&engine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/tts/util/DogfoodUtil;->getDogfoodEngine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/network/HttpHelper;->SPEECH_DOGFOOD_REQUEST_STR:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/common/base/Function;Lcom/google/android/tts/network/NetworkVoicesManager;Landroid/content/pm/PackageManager;)V
    .locals 0
    .param p2, "helper"    # Lcom/google/android/tts/network/NetworkVoicesManager;
    .param p3, "packageManager"    # Landroid/content/pm/PackageManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/tts/network/NetworkVoicesManager;",
            "Landroid/content/pm/PackageManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "urlRewriter":Lcom/google/android/common/base/Function;, "Lcom/google/android/common/base/Function<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lcom/google/android/tts/network/HttpHelper;->mUrlRewriter:Lcom/google/android/common/base/Function;

    .line 91
    iput-object p2, p0, Lcom/google/android/tts/network/HttpHelper;->mLangCountryHelper:Lcom/google/android/tts/network/NetworkVoicesManager;

    .line 92
    iput-object p3, p0, Lcom/google/android/tts/network/HttpHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 93
    return-void
.end method

.method private buildRequestUrl(Lcom/google/android/tts/network/NetworkRequest;)Ljava/net/URL;
    .locals 6
    .param p1, "request"    # Lcom/google/android/tts/network/NetworkRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;
        }
    .end annotation

    .prologue
    .line 223
    new-instance v0, Ljava/util/Formatter;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v3}, Ljava/util/Formatter;-><init>(Ljava/util/Locale;)V

    .line 224
    .local v0, "f":Ljava/util/Formatter;
    iget-boolean v3, p1, Lcom/google/android/tts/network/NetworkRequest;->isDogfood:Z

    if-nez v3, :cond_0

    const-string v1, "https://www.google.com/speech-api/v1/synthesize?client=%s&lang=%s&rate=16000&speed=%f&pitch=%f&enc=speex"

    .line 227
    .local v1, "request_str":Ljava/lang/String;
    :goto_0
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/tts/network/HttpHelper;->getClientString(Lcom/google/android/tts/network/NetworkRequest;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/tts/network/HttpHelper;->getLanguage(Lcom/google/android/tts/network/NetworkRequest;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget v5, p1, Lcom/google/android/tts/network/NetworkRequest;->durationMultiplier:F

    invoke-static {v5}, Lcom/google/android/tts/util/ConvertHelper;->speechDurationToNetworkSpeed(F)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget v5, p1, Lcom/google/android/tts/network/NetworkRequest;->pitchMultiplier:F

    invoke-static {v5}, Lcom/google/android/tts/util/ConvertHelper;->speechPitchToNetworkPitch(F)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 233
    iget-object v3, p0, Lcom/google/android/tts/network/HttpHelper;->mUrlRewriter:Lcom/google/android/common/base/Function;

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 235
    .local v2, "transformedUrl":Ljava/lang/String;
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    return-object v3

    .line 224
    .end local v1    # "request_str":Ljava/lang/String;
    .end local v2    # "transformedUrl":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/google/android/tts/network/HttpHelper;->SPEECH_DOGFOOD_REQUEST_STR:Ljava/lang/String;

    goto :goto_0
.end method

.method private getClientString(Lcom/google/android/tts/network/NetworkRequest;)Ljava/lang/String;
    .locals 13
    .param p1, "request"    # Lcom/google/android/tts/network/NetworkRequest;

    .prologue
    const/16 v12, 0x1f4

    const/4 v11, 0x0

    .line 188
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x13

    if-ge v9, v10, :cond_1

    .line 189
    const-string v7, "android-tts"

    .line 218
    :cond_0
    :goto_0
    return-object v7

    .line 192
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v9, "android-tts"

    invoke-direct {v1, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 194
    .local v1, "clientStringBuilder":Ljava/lang/StringBuilder;
    iget-object v9, p0, Lcom/google/android/tts/network/HttpHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    iget v10, p1, Lcom/google/android/tts/network/NetworkRequest;->callerUid:I

    invoke-virtual {v9, v10}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v6

    .line 195
    .local v6, "packagesNames":[Ljava/lang/String;
    if-eqz v6, :cond_2

    array-length v9, v6

    if-nez v9, :cond_3

    .line 197
    :cond_2
    const-string v9, "/unknown"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 202
    :cond_3
    invoke-static {v6}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 203
    move-object v0, v6

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_4

    aget-object v5, v0, v3

    .line 204
    .local v5, "packageName":Ljava/lang/String;
    const/16 v9, 0x2f

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 205
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    :try_start_0
    iget-object v9, p0, Lcom/google/android/tts/network/HttpHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v10, 0x0

    invoke-virtual {v9, v5, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    iget-object v8, v9, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 209
    .local v8, "versionName":Ljava/lang/String;
    const/16 v9, 0x3a

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 210
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    .end local v8    # "versionName":Ljava/lang/String;
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 211
    :catch_0
    move-exception v2

    .line 212
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v9, "GoogleTTS.HttpHelper"

    const-string v10, "Couldn\'t find client package info"

    invoke-static {v9, v10, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 215
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v5    # "packageName":Ljava/lang/String;
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 218
    .local v7, "result":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-le v9, v12, :cond_0

    invoke-virtual {v7, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method private getLanguage(Lcom/google/android/tts/network/NetworkRequest;)Ljava/lang/String;
    .locals 3
    .param p1, "request"    # Lcom/google/android/tts/network/NetworkRequest;

    .prologue
    .line 254
    iget-object v1, p1, Lcom/google/android/tts/network/NetworkRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/tts/util/LocalesHelper;->sanitizeLanguageCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 256
    .local v0, "language":Ljava/lang/String;
    iget-object v1, p1, Lcom/google/android/tts/network/NetworkRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 259
    .end local v0    # "language":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "language":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/tts/network/NetworkRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static isProtoLike(Ljava/lang/String;)Z
    .locals 1
    .param p0, "text"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 248
    const-string v0, "say {"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static isSsmlLike(Ljava/lang/String;)Z
    .locals 1
    .param p0, "text"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 242
    const-string v0, "</speak>"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private openServerConnection(Lcom/google/android/tts/network/NetworkRequest;)Ljava/net/HttpURLConnection;
    .locals 3
    .param p1, "request"    # Lcom/google/android/tts/network/NetworkRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/MalformedURLException;
        }
    .end annotation

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/google/android/tts/network/HttpHelper;->buildRequestUrl(Lcom/google/android/tts/network/NetworkRequest;)Ljava/net/URL;

    move-result-object v1

    .line 126
    .local v1, "url":Ljava/net/URL;
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 129
    .local v0, "conn":Ljava/net/HttpURLConnection;
    iget v2, p1, Lcom/google/android/tts/network/NetworkRequest;->networkTimeoutMs:I

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 130
    const-string v2, "POST"

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 131
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 132
    invoke-static {v0, p1}, Lcom/google/android/tts/network/HttpHelper;->writePostHeaders(Ljava/net/URLConnection;Lcom/google/android/tts/network/NetworkRequest;)V

    .line 133
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 134
    invoke-direct {p0, v0, p1}, Lcom/google/android/tts/network/HttpHelper;->sendPostBody(Ljava/net/HttpURLConnection;Lcom/google/android/tts/network/NetworkRequest;)V

    .line 135
    return-object v0
.end method

.method private sendPostBody(Ljava/net/HttpURLConnection;Lcom/google/android/tts/network/NetworkRequest;)V
    .locals 3
    .param p1, "conn"    # Ljava/net/HttpURLConnection;
    .param p2, "request"    # Lcom/google/android/tts/network/NetworkRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    iget-object v2, p2, Lcom/google/android/tts/network/NetworkRequest;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/tts/network/HttpHelper;->isProtoLike(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p2, Lcom/google/android/tts/network/NetworkRequest;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/tts/network/HttpHelper;->isSsmlLike(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p2, Lcom/google/android/tts/network/NetworkRequest;->proto:Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    if-nez v2, :cond_1

    .line 167
    :cond_0
    new-instance v1, Ljava/io/OutputStreamWriter;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 168
    .local v1, "osw":Ljava/io/OutputStreamWriter;
    iget-object v2, p2, Lcom/google/android/tts/network/NetworkRequest;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 169
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V

    .line 175
    .end local v1    # "osw":Ljava/io/OutputStreamWriter;
    :goto_0
    return-void

    .line 171
    :cond_1
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 172
    .local v0, "ds":Ljava/io/DataOutputStream;
    iget-object v2, p2, Lcom/google/android/tts/network/NetworkRequest;->proto:Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    invoke-virtual {v2}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->write([B)V

    .line 173
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    goto :goto_0
.end method

.method protected static writePostHeaders(Ljava/net/URLConnection;Lcom/google/android/tts/network/NetworkRequest;)V
    .locals 4
    .param p0, "conn"    # Ljava/net/URLConnection;
    .param p1, "request"    # Lcom/google/android/tts/network/NetworkRequest;

    .prologue
    .line 145
    iget-object v2, p1, Lcom/google/android/tts/network/NetworkRequest;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/tts/network/HttpHelper;->isProtoLike(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p1, Lcom/google/android/tts/network/NetworkRequest;->proto:Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    if-nez v2, :cond_1

    .line 146
    :cond_0
    const-string v1, "text/plain"

    .line 147
    .local v1, "contentType":Ljava/lang/String;
    iget-object v2, p1, Lcom/google/android/tts/network/NetworkRequest;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 155
    .local v0, "contentLength":I
    :goto_0
    const-string v2, "Content-length"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Ljava/net/URLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v2, "Content-type"

    invoke-virtual {p0, v2, v1}, Ljava/net/URLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    return-void

    .line 148
    .end local v0    # "contentLength":I
    .end local v1    # "contentType":Ljava/lang/String;
    :cond_1
    iget-object v2, p1, Lcom/google/android/tts/network/NetworkRequest;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/tts/network/HttpHelper;->isSsmlLike(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 149
    const-string v1, "text/xml"

    .line 150
    .restart local v1    # "contentType":Ljava/lang/String;
    iget-object v2, p1, Lcom/google/android/tts/network/NetworkRequest;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .restart local v0    # "contentLength":I
    goto :goto_0

    .line 152
    .end local v0    # "contentLength":I
    .end local v1    # "contentType":Ljava/lang/String;
    :cond_2
    const-string v1, "application/vnd.google.proto; proto=speech.SynthesisEngineSpecificRequest"

    .line 153
    .restart local v1    # "contentType":Ljava/lang/String;
    iget-object v2, p1, Lcom/google/android/tts/network/NetworkRequest;->proto:Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    invoke-virtual {v2}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->toByteArray()[B

    move-result-object v2

    array-length v0, v2

    .restart local v0    # "contentLength":I
    goto :goto_0
.end method


# virtual methods
.method public getConnection(Lcom/google/android/tts/network/NetworkRequest;)Ljava/net/HttpURLConnection;
    .locals 3
    .param p1, "request"    # Lcom/google/android/tts/network/NetworkRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/MalformedURLException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/google/android/tts/network/HttpHelper;->openServerConnection(Lcom/google/android/tts/network/NetworkRequest;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 108
    .local v0, "conn":Ljava/net/HttpURLConnection;
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    .line 109
    .local v1, "responseCode":I
    const/16 v2, 0xc8

    if-eq v1, v2, :cond_0

    .line 111
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 112
    const/4 v0, 0x0

    .line 115
    .end local v0    # "conn":Ljava/net/HttpURLConnection;
    :cond_0
    return-object v0
.end method
