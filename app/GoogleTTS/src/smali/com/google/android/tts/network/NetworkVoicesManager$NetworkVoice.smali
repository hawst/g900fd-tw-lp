.class public Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
.super Ljava/lang/Object;
.source "NetworkVoicesManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/network/NetworkVoicesManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NetworkVoice"
.end annotation


# instance fields
.field public isDogfood:Z

.field public isGoogleVoice:Z

.field public locale:Ljava/util/Locale;

.field public quality:I


# direct methods
.method public constructor <init>(Ljava/util/Locale;IZZ)V
    .locals 0
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "quality"    # I
    .param p3, "isDogfood"    # Z
    .param p4, "isGoogleVoice"    # Z

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;->locale:Ljava/util/Locale;

    .line 35
    iput p2, p0, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;->quality:I

    .line 36
    iput-boolean p3, p0, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;->isDogfood:Z

    .line 37
    iput-boolean p4, p0, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;->isGoogleVoice:Z

    .line 38
    return-void
.end method
