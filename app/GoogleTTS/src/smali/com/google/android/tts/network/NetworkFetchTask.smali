.class public Lcom/google/android/tts/network/NetworkFetchTask;
.super Ljava/lang/Object;
.source "NetworkFetchTask.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/android/tts/network/ByteArrayHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final mBuffer:[B

.field private final mHttpHelper:Lcom/google/android/tts/network/HttpHelper;

.field private final mRequest:Lcom/google/android/tts/network/NetworkRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/tts/network/NetworkRequest;Lcom/google/android/tts/network/HttpHelper;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/tts/network/NetworkRequest;
    .param p2, "helper"    # Lcom/google/android/tts/network/HttpHelper;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    const/16 v0, 0x200

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/tts/network/NetworkFetchTask;->mBuffer:[B

    .line 31
    iput-object p1, p0, Lcom/google/android/tts/network/NetworkFetchTask;->mRequest:Lcom/google/android/tts/network/NetworkRequest;

    .line 32
    iput-object p2, p0, Lcom/google/android/tts/network/NetworkFetchTask;->mHttpHelper:Lcom/google/android/tts/network/HttpHelper;

    .line 33
    return-void
.end method

.method private checkInterrupted()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 148
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 152
    :cond_0
    return-void
.end method

.method private slurpData(Ljava/net/HttpURLConnection;)[B
    .locals 2
    .param p1, "connection"    # Ljava/net/HttpURLConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v0

    .line 89
    .local v0, "contentLength":I
    if-lez v0, :cond_0

    .line 90
    invoke-direct {p0, p1, v0}, Lcom/google/android/tts/network/NetworkFetchTask;->slurpFixedLengthData(Ljava/net/HttpURLConnection;I)[B

    move-result-object v1

    .line 92
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/tts/network/NetworkFetchTask;->slurpUnknownLengthData(Ljava/net/HttpURLConnection;)[B

    move-result-object v1

    goto :goto_0
.end method

.method private slurpFixedLengthData(Ljava/net/HttpURLConnection;I)[B
    .locals 6
    .param p1, "connection"    # Ljava/net/HttpURLConnection;
    .param p2, "contentLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 98
    new-array v3, p2, [B

    .line 100
    .local v3, "response":[B
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 101
    .local v1, "is":Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 102
    .local v0, "count":I
    const/4 v2, 0x0

    .line 104
    .local v2, "offset":I
    :goto_0
    sub-int v4, p2, v2

    const/16 v5, 0x200

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {v1, v3, v2, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    if-lez v0, :cond_0

    .line 105
    invoke-direct {p0}, Lcom/google/android/tts/network/NetworkFetchTask;->checkInterrupted()V

    .line 106
    add-int/2addr v2, v0

    goto :goto_0

    .line 109
    :cond_0
    return-object v3
.end method

.method private slurpUnknownLengthData(Ljava/net/HttpURLConnection;)[B
    .locals 5
    .param p1, "connection"    # Ljava/net/HttpURLConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 117
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 122
    .local v2, "is":Ljava/io/InputStream;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x2000

    invoke-direct {v0, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 124
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    const/4 v1, 0x0

    .line 125
    .local v1, "count":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/tts/network/NetworkFetchTask;->mBuffer:[B

    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-lez v1, :cond_0

    .line 126
    invoke-direct {p0}, Lcom/google/android/tts/network/NetworkFetchTask;->checkInterrupted()V

    .line 127
    iget-object v3, p0, Lcom/google/android/tts/network/NetworkFetchTask;->mBuffer:[B

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 130
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3
.end method

.method private verifyAudioFormat([Lorg/apache/http/HeaderElement;)Z
    .locals 5
    .param p1, "elements"    # [Lorg/apache/http/HeaderElement;

    .prologue
    const/4 v2, 0x0

    .line 134
    array-length v3, p1

    if-lez v3, :cond_0

    .line 135
    aget-object v0, p1, v2

    .line 136
    .local v0, "elem":Lorg/apache/http/HeaderElement;
    invoke-interface {v0}, Lorg/apache/http/HeaderElement;->getName()Ljava/lang/String;

    move-result-object v1

    .line 137
    .local v1, "format":Ljava/lang/String;
    const-string v3, "audio/speex"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 139
    const/4 v2, 0x1

    .line 143
    .end local v0    # "elem":Lorg/apache/http/HeaderElement;
    .end local v1    # "format":Ljava/lang/String;
    :goto_0
    return v2

    .line 142
    :cond_0
    const-string v3, "NetworkFetchTask"

    const-string v4, "Received unexpected audio format : "

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public call()Lcom/google/android/tts/network/ByteArrayHolder;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 41
    const/4 v0, 0x0

    .line 45
    .local v0, "conn":Ljava/net/HttpURLConnection;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/tts/network/NetworkFetchTask;->mHttpHelper:Lcom/google/android/tts/network/HttpHelper;

    iget-object v6, p0, Lcom/google/android/tts/network/NetworkFetchTask;->mRequest:Lcom/google/android/tts/network/NetworkRequest;

    invoke-virtual {v5, v6}, Lcom/google/android/tts/network/HttpHelper;->getConnection(Lcom/google/android/tts/network/NetworkRequest;)Ljava/net/HttpURLConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 46
    if-nez v0, :cond_1

    .line 74
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    :goto_0
    return-object v4

    .line 55
    :cond_1
    :try_start_1
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-Type"

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .local v2, "header":Lorg/apache/http/message/BasicHeader;
    invoke-virtual {v2}, Lorg/apache/http/message/BasicHeader;->getElements()[Lorg/apache/http/HeaderElement;

    move-result-object v1

    .line 59
    .local v1, "elements":[Lorg/apache/http/HeaderElement;
    invoke-direct {p0, v1}, Lcom/google/android/tts/network/NetworkFetchTask;->verifyAudioFormat([Lorg/apache/http/HeaderElement;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 64
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Invalid response data"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    .end local v1    # "elements":[Lorg/apache/http/HeaderElement;
    .end local v2    # "header":Lorg/apache/http/message/BasicHeader;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_2

    .line 75
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    throw v4

    .line 69
    .restart local v1    # "elements":[Lorg/apache/http/HeaderElement;
    .restart local v2    # "header":Lorg/apache/http/message/BasicHeader;
    :cond_3
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/tts/network/NetworkFetchTask;->checkInterrupted()V

    .line 70
    invoke-direct {p0, v0}, Lcom/google/android/tts/network/NetworkFetchTask;->slurpData(Ljava/net/HttpURLConnection;)[B

    move-result-object v3

    .line 71
    .local v3, "responseBytes":[B
    if-eqz v3, :cond_4

    array-length v5, v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v5, :cond_5

    .line 74
    :cond_4
    :goto_1
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 71
    :cond_5
    :try_start_3
    new-instance v4, Lcom/google/android/tts/network/ByteArrayHolder;

    invoke-direct {v4, v3}, Lcom/google/android/tts/network/ByteArrayHolder;-><init>([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/google/android/tts/network/NetworkFetchTask;->call()Lcom/google/android/tts/network/ByteArrayHolder;

    move-result-object v0

    return-object v0
.end method
