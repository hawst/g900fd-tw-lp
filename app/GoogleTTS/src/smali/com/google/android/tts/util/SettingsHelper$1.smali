.class final Lcom/google/android/tts/util/SettingsHelper$1;
.super Landroid/database/ContentObserver;
.source "SettingsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/tts/util/SettingsHelper;->registerTTSDefaultLocaleListener(Landroid/content/Context;Lcom/google/android/tts/util/SettingsHelper$DefaultTTSLocaleListener;)Landroid/database/ContentObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private currentLocale:Ljava/util/Locale;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$listener:Lcom/google/android/tts/util/SettingsHelper$DefaultTTSLocaleListener;


# direct methods
.method constructor <init>(Landroid/os/Handler;Landroid/content/Context;Lcom/google/android/tts/util/SettingsHelper$DefaultTTSLocaleListener;)V
    .locals 1
    .param p1, "x0"    # Landroid/os/Handler;

    .prologue
    .line 105
    iput-object p2, p0, Lcom/google/android/tts/util/SettingsHelper$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/tts/util/SettingsHelper$1;->val$listener:Lcom/google/android/tts/util/SettingsHelper$DefaultTTSLocaleListener;

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/tts/util/SettingsHelper$1;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/tts/util/SettingsHelper;->getPreferedLocaleFromSettings(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/tts/util/SettingsHelper;->getDeviceDefaultLocaleIfNull(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/util/SettingsHelper$1;->currentLocale:Ljava/util/Locale;

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 111
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 113
    iget-object v1, p0, Lcom/google/android/tts/util/SettingsHelper$1;->val$context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/tts/util/SettingsHelper;->getPreferedLocaleFromSettings(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/tts/util/SettingsHelper;->getDeviceDefaultLocaleIfNull(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    .line 115
    .local v0, "newLocale":Ljava/util/Locale;
    iget-object v1, p0, Lcom/google/android/tts/util/SettingsHelper$1;->currentLocale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    iget-object v1, p0, Lcom/google/android/tts/util/SettingsHelper$1;->val$listener:Lcom/google/android/tts/util/SettingsHelper$DefaultTTSLocaleListener;

    iget-object v2, p0, Lcom/google/android/tts/util/SettingsHelper$1;->currentLocale:Ljava/util/Locale;

    invoke-interface {v1, v2, v0}, Lcom/google/android/tts/util/SettingsHelper$DefaultTTSLocaleListener;->onDefaultTTSLocaleChange(Ljava/util/Locale;Ljava/util/Locale;)V

    .line 118
    :cond_0
    iput-object v0, p0, Lcom/google/android/tts/util/SettingsHelper$1;->currentLocale:Ljava/util/Locale;

    .line 119
    return-void
.end method
