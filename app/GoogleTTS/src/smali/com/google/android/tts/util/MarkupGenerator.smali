.class public final Lcom/google/android/tts/util/MarkupGenerator;
.super Ljava/lang/Object;
.source "MarkupGenerator.java"


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final mBreakIterators:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/text/BreakIterator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/tts/util/MarkupGenerator;->mBreakIterators:Ljava/util/Map;

    .line 45
    sget-object v0, Lcom/google/android/tts/util/MarkupGenerator;->mBreakIterators:Ljava/util/Map;

    const-string v1, "default"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v2}, Ljava/text/BreakIterator;->getSentenceInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static breakIntoMultipleSentences(Lcom/google/speech/tts/Sentence;Ljava/util/Locale;)Ljava/util/List;
    .locals 14
    .param p0, "sentence"    # Lcom/google/speech/tts/Sentence;
    .param p1, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/speech/tts/Sentence;",
            "Ljava/util/Locale;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Sentence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/speech/tts/Sentence;->getSayList()Ljava/util/List;

    move-result-object v8

    .line 105
    .local v8, "sayList":Ljava/util/List;, "Ljava/util/List<Lcom/google/speech/tts/Say;>;"
    const/4 v3, 0x0

    .line 106
    .local v3, "it":Ljava/text/BreakIterator;
    if-eqz p1, :cond_0

    .line 107
    invoke-virtual {p1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/android/tts/util/MarkupGenerator;->getIteratorFor(Ljava/lang/String;Ljava/lang/String;)Ljava/text/BreakIterator;

    move-result-object v3

    .line 109
    :cond_0
    if-nez v3, :cond_1

    .line 110
    const-string v11, "MarkupGenerator"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Unable to find break iterator, lang = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", country ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p1}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 114
    .local v5, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/google/speech/tts/Sentence;>;"
    invoke-interface {v5, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v6, v5

    .line 169
    .end local v5    # "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/google/speech/tts/Sentence;>;"
    .local v6, "resultList":Ljava/lang/Object;, "Ljava/util/List<Lcom/google/speech/tts/Sentence;>;"
    :goto_0
    return-object v6

    .line 118
    .end local v6    # "resultList":Ljava/lang/Object;, "Ljava/util/List<Lcom/google/speech/tts/Sentence;>;"
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 120
    .restart local v5    # "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/google/speech/tts/Sentence;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v11

    invoke-direct {v0, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 121
    .local v0, "brokenSayList":Ljava/util/List;, "Ljava/util/List<Lcom/google/speech/tts/Say;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/speech/tts/Say;

    .line 122
    .local v7, "say":Lcom/google/speech/tts/Say;
    invoke-virtual {v7}, Lcom/google/speech/tts/Say;->hasText()Z

    move-result v11

    if-nez v11, :cond_3

    .line 123
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 127
    :cond_3
    invoke-virtual {v7}, Lcom/google/speech/tts/Say;->getText()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 128
    invoke-virtual {v3}, Ljava/text/BreakIterator;->first()I

    move-result v10

    .line 129
    .local v10, "start":I
    invoke-virtual {v3}, Ljava/text/BreakIterator;->next()I

    move-result v1

    .local v1, "end":I
    :goto_2
    const/4 v11, -0x1

    if-eq v1, v11, :cond_2

    .line 132
    if-eqz v10, :cond_4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_4

    .line 134
    invoke-static {p0}, Lcom/google/speech/tts/Sentence;->newBuilder(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/speech/tts/Sentence$Builder;->clearSay()Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v11

    invoke-virtual {v11, v0}, Lcom/google/speech/tts/Sentence$Builder;->addAllSay(Ljava/lang/Iterable;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/speech/tts/Sentence$Builder;->build()Lcom/google/speech/tts/Sentence;

    move-result-object v4

    .line 139
    .local v4, "newSentence":Lcom/google/speech/tts/Sentence;
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 143
    .end local v4    # "newSentence":Lcom/google/speech/tts/Sentence;
    :cond_4
    sget-object v11, Lcom/google/common/base/CharMatcher;->WHITESPACE:Lcom/google/common/base/CharMatcher;

    invoke-virtual {v7}, Lcom/google/speech/tts/Say;->getText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v10, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/google/common/base/CharMatcher;->trimFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 146
    .local v9, "sentenceString":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 129
    :goto_3
    move v10, v1

    .line 130
    invoke-virtual {v3}, Ljava/text/BreakIterator;->next()I

    move-result v1

    goto :goto_2

    .line 150
    :cond_5
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v11

    const/16 v12, 0xc8

    if-le v11, v12, :cond_6

    .line 156
    :cond_6
    invoke-static {v7}, Lcom/google/speech/tts/Say;->newBuilder(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Say$Builder;

    move-result-object v11

    invoke-virtual {v11, v9}, Lcom/google/speech/tts/Say$Builder;->setText(Ljava/lang/String;)Lcom/google/speech/tts/Say$Builder;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/speech/tts/Say$Builder;->build()Lcom/google/speech/tts/Say;

    move-result-object v11

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 160
    .end local v1    # "end":I
    .end local v7    # "say":Lcom/google/speech/tts/Say;
    .end local v9    # "sentenceString":Ljava/lang/String;
    .end local v10    # "start":I
    :cond_7
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_8

    .line 161
    invoke-static {p0}, Lcom/google/speech/tts/Sentence;->newBuilder(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/speech/tts/Sentence$Builder;->clearSay()Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v11

    invoke-virtual {v11, v0}, Lcom/google/speech/tts/Sentence$Builder;->addAllSay(Ljava/lang/Iterable;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/speech/tts/Sentence$Builder;->build()Lcom/google/speech/tts/Sentence;

    move-result-object v4

    .line 166
    .restart local v4    # "newSentence":Lcom/google/speech/tts/Sentence;
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .end local v4    # "newSentence":Lcom/google/speech/tts/Sentence;
    :cond_8
    move-object v6, v5

    .line 169
    .restart local v6    # "resultList":Ljava/lang/Object;, "Ljava/util/List<Lcom/google/speech/tts/Sentence;>;"
    goto/16 :goto_0
.end method

.method private static getBreakIteratorKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "language"    # Ljava/lang/String;
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getIteratorFor(Ljava/lang/String;Ljava/lang/String;)Ljava/text/BreakIterator;
    .locals 5
    .param p0, "language"    # Ljava/lang/String;
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    .line 173
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 174
    :cond_0
    sget-object v3, Lcom/google/android/tts/util/MarkupGenerator;->mBreakIterators:Ljava/util/Map;

    const-string v4, "default"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/text/BreakIterator;

    .line 186
    :goto_0
    return-object v3

    .line 176
    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/tts/util/MarkupGenerator;->getBreakIteratorKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, "key":Ljava/lang/String;
    sget-object v3, Lcom/google/android/tts/util/MarkupGenerator;->mBreakIterators:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 178
    sget-object v3, Lcom/google/android/tts/util/MarkupGenerator;->mBreakIterators:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/text/BreakIterator;

    goto :goto_0

    .line 180
    :cond_2
    new-instance v2, Ljava/util/Locale;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    .local v2, "locale":Ljava/util/Locale;
    invoke-static {v2}, Ljava/text/BreakIterator;->getSentenceInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    move-result-object v0

    .line 182
    .local v0, "breakIt":Ljava/text/BreakIterator;
    if-nez v0, :cond_3

    .line 183
    sget-object v3, Lcom/google/android/tts/util/MarkupGenerator;->mBreakIterators:Ljava/util/Map;

    const-string v4, "default"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/text/BreakIterator;

    goto :goto_0

    .line 185
    :cond_3
    sget-object v3, Lcom/google/android/tts/util/MarkupGenerator;->mBreakIterators:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v0

    .line 186
    goto :goto_0
.end method

.method static isSsmlLike(Ljava/lang/String;)Z
    .locals 1
    .param p0, "text"    # Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 198
    if-eqz p0, :cond_0

    const-string v0, "<speak"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static parseSpanned(Landroid/text/Spanned;Ljava/util/Locale;)Lcom/google/speech/tts/Sentence$Builder;
    .locals 19
    .param p0, "text"    # Landroid/text/Spanned;
    .param p1, "locale"    # Ljava/util/Locale;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 225
    invoke-static {}, Lcom/google/speech/tts/Sentence;->newBuilder()Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v12

    .line 227
    .local v12, "sentence":Lcom/google/speech/tts/Sentence$Builder;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 228
    .local v11, "sayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/speech/tts/Say;>;"
    const/4 v8, 0x0

    .line 229
    .local v8, "next":I
    const/4 v6, 0x0

    .line 230
    .local v6, "last":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface/range {p0 .. p0}, Landroid/text/Spanned;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v4, v0, :cond_5

    .line 232
    add-int/lit8 v17, v4, 0x1

    const-class v18, Landroid/text/style/TtsSpan;

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v4, v1, v2}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [Landroid/text/style/TtsSpan;

    .line 235
    .local v15, "spans":[Landroid/text/style/TtsSpan;
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 236
    .local v16, "startSpans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/text/style/TtsSpan;>;"
    move-object v3, v15

    .local v3, "arr$":[Landroid/text/style/TtsSpan;
    array-length v7, v3

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v7, :cond_1

    aget-object v13, v3, v5

    .line 237
    .local v13, "span":Landroid/text/style/TtsSpan;
    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v17

    move/from16 v0, v17

    if-ne v0, v4, :cond_0

    .line 238
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 242
    .end local v13    # "span":Landroid/text/style/TtsSpan;
    :cond_1
    const/4 v9, 0x0

    .line 243
    .local v9, "processedSpan":Z
    move-object v3, v15

    array-length v7, v3

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v7, :cond_3

    aget-object v13, v3, v5

    .line 244
    .restart local v13    # "span":Landroid/text/style/TtsSpan;
    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v17

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-interface {v0, v1, v2}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v14

    .line 246
    .local v14, "spannedText":Ljava/lang/CharSequence;
    move-object/from16 v0, p1

    invoke-static {v13, v12, v14, v0}, Lcom/google/android/tts/util/MarkupHelper;->createSay(Landroid/text/style/TtsSpan;Lcom/google/speech/tts/Sentence$Builder;Ljava/lang/CharSequence;Ljava/util/Locale;)Lcom/google/speech/tts/Say;

    move-result-object v10

    .line 247
    .local v10, "say":Lcom/google/speech/tts/Say;
    if-nez v10, :cond_2

    .line 243
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 250
    :cond_2
    const/4 v9, 0x1

    .line 251
    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    .line 252
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    .end local v10    # "say":Lcom/google/speech/tts/Say;
    .end local v13    # "span":Landroid/text/style/TtsSpan;
    .end local v14    # "spannedText":Ljava/lang/CharSequence;
    :cond_3
    if-nez v9, :cond_4

    .line 262
    invoke-interface/range {p0 .. p0}, Landroid/text/Spanned;->length()I

    move-result v17

    const-class v18, Landroid/text/style/TtsSpan;

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-interface {v0, v4, v1, v2}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v8

    .line 263
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Lcom/google/android/tts/util/MarkupHelper;->createSayText(Ljava/lang/String;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/Say;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    :cond_4
    move v4, v8

    goto/16 :goto_0

    .line 266
    .end local v3    # "arr$":[Landroid/text/style/TtsSpan;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    .end local v9    # "processedSpan":Z
    .end local v15    # "spans":[Landroid/text/style/TtsSpan;
    .end local v16    # "startSpans":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/text/style/TtsSpan;>;"
    :cond_5
    invoke-virtual {v12, v11}, Lcom/google/speech/tts/Sentence$Builder;->addAllSay(Ljava/lang/Iterable;)Lcom/google/speech/tts/Sentence$Builder;

    .line 267
    return-object v12
.end method

.method static parseSsml(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6
    .param p0, "text"    # Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/speech/tts/Say;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 208
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/speech/tts/Say;>;"
    new-instance v1, Lcom/google/android/tts/util/SsmlHandler;

    invoke-direct {v1, v2}, Lcom/google/android/tts/util/SsmlHandler;-><init>(Ljava/util/List;)V

    .line 211
    .local v1, "handler":Lcom/google/android/tts/util/SsmlHandler;
    :try_start_0
    invoke-static {p0, v1}, Landroid/util/Xml;->parse(Ljava/lang/String;Lorg/xml/sax/ContentHandler;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :goto_0
    return-object v2

    .line 212
    :catch_0
    move-exception v0

    .line 213
    .local v0, "e":Lorg/xml/sax/SAXException;
    const-string v3, "MarkupGenerator"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error parsing XML : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public static process(Ljava/lang/CharSequence;Ljava/util/Locale;Lcom/google/speech/tts/VoiceMod;Z)Ljava/util/List;
    .locals 9
    .param p0, "text"    # Ljava/lang/CharSequence;
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "voiceMod"    # Lcom/google/speech/tts/VoiceMod;
    .param p3, "breakSentence"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/Locale;",
            "Lcom/google/speech/tts/VoiceMod;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Sentence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/tts/util/MarkupGenerator;->isSsmlLike(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 66
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/tts/util/MarkupGenerator;->parseSsml(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/tts/util/MarkupHelper;->createSentenceBuilder(Ljava/util/List;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v6

    .line 76
    .end local p0    # "text":Ljava/lang/CharSequence;
    .local v6, "sentence":Lcom/google/speech/tts/Sentence$Builder;
    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 77
    .local v5, "saysWithVoiceMod":Ljava/util/List;, "Ljava/util/List<Lcom/google/speech/tts/Say;>;"
    invoke-virtual {v6}, Lcom/google/speech/tts/Sentence$Builder;->getSayList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/speech/tts/Say;

    .line 78
    .local v3, "say":Lcom/google/speech/tts/Say;
    invoke-virtual {v3}, Lcom/google/speech/tts/Say;->hasVoicemod()Z

    move-result v7

    if-nez v7, :cond_2

    if-eqz p2, :cond_2

    .line 79
    invoke-static {v3}, Lcom/google/speech/tts/Say;->newBuilder(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Say$Builder;

    move-result-object v7

    invoke-virtual {v7, p2}, Lcom/google/speech/tts/Say$Builder;->setVoicemod(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/Say$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/speech/tts/Say$Builder;->build()Lcom/google/speech/tts/Say;

    move-result-object v1

    .line 80
    .local v1, "newSay":Lcom/google/speech/tts/Say;
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 67
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "newSay":Lcom/google/speech/tts/Say;
    .end local v3    # "say":Lcom/google/speech/tts/Say;
    .end local v5    # "saysWithVoiceMod":Ljava/util/List;, "Ljava/util/List<Lcom/google/speech/tts/Say;>;"
    .end local v6    # "sentence":Lcom/google/speech/tts/Sentence$Builder;
    .restart local p0    # "text":Ljava/lang/CharSequence;
    :cond_0
    instance-of v7, p0, Landroid/text/Spanned;

    if-eqz v7, :cond_1

    .line 68
    check-cast p0, Landroid/text/Spanned;

    .end local p0    # "text":Ljava/lang/CharSequence;
    invoke-static {p0, p1}, Lcom/google/android/tts/util/MarkupGenerator;->parseSpanned(Landroid/text/Spanned;Ljava/util/Locale;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v6

    .restart local v6    # "sentence":Lcom/google/speech/tts/Sentence$Builder;
    goto :goto_0

    .line 70
    .end local v6    # "sentence":Lcom/google/speech/tts/Sentence$Builder;
    .restart local p0    # "text":Ljava/lang/CharSequence;
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .local v4, "sayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/speech/tts/Say;>;"
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/google/android/tts/util/MarkupHelper;->createSayText(Ljava/lang/String;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/Say;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    invoke-static {v4}, Lcom/google/android/tts/util/MarkupHelper;->createSentenceBuilder(Ljava/util/List;)Lcom/google/speech/tts/Sentence$Builder;

    move-result-object v6

    .restart local v6    # "sentence":Lcom/google/speech/tts/Sentence$Builder;
    goto :goto_0

    .line 82
    .end local v4    # "sayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/speech/tts/Say;>;"
    .end local p0    # "text":Ljava/lang/CharSequence;
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v3    # "say":Lcom/google/speech/tts/Say;
    .restart local v5    # "saysWithVoiceMod":Ljava/util/List;, "Ljava/util/List<Lcom/google/speech/tts/Say;>;"
    :cond_2
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 85
    .end local v3    # "say":Lcom/google/speech/tts/Say;
    :cond_3
    invoke-virtual {v6}, Lcom/google/speech/tts/Sentence$Builder;->clearSay()Lcom/google/speech/tts/Sentence$Builder;

    .line 86
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/speech/tts/Say;

    .line 87
    .restart local v3    # "say":Lcom/google/speech/tts/Say;
    invoke-virtual {v6, v3}, Lcom/google/speech/tts/Sentence$Builder;->addSay(Lcom/google/speech/tts/Say;)Lcom/google/speech/tts/Sentence$Builder;

    goto :goto_2

    .line 91
    .end local v3    # "say":Lcom/google/speech/tts/Say;
    :cond_4
    if-eqz p3, :cond_5

    .line 92
    invoke-virtual {v6}, Lcom/google/speech/tts/Sentence$Builder;->build()Lcom/google/speech/tts/Sentence;

    move-result-object v7

    invoke-static {v7, p1}, Lcom/google/android/tts/util/MarkupGenerator;->breakIntoMultipleSentences(Lcom/google/speech/tts/Sentence;Ljava/util/Locale;)Ljava/util/List;

    move-result-object v2

    .line 96
    :goto_3
    return-object v2

    .line 94
    :cond_5
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v2, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/google/speech/tts/Sentence;>;"
    invoke-virtual {v6}, Lcom/google/speech/tts/Sentence$Builder;->build()Lcom/google/speech/tts/Sentence;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method
