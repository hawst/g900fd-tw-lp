.class public abstract Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;
.super Landroid/app/Activity;
.source "VoiceDataListenerBaseActivity.java"


# instance fields
.field private final mDownloaderListener:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;

.field private mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

.field private mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

.field private final mVoicesDataListener:Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 43
    new-instance v0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity$1;-><init>(Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;)V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->mVoicesDataListener:Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;

    .line 57
    new-instance v0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity$2;-><init>(Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;)V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->mDownloaderListener:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->updateVoiceData()V

    return-void
.end method

.method private updateVoiceData()V
    .locals 1

    .prologue
    .line 77
    new-instance v0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity$3;-><init>(Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 83
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    invoke-static {p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v0

    .line 22
    .local v0, "ttsApp":Lcom/google/android/tts/service/GoogleTTSApplication;
    invoke-virtual {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataDownloader()Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    .line 23
    invoke-virtual {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    .line 24
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->mVoicesDataListener:Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;

    invoke-virtual {v0, v1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->removeVoicesDataListener(Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;)V

    .line 37
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->mDownloaderListener:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;

    invoke-interface {v0, v1}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;->removeListener(Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;)V

    .line 38
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 39
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 29
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->mVoicesDataListener:Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;

    invoke-virtual {v0, v1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->addVoicesDataListener(Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;)V

    .line 30
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->mDownloaderListener:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;

    invoke-interface {v0, v1}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;->addListener(Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;)V

    .line 31
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->updateVoiceData()V

    .line 32
    return-void
.end method

.method protected abstract onUpdateVoiceData()V
.end method
