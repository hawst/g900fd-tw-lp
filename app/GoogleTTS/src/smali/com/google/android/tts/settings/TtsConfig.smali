.class public interface abstract Lcom/google/android/tts/settings/TtsConfig;
.super Ljava/lang/Object;
.source "TtsConfig.java"


# virtual methods
.method public abstract getAutoUpdate()Z
.end method

.method public abstract getDefaultTimeoutNetworkFirst()I
.end method

.method public abstract getDefaultTimeoutNetworkOnly()I
.end method

.method public abstract getDefaultVoiceName(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getMetadataUpdateFrequencyMs()I
.end method

.method public abstract getMetadataUpdateTime()J
.end method

.method public abstract getWifiOnly()Z
.end method

.method public abstract setDefaultVoiceName(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setMetadataUpdateTime(J)V
.end method
