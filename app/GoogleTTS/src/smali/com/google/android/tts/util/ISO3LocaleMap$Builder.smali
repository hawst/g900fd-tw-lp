.class public final Lcom/google/android/tts/util/ISO3LocaleMap$Builder;
.super Ljava/lang/Object;
.source "ISO3LocaleMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/util/ISO3LocaleMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mLocaleToEntry:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/util/Locale;",
            "Lcom/google/android/tts/util/ISO3LocaleMap$Builder",
            "<TT;>.ScoredEntry<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/tts/util/ISO3LocaleMap$Builder;, "Lcom/google/android/tts/util/ISO3LocaleMap$Builder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;->mLocaleToEntry:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addLocale(Ljava/util/Locale;IILjava/lang/Object;)V
    .locals 6
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "localeScore"    # I
    .param p3, "languageScore"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            "IITT;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lcom/google/android/tts/util/ISO3LocaleMap$Builder;, "Lcom/google/android/tts/util/ISO3LocaleMap$Builder<TT;>;"
    .local p4, "value":Ljava/lang/Object;, "TT;"
    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 54
    # invokes: Lcom/google/android/tts/util/ISO3LocaleMap;->toISO3Locale(Ljava/util/Locale;)Ljava/util/Locale;
    invoke-static {p1}, Lcom/google/android/tts/util/ISO3LocaleMap;->access$000(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v2

    .line 55
    .local v2, "iso3Locale":Ljava/util/Locale;
    iget-object v4, p0, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;->mLocaleToEntry:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;

    .line 56
    .local v0, "entry":Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;, "Lcom/google/android/tts/util/ISO3LocaleMap$Builder<TT;>.ScoredEntry<TT;>;"
    if-eqz v0, :cond_0

    iget v4, v0, Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;->score:I

    if-ge v4, p2, :cond_1

    .line 57
    :cond_0
    iget-object v4, p0, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;->mLocaleToEntry:Ljava/util/Map;

    new-instance v5, Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;

    invoke-direct {v5, p0, p4, p2}, Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;-><init>(Lcom/google/android/tts/util/ISO3LocaleMap$Builder;Ljava/lang/Object;I)V

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    .end local v0    # "entry":Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;, "Lcom/google/android/tts/util/ISO3LocaleMap$Builder<TT;>.ScoredEntry<TT;>;"
    .end local v2    # "iso3Locale":Ljava/util/Locale;
    :cond_1
    # invokes: Lcom/google/android/tts/util/ISO3LocaleMap;->toISO3LanguageLocale(Ljava/util/Locale;)Ljava/util/Locale;
    invoke-static {p1}, Lcom/google/android/tts/util/ISO3LocaleMap;->access$100(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v1

    .line 62
    .local v1, "iso3LanguageLocale":Ljava/util/Locale;
    iget-object v4, p0, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;->mLocaleToEntry:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;

    .line 63
    .local v3, "languageEntry":Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;, "Lcom/google/android/tts/util/ISO3LocaleMap$Builder<TT;>.ScoredEntry<TT;>;"
    if-eqz v3, :cond_2

    iget v4, v3, Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;->score:I

    if-ge v4, p3, :cond_3

    .line 64
    :cond_2
    iget-object v4, p0, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;->mLocaleToEntry:Ljava/util/Map;

    new-instance v5, Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;

    invoke-direct {v5, p0, p4, p3}, Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;-><init>(Lcom/google/android/tts/util/ISO3LocaleMap$Builder;Ljava/lang/Object;I)V

    invoke-interface {v4, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    :cond_3
    return-void
.end method

.method public build()Lcom/google/android/tts/util/ISO3LocaleMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/tts/util/ISO3LocaleMap",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "this":Lcom/google/android/tts/util/ISO3LocaleMap$Builder;, "Lcom/google/android/tts/util/ISO3LocaleMap$Builder<TT;>;"
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 73
    .local v2, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/util/Locale;TT;>;"
    iget-object v3, p0, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;->mLocaleToEntry:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 74
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/util/Locale;Lcom/google/android/tts/util/ISO3LocaleMap$Builder<TT;>.ScoredEntry<TT;>;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;

    iget-object v3, v3, Lcom/google/android/tts/util/ISO3LocaleMap$Builder$ScoredEntry;->value:Ljava/lang/Object;

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 76
    .end local v0    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/util/Locale;Lcom/google/android/tts/util/ISO3LocaleMap$Builder<TT;>.ScoredEntry<TT;>;>;"
    :cond_0
    new-instance v3, Lcom/google/android/tts/util/ISO3LocaleMap;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcom/google/android/tts/util/ISO3LocaleMap;-><init>(Ljava/util/Map;Lcom/google/android/tts/util/ISO3LocaleMap$1;)V

    return-object v3
.end method
