.class final Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;
.super Ljava/lang/Object;
.source "VoiceEntryListUIHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper;->getVoicesEntryOverflowListener(Landroid/content/Context;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$entry:Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

.field final synthetic val$listener:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;->val$entry:Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    iput-object p3, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;->val$listener:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 28
    new-instance v1, Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;->val$context:Landroid/content/Context;

    invoke-direct {v1, v2, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 29
    .local v1, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 30
    .local v0, "minflater":Landroid/view/MenuInflater;
    const v2, 0x7f090001

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 32
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;->val$entry:Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    invoke-virtual {v3}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isUpdatable()Z

    move-result v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 34
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;->val$entry:Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    invoke-virtual {v3}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isRemovable()Z

    move-result v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 36
    new-instance v2, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1$1;

    invoke-direct {v2, p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1$1;-><init>(Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 51
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    .line 52
    return-void
.end method
