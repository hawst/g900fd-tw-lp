.class public final Lcom/google/android/tts/local/LocalLegacyVoiceResolver;
.super Ljava/lang/Object;
.source "LocalLegacyVoiceResolver.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAutoDownloadMap:Lcom/google/android/tts/util/ISO3LocaleMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/tts/util/ISO3LocaleMap",
            "<",
            "Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private mInstalledMap:Lcom/google/android/tts/util/ISO3LocaleMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/tts/util/ISO3LocaleMap",
            "<",
            "Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private final mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;

.field private final mTtsConfig:Lcom/google/android/tts/settings/TtsConfig;

.field private final mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/tts/local/voicepack/VoiceDataManager;Lcom/google/android/tts/settings/TtsConfig;Lcom/google/android/tts/util/PreferredLocales;)V
    .locals 1
    .param p1, "voiceMgr"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager;
    .param p2, "ttsConfig"    # Lcom/google/android/tts/settings/TtsConfig;
    .param p3, "preferredLocales"    # Lcom/google/android/tts/util/PreferredLocales;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mLock:Ljava/lang/Object;

    .line 35
    iput-object p1, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    .line 36
    iput-object p2, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfig;

    .line 37
    iput-object p3, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;

    .line 38
    return-void
.end method


# virtual methods
.method public getAutoInstallVoiceFor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 3
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 150
    iget-object v2, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 151
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mAutoDownloadMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    .line 152
    .local v0, "autoDownloadMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/util/ISO3LocaleMap;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    return-object v1

    .line 152
    .end local v0    # "autoDownloadMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public getInstalledVoiceFor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    .locals 3
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 138
    iget-object v2, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 139
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mInstalledMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    .line 140
    .local v0, "localemap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/util/ISO3LocaleMap;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    return-object v1

    .line 140
    .end local v0    # "localemap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 110
    iget-object v5, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 111
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mAutoDownloadMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    .line 112
    .local v0, "autoDownloadMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    iget-object v2, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mInstalledMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    .line 113
    .local v2, "installedMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    invoke-virtual {v2, p1, p2}, Lcom/google/android/tts/util/ISO3LocaleMap;->isLanguageAvailableV1(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 123
    .local v3, "installedStatus":I
    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/util/ISO3LocaleMap;->isLanguageAvailableV1(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 124
    .local v1, "autoDownloadStatus":I
    if-ltz v3, :cond_0

    .line 130
    .end local v3    # "installedStatus":I
    :goto_0
    return v3

    .line 113
    .end local v0    # "autoDownloadMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    .end local v1    # "autoDownloadStatus":I
    .end local v2    # "installedMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 127
    .restart local v0    # "autoDownloadMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    .restart local v1    # "autoDownloadStatus":I
    .restart local v2    # "installedMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    .restart local v3    # "installedStatus":I
    :cond_0
    if-ltz v1, :cond_1

    .line 128
    const/4 v3, -0x1

    goto :goto_0

    .line 130
    :cond_1
    const/4 v3, -0x2

    goto :goto_0
.end method

.method public isVoiceAvailableForDownloadByLanguage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 163
    iget-object v6, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mLock:Ljava/lang/Object;

    monitor-enter v6

    .line 164
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mAutoDownloadMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    .line 165
    .local v0, "autoDownloadMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    iget-object v2, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mInstalledMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    .line 166
    .local v2, "installedMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    invoke-virtual {v2, p1, p2}, Lcom/google/android/tts/util/ISO3LocaleMap;->isLanguageAvailableV1(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 169
    .local v3, "installedStatus":I
    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/util/ISO3LocaleMap;->isLanguageAvailableV1(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 170
    .local v1, "autoDownloadStatus":I
    if-lt v3, v5, :cond_1

    .line 176
    :cond_0
    :goto_0
    return v4

    .line 166
    .end local v0    # "autoDownloadMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    .end local v1    # "autoDownloadStatus":I
    .end local v2    # "installedMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    .end local v3    # "installedStatus":I
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 173
    .restart local v0    # "autoDownloadMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    .restart local v1    # "autoDownloadStatus":I
    .restart local v2    # "installedMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    .restart local v3    # "installedStatus":I
    :cond_1
    if-ltz v1, :cond_0

    move v4, v5

    .line 174
    goto :goto_0
.end method

.method public isVoiceAvailableForDownloadByName(Ljava/lang/String;)Z
    .locals 4
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 185
    iget-object v3, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 186
    :try_start_0
    iget-object v2, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v2}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAvailableVoicesInfo()Ljava/util/Map;

    move-result-object v1

    .line 187
    .local v1, "installedVoices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    iget-object v2, p0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v2}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAutoInstallVoicesMetadata()Ljava/util/Map;

    move-result-object v0

    .line 188
    .local v0, "autoInstalVoices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    .line 188
    .end local v0    # "autoInstalVoices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    .end local v1    # "installedVoices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 190
    .restart local v0    # "autoInstalVoices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    .restart local v1    # "installedVoices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public updateLocaleList()V
    .locals 23

    .prologue
    .line 42
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAvailableVoicesInfo()Ljava/util/Map;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    .line 44
    .local v5, "availableVoices":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    new-instance v7, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;

    invoke-direct {v7}, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;-><init>()V

    .line 47
    .local v7, "installedBuilder":Lcom/google/android/tts/util/ISO3LocaleMap$Builder;, "Lcom/google/android/tts/util/ISO3LocaleMap$Builder<Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 48
    .local v19, "voice":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfig;

    move-object/from16 v21, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-interface/range {v21 .. v22}, Lcom/google/android/tts/settings/TtsConfig;->getDefaultVoiceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 49
    .local v15, "localeDefault":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;

    move-object/from16 v21, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/tts/util/PreferredLocales;->getPreffered(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v12

    .line 52
    .local v12, "languagePreferredLocale":Ljava/util/Locale;
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getName()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 53
    .local v8, "isLocaleDefault":Z
    if-eqz v12, :cond_0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    const/4 v10, 0x1

    .line 69
    .local v10, "isPreferredForLanguage":Z
    :goto_1
    if-eqz v8, :cond_1

    const/16 v16, 0x1

    .line 70
    .local v16, "localeScore":I
    :goto_2
    if-eqz v10, :cond_2

    const/16 v21, 0x2

    :goto_3
    add-int v13, v21, v16

    .line 72
    .local v13, "languageScore":I
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLocale()Ljava/util/Locale;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v16

    move-object/from16 v2, v19

    invoke-virtual {v7, v0, v1, v13, v2}, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;->addLocale(Ljava/util/Locale;IILjava/lang/Object;)V

    goto :goto_0

    .line 53
    .end local v10    # "isPreferredForLanguage":Z
    .end local v13    # "languageScore":I
    .end local v16    # "localeScore":I
    :cond_0
    const/4 v10, 0x0

    goto :goto_1

    .line 69
    .restart local v10    # "isPreferredForLanguage":Z
    :cond_1
    const/16 v16, 0x0

    goto :goto_2

    .line 70
    .restart local v16    # "localeScore":I
    :cond_2
    const/16 v21, 0x0

    goto :goto_3

    .line 76
    .end local v8    # "isLocaleDefault":Z
    .end local v10    # "isPreferredForLanguage":Z
    .end local v12    # "languagePreferredLocale":Ljava/util/Locale;
    .end local v15    # "localeDefault":Ljava/lang/String;
    .end local v16    # "localeScore":I
    .end local v19    # "voice":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAutoInstallVoicesMetadata()Ljava/util/Map;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    .line 78
    .local v4, "autoInstallVoices":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    new-instance v3, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;

    invoke-direct {v3}, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;-><init>()V

    .line 80
    .local v3, "autoInstallBuilder":Lcom/google/android/tts/util/ISO3LocaleMap$Builder;, "Lcom/google/android/tts/util/ISO3LocaleMap$Builder<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 81
    .local v20, "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-static/range {v20 .. v20}, Lcom/google/android/tts/util/LocalesHelper;->createFromMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/util/Locale;

    move-result-object v14

    .line 82
    .local v14, "locale":Ljava/util/Locale;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;

    move-object/from16 v21, v0

    invoke-virtual {v14}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/tts/util/PreferredLocales;->getPreffered(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v11

    .line 83
    .local v11, "languagePreferedLocale":Ljava/util/Locale;
    if-eqz v11, :cond_4

    invoke-virtual {v11, v14}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    const/4 v9, 0x1

    .line 86
    .local v9, "isPreferedForLanguage":Z
    :goto_5
    const/16 v22, 0x1

    if-eqz v9, :cond_5

    const/16 v21, 0x1

    :goto_6
    move/from16 v0, v22

    move/from16 v1, v21

    move-object/from16 v2, v20

    invoke-virtual {v3, v14, v0, v1, v2}, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;->addLocale(Ljava/util/Locale;IILjava/lang/Object;)V

    goto :goto_4

    .line 83
    .end local v9    # "isPreferedForLanguage":Z
    :cond_4
    const/4 v9, 0x0

    goto :goto_5

    .line 86
    .restart local v9    # "isPreferedForLanguage":Z
    :cond_5
    const/16 v21, 0x0

    goto :goto_6

    .line 90
    .end local v9    # "isPreferedForLanguage":Z
    .end local v11    # "languagePreferedLocale":Ljava/util/Locale;
    .end local v14    # "locale":Ljava/util/Locale;
    .end local v20    # "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :cond_6
    invoke-virtual {v7}, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;->build()Lcom/google/android/tts/util/ISO3LocaleMap;

    move-result-object v18

    .line 92
    .local v18, "newInstalledMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    invoke-virtual {v3}, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;->build()Lcom/google/android/tts/util/ISO3LocaleMap;

    move-result-object v17

    .line 94
    .local v17, "newAutoInstallMap":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mLock:Ljava/lang/Object;

    move-object/from16 v22, v0

    monitor-enter v22

    .line 95
    :try_start_0
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mInstalledMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    .line 96
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->mAutoDownloadMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    .line 97
    monitor-exit v22

    .line 98
    return-void

    .line 97
    :catchall_0
    move-exception v21

    monitor-exit v22
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v21
.end method
