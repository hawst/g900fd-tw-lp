.class Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1$1;
.super Ljava/lang/Object;
.source "VoiceEntryListUIHelper.java"

# interfaces
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;


# direct methods
.method constructor <init>(Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1$1;->this$0:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "mi"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 40
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0a000e

    if-ne v1, v2, :cond_0

    .line 41
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1$1;->this$0:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;

    iget-object v1, v1, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;->val$listener:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1$1;->this$0:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;

    iget-object v2, v2, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;->val$entry:Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    invoke-interface {v1, v2}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;->onUpdateClick(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V

    .line 48
    :goto_0
    return v0

    .line 43
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0a000f

    if-ne v1, v2, :cond_1

    .line 45
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1$1;->this$0:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;

    iget-object v1, v1, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1$1;->this$0:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;

    iget-object v2, v2, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;->val$entry:Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1$1;->this$0:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;

    iget-object v3, v3, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;->val$listener:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;

    invoke-static {v1, v2, v3}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper;->showUninstallWarning(Landroid/content/Context;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;)V

    goto :goto_0

    .line 48
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
