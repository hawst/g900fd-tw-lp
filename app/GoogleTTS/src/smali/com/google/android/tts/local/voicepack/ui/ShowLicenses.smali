.class public Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;
.super Ljava/lang/Object;
.source "ShowLicenses.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/local/voicepack/ui/ShowLicenses$LicenseFileLoader;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mSpinnerDlg:Landroid/app/ProgressDialog;

.field private mTextDlg:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->showPageOfText(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->showError()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mSpinnerDlg:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mSpinnerDlg:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mTextDlg:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private showError()V
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mSpinnerDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mSpinnerDlg:Landroid/app/ProgressDialog;

    .line 110
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mContext:Landroid/content/Context;

    const v1, 0x7f060006

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 112
    return-void
.end method

.method private showPageOfText(Ljava/lang/String;)V
    .locals 7
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 82
    new-instance v0, Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 83
    .local v0, "webView":Landroid/webkit/WebView;
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mContext:Landroid/content/Context;

    invoke-direct {v6, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 84
    .local v6, "builder":Landroid/app/AlertDialog$Builder;
    const/4 v2, 0x1

    invoke-virtual {v6, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f060004

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 88
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mTextDlg:Landroid/app/AlertDialog;

    .line 91
    const-string v3, "text/html"

    const-string v4, "utf-8"

    move-object v2, p1

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    new-instance v1, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses$2;

    invoke-direct {v1, p0}, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses$2;-><init>(Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 105
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses$1;

    invoke-direct {v0, p0}, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses$1;-><init>(Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;)V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mHandler:Landroid/os/Handler;

    .line 49
    if-eqz p1, :cond_0

    const-string v0, "showing_licenses"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->showOpenSourceLicenses()V

    .line 53
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mTextDlg:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mTextDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 58
    iput-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mTextDlg:Landroid/app/AlertDialog;

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mSpinnerDlg:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mSpinnerDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 63
    iput-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mSpinnerDlg:Landroid/app/ProgressDialog;

    .line 65
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mTextDlg:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mTextDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const-string v0, "showing_licenses"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 157
    :cond_0
    return-void
.end method

.method public showOpenSourceLicenses()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 68
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mContext:Landroid/content/Context;

    const v5, 0x7f060004

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 69
    .local v3, "title":Ljava/lang/CharSequence;
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mContext:Landroid/content/Context;

    const v5, 0x7f060005

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 71
    .local v0, "msg":Ljava/lang/CharSequence;
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-static {v4, v3, v0, v5, v6}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v1

    .line 72
    .local v1, "pd":Landroid/app/ProgressDialog;
    invoke-virtual {v1, v6}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 73
    iput-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->mSpinnerDlg:Landroid/app/ProgressDialog;

    .line 76
    new-instance v2, Ljava/lang/Thread;

    new-instance v4, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses$LicenseFileLoader;

    invoke-direct {v4, p0}, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses$LicenseFileLoader;-><init>(Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;)V

    invoke-direct {v2, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 77
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 78
    return-void
.end method
