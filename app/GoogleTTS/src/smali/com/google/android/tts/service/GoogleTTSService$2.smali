.class Lcom/google/android/tts/service/GoogleTTSService$2;
.super Ljava/lang/Object;
.source "GoogleTTSService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/tts/service/GoogleTTSService;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/tts/service/GoogleTTSService;

.field final synthetic val$countryLower:Ljava/lang/String;

.field final synthetic val$langLower:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/tts/service/GoogleTTSService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/google/android/tts/service/GoogleTTSService$2;->this$0:Lcom/google/android/tts/service/GoogleTTSService;

    iput-object p2, p0, Lcom/google/android/tts/service/GoogleTTSService$2;->val$langLower:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/tts/service/GoogleTTSService$2;->val$countryLower:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService$2;->this$0:Lcom/google/android/tts/service/GoogleTTSService;

    # getter for: Lcom/google/android/tts/service/GoogleTTSService;->mLocaleBasedDispatcher:Lcom/google/android/tts/voices/LocaleBasedDispatcher;
    invoke-static {v0}, Lcom/google/android/tts/service/GoogleTTSService;->access$000(Lcom/google/android/tts/service/GoogleTTSService;)Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSService$2;->val$langLower:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/tts/service/GoogleTTSService$2;->val$countryLower:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    return-void
.end method
