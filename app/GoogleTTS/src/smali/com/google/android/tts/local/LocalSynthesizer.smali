.class public Lcom/google/android/tts/local/LocalSynthesizer;
.super Ljava/lang/Object;
.source "LocalSynthesizer.java"

# interfaces
.implements Lcom/google/android/tts/util/DownloadEnabler;
.implements Lcom/google/android/tts/voices/VoiceSynthesizer;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mChangeListener:Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;

.field private final mContext:Landroid/content/Context;

.field private mCurrentVoice:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

.field private mFallbackSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

.field private final mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

.field private mRequestStartTime:J

.field private volatile mStopRequested:Z

.field private final mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

.field private mSynthesisBuf:Ljava/nio/ByteBuffer;

.field private final mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

.field private final mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/google/android/tts/local/LocalSynthesizer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/LocalSynthesizer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/tts/local/LocalLegacyVoiceResolver;Lcom/google/android/tts/local/voicepack/VoiceDataManager;Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;Lcom/google/android/tts/voices/Synthesizer;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "localLegacyVoiceResolver"    # Lcom/google/android/tts/local/LocalLegacyVoiceResolver;
    .param p3, "voiceDataManager"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager;
    .param p4, "voiceDataDownloader"    # Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;
    .param p5, "fallbackSynthesizer"    # Lcom/google/android/tts/voices/Synthesizer;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mRequestStartTime:J

    .line 106
    new-instance v0, Lcom/google/android/tts/local/LocalSynthesizer$2;

    invoke-direct {v0, p0}, Lcom/google/android/tts/local/LocalSynthesizer$2;-><init>(Lcom/google/android/tts/local/LocalSynthesizer;)V

    iput-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mChangeListener:Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;

    .line 87
    new-instance v0, Lcom/google/android/tts/local/LocalSynthesizer$1;

    invoke-direct {v0, p0}, Lcom/google/android/tts/local/LocalSynthesizer$1;-><init>(Lcom/google/android/tts/local/LocalSynthesizer;)V

    iput-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    .line 93
    iput-object p2, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    .line 94
    iput-object p3, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    .line 95
    iput-object p4, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    .line 97
    iput-object p5, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mFallbackSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    .line 98
    iput-object p1, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mContext:Landroid/content/Context;

    .line 101
    iget-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    iget-object v1, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mChangeListener:Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;

    invoke-virtual {v0, v1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->addVoicesDataListener(Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    invoke-virtual {v0}, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->updateLocaleList()V

    .line 104
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/local/LocalSynthesizer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/LocalSynthesizer;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/tts/local/LocalSynthesizer;)Lcom/google/android/tts/local/LocalLegacyVoiceResolver;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/LocalSynthesizer;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    return-object v0
.end method

.method static createVoiceModV1(Lcom/google/android/tts/service/GoogleTTSRequest;)Lcom/google/speech/tts/VoiceMod;
    .locals 3
    .param p0, "request"    # Lcom/google/android/tts/service/GoogleTTSRequest;

    .prologue
    .line 420
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/tts/service/GoogleTTSRequest;->getAndroidPitch()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/16 v2, 0x1f4

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float v0, v1, v2

    .line 421
    .local v0, "scaledPitch":F
    invoke-static {}, Lcom/google/speech/tts/VoiceMod;->newBuilder()Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/tts/service/GoogleTTSRequest;->getSpeechDurationMultiplier()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/speech/tts/VoiceMod$Builder;->setDurScale(F)Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/speech/tts/VoiceMod$Builder;->setF0Scale(F)Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/VoiceMod$Builder;->build()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    return-object v1
.end method

.method private destroySynthesizer()V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v0}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v0}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->shutdown()V

    .line 384
    :cond_0
    return-void
.end method

.method private initializeSynthesizer(Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;)V
    .locals 10
    .param p1, "voice"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .prologue
    .line 342
    iget-object v6, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mCurrentVoice:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    invoke-virtual {p1, v6}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v6}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 378
    :goto_0
    return-void

    .line 345
    :cond_0
    iget-object v6, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v6}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 346
    iget-object v6, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v6}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->shutdown()V

    .line 348
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getAssetManager()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 349
    .local v0, "am":Landroid/content/res/AssetManager;
    iget-object v6, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 350
    .local v1, "amPackageName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->isInApk()Z

    move-result v5

    .line 351
    .local v5, "isInsideApk":Z
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getAbsoluteFilePath()Ljava/lang/String;

    move-result-object v2

    .line 352
    .local v2, "dataPath":Ljava/lang/String;
    const/4 v4, 0x0

    .line 354
    .local v4, "init":Z
    iget-object v6, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v6, v1, v0, v5}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->setAssetManager(Ljava/lang/String;Landroid/content/res/AssetManager;Z)V

    .line 357
    if-eqz v5, :cond_2

    .line 360
    :try_start_0
    iget-object v6, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "project_hq"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v7

    invoke-virtual {v6, v7, v2}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->initialize(Ljava/io/InputStream;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 373
    :goto_1
    if-nez v4, :cond_3

    .line 374
    sget-object v6, Lcom/google/android/tts/local/LocalSynthesizer;->TAG:Ljava/lang/String;

    const-string v7, "Failed to load synthesis data."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 365
    :cond_2
    :try_start_1
    iget-object v6, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    new-instance v7, Ljava/io/FileInputStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "project_hq"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7, v2}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->initialize(Ljava/io/InputStream;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    goto :goto_1

    .line 369
    :catch_0
    move-exception v3

    .line 370
    .local v3, "e":Ljava/io/IOException;
    sget-object v6, Lcom/google/android/tts/local/LocalSynthesizer;->TAG:Ljava/lang/String;

    const-string v7, "Unable to open project file."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 376
    .end local v3    # "e":Ljava/io/IOException;
    :cond_3
    iput-object p1, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mCurrentVoice:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    goto/16 :goto_0
.end method

.method private synthesizeMarkup(Lcom/google/android/tts/service/GoogleTTSRequest;Lcom/google/speech/tts/Sentence;Landroid/speech/tts/SynthesisCallback;)Z
    .locals 10
    .param p1, "request"    # Lcom/google/android/tts/service/GoogleTTSRequest;
    .param p2, "sentence"    # Lcom/google/speech/tts/Sentence;
    .param p3, "callback"    # Landroid/speech/tts/SynthesisCallback;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 244
    iget-object v9, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v9}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v9

    if-nez v9, :cond_0

    .line 245
    sget-object v8, Lcom/google/android/tts/local/LocalSynthesizer;->TAG:Ljava/lang/String;

    const-string v9, "synthesizeMarkup requested before engine initialized."

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-interface {p3}, Landroid/speech/tts/SynthesisCallback;->error()V

    .line 304
    :goto_0
    return v7

    .line 252
    :cond_0
    const/4 v3, 0x0

    .line 254
    .local v3, "handle":Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;
    :try_start_0
    iget-object v9, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v9, p2}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->initBufferedSynthesis(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;
    :try_end_0
    .catch Lcom/google/speech/patts/api/common/SpeechSynthesizer$NonAudiableInputException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 262
    if-nez v3, :cond_1

    .line 263
    invoke-interface {p3}, Landroid/speech/tts/SynthesisCallback;->error()V

    goto :goto_0

    .line 255
    :catch_0
    move-exception v2

    .local v2, "e":Lcom/google/speech/patts/api/common/SpeechSynthesizer$NonAudiableInputException;
    move v7, v8

    .line 257
    goto :goto_0

    .line 267
    .end local v2    # "e":Lcom/google/speech/patts/api/common/SpeechSynthesizer$NonAudiableInputException;
    :cond_1
    const/4 v6, 0x0

    .line 269
    .local v6, "numBytes":I
    :cond_2
    iget-object v7, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    iget-object v9, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynthesisBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v7, v3, v9}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->synthesizeToDirectBuffer(Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;Ljava/nio/ByteBuffer;)I

    move-result v6

    .line 270
    if-lez v6, :cond_6

    .line 271
    const/4 v0, 0x0

    .line 272
    .local v0, "audioBytes":[B
    const/4 v1, 0x0

    .line 274
    .local v1, "audioBytesOffset":I
    iget-object v7, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynthesisBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 275
    iget-object v7, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynthesisBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 276
    iget-object v7, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynthesisBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v1

    .line 288
    :goto_1
    invoke-interface {p3, v0, v1, v6}, Landroid/speech/tts/SynthesisCallback;->audioAvailable([BII)I

    move-result v7

    if-eqz v7, :cond_6

    iget-boolean v7, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mStopRequested:Z

    if-nez v7, :cond_6

    .line 293
    sget-object v7, Lcom/google/android/tts/local/LocalSynthesizer;->TAG:Ljava/lang/String;

    const-string v9, "synthesizeMarkup call to callback.audioAvailable failed."

    invoke-static {v7, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    .end local v0    # "audioBytes":[B
    .end local v1    # "audioBytesOffset":I
    :cond_3
    :goto_2
    invoke-virtual {v3}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->destroy()V

    move v7, v8

    .line 304
    goto :goto_0

    .line 280
    .restart local v0    # "audioBytes":[B
    .restart local v1    # "audioBytesOffset":I
    :cond_4
    new-array v0, v6, [B

    .line 281
    const/4 v4, 0x0

    .line 282
    .local v4, "i":I
    :goto_3
    iget-object v7, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynthesisBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 283
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .local v5, "i":I
    iget-object v7, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynthesisBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->get()B

    move-result v7

    aput-byte v7, v0, v4

    move v4, v5

    .end local v5    # "i":I
    .restart local v4    # "i":I
    goto :goto_3

    .line 285
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 301
    .end local v0    # "audioBytes":[B
    .end local v1    # "audioBytesOffset":I
    .end local v4    # "i":I
    :cond_6
    invoke-virtual {v3}, Lcom/google/speech/patts/api/common/BufferedSynthesisHandle;->isInProgress()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-boolean v7, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mStopRequested:Z

    if-eqz v7, :cond_2

    goto :goto_2
.end method


# virtual methods
.method public enqueueDownloadByVoiceName(Ljava/lang/String;)V
    .locals 3
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 405
    new-instance v1, Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    iget-object v2, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    invoke-interface {v2}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;->getAllVoicesMetadata()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;-><init>(Ljava/util/List;)V

    invoke-virtual {v1}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->onlyNewestRevisions()Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->firstByName(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v0

    .line 409
    .local v0, "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    if-eqz v0, :cond_0

    .line 410
    iget-object v1, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v1, v0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->enoughFreeSpaceForInstall(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 411
    iget-object v1, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;->downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Z)Z

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 413
    :cond_1
    sget-object v1, Lcom/google/android/tts/local/LocalSynthesizer;->TAG:Ljava/lang/String;

    const-string v2, "Not enough space to initialize auto-download"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public enqueueVoiceDownloadByLanguage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 393
    iget-object v1, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->getAutoInstallVoiceFor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v0

    .line 395
    .local v0, "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    if-eqz v0, :cond_0

    .line 396
    iget-object v1, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v1, v0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->enoughFreeSpaceForInstall(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 397
    iget-object v1, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;->downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Z)Z

    .line 402
    :cond_0
    :goto_0
    return-void

    .line 399
    :cond_1
    sget-object v1, Lcom/google/android/tts/local/LocalSynthesizer;->TAG:Ljava/lang/String;

    const-string v2, "Not enough space to initialize auto-download"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized getLanguage()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 334
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mCurrentVoice:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    if-eqz v1, :cond_0

    .line 335
    iget-object v1, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mCurrentVoice:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    invoke-virtual {v1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLocale()Ljava/util/Locale;

    move-result-object v0

    .line 336
    .local v0, "locale":Ljava/util/Locale;
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, ""

    aput-object v3, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 338
    .end local v0    # "locale":Ljava/util/Locale;
    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 334
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getPreferedLocaleFor(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;
    .locals 3
    .param p1, "iso3Language"    # Ljava/lang/String;
    .param p2, "iso3Country"    # Ljava/lang/String;

    .prologue
    .line 499
    iget-object v2, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->getInstalledVoiceFor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    move-result-object v0

    .line 501
    .local v0, "installedVoice":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    if-eqz v0, :cond_0

    .line 502
    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLocale()Ljava/util/Locale;

    move-result-object v2

    .line 510
    :goto_0
    return-object v2

    .line 505
    :cond_0
    iget-object v2, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->getAutoInstallVoiceFor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v1

    .line 507
    .local v1, "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    if-eqz v1, :cond_1

    .line 508
    invoke-static {v1}, Lcom/google/android/tts/util/LocalesHelper;->createFromMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/util/Locale;

    move-result-object v2

    goto :goto_0

    .line 510
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getSupportedLocales()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .prologue
    .line 481
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 484
    .local v2, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Locale;>;"
    iget-object v4, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v4}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAvailableVoicesInfo()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 485
    .local v0, "avi":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 490
    .end local v0    # "avi":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    :cond_0
    iget-object v4, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v4}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAutoInstallVoicesMetadata()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 492
    .local v3, "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-static {v3}, Lcom/google/android/tts/util/LocalesHelper;->createFromMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/util/Locale;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 494
    .end local v3    # "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :cond_1
    return-object v2
.end method

.method public getVoices(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voices/InternalVoice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 430
    .local p1, "voicesList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/voices/InternalVoice;>;"
    iget-object v2, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v2}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAvailableVoicesInfo()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 431
    .local v9, "avi":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    new-instance v0, Lcom/google/android/tts/voices/InternalVoice;

    invoke-virtual {v9}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getQuality()I

    move-result v3

    invoke-virtual {v9}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLatency()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/tts/voices/InternalVoice;-><init>(Ljava/lang/String;Ljava/util/Locale;IIZLjava/util/Set;Lcom/google/android/tts/voices/VoiceSynthesizer;)V

    .line 434
    .local v0, "voiceInfo":Lcom/google/android/tts/voices/InternalVoice;
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 439
    .end local v0    # "voiceInfo":Lcom/google/android/tts/voices/InternalVoice;
    .end local v9    # "avi":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    :cond_0
    iget-object v2, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v2}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAutoInstallVoicesMetadata()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 441
    .local v11, "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 442
    .local v7, "features":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v2, "notInstalled"

    invoke-virtual {v7, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 444
    invoke-static {v11}, Lcom/google/android/tts/util/LocalesHelper;->createFromMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/util/Locale;

    move-result-object v3

    .line 445
    .local v3, "locale":Ljava/util/Locale;
    new-instance v1, Lcom/google/android/tts/voices/InternalVoice;

    invoke-virtual {v11}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getSynthesisQuality()I

    move-result v4

    invoke-virtual {v11}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getSynthesisLatency()I

    move-result v5

    const/4 v6, 0x0

    move-object v8, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/tts/voices/InternalVoice;-><init>(Ljava/lang/String;Ljava/util/Locale;IIZLjava/util/Set;Lcom/google/android/tts/voices/VoiceSynthesizer;)V

    .line 449
    .local v1, "voice":Lcom/google/android/tts/voices/InternalVoice;
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 451
    .end local v1    # "voice":Lcom/google/android/tts/voices/InternalVoice;
    .end local v3    # "locale":Ljava/util/Locale;
    .end local v7    # "features":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v11    # "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :cond_1
    return-void
.end method

.method public isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public isVoiceAvailableForDownloadByLanguage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 515
    iget-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->isVoiceAvailableForDownloadByLanguage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isVoiceAvailableForDownloadByName(Ljava/lang/String;)Z
    .locals 1
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    invoke-virtual {v0, p1}, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->isVoiceAvailableForDownloadByName(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized onClose()V
    .locals 2

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    iget-object v1, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mChangeListener:Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;

    invoke-virtual {v0, v1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->removeVoicesDataListener(Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;)V

    .line 120
    invoke-direct {p0}, Lcom/google/android/tts/local/LocalSynthesizer;->destroySynthesizer()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    monitor-exit p0

    return-void

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onInit()V
    .locals 1

    .prologue
    .line 129
    const/16 v0, 0x400

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynthesisBuf:Ljava/nio/ByteBuffer;

    .line 130
    return-void
.end method

.method public declared-synchronized onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x2

    .line 309
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 310
    .local v0, "availability":I
    if-eq v0, v2, :cond_0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    .line 329
    .end local v0    # "availability":I
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 316
    .restart local v0    # "availability":I
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->getInstalledVoiceFor(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    move-result-object v1

    .line 319
    .local v1, "voice":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    iget-object v3, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mCurrentVoice:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    invoke-virtual {v1, v3}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v3}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 324
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/tts/local/LocalSynthesizer;->initializeSynthesizer(Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;)V

    .line 325
    iget-object v3, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v3}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 326
    sget-object v3, Lcom/google/android/tts/local/LocalSynthesizer;->TAG:Ljava/lang/String;

    const-string v4, "onLoadLanguage : Synthesizer failed to initialize"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v2

    .line 327
    goto :goto_0

    .line 309
    .end local v0    # "availability":I
    .end local v1    # "voice":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized onLoadVoice(Lcom/google/android/tts/voices/InternalVoice;)Z
    .locals 1
    .param p1, "voice"    # Lcom/google/android/tts/voices/InternalVoice;

    .prologue
    .line 455
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/tts/voices/InternalVoice;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/tts/local/LocalSynthesizer;->onLoadVoice(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onLoadVoice(Ljava/lang/String;)Z
    .locals 4
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 459
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v3}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAvailableVoicesInfo()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 462
    .local v0, "installedVoice":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    if-nez v0, :cond_1

    .line 476
    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    .line 467
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mCurrentVoice:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    invoke-virtual {v0, v3}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v3}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_2

    move v1, v2

    .line 469
    goto :goto_0

    .line 472
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/tts/local/LocalSynthesizer;->initializeSynthesizer(Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;)V

    .line 473
    iget-object v3, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v3}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 476
    goto :goto_0

    .line 459
    .end local v0    # "installedVoice":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mStopRequested:Z

    .line 135
    return-void
.end method

.method public onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 1
    .param p1, "ttsRequest"    # Lcom/google/android/tts/service/GoogleTTSRequest;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "callback"    # Landroid/speech/tts/SynthesisCallback;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mStopRequested:Z

    .line 161
    invoke-virtual {p0, p1, p2}, Lcom/google/android/tts/local/LocalSynthesizer;->synthesizeBuffered(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V

    .line 162
    return-void
.end method

.method declared-synchronized synthesizeBuffered(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 10
    .param p1, "request"    # Lcom/google/android/tts/service/GoogleTTSRequest;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "callback"    # Landroid/speech/tts/SynthesisCallback;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getVoiceName()Ljava/lang/String;

    move-result-object v5

    .line 175
    .local v5, "voiceName":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 179
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Language()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Country()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/google/android/tts/local/LocalSynthesizer;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 182
    .local v2, "onLoadLanguageStatus":I
    if-gez v2, :cond_3

    .line 183
    sget-object v6, Lcom/google/android/tts/local/LocalSynthesizer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Synthesis request for unsupported locale: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Language()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Country()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    .line 186
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->done()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    .end local v2    # "onLoadLanguageStatus":I
    :goto_0
    monitor-exit p0

    return-void

    .line 191
    :cond_0
    :try_start_1
    invoke-virtual {p0, v5}, Lcom/google/android/tts/local/LocalSynthesizer;->onLoadVoice(Ljava/lang/String;)Z

    move-result v3

    .line 192
    .local v3, "onLoadVoiceStatus":Z
    if-nez v3, :cond_3

    .line 193
    invoke-virtual {p0, v5}, Lcom/google/android/tts/local/LocalSynthesizer;->isVoiceAvailableForDownloadByName(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 194
    invoke-virtual {p0, v5}, Lcom/google/android/tts/local/LocalSynthesizer;->enqueueDownloadByVoiceName(Ljava/lang/String;)V

    .line 198
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->isLocalOnly()Z

    move-result v6

    if-nez v6, :cond_2

    .line 199
    iget-object v6, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mFallbackSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v6, p1, p2}, Lcom/google/android/tts/voices/Synthesizer;->onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 174
    .end local v3    # "onLoadVoiceStatus":Z
    .end local v5    # "voiceName":Ljava/lang/String;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 201
    .restart local v3    # "onLoadVoiceStatus":Z
    .restart local v5    # "voiceName":Ljava/lang/String;
    :cond_2
    :try_start_2
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    .line 202
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->done()I

    goto :goto_0

    .line 211
    .end local v3    # "onLoadVoiceStatus":Z
    :cond_3
    iget-object v6, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v6}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->isInitialized()Z

    move-result v6

    if-nez v6, :cond_4

    .line 212
    sget-object v6, Lcom/google/android/tts/local/LocalSynthesizer;->TAG:Ljava/lang/String;

    const-string v7, "Synthesis requested before engine intialized: "

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    .line 214
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->done()I

    goto :goto_0

    .line 218
    :cond_4
    iget-object v6, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mSynth:Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;

    invoke-virtual {v6}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;->getSampleRate()I

    move-result v6

    const/4 v7, 0x2

    const/4 v8, 0x1

    invoke-interface {p2, v6, v7, v8}, Landroid/speech/tts/SynthesisCallback;->start(III)I

    move-result v6

    if-eqz v6, :cond_5

    .line 220
    sget-object v6, Lcom/google/android/tts/local/LocalSynthesizer;->TAG:Ljava/lang/String;

    const-string v7, "callback.start() failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    .line 222
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->done()I

    goto :goto_0

    .line 226
    :cond_5
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Locale()Ljava/util/Locale;

    move-result-object v7

    invoke-static {p1}, Lcom/google/android/tts/local/LocalSynthesizer;->createVoiceModV1(Lcom/google/android/tts/service/GoogleTTSRequest;)Lcom/google/speech/tts/VoiceMod;

    move-result-object v8

    const/4 v9, 0x1

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/tts/util/MarkupGenerator;->process(Ljava/lang/CharSequence;Ljava/util/Locale;Lcom/google/speech/tts/VoiceMod;Z)Ljava/util/List;

    move-result-object v1

    .line 229
    .local v1, "markupSentences":Ljava/util/List;, "Ljava/util/List<Lcom/google/speech/tts/Sentence;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/speech/tts/Sentence;

    .line 230
    .local v4, "sentence":Lcom/google/speech/tts/Sentence;
    iget-boolean v6, p0, Lcom/google/android/tts/local/LocalSynthesizer;->mStopRequested:Z

    if-eqz v6, :cond_8

    .line 239
    .end local v4    # "sentence":Lcom/google/speech/tts/Sentence;
    :cond_7
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->done()I

    goto/16 :goto_0

    .line 233
    .restart local v4    # "sentence":Lcom/google/speech/tts/Sentence;
    :cond_8
    invoke-direct {p0, p1, v4, p2}, Lcom/google/android/tts/local/LocalSynthesizer;->synthesizeMarkup(Lcom/google/android/tts/service/GoogleTTSRequest;Lcom/google/speech/tts/Sentence;Landroid/speech/tts/SynthesisCallback;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 234
    sget-object v6, Lcom/google/android/tts/local/LocalSynthesizer;->TAG:Ljava/lang/String;

    const-string v7, "synthesizeMarkup() failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
