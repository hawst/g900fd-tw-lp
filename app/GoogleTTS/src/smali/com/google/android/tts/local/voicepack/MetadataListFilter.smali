.class public Lcom/google/android/tts/local/voicepack/MetadataListFilter;
.super Ljava/lang/Object;
.source "MetadataListFilter.java"


# instance fields
.field private final mVoiceMetadataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "metadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->mVoiceMetadataList:Ljava/util/List;

    .line 24
    return-void
.end method


# virtual methods
.method public firstByName(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 3
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 122
    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->mVoiceMetadataList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 123
    .local v1, "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-virtual {v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 127
    .end local v1    # "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNameMap()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 114
    .local v2, "nameMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->mVoiceMetadataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 115
    .local v1, "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-virtual {v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 117
    .end local v1    # "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :cond_0
    return-object v2
.end method

.method public getUniqueLocales()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 59
    .local v2, "output":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->mVoiceMetadataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 60
    .local v1, "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-virtual {v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 62
    .end local v1    # "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :cond_0
    return-object v2
.end method

.method public onlyAutoInstalls()Lcom/google/android/tts/local/voicepack/MetadataListFilter;
    .locals 4

    .prologue
    .line 81
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v2, "output":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->mVoiceMetadataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 83
    .local v1, "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-virtual {v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getAutoInstall()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 84
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 87
    .end local v1    # "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :cond_1
    new-instance v3, Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    invoke-direct {v3, v2}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;-><init>(Ljava/util/List;)V

    return-object v3
.end method

.method public onlyNewestRevisions()Lcom/google/android/tts/local/voicepack/MetadataListFilter;
    .locals 8

    .prologue
    .line 92
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 94
    .local v1, "highestRevisions":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    iget-object v5, p0, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->mVoiceMetadataList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 95
    .local v3, "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-virtual {v3}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "fullName":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 97
    .local v4, "savedVoiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v5

    invoke-virtual {v3}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 98
    :cond_1
    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 101
    .end local v0    # "fullName":Ljava/lang/String;
    .end local v3    # "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .end local v4    # "savedVoiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :cond_2
    new-instance v5, Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v5, v6}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;-><init>(Ljava/util/List;)V

    return-object v5
.end method

.method public removeLocalesSet(Ljava/util/Set;)Lcom/google/android/tts/local/voicepack/MetadataListFilter;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/google/android/tts/local/voicepack/MetadataListFilter;"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "locales":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Locale;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v3, "output":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->mVoiceMetadataList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 71
    .local v2, "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-static {v2}, Lcom/google/android/tts/util/LocalesHelper;->createFromMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/util/Locale;

    move-result-object v1

    .line 72
    .local v1, "locale":Ljava/util/Locale;
    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 73
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 76
    .end local v1    # "locale":Ljava/util/Locale;
    .end local v2    # "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :cond_1
    new-instance v4, Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    invoke-direct {v4, v3}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;-><init>(Ljava/util/List;)V

    return-object v4
.end method

.method public selectForApkVersion(J)Lcom/google/android/tts/local/voicepack/MetadataListFilter;
    .locals 7
    .param p1, "versionCode"    # J

    .prologue
    .line 30
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 31
    .local v2, "output":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->mVoiceMetadataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 32
    .local v1, "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-virtual {v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getMinApkVersionCode()J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-gtz v3, :cond_0

    .line 33
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 36
    .end local v1    # "meta":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :cond_1
    new-instance v3, Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    invoke-direct {v3, v2}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;-><init>(Ljava/util/List;)V

    return-object v3
.end method
