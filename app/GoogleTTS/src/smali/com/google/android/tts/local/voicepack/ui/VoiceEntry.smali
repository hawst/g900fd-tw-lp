.class public final Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;
.super Ljava/lang/Object;
.source "VoiceEntry.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mFlags:I

.field private final mInstalledVoiceInfo:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

.field private final mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;I)V
    .locals 0
    .param p1, "voiceMetadata"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .param p2, "installedVoiceInfo"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    .param p3, "flags"    # I

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 62
    iput-object p2, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mInstalledVoiceInfo:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 63
    iput p3, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mFlags:I

    .line 64
    return-void
.end method

.method public static getAll(Landroid/content/Context;)Ljava/util/List;
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 178
    .local v19, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;>;"
    invoke-static/range {p0 .. p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v5

    .line 179
    .local v5, "app":Lcom/google/android/tts/service/GoogleTTSApplication;
    invoke-virtual {v5}, Lcom/google/android/tts/service/GoogleTTSApplication;->getTtsConfig()Lcom/google/android/tts/settings/TtsConfig;

    move-result-object v7

    .line 180
    .local v7, "config":Lcom/google/android/tts/settings/TtsConfig;
    invoke-virtual {v5}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataDownloader()Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    move-result-object v10

    .line 181
    .local v10, "downloader":Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;
    invoke-virtual {v5}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVersionCode()I

    move-result v6

    .line 184
    .local v6, "appVersionCode":I
    new-instance v22, Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    invoke-virtual {v5}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataDownloader()Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;->getAllVoicesMetadata()Ljava/util/List;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;-><init>(Ljava/util/List;)V

    int-to-long v0, v6

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->selectForApkVersion(J)Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->onlyNewestRevisions()Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->getNameMap()Ljava/util/Map;

    move-result-object v18

    .line 190
    .local v18, "metadataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    invoke-virtual {v5}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAvailableVoicesInfo()Ljava/util/Map;

    move-result-object v14

    .line 194
    .local v14, "installedVoices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    new-instance v4, Ljava/util/HashSet;

    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v4, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 195
    .local v4, "allVoicesSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v4, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 210
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 211
    .local v16, "localeVoiceCounters":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/util/Locale;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 212
    .local v21, "voiceName":Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 213
    .local v17, "metadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    move-object/from16 v0, v21

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 215
    .local v13, "installed":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    const/4 v15, 0x0

    .line 216
    .local v15, "locale":Ljava/util/Locale;
    if-eqz v17, :cond_0

    .line 217
    invoke-static/range {v17 .. v17}, Lcom/google/android/tts/util/LocalesHelper;->createFromMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/util/Locale;

    move-result-object v15

    .line 225
    :goto_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;

    .line 226
    .local v8, "count":Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;
    if-nez v8, :cond_3

    .line 227
    new-instance v23, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;

    if-eqz v13, :cond_2

    const/16 v22, 0x1

    :goto_2
    const/16 v24, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v22

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;-><init>(II)V

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v15, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 218
    .end local v8    # "count":Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;
    :cond_0
    if-eqz v13, :cond_1

    .line 219
    iget-object v15, v13, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    goto :goto_1

    .line 221
    :cond_1
    sget-object v22, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->TAG:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Couldn\'t find locale of voice "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 227
    .restart local v8    # "count":Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;
    :cond_2
    const/16 v22, 0x0

    goto :goto_2

    .line 230
    :cond_3
    iget v0, v8, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;->installedCount:I

    move/from16 v23, v0

    if-eqz v13, :cond_4

    const/16 v22, 0x1

    :goto_3
    add-int v22, v22, v23

    move/from16 v0, v22

    iput v0, v8, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;->installedCount:I

    .line 231
    iget v0, v8, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;->availableCount:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, 0x1

    move/from16 v0, v22

    iput v0, v8, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;->availableCount:I

    goto/16 :goto_0

    .line 230
    :cond_4
    const/16 v22, 0x0

    goto :goto_3

    .line 236
    .end local v8    # "count":Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;
    .end local v13    # "installed":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    .end local v15    # "locale":Ljava/util/Locale;
    .end local v17    # "metadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .end local v21    # "voiceName":Ljava/lang/String;
    :cond_5
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_c

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 237
    .restart local v21    # "voiceName":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 239
    .restart local v13    # "installed":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 242
    .restart local v17    # "metadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    if-eqz v17, :cond_a

    .line 243
    invoke-static/range {v17 .. v17}, Lcom/google/android/tts/util/LocalesHelper;->createFromMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/util/Locale;

    move-result-object v15

    .line 252
    .restart local v15    # "locale":Ljava/util/Locale;
    :goto_5
    const/4 v11, 0x0

    .line 253
    .local v11, "flags":I
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;

    .line 254
    .local v20, "voiceCounters":Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;
    move-object/from16 v0, v20

    iget v0, v0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;->installedCount:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_6

    .line 255
    or-int/lit8 v11, v11, 0x2

    .line 257
    :cond_6
    move-object/from16 v0, v20

    iget v0, v0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;->availableCount:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_7

    .line 258
    or-int/lit8 v11, v11, 0x1

    .line 261
    :cond_7
    invoke-virtual {v15}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v7, v0}, Lcom/google/android/tts/settings/TtsConfig;->getDefaultVoiceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 262
    .local v9, "defaultVoiceForLocale":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 263
    or-int/lit8 v11, v11, 0x8

    .line 265
    :cond_8
    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;->isActiveDownload(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 266
    or-int/lit8 v11, v11, 0x10

    .line 269
    :cond_9
    new-instance v22, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v13, v11}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;-><init>(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;I)V

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 244
    .end local v9    # "defaultVoiceForLocale":Ljava/lang/String;
    .end local v11    # "flags":I
    .end local v15    # "locale":Ljava/util/Locale;
    .end local v20    # "voiceCounters":Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1VoiceCounters;
    :cond_a
    if-eqz v13, :cond_b

    .line 245
    iget-object v15, v13, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    .line 246
    .restart local v15    # "locale":Ljava/util/Locale;
    iget-object v0, v13, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-object/from16 v17, v0

    goto :goto_5

    .line 248
    .end local v15    # "locale":Ljava/util/Locale;
    :cond_b
    sget-object v22, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->TAG:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Couldn\'t find locale of voice "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 273
    .end local v13    # "installed":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    .end local v17    # "metadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .end local v21    # "voiceName":Ljava/lang/String;
    :cond_c
    new-instance v22, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1;

    invoke-direct/range {v22 .. v22}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 291
    return-object v19
.end method

.method public static getDisplayName(Landroid/content/Context;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "voiceMetadata"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 76
    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getTag()I

    move-result v1

    .line 80
    .local v1, "tag":I
    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getGender()Ljava/lang/String;

    move-result-object v2

    const-string v3, "male"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    const v2, 0x7f060023

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "gender":Ljava/lang/String;
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 94
    const v2, 0x7f060028

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    return-object v2

    .line 82
    .end local v0    # "gender":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getGender()Ljava/lang/String;

    move-result-object v2

    const-string v3, "female"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 83
    const v2, 0x7f060022

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "gender":Ljava/lang/String;
    goto :goto_0

    .line 85
    .end local v0    # "gender":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getGender()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "gender":Ljava/lang/String;
    goto :goto_0

    .line 91
    :pswitch_0
    const v2, 0x7f060029

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static toLocaleSortedList(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .prologue
    .line 318
    .local p0, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;>;"
    new-instance v2, Ljava/util/TreeSet;

    new-instance v3, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$3;

    invoke-direct {v3}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$3;-><init>()V

    invoke-direct {v2, v3}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 325
    .local v2, "ret":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/util/Locale;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    .line 326
    .local v0, "entry":Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;
    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 328
    .end local v0    # "entry":Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v3
.end method

.method public static toLocaleSortedMap(Ljava/util/List;)Ljava/util/SortedMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;",
            ">;)",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/util/Locale;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 297
    .local p0, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;>;"
    new-instance v3, Ljava/util/TreeMap;

    new-instance v4, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$2;

    invoke-direct {v4}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$2;-><init>()V

    invoke-direct {v3, v4}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    .line 305
    .local v3, "ret":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/util/Locale;Ljava/util/List<Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;>;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    .line 306
    .local v0, "entry":Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;
    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 307
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;>;"
    if-nez v2, :cond_0

    .line 308
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 309
    .restart local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;>;"
    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    :cond_0
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 313
    .end local v0    # "entry":Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;>;"
    :cond_1
    return-object v3
.end method


# virtual methods
.method public getDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getVoiceMetadata()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getDisplayName(Landroid/content/Context;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInstalledVoice()Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mInstalledVoiceInfo:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-static {v0}, Lcom/google/android/tts/util/LocalesHelper;->createFromMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public getStatus(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isDownloading()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    const v1, 0x7f060027

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 121
    :goto_0
    return-object v1

    .line 105
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v0, "statuses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mInstalledVoiceInfo:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    if-eqz v1, :cond_2

    .line 107
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mInstalledVoiceInfo:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    invoke-virtual {v1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getInstalledSize()J

    move-result-wide v2

    invoke-static {p1, v2, v3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isUpdatable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 118
    const v1, 0x7f060024

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    :cond_1
    const-string v1, " - "

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 112
    :cond_2
    const v1, 0x7f060025

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v4}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getUnpackedSizeKb()I

    move-result v4

    mul-int/lit16 v4, v4, 0x400

    int-to-long v4, v4

    invoke-static {p1, v4, v5}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getVoiceMetadata()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    return-object v0
.end method

.method public isDefaultForLocale()Z
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mFlags:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDownloading()Z
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mFlags:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInstalled()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mInstalledVoiceInfo:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOneOfLocaleManyInstalledVoices()Z
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mFlags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRemovable()Z
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mInstalledVoiceInfo:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mInstalledVoiceInfo:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    iget v0, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUpdatable()Z
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mInstalledVoiceInfo:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mInstalledVoiceInfo:Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    iget-object v0, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isDownloading()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
