.class public interface abstract Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;
.super Ljava/lang/Object;
.source "VoiceEntryActionsListener.java"


# virtual methods
.method public abstract onCancelDownloadClick(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V
.end method

.method public abstract onDownloadClick(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V
.end method

.method public abstract onMakeCurrentClick(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V
.end method

.method public abstract onUninstallClick(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V
.end method

.method public abstract onUpdateClick(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V
.end method
