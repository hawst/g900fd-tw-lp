.class public interface abstract Lcom/google/android/tts/voices/Synthesizer;
.super Ljava/lang/Object;
.source "Synthesizer.java"


# virtual methods
.method public abstract getLanguage()[Ljava/lang/String;
.end method

.method public abstract getPreferedLocaleFor(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;
.end method

.method public abstract getSupportedLocales()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract onStop()V
.end method

.method public abstract onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V
.end method
