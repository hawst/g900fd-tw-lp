.class public Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask;
.super Landroid/os/AsyncTask;
.source "CheckVoiceDataAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/app/Activity;",
        "Ljava/lang/Void;",
        "Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 29
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/app/Activity;)Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;
    .locals 16
    .param p1, "params"    # [Landroid/app/Activity;

    .prologue
    .line 37
    const/4 v13, 0x0

    aget-object v4, p1, v13

    .line 39
    .local v4, "checkVoiceDataActivity":Landroid/app/Activity;
    invoke-static {v4}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v0

    .line 41
    .local v0, "app":Lcom/google/android/tts/service/GoogleTTSApplication;
    invoke-virtual {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v12

    .line 43
    .local v12, "voiceDataManager":Lcom/google/android/tts/local/voicepack/VoiceDataManager;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 45
    .local v1, "availableVoices":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceMetadataListManager()Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    move-result-object v8

    .line 46
    .local v8, "metadataManager":Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;
    invoke-virtual {v8}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->forceEvictionAndUpdate()V

    .line 48
    invoke-virtual {v12}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->checkData()V

    .line 51
    invoke-virtual {v12}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAvailableVoicesInfo()Ljava/util/Map;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 54
    .local v3, "avi":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    invoke-virtual {v3}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLegacyLocaleString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v1, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 58
    .end local v3    # "avi":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    :cond_0
    new-instance v13, Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    invoke-virtual {v8}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->getAllVoiceMetadataList()Ljava/util/List;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;-><init>(Ljava/util/List;)V

    invoke-virtual {v13}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->onlyAutoInstalls()Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    move-result-object v13

    invoke-virtual {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVersionCode()I

    move-result v14

    int-to-long v14, v14

    invoke-virtual {v13, v14, v15}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->selectForApkVersion(J)Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->getUniqueLocales()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 61
    .local v7, "localeStr":Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/tts/util/LocalesHelper;->createFromString(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v6

    .line 64
    .local v6, "locale":Ljava/util/Locale;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "-"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v6}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v1, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 69
    .end local v6    # "locale":Ljava/util/Locale;
    .end local v7    # "localeStr":Ljava/lang/String;
    :cond_1
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 70
    .local v10, "returnVal":Landroid/content/Intent;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 71
    .local v2, "availableVoicesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v13, "availableVoices"

    invoke-virtual {v10, v13, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 76
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 77
    .local v11, "unavailableVoicesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v13, ""

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    const-string v13, "unavailableVoices"

    invoke-virtual {v10, v13, v11}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 81
    new-instance v9, Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;

    invoke-direct {v9}, Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;-><init>()V

    .line 82
    .local v9, "output":Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;
    iput-object v4, v9, Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;->checkVoiceDataActivity:Landroid/app/Activity;

    .line 83
    iput-object v10, v9, Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;->outputIntent:Landroid/content/Intent;

    .line 84
    const/4 v13, 0x1

    iput v13, v9, Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;->returnCode:I

    .line 86
    return-object v9
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, [Landroid/app/Activity;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask;->doInBackground([Landroid/app/Activity;)Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;)V
    .locals 3
    .param p1, "output"    # Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;

    .prologue
    .line 92
    iget-object v0, p1, Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;->checkVoiceDataActivity:Landroid/app/Activity;

    iget v1, p1, Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;->returnCode:I

    iget-object v2, p1, Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;->outputIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 93
    iget-object v0, p1, Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;->checkVoiceDataActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 94
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask;->onPostExecute(Lcom/google/android/tts/settings/CheckVoiceDataAsyncTask$Output;)V

    return-void
.end method
