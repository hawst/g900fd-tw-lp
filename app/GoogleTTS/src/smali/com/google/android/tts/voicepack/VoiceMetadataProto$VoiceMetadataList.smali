.class public final Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "VoiceMetadataProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/voicepack/VoiceMetadataProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VoiceMetadataList"
.end annotation


# instance fields
.field private cachedSize:I

.field private data_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private hasRevision:Z

.field private revision_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 521
    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    .line 526
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->revision_:I

    .line 542
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->data_:Ljava/util/List;

    .line 599
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->cachedSize:I

    .line 521
    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    .prologue
    .line 655
    new-instance v0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    invoke-direct {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    check-cast v0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    return-object v0
.end method


# virtual methods
.method public addData(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    .locals 1
    .param p1, "value"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .prologue
    .line 559
    if-nez p1, :cond_0

    .line 560
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->data_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 563
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->data_:Ljava/util/List;

    .line 565
    :cond_1
    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->data_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 566
    return-object p0
.end method

.method public clearData()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    .locals 1

    .prologue
    .line 569
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->data_:Ljava/util/List;

    .line 570
    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    .prologue
    .line 602
    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->cachedSize:I

    if-gez v0, :cond_0

    .line 604
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->getSerializedSize()I

    .line 606
    :cond_0
    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->cachedSize:I

    return v0
.end method

.method public getDataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 545
    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->data_:Ljava/util/List;

    return-object v0
.end method

.method public getRevision()I
    .locals 1

    .prologue
    .line 527
    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->revision_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 611
    const/4 v2, 0x0

    .line 612
    .local v2, "size":I
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->hasRevision()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 613
    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->getRevision()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 616
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->getDataList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 617
    .local v0, "element":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    .line 619
    goto :goto_0

    .line 620
    .end local v0    # "element":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :cond_1
    iput v2, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->cachedSize:I

    .line 621
    return v2
.end method

.method public hasRevision()Z
    .locals 1

    .prologue
    .line 528
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->hasRevision:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 629
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    .line 630
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 634
    invoke-virtual {p0, p1, v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 635
    :sswitch_0
    return-object p0

    .line 640
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->setRevision(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    goto :goto_0

    .line 644
    :sswitch_2
    new-instance v1, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-direct {v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;-><init>()V

    .line 645
    .local v1, "value":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    .line 646
    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->addData(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    goto :goto_0

    .line 630
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 518
    invoke-virtual {p0, p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    move-result-object v0

    return-object v0
.end method

.method public setRevision(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 530
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->hasRevision:Z

    .line 531
    iput p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->revision_:I

    .line 532
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 591
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->hasRevision()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 592
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->getRevision()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    .line 594
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->getDataList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 595
    .local v0, "element":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    .line 597
    .end local v0    # "element":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :cond_1
    return-void
.end method
