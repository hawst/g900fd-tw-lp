.class public Lcom/google/android/tts/network/NetworkFallbackSynthesizer;
.super Ljava/lang/Object;
.source "NetworkFallbackSynthesizer.java"

# interfaces
.implements Lcom/google/android/tts/voices/Synthesizer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;
    }
.end annotation


# instance fields
.field private final mFallback:Lcom/google/android/tts/voices/Synthesizer;

.field private final mNetworkStatus:Lcom/google/android/tts/util/NetworkStatus;

.field private final mPrimary:Lcom/google/android/tts/voices/Synthesizer;


# direct methods
.method public constructor <init>(Lcom/google/android/tts/util/NetworkStatus;Lcom/google/android/tts/voices/Synthesizer;Lcom/google/android/tts/voices/Synthesizer;)V
    .locals 0
    .param p1, "networkStatus"    # Lcom/google/android/tts/util/NetworkStatus;
    .param p2, "primary"    # Lcom/google/android/tts/voices/Synthesizer;
    .param p3, "fallback"    # Lcom/google/android/tts/voices/Synthesizer;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p2, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer;->mPrimary:Lcom/google/android/tts/voices/Synthesizer;

    .line 30
    iput-object p3, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer;->mFallback:Lcom/google/android/tts/voices/Synthesizer;

    .line 31
    iput-object p1, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer;->mNetworkStatus:Lcom/google/android/tts/util/NetworkStatus;

    .line 32
    return-void
.end method

.method private doFallback(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 4
    .param p1, "ttsRequest"    # Lcom/google/android/tts/service/GoogleTTSRequest;
    .param p2, "callback"    # Landroid/speech/tts/SynthesisCallback;

    .prologue
    .line 132
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer;->mFallback:Lcom/google/android/tts/voices/Synthesizer;

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Language()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Country()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/tts/voices/Synthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 135
    .local v0, "langAvaiable":I
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer;->mFallback:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v1, p1, p2}, Lcom/google/android/tts/voices/Synthesizer;->onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_1
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    goto :goto_0
.end method


# virtual methods
.method public getLanguage()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer;->mFallback:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v0}, Lcom/google/android/tts/voices/Synthesizer;->getLanguage()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPreferedLocaleFor(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;
    .locals 1
    .param p1, "iso3Language"    # Ljava/lang/String;
    .param p2, "iso3Country"    # Ljava/lang/String;

    .prologue
    .line 166
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getSupportedLocales()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "country"    # Ljava/lang/String;
    .param p2, "language"    # Ljava/lang/String;

    .prologue
    .line 151
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "country"    # Ljava/lang/String;
    .param p2, "language"    # Ljava/lang/String;

    .prologue
    .line 146
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 115
    return-void
.end method

.method public onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 2
    .param p1, "ttsRequest"    # Lcom/google/android/tts/service/GoogleTTSRequest;
    .param p2, "callback"    # Landroid/speech/tts/SynthesisCallback;

    .prologue
    .line 119
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer;->mNetworkStatus:Lcom/google/android/tts/util/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/tts/util/NetworkStatus;->isNetworkConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 120
    new-instance v0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;

    invoke-direct {v0, p0, p2}, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;-><init>(Lcom/google/android/tts/network/NetworkFallbackSynthesizer;Landroid/speech/tts/SynthesisCallback;)V

    .line 122
    .local v0, "fallbackCallback":Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer;->mPrimary:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v1, p1, v0}, Lcom/google/android/tts/voices/Synthesizer;->onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V

    .line 123
    invoke-virtual {v0}, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->isSuccesful()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->isRecoverable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/network/NetworkFallbackSynthesizer;->doFallback(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V

    .line 129
    .end local v0    # "fallbackCallback":Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/network/NetworkFallbackSynthesizer;->doFallback(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V

    goto :goto_0
.end method
