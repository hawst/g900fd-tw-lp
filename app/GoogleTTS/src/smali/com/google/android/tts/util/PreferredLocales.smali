.class public Lcom/google/android/tts/util/PreferredLocales;
.super Ljava/lang/Object;
.source "PreferredLocales.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mLanguageToLocale:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mTtsDefaultLocale:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/android/tts/util/PreferredLocales;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/util/PreferredLocales;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/Locale;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefferedLocalesMappingsRes"    # I
    .param p3, "ttsDefaultLocale"    # Ljava/util/Locale;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/util/PreferredLocales;->mLanguageToLocale:Ljava/util/HashMap;

    .line 28
    iput-object p3, p0, Lcom/google/android/tts/util/PreferredLocales;->mTtsDefaultLocale:Ljava/util/Locale;

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/tts/util/PreferredLocales;->buildPreferredLocales([Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method private buildPreferredLocales([Ljava/lang/String;)V
    .locals 12
    .param p1, "prefferedLocalesMappings"    # [Ljava/lang/String;

    .prologue
    .line 33
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v7, v0, v1

    .line 34
    .local v7, "preferredLocale":Ljava/lang/String;
    const-string v9, "\\:"

    invoke-virtual {v7, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 35
    .local v8, "preferredLocaleSplit":[Ljava/lang/String;
    array-length v9, v8

    const/4 v10, 0x2

    if-eq v9, v10, :cond_0

    .line 36
    sget-object v9, Lcom/google/android/tts/util/PreferredLocales;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid preferred locales entry: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 39
    :cond_0
    const/4 v9, 0x0

    aget-object v4, v8, v9

    .line 40
    .local v4, "localeLanguage":Ljava/lang/String;
    const/4 v9, 0x1

    aget-object v5, v8, v9

    .line 42
    .local v5, "localeStr":Ljava/lang/String;
    const-string v9, "-"

    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 43
    .local v6, "localeStrSplit":[Ljava/lang/String;
    array-length v9, v6

    const/4 v10, 0x2

    if-eq v9, v10, :cond_1

    .line 44
    sget-object v9, Lcom/google/android/tts/util/PreferredLocales;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid preferred locales entry: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 47
    :cond_1
    new-instance v3, Ljava/util/Locale;

    const/4 v9, 0x0

    aget-object v9, v6, v9

    const/4 v10, 0x1

    aget-object v10, v6, v10

    invoke-direct {v3, v9, v10}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .local v3, "locale":Ljava/util/Locale;
    iget-object v9, p0, Lcom/google/android/tts/util/PreferredLocales;->mLanguageToLocale:Ljava/util/HashMap;

    invoke-virtual {v9, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 51
    .end local v3    # "locale":Ljava/util/Locale;
    .end local v4    # "localeLanguage":Ljava/lang/String;
    .end local v5    # "localeStr":Ljava/lang/String;
    .end local v6    # "localeStrSplit":[Ljava/lang/String;
    .end local v7    # "preferredLocale":Ljava/lang/String;
    .end local v8    # "preferredLocaleSplit":[Ljava/lang/String;
    :cond_2
    return-void
.end method


# virtual methods
.method public getPreffered(Ljava/lang/String;)Ljava/util/Locale;
    .locals 2
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/util/PreferredLocales;->mTtsDefaultLocale:Ljava/util/Locale;

    .line 61
    .local v0, "defaultLocale":Ljava/util/Locale;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    .end local v0    # "defaultLocale":Ljava/util/Locale;
    :goto_0
    return-object v0

    .line 65
    :catch_0
    move-exception v1

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/google/android/tts/util/PreferredLocales;->mLanguageToLocale:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    move-object v0, v1

    goto :goto_0
.end method

.method public updateTtsDefaultLocale(Ljava/util/Locale;)V
    .locals 0
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/tts/util/PreferredLocales;->mTtsDefaultLocale:Ljava/util/Locale;

    .line 55
    return-void
.end method
