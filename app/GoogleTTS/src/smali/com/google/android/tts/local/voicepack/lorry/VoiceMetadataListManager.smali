.class public Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;
.super Ljava/lang/Object;
.source "VoiceMetadataListManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mConfig:Lcom/google/android/tts/settings/TtsConfig;

.field private final mContext:Landroid/content/Context;

.field private final mFetchUrl:Ljava/lang/String;

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private mUpdateRunning:Z

.field private final mUrlRewriter:Lcom/google/android/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVoiceMetadataList:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/tts/settings/TtsConfig;Lcom/google/android/common/base/Function;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "config"    # Lcom/google/android/tts/settings/TtsConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/tts/settings/TtsConfig;",
            "Lcom/google/android/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p3, "urlRewriter":Lcom/google/android/common/base/Function;, "Lcom/google/android/common/base/Function<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mLock:Ljava/lang/Object;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mListeners:Ljava/util/List;

    .line 63
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mContext:Landroid/content/Context;

    .line 64
    iput-object p2, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    .line 65
    const/high16 v0, 0x7f060000

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mFetchUrl:Ljava/lang/String;

    .line 66
    iput-object p3, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mUrlRewriter:Lcom/google/android/common/base/Function;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mVoiceMetadataList:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    .line 68
    return-void
.end method

.method static emptyVoiceMetadataList()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    invoke-direct {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;-><init>()V

    .line 191
    .local v0, "list":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->setRevision(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    .line 192
    invoke-virtual {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->clearData()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    .line 193
    return-object v0
.end method


# virtual methods
.method public addChangeListener(Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;

    .prologue
    .line 71
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    monitor-exit v1

    .line 74
    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public forceChangeListenersCall()V
    .locals 6

    .prologue
    .line 84
    const/4 v3, 0x0

    .line 85
    .local v3, "listenersCopy":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;>;"
    const/4 v1, 0x0

    .line 87
    .local v1, "list":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    iget-object v5, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 88
    :try_start_0
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mListeners:Ljava/util/List;

    invoke-static {v4}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    .line 89
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mVoiceMetadataList:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    .line 90
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;

    .line 93
    .local v2, "listener":Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;
    invoke-interface {v2, v1}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;->onVoiceMetadataListChange(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)V

    goto :goto_0

    .line 90
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 95
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method public forceEvictionAndUpdate()V
    .locals 8

    .prologue
    .line 109
    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mLock:Ljava/lang/Object;

    monitor-enter v7

    .line 110
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mUpdateRunning:Z

    if-eqz v0, :cond_0

    .line 111
    monitor-exit v7

    .line 117
    :goto_0
    return-void

    .line 113
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mUpdateRunning:Z

    .line 114
    new-instance v0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mFetchUrl:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    iget-object v5, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mUrlRewriter:Lcom/google/android/common/base/Function;

    const/4 v6, 0x0

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;Lcom/google/android/tts/settings/TtsConfig;Lcom/google/android/common/base/Function;Z)V

    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->execute()V

    .line 116
    monitor-exit v7

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAllVoiceMetadataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->getVoiceMetadataListProto()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->getDataList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getVoiceMetadataByName(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 3
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->getVoiceMetadataListProto()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->getDataList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 131
    .local v1, "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-virtual {v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135
    .end local v1    # "voiceMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getVoiceMetadataListProto()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    .locals 4

    .prologue
    .line 139
    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 140
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mVoiceMetadataList:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 148
    :try_start_1
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    sget-object v1, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->TAG:Ljava/lang/String;

    const-string v3, "Interrupted waiting for metadata list fetch"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 155
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 154
    :cond_0
    :try_start_3
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mVoiceMetadataList:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v1
.end method

.method markUpdateComplete()V
    .locals 2

    .prologue
    .line 183
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 184
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mUpdateRunning:Z

    invoke-static {v0}, Lcom/google/android/common/base/Preconditions;->checkState(Z)V

    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mUpdateRunning:Z

    .line 186
    monitor-exit v1

    .line 187
    return-void

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setVoiceMetadataList(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)V
    .locals 6
    .param p1, "list"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    .prologue
    .line 160
    const/4 v2, 0x0

    .line 161
    .local v2, "listenersCopy":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;>;"
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 162
    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mUpdateRunning:Z

    invoke-static {v3}, Lcom/google/android/common/base/Preconditions;->checkState(Z)V

    .line 165
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mVoiceMetadataList:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mVoiceMetadataList:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    invoke-virtual {v3}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->getRevision()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->getRevision()I

    move-result v5

    if-ne v3, v5, :cond_1

    .line 167
    monitor-exit v4

    .line 179
    :cond_0
    return-void

    .line 170
    :cond_1
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mVoiceMetadataList:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    .line 171
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 173
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->mListeners:Ljava/util/List;

    invoke-static {v3}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    .line 174
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;

    .line 177
    .local v1, "listener":Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;
    invoke-interface {v1, p1}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;->onVoiceMetadataListChange(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)V

    goto :goto_0

    .line 174
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method
