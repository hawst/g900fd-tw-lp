.class public Lcom/google/android/tts/network/NetworkVoicesManager;
.super Ljava/lang/Object;
.source "NetworkVoicesManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    }
.end annotation


# instance fields
.field private final mDogfoodVoicesArray:[Ljava/lang/String;

.field private mISO3LocaleMap:Lcom/google/android/tts/util/ISO3LocaleMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/tts/util/ISO3LocaleMap",
            "<",
            "Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;",
            ">;"
        }
    .end annotation
.end field

.field private mLock:Ljava/lang/Object;

.field private mNetworkVoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;",
            ">;"
        }
    .end annotation
.end field

.field private final mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;

.field private final mProdVoicesArray:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILcom/google/android/tts/util/PreferredLocales;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "availableVoicesRes"    # I
    .param p3, "dogfoodVoicesRes"    # I
    .param p4, "preferredLocales"    # Lcom/google/android/tts/util/PreferredLocales;

    .prologue
    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p4}, Lcom/google/android/tts/network/NetworkVoicesManager;-><init>([Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/tts/util/PreferredLocales;)V

    .line 69
    return-void
.end method

.method protected constructor <init>([Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/tts/util/PreferredLocales;)V
    .locals 1
    .param p1, "releasedVoices"    # [Ljava/lang/String;
    .param p2, "dogfoodVoices"    # [Ljava/lang/String;
    .param p3, "preferredLocales"    # Lcom/google/android/tts/util/PreferredLocales;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mNetworkVoices:Ljava/util/ArrayList;

    .line 55
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mLock:Ljava/lang/Object;

    .line 74
    iput-object p3, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;

    .line 75
    iput-object p1, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mProdVoicesArray:[Ljava/lang/String;

    .line 76
    iput-object p2, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mDogfoodVoicesArray:[Ljava/lang/String;

    .line 77
    invoke-direct {p0}, Lcom/google/android/tts/network/NetworkVoicesManager;->buildData()V

    .line 78
    return-void
.end method

.method private addVoice(Lcom/google/android/tts/util/ISO3LocaleMap$Builder;Ljava/util/ArrayList;Ljava/lang/String;Z)V
    .locals 10
    .param p3, "voiceInfo"    # Ljava/lang/String;
    .param p4, "isDogfood"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/tts/util/ISO3LocaleMap$Builder",
            "<",
            "Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "builder":Lcom/google/android/tts/util/ISO3LocaleMap$Builder;, "Lcom/google/android/tts/util/ISO3LocaleMap$Builder<Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;>;"
    .local p2, "voicesArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;>;"
    const-string v7, ":"

    invoke-virtual {p3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 85
    .local v6, "voiceInfoFields":[Ljava/lang/String;
    array-length v7, v6

    const/4 v8, 0x2

    if-ge v7, v8, :cond_0

    .line 86
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Couldn\'t create network voice info from thisline: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 89
    :cond_0
    const/4 v7, 0x0

    aget-object v7, v6, v7

    invoke-static {v7}, Lcom/google/android/tts/util/LocalesHelper;->createFromString(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v3

    .line 90
    .local v3, "locale":Ljava/util/Locale;
    const/4 v7, 0x1

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 93
    .local v5, "quality":I
    const/16 v7, 0x1f4

    if-lt v5, v7, :cond_1

    const/4 v0, 0x1

    .line 95
    .local v0, "isGoogleVoice":Z
    :goto_0
    new-instance v4, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;

    invoke-direct {v4, v3, v5, p4, v0}, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;-><init>(Ljava/util/Locale;IZZ)V

    .line 96
    .local v4, "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    iget-object v7, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/tts/util/PreferredLocales;->getPreffered(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v2

    .line 100
    .local v2, "languagePreferredLocale":Ljava/util/Locale;
    if-eqz v2, :cond_2

    invoke-virtual {v2, v3}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v1, 0x1

    .line 102
    .local v1, "isPreferredForLanguage":Z
    :goto_1
    const/4 v8, 0x0

    if-eqz v1, :cond_3

    const/4 v7, 0x2

    :goto_2
    invoke-virtual {p1, v3, v8, v7, v4}, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;->addLocale(Ljava/util/Locale;IILjava/lang/Object;)V

    .line 103
    return-void

    .line 93
    .end local v0    # "isGoogleVoice":Z
    .end local v1    # "isPreferredForLanguage":Z
    .end local v2    # "languagePreferredLocale":Ljava/util/Locale;
    .end local v4    # "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 100
    .restart local v0    # "isGoogleVoice":Z
    .restart local v2    # "languagePreferredLocale":Ljava/util/Locale;
    .restart local v4    # "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 102
    .restart local v1    # "isPreferredForLanguage":Z
    :cond_3
    const/4 v7, 0x0

    goto :goto_2
.end method

.method private buildData()V
    .locals 8

    .prologue
    .line 106
    new-instance v1, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;

    invoke-direct {v1}, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;-><init>()V

    .line 108
    .local v1, "builder":Lcom/google/android/tts/util/ISO3LocaleMap$Builder;, "Lcom/google/android/tts/util/ISO3LocaleMap$Builder<Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v5, "voicesArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;>;"
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mProdVoicesArray:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 111
    .local v4, "voiceInfo":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-direct {p0, v1, v5, v4, v6}, Lcom/google/android/tts/network/NetworkVoicesManager;->addVoice(Lcom/google/android/tts/util/ISO3LocaleMap$Builder;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    .line 110
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 114
    .end local v4    # "voiceInfo":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mDogfoodVoicesArray:[Ljava/lang/String;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 115
    .restart local v4    # "voiceInfo":Ljava/lang/String;
    const/4 v6, 0x1

    invoke-direct {p0, v1, v5, v4, v6}, Lcom/google/android/tts/network/NetworkVoicesManager;->addVoice(Lcom/google/android/tts/util/ISO3LocaleMap$Builder;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    .line 114
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 118
    .end local v4    # "voiceInfo":Ljava/lang/String;
    :cond_1
    iget-object v7, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mLock:Ljava/lang/Object;

    monitor-enter v7

    .line 119
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/tts/util/ISO3LocaleMap$Builder;->build()Lcom/google/android/tts/util/ISO3LocaleMap;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mISO3LocaleMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    .line 120
    iput-object v5, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mNetworkVoices:Ljava/util/ArrayList;

    .line 121
    monitor-exit v7

    .line 122
    return-void

    .line 121
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method


# virtual methods
.method public checkData()V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/google/android/tts/network/NetworkVoicesManager;->buildData()V

    .line 126
    return-void
.end method

.method public getAvailableVoices()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 166
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mNetworkVoices:Ljava/util/ArrayList;

    monitor-exit v1

    return-object v0

    .line 167
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getLocale(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;
    .locals 3
    .param p1, "languageA3"    # Ljava/lang/String;
    .param p2, "countryA3"    # Ljava/lang/String;

    .prologue
    .line 135
    iget-object v2, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 136
    :try_start_0
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mISO3LocaleMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/tts/util/ISO3LocaleMap;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;

    .line 137
    .local v0, "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    if-nez v0, :cond_0

    .line 138
    const/4 v1, 0x0

    monitor-exit v2

    .line 140
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;->locale:Ljava/util/Locale;

    monitor-exit v2

    goto :goto_0

    .line 141
    .end local v0    # "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isDogfood(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "languageA3"    # Ljava/lang/String;
    .param p2, "countryA3"    # Ljava/lang/String;

    .prologue
    .line 145
    iget-object v2, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 146
    :try_start_0
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mISO3LocaleMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/tts/util/ISO3LocaleMap;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;

    .line 147
    .local v0, "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    if-nez v0, :cond_0

    .line 148
    const/4 v1, 0x0

    monitor-exit v2

    .line 150
    :goto_0
    return v1

    :cond_0
    iget-boolean v1, v0, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;->isDogfood:Z

    monitor-exit v2

    goto :goto_0

    .line 151
    .end local v0    # "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isGoogleVoice(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "languageA3"    # Ljava/lang/String;
    .param p2, "countryA3"    # Ljava/lang/String;

    .prologue
    .line 155
    iget-object v2, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 156
    :try_start_0
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mISO3LocaleMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/tts/util/ISO3LocaleMap;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;

    .line 157
    .local v0, "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    if-nez v0, :cond_0

    .line 158
    const/4 v1, 0x0

    monitor-exit v2

    .line 160
    :goto_0
    return v1

    :cond_0
    iget-boolean v1, v0, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;->isGoogleVoice:Z

    monitor-exit v2

    goto :goto_0

    .line 161
    .end local v0    # "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "languageA3"    # Ljava/lang/String;
    .param p2, "countryA3"    # Ljava/lang/String;

    .prologue
    .line 129
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 130
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkVoicesManager;->mISO3LocaleMap:Lcom/google/android/tts/util/ISO3LocaleMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/util/ISO3LocaleMap;->isLanguageAvailableV1(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
