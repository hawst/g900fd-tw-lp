.class public Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;
.super Landroid/app/Service;
.source "UnpackVoicePackService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mConcurrentVoicePacksCounter:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 30
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;->mConcurrentVoicePacksCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;->cleanup()V

    return-void
.end method

.method private cleanup()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;->mConcurrentVoicePacksCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-gtz v0, :cond_0

    .line 67
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;->stopForeground(Z)V

    .line 69
    :cond_0
    return-void
.end method

.method private startForeground()V
    .locals 4

    .prologue
    .line 52
    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;->mConcurrentVoicePacksCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 53
    .local v0, "counter":I
    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    .line 62
    :goto_0
    return-void

    .line 57
    :cond_0
    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v2, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f020009

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const v3, 0x7f060044

    invoke-virtual {p0, v3}, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 61
    .local v1, "notificationBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    const/16 v2, 0x40e3

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method private startProcessing(J)V
    .locals 5
    .param p1, "downloadId"    # J

    .prologue
    .line 39
    invoke-static {p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v0

    .line 40
    .local v0, "app":Lcom/google/android/tts/service/GoogleTTSApplication;
    new-instance v1, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;

    new-instance v2, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;

    invoke-direct {v2, p1, p2}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;-><init>(J)V

    new-instance v3, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService$1;

    invoke-direct {v3, p0}, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService$1;-><init>(Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;)V

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;-><init>(Lcom/google/android/tts/service/GoogleTTSApplication;Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask$Input;Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Lcom/google/android/tts/local/voicepack/lorry/ProcessVoicePackTask;->execute()V

    .line 48
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 34
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const-wide/16 v4, -0x1

    .line 73
    if-eqz p1, :cond_0

    .line 74
    const-string v2, "DOWNLOAD_ID"

    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 75
    .local v0, "downloadId":J
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 76
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;->startForeground()V

    .line 77
    invoke-direct {p0, v0, v1}, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;->startProcessing(J)V

    .line 82
    .end local v0    # "downloadId":J
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v2

    return v2

    .line 79
    .restart local v0    # "downloadId":J
    :cond_1
    sget-object v2, Lcom/google/android/tts/local/voicepack/lorry/UnpackVoicePackService;->TAG:Ljava/lang/String;

    const-string v3, "Missing intent extra parameter DOWNLOAD_ID"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
