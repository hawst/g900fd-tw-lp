.class public Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
.super Ljava/lang/Object;
.source "VoiceDataManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/local/voicepack/VoiceDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "InstalledVoiceInfo"
.end annotation


# instance fields
.field public final mDefaultForLanguage:Z

.field public final mLocale:Ljava/util/Locale;

.field public final mLocation:I

.field public final mPackageName:Ljava/lang/String;

.field public final mSize:J

.field public final mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

.field final synthetic this$0:Lcom/google/android/tts/local/voicepack/VoiceDataManager;


# direct methods
.method private constructor <init>(Lcom/google/android/tts/local/voicepack/VoiceDataManager;ILcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Ljava/lang/String;JZ)V
    .locals 3
    .param p2, "location"    # I
    .param p3, "metadata"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "size"    # J
    .param p7, "defaultForLanguage"    # Z

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->this$0:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput p2, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocation:I

    .line 111
    iput-object p3, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 112
    iput-object p4, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mPackageName:Ljava/lang/String;

    .line 113
    iput-wide p5, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mSize:J

    .line 114
    iput-boolean p7, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mDefaultForLanguage:Z

    .line 116
    invoke-static {p3}, Lcom/google/android/tts/util/LocalesHelper;->createFromMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    .line 118
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasName()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Voice metadata without a name (locale: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v2}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/tts/local/voicepack/VoiceDataManager;ILcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Ljava/lang/String;JZLcom/google/android/tts/local/voicepack/VoiceDataManager$1;)V
    .locals 1
    .param p1, "x0"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # J
    .param p7, "x5"    # Z
    .param p8, "x6"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager$1;

    .prologue
    .line 89
    invoke-direct/range {p0 .. p7}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;-><init>(Lcom/google/android/tts/local/voicepack/VoiceDataManager;ILcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Ljava/lang/String;JZ)V

    return-void
.end method

.method private getOuterType()Lcom/google/android/tts/local/voicepack/VoiceDataManager;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->this$0:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    return-object v0
.end method

.method private getVoiceDirName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 203
    if-ne p0, p1, :cond_1

    .line 233
    :cond_0
    :goto_0
    return v1

    .line 205
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 206
    goto :goto_0

    .line 207
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 208
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 209
    check-cast v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 210
    .local v0, "other":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getOuterType()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v3

    invoke-direct {v0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getOuterType()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 211
    goto :goto_0

    .line 212
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mDefaultForLanguage:Z

    iget-boolean v4, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mDefaultForLanguage:Z

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 213
    goto :goto_0

    .line 214
    :cond_5
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    if-nez v3, :cond_6

    .line 215
    iget-object v3, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    if-eqz v3, :cond_7

    move v1, v2

    .line 216
    goto :goto_0

    .line 217
    :cond_6
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    iget-object v4, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 218
    goto :goto_0

    .line 219
    :cond_7
    iget v3, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocation:I

    iget v4, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocation:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 220
    goto :goto_0

    .line 221
    :cond_8
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mPackageName:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 222
    iget-object v3, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mPackageName:Ljava/lang/String;

    if-eqz v3, :cond_a

    move v1, v2

    .line 223
    goto :goto_0

    .line 224
    :cond_9
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mPackageName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 225
    goto :goto_0

    .line 226
    :cond_a
    iget-wide v4, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mSize:J

    iget-wide v6, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mSize:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_b

    move v1, v2

    .line 227
    goto :goto_0

    .line 228
    :cond_b
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    if-nez v3, :cond_c

    .line 229
    iget-object v3, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    if-eqz v3, :cond_0

    move v1, v2

    .line 230
    goto :goto_0

    .line 231
    :cond_c
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    iget-object v4, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-static {v3, v4}, Lcom/google/android/tts/local/voicepack/MetadataHelper;->simpleEqual(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 232
    goto :goto_0
.end method

.method public getAbsoluteFilePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocation:I

    packed-switch v0, :pswitch_data_0

    .line 144
    # getter for: Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Unknown voice location"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    const-string v0, ""

    :goto_0
    return-object v0

    .line 135
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->this$0:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    # getter for: Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mPattsSystemDir:Ljava/io/File;
    invoke-static {v1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->access$000(Lcom/google/android/tts/local/voicepack/VoiceDataManager;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getVoiceDirName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 138
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->this$0:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    # getter for: Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mPattsDataDir:Ljava/io/File;
    invoke-static {v1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->access$100(Lcom/google/android/tts/local/voicepack/VoiceDataManager;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getVoiceDirName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 141
    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "voices"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getVoiceDirName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 133
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getAssetManager()Landroid/content/res/AssetManager;
    .locals 2

    .prologue
    .line 155
    iget v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocation:I

    packed-switch v0, :pswitch_data_0

    .line 161
    # getter for: Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Unknown voice location"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 159
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->this$0:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    # getter for: Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->access$300(Lcom/google/android/tts/local/voicepack/VoiceDataManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    goto :goto_0

    .line 155
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getInstalledSize()J
    .locals 2

    .prologue
    .line 237
    iget-wide v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mSize:J

    return-wide v0
.end method

.method public getLatency()I
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getSynthesisLatency()I

    move-result v0

    return v0
.end method

.method public getLegacyLocaleString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getQuality()I
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getSynthesisQuality()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 188
    const/16 v0, 0x1f

    .line 189
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 190
    .local v1, "result":I
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getOuterType()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/lit8 v1, v2, 0x1f

    .line 191
    mul-int/lit8 v4, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mDefaultForLanguage:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x4cf

    :goto_0
    add-int v1, v4, v2

    .line 192
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 193
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocation:I

    add-int v1, v2, v4

    .line 194
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mPackageName:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 195
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mSize:J

    iget-wide v6, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mSize:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v1, v2, v4

    .line 196
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    if-nez v4, :cond_3

    :goto_3
    add-int v1, v2, v3

    .line 198
    return v1

    .line 191
    :cond_0
    const/16 v2, 0x4d5

    goto :goto_0

    .line 192
    :cond_1
    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->hashCode()I

    move-result v2

    goto :goto_1

    .line 194
    :cond_2
    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 196
    :cond_3
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-static {v3}, Lcom/google/android/tts/local/voicepack/MetadataHelper;->simpleHashCode(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)I

    move-result v3

    goto :goto_3
.end method

.method public isInApk()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 150
    iget v1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mLocation:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
