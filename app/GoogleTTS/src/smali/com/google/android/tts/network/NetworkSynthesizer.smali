.class public Lcom/google/android/tts/network/NetworkSynthesizer;
.super Ljava/lang/Object;
.source "NetworkSynthesizer.java"

# interfaces
.implements Lcom/google/android/tts/voices/VoiceSynthesizer;


# instance fields
.field private final mConfig:Lcom/google/android/tts/settings/TtsConfig;

.field private mCurrentLocale:[Ljava/lang/String;

.field private mFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/android/tts/network/ByteArrayHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final mHttpHelper:Lcom/google/android/tts/network/HttpHelper;

.field private final mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

.field private mService:Ljava/util/concurrent/ExecutorService;

.field private final mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

.field private final mStateLock:Ljava/lang/Object;

.field private mStopRequested:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/tts/settings/TtsConfig;Lcom/google/android/tts/network/NetworkVoicesManager;Lcom/google/android/common/base/Function;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "config"    # Lcom/google/android/tts/settings/TtsConfig;
    .param p3, "networkVoices"    # Lcom/google/android/tts/network/NetworkVoicesManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/tts/settings/TtsConfig;",
            "Lcom/google/android/tts/network/NetworkVoicesManager;",
            "Lcom/google/android/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p4, "urlRewriter":Lcom/google/android/common/base/Function;, "Lcom/google/android/common/base/Function<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStateLock:Ljava/lang/Object;

    .line 85
    iput-object p3, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    .line 86
    iput-object p2, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    .line 87
    new-instance v0, Lcom/google/android/tts/network/HttpHelper;

    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-direct {v0, p4, v1, v2}, Lcom/google/android/tts/network/HttpHelper;-><init>(Lcom/google/android/common/base/Function;Lcom/google/android/tts/network/NetworkVoicesManager;Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mHttpHelper:Lcom/google/android/tts/network/HttpHelper;

    .line 88
    new-instance v0, Lcom/google/android/tts/network/BufferedSpeexDecoder;

    invoke-direct {v0, p1}, Lcom/google/android/tts/network/BufferedSpeexDecoder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    .line 90
    return-void
.end method

.method private decodeAndWriteData([BLandroid/speech/tts/SynthesisCallback;)V
    .locals 8
    .param p1, "bytes"    # [B
    .param p2, "callback"    # Landroid/speech/tts/SynthesisCallback;

    .prologue
    .line 243
    iget-object v6, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStateLock:Ljava/lang/Object;

    monitor-enter v6

    .line 244
    :try_start_0
    iget-boolean v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    if-eqz v5, :cond_0

    .line 245
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    .line 246
    monitor-exit v6

    .line 287
    :goto_0
    return-void

    .line 248
    :cond_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    sget-object v5, Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;->WB:Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;

    invoke-virtual {v5}, Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;->getRate()I

    move-result v5

    const/4 v6, 0x2

    const/4 v7, 0x1

    invoke-interface {p2, v5, v6, v7}, Landroid/speech/tts/SynthesisCallback;->start(III)I

    move-result v5

    if-eqz v5, :cond_1

    .line 252
    const-string v5, "NetworkSynthesizer"

    const-string v6, "callback.start() failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    .line 254
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->done()I

    goto :goto_0

    .line 248
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 259
    :cond_1
    iget-object v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    sget-object v6, Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;->WB:Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;

    const/16 v7, 0xa

    invoke-virtual {v5, v6, v7}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->startSynthesis(Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;I)V

    .line 262
    :try_start_2
    iget-object v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    invoke-virtual {v5}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->getOutputBufferSize()I

    move-result v5

    new-array v0, v5, [B

    .line 263
    .local v0, "buffer":[B
    iget-object v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    invoke-virtual {v5}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->getInputBufferSize()I

    move-result v4

    .line 264
    .local v4, "frameSize":I
    const/4 v3, 0x0

    .line 266
    .local v3, "count":I
    :cond_2
    array-length v5, p1

    if-ge v3, v5, :cond_3

    .line 267
    array-length v5, p1

    sub-int/2addr v5, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 268
    .local v2, "bytesToRead":I
    iget-object v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    invoke-virtual {v5, p1, v3, v2, v0}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->getNextChunk([BII[B)I

    move-result v1

    .line 269
    .local v1, "bytesDecoded":I
    if-lez v1, :cond_4

    .line 270
    add-int/2addr v3, v2

    .line 271
    const/4 v5, 0x0

    invoke-interface {p2, v0, v5, v1}, Landroid/speech/tts/SynthesisCallback;->audioAvailable([BII)I

    move-result v5

    if-eqz v5, :cond_2

    .line 273
    const-string v5, "NetworkSynthesizer"

    const-string v6, "callback.audioAvailable() failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    .line 283
    .end local v1    # "bytesDecoded":I
    .end local v2    # "bytesToRead":I
    :cond_3
    :goto_1
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->done()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 285
    iget-object v5, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    invoke-virtual {v5}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->endSynthesis()V

    goto :goto_0

    .line 278
    .restart local v1    # "bytesDecoded":I
    .restart local v2    # "bytesToRead":I
    :cond_4
    :try_start_3
    const-string v5, "NetworkSynthesizer"

    const-string v6, "speex decoder didn\'t return any bytes"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 285
    .end local v0    # "buffer":[B
    .end local v1    # "bytesDecoded":I
    .end local v2    # "bytesToRead":I
    .end local v3    # "count":I
    .end local v4    # "frameSize":I
    :catchall_1
    move-exception v5

    iget-object v6, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mSpeexDecoder:Lcom/google/android/tts/network/BufferedSpeexDecoder;

    invoke-virtual {v6}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->endSynthesis()V

    throw v5
.end method

.method private setCurrentLocale([Ljava/lang/String;)V
    .locals 4
    .param p1, "iso3locale"    # [Ljava/lang/String;

    .prologue
    .line 315
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 316
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mCurrentLocale:[Ljava/lang/String;

    if-eqz v1, :cond_0

    aget-object v1, p1, v0

    iget-object v2, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mCurrentLocale:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 317
    :cond_0
    const-string v1, "NetworkSynthesizer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changing network TTS locale to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_1
    iput-object p1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mCurrentLocale:[Ljava/lang/String;

    .line 323
    return-void

    .line 315
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getLanguage()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mCurrentLocale:[Ljava/lang/String;

    return-object v0
.end method

.method public getPreferedLocaleFor(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;
    .locals 1
    .param p1, "iso3Language"    # Ljava/lang/String;
    .param p2, "iso3Country"    # Ljava/lang/String;

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/network/NetworkVoicesManager;->getLocale(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedLocales()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .prologue
    .line 369
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 371
    .local v2, "results":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/util/Locale;>;"
    iget-object v3, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    invoke-virtual {v3}, Lcom/google/android/tts/network/NetworkVoicesManager;->getAvailableVoices()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;

    .line 372
    .local v1, "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    iget-object v3, v1, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;->locale:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 374
    .end local v1    # "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    :cond_0
    return-object v2
.end method

.method public getVoices(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voices/InternalVoice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 337
    .local p1, "voicesList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/voices/InternalVoice;>;"
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    invoke-virtual {v1}, Lcom/google/android/tts/network/NetworkVoicesManager;->getAvailableVoices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;

    .line 338
    .local v9, "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 339
    .local v6, "features":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v1, "networkRetriesCount"

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 340
    const-string v1, "networkTimeoutMs"

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 342
    new-instance v0, Lcom/google/android/tts/voices/InternalVoice;

    iget-object v1, v9, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v9, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;->locale:Ljava/util/Locale;

    iget v3, v9, Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;->quality:I

    const/16 v4, 0x190

    const/4 v5, 0x1

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/tts/voices/InternalVoice;-><init>(Ljava/lang/String;Ljava/util/Locale;IIZLjava/util/Set;Lcom/google/android/tts/voices/VoiceSynthesizer;)V

    .line 345
    .local v0, "voice":Lcom/google/android/tts/voices/InternalVoice;
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 347
    .end local v0    # "voice":Lcom/google/android/tts/voices/InternalVoice;
    .end local v6    # "features":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v9    # "networkVoice":Lcom/google/android/tts/network/NetworkVoicesManager$NetworkVoice;
    :cond_0
    return-void
.end method

.method public isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/network/NetworkVoicesManager;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public onClose()V
    .locals 5

    .prologue
    .line 98
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 103
    :try_start_0
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mService:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v2, 0x5

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    const-string v1, "NetworkSynthesizer"

    const-string v2, "Network tasks did not terminate within timeout."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "ie":Ljava/lang/InterruptedException;
    const-string v1, "NetworkSynthesizer"

    const-string v2, "Thread interrupted while waiting for tasks to complete."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onInit()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mService:Ljava/util/concurrent/ExecutorService;

    .line 95
    return-void
.end method

.method public onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 303
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/tts/network/NetworkVoicesManager;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 304
    .local v0, "available":I
    if-nez v0, :cond_1

    .line 305
    new-array v1, v5, [Ljava/lang/String;

    aput-object p1, v1, v2

    const-string v2, ""

    aput-object v2, v1, v3

    const-string v2, ""

    aput-object v2, v1, v4

    invoke-direct {p0, v1}, Lcom/google/android/tts/network/NetworkSynthesizer;->setCurrentLocale([Ljava/lang/String;)V

    .line 310
    :cond_0
    :goto_0
    return v0

    .line 306
    :cond_1
    if-ne v0, v3, :cond_0

    .line 307
    new-array v1, v5, [Ljava/lang/String;

    aput-object p1, v1, v2

    aput-object p2, v1, v3

    const-string v2, ""

    aput-object v2, v1, v4

    invoke-direct {p0, v1}, Lcom/google/android/tts/network/NetworkSynthesizer;->setCurrentLocale([Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onLoadVoice(Lcom/google/android/tts/voices/InternalVoice;)Z
    .locals 6
    .param p1, "voice"    # Lcom/google/android/tts/voices/InternalVoice;

    .prologue
    const/4 v5, 0x1

    .line 351
    const-string v1, ""

    .line 353
    .local v1, "language":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/tts/voices/InternalVoice;->getLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 357
    :goto_0
    const-string v0, ""

    .line 359
    .local v0, "country":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/tts/voices/InternalVoice;->getLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 363
    :goto_1
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    aput-object v0, v2, v5

    const/4 v3, 0x2

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-direct {p0, v2}, Lcom/google/android/tts/network/NetworkSynthesizer;->setCurrentLocale([Ljava/lang/String;)V

    .line 364
    return v5

    .line 360
    :catch_0
    move-exception v2

    goto :goto_1

    .line 354
    .end local v0    # "country":Ljava/lang/String;
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 291
    iget-object v1, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 294
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mFuture:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mFuture:Ljava/util/concurrent/Future;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 297
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    .line 298
    monitor-exit v1

    .line 299
    return-void

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 10
    .param p1, "ttsRequest"    # Lcom/google/android/tts/service/GoogleTTSRequest;
    .param p2, "callback"    # Landroid/speech/tts/SynthesisCallback;

    .prologue
    const/4 v9, 0x0

    .line 177
    iget-object v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStateLock:Ljava/lang/Object;

    monitor-enter v7

    .line 182
    const/4 v6, 0x0

    :try_start_0
    iput-boolean v6, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    .line 183
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getTimeout()I

    move-result v0

    .line 186
    .local v0, "delayMs":I
    if-gez v0, :cond_0

    .line 187
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->isNetworkOnly()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 188
    iget-object v6, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-interface {v6}, Lcom/google/android/tts/settings/TtsConfig;->getDefaultTimeoutNetworkOnly()I

    move-result v0

    .line 194
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getRetryCount()I

    move-result v4

    .line 195
    .local v4, "retryCount":I
    if-gez v4, :cond_1

    .line 196
    const/4 v4, 0x0

    .line 200
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Language()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Country()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/google/android/tts/network/NetworkSynthesizer;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x2

    if-ne v6, v7, :cond_3

    .line 207
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    .line 208
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->done()I

    .line 236
    :goto_1
    return-void

    .line 183
    .end local v0    # "delayMs":I
    .end local v4    # "retryCount":I
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 190
    .restart local v0    # "delayMs":I
    :cond_2
    iget-object v6, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-interface {v6}, Lcom/google/android/tts/settings/TtsConfig;->getDefaultTimeoutNetworkFirst()I

    move-result v0

    goto :goto_0

    .line 212
    .restart local v4    # "retryCount":I
    :cond_3
    new-instance v1, Lcom/google/android/tts/network/NetworkRequest;

    invoke-direct {v1}, Lcom/google/android/tts/network/NetworkRequest;-><init>()V

    .line 213
    .local v1, "networkRequest":Lcom/google/android/tts/network/NetworkRequest;
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/tts/network/NetworkRequest;->text:Ljava/lang/String;

    .line 214
    iget-object v6, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Language()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Country()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/tts/network/NetworkVoicesManager;->isGoogleVoice(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 216
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Locale()Ljava/util/Locale;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/tts/util/MarkupGenerator;->process(Ljava/lang/CharSequence;Ljava/util/Locale;Lcom/google/speech/tts/VoiceMod;Z)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/speech/tts/Sentence;

    .line 218
    .local v5, "sentence":Lcom/google/speech/tts/Sentence;
    invoke-static {}, Lcom/google/speech/patts/PattsRequest;->newBuilder()Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/google/speech/patts/PattsRequest$Builder;->setTtsSentence(Lcom/google/speech/tts/Sentence;)Lcom/google/speech/patts/PattsRequest$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/speech/patts/PattsRequest$Builder;->build()Lcom/google/speech/patts/PattsRequest;

    move-result-object v2

    .line 219
    .local v2, "pattsRequest":Lcom/google/speech/patts/PattsRequest;
    invoke-static {}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;->newBuilder()Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;

    move-result-object v6

    sget-object v7, Lcom/google/speech/patts/PattsRequestProto;->pattsRequest:Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;

    invoke-virtual {v6, v7, v2}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;->setExtension(Lcom/google/protobuf/GeneratedMessageLite$GeneratedExtension;Ljava/lang/Object;)Lcom/google/protobuf/GeneratedMessageLite$ExtendableBuilder;

    move-result-object v6

    check-cast v6, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;

    invoke-virtual {v6}, Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest$Builder;->build()Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    move-result-object v3

    .line 223
    .local v3, "requestProto":Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;
    iput-object v3, v1, Lcom/google/android/tts/network/NetworkRequest;->proto:Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;

    .line 226
    .end local v2    # "pattsRequest":Lcom/google/speech/patts/PattsRequest;
    .end local v3    # "requestProto":Lcom/google/speech/synthesizer/SynthesisEngineSpecificRequest;
    .end local v5    # "sentence":Lcom/google/speech/tts/Sentence;
    :cond_4
    iput v0, v1, Lcom/google/android/tts/network/NetworkRequest;->networkTimeoutMs:I

    .line 227
    iget-object v6, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Language()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Country()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/tts/network/NetworkVoicesManager;->getLocale(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/tts/network/NetworkRequest;->locale:Ljava/util/Locale;

    .line 229
    iget-object v6, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Language()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Country()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/tts/network/NetworkVoicesManager;->isDogfood(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, v1, Lcom/google/android/tts/network/NetworkRequest;->isDogfood:Z

    .line 231
    iput v4, v1, Lcom/google/android/tts/network/NetworkRequest;->networkRetryCount:I

    .line 232
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getSpeechDurationMultiplier()F

    move-result v6

    iput v6, v1, Lcom/google/android/tts/network/NetworkRequest;->durationMultiplier:F

    .line 233
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getAndroidPitch()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x42c80000    # 100.0f

    div-float/2addr v6, v7

    iput v6, v1, Lcom/google/android/tts/network/NetworkRequest;->pitchMultiplier:F

    .line 235
    invoke-virtual {p0, v1, p2, v9}, Lcom/google/android/tts/network/NetworkSynthesizer;->synthesizeText(Lcom/google/android/tts/network/NetworkRequest;Landroid/speech/tts/SynthesisCallback;I)V

    goto/16 :goto_1
.end method

.method public synthesizeText(Lcom/google/android/tts/network/NetworkRequest;Landroid/speech/tts/SynthesisCallback;I)V
    .locals 12
    .param p1, "request"    # Lcom/google/android/tts/network/NetworkRequest;
    .param p2, "callback"    # Landroid/speech/tts/SynthesisCallback;
    .param p3, "attempt"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v11, -0x4

    const/4 v4, 0x0

    .line 112
    iget v7, p1, Lcom/google/android/tts/network/NetworkRequest;->networkRetryCount:I

    if-le p3, v7, :cond_0

    .line 171
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStateLock:Ljava/lang/Object;

    monitor-enter v7

    .line 117
    :try_start_0
    iget-boolean v8, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    if-eqz v8, :cond_1

    .line 118
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    .line 119
    monitor-exit v7

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 121
    :cond_1
    :try_start_1
    iget-object v8, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mService:Ljava/util/concurrent/ExecutorService;

    new-instance v9, Lcom/google/android/tts/network/NetworkFetchTask;

    iget-object v10, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mHttpHelper:Lcom/google/android/tts/network/HttpHelper;

    invoke-direct {v9, p1, v10}, Lcom/google/android/tts/network/NetworkFetchTask;-><init>(Lcom/google/android/tts/network/NetworkRequest;Lcom/google/android/tts/network/HttpHelper;)V

    invoke-interface {v8, v9}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mFuture:Ljava/util/concurrent/Future;

    .line 123
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 125
    iget v7, p1, Lcom/google/android/tts/network/NetworkRequest;->networkRetryCount:I

    if-ltz v7, :cond_2

    add-int/lit8 v7, p3, 0x1

    iget v8, p1, Lcom/google/android/tts/network/NetworkRequest;->networkRetryCount:I

    if-gt v7, v8, :cond_2

    move v4, v6

    .line 127
    .local v4, "retry":Z
    :cond_2
    const/4 v2, 0x0

    .line 129
    .local v2, "holder":Lcom/google/android/tts/network/ByteArrayHolder;
    :try_start_2
    iget-object v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mFuture:Ljava/util/concurrent/Future;

    iget v8, p1, Lcom/google/android/tts/network/NetworkRequest;->networkTimeoutMs:I

    int-to-long v8, v8

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v7, v8, v9, v10}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "holder":Lcom/google/android/tts/network/ByteArrayHolder;
    check-cast v2, Lcom/google/android/tts/network/ByteArrayHolder;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_3

    .line 166
    .restart local v2    # "holder":Lcom/google/android/tts/network/ByteArrayHolder;
    if-eqz v2, :cond_6

    .line 167
    invoke-virtual {v2}, Lcom/google/android/tts/network/ByteArrayHolder;->get()[B

    move-result-object v6

    invoke-direct {p0, v6, p2}, Lcom/google/android/tts/network/NetworkSynthesizer;->decodeAndWriteData([BLandroid/speech/tts/SynthesisCallback;)V

    goto :goto_0

    .line 130
    .end local v2    # "holder":Lcom/google/android/tts/network/ByteArrayHolder;
    :catch_0
    move-exception v3

    .line 131
    .local v3, "ie":Ljava/lang/InterruptedException;
    invoke-static {p2, v11}, Lcom/google/android/tts/util/CompatHelper;->callbackError(Landroid/speech/tts/SynthesisCallback;I)V

    goto :goto_0

    .line 133
    .end local v3    # "ie":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 134
    .local v1, "ee":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v6

    instance-of v6, v6, Ljava/io/IOException;

    if-eqz v6, :cond_4

    .line 135
    if-eqz v4, :cond_3

    .line 136
    add-int/lit8 v6, p3, 0x1

    invoke-virtual {p0, p1, p2, v6}, Lcom/google/android/tts/network/NetworkSynthesizer;->synthesizeText(Lcom/google/android/tts/network/NetworkRequest;Landroid/speech/tts/SynthesisCallback;I)V

    goto :goto_0

    .line 139
    :cond_3
    const/4 v6, -0x6

    invoke-static {p2, v6}, Lcom/google/android/tts/util/CompatHelper;->callbackError(Landroid/speech/tts/SynthesisCallback;I)V

    goto :goto_0

    .line 143
    :cond_4
    invoke-static {p2, v11}, Lcom/google/android/tts/util/CompatHelper;->callbackError(Landroid/speech/tts/SynthesisCallback;I)V

    goto :goto_0

    .line 146
    .end local v1    # "ee":Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v5

    .line 149
    .local v5, "te":Ljava/util/concurrent/TimeoutException;
    iget-object v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mFuture:Ljava/util/concurrent/Future;

    invoke-interface {v7, v6}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 150
    if-eqz v4, :cond_5

    .line 151
    add-int/lit8 v6, p3, 0x1

    invoke-virtual {p0, p1, p2, v6}, Lcom/google/android/tts/network/NetworkSynthesizer;->synthesizeText(Lcom/google/android/tts/network/NetworkRequest;Landroid/speech/tts/SynthesisCallback;I)V

    goto :goto_0

    .line 154
    :cond_5
    const/4 v6, -0x7

    invoke-static {p2, v6}, Lcom/google/android/tts/util/CompatHelper;->callbackError(Landroid/speech/tts/SynthesisCallback;I)V

    goto :goto_0

    .line 158
    .end local v5    # "te":Ljava/util/concurrent/TimeoutException;
    :catch_3
    move-exception v0

    .line 160
    .local v0, "ce":Ljava/util/concurrent/CancellationException;
    iget-object v7, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStateLock:Ljava/lang/Object;

    monitor-enter v7

    .line 161
    const/4 v6, 0x0

    :try_start_3
    iput-boolean v6, p0, Lcom/google/android/tts/network/NetworkSynthesizer;->mStopRequested:Z

    .line 162
    monitor-exit v7

    goto :goto_0

    :catchall_1
    move-exception v6

    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v6

    .line 170
    .end local v0    # "ce":Ljava/util/concurrent/CancellationException;
    .restart local v2    # "holder":Lcom/google/android/tts/network/ByteArrayHolder;
    :cond_6
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    goto/16 :goto_0
.end method
