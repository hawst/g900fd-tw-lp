.class public Lcom/google/android/tts/service/PattsJniLoader;
.super Ljava/lang/Object;
.source "PattsJniLoader.java"


# static fields
.field private static sNativeLibraryDirPrefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/tts/service/PattsJniLoader;->sNativeLibraryDirPrefix:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cleanOtherWorkaroundJniFiles(Landroid/content/Context;Ljava/lang/String;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "libraryName"    # Ljava/lang/String;

    .prologue
    .line 224
    invoke-static {p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVersionCode()I

    move-result v7

    .line 225
    .local v7, "versionCode":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\\.([0-9]+)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v8

    .line 227
    .local v8, "versionNumber":Ljava/util/regex/Pattern;
    const-string v9, "pm_workaround"

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v10}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    .line 228
    .local v5, "pmWorkaroundDir":Ljava/io/File;
    if-nez v5, :cond_1

    .line 242
    :cond_0
    return-void

    .line 231
    :cond_1
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 232
    .local v1, "child":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 233
    .local v4, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 234
    const/4 v9, 0x1

    invoke-virtual {v4, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 235
    .local v6, "version":I
    if-eq v6, v7, :cond_2

    .line 236
    const-string v9, "patts"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Removing old workaround library "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 231
    .end local v6    # "version":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static getUnbunbledLibraryName(ILjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "versionCode"    # I
    .param p1, "libraryName"    # Ljava/lang/String;

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_ub"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getWorkaroundJniFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "libraryName"    # Ljava/lang/String;

    .prologue
    .line 216
    invoke-static {p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVersionCode()I

    move-result v1

    .line 217
    .local v1, "versionCode":I
    invoke-static {p1}, Ljava/lang/System;->mapLibraryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 218
    .local v0, "libname":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    const-string v3, "pm_workaround"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v2
.end method

.method private static initializeNativeLibraryDirPrefix(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    sget-object v4, Lcom/google/android/tts/service/PattsJniLoader;->sNativeLibraryDirPrefix:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 61
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    .line 64
    .local v2, "nativeLibDir":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 68
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 69
    .local v3, "packageName":Ljava/lang/String;
    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 70
    .local v1, "idx":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    .line 74
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/tts/service/PattsJniLoader;->sNativeLibraryDirPrefix:Ljava/lang/String;

    goto :goto_0
.end method

.method public static loadJni(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "libraryName"    # Ljava/lang/String;

    .prologue
    .line 35
    const-class v3, Lcom/google/android/tts/service/PattsJniLoader;

    monitor-enter v3

    .line 37
    :try_start_0
    invoke-static {p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVersionCode()I

    move-result v1

    .line 38
    .local v1, "versionCode":I
    invoke-static {v1, p1}, Lcom/google/android/tts/service/PattsJniLoader;->getUnbunbledLibraryName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "fullLibraryName":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/tts/service/PattsJniLoader;->initializeNativeLibraryDirPrefix(Landroid/content/Context;)V

    .line 40
    invoke-static {p0, v0}, Lcom/google/android/tts/service/PattsJniLoader;->cleanOtherWorkaroundJniFiles(Landroid/content/Context;Ljava/lang/String;)V

    .line 42
    invoke-static {p0, v0}, Lcom/google/android/tts/service/PattsJniLoader;->tryToLoadPattsJni(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 43
    invoke-static {p0, v0}, Lcom/google/android/tts/service/PattsJniLoader;->tryUnpackAndLoadWorkaroundPattsJni(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 44
    const/4 v2, 0x0

    monitor-exit v3

    .line 47
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    monitor-exit v3

    goto :goto_0

    .line 51
    .end local v0    # "fullLibraryName":Ljava/lang/String;
    .end local v1    # "versionCode":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static tryToLoadPattsJni(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "libraryName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 109
    invoke-static {p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVersionCode()I

    move-result v4

    .line 112
    .local v4, "versionCode":I
    :try_start_0
    invoke-static {p1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :goto_0
    return v5

    .line 114
    :catch_0
    move-exception v3

    .line 115
    .local v3, "ule":Ljava/lang/UnsatisfiedLinkError;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_1
    const/4 v6, 0x2

    if-gt v1, v6, :cond_1

    .line 116
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/google/android/tts/service/PattsJniLoader;->sNativeLibraryDirPrefix:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p1}, Ljava/lang/System;->mapLibraryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "libPath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 119
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 120
    const-string v6, "patts"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Loading library "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-static {v2}, Ljava/lang/System;->load(Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 125
    .end local v0    # "f":Ljava/io/File;
    .end local v2    # "libPath":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private static tryUnpackAndLoadWorkaroundPattsJni(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "libraryName"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 139
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    invoke-static/range {p0 .. p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVersionCode()I

    move-result v12

    .line 141
    .local v12, "versionCode":I
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "lib/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static/range {p1 .. p1}, Ljava/lang/System;->mapLibraryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 143
    .local v8, "jniNameInApk":Ljava/lang/String;
    invoke-static/range {p0 .. p1}, Lcom/google/android/tts/service/PattsJniLoader;->getWorkaroundJniFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v11

    .line 146
    .local v11, "outputFile":Ljava/io/File;
    :try_start_0
    new-instance v6, Ljava/util/zip/ZipFile;

    new-instance v13, Ljava/io/File;

    iget-object v14, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v14, 0x1

    invoke-direct {v6, v13, v14}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;I)V

    .line 147
    .local v6, "file":Ljava/util/zip/ZipFile;
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v4

    .line 148
    .local v4, "entries":Ljava/util/Enumeration;, "Ljava/util/Enumeration<+Ljava/util/zip/ZipEntry;>;"
    :cond_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 149
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/zip/ZipEntry;

    .line 150
    .local v5, "entry":Ljava/util/zip/ZipEntry;
    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 154
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 155
    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v14

    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->getSize()J

    move-result-wide v16

    cmp-long v13, v14, v16

    if-nez v13, :cond_3

    .line 177
    .end local v5    # "entry":Ljava/util/zip/ZipEntry;
    :cond_1
    :goto_0
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->close()V

    .line 178
    const/4 v13, 0x1

    invoke-virtual {v11, v13}, Ljava/io/File;->setExecutable(Z)Z

    .line 179
    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Ljava/io/File;->setWritable(Z)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    const-string v13, "patts"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Loading library "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/System;->load(Ljava/lang/String;)V

    .line 192
    const/4 v13, 0x1

    .end local v4    # "entries":Ljava/util/Enumeration;, "Ljava/util/Enumeration<+Ljava/util/zip/ZipEntry;>;"
    .end local v6    # "file":Ljava/util/zip/ZipFile;
    :goto_1
    return v13

    .line 159
    .restart local v4    # "entries":Ljava/util/Enumeration;, "Ljava/util/Enumeration<+Ljava/util/zip/ZipEntry;>;"
    .restart local v5    # "entry":Ljava/util/zip/ZipEntry;
    .restart local v6    # "file":Ljava/util/zip/ZipFile;
    :cond_2
    :try_start_1
    invoke-virtual {v11}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 161
    :cond_3
    const/4 v7, 0x0

    .line 162
    .local v7, "is":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 164
    .local v9, "os":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v6, v5}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v7

    .line 165
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 166
    .end local v9    # "os":Ljava/io/FileOutputStream;
    .local v10, "os":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 167
    .local v2, "count":I
    const/16 v13, 0x1000

    :try_start_3
    new-array v1, v13, [B

    .line 168
    .local v1, "buffer":[B
    :goto_2
    invoke-virtual {v7, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-lez v2, :cond_7

    .line 169
    const/4 v13, 0x0

    invoke-virtual {v10, v1, v13, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 172
    .end local v1    # "buffer":[B
    :catchall_0
    move-exception v13

    move-object v9, v10

    .end local v2    # "count":I
    .end local v10    # "os":Ljava/io/FileOutputStream;
    .restart local v9    # "os":Ljava/io/FileOutputStream;
    :goto_3
    if-eqz v7, :cond_4

    :try_start_4
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 173
    :cond_4
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    :cond_5
    throw v13
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 180
    .end local v4    # "entries":Ljava/util/Enumeration;, "Ljava/util/Enumeration<+Ljava/util/zip/ZipEntry;>;"
    .end local v5    # "entry":Ljava/util/zip/ZipEntry;
    .end local v6    # "file":Ljava/util/zip/ZipFile;
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v9    # "os":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v3

    .line 181
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 182
    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    .line 184
    :cond_6
    const-string v13, "patts"

    const-string v14, "Failed to unpack missing JNI library"

    invoke-static {v13, v14, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 186
    const/4 v13, 0x0

    goto :goto_1

    .line 172
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v1    # "buffer":[B
    .restart local v2    # "count":I
    .restart local v4    # "entries":Ljava/util/Enumeration;, "Ljava/util/Enumeration<+Ljava/util/zip/ZipEntry;>;"
    .restart local v5    # "entry":Ljava/util/zip/ZipEntry;
    .restart local v6    # "file":Ljava/util/zip/ZipFile;
    .restart local v7    # "is":Ljava/io/InputStream;
    .restart local v10    # "os":Ljava/io/FileOutputStream;
    :cond_7
    if-eqz v7, :cond_8

    :try_start_5
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 173
    :cond_8
    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_0

    .line 172
    .end local v1    # "buffer":[B
    .end local v2    # "count":I
    .end local v10    # "os":Ljava/io/FileOutputStream;
    .restart local v9    # "os":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v13

    goto :goto_3
.end method
