.class public Lcom/google/android/tts/settings/GetSampleText;
.super Landroid/app/Activity;
.source "GetSampleText.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, -0x2

    .line 20
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 23
    .local v3, "resultIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/tts/settings/GetSampleText;->getIntent()Landroid/content/Intent;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/google/android/tts/settings/GetSampleText;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/google/android/tts/settings/GetSampleText;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "language"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 26
    :cond_0
    invoke-virtual {p0, v7, v3}, Lcom/google/android/tts/settings/GetSampleText;->setResult(ILandroid/content/Intent;)V

    .line 27
    invoke-virtual {p0}, Lcom/google/android/tts/settings/GetSampleText;->finish()V

    .line 63
    :goto_0
    return-void

    .line 31
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/tts/settings/GetSampleText;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "language"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 33
    .local v4, "searchedLanguage":Ljava/lang/String;
    const/4 v0, 0x0

    .line 34
    .local v0, "found":Z
    invoke-virtual {p0}, Lcom/google/android/tts/settings/GetSampleText;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050003

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    .line 36
    .local v6, "strings":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/tts/settings/GetSampleText;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050004

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "langs":[Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 40
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_5

    .line 41
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v8, v6

    if-ge v1, v8, :cond_2

    .line 42
    aget-object v8, v2, v1

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 43
    const-string v8, "sampleText"

    aget-object v9, v6, v1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 44
    const/4 v0, 0x1

    .line 59
    .end local v1    # "i":I
    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    const/4 v7, 0x0

    :cond_3
    invoke-virtual {p0, v7, v3}, Lcom/google/android/tts/settings/GetSampleText;->setResult(ILandroid/content/Intent;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/tts/settings/GetSampleText;->finish()V

    goto :goto_0

    .line 41
    .restart local v1    # "i":I
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 48
    .end local v1    # "i":I
    :cond_5
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x3

    if-ne v8, v9, :cond_2

    .line 49
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    array-length v8, v6

    if-ge v1, v8, :cond_2

    .line 50
    new-instance v5, Ljava/util/Locale;

    aget-object v8, v2, v1

    invoke-direct {v5, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 51
    .local v5, "stringLocale":Ljava/util/Locale;
    invoke-virtual {v5}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 52
    const-string v8, "sampleText"

    aget-object v9, v6, v1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const/4 v0, 0x1

    .line 54
    goto :goto_2

    .line 49
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method
