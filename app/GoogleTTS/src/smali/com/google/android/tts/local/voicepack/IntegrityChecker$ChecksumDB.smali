.class Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;
.super Ljava/lang/Object;
.source "IntegrityChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/local/voicepack/IntegrityChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ChecksumDB"
.end annotation


# instance fields
.field private final mChecksums:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[B>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "isAsset"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;->mChecksums:Ljava/util/HashMap;

    .line 38
    if-eqz p3, :cond_0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;->loadFromAssetFile(Landroid/content/Context;Ljava/lang/String;)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;->loadFromFile(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getKeyFor(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p1, "voiceName"    # Ljava/lang/String;
    .param p2, "revision"    # I

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private loadFromAssetFile(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;->loadFromStream(Ljava/io/InputStream;)V

    .line 47
    return-void
.end method

.method private loadFromFile(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;->loadFromStream(Ljava/io/InputStream;)V

    .line 50
    return-void
.end method

.method private loadFromStream(Ljava/io/InputStream;)V
    .locals 13
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-direct {v10, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v7, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 60
    .local v7, "reader":Ljava/io/BufferedReader;
    :goto_0
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 61
    .local v4, "line":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 91
    return-void

    .line 65
    :cond_0
    const-string v10, ","

    invoke-virtual {v4, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "csv":[Ljava/lang/String;
    array-length v10, v0

    const/4 v11, 0x4

    if-ge v10, v11, :cond_1

    .line 67
    # getter for: Lcom/google/android/tts/local/voicepack/IntegrityChecker;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->access$000()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unparsable line in file with voice data checksums: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 70
    :cond_1
    const/4 v10, 0x0

    aget-object v9, v0, v10

    .line 71
    .local v9, "voiceName":Ljava/lang/String;
    const/4 v8, 0x0

    .line 73
    .local v8, "revision":I
    const/4 v10, 0x1

    :try_start_0
    aget-object v10, v0, v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 77
    :goto_1
    const/4 v10, 0x2

    aget-object v2, v0, v10

    .line 78
    .local v2, "filename":Ljava/lang/String;
    const/4 v10, 0x3

    aget-object v5, v0, v10

    .line 80
    .local v5, "md5":Ljava/lang/String;
    invoke-direct {p0, v9, v8}, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;->getKeyFor(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    .line 82
    .local v6, "packRevisionKey":Ljava/lang/String;
    const/4 v3, 0x0

    .line 83
    .local v3, "files":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[B>;"
    iget-object v10, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;->mChecksums:Ljava/util/HashMap;

    invoke-virtual {v10, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 84
    iget-object v10, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;->mChecksums:Ljava/util/HashMap;

    invoke-virtual {v10, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "files":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[B>;"
    check-cast v3, Ljava/util/HashMap;

    .line 89
    .restart local v3    # "files":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[B>;"
    :goto_2
    invoke-static {v5}, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->fromHexString(Ljava/lang/String;)[B

    move-result-object v10

    invoke-virtual {v3, v2, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 74
    .end local v2    # "filename":Ljava/lang/String;
    .end local v3    # "files":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[B>;"
    .end local v5    # "md5":Ljava/lang/String;
    .end local v6    # "packRevisionKey":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 75
    .local v1, "e":Ljava/lang/NumberFormatException;
    # getter for: Lcom/google/android/tts/local/voicepack/IntegrityChecker;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->access$000()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to parse revision number in line: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 86
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    .restart local v2    # "filename":Ljava/lang/String;
    .restart local v3    # "files":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[B>;"
    .restart local v5    # "md5":Ljava/lang/String;
    .restart local v6    # "packRevisionKey":Ljava/lang/String;
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    .end local v3    # "files":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[B>;"
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 87
    .restart local v3    # "files":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[B>;"
    iget-object v10, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;->mChecksums:Ljava/util/HashMap;

    invoke-virtual {v10, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method


# virtual methods
.method getFiles(Ljava/lang/String;I)Ljava/util/Map;
    .locals 4
    .param p1, "voiceName"    # Ljava/lang/String;
    .param p2, "revision"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;->mChecksums:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/local/voicepack/IntegrityChecker$ChecksumDB;->getKeyFor(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 99
    .local v0, "ret":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[B>;"
    if-eqz v0, :cond_0

    .line 100
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .end local v0    # "ret":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[B>;"
    .local v1, "ret":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[B>;"
    move-object v0, v1

    .line 102
    .end local v1    # "ret":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[B>;"
    .restart local v0    # "ret":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[B>;"
    :cond_0
    return-object v0
.end method
