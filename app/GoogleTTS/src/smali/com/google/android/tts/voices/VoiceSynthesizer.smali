.class public interface abstract Lcom/google/android/tts/voices/VoiceSynthesizer;
.super Ljava/lang/Object;
.source "VoiceSynthesizer.java"

# interfaces
.implements Lcom/google/android/tts/voices/Synthesizer;


# virtual methods
.method public abstract getVoices(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voices/InternalVoice;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onLoadVoice(Lcom/google/android/tts/voices/InternalVoice;)Z
.end method
