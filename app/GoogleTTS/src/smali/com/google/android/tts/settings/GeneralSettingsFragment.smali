.class public Lcom/google/android/tts/settings/GeneralSettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "GeneralSettingsFragment.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 17
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const/high16 v0, 0x7f040000

    invoke-virtual {p0, v0}, Lcom/google/android/tts/settings/GeneralSettingsFragment;->addPreferencesFromResource(I)V

    .line 19
    invoke-virtual {p0}, Lcom/google/android/tts/settings/GeneralSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/settings/GeneralSettingsFragment;->mContext:Landroid/content/Context;

    .line 20
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/tts/settings/GeneralSettingsFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceMetadataListManager()Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->forceChangeListenersCall()V

    .line 28
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 29
    return-void
.end method
