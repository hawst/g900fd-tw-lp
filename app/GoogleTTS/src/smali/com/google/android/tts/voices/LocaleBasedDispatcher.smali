.class public Lcom/google/android/tts/voices/LocaleBasedDispatcher;
.super Ljava/lang/Object;
.source "LocaleBasedDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mDownloader:Lcom/google/android/tts/util/DownloadEnabler;

.field private final mHybridSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

.field private volatile mLanguage:[Ljava/lang/String;

.field private final mLocalSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

.field private final mNetworkStatus:Lcom/google/android/tts/util/NetworkStatus;

.field private final mNetworkSynthesizer:Lcom/google/android/tts/voices/Synthesizer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/tts/util/NetworkStatus;Lcom/google/android/tts/voices/Synthesizer;Lcom/google/android/tts/voices/Synthesizer;Lcom/google/android/tts/util/DownloadEnabler;)V
    .locals 3
    .param p1, "networkStatus"    # Lcom/google/android/tts/util/NetworkStatus;
    .param p2, "localSynth"    # Lcom/google/android/tts/voices/Synthesizer;
    .param p3, "networkSynth"    # Lcom/google/android/tts/voices/Synthesizer;
    .param p4, "downloader"    # Lcom/google/android/tts/util/DownloadEnabler;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p3, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    .line 54
    iput-object p2, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLocalSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    .line 55
    new-instance v0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer;

    iget-object v1, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    iget-object v2, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLocalSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/tts/network/NetworkFallbackSynthesizer;-><init>(Lcom/google/android/tts/util/NetworkStatus;Lcom/google/android/tts/voices/Synthesizer;Lcom/google/android/tts/voices/Synthesizer;)V

    iput-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mHybridSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    .line 57
    iput-object p4, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mDownloader:Lcom/google/android/tts/util/DownloadEnabler;

    .line 59
    iput-object p1, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkStatus:Lcom/google/android/tts/util/NetworkStatus;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/voices/LocaleBasedDispatcher;)Lcom/google/android/tts/voices/Synthesizer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLocalSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/tts/voices/LocaleBasedDispatcher;)Lcom/google/android/tts/voices/Synthesizer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/tts/voices/LocaleBasedDispatcher;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/voices/LocaleBasedDispatcher;
    .param p1, "x1"    # Ljava/util/Locale;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->getVoiceNameForLocale(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getVoiceNameForLocale(Ljava/util/Locale;)Ljava/lang/String;
    .locals 2
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-locale"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleEmptySynthesis(Landroid/speech/tts/SynthesisCallback;)V
    .locals 3
    .param p1, "cb"    # Landroid/speech/tts/SynthesisCallback;

    .prologue
    .line 175
    sget-object v0, Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;->WB:Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;

    invoke-virtual {v0}, Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;->getRate()I

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-interface {p1, v0, v1, v2}, Landroid/speech/tts/SynthesisCallback;->start(III)I

    .line 176
    invoke-interface {p1}, Landroid/speech/tts/SynthesisCallback;->done()I

    .line 177
    return-void
.end method


# virtual methods
.method public createVoiceSynthesizer()Lcom/google/android/tts/voices/VoiceSynthesizer;
    .locals 1

    .prologue
    .line 212
    new-instance v0, Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;

    invoke-direct {v0, p0}, Lcom/google/android/tts/voices/LocaleBasedDispatcher$VoiceSynthesizerAdapter;-><init>(Lcom/google/android/tts/voices/LocaleBasedDispatcher;)V

    return-object v0
.end method

.method public getDefaultVoiceNameFor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "iso3Language"    # Ljava/lang/String;
    .param p2, "iso3Country"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 185
    invoke-virtual {p0, p1, p2}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->selectSynthesizerByCapability(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/tts/voices/Synthesizer;

    move-result-object v1

    .line 186
    .local v1, "synthesizer":Lcom/google/android/tts/voices/Synthesizer;
    if-eqz v1, :cond_0

    .line 187
    invoke-interface {v1, p1, p2}, Lcom/google/android/tts/voices/Synthesizer;->getPreferedLocaleFor(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    .line 188
    .local v0, "locale":Ljava/util/Locale;
    if-nez v0, :cond_1

    .line 193
    .end local v0    # "locale":Ljava/util/Locale;
    :cond_0
    :goto_0
    return-object v2

    .line 191
    .restart local v0    # "locale":Ljava/util/Locale;
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->getVoiceNameForLocale(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method getHybridDelegate()Lcom/google/android/tts/voices/Synthesizer;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mHybridSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    return-object v0
.end method

.method public getLanguage()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLanguage:[Ljava/lang/String;

    return-object v0
.end method

.method public isDownloadTrigger(Lcom/google/android/tts/service/GoogleTTSRequest;)Z
    .locals 3
    .param p1, "request"    # Lcom/google/android/tts/service/GoogleTTSRequest;

    .prologue
    .line 122
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->isNetworkOnly()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mDownloader:Lcom/google/android/tts/util/DownloadEnabler;

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Country()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/tts/util/DownloadEnabler;->isVoiceAvailableForDownloadByLanguage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 156
    iget-object v2, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLocalSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v2, p1, p2}, Lcom/google/android/tts/voices/Synthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 158
    .local v0, "availableLocally":I
    iget-object v2, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v2, p1, p2}, Lcom/google/android/tts/voices/Synthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 161
    .local v1, "availableNetwork":I
    iget-object v2, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkStatus:Lcom/google/android/tts/util/NetworkStatus;

    invoke-interface {v2}, Lcom/google/android/tts/util/NetworkStatus;->isNetworkConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .end local v0    # "availableLocally":I
    :cond_0
    return v0
.end method

.method public onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 131
    iget-object v3, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLocalSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v3, p1, p2}, Lcom/google/android/tts/voices/Synthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 133
    .local v0, "availableLocally":I
    iget-object v3, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v3, p1, p2}, Lcom/google/android/tts/voices/Synthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 136
    .local v1, "availableOnNetwork":I
    if-gez v0, :cond_0

    if-ltz v1, :cond_2

    .line 138
    :cond_0
    if-lt v0, v1, :cond_1

    .line 140
    iget-object v3, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLocalSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v3, p1, p2}, Lcom/google/android/tts/voices/Synthesizer;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 141
    .local v2, "result":I
    iget-object v3, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLocalSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v3}, Lcom/google/android/tts/voices/Synthesizer;->getLanguage()[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLanguage:[Ljava/lang/String;

    .line 152
    .end local v2    # "result":I
    :goto_0
    return v2

    .line 145
    :cond_1
    iget-object v3, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v3, p1, p2}, Lcom/google/android/tts/voices/Synthesizer;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 146
    .restart local v2    # "result":I
    iget-object v3, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v3}, Lcom/google/android/tts/voices/Synthesizer;->getLanguage()[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLanguage:[Ljava/lang/String;

    goto :goto_0

    .line 152
    .end local v2    # "result":I
    :cond_2
    const/4 v2, -0x2

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mHybridSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v0}, Lcom/google/android/tts/voices/Synthesizer;->onStop()V

    .line 64
    return-void
.end method

.method public onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/tts/service/GoogleTTSRequest;
    .param p2, "callback"    # Landroid/speech/tts/SynthesisCallback;

    .prologue
    .line 101
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/common/base/CharMatcher;->WHITESPACE:Lcom/google/android/common/base/CharMatcher;

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/common/base/CharMatcher;->matchesAllOf(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 103
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->handleEmptySynthesis(Landroid/speech/tts/SynthesisCallback;)V

    .line 118
    :goto_0
    return-void

    .line 107
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->select(Lcom/google/android/tts/service/GoogleTTSRequest;)Lcom/google/android/tts/voices/Synthesizer;

    move-result-object v0

    .line 108
    .local v0, "choice":Lcom/google/android/tts/voices/Synthesizer;
    invoke-virtual {p0, p1}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->isDownloadTrigger(Lcom/google/android/tts/service/GoogleTTSRequest;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 109
    iget-object v1, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mDownloader:Lcom/google/android/tts/util/DownloadEnabler;

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Language()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Country()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/tts/util/DownloadEnabler;->enqueueVoiceDownloadByLanguage(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_2
    if-eqz v0, :cond_3

    .line 113
    invoke-interface {v0, p1, p2}, Lcom/google/android/tts/voices/Synthesizer;->onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V

    goto :goto_0

    .line 115
    :cond_3
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->error()V

    .line 116
    invoke-interface {p2}, Landroid/speech/tts/SynthesisCallback;->done()I

    goto :goto_0
.end method

.method select(Lcom/google/android/tts/service/GoogleTTSRequest;)Lcom/google/android/tts/voices/Synthesizer;
    .locals 2
    .param p1, "request"    # Lcom/google/android/tts/service/GoogleTTSRequest;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->isLocalOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLocalSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    .line 78
    :goto_0
    return-object v0

    .line 71
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->isNetworkOnly()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    goto :goto_0

    .line 74
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->isNetworkFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mHybridSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Language()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Country()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->selectSynthesizerByCapability(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/tts/voices/Synthesizer;

    move-result-object v0

    goto :goto_0
.end method

.method selectSynthesizerByCapability(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/tts/voices/Synthesizer;
    .locals 3
    .param p1, "iso3Language"    # Ljava/lang/String;
    .param p2, "iso3Country"    # Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 84
    iget-object v2, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLocalSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v2, p1, p2}, Lcom/google/android/tts/voices/Synthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 86
    .local v0, "availableLocally":I
    iget-object v2, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    invoke-interface {v2, p1, p2}, Lcom/google/android/tts/voices/Synthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 89
    .local v1, "availableOnNetwork":I
    if-lt v0, v1, :cond_0

    if-ltz v0, :cond_0

    .line 91
    iget-object v2, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mLocalSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    .line 97
    :goto_0
    return-object v2

    .line 93
    :cond_0
    if-ge v0, v1, :cond_1

    if-ltz v1, :cond_1

    .line 95
    iget-object v2, p0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->mNetworkSynthesizer:Lcom/google/android/tts/voices/Synthesizer;

    goto :goto_0

    .line 97
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
