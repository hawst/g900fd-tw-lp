.class public Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;
.super Landroid/app/Activity;
.source "VoiceDataInstallActivity.java"


# instance fields
.field protected mLicenses:Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

.field protected mVoicesListAdapter:Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    invoke-static {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getAll(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const/high16 v1, 0x7f030000

    invoke-virtual {p0, v1}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->setContentView(I)V

    .line 31
    new-instance v1, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    invoke-direct {v1, p0}, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->mLicenses:Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    .line 32
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->mLicenses:Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    invoke-virtual {v1, p1}, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->onCreate(Landroid/os/Bundle;)V

    .line 34
    new-instance v1, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;

    invoke-direct {v1, p0}, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->mVoicesListAdapter:Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;

    .line 35
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->mVoicesListAdapter:Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;

    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->getEntries()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->toLocaleSortedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->setEntries(Ljava/util/List;)V

    .line 37
    const/high16 v1, 0x7f0a0000

    invoke-virtual {p0, v1}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 38
    .local v0, "voicesListView":Landroid/widget/ListView;
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->mVoicesListAdapter:Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 39
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 48
    .local v0, "inflater":Landroid/view/MenuInflater;
    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->mLicenses:Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->mLicenses:Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->onDestroy()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->mLicenses:Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    .line 72
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 73
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 54
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0a000c

    if-ne v2, v3, :cond_0

    .line 55
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 56
    .local v0, "intent":Landroid/content/Intent;
    const-class v2, Lcom/google/android/tts/settings/EngineSettings;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    invoke-virtual {p0, v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->startActivity(Landroid/content/Intent;)V

    .line 63
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return v1

    .line 59
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0a000d

    if-ne v2, v3, :cond_1

    .line 60
    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->mLicenses:Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    invoke-virtual {v2}, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->showOpenSourceLicenses()V

    goto :goto_0

    .line 63
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->mLicenses:Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataInstallActivity;->mLicenses:Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;

    invoke-virtual {v0, p1}, Lcom/google/android/tts/local/voicepack/ui/ShowLicenses;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 81
    :cond_0
    return-void
.end method
