.class public Lcom/google/android/tts/voices/InternalVoice;
.super Ljava/lang/Object;
.source "InternalVoice.java"


# instance fields
.field private final mFeatures:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mLatency:I

.field private final mLocale:Ljava/util/Locale;

.field private final mName:Ljava/lang/String;

.field private final mQuality:I

.field private final mRequiresNetworkConnection:Z

.field private final mSynthesizer:Lcom/google/android/tts/voices/VoiceSynthesizer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;IIZLjava/util/Set;Lcom/google/android/tts/voices/VoiceSynthesizer;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/util/Locale;
    .param p3, "quality"    # I
    .param p4, "latency"    # I
    .param p5, "requiresNetworkConnection"    # Z
    .param p7, "synthesizer"    # Lcom/google/android/tts/voices/VoiceSynthesizer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            "IIZ",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/tts/voices/VoiceSynthesizer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    .local p6, "features":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/google/android/tts/voices/InternalVoice;->mName:Ljava/lang/String;

    .line 60
    iput-object p2, p0, Lcom/google/android/tts/voices/InternalVoice;->mLocale:Ljava/util/Locale;

    .line 61
    iput p3, p0, Lcom/google/android/tts/voices/InternalVoice;->mQuality:I

    .line 62
    iput p4, p0, Lcom/google/android/tts/voices/InternalVoice;->mLatency:I

    .line 63
    iput-boolean p5, p0, Lcom/google/android/tts/voices/InternalVoice;->mRequiresNetworkConnection:Z

    .line 64
    iput-object p6, p0, Lcom/google/android/tts/voices/InternalVoice;->mFeatures:Ljava/util/Set;

    .line 65
    iput-object p7, p0, Lcom/google/android/tts/voices/InternalVoice;->mSynthesizer:Lcom/google/android/tts/voices/VoiceSynthesizer;

    .line 66
    return-void
.end method


# virtual methods
.method public getFeatures()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/tts/voices/InternalVoice;->mFeatures:Ljava/util/Set;

    return-object v0
.end method

.method public getLatency()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/google/android/tts/voices/InternalVoice;->mLatency:I

    return v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/tts/voices/InternalVoice;->mLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/tts/voices/InternalVoice;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getQuality()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/google/android/tts/voices/InternalVoice;->mQuality:I

    return v0
.end method

.method public getRequiresNetworkConnection()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/android/tts/voices/InternalVoice;->mRequiresNetworkConnection:Z

    return v0
.end method

.method public getSynthesizer()Lcom/google/android/tts/voices/VoiceSynthesizer;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/tts/voices/InternalVoice;->mSynthesizer:Lcom/google/android/tts/voices/VoiceSynthesizer;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 141
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "Voice[Name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/tts/voices/InternalVoice;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", locale: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/tts/voices/InternalVoice;->mLocale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", quality: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/tts/voices/InternalVoice;->mQuality:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", latency: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/tts/voices/InternalVoice;->mLatency:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", requiresNetwork: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/tts/voices/InternalVoice;->mRequiresNetworkConnection:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", features: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/tts/voices/InternalVoice;->mFeatures:Ljava/util/Set;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
