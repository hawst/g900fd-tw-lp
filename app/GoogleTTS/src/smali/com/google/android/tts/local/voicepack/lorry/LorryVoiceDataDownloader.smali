.class public Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;
.super Ljava/lang/Object;
.source "LorryVoiceDataDownloader.java"

# interfaces
.implements Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActiveDownloads:Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

.field private final mCallbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final mConfig:Lcom/google/android/tts/settings/TtsConfig;

.field private final mContext:Landroid/content/Context;

.field private mDownloadManager:Landroid/app/DownloadManager;

.field private final mLock:Ljava/lang/Object;

.field private final mMetadataManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;Lcom/google/android/tts/settings/TtsConfig;)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "metadataManager"    # Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;
    .param p3, "config"    # Lcom/google/android/tts/settings/TtsConfig;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mLock:Ljava/lang/Object;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mCallbacks:Ljava/util/List;

    .line 90
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mContext:Landroid/content/Context;

    .line 91
    iput-object p2, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mMetadataManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    .line 92
    new-instance v0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "active_downloads"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mActiveDownloads:Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    .line 93
    iput-object p3, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    .line 94
    return-void
.end method

.method private buildDownloadRequest(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;ZLjava/lang/String;)Landroid/app/DownloadManager$Request;
    .locals 12
    .param p1, "voice"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .param p2, "userInitiated"    # Z
    .param p3, "url"    # Ljava/lang/String;

    .prologue
    .line 167
    :try_start_0
    new-instance v5, Landroid/app/DownloadManager$Request;

    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v5, v7}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    .local v5, "request":Landroid/app/DownloadManager$Request;
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    .line 184
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 187
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x10

    if-lt v7, v8, :cond_1

    .line 188
    if-nez p2, :cond_0

    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-interface {v7}, Lcom/google/android/tts/settings/TtsConfig;->getWifiOnly()Z

    move-result v7

    if-nez v7, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 190
    .local v0, "allowedOverMetered":Z
    :goto_0
    invoke-virtual {v5, v0}, Landroid/app/DownloadManager$Request;->setAllowedOverMetered(Z)Landroid/app/DownloadManager$Request;

    .line 194
    if-nez v0, :cond_1

    .line 195
    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mContext:Landroid/content/Context;

    const-string v8, "connectivity"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 197
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->isActiveNetworkMetered()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 198
    const/4 v7, 0x2

    invoke-virtual {v5, v7}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 203
    .end local v0    # "allowedOverMetered":Z
    .end local v1    # "connectivityManager":Landroid/net/ConnectivityManager;
    :cond_1
    invoke-static {p1}, Lcom/google/android/tts/util/LocalesHelper;->createFromMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/util/Locale;

    move-result-object v6

    .line 204
    .local v6, "voiceLocale":Ljava/util/Locale;
    if-nez v6, :cond_3

    .line 205
    sget-object v7, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invalid locale metadata: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    const/4 v5, 0x0

    .line 222
    .end local v5    # "request":Landroid/app/DownloadManager$Request;
    .end local v6    # "voiceLocale":Ljava/util/Locale;
    :goto_1
    return-object v5

    .line 172
    :catch_0
    move-exception v2

    .line 173
    .local v2, "iae":Ljava/lang/IllegalArgumentException;
    sget-object v7, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invalid download URI in metadata: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    const/4 v5, 0x0

    goto :goto_1

    .line 188
    .end local v2    # "iae":Ljava/lang/IllegalArgumentException;
    .restart local v5    # "request":Landroid/app/DownloadManager$Request;
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 209
    .restart local v6    # "voiceLocale":Ljava/util/Locale;
    :cond_3
    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mContext:Landroid/content/Context;

    const v8, 0x7f060015

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v6}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mContext:Landroid/content/Context;

    invoke-static {v11, p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getDisplayName(Landroid/content/Context;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 211
    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mContext:Landroid/content/Context;

    const v8, 0x7f060016

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/app/DownloadManager$Request;->setDescription(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 214
    :try_start_1
    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mContext:Landroid/content/Context;

    const-string v8, "download_cache"

    invoke-static {p1}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->formatDownloadFilename(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v7, v8, v9}, Landroid/app/DownloadManager$Request;->setDestinationInExternalFilesDir(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 216
    :catch_1
    move-exception v4

    .line 217
    .local v4, "npe":Ljava/lang/NullPointerException;
    sget-object v7, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error from #setDestinationInExternalFilesDir :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 218
    .end local v4    # "npe":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v3

    .line 219
    .local v3, "ise":Ljava/lang/IllegalStateException;
    sget-object v7, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error from #setDestinationInExternalFilesDir :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private ensureDownloadManager()Z
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mDownloadManager:Landroid/app/DownloadManager;

    if-nez v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mContext:Landroid/content/Context;

    const-string v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mDownloadManager:Landroid/app/DownloadManager;

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mDownloadManager:Landroid/app/DownloadManager;

    if-nez v0, :cond_1

    .line 288
    sget-object v0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->TAG:Ljava/lang/String;

    const-string v1, "Failed to get Download manager"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->showDownloadManagerDisabledToast()V

    .line 290
    const/4 v0, 0x0

    .line 292
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static formatDownloadFilename(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/lang/String;
    .locals 2
    .param p0, "voice"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .prologue
    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".zip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addListener(Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;)V
    .locals 2
    .param p1, "callback"    # Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;

    .prologue
    .line 364
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 365
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    monitor-exit v1

    .line 367
    return-void

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cancelDownload(Ljava/lang/String;)V
    .locals 10
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 133
    iget-object v5, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 134
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->ensureDownloadManager()Z

    move-result v4

    if-nez v4, :cond_0

    .line 135
    monitor-exit v5

    .line 161
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mActiveDownloads:Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    invoke-virtual {v4, p1}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->isActive(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 139
    monitor-exit v5

    goto :goto_0

    .line 160
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 142
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mActiveDownloads:Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    invoke-virtual {v4, p1}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->getEntry(Ljava/lang/String;)Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;

    move-result-object v2

    .line 143
    .local v2, "entry":Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;
    if-nez v2, :cond_2

    .line 144
    monitor-exit v5

    goto :goto_0

    .line 146
    :cond_2
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mActiveDownloads:Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    invoke-virtual {v4, p1}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->setFinished(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149
    :try_start_2
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mDownloadManager:Landroid/app/DownloadManager;

    const/4 v6, 0x1

    new-array v6, v6, [J

    const/4 v7, 0x0

    iget-wide v8, v2, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;->mDownloadId:J

    aput-wide v8, v6, v7

    invoke-virtual {v4, v6}, Landroid/app/DownloadManager;->remove([J)I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 157
    :try_start_3
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mCallbacks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;

    .line 158
    .local v0, "callback":Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;
    invoke-interface {v0, p1}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;->onVoiceDownloadFail(Ljava/lang/String;)V

    goto :goto_1

    .line 150
    .end local v0    # "callback":Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v1

    .line 152
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->TAG:Ljava/lang/String;

    const-string v6, "Exception from DownloadManager"

    invoke-static {v4, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 153
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->showDownloadManagerDisabledToast()V

    .line 154
    monitor-exit v5

    goto :goto_0

    .line 160
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Z)Z
    .locals 2
    .param p1, "voiceMetadata"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .param p2, "userInitiated"    # Z

    .prologue
    .line 306
    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getUrl()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;ZLjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;ZLjava/lang/String;Z)Z
    .locals 11
    .param p1, "voiceMetadata"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .param p2, "userInitiated"    # Z
    .param p3, "voiceDataUrl"    # Ljava/lang/String;
    .param p4, "suppressListeners"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 319
    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v5

    .line 320
    .local v5, "voiceName":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mLock:Ljava/lang/Object;

    monitor-enter v8

    .line 321
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->ensureDownloadManager()Z

    move-result v9

    if-nez v9, :cond_0

    .line 322
    monitor-exit v8

    .line 350
    :goto_0
    return v6

    .line 326
    :cond_0
    iget-object v9, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mActiveDownloads:Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    invoke-virtual {v9, v5}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->isActive(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 328
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v6, v7

    goto :goto_0

    .line 334
    :cond_1
    :try_start_1
    iget-object v9, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->buildDownloadRequest(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;ZLjava/lang/String;)Landroid/app/DownloadManager$Request;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v2

    .line 337
    .local v2, "downloadId":J
    iget-object v9, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mActiveDownloads:Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    new-instance v10, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;

    invoke-direct {v10, v2, v3, p2}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;-><init>(JZ)V

    invoke-virtual {v9, v5, v10}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->setActive(Ljava/lang/String;Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345
    if-nez p4, :cond_2

    .line 346
    :try_start_2
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mCallbacks:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;

    .line 347
    .local v0, "callback":Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;
    invoke-interface {v0, v5}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;->onVoiceDownloadStart(Ljava/lang/String;)V

    goto :goto_1

    .line 351
    .end local v0    # "callback":Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;
    .end local v2    # "downloadId":J
    .end local v4    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v6

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 339
    :catch_0
    move-exception v1

    .line 341
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    sget-object v7, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->TAG:Ljava/lang/String;

    const-string v9, "Exception from DownloadManager"

    invoke-static {v7, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 342
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->showDownloadManagerDisabledToast()V

    .line 343
    monitor-exit v8

    goto :goto_0

    .line 350
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v2    # "downloadId":J
    :cond_2
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v6, v7

    goto :goto_0
.end method

.method public getAllVoicesMetadata()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mMetadataManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->getAllVoiceMetadataList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDownloadInfo(J)Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;
    .locals 9
    .param p1, "downloadId"    # J

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 226
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->ensureDownloadManager()Z

    move-result v6

    if-nez v6, :cond_1

    move-object v3, v5

    .line 279
    :cond_0
    :goto_0
    return-object v3

    .line 230
    :cond_1
    new-instance v4, Landroid/app/DownloadManager$Query;

    invoke-direct {v4}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 231
    .local v4, "query":Landroid/app/DownloadManager$Query;
    new-array v6, v8, [J

    const/4 v7, 0x0

    aput-wide p1, v6, v7

    invoke-virtual {v4, v6}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 232
    const/4 v1, 0x0

    .line 233
    .local v1, "c":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 235
    .local v2, "info":Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;
    :try_start_0
    new-instance v3, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;

    invoke-direct {v3}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    .end local v2    # "info":Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;
    .local v3, "info":Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;
    :try_start_1
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mActiveDownloads:Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    invoke-virtual {v6, p1, p2}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->getByDownloadID(J)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    .line 238
    iget-object v6, v3, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v6, :cond_3

    .line 276
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v3, v5

    goto :goto_0

    .line 243
    :cond_3
    :try_start_2
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mActiveDownloads:Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    iget-object v7, v3, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mVoiceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->getEntry(Ljava/lang/String;)Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 245
    .local v0, "activeDownloadsEntry":Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;
    if-nez v0, :cond_5

    .line 276
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v3, v5

    goto :goto_0

    .line 248
    :cond_5
    :try_start_3
    iget-boolean v6, v0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;->mUserInitiated:Z

    iput-boolean v6, v3, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mUserInitiated:Z

    .line 250
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v6, v4}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v1

    .line 251
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 252
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-eq v6, v8, :cond_7

    .line 253
    sget-object v6, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Querying download manager failed for ID :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 276
    if-eqz v1, :cond_6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    move-object v3, v5

    goto :goto_0

    .line 258
    :cond_7
    :try_start_4
    iput-wide p1, v3, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mDownloadId:J

    .line 259
    const-string v5, "status"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v3, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mStatus:I

    .line 261
    const-string v5, "local_filename"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mFileName:Ljava/lang/String;

    .line 263
    const-string v5, "uri"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;->mUri:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 276
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .end local v0    # "activeDownloadsEntry":Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;
    .end local v3    # "info":Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;
    .restart local v2    # "info":Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;
    :catchall_0
    move-exception v5

    :goto_1
    if-eqz v1, :cond_8

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v5

    .end local v2    # "info":Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;
    .restart local v3    # "info":Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "info":Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;
    .restart local v2    # "info":Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader$DownloadInfo;
    goto :goto_1
.end method

.method public isActiveDownload(Ljava/lang/String;)Z
    .locals 2
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 357
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 358
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mActiveDownloads:Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    invoke-virtual {v0, p1}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->isActive(Ljava/lang/String;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 359
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public markCompleted(JZ)V
    .locals 7
    .param p1, "downloadId"    # J
    .param p3, "suppressListeners"    # Z

    .prologue
    .line 109
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 110
    :try_start_0
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mActiveDownloads:Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->getByDownloadID(J)Ljava/lang/String;

    move-result-object v2

    .line 111
    .local v2, "voiceName":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 112
    sget-object v3, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to find name for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "in active downloads"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    monitor-exit v4

    .line 124
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mActiveDownloads:Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    invoke-virtual {v3, v2}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->setFinished(Ljava/lang/String;)V

    .line 118
    if-nez p3, :cond_1

    .line 119
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mCallbacks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;

    .line 120
    .local v0, "callback":Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;
    invoke-interface {v0, v2}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;->onVoiceDownloadSuccess(Ljava/lang/String;)V

    goto :goto_1

    .line 123
    .end local v0    # "callback":Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "voiceName":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v2    # "voiceName":Ljava/lang/String;
    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public removeListener(Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;)V
    .locals 2
    .param p1, "callback"    # Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;

    .prologue
    .line 371
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 372
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 373
    monitor-exit v1

    .line 374
    return-void

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected showDownloadManagerDisabledToast()V
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->mContext:Landroid/content/Context;

    const v1, 0x7f060043

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 379
    return-void
.end method
