.class public Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;
.super Ljava/lang/Object;
.source "ActiveDownloads.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActiveDownloads:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final mActiveDownloadsFile:Ljava/io/File;

.field private final mLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "activeDownloadsFile"    # Ljava/io/File;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mLock:Ljava/lang/Object;

    .line 91
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloadsFile:Ljava/io/File;

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloads:Ljava/util/Map;

    .line 93
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->readActiveDownloads()V

    .line 94
    return-void
.end method

.method private readActiveDownloads()V
    .locals 5

    .prologue
    .line 98
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloads:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 99
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloadsFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    const/4 v0, 0x0

    .line 106
    .local v0, "data":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloadsFile:Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 107
    .local v2, "reader":Ljava/io/BufferedReader;
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 108
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {p0, v0}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->parseActiveDownloads(Ljava/lang/String;)V

    goto :goto_0

    .line 109
    .end local v2    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v1

    .line 110
    .local v1, "e":Ljava/io/IOException;
    sget-object v3, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->TAG:Ljava/lang/String;

    const-string v4, "Couldn\'t read active downloads list"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private writeActiveDownloads()V
    .locals 4

    .prologue
    .line 155
    :try_start_0
    new-instance v1, Ljava/io/FileWriter;

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloadsFile:Ljava/io/File;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    .line 156
    .local v1, "writer":Ljava/io/FileWriter;
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->serializeActiveDownloads()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 157
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    .end local v1    # "writer":Ljava/io/FileWriter;
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->TAG:Ljava/lang/String;

    const-string v3, "Couldn\'t write down active downloads list"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method getByDownloadID(J)Ljava/lang/String;
    .locals 7
    .param p1, "downloadId"    # J

    .prologue
    .line 181
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 182
    :try_start_0
    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloads:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 183
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;

    iget-wide v4, v2, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;->mDownloadId:J

    cmp-long v2, v4, p1

    if-nez v2, :cond_0

    .line 184
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    monitor-exit v3

    .line 187
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;>;"
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    monitor-exit v3

    goto :goto_0

    .line 188
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method getEntry(Ljava/lang/String;)Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;
    .locals 2
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 200
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 201
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloads:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;

    monitor-exit v1

    return-object v0

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method isActive(Ljava/lang/String;)Z
    .locals 2
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 193
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 194
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloads:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method parseActiveDownloads(Ljava/lang/String;)V
    .locals 12
    .param p1, "serializedActiveDownloads"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 121
    const-string v6, ","

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    .line 122
    .local v2, "entry":Ljava/lang/String;
    const-string v6, ":"

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 123
    .local v5, "pair":[Ljava/lang/String;
    array-length v6, v5

    const/4 v7, 0x2

    if-eq v6, v7, :cond_0

    .line 124
    sget-object v6, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Couldn\'t parse active downloads list entry: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 128
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloads:Ljava/util/Map;

    const/4 v7, 0x0

    aget-object v7, v5, v7

    new-instance v8, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;

    const/4 v9, 0x1

    aget-object v9, v5, v9

    invoke-direct {v8, v9}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;-><init>(Ljava/lang/String;)V

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 129
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v6, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to parse active download entry for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v5, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v5, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 135
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .end local v2    # "entry":Ljava/lang/String;
    .end local v5    # "pair":[Ljava/lang/String;
    :cond_1
    return-void
.end method

.method serializeActiveDownloads()Ljava/lang/String;
    .locals 6

    .prologue
    .line 139
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloads:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0xa

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 140
    .local v3, "output":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    .line 141
    .local v1, "first":Z
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloads:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 142
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;>;"
    if-nez v1, :cond_0

    .line 143
    const/16 v4, 0x2c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 145
    :cond_0
    const/4 v1, 0x0

    .line 146
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;

    invoke-virtual {v4}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 148
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;>;"
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method setActive(Ljava/lang/String;Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;)V
    .locals 2
    .param p1, "voiceName"    # Ljava/lang/String;
    .param p2, "entry"    # Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads$Entry;

    .prologue
    .line 165
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 166
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloads:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->writeActiveDownloads()V

    .line 168
    monitor-exit v1

    .line 169
    return-void

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setFinished(Ljava/lang/String;)V
    .locals 2
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 173
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 174
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->mActiveDownloads:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/ActiveDownloads;->writeActiveDownloads()V

    .line 176
    monitor-exit v1

    .line 177
    return-void

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
