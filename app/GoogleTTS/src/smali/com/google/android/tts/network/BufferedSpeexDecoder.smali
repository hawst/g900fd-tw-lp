.class public Lcom/google/android/tts/network/BufferedSpeexDecoder;
.super Ljava/lang/Object;
.source "BufferedSpeexDecoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;
    }
.end annotation


# instance fields
.field private mChunkSize:I

.field private mInputBufferSizeBytes:I

.field private mOutputBufferSizeBytes:I

.field private mSynthesisHandle:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-string v0, "speexwrapper"

    invoke-static {p1, v0}, Lcom/google/android/tts/service/PattsJniLoader;->loadJni(Landroid/content/Context;Ljava/lang/String;)Z

    .line 56
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mSynthesisHandle:J

    .line 57
    return-void
.end method

.method private static native native_destroy(J)V
.end method

.method private static native native_getChunk(J[BII[BI)I
.end method

.method private static native native_getInputFrameSize(J)I
.end method

.method private static native native_getOutputFrameSize(J)I
.end method

.method private static native native_start(I)J
.end method


# virtual methods
.method public endSynthesis()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 91
    iget-wide v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mSynthesisHandle:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/common/base/Preconditions;->checkState(Z)V

    .line 92
    iget-wide v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mSynthesisHandle:J

    invoke-static {v0, v1}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->native_destroy(J)V

    .line 93
    iput-wide v2, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mSynthesisHandle:J

    .line 94
    return-void

    .line 91
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInputBufferSize()I
    .locals 4

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mSynthesisHandle:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/common/base/Preconditions;->checkState(Z)V

    .line 87
    iget v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mInputBufferSizeBytes:I

    return v0

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNextChunk([BII[B)I
    .locals 8
    .param p1, "encoded"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "decoded"    # [B

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    iget-wide v4, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mSynthesisHandle:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/common/base/Preconditions;->checkState(Z)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->getInputBufferSize()I

    move-result v0

    if-gt p3, v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/common/base/Preconditions;->checkArgument(Z)V

    .line 74
    array-length v0, p4

    invoke-virtual {p0}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->getOutputBufferSize()I

    move-result v3

    if-gt v0, v3, :cond_2

    :goto_2
    invoke-static {v1}, Lcom/google/android/common/base/Preconditions;->checkArgument(Z)V

    .line 76
    iget-wide v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mSynthesisHandle:J

    array-length v6, p4

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->native_getChunk(J[BII[BI)I

    move-result v0

    return v0

    :cond_0
    move v0, v2

    .line 72
    goto :goto_0

    :cond_1
    move v0, v2

    .line 73
    goto :goto_1

    :cond_2
    move v1, v2

    .line 74
    goto :goto_2
.end method

.method public getOutputBufferSize()I
    .locals 4

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mSynthesisHandle:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/common/base/Preconditions;->checkState(Z)V

    .line 82
    iget v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mOutputBufferSizeBytes:I

    return v0

    .line 81
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startSynthesis(Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;I)V
    .locals 4
    .param p1, "rate"    # Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;
    .param p2, "chunkSize"    # I

    .prologue
    .line 60
    invoke-virtual {p1}, Lcom/google/android/tts/network/BufferedSpeexDecoder$SamplingRate;->getRate()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->native_start(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mSynthesisHandle:J

    .line 61
    iget-wide v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mSynthesisHandle:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error initializing native decoder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_0
    iput p2, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mChunkSize:I

    .line 66
    iget-wide v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mSynthesisHandle:J

    invoke-static {v0, v1}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->native_getOutputFrameSize(J)I

    move-result v0

    iget v1, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mChunkSize:I

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mOutputBufferSizeBytes:I

    .line 67
    iget-wide v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mSynthesisHandle:J

    invoke-static {v0, v1}, Lcom/google/android/tts/network/BufferedSpeexDecoder;->native_getInputFrameSize(J)I

    move-result v0

    iget v1, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mChunkSize:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/tts/network/BufferedSpeexDecoder;->mInputBufferSizeBytes:I

    .line 68
    return-void
.end method
