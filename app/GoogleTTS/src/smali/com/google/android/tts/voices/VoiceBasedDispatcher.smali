.class public Lcom/google/android/tts/voices/VoiceBasedDispatcher;
.super Ljava/lang/Object;
.source "VoiceBasedDispatcher.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mSynthesizers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voices/VoiceSynthesizer;",
            ">;"
        }
    .end annotation
.end field

.field private final mVoicesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/tts/voices/InternalVoice;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->mSynthesizers:Ljava/util/List;

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->mVoicesMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addSynthesizer(Lcom/google/android/tts/voices/VoiceSynthesizer;)V
    .locals 1
    .param p1, "synthesizer"    # Lcom/google/android/tts/voices/VoiceSynthesizer;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->mSynthesizers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    return-void
.end method

.method public getSynthesizerFor(Landroid/speech/tts/SynthesisRequest;)Lcom/google/android/tts/voices/VoiceSynthesizer;
    .locals 3
    .param p1, "request"    # Landroid/speech/tts/SynthesisRequest;

    .prologue
    .line 56
    iget-object v1, p0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->mVoicesMap:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getVoiceName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/voices/InternalVoice;

    .line 57
    .local v0, "voice":Lcom/google/android/tts/voices/InternalVoice;
    if-nez v0, :cond_0

    .line 58
    const/4 v1, 0x0

    .line 60
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/tts/voices/InternalVoice;->getSynthesizer()Lcom/google/android/tts/voices/VoiceSynthesizer;

    move-result-object v1

    goto :goto_0
.end method

.method public getVoices()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/tts/voices/InternalVoice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->mVoicesMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public isValidVoiceName(Ljava/lang/String;)Z
    .locals 1
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->mVoicesMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onLoadVoice(Ljava/lang/String;)Z
    .locals 2
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 68
    iget-object v1, p0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->mVoicesMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/voices/InternalVoice;

    .line 69
    .local v0, "voice":Lcom/google/android/tts/voices/InternalVoice;
    if-nez v0, :cond_0

    .line 70
    const/4 v1, 0x0

    .line 72
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/tts/voices/InternalVoice;->getSynthesizer()Lcom/google/android/tts/voices/VoiceSynthesizer;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/tts/voices/VoiceSynthesizer;->onLoadVoice(Lcom/google/android/tts/voices/InternalVoice;)Z

    move-result v1

    goto :goto_0
.end method

.method public updateVoicesSet()V
    .locals 7

    .prologue
    .line 33
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .local v3, "voicesList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/voices/InternalVoice;>;"
    iget-object v4, p0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->mSynthesizers:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/tts/voices/VoiceSynthesizer;

    .line 37
    .local v2, "synthesizer":Lcom/google/android/tts/voices/VoiceSynthesizer;
    invoke-interface {v2, v3}, Lcom/google/android/tts/voices/VoiceSynthesizer;->getVoices(Ljava/util/List;)V

    goto :goto_0

    .line 40
    .end local v2    # "synthesizer":Lcom/google/android/tts/voices/VoiceSynthesizer;
    :cond_0
    iget-object v4, p0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->mVoicesMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 41
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/voices/InternalVoice;

    .line 42
    .local v1, "internalVoiceInfo":Lcom/google/android/tts/voices/InternalVoice;
    iget-object v4, p0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->mVoicesMap:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/google/android/tts/voices/InternalVoice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 43
    sget-object v4, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Two voices added with a same name: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/tts/voices/InternalVoice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Two voices added with a same name: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/tts/voices/InternalVoice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 48
    .end local v1    # "internalVoiceInfo":Lcom/google/android/tts/voices/InternalVoice;
    :cond_2
    return-void
.end method
