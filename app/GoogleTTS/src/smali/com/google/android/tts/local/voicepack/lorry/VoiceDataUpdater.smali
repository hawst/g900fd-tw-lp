.class public Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;
.super Ljava/lang/Object;
.source "VoiceDataUpdater.java"

# interfaces
.implements Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAppVersionCode:J

.field private final mTtsConfig:Lcom/google/android/tts/settings/TtsConfig;

.field private final mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;

.field private final mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(JLcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;Lcom/google/android/tts/local/voicepack/VoiceDataManager;Lcom/google/android/tts/settings/TtsConfig;)V
    .locals 1
    .param p1, "appVersionCode"    # J
    .param p3, "voiceDataDownloader"    # Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;
    .param p4, "voiceDataManager"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager;
    .param p5, "ttsConfig"    # Lcom/google/android/tts/settings/TtsConfig;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-wide p1, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->mAppVersionCode:J

    .line 37
    iput-object p3, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;

    .line 38
    iput-object p4, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    .line 39
    iput-object p5, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfig;

    .line 40
    return-void
.end method


# virtual methods
.method public downloadUpdatedVoices(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)V
    .locals 10
    .param p1, "newList"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    .prologue
    .line 65
    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-interface {v7}, Lcom/google/android/tts/settings/TtsConfig;->getAutoUpdate()Z

    move-result v7

    if-nez v7, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v7}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAvailableVoicesInfo()Ljava/util/Map;

    move-result-object v1

    .line 70
    .local v1, "availableVoicesInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    if-nez v1, :cond_2

    .line 71
    sget-object v7, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->TAG:Ljava/lang/String;

    const-string v8, "Avaiable voices info is not ready yet"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 74
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->getDataList()Ljava/util/List;

    move-result-object v6

    .line 75
    .local v6, "newVoices":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;>;"
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 78
    .local v0, "availableVoices":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 79
    .local v4, "info":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .line 80
    .local v5, "newVoice":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    iget-object v7, v4, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {p0, v7, v5}, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->isNewerMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 81
    sget-object v7, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Requesting update for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v9}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "from revision "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v9}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;

    const/4 v8, 0x0

    invoke-virtual {v7, v5, v8}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;->downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Z)Z

    goto :goto_1
.end method

.method public isNewerMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z
    .locals 10
    .param p1, "metadataA"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .param p2, "metadataB"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .prologue
    const/4 v4, 0x0

    .line 44
    invoke-static {p1}, Lcom/google/android/tts/util/LocalesHelper;->createFromMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/util/Locale;

    move-result-object v0

    .line 45
    .local v0, "localeA":Ljava/util/Locale;
    invoke-static {p2}, Lcom/google/android/tts/util/LocalesHelper;->createFromMetadata(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Ljava/util/Locale;

    move-result-object v1

    .line 46
    .local v1, "localeB":Ljava/util/Locale;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v4

    .line 50
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v2

    .line 51
    .local v2, "nameA":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v3

    .line 52
    .local v3, "nameB":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 55
    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v5

    invoke-virtual {p2}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 58
    invoke-virtual {p2}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getMinApkVersionCode()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->mAppVersionCode:J

    cmp-long v5, v6, v8

    if-gtz v5, :cond_0

    .line 61
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public onVoiceMetadataListChange(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)V
    .locals 0
    .param p1, "newList"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->downloadUpdatedVoices(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)V

    .line 94
    return-void
.end method
