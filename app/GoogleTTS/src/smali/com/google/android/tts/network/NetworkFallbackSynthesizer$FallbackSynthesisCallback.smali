.class Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;
.super Ljava/lang/Object;
.source "NetworkFallbackSynthesizer.java"

# interfaces
.implements Landroid/speech/tts/SynthesisCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/network/NetworkFallbackSynthesizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FallbackSynthesisCallback"
.end annotation


# instance fields
.field private mErred:Z

.field private mFinished:Z

.field private final mRealCallback:Landroid/speech/tts/SynthesisCallback;

.field private mStarted:Z

.field final synthetic this$0:Lcom/google/android/tts/network/NetworkFallbackSynthesizer;


# direct methods
.method constructor <init>(Lcom/google/android/tts/network/NetworkFallbackSynthesizer;Landroid/speech/tts/SynthesisCallback;)V
    .locals 0
    .param p2, "realCallback"    # Landroid/speech/tts/SynthesisCallback;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->this$0:Lcom/google/android/tts/network/NetworkFallbackSynthesizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p2, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mRealCallback:Landroid/speech/tts/SynthesisCallback;

    .line 53
    return-void
.end method


# virtual methods
.method public audioAvailable([BII)I
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mRealCallback:Landroid/speech/tts/SynthesisCallback;

    invoke-interface {v0, p1, p2, p3}, Landroid/speech/tts/SynthesisCallback;->audioAvailable([BII)I

    move-result v0

    return v0
.end method

.method public done()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mFinished:Z

    .line 83
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mRealCallback:Landroid/speech/tts/SynthesisCallback;

    invoke-interface {v0}, Landroid/speech/tts/SynthesisCallback;->done()I

    move-result v0

    return v0
.end method

.method public error()V
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mStarted:Z

    if-eqz v0, :cond_0

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mErred:Z

    .line 92
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mRealCallback:Landroid/speech/tts/SynthesisCallback;

    invoke-interface {v0}, Landroid/speech/tts/SynthesisCallback;->error()V

    .line 94
    :cond_0
    return-void
.end method

.method public error(I)V
    .locals 0
    .param p1, "errorCode"    # I

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->error()V

    .line 100
    return-void
.end method

.method public getMaxBufferSize()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mRealCallback:Landroid/speech/tts/SynthesisCallback;

    invoke-interface {v0}, Landroid/speech/tts/SynthesisCallback;->getMaxBufferSize()I

    move-result v0

    return v0
.end method

.method public hasFinished()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mFinished:Z

    return v0
.end method

.method public hasStarted()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mStarted:Z

    return v0
.end method

.method public isRecoverable()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mStarted:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mErred:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSuccesful()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mStarted:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mErred:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mFinished:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public start(III)I
    .locals 1
    .param p1, "sampleRateInHz"    # I
    .param p2, "audioFormat"    # I
    .param p3, "channelCount"    # I

    .prologue
    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mStarted:Z

    .line 72
    iget-object v0, p0, Lcom/google/android/tts/network/NetworkFallbackSynthesizer$FallbackSynthesisCallback;->mRealCallback:Landroid/speech/tts/SynthesisCallback;

    invoke-interface {v0, p1, p2, p3}, Landroid/speech/tts/SynthesisCallback;->start(III)I

    move-result v0

    return v0
.end method
