.class public Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper;
.super Ljava/lang/Object;
.source "VoiceEntryListUIHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getVoicesEntryOverflowListener(Landroid/content/Context;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "entry"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;
    .param p2, "listener"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$1;-><init>(Landroid/content/Context;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;)V

    return-object v0
.end method

.method public static showUninstallWarning(Landroid/content/Context;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "entry"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;
    .param p2, "listener"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;

    .prologue
    .line 61
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 62
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f06003e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 63
    const v1, 0x7f060040

    new-instance v2, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$2;

    invoke-direct {v2, p2, p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$2;-><init>(Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 70
    const v1, 0x7f06003f

    new-instance v2, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$3;

    invoke-direct {v2}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper$3;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 77
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 78
    return-void
.end method
