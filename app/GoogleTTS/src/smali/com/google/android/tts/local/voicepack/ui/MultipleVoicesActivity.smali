.class public Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;
.super Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;
.source "MultipleVoicesActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected mLocale:Ljava/util/Locale;

.field protected mVoicesListAdapter:Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 26
    invoke-super {p0, p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-nez v2, :cond_1

    .line 28
    :cond_0
    sget-object v2, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->TAG:Ljava/lang/String;

    const-string v3, "Missing intent extras"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->finish()V

    .line 53
    :goto_0
    return-void

    .line 32
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "locale"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "localeStr":[Ljava/lang/String;
    if-nez v0, :cond_2

    .line 34
    sget-object v2, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->TAG:Ljava/lang/String;

    const-string v3, "Missing intent extra \"locale\""

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->finish()V

    goto :goto_0

    .line 38
    :cond_2
    array-length v2, v0

    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    .line 39
    sget-object v2, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->TAG:Ljava/lang/String;

    const-string v3, "Intent extra \"locale\" should be a string array with 2 items"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->finish()V

    goto :goto_0

    .line 43
    :cond_3
    new-instance v2, Ljava/util/Locale;

    aget-object v3, v0, v5

    aget-object v4, v0, v6

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->mLocale:Ljava/util/Locale;

    .line 45
    const v2, 0x7f060042

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->mLocale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 46
    const/high16 v2, 0x7f030000

    invoke-virtual {p0, v2}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->setContentView(I)V

    .line 48
    new-instance v2, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;

    new-instance v3, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;

    invoke-direct {v3, p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;-><init>(Landroid/content/Context;)V

    invoke-direct {v2, p0, v3}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;)V

    iput-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->mVoicesListAdapter:Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;

    .line 51
    const/high16 v2, 0x7f0a0000

    invoke-virtual {p0, v2}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 52
    .local v1, "voicesListView":Landroid/widget/ListView;
    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->mVoicesListAdapter:Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 57
    invoke-super {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->onResume()V

    .line 58
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->mVoicesListAdapter:Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;

    invoke-static {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getAll(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->toLocaleSortedMap(Ljava/util/List;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->mLocale:Ljava/util/Locale;

    invoke-interface {v0, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->setEntries(Ljava/util/List;)V

    .line 60
    return-void
.end method

.method protected onUpdateVoiceData()V
    .locals 3

    .prologue
    .line 64
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->mVoicesListAdapter:Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;

    invoke-static {p0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getAll(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->toLocaleSortedMap(Ljava/util/List;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesActivity;->mLocale:Ljava/util/Locale;

    invoke-interface {v0, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->setEntries(Ljava/util/List;)V

    .line 67
    return-void
.end method
