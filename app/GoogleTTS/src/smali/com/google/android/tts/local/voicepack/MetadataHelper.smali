.class public Lcom/google/android/tts/local/voicepack/MetadataHelper;
.super Ljava/lang/Object;
.source "MetadataHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isDefaultForLocale(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Lcom/google/android/tts/settings/TtsConfig;)Z
    .locals 2
    .param p0, "voiceMetadata"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .param p1, "ttsConfig"    # Lcom/google/android/tts/settings/TtsConfig;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/google/android/tts/settings/TtsConfig;->getDefaultVoiceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static simpleEqual(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z
    .locals 2
    .param p0, "a"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .param p1, "b"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static simpleHashCode(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)I
    .locals 5
    .param p0, "a"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .prologue
    const/4 v3, 0x0

    .line 23
    const/16 v0, 0x1f

    .line 24
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 25
    .local v1, "result":I
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasName()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 26
    mul-int/lit8 v2, v1, 0x1f

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v4

    add-int v1, v2, v4

    .line 27
    mul-int/lit8 v2, v1, 0x1f

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasLocale()Z

    move-result v4

    if-nez v4, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 28
    return v1

    .line 25
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 27
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_1
.end method
