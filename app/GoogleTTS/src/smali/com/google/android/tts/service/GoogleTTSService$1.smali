.class Lcom/google/android/tts/service/GoogleTTSService$1;
.super Ljava/lang/Object;
.source "GoogleTTSService.java"

# interfaces
.implements Lcom/google/android/tts/util/NetworkStatus;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/tts/service/GoogleTTSService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/tts/service/GoogleTTSService;


# direct methods
.method constructor <init>(Lcom/google/android/tts/service/GoogleTTSService;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/tts/service/GoogleTTSService$1;->this$0:Lcom/google/android/tts/service/GoogleTTSService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isNetworkConnected()Z
    .locals 4

    .prologue
    .line 103
    iget-object v2, p0, Lcom/google/android/tts/service/GoogleTTSService$1;->this$0:Lcom/google/android/tts/service/GoogleTTSService;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Lcom/google/android/tts/service/GoogleTTSService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 105
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 106
    .local v1, "ni":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
