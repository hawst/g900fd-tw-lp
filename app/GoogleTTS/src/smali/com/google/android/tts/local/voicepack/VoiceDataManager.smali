.class public Lcom/google/android/tts/local/voicepack/VoiceDataManager;
.super Ljava/lang/Object;
.source "VoiceDataManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/local/voicepack/VoiceDataManager$1;,
        Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;,
        Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    }
.end annotation


# static fields
.field public static final PATTS_DATA_OLD_DIRS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAutoInstallableVoices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mAvailableVoicesInfo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mConfig:Lcom/google/android/tts/settings/TtsConfig;

.field private final mContext:Landroid/content/Context;

.field private final mExcludedVoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mIntegrityChecker:Lcom/google/android/tts/local/voicepack/IntegrityChecker;

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private final mPattsDataDir:Ljava/io/File;

.field private final mPattsSystemDir:Ljava/io/File;

.field private final mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

.field private final mVoiceDataObsoletionChecker:Lcom/google/android/tts/local/voicepack/VoiceDataObsoletionChecker;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const-class v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    .line 42
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "patts"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "voices"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->PATTS_DATA_OLD_DIRS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;Lcom/google/android/tts/settings/TtsConfig;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "voiceDataDownloader"    # Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;
    .param p3, "ttsConfig"    # Lcom/google/android/tts/settings/TtsConfig;

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mLock:Ljava/lang/Object;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mListeners:Ljava/util/List;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mExcludedVoices:Ljava/util/ArrayList;

    .line 254
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mContext:Landroid/content/Context;

    .line 255
    iput-object p2, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    .line 256
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mContext:Landroid/content/Context;

    const-string v1, "voices_v2"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mPattsDataDir:Ljava/io/File;

    .line 257
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/tts/google"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mPattsSystemDir:Ljava/io/File;

    .line 258
    iput-object p3, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    .line 259
    new-instance v0, Lcom/google/android/tts/local/voicepack/VoiceDataObsoletionChecker;

    invoke-direct {v0, p1}, Lcom/google/android/tts/local/voicepack/VoiceDataObsoletionChecker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mVoiceDataObsoletionChecker:Lcom/google/android/tts/local/voicepack/VoiceDataObsoletionChecker;

    .line 260
    new-instance v0, Lcom/google/android/tts/local/voicepack/IntegrityChecker;

    invoke-direct {v0, p1}, Lcom/google/android/tts/local/voicepack/IntegrityChecker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mIntegrityChecker:Lcom/google/android/tts/local/voicepack/IntegrityChecker;

    .line 261
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/local/voicepack/VoiceDataManager;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mPattsSystemDir:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/tts/local/voicepack/VoiceDataManager;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mPattsDataDir:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/tts/local/voicepack/VoiceDataManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private addAvailableVoiceInfo(Ljava/util/Map;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;)V
    .locals 8
    .param p2, "voiceInfo"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;",
            ">;",
            "Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 390
    .local p1, "voicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    invoke-virtual {p2}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getName()Ljava/lang/String;

    move-result-object v2

    .line 391
    .local v2, "uniqueName":Ljava/lang/String;
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 392
    .local v1, "prevVoiceInfo":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    if-eqz v1, :cond_0

    .line 393
    iget-object v4, v1, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v4}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v4

    iget-object v5, p2, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v5}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v5

    if-lt v4, v5, :cond_0

    .line 418
    :goto_0
    return-void

    .line 401
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget v3, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 403
    .local v3, "versionCode":I
    iget-object v4, p2, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v4}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getMinApkVersionCode()J

    move-result-wide v4

    int-to-long v6, v3

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 404
    sget-object v4, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found voice "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " that requries newer apk (required: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p2, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v6}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getMinApkVersionCode()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", current: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 409
    .end local v3    # "versionCode":I
    :catch_0
    move-exception v0

    .line 410
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v4, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    const-string v5, "Couldn\'t find my own package"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 413
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mVoiceDataObsoletionChecker:Lcom/google/android/tts/local/voicepack/VoiceDataObsoletionChecker;

    iget-object v5, p2, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v4, v5}, Lcom/google/android/tts/local/voicepack/VoiceDataObsoletionChecker;->isVoiceObsolete(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 414
    sget-object v4, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found older voice "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " that is obsolete"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    :cond_2
    invoke-interface {p1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private addVoicesFromApk(Ljava/util/Map;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 348
    .local p1, "voicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    .line 350
    .local v2, "am":Landroid/content/res/AssetManager;
    const/16 v19, 0x0

    .line 352
    .local v19, "voicesAssets":[Ljava/lang/String;
    :try_start_0
    const-string v3, "voices"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v19

    .line 358
    move-object/from16 v12, v19

    .local v12, "arr$":[Ljava/lang/String;
    array-length v15, v12

    .local v15, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_0
    if-ge v14, v15, :cond_1

    aget-object v16, v12, v14

    .line 359
    .local v16, "localeDirName":Ljava/lang/String;
    const/16 v17, 0x0

    .line 361
    .local v17, "metadataStream":Ljava/io/InputStream;
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "voices"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 363
    .local v18, "voicePath":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "voice_metadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v17

    .line 365
    invoke-static/range {v17 .. v17}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->newInstance(Ljava/io/InputStream;)Lcom/google/protobuf/micro/CodedInputStreamMicro;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->parseFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v6

    .line 367
    .local v6, "metadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mVoiceDataObsoletionChecker:Lcom/google/android/tts/local/voicepack/VoiceDataObsoletionChecker;

    invoke-virtual {v3, v6}, Lcom/google/android/tts/local/voicepack/VoiceDataObsoletionChecker;->isVoiceObsolete(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 369
    sget-object v3, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "APK bundled voice is obsolete: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 380
    if-eqz v17, :cond_0

    .line 381
    invoke-static/range {v17 .. v17}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 358
    .end local v6    # "metadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .end local v18    # "voicePath":Ljava/lang/String;
    :cond_0
    :goto_1
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 353
    .end local v12    # "arr$":[Ljava/lang/String;
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    .end local v16    # "localeDirName":Ljava/lang/String;
    .end local v17    # "metadataStream":Ljava/io/InputStream;
    :catch_0
    move-exception v13

    .line 354
    .local v13, "e":Ljava/io/IOException;
    sget-object v3, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    const-string v4, "Couldn\'t list voices assets"

    invoke-static {v3, v4, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 385
    .end local v13    # "e":Ljava/io/IOException;
    :cond_1
    return-void

    .line 373
    .restart local v6    # "metadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .restart local v12    # "arr$":[Ljava/lang/String;
    .restart local v14    # "i$":I
    .restart local v15    # "len$":I
    .restart local v16    # "localeDirName":Ljava/lang/String;
    .restart local v17    # "metadataStream":Ljava/io/InputStream;
    .restart local v18    # "voicePath":Ljava/lang/String;
    :cond_2
    :try_start_2
    new-instance v3, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getUnpackedSizeKb()I

    move-result v4

    mul-int/lit16 v4, v4, 0x400

    int-to-long v8, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-static {v6, v4}, Lcom/google/android/tts/local/voicepack/MetadataHelper;->isDefaultForLocale(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Lcom/google/android/tts/settings/TtsConfig;)Z

    move-result v10

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v11}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;-><init>(Lcom/google/android/tts/local/voicepack/VoiceDataManager;ILcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Ljava/lang/String;JZLcom/google/android/tts/local/voicepack/VoiceDataManager$1;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->addAvailableVoiceInfo(Ljava/util/Map;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 380
    if-eqz v17, :cond_0

    .line 381
    invoke-static/range {v17 .. v17}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    .line 377
    .end local v6    # "metadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .end local v18    # "voicePath":Ljava/lang/String;
    :catch_1
    move-exception v13

    .line 378
    .restart local v13    # "e":Ljava/io/IOException;
    :try_start_3
    sget-object v3, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    const-string v4, "IOException while reading metadata of apk voice"

    invoke-static {v3, v4, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 380
    if-eqz v17, :cond_0

    .line 381
    invoke-static/range {v17 .. v17}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    .line 380
    .end local v13    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v17, :cond_3

    .line 381
    invoke-static/range {v17 .. v17}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :cond_3
    throw v3
.end method

.method private addVoicesFromDir(Ljava/util/Map;Ljava/io/File;Z)Z
    .locals 22
    .param p2, "directory"    # Ljava/io/File;
    .param p3, "inSystem"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;",
            ">;",
            "Ljava/io/File;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 499
    .local p1, "voicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_1

    .line 500
    :cond_0
    const/4 v3, 0x0

    .line 547
    :goto_0
    return v3

    .line 502
    :cond_1
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .local v2, "arr$":[Ljava/io/File;
    array-length v0, v2

    move/from16 v18, v0

    .local v18, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    move/from16 v17, v16

    .end local v2    # "arr$":[Ljava/io/File;
    .end local v16    # "i$":I
    .end local v18    # "len$":I
    .local v17, "i$":I
    :goto_1
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_b

    aget-object v14, v2, v17

    .line 503
    .local v14, "dir":Ljava/io/File;
    const-string v3, "."

    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, ".."

    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 502
    .end local v17    # "i$":I
    :cond_2
    :goto_2
    add-int/lit8 v16, v17, 0x1

    .restart local v16    # "i$":I
    move/from16 v17, v16

    .end local v16    # "i$":I
    .restart local v17    # "i$":I
    goto :goto_1

    .line 506
    :cond_3
    invoke-virtual {v14}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v14}, Ljava/io/File;->canRead()Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mExcludedVoices:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 509
    const-wide/16 v8, 0x0

    .line 510
    .local v8, "totalSize":J
    const/4 v6, 0x0

    .line 512
    .local v6, "metadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-virtual {v14}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    .local v12, "arr$":[Ljava/io/File;
    array-length v0, v12

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v16, 0x0

    .end local v17    # "i$":I
    .restart local v16    # "i$":I
    :goto_3
    move/from16 v0, v16

    move/from16 v1, v19

    if-ge v0, v1, :cond_7

    aget-object v13, v12, v16

    .line 513
    .local v13, "child":Ljava/io/File;
    const-string v3, "."

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, ".."

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 512
    :cond_4
    :goto_4
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 516
    :cond_5
    const-string v3, "voice_metadata"

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v10

    const-wide/16 v20, 0x0

    cmp-long v3, v10, v20

    if-lez v3, :cond_6

    .line 519
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v13}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->parseVoiceMetadata(Ljava/io/InputStream;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 524
    :cond_6
    :goto_5
    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v10

    add-long/2addr v8, v10

    goto :goto_4

    .line 520
    :catch_0
    move-exception v15

    .line 521
    .local v15, "e":Ljava/io/FileNotFoundException;
    sget-object v3, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    const-string v4, "Couldn\'t find metadata file"

    invoke-static {v3, v4, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    .line 527
    .end local v13    # "child":Ljava/io/File;
    .end local v15    # "e":Ljava/io/FileNotFoundException;
    :cond_7
    if-eqz v6, :cond_2

    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-lez v3, :cond_2

    .line 528
    invoke-virtual {v6}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasName()Z

    move-result v3

    if-nez v3, :cond_8

    .line 529
    sget-object v3, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Outdated voice pack, ignoring voice found in: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 533
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mIntegrityChecker:Lcom/google/android/tts/local/voicepack/IntegrityChecker;

    invoke-virtual {v3, v6, v14}, Lcom/google/android/tts/local/voicepack/IntegrityChecker;->isConsistent(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Ljava/io/File;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 534
    sget-object v3, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Voice pack contents not consistent with metadata, removing voice found in: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v3}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->deleteVoiceDataFiles(Ljava/io/File;Z)Z

    goto/16 :goto_2

    .line 540
    :cond_9
    if-eqz p3, :cond_a

    const/4 v5, 0x3

    .line 541
    .local v5, "loc":I
    :goto_6
    new-instance v3, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    const-string v7, ""

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-static {v6, v4}, Lcom/google/android/tts/local/voicepack/MetadataHelper;->isDefaultForLocale(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Lcom/google/android/tts/settings/TtsConfig;)Z

    move-result v10

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v11}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;-><init>(Lcom/google/android/tts/local/voicepack/VoiceDataManager;ILcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Ljava/lang/String;JZLcom/google/android/tts/local/voicepack/VoiceDataManager$1;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->addAvailableVoiceInfo(Ljava/util/Map;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;)V

    goto/16 :goto_2

    .line 540
    .end local v5    # "loc":I
    :cond_a
    const/4 v5, 0x2

    goto :goto_6

    .line 547
    .end local v6    # "metadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .end local v8    # "totalSize":J
    .end local v12    # "arr$":[Ljava/io/File;
    .end local v14    # "dir":Ljava/io/File;
    .end local v16    # "i$":I
    .end local v19    # "len$":I
    .restart local v17    # "i$":I
    :cond_b
    const/4 v3, 0x1

    goto/16 :goto_0
.end method

.method private chooseRequiredDefaults()Z
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 558
    const/4 v6, 0x0

    .line 560
    .local v6, "needUpdate":Z
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 561
    .local v5, "localesCount":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/util/Locale;Ljava/lang/Integer;>;"
    iget-object v9, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAvailableVoicesInfo:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 562
    .local v4, "installedVoiceInfo":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    invoke-virtual {v4}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLocale()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 563
    .local v8, "val":Ljava/lang/Integer;
    if-nez v8, :cond_0

    .line 564
    new-instance v8, Ljava/lang/Integer;

    .end local v8    # "val":Ljava/lang/Integer;
    const/4 v9, 0x0

    invoke-direct {v8, v9}, Ljava/lang/Integer;-><init>(I)V

    .line 566
    .restart local v8    # "val":Ljava/lang/Integer;
    :cond_0
    invoke-virtual {v4}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLocale()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 569
    .end local v4    # "installedVoiceInfo":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    .end local v8    # "val":Ljava/lang/Integer;
    :cond_1
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 571
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/util/Locale;Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-le v9, v12, :cond_2

    .line 574
    iget-object v10, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Locale;

    invoke-virtual {v9}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v10, v9}, Lcom/google/android/tts/settings/TtsConfig;->getDefaultVoiceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 575
    .local v0, "defaultVoiceName":Ljava/lang/String;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 579
    :cond_3
    const/4 v6, 0x1

    .line 582
    const/4 v7, 0x0

    .line 583
    .local v7, "proposedVoice":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    iget-object v9, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAvailableVoicesInfo:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 584
    .restart local v4    # "installedVoiceInfo":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    invoke-virtual {v4}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLocale()Ljava/util/Locale;

    move-result-object v9

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 586
    if-eqz v7, :cond_5

    iget-object v9, v4, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->mVoiceMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-virtual {v9}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getTag()I

    move-result v9

    if-ne v9, v12, :cond_4

    .line 588
    :cond_5
    move-object v7, v4

    goto :goto_2

    .line 593
    .end local v4    # "installedVoiceInfo":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    :cond_6
    if-eqz v7, :cond_7

    .line 594
    iget-object v10, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Locale;

    invoke-virtual {v9}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v9, v11}, Lcom/google/android/tts/settings/TtsConfig;->setDefaultVoiceName(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 596
    :cond_7
    sget-object v9, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Couldn\'t find candidate for a default for locale "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 599
    .end local v0    # "defaultVoiceName":Ljava/lang/String;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/util/Locale;Ljava/lang/Integer;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "proposedVoice":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    :cond_8
    return v6
.end method

.method private deleteRecursive(Ljava/io/File;)V
    .locals 5
    .param p1, "fileOrDirectory"    # Ljava/io/File;

    .prologue
    .line 724
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 725
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 726
    .local v1, "child":Ljava/io/File;
    invoke-direct {p0, v1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->deleteRecursive(Ljava/io/File;)V

    .line 725
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 727
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "child":Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 728
    return-void
.end method

.method private parseVoiceMetadata(Ljava/io/InputStream;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 3
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 423
    :try_start_0
    invoke-static {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->newInstance(Ljava/io/InputStream;)Lcom/google/protobuf/micro/CodedInputStreamMicro;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->parseFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 428
    if-eqz p1, :cond_0

    .line 429
    invoke-static {p1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 432
    :cond_0
    :goto_0
    return-object v1

    .line 425
    :catch_0
    move-exception v0

    .line 426
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    sget-object v1, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    const-string v2, "Couldn\'t read metadata file"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 428
    if-eqz p1, :cond_1

    .line 429
    invoke-static {p1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 432
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 428
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    if-eqz p1, :cond_2

    .line 429
    invoke-static {p1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :cond_2
    throw v1
.end method

.method private removeUnsupportedVoicesFromDir(Ljava/io/File;)V
    .locals 20
    .param p1, "directory"    # Ljava/io/File;

    .prologue
    .line 437
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isDirectory()Z

    move-result v15

    if-nez v15, :cond_1

    .line 494
    :cond_0
    return-void

    .line 440
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .local v2, "arr$":[Ljava/io/File;
    array-length v10, v2

    .local v10, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    move v9, v8

    .end local v2    # "arr$":[Ljava/io/File;
    .end local v8    # "i$":I
    .end local v10    # "len$":I
    .local v9, "i$":I
    :goto_0
    if-ge v9, v10, :cond_0

    aget-object v5, v2, v9

    .line 441
    .local v5, "dir":Ljava/io/File;
    const-string v15, "."

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    const-string v15, ".."

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 440
    .end local v9    # "i$":I
    :cond_2
    :goto_1
    add-int/lit8 v8, v9, 0x1

    .restart local v8    # "i$":I
    move v9, v8

    .end local v8    # "i$":I
    .restart local v9    # "i$":I
    goto :goto_0

    .line 444
    :cond_3
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v15

    if-eqz v15, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->canRead()Z

    move-result v15

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mExcludedVoices:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 447
    const/4 v12, 0x0

    .line 448
    .local v12, "metadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .local v3, "arr$":[Ljava/io/File;
    array-length v11, v3

    .local v11, "len$":I
    const/4 v8, 0x0

    .end local v9    # "i$":I
    .restart local v8    # "i$":I
    :goto_2
    if-ge v8, v11, :cond_6

    aget-object v4, v3, v8

    .line 449
    .local v4, "child":Ljava/io/File;
    const-string v15, "."

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_4

    const-string v15, ".."

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 448
    :cond_4
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 452
    :cond_5
    const-string v15, "voice_metadata"

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v16

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-lez v15, :cond_4

    .line 455
    :try_start_0
    new-instance v15, Ljava/io/FileInputStream;

    invoke-direct {v15, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->parseVoiceMetadata(Ljava/io/InputStream;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 463
    .end local v4    # "child":Ljava/io/File;
    :cond_6
    const/4 v14, 0x0

    .line 464
    .local v14, "nukeDir":Z
    const/4 v7, 0x0

    .line 466
    .local v7, "enqueue":Z
    if-nez v12, :cond_9

    .line 467
    sget-object v15, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Couldn\'t find a valid metadata entry in "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", removing directory"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    const/4 v14, 0x1

    .line 478
    :cond_7
    :goto_4
    if-eqz v14, :cond_8

    .line 479
    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v15}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->deleteVoiceDataFiles(Ljava/io/File;Z)Z

    .line 481
    :cond_8
    if-eqz v7, :cond_2

    .line 483
    new-instance v15, Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;->getAllVoicesMetadata()Ljava/util/List;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;-><init>(Ljava/util/List;)V

    invoke-virtual {v12}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->firstByName(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v13

    .line 486
    .local v13, "newMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    if-eqz v13, :cond_a

    .line 487
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v15, v13, v0}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;->downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Z)Z

    goto/16 :goto_1

    .line 457
    .end local v7    # "enqueue":Z
    .end local v13    # "newMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .end local v14    # "nukeDir":Z
    .restart local v4    # "child":Ljava/io/File;
    :catch_0
    move-exception v6

    .line 458
    .local v6, "e":Ljava/io/FileNotFoundException;
    sget-object v15, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    const-string v16, "Couldn\'t find metadata file"

    move-object/from16 v0, v16

    invoke-static {v15, v0, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3

    .line 470
    .end local v4    # "child":Ljava/io/File;
    .end local v6    # "e":Ljava/io/FileNotFoundException;
    .restart local v7    # "enqueue":Z
    .restart local v14    # "nukeDir":Z
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mVoiceDataObsoletionChecker:Lcom/google/android/tts/local/voicepack/VoiceDataObsoletionChecker;

    invoke-virtual {v15, v12}, Lcom/google/android/tts/local/voicepack/VoiceDataObsoletionChecker;->isVoiceObsolete(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 471
    sget-object v15, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "The voice located in "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " is no longer supported by the"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " current version of the GoogleTTS. The voice will be removed"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " and a download of the updated voice version will be scheduled"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    const/4 v14, 0x1

    .line 475
    const/4 v7, 0x1

    goto :goto_4

    .line 489
    .restart local v13    # "newMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    :cond_a
    sget-object v15, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "No replacement for removed voice in: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method


# virtual methods
.method public addVoicesDataListener(Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;

    .prologue
    .line 273
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 274
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    monitor-exit v1

    .line 276
    return-void

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public checkData()V
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 294
    const/4 v5, 0x0

    .line 295
    .local v5, "setChanged":Z
    const/4 v6, 0x0

    .line 296
    .local v6, "setNew":Z
    iget-object v9, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mLock:Ljava/lang/Object;

    monitor-enter v9

    .line 299
    :try_start_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 303
    .local v3, "newAvailableVoicesInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    invoke-direct {p0, v3}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->addVoicesFromApk(Ljava/util/Map;)V

    .line 306
    iget-object v10, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mPattsSystemDir:Ljava/io/File;

    const/4 v11, 0x1

    invoke-direct {p0, v3, v10, v11}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->addVoicesFromDir(Ljava/util/Map;Ljava/io/File;Z)Z

    .line 310
    iget-object v10, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mPattsDataDir:Ljava/io/File;

    const/4 v11, 0x0

    invoke-direct {p0, v3, v10, v11}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->addVoicesFromDir(Ljava/util/Map;Ljava/io/File;Z)Z

    move-result v10

    if-nez v10, :cond_0

    .line 311
    sget-object v10, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Directory "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mPattsDataDir:Ljava/io/File;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " doesn\'t exist"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :cond_0
    new-instance v0, Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    iget-object v10, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    invoke-interface {v10}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;->getAllVoicesMetadata()Ljava/util/List;

    move-result-object v10

    invoke-direct {v0, v10}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;-><init>(Ljava/util/List;)V

    .line 317
    .local v0, "filter":Lcom/google/android/tts/local/voicepack/MetadataListFilter;
    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->onlyAutoInstalls()Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    move-result-object v10

    invoke-virtual {p0, v3}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getAvailableLocales(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->removeLocalesSet(Ljava/util/Set;)Lcom/google/android/tts/local/voicepack/MetadataListFilter;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/tts/local/voicepack/MetadataListFilter;->getNameMap()Ljava/util/Map;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAutoInstallableVoices:Ljava/util/Map;

    .line 322
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAvailableVoicesInfo:Ljava/util/Map;

    .line 323
    .local v4, "oldAvailableVoicesInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    invoke-static {v3}, Lcom/google/common/collect/ImmutableMap;->copyOf(Ljava/util/Map;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAvailableVoicesInfo:Ljava/util/Map;

    .line 329
    if-nez v4, :cond_1

    move v6, v7

    .line 330
    :goto_0
    if-eqz v4, :cond_2

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAvailableVoicesInfo:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    move v5, v7

    .line 334
    :goto_1
    if-eqz v6, :cond_3

    .line 335
    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mListeners:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;

    .line 336
    .local v2, "listener":Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;
    invoke-interface {v2}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;->onVoicesDataInit()V

    goto :goto_2

    .line 343
    .end local v0    # "filter":Lcom/google/android/tts/local/voicepack/MetadataListFilter;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;
    .end local v3    # "newAvailableVoicesInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    .end local v4    # "oldAvailableVoicesInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    :catchall_0
    move-exception v7

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .restart local v0    # "filter":Lcom/google/android/tts/local/voicepack/MetadataListFilter;
    .restart local v3    # "newAvailableVoicesInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    .restart local v4    # "oldAvailableVoicesInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    :cond_1
    move v6, v8

    .line 329
    goto :goto_0

    :cond_2
    move v5, v8

    .line 330
    goto :goto_1

    .line 338
    :cond_3
    if-eqz v5, :cond_4

    .line 339
    :try_start_1
    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mListeners:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;

    .line 340
    .restart local v2    # "listener":Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;
    invoke-interface {v2}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;->onVoicesDataChange()V

    goto :goto_3

    .line 343
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;
    :cond_4
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344
    return-void
.end method

.method public deleteVoiceData(Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;)V
    .locals 8
    .param p1, "voiceInfo"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .prologue
    .line 632
    iget-object v5, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 633
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getAbsoluteFilePath()Ljava/lang/String;

    move-result-object v2

    .line 636
    .local v2, "path":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x1

    invoke-virtual {p0, v4, v6}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->deleteVoiceDataFiles(Ljava/io/File;Z)Z

    move-result v3

    .line 639
    .local v3, "success":Z
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLocale()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lcom/google/android/tts/settings/TtsConfig;->getDefaultVoiceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 640
    .local v0, "defaultVoice":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 641
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLocale()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-interface {v4, v6, v7}, Lcom/google/android/tts/settings/TtsConfig;->setDefaultVoiceName(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->checkData()V

    .line 648
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->chooseRequiredDefaults()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 649
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->checkData()V

    .line 656
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.speech.tts.engine.TTS_DATA_INSTALLED"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 658
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "dataInstalled"

    if-eqz v3, :cond_2

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 660
    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 661
    monitor-exit v5

    .line 662
    return-void

    .line 658
    :cond_2
    const/4 v4, -0x1

    goto :goto_0

    .line 661
    .end local v0    # "defaultVoice":Ljava/lang/String;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "success":Z
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public deleteVoiceDataFiles(Ljava/io/File;Z)Z
    .locals 10
    .param p1, "dataDirectory"    # Ljava/io/File;
    .param p2, "expectToExist"    # Z

    .prologue
    .line 603
    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mLock:Ljava/lang/Object;

    monitor-enter v7

    .line 604
    const/4 v5, 0x1

    .line 605
    .local v5, "success":Z
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 606
    .local v2, "dataDirectoryName":Ljava/lang/String;
    if-nez p2, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 607
    const/4 v6, 0x1

    monitor-exit v7

    .line 627
    :goto_0
    return v6

    .line 609
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_2

    .line 610
    :cond_1
    sget-object v6, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error deleting a voice that is not installed: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    const/4 v6, 0x0

    monitor-exit v7

    goto :goto_0

    .line 628
    .end local v2    # "dataDirectoryName":Ljava/lang/String;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 614
    .restart local v2    # "dataDirectoryName":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_5

    aget-object v1, v0, v3

    .line 615
    .local v1, "child":Ljava/io/File;
    const-string v6, "."

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, ".."

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 614
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 618
    :cond_4
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_3

    .line 619
    const/4 v5, 0x0

    .line 620
    sget-object v6, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error deleting voice data: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 623
    .end local v1    # "child":Ljava/io/File;
    :cond_5
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_6

    .line 624
    const/4 v5, 0x0

    .line 625
    sget-object v6, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error deleting voice directory: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    :cond_6
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v6, v5

    goto/16 :goto_0
.end method

.method public enoughFreeSpaceForInstall(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z
    .locals 6
    .param p1, "voiceMetadata"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .prologue
    const/4 v0, 0x1

    .line 736
    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasUnpackedSizeKb()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 737
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getFreeSpaceInDataDirKb()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getUnpackedSizeKb()I

    move-result v1

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 739
    .local v0, "enough":Z
    :goto_0
    if-nez v0, :cond_0

    .line 740
    sget-object v1, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t install voice "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". required "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getUnpackedSizeKb()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "kb, free: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->getFreeSpaceInDataDirKb()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "kb"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    .end local v0    # "enough":Z
    :cond_0
    :goto_1
    return v0

    .line 737
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 746
    :cond_2
    sget-object v1, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Voice "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "has no unpacked size field"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getAutoInstallVoicesMetadata()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 696
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAvailableVoicesInfo:Ljava/util/Map;

    .line 697
    .local v0, "readVolatile":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAutoInstallableVoices:Ljava/util/Map;

    return-object v1
.end method

.method getAvailableLocales(Ljava/util/Map;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .prologue
    .line 681
    .local p1, "availableVoicesInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 682
    .local v2, "locales":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Locale;>;"
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    .line 683
    .local v1, "ivi":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    invoke-virtual {v1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;->getLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 685
    .end local v1    # "ivi":Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;
    :cond_0
    return-object v2
.end method

.method public getAvailableVoicesInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 677
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAvailableVoicesInfo:Ljava/util/Map;

    return-object v0
.end method

.method public getFreeSpaceInDataDirKb()J
    .locals 6

    .prologue
    .line 731
    new-instance v0, Landroid/os/StatFs;

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mPattsDataDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 732
    .local v0, "statFs":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x400

    div-long/2addr v2, v4

    return-wide v2
.end method

.method public getVoiceDataDir(Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 705
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 706
    :cond_0
    const/4 v0, 0x0

    .line 708
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mPattsDataDir:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public nukeOldDataDirs()V
    .locals 8

    .prologue
    .line 713
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mLock:Ljava/lang/Object;

    monitor-enter v6

    .line 714
    :try_start_0
    sget-object v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->PATTS_DATA_OLD_DIRS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 715
    .local v4, "oldDirName":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mContext:Landroid/content/Context;

    const/4 v7, 0x0

    invoke-virtual {v5, v4, v7}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    .line 716
    .local v3, "oldDataDir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 717
    invoke-direct {p0, v3}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->deleteRecursive(Ljava/io/File;)V

    .line 714
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 720
    .end local v3    # "oldDataDir":Ljava/io/File;
    .end local v4    # "oldDirName":Ljava/lang/String;
    :cond_1
    monitor-exit v6

    .line 721
    return-void

    .line 720
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public removeOldData()V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mPattsDataDir:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->removeUnsupportedVoicesFromDir(Ljava/io/File;)V

    .line 268
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->nukeOldDataDirs()V

    .line 269
    return-void
.end method

.method public removeVoicesDataListener(Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/tts/local/voicepack/VoiceDataManager$VoicesDataListener;

    .prologue
    .line 280
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 281
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 282
    monitor-exit v1

    .line 283
    return-void

    .line 282
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public voicePackInstallBegin(Ljava/lang/String;)V
    .locals 3
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 756
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 758
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mExcludedVoices:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 761
    new-instance v0, Ljava/util/HashMap;

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAvailableVoicesInfo:Ljava/util/Map;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAvailableVoicesInfo:Ljava/util/Map;

    .line 762
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAvailableVoicesInfo:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 763
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAvailableVoicesInfo:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableMap;->copyOf(Ljava/util/Map;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mAvailableVoicesInfo:Ljava/util/Map;

    .line 764
    monitor-exit v1

    .line 765
    return-void

    .line 764
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public voicePackInstallEnd(Ljava/lang/String;)V
    .locals 2
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 772
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 773
    :try_start_0
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->mExcludedVoices:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 774
    monitor-exit v1

    .line 775
    return-void

    .line 774
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
