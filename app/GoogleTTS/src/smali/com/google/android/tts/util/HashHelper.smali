.class public Lcom/google/android/tts/util/HashHelper;
.super Ljava/lang/Object;
.source "HashHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static verifyMD5Checksum([BLjava/io/InputStream;)Z
    .locals 7
    .param p0, "expectedMD5Checksum"    # [B
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    :try_start_0
    const-string v5, "MD5"

    invoke-static {v5}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 16
    .local v1, "digest":Ljava/security/MessageDigest;
    const/16 v5, 0x1000

    new-array v0, v5, [B

    .line 18
    .local v0, "buffer":[B
    const/4 v4, 0x0

    .line 20
    .local v4, "updatesCount":I
    :cond_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 21
    .local v3, "numRead":I
    if-lez v3, :cond_1

    .line 22
    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5, v3}, Ljava/security/MessageDigest;->update([BII)V

    .line 24
    :cond_1
    add-int/lit8 v4, v4, 0x1

    .line 25
    rem-int/lit16 v5, v4, 0x400

    if-nez v5, :cond_2

    .line 27
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 29
    :cond_2
    const/4 v5, -0x1

    if-ne v3, v5, :cond_0

    .line 30
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 32
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    invoke-static {p0, v5}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    return v5

    .line 33
    .end local v0    # "buffer":[B
    .end local v1    # "digest":Ljava/security/MessageDigest;
    .end local v3    # "numRead":I
    .end local v4    # "updatesCount":I
    :catch_0
    move-exception v2

    .line 34
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "MD5 is not supported"

    invoke-direct {v5, v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method
