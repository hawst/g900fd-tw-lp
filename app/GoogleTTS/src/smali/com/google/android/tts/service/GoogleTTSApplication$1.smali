.class Lcom/google/android/tts/service/GoogleTTSApplication$1;
.super Ljava/lang/Object;
.source "GoogleTTSApplication.java"

# interfaces
.implements Lcom/google/android/tts/util/SettingsHelper$DefaultTTSLocaleListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/tts/service/GoogleTTSApplication;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/tts/service/GoogleTTSApplication;


# direct methods
.method constructor <init>(Lcom/google/android/tts/service/GoogleTTSApplication;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/tts/service/GoogleTTSApplication$1;->this$0:Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDefaultTTSLocaleChange(Ljava/util/Locale;Ljava/util/Locale;)V
    .locals 3
    .param p1, "oldLocale"    # Ljava/util/Locale;
    .param p2, "newLocale"    # Ljava/util/Locale;

    .prologue
    .line 85
    sget-object v0, Lcom/google/android/tts/service/GoogleTTSApplication;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TTS Default locale changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication$1;->this$0:Lcom/google/android/tts/service/GoogleTTSApplication;

    # getter for: Lcom/google/android/tts/service/GoogleTTSApplication;->mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;
    invoke-static {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->access$000(Lcom/google/android/tts/service/GoogleTTSApplication;)Lcom/google/android/tts/util/PreferredLocales;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/tts/util/PreferredLocales;->updateTtsDefaultLocale(Ljava/util/Locale;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication$1;->this$0:Lcom/google/android/tts/service/GoogleTTSApplication;

    # getter for: Lcom/google/android/tts/service/GoogleTTSApplication;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;
    invoke-static {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->access$100(Lcom/google/android/tts/service/GoogleTTSApplication;)Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;->updateLocaleList()V

    .line 89
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication$1;->this$0:Lcom/google/android/tts/service/GoogleTTSApplication;

    # getter for: Lcom/google/android/tts/service/GoogleTTSApplication;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;
    invoke-static {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->access$200(Lcom/google/android/tts/service/GoogleTTSApplication;)Lcom/google/android/tts/network/NetworkVoicesManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/tts/network/NetworkVoicesManager;->checkData()V

    .line 90
    return-void
.end method
