.class public final Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "VoiceMetadataProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/voicepack/VoiceMetadataProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VoiceMetadata"
.end annotation


# instance fields
.field private autoInstall_:Z

.field private cachedSize:I

.field private description_:Ljava/lang/String;

.field private displaySpeakerName_:Ljava/lang/String;

.field private gender_:Ljava/lang/String;

.field private hasAutoInstall:Z

.field private hasDescription:Z

.field private hasDisplaySpeakerName:Z

.field private hasGender:Z

.field private hasLocale:Z

.field private hasMd5Checksum:Z

.field private hasMinApkVersionCode:Z

.field private hasName:Z

.field private hasPackedSizeKb:Z

.field private hasRevision:Z

.field private hasSynthesisLatency:Z

.field private hasSynthesisQuality:Z

.field private hasTag:Z

.field private hasUnpackedSizeKb:Z

.field private hasUrl:Z

.field private locale_:Ljava/lang/String;

.field private md5Checksum_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private minApkVersionCode_:J

.field private name_:Ljava/lang/String;

.field private packedSizeKb_:I

.field private revision_:I

.field private synthesisLatency_:I

.field private synthesisQuality_:I

.field private tag_:I

.field private unpackedSizeKb_:I

.field private url_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v1, 0x12c

    const/4 v2, 0x0

    .line 10
    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->name_:Ljava/lang/String;

    .line 38
    iput v1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->synthesisQuality_:I

    .line 55
    iput v1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->synthesisLatency_:I

    .line 72
    iput-boolean v2, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->autoInstall_:Z

    .line 89
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->locale_:Ljava/lang/String;

    .line 106
    iput v2, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->revision_:I

    .line 123
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->url_:Ljava/lang/String;

    .line 140
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->minApkVersionCode_:J

    .line 157
    iput v2, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->packedSizeKb_:I

    .line 174
    iput v2, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->unpackedSizeKb_:I

    .line 191
    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->md5Checksum_:Lcom/google/protobuf/micro/ByteStringMicro;

    .line 208
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->description_:Ljava/lang/String;

    .line 225
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->gender_:Ljava/lang/String;

    .line 242
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->tag_:I

    .line 259
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->displaySpeakerName_:Ljava/lang/String;

    .line 349
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->cachedSize:I

    .line 10
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 513
    new-instance v0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    invoke-direct {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAutoInstall()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->autoInstall_:Z

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    .prologue
    .line 352
    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->cachedSize:I

    if-gez v0, :cond_0

    .line 354
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getSerializedSize()I

    .line 356
    :cond_0
    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->cachedSize:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplaySpeakerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->displaySpeakerName_:Ljava/lang/String;

    return-object v0
.end method

.method public getGender()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->gender_:Ljava/lang/String;

    return-object v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->locale_:Ljava/lang/String;

    return-object v0
.end method

.method public getMd5Checksum()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->md5Checksum_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getMinApkVersionCode()J
    .locals 2

    .prologue
    .line 141
    iget-wide v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->minApkVersionCode_:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getPackedSizeKb()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->packedSizeKb_:I

    return v0
.end method

.method public getRevision()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->revision_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 361
    const/4 v0, 0x0

    .line 362
    .local v0, "size":I
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasLocale()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 363
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 366
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasUrl()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 367
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 370
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasRevision()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 371
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 374
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasPackedSizeKb()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 375
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getPackedSizeKb()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 378
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasDescription()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 379
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 382
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasMinApkVersionCode()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 383
    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getMinApkVersionCode()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 386
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasGender()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 387
    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getGender()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 390
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasName()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 391
    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 394
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasSynthesisQuality()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 395
    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getSynthesisQuality()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 398
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasSynthesisLatency()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 399
    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getSynthesisLatency()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 402
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasAutoInstall()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 403
    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getAutoInstall()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 406
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasTag()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 407
    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getTag()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 410
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasDisplaySpeakerName()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 411
    const/16 v1, 0xe

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getDisplaySpeakerName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 414
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasUnpackedSizeKb()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 415
    const/16 v1, 0xf

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getUnpackedSizeKb()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 418
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasMd5Checksum()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 419
    const/16 v1, 0x10

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getMd5Checksum()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v1

    add-int/2addr v0, v1

    .line 422
    :cond_e
    iput v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->cachedSize:I

    .line 423
    return v0
.end method

.method public getSynthesisLatency()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->synthesisLatency_:I

    return v0
.end method

.method public getSynthesisQuality()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->synthesisQuality_:I

    return v0
.end method

.method public getTag()I
    .locals 1

    .prologue
    .line 244
    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->tag_:I

    return v0
.end method

.method public getUnpackedSizeKb()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->unpackedSizeKb_:I

    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAutoInstall()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasAutoInstall:Z

    return v0
.end method

.method public hasDescription()Z
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasDescription:Z

    return v0
.end method

.method public hasDisplaySpeakerName()Z
    .locals 1

    .prologue
    .line 261
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasDisplaySpeakerName:Z

    return v0
.end method

.method public hasGender()Z
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasGender:Z

    return v0
.end method

.method public hasLocale()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasLocale:Z

    return v0
.end method

.method public hasMd5Checksum()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasMd5Checksum:Z

    return v0
.end method

.method public hasMinApkVersionCode()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasMinApkVersionCode:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasName:Z

    return v0
.end method

.method public hasPackedSizeKb()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasPackedSizeKb:Z

    return v0
.end method

.method public hasRevision()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasRevision:Z

    return v0
.end method

.method public hasSynthesisLatency()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasSynthesisLatency:Z

    return v0
.end method

.method public hasSynthesisQuality()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasSynthesisQuality:Z

    return v0
.end method

.method public hasTag()Z
    .locals 1

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasTag:Z

    return v0
.end method

.method public hasUnpackedSizeKb()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasUnpackedSizeKb:Z

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 431
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    .line 432
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 436
    invoke-virtual {p0, p1, v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 437
    :sswitch_0
    return-object p0

    .line 442
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setLocale(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 446
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setUrl(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 450
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setRevision(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 454
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setPackedSizeKb(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 458
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setDescription(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 462
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setMinApkVersionCode(J)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 466
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setGender(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 470
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setName(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 474
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setSynthesisQuality(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 478
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setSynthesisLatency(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 482
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setAutoInstall(Z)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 486
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setTag(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 490
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setDisplaySpeakerName(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 494
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setUnpackedSizeKb(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto :goto_0

    .line 498
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->setMd5Checksum(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    goto/16 :goto_0

    .line 432
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x68 -> :sswitch_c
        0x72 -> :sswitch_d
        0x78 -> :sswitch_e
        0x82 -> :sswitch_f
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7
    invoke-virtual {p0, p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v0

    return-object v0
.end method

.method public setAutoInstall(Z)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasAutoInstall:Z

    .line 77
    iput-boolean p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->autoInstall_:Z

    .line 78
    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasDescription:Z

    .line 213
    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->description_:Ljava/lang/String;

    .line 214
    return-object p0
.end method

.method public setDisplaySpeakerName(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 263
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasDisplaySpeakerName:Z

    .line 264
    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->displaySpeakerName_:Ljava/lang/String;

    .line 265
    return-object p0
.end method

.method public setGender(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 229
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasGender:Z

    .line 230
    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->gender_:Ljava/lang/String;

    .line 231
    return-object p0
.end method

.method public setLocale(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasLocale:Z

    .line 94
    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->locale_:Ljava/lang/String;

    .line 95
    return-object p0
.end method

.method public setMd5Checksum(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # Lcom/google/protobuf/micro/ByteStringMicro;

    .prologue
    .line 195
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasMd5Checksum:Z

    .line 196
    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->md5Checksum_:Lcom/google/protobuf/micro/ByteStringMicro;

    .line 197
    return-object p0
.end method

.method public setMinApkVersionCode(J)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasMinApkVersionCode:Z

    .line 145
    iput-wide p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->minApkVersionCode_:J

    .line 146
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasName:Z

    .line 26
    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->name_:Ljava/lang/String;

    .line 27
    return-object p0
.end method

.method public setPackedSizeKb(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasPackedSizeKb:Z

    .line 162
    iput p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->packedSizeKb_:I

    .line 163
    return-object p0
.end method

.method public setRevision(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasRevision:Z

    .line 111
    iput p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->revision_:I

    .line 112
    return-object p0
.end method

.method public setSynthesisLatency(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasSynthesisLatency:Z

    .line 60
    iput p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->synthesisLatency_:I

    .line 61
    return-object p0
.end method

.method public setSynthesisQuality(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasSynthesisQuality:Z

    .line 43
    iput p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->synthesisQuality_:I

    .line 44
    return-object p0
.end method

.method public setTag(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasTag:Z

    .line 247
    iput p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->tag_:I

    .line 248
    return-object p0
.end method

.method public setUnpackedSizeKb(I)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 178
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasUnpackedSizeKb:Z

    .line 179
    iput p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->unpackedSizeKb_:I

    .line 180
    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasUrl:Z

    .line 128
    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->url_:Ljava/lang/String;

    .line 129
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasLocale()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    .line 305
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 306
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    .line 308
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasRevision()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 309
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getRevision()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    .line 311
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasPackedSizeKb()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 312
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getPackedSizeKb()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    .line 314
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasDescription()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 315
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    .line 317
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasMinApkVersionCode()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 318
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getMinApkVersionCode()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    .line 320
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasGender()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 321
    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getGender()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    .line 323
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasName()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 324
    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    .line 326
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasSynthesisQuality()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 327
    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getSynthesisQuality()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    .line 329
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasSynthesisLatency()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 330
    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getSynthesisLatency()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    .line 332
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasAutoInstall()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 333
    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getAutoInstall()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    .line 335
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasTag()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 336
    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getTag()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    .line 338
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasDisplaySpeakerName()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 339
    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getDisplaySpeakerName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    .line 341
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasUnpackedSizeKb()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 342
    const/16 v0, 0xf

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getUnpackedSizeKb()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    .line 344
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->hasMd5Checksum()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 345
    const/16 v0, 0x10

    invoke-virtual {p0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getMd5Checksum()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    .line 347
    :cond_e
    return-void
.end method
