.class public Lcom/google/android/tts/service/GoogleTTSRequest;
.super Ljava/lang/Object;
.source "GoogleTTSRequest.java"


# instance fields
.field private final mRequest:Landroid/speech/tts/SynthesisRequest;

.field private final mSuppressVoiceName:Z


# direct methods
.method public constructor <init>(Landroid/speech/tts/SynthesisRequest;)V
    .locals 1
    .param p1, "wrapped"    # Landroid/speech/tts/SynthesisRequest;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mSuppressVoiceName:Z

    .line 48
    return-void
.end method

.method constructor <init>(Landroid/speech/tts/SynthesisRequest;Z)V
    .locals 0
    .param p1, "wrapped"    # Landroid/speech/tts/SynthesisRequest;
    .param p2, "suppressVoiceName"    # Z

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    .line 52
    iput-boolean p2, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mSuppressVoiceName:Z

    .line 53
    return-void
.end method

.method public static cloneWithoutVoiceName(Lcom/google/android/tts/service/GoogleTTSRequest;)Lcom/google/android/tts/service/GoogleTTSRequest;
    .locals 3
    .param p0, "request"    # Lcom/google/android/tts/service/GoogleTTSRequest;

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/tts/service/GoogleTTSRequest;

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/tts/service/GoogleTTSRequest;-><init>(Landroid/speech/tts/SynthesisRequest;Z)V

    return-object v0
.end method

.method private getSynthesisModeParam()Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    invoke-virtual {v0}, Landroid/speech/tts/SynthesisRequest;->getParams()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.tts:Mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static safeToLower(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 200
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getAndroidPitch()I
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    invoke-virtual {v0}, Landroid/speech/tts/SynthesisRequest;->getPitch()I

    move-result v0

    return v0
.end method

.method public getAndroidSpeechRate()I
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    invoke-virtual {v0}, Landroid/speech/tts/SynthesisRequest;->getSpeechRate()I

    move-result v0

    return v0
.end method

.method public getISO3Country()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    invoke-virtual {v0}, Landroid/speech/tts/SynthesisRequest;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/tts/service/GoogleTTSRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getISO3Language()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    invoke-virtual {v0}, Landroid/speech/tts/SynthesisRequest;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/tts/service/GoogleTTSRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getISO3Locale()Ljava/util/Locale;
    .locals 3

    .prologue
    .line 162
    new-instance v0, Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/tts/service/GoogleTTSRequest;->getISO3Country()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getRetryCount()I
    .locals 4

    .prologue
    .line 110
    const/4 v1, -0x1

    .line 111
    .local v1, "retryCount":I
    iget-object v3, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    invoke-virtual {v3}, Landroid/speech/tts/SynthesisRequest;->getParams()Landroid/os/Bundle;

    move-result-object v0

    .line 112
    .local v0, "requestParams":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v3, "networkRetriesCount"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 115
    const-string v3, "networkRetriesCount"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "retryCountStr":Ljava/lang/String;
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 123
    .end local v2    # "retryCountStr":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 119
    .restart local v2    # "retryCountStr":Ljava/lang/String;
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public getSpeechDurationMultiplier()F
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/google/android/tts/service/GoogleTTSRequest;->getAndroidSpeechRate()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, Lcom/google/android/tts/util/ConvertHelper;->androidSpeechRateToSpeechDuration(F)F

    move-result v0

    return v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 192
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    invoke-virtual {v0}, Landroid/speech/tts/SynthesisRequest;->getCharSequenceText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 195
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    invoke-virtual {v0}, Landroid/speech/tts/SynthesisRequest;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTimeout()I
    .locals 4

    .prologue
    .line 83
    const/4 v0, -0x1

    .line 84
    .local v0, "delay":I
    iget-object v3, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    invoke-virtual {v3}, Landroid/speech/tts/SynthesisRequest;->getParams()Landroid/os/Bundle;

    move-result-object v2

    .line 85
    .local v2, "requestParams":Landroid/os/Bundle;
    if-eqz v2, :cond_1

    const-string v3, "networkTimeoutMs"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 88
    const-string v3, "networkTimeoutMs"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, "delayStr":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 105
    .end local v1    # "delayStr":Ljava/lang/String;
    :cond_0
    :goto_0
    return v0

    .line 95
    :cond_1
    if-eqz v2, :cond_0

    const-string v3, "com.google.android.tts:NetworkTimeout"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 98
    const-string v3, "com.google.android.tts:NetworkTimeout"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 100
    .restart local v1    # "delayStr":Ljava/lang/String;
    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    goto :goto_0

    .line 92
    :catch_0
    move-exception v3

    goto :goto_0

    .line 101
    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method public getVoiceName()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 170
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 171
    iget-boolean v1, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mSuppressVoiceName:Z

    if-eqz v1, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-object v0

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    invoke-virtual {v0}, Landroid/speech/tts/SynthesisRequest;->getVoiceName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/tts/service/GoogleTTSRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isLocalOnly()Z
    .locals 3

    .prologue
    .line 67
    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    invoke-virtual {v1}, Landroid/speech/tts/SynthesisRequest;->getParams()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "embeddedTts"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "embeddedSynthValue":Ljava/lang/String;
    const-string v1, "LocalOnly"

    invoke-direct {p0}, Lcom/google/android/tts/service/GoogleTTSRequest;->getSynthesisModeParam()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isNetworkFirst()Z
    .locals 2

    .prologue
    .line 74
    const-string v0, "NetworkFirst"

    invoke-direct {p0}, Lcom/google/android/tts/service/GoogleTTSRequest;->getSynthesisModeParam()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isNetworkOnly()Z
    .locals 3

    .prologue
    .line 60
    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSRequest;->mRequest:Landroid/speech/tts/SynthesisRequest;

    invoke-virtual {v1}, Landroid/speech/tts/SynthesisRequest;->getParams()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "networkTts"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "networkSynthKey":Ljava/lang/String;
    const-string v1, "NetworkOnly"

    invoke-direct {p0}, Lcom/google/android/tts/service/GoogleTTSRequest;->getSynthesisModeParam()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
