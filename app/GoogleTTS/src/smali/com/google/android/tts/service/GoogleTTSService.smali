.class public Lcom/google/android/tts/service/GoogleTTSService;
.super Landroid/speech/tts/TextToSpeechService;
.source "GoogleTTSService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mLocal:Lcom/google/android/tts/local/LocalSynthesizer;

.field private mLocaleBasedDispatcher:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

.field private mLock:Ljava/lang/Object;

.field private mNetwork:Lcom/google/android/tts/network/NetworkSynthesizer;

.field private mNextLoadLanguageIsAsync:Z

.field private mTtsConfig:Lcom/google/android/tts/settings/TtsConfig;

.field private mVoiceBasedDispatcher:Lcom/google/android/tts/voices/VoiceBasedDispatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/google/android/tts/service/GoogleTTSService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/service/GoogleTTSService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/speech/tts/TextToSpeechService;-><init>()V

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mNextLoadLanguageIsAsync:Z

    .line 75
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLock:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/service/GoogleTTSService;)Lcom/google/android/tts/voices/LocaleBasedDispatcher;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/service/GoogleTTSService;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocaleBasedDispatcher:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 9

    .prologue
    .line 79
    invoke-static {p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v6

    .line 80
    .local v6, "app":Lcom/google/android/tts/service/GoogleTTSApplication;
    invoke-virtual {v6, p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->setService(Lcom/google/android/tts/service/GoogleTTSService;)V

    .line 81
    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getTtsConfig()Lcom/google/android/tts/settings/TtsConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfig;

    .line 83
    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVersionName()Ljava/lang/String;

    move-result-object v8

    .line 84
    .local v8, "versionName":Ljava/lang/String;
    sget-object v0, Lcom/google/android/tts/service/GoogleTTSService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating Google TTS service, version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v3

    .line 88
    .local v3, "voiceDataManager":Lcom/google/android/tts/local/voicepack/VoiceDataManager;
    new-instance v0, Lcom/google/android/tts/network/NetworkSynthesizer;

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSService;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getNetworkVoicesManager()Lcom/google/android/tts/network/NetworkVoicesManager;

    move-result-object v2

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getUrlRewriter()Lcom/google/android/common/base/Function;

    move-result-object v4

    invoke-direct {v0, p0, v1, v2, v4}, Lcom/google/android/tts/network/NetworkSynthesizer;-><init>(Landroid/content/Context;Lcom/google/android/tts/settings/TtsConfig;Lcom/google/android/tts/network/NetworkVoicesManager;Lcom/google/android/common/base/Function;)V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mNetwork:Lcom/google/android/tts/network/NetworkSynthesizer;

    .line 91
    new-instance v0, Lcom/google/android/tts/local/LocalSynthesizer;

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getLegacyVoiceResolver()Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    move-result-object v2

    invoke-virtual {v6}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataDownloader()Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/tts/service/GoogleTTSService;->mNetwork:Lcom/google/android/tts/network/NetworkSynthesizer;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/tts/local/LocalSynthesizer;-><init>(Landroid/content/Context;Lcom/google/android/tts/local/LocalLegacyVoiceResolver;Lcom/google/android/tts/local/voicepack/VoiceDataManager;Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;Lcom/google/android/tts/voices/Synthesizer;)V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocal:Lcom/google/android/tts/local/LocalSynthesizer;

    .line 96
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mNetwork:Lcom/google/android/tts/network/NetworkSynthesizer;

    invoke-virtual {v0}, Lcom/google/android/tts/network/NetworkSynthesizer;->onInit()V

    .line 97
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocal:Lcom/google/android/tts/local/LocalSynthesizer;

    invoke-virtual {v0}, Lcom/google/android/tts/local/LocalSynthesizer;->onInit()V

    .line 100
    new-instance v7, Lcom/google/android/tts/service/GoogleTTSService$1;

    invoke-direct {v7, p0}, Lcom/google/android/tts/service/GoogleTTSService$1;-><init>(Lcom/google/android/tts/service/GoogleTTSService;)V

    .line 109
    .local v7, "networkStatus":Lcom/google/android/tts/util/NetworkStatus;
    new-instance v0, Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocal:Lcom/google/android/tts/local/LocalSynthesizer;

    iget-object v2, p0, Lcom/google/android/tts/service/GoogleTTSService;->mNetwork:Lcom/google/android/tts/network/NetworkSynthesizer;

    iget-object v4, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocal:Lcom/google/android/tts/local/LocalSynthesizer;

    invoke-direct {v0, v7, v1, v2, v4}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;-><init>(Lcom/google/android/tts/util/NetworkStatus;Lcom/google/android/tts/voices/Synthesizer;Lcom/google/android/tts/voices/Synthesizer;Lcom/google/android/tts/util/DownloadEnabler;)V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocaleBasedDispatcher:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    .line 113
    new-instance v0, Lcom/google/android/tts/voices/VoiceBasedDispatcher;

    invoke-direct {v0}, Lcom/google/android/tts/voices/VoiceBasedDispatcher;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mVoiceBasedDispatcher:Lcom/google/android/tts/voices/VoiceBasedDispatcher;

    .line 114
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mVoiceBasedDispatcher:Lcom/google/android/tts/voices/VoiceBasedDispatcher;

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocal:Lcom/google/android/tts/local/LocalSynthesizer;

    invoke-virtual {v0, v1}, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->addSynthesizer(Lcom/google/android/tts/voices/VoiceSynthesizer;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mVoiceBasedDispatcher:Lcom/google/android/tts/voices/VoiceBasedDispatcher;

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSService;->mNetwork:Lcom/google/android/tts/network/NetworkSynthesizer;

    invoke-virtual {v0, v1}, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->addSynthesizer(Lcom/google/android/tts/voices/VoiceSynthesizer;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mVoiceBasedDispatcher:Lcom/google/android/tts/voices/VoiceBasedDispatcher;

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocaleBasedDispatcher:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v1}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->createVoiceSynthesizer()Lcom/google/android/tts/voices/VoiceSynthesizer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->addSynthesizer(Lcom/google/android/tts/voices/VoiceSynthesizer;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mVoiceBasedDispatcher:Lcom/google/android/tts/voices/VoiceBasedDispatcher;

    invoke-virtual {v0}, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->updateVoicesSet()V

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mNextLoadLanguageIsAsync:Z

    .line 122
    invoke-super {p0}, Landroid/speech/tts/TextToSpeechService;->onCreate()V

    .line 123
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 127
    invoke-static {p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/tts/service/GoogleTTSApplication;->setService(Lcom/google/android/tts/service/GoogleTTSService;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mNetwork:Lcom/google/android/tts/network/NetworkSynthesizer;

    invoke-virtual {v0}, Lcom/google/android/tts/network/NetworkSynthesizer;->onClose()V

    .line 129
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocal:Lcom/google/android/tts/local/LocalSynthesizer;

    invoke-virtual {v0}, Lcom/google/android/tts/local/LocalSynthesizer;->onClose()V

    .line 130
    invoke-super {p0}, Landroid/speech/tts/TextToSpeechService;->onDestroy()V

    .line 131
    return-void
.end method

.method public onGetDefaultVoiceNameFor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;
    .param p3, "variant"    # Ljava/lang/String;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocaleBasedDispatcher:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->getDefaultVoiceNameFor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onGetFeaturesForLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;
    .locals 7
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;
    .param p3, "variant"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 197
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 199
    .local v1, "features":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 200
    .local v2, "langLower":Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/tts/service/GoogleTTSRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "countryLower":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/tts/service/GoogleTTSService;->mNetwork:Lcom/google/android/tts/network/NetworkSynthesizer;

    invoke-virtual {v5, v2, v0}, Lcom/google/android/tts/network/NetworkSynthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 205
    .local v4, "networkAvailability":I
    iget-object v5, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocal:Lcom/google/android/tts/local/LocalSynthesizer;

    invoke-virtual {v5, v2, v0}, Lcom/google/android/tts/local/LocalSynthesizer;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 207
    .local v3, "localAvailability":I
    if-eqz v4, :cond_0

    if-ne v4, v6, :cond_1

    .line 210
    :cond_0
    const-string v5, "networkTts"

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_1
    if-eqz v3, :cond_2

    if-ne v3, v6, :cond_3

    .line 218
    :cond_2
    const-string v5, "embeddedTts"

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 221
    :cond_3
    return-object v1
.end method

.method protected onGetLanguage()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocaleBasedDispatcher:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v0}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->getLanguage()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onGetVoices()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/speech/tts/Voice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mVoiceBasedDispatcher:Lcom/google/android/tts/voices/VoiceBasedDispatcher;

    invoke-virtual {v0}, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->getVoices()Ljava/util/Collection;

    move-result-object v8

    .line 227
    .local v8, "internalVoices":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/tts/voices/InternalVoice;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 228
    .local v9, "resultVoices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/speech/tts/Voice;>;"
    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/tts/voices/InternalVoice;

    .line 229
    .local v10, "voice":Lcom/google/android/tts/voices/InternalVoice;
    new-instance v0, Landroid/speech/tts/Voice;

    invoke-virtual {v10}, Lcom/google/android/tts/voices/InternalVoice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10}, Lcom/google/android/tts/voices/InternalVoice;->getLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v10}, Lcom/google/android/tts/voices/InternalVoice;->getQuality()I

    move-result v3

    invoke-virtual {v10}, Lcom/google/android/tts/voices/InternalVoice;->getLatency()I

    move-result v4

    invoke-virtual {v10}, Lcom/google/android/tts/voices/InternalVoice;->getRequiresNetworkConnection()Z

    move-result v5

    invoke-virtual {v10}, Lcom/google/android/tts/voices/InternalVoice;->getFeatures()Ljava/util/Set;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Landroid/speech/tts/Voice;-><init>(Ljava/lang/String;Ljava/util/Locale;IIZLjava/util/Set;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 233
    .end local v10    # "voice":Lcom/google/android/tts/voices/InternalVoice;
    :cond_0
    return-object v9
.end method

.method protected onIsLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;
    .param p3, "unusedVariant"    # Ljava/lang/String;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocaleBasedDispatcher:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-static {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/tts/service/GoogleTTSRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public onIsValidVoiceName(Ljava/lang/String;)I
    .locals 1
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mVoiceBasedDispatcher:Lcom/google/android/tts/voices/VoiceBasedDispatcher;

    invoke-virtual {v0, p1}, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->isValidVoiceName(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected onLoadLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;
    .param p3, "unusedVariant"    # Ljava/lang/String;

    .prologue
    .line 153
    invoke-static {p1}, Lcom/google/android/tts/service/GoogleTTSRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "langLower":Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/tts/service/GoogleTTSRequest;->safeToLower(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "countryLower":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 158
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/tts/service/GoogleTTSService;->mNextLoadLanguageIsAsync:Z

    if-eqz v2, :cond_0

    .line 159
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/tts/service/GoogleTTSService;->mNextLoadLanguageIsAsync:Z

    .line 160
    new-instance v2, Lcom/google/android/tts/service/GoogleTTSService$2;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/tts/service/GoogleTTSService$2;-><init>(Lcom/google/android/tts/service/GoogleTTSService;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 166
    iget-object v2, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocaleBasedDispatcher:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    monitor-exit v3

    .line 169
    :goto_0
    return v2

    .line 168
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    iget-object v2, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocaleBasedDispatcher:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 168
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public onLoadVoice(Ljava/lang/String;)I
    .locals 1
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mVoiceBasedDispatcher:Lcom/google/android/tts/voices/VoiceBasedDispatcher;

    invoke-virtual {v0, p1}, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->onLoadVoice(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mNetwork:Lcom/google/android/tts/network/NetworkSynthesizer;

    invoke-virtual {v0}, Lcom/google/android/tts/network/NetworkSynthesizer;->onStop()V

    .line 175
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocal:Lcom/google/android/tts/local/LocalSynthesizer;

    invoke-virtual {v0}, Lcom/google/android/tts/local/LocalSynthesizer;->onStop()V

    .line 176
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocaleBasedDispatcher:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v0}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->onStop()V

    .line 177
    return-void
.end method

.method protected onSynthesizeText(Landroid/speech/tts/SynthesisRequest;Landroid/speech/tts/SynthesisCallback;)V
    .locals 4
    .param p1, "request"    # Landroid/speech/tts/SynthesisRequest;
    .param p2, "callback"    # Landroid/speech/tts/SynthesisCallback;

    .prologue
    .line 183
    new-instance v2, Lcom/google/android/tts/service/GoogleTTSRequest;

    invoke-direct {v2, p1}, Lcom/google/android/tts/service/GoogleTTSRequest;-><init>(Landroid/speech/tts/SynthesisRequest;)V

    .line 184
    .local v2, "wrappedRequest":Lcom/google/android/tts/service/GoogleTTSRequest;
    invoke-virtual {v2}, Lcom/google/android/tts/service/GoogleTTSRequest;->getVoiceName()Ljava/lang/String;

    move-result-object v1

    .line 185
    .local v1, "voiceName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 186
    iget-object v3, p0, Lcom/google/android/tts/service/GoogleTTSService;->mVoiceBasedDispatcher:Lcom/google/android/tts/voices/VoiceBasedDispatcher;

    invoke-virtual {v3, p1}, Lcom/google/android/tts/voices/VoiceBasedDispatcher;->getSynthesizerFor(Landroid/speech/tts/SynthesisRequest;)Lcom/google/android/tts/voices/VoiceSynthesizer;

    move-result-object v0

    .line 187
    .local v0, "synthesizer":Lcom/google/android/tts/voices/Synthesizer;
    if-eqz v0, :cond_0

    .line 188
    invoke-interface {v0, v2, p2}, Lcom/google/android/tts/voices/Synthesizer;->onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V

    .line 193
    .end local v0    # "synthesizer":Lcom/google/android/tts/voices/Synthesizer;
    :goto_0
    return-void

    .line 192
    :cond_0
    iget-object v3, p0, Lcom/google/android/tts/service/GoogleTTSService;->mLocaleBasedDispatcher:Lcom/google/android/tts/voices/LocaleBasedDispatcher;

    invoke-virtual {v3, v2, p2}, Lcom/google/android/tts/voices/LocaleBasedDispatcher;->onSynthesize(Lcom/google/android/tts/service/GoogleTTSRequest;Landroid/speech/tts/SynthesisCallback;)V

    goto :goto_0
.end method
