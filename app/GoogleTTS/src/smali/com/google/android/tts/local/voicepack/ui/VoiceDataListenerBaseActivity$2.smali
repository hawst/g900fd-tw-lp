.class Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity$2;
.super Ljava/lang/Object;
.source "VoiceDataListenerBaseActivity.java"

# interfaces
.implements Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;


# direct methods
.method constructor <init>(Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity$2;->this$0:Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVoiceDownloadFail(Ljava/lang/String;)V
    .locals 1
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity$2;->this$0:Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;

    # invokes: Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->updateVoiceData()V
    invoke-static {v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->access$000(Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;)V

    .line 72
    return-void
.end method

.method public onVoiceDownloadStart(Ljava/lang/String;)V
    .locals 1
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity$2;->this$0:Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;

    # invokes: Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->updateVoiceData()V
    invoke-static {v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->access$000(Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;)V

    .line 62
    return-void
.end method

.method public onVoiceDownloadSuccess(Ljava/lang/String;)V
    .locals 1
    .param p1, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity$2;->this$0:Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;

    # invokes: Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->updateVoiceData()V
    invoke-static {v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;->access$000(Lcom/google/android/tts/local/voicepack/ui/VoiceDataListenerBaseActivity;)V

    .line 67
    return-void
.end method
