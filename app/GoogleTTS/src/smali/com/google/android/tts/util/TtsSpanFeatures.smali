.class public Lcom/google/android/tts/util/TtsSpanFeatures;
.super Ljava/lang/Object;
.source "TtsSpanFeatures.java"


# static fields
.field public static ARG_CELL_ID:Ljava/lang/String;

.field public static ARG_FPRINT:Ljava/lang/String;

.field public static ARG_LATITUDE_E7:Ljava/lang/String;

.field public static ARG_LONGITUDE_E7:Ljava/lang/String;

.field public static ARG_PRONUNCIATION:Ljava/lang/String;

.field public static TYPE_LOCATION:Ljava/lang/String;

.field public static TYPE_PRONUNCIATION:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string v0, "android.type.pronunciation"

    sput-object v0, Lcom/google/android/tts/util/TtsSpanFeatures;->TYPE_PRONUNCIATION:Ljava/lang/String;

    .line 23
    const-string v0, "android.type.location"

    sput-object v0, Lcom/google/android/tts/util/TtsSpanFeatures;->TYPE_LOCATION:Ljava/lang/String;

    .line 29
    const-string v0, "android.pronunciation"

    sput-object v0, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_PRONUNCIATION:Ljava/lang/String;

    .line 39
    const-string v0, "android.latitude_e7"

    sput-object v0, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_LATITUDE_E7:Ljava/lang/String;

    .line 48
    const-string v0, "android.longitude_e7"

    sput-object v0, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_LONGITUDE_E7:Ljava/lang/String;

    .line 55
    const-string v0, "android.cell_id"

    sput-object v0, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_CELL_ID:Ljava/lang/String;

    .line 63
    const-string v0, "android.fprint"

    sput-object v0, Lcom/google/android/tts/util/TtsSpanFeatures;->ARG_FPRINT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
