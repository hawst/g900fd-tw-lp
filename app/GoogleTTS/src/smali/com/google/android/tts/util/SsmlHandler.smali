.class Lcom/google/android/tts/util/SsmlHandler;
.super Ljava/lang/Object;
.source "SsmlHandler.java"

# interfaces
.implements Lorg/xml/sax/ContentHandler;


# static fields
.field static final DEFAULT_BREAK_SEC:F = 0.2f
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field private mCurrentBuilder:Ljava/lang/StringBuilder;

.field private final mSayList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Say;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/tts/Say;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "sayList":Ljava/util/List;, "Ljava/util/List<Lcom/google/speech/tts/Say;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/tts/util/SsmlHandler;->mCurrentBuilder:Ljava/lang/StringBuilder;

    .line 44
    iput-object p1, p0, Lcom/google/android/tts/util/SsmlHandler;->mSayList:Ljava/util/List;

    .line 45
    return-void
.end method

.method private maybeAppendCurrentString()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 158
    iget-object v1, p0, Lcom/google/android/tts/util/SsmlHandler;->mCurrentBuilder:Ljava/lang/StringBuilder;

    if-eqz v1, :cond_0

    .line 159
    iget-object v1, p0, Lcom/google/android/tts/util/SsmlHandler;->mCurrentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "current":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/tts/util/SsmlHandler;->mSayList:Ljava/util/List;

    invoke-static {v0, v3}, Lcom/google/android/tts/util/MarkupHelper;->createSayText(Ljava/lang/String;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/Say;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    iput-object v3, p0, Lcom/google/android/tts/util/SsmlHandler;->mCurrentBuilder:Ljava/lang/StringBuilder;

    .line 164
    .end local v0    # "current":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private parseTimeAttributeToSeconds(Ljava/lang/String;)F
    .locals 7
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const v4, 0x3e4ccccd    # 0.2f

    .line 121
    const/4 v2, 0x0

    .line 122
    .local v2, "numberPart":Ljava/lang/String;
    const/4 v0, 0x0

    .line 123
    .local v0, "isSeconds":Z
    const-string v5, "ms"

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_2

    .line 124
    const-string v5, "ms"

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 136
    :goto_0
    const/4 v3, 0x0

    .line 138
    .local v3, "silence":F
    :try_start_0
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    long-to-float v3, v4

    .line 144
    if-nez v0, :cond_0

    .line 145
    const/high16 v4, 0x447a0000    # 1000.0f

    div-float/2addr v3, v4

    .line 148
    :cond_0
    const/4 v4, 0x0

    cmpg-float v4, v3, v4

    if-gez v4, :cond_4

    .line 149
    const v3, 0x3e4ccccd    # 0.2f

    .line 154
    .end local v3    # "silence":F
    :cond_1
    :goto_1
    return v3

    .line 125
    :cond_2
    const-string v5, "s"

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_3

    .line 126
    const-string v5, "s"

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 127
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v3, v4

    .line 130
    goto :goto_1

    .line 139
    .restart local v3    # "silence":F
    :catch_0
    move-exception v1

    .local v1, "nfe":Ljava/lang/NumberFormatException;
    move v3, v4

    .line 140
    goto :goto_1

    .line 150
    .end local v1    # "nfe":Ljava/lang/NumberFormatException;
    :cond_4
    const/high16 v4, 0x41200000    # 10.0f

    cmpl-float v4, v3, v4

    if-lez v4, :cond_1

    .line 151
    const/high16 v3, 0x41200000    # 10.0f

    goto :goto_1
.end method


# virtual methods
.method public characters([CII)V
    .locals 2
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/tts/util/SsmlHandler;->mCurrentBuilder:Ljava/lang/StringBuilder;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/tts/util/SsmlHandler;->mCurrentBuilder:Ljava/lang/StringBuilder;

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/tts/util/SsmlHandler;->mCurrentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/google/android/tts/util/SsmlHandler;->mCurrentBuilder:Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/google/android/tts/util/SsmlHandler;->mCurrentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 57
    return-void
.end method

.method public endDocument()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/tts/util/SsmlHandler;->maybeAppendCurrentString()V

    .line 62
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;

    .prologue
    .line 65
    return-void
.end method

.method public endPrefixMapping(Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 68
    return-void
.end method

.method handleBreakElement(Ljava/lang/String;)V
    .locals 3
    .param p1, "time"    # Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/tts/util/SsmlHandler;->maybeAppendCurrentString()V

    .line 103
    const v0, 0x3e4ccccd    # 0.2f

    .line 106
    .local v0, "breakDurationS":F
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 107
    invoke-direct {p0, p1}, Lcom/google/android/tts/util/SsmlHandler;->parseTimeAttributeToSeconds(Ljava/lang/String;)F

    move-result v0

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/google/android/tts/util/SsmlHandler;->mSayList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/tts/util/MarkupHelper;->createSayPause(FLcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/Say;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    return-void
.end method

.method public ignorableWhitespace([CII)V
    .locals 0
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 71
    return-void
.end method

.method public processingInstruction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;

    .prologue
    .line 74
    return-void
.end method

.method public setDocumentLocator(Lorg/xml/sax/Locator;)V
    .locals 0
    .param p1, "locator"    # Lorg/xml/sax/Locator;

    .prologue
    .line 77
    return-void
.end method

.method public skippedEntity(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 80
    return-void
.end method

.method public startDocument()V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "atts"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 88
    const-string v0, "break"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const-string v0, ""

    const-string v1, "time"

    invoke-interface {p4, v0, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/tts/util/SsmlHandler;->handleBreakElement(Ljava/lang/String;)V

    .line 91
    :cond_0
    return-void
.end method

.method public startPrefixMapping(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 94
    return-void
.end method
