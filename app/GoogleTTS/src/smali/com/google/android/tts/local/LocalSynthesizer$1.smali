.class Lcom/google/android/tts/local/LocalSynthesizer$1;
.super Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;
.source "LocalSynthesizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/tts/local/LocalSynthesizer;-><init>(Landroid/content/Context;Lcom/google/android/tts/local/LocalLegacyVoiceResolver;Lcom/google/android/tts/local/voicepack/VoiceDataManager;Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;Lcom/google/android/tts/voices/Synthesizer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/tts/local/LocalSynthesizer;


# direct methods
.method constructor <init>(Lcom/google/android/tts/local/LocalSynthesizer;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/tts/local/LocalSynthesizer$1;->this$0:Lcom/google/android/tts/local/LocalSynthesizer;

    invoke-direct {p0}, Lcom/google/speech/patts/api/android/AndroidSpeechSynthesizer;-><init>()V

    return-void
.end method


# virtual methods
.method protected loadJni()Z
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/tts/local/LocalSynthesizer$1;->this$0:Lcom/google/android/tts/local/LocalSynthesizer;

    # getter for: Lcom/google/android/tts/local/LocalSynthesizer;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/tts/local/LocalSynthesizer;->access$000(Lcom/google/android/tts/local/LocalSynthesizer;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "patts_engine_jni_api"

    invoke-static {v0, v1}, Lcom/google/android/tts/service/PattsJniLoader;->loadJni(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
