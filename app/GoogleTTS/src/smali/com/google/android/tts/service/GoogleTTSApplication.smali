.class public Lcom/google/android/tts/service/GoogleTTSApplication;
.super Landroid/app/Application;
.source "GoogleTTSApplication.java"


# static fields
.field protected static final TAG:Ljava/lang/String;

.field private static sThisPackageInfo:Landroid/content/pm/PackageInfo;


# instance fields
.field private mGoogleTTSService:Lcom/google/android/tts/service/GoogleTTSService;

.field private mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

.field private mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

.field private mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;

.field private mTtsConfig:Lcom/google/android/tts/settings/TtsConfigImpl;

.field private mTtsDefaultLocaleObserver:Landroid/database/ContentObserver;

.field private mUrlRewriter:Lcom/google/android/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

.field private mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

.field private mVoiceDataUpdater:Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;

.field private mVoiceMetadataListManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/tts/service/GoogleTTSApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/service/GoogleTTSApplication;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/service/GoogleTTSApplication;)Lcom/google/android/tts/util/PreferredLocales;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/service/GoogleTTSApplication;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/tts/service/GoogleTTSApplication;)Lcom/google/android/tts/local/LocalLegacyVoiceResolver;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/service/GoogleTTSApplication;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/tts/service/GoogleTTSApplication;)Lcom/google/android/tts/network/NetworkVoicesManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/service/GoogleTTSApplication;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    return-object v0
.end method

.method public static get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 99
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/service/GoogleTTSApplication;

    return-object v0
.end method

.method private static declared-synchronized getPkgInfo(Landroid/content/Context;)Landroid/content/pm/PackageInfo;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 137
    const-class v4, Lcom/google/android/tts/service/GoogleTTSApplication;

    monitor-enter v4

    :try_start_0
    sget-object v3, Lcom/google/android/tts/service/GoogleTTSApplication;->sThisPackageInfo:Landroid/content/pm/PackageInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    .line 139
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 140
    .local v2, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 141
    .local v1, "pkgInfo":Landroid/content/pm/PackageInfo;
    sput-object v1, Lcom/google/android/tts/service/GoogleTTSApplication;->sThisPackageInfo:Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    .end local v1    # "pkgInfo":Landroid/content/pm/PackageInfo;
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    :try_start_2
    sget-object v3, Lcom/google/android/tts/service/GoogleTTSApplication;->sThisPackageInfo:Landroid/content/pm/PackageInfo;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v4

    return-object v3

    .line 142
    :catch_0
    move-exception v0

    .line 145
    .local v0, "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_3
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 137
    .end local v0    # "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method


# virtual methods
.method public getLegacyVoiceResolver()Lcom/google/android/tts/local/LocalLegacyVoiceResolver;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    return-object v0
.end method

.method public getNetworkVoicesManager()Lcom/google/android/tts/network/NetworkVoicesManager;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    return-object v0
.end method

.method public getTtsConfig()Lcom/google/android/tts/settings/TtsConfig;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfigImpl;

    return-object v0
.end method

.method public getUrlRewriter()Lcom/google/android/common/base/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mUrlRewriter:Lcom/google/android/common/base/Function;

    return-object v0
.end method

.method public getVersionCode()I
    .locals 1

    .prologue
    .line 152
    invoke-static {p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getPkgInfo(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    invoke-static {p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getPkgInfo(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    return-object v0
.end method

.method public getVoiceDataDownloader()Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    return-object v0
.end method

.method public getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    return-object v0
.end method

.method public getVoiceMetadataListManager()Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceMetadataListManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    .line 52
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 54
    new-instance v0, Lcom/google/android/tts/settings/TtsConfigImpl;

    invoke-direct {v0, p0}, Lcom/google/android/tts/settings/TtsConfigImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfigImpl;

    .line 55
    new-instance v0, Lcom/google/android/tts/settings/GservicesUrlRewriter;

    invoke-direct {v0, p0}, Lcom/google/android/tts/settings/GservicesUrlRewriter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mUrlRewriter:Lcom/google/android/common/base/Function;

    .line 57
    new-instance v0, Lcom/google/android/tts/util/PreferredLocales;

    const v1, 0x7f050001

    invoke-static {p0}, Lcom/google/android/tts/util/SettingsHelper;->getPreferedLocaleFromSettings(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/tts/util/PreferredLocales;-><init>(Landroid/content/Context;ILjava/util/Locale;)V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;

    .line 59
    new-instance v0, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfigImpl;

    iget-object v2, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mUrlRewriter:Lcom/google/android/common/base/Function;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;-><init>(Landroid/content/Context;Lcom/google/android/tts/settings/TtsConfig;Lcom/google/android/common/base/Function;)V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceMetadataListManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    .line 60
    new-instance v0, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceMetadataListManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    iget-object v2, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfigImpl;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;-><init>(Landroid/content/Context;Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;Lcom/google/android/tts/settings/TtsConfig;)V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    .line 62
    new-instance v0, Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    iget-object v2, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfigImpl;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;-><init>(Landroid/content/Context;Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;Lcom/google/android/tts/settings/TtsConfig;)V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    .line 63
    new-instance v0, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    iget-object v2, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfigImpl;

    iget-object v3, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/tts/local/LocalLegacyVoiceResolver;-><init>(Lcom/google/android/tts/local/voicepack/VoiceDataManager;Lcom/google/android/tts/settings/TtsConfig;Lcom/google/android/tts/util/PreferredLocales;)V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mLegacyVoiceResolver:Lcom/google/android/tts/local/LocalLegacyVoiceResolver;

    .line 65
    new-instance v1, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;

    invoke-virtual {p0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVersionCode()I

    move-result v0

    int-to-long v2, v0

    iget-object v4, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    check-cast v4, Lcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;

    iget-object v5, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    iget-object v6, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfigImpl;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;-><init>(JLcom/google/android/tts/local/voicepack/lorry/LorryVoiceDataDownloader;Lcom/google/android/tts/local/voicepack/VoiceDataManager;Lcom/google/android/tts/settings/TtsConfig;)V

    iput-object v1, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataUpdater:Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;

    .line 67
    new-instance v0, Lcom/google/android/tts/network/NetworkVoicesManager;

    const/high16 v1, 0x7f050000

    const v2, 0x7f050002

    iget-object v3, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mPreferredLocales:Lcom/google/android/tts/util/PreferredLocales;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/tts/network/NetworkVoicesManager;-><init>(Landroid/content/Context;IILcom/google/android/tts/util/PreferredLocales;)V

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mNetworkVoicesManager:Lcom/google/android/tts/network/NetworkVoicesManager;

    .line 71
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceMetadataListManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->forceEvictionAndUpdate()V

    .line 72
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->removeOldData()V

    .line 73
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->checkData()V

    .line 77
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataUpdater:Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceMetadataListManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    invoke-virtual {v1}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->getVoiceMetadataListProto()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;->downloadUpdatedVoices(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceMetadataListManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    iget-object v1, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mVoiceDataUpdater:Lcom/google/android/tts/local/voicepack/lorry/VoiceDataUpdater;

    invoke-virtual {v0, v1}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->addChangeListener(Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager$ChangeListener;)V

    .line 81
    new-instance v0, Lcom/google/android/tts/service/GoogleTTSApplication$1;

    invoke-direct {v0, p0}, Lcom/google/android/tts/service/GoogleTTSApplication$1;-><init>(Lcom/google/android/tts/service/GoogleTTSApplication;)V

    invoke-static {p0, v0}, Lcom/google/android/tts/util/SettingsHelper;->registerTTSDefaultLocaleListener(Landroid/content/Context;Lcom/google/android/tts/util/SettingsHelper$DefaultTTSLocaleListener;)Landroid/database/ContentObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mTtsDefaultLocaleObserver:Landroid/database/ContentObserver;

    .line 92
    return-void
.end method

.method public onTerminate()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mTtsDefaultLocaleObserver:Landroid/database/ContentObserver;

    invoke-static {p0, v0}, Lcom/google/android/tts/util/SettingsHelper;->unregisterTTSDefaultLocaleListener(Landroid/content/Context;Landroid/database/ContentObserver;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfigImpl;

    invoke-virtual {v0}, Lcom/google/android/tts/settings/TtsConfigImpl;->onClose()V

    .line 134
    return-void
.end method

.method setService(Lcom/google/android/tts/service/GoogleTTSService;)V
    .locals 2
    .param p1, "service"    # Lcom/google/android/tts/service/GoogleTTSService;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mGoogleTTSService:Lcom/google/android/tts/service/GoogleTTSService;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 161
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Setting new tts service reference while old one  is not cleared"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_0
    iput-object p1, p0, Lcom/google/android/tts/service/GoogleTTSApplication;->mGoogleTTSService:Lcom/google/android/tts/service/GoogleTTSService;

    .line 165
    return-void
.end method
