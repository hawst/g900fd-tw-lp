.class public final Lcom/google/android/tts/util/ISO3LocaleMap;
.super Ljava/lang/Object;
.source "ISO3LocaleMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/util/ISO3LocaleMap$1;,
        Lcom/google/android/tts/util/ISO3LocaleMap$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected final mLocaleToEntry:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/util/Locale;",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/android/tts/util/ISO3LocaleMap;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/util/ISO3LocaleMap;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/util/Locale;",
            "TT;>;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<TT;>;"
    .local p1, "localeToEntry":Ljava/util/Map;, "Ljava/util/Map<Ljava/util/Locale;TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/tts/util/ISO3LocaleMap;->mLocaleToEntry:Ljava/util/Map;

    .line 24
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Map;Lcom/google/android/tts/util/ISO3LocaleMap$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/Map;
    .param p2, "x1"    # Lcom/google/android/tts/util/ISO3LocaleMap$1;

    .prologue
    .line 16
    .local p0, "this":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<TT;>;"
    invoke-direct {p0, p1}, Lcom/google/android/tts/util/ISO3LocaleMap;-><init>(Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$000(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 1
    .param p0, "x0"    # Ljava/util/Locale;

    .prologue
    .line 16
    invoke-static {p0}, Lcom/google/android/tts/util/ISO3LocaleMap;->toISO3Locale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 1
    .param p0, "x0"    # Ljava/util/Locale;

    .prologue
    .line 16
    invoke-static {p0}, Lcom/google/android/tts/util/ISO3LocaleMap;->toISO3LanguageLocale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method private areParamsValid(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "languageA3"    # Ljava/lang/String;
    .param p2, "countryA3"    # Ljava/lang/String;

    .prologue
    .local p0, "this":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<TT;>;"
    const/4 v2, 0x3

    const/4 v0, 0x0

    .line 100
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v1, v2, :cond_1

    .line 101
    :cond_0
    sget-object v1, Lcom/google/android/tts/util/ISO3LocaleMap;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Language is not a 3 letter code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :goto_0
    return v0

    .line 105
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v1, v2, :cond_2

    .line 106
    sget-object v1, Lcom/google/android/tts/util/ISO3LocaleMap;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Country is not a 3 letter code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 109
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static toISO3LanguageLocale(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 2
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 93
    new-instance v0, Ljava/util/Locale;

    invoke-virtual {p0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static toISO3Locale(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 4
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 84
    new-instance v0, Ljava/util/Locale;

    invoke-virtual {p0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 5
    .param p1, "languageA3"    # Ljava/lang/String;
    .param p2, "countryA3"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<TT;>;"
    const/4 v3, 0x0

    .line 137
    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/util/ISO3LocaleMap;->areParamsValid(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    move-object v2, v3

    .line 151
    :cond_0
    :goto_0
    return-object v2

    .line 140
    :cond_1
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, p1, p2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    .local v0, "full":Ljava/util/Locale;
    iget-object v4, p0, Lcom/google/android/tts/util/ISO3LocaleMap;->mLocaleToEntry:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 142
    .local v2, "value":Ljava/lang/Object;, "TT;"
    if-nez v2, :cond_0

    .line 146
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 147
    .local v1, "languageOnly":Ljava/util/Locale;
    iget-object v4, p0, Lcom/google/android/tts/util/ISO3LocaleMap;->mLocaleToEntry:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 148
    if-nez v2, :cond_0

    move-object v2, v3

    .line 151
    goto :goto_0
.end method

.method public isLanguageAvailableV1(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p1, "languageA3"    # Ljava/lang/String;
    .param p2, "countryA3"    # Ljava/lang/String;

    .prologue
    .local p0, "this":Lcom/google/android/tts/util/ISO3LocaleMap;, "Lcom/google/android/tts/util/ISO3LocaleMap<TT;>;"
    const/4 v2, -0x2

    .line 116
    invoke-direct {p0, p1, p2}, Lcom/google/android/tts/util/ISO3LocaleMap;->areParamsValid(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v2

    .line 119
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 120
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, p1, p2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .local v0, "full":Ljava/util/Locale;
    iget-object v3, p0, Lcom/google/android/tts/util/ISO3LocaleMap;->mLocaleToEntry:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 122
    const/4 v2, 0x1

    goto :goto_0

    .line 126
    .end local v0    # "full":Ljava/util/Locale;
    :cond_2
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 127
    .local v1, "languageOnly":Ljava/util/Locale;
    iget-object v3, p0, Lcom/google/android/tts/util/ISO3LocaleMap;->mLocaleToEntry:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 128
    const/4 v2, 0x0

    goto :goto_0
.end method
