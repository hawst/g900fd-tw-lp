.class public Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;
.super Landroid/widget/BaseAdapter;
.source "LocalesAdapter.java"


# static fields
.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mLocales:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->mActivity:Landroid/app/Activity;

    .line 28
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->mActivity:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->mLocales:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->mLocales:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 42
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEntryView(Ljava/util/Locale;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 61
    if-eqz p2, :cond_0

    .line 62
    move-object v1, p2

    .line 67
    .local v1, "view":Landroid/view/View;
    :goto_0
    const v2, 0x7f0a0001

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 68
    .local v0, "name":Landroid/widget/TextView;
    invoke-virtual {p1}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    new-instance v2, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter$1;-><init>(Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;Ljava/util/Locale;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    return-object v1

    .line 64
    .end local v0    # "name":Landroid/widget/TextView;
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030001

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .restart local v1    # "view":Landroid/view/View;
    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->mLocales:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->mLocales:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 55
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->getEntryView(Ljava/util/Locale;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public setEntries(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p1, "entries":Ljava/util/List;, "Ljava/util/List<Ljava/util/Locale;>;"
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->mLocales:Ljava/util/List;

    .line 34
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/LocalesAdapter;->notifyDataSetChanged()V

    .line 35
    return-void
.end method
