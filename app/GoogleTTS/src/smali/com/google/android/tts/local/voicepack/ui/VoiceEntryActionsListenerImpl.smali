.class public Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;
.super Ljava/lang/Object;
.source "VoiceEntryActionsListenerImpl.java"

# interfaces
.implements Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTtsConfig:Lcom/google/android/tts/settings/TtsConfig;

.field private mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

.field private mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v0

    .line 39
    .local v0, "app":Lcom/google/android/tts/service/GoogleTTSApplication;
    invoke-virtual {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataManager()Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    .line 40
    invoke-virtual {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVoiceDataDownloader()Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    .line 41
    invoke-virtual {v0}, Lcom/google/android/tts/service/GoogleTTSApplication;->getTtsConfig()Lcom/google/android/tts/settings/TtsConfig;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfig;

    .line 42
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->mContext:Landroid/content/Context;

    .line 43
    return-void
.end method

.method private downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z
    .locals 3
    .param p1, "voiceMetadata"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    .prologue
    const/4 v0, 0x1

    .line 46
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v1, p1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->enoughFreeSpaceForInstall(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    invoke-interface {v1, p1, v0}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;->downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Z)Z

    .line 52
    :goto_0
    return v0

    .line 50
    :cond_0
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->mContext:Landroid/content/Context;

    const v2, 0x7f060039

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 52
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCancelDownloadClick(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V
    .locals 2
    .param p1, "entry"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->mVoiceDataDownloader:Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;

    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getVoiceMetadata()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;->cancelDownload(Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public onDownloadClick(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V
    .locals 1
    .param p1, "entry"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    .prologue
    .line 58
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getVoiceMetadata()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z

    .line 59
    return-void
.end method

.method public onMakeCurrentClick(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V
    .locals 3
    .param p1, "entry"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    .prologue
    .line 69
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isOneOfLocaleManyInstalledVoices()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->mTtsConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getVoiceMetadata()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/tts/settings/TtsConfig;->setDefaultVoiceName(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {v0}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->checkData()V

    .line 74
    :cond_0
    return-void
.end method

.method public onUninstallClick(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V
    .locals 2
    .param p1, "entry"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->mVoiceDataManager:Lcom/google/android/tts/local/voicepack/VoiceDataManager;

    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getInstalledVoice()Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/tts/local/voicepack/VoiceDataManager;->deleteVoiceData(Lcom/google/android/tts/local/voicepack/VoiceDataManager$InstalledVoiceInfo;)V

    .line 84
    return-void
.end method

.method public onUpdateClick(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V
    .locals 1
    .param p1, "entry"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    .prologue
    .line 78
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getVoiceMetadata()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListenerImpl;->downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;)Z

    .line 79
    return-void
.end method
