.class public Lcom/google/android/tts/settings/TtsConfigImpl;
.super Ljava/lang/Object;
.source "TtsConfigImpl.java"

# interfaces
.implements Lcom/google/android/tts/settings/TtsConfig;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/settings/TtsConfigImpl$GservicesKeys;,
        Lcom/google/android/tts/settings/TtsConfigImpl$SettingsKeys;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mGservicesCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mGservicesReceiver:Landroid/content/BroadcastReceiver;

.field private final mLock:Ljava/lang/Object;

.field private final mPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mUpdateExecutor:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mLock:Ljava/lang/Object;

    .line 178
    new-instance v1, Lcom/google/android/tts/settings/TtsConfigImpl$2;

    invoke-direct {v1, p0}, Lcom/google/android/tts/settings/TtsConfigImpl$2;-><init>(Lcom/google/android/tts/settings/TtsConfigImpl;)V

    iput-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mGservicesReceiver:Landroid/content/BroadcastReceiver;

    .line 75
    iput-object p1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mContext:Landroid/content/Context;

    .line 76
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mPreferences:Landroid/content/SharedPreferences;

    .line 77
    iget-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    iget-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mPreferences:Landroid/content/SharedPreferences;

    invoke-static {v1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 81
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 82
    iget-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mGservicesReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 84
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mUpdateExecutor:Ljava/util/concurrent/ExecutorService;

    .line 85
    iget-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mUpdateExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/google/android/tts/settings/TtsConfigImpl$1;

    invoke-direct {v2, p0}, Lcom/google/android/tts/settings/TtsConfigImpl$1;-><init>(Lcom/google/android/tts/settings/TtsConfigImpl;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/settings/TtsConfigImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/tts/settings/TtsConfigImpl;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/tts/settings/TtsConfigImpl;->updateCache()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/tts/settings/TtsConfigImpl;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/settings/TtsConfigImpl;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mUpdateExecutor:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method private getGservicesCache()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    iget-object v2, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 167
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mGservicesCache:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 169
    :try_start_1
    iget-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 170
    :catch_0
    move-exception v0

    .line 171
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v1, "TtsConfig"

    const-string v3, "Interrupted while waiting for Gservices cache"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 175
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 174
    :cond_0
    :try_start_3
    iget-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mGservicesCache:Ljava/util/Map;

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v1
.end method

.method private getGservicesInt(Ljava/lang/String;I)I
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defValue"    # I

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/google/android/tts/settings/TtsConfigImpl;->getGservicesCache()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 141
    .local v0, "value":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 152
    .end local p2    # "defValue":I
    :goto_0
    return p2

    .line 145
    .restart local p2    # "defValue":I
    :cond_0
    move v1, p2

    .line 147
    .local v1, "valueInt":I
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move p2, v1

    .line 152
    goto :goto_0

    .line 148
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private updateCache()V
    .locals 5

    .prologue
    .line 156
    iget-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "googletts:"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gsf/Gservices;->getStringsByPrefix(Landroid/content/ContentResolver;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 159
    .local v0, "cache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 160
    :try_start_0
    iput-object v0, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mGservicesCache:Ljava/util/Map;

    .line 161
    iget-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 162
    monitor-exit v2

    .line 163
    return-void

    .line 162
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public getAutoUpdate()Z
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "auto_update"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getDefaultTimeoutNetworkFirst()I
    .locals 2

    .prologue
    .line 99
    const-string v0, "googletts:timeout_network_first"

    const/16 v1, 0xbb8

    invoke-direct {p0, v0, v1}, Lcom/google/android/tts/settings/TtsConfigImpl;->getGservicesInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getDefaultTimeoutNetworkOnly()I
    .locals 2

    .prologue
    .line 105
    const-string v0, "googletts:timeout_network_only"

    const/16 v1, 0x2710

    invoke-direct {p0, v0, v1}, Lcom/google/android/tts/settings/TtsConfigImpl;->getGservicesInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getDefaultVoiceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "bcp47locale"    # Ljava/lang/String;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mPreferences:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default_voice_for_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    const-string v3, "-"

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMetadataUpdateFrequencyMs()I
    .locals 4

    .prologue
    .line 192
    const-string v0, "googletts:metadata_update_frequency_ms"

    const v1, 0x5265c00

    invoke-direct {p0, v0, v1}, Lcom/google/android/tts/settings/TtsConfigImpl;->getGservicesInt(Ljava/lang/String;I)I

    move-result v0

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    const-string v2, "googletts:metadata_update_frequency_max_fuzzy_add_ms"

    const v3, 0x36ee80

    invoke-direct {p0, v2, v3}, Lcom/google/android/tts/settings/TtsConfigImpl;->getGservicesInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getMetadataUpdateTime()J
    .locals 4

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "METADATA_UPDATE_TIME_MS"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getWifiOnly()Z
    .locals 3

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "wifi_only"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public onClose()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mGservicesReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 95
    return-void
.end method

.method public setDefaultVoiceName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "bcp47locale"    # Ljava/lang/String;
    .param p2, "voiceName"    # Ljava/lang/String;

    .prologue
    .line 245
    iget-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 246
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default_voice_for_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    const-string v3, "-"

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 248
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 249
    return-void
.end method

.method public setMetadataUpdateTime(J)V
    .locals 3
    .param p1, "timeMs"    # J

    .prologue
    .line 208
    iget-object v1, p0, Lcom/google/android/tts/settings/TtsConfigImpl;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 209
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "METADATA_UPDATE_TIME_MS"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 210
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 211
    return-void
.end method
