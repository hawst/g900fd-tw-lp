.class final Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1;
.super Ljava/lang/Object;
.source "VoiceEntry.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getAll(Landroid/content/Context;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)I
    .locals 4
    .param p1, "arg0"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;
    .param p2, "arg1"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    .prologue
    .line 277
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getVoiceMetadata()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getVoiceMetadata()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getLocale()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 279
    .local v0, "localeCompare":I
    if-nez v0, :cond_1

    .line 280
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getVoiceMetadata()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getTag()I

    move-result v2

    invoke-virtual {p2}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getVoiceMetadata()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getTag()I

    move-result v3

    sub-int v1, v2, v3

    .line 282
    .local v1, "tagCompare":I
    if-nez v1, :cond_0

    .line 283
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getVoiceMetadata()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getVoiceMetadata()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    .line 288
    .end local v1    # "tagCompare":I
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 273
    check-cast p1, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry$1;->compare(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)I

    move-result v0

    return v0
.end method
