.class public interface abstract Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader;
.super Ljava/lang/Object;
.source "IVoiceDataDownloader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;
    }
.end annotation


# virtual methods
.method public abstract addListener(Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;)V
.end method

.method public abstract cancelDownload(Ljava/lang/String;)V
.end method

.method public abstract downloadVoice(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;Z)Z
.end method

.method public abstract getAllVoicesMetadata()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadata;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isActiveDownload(Ljava/lang/String;)Z
.end method

.method public abstract removeListener(Lcom/google/android/tts/local/voicepack/IVoiceDataDownloader$Listener;)V
.end method
