.class public Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;
.super Landroid/widget/BaseAdapter;
.source "MultipleVoicesAdapter.java"


# static fields
.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mListener:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "listener"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mActivity:Landroid/app/Activity;

    .line 32
    iput-object p2, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mListener:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;

    .line 33
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;)Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mListener:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mEntries:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 47
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEntryView(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1, "entry"    # Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 67
    if-eqz p2, :cond_2

    .line 68
    move-object/from16 v10, p2

    .line 73
    .local v10, "view":Landroid/view/View;
    :goto_0
    const v11, 0x7f0a0004

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 74
    .local v7, "name":Landroid/widget/TextView;
    const v11, 0x7f0a0005

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 76
    .local v9, "status":Landroid/widget/TextView;
    const v11, 0x7f0a000b

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 78
    .local v8, "overflow":Landroid/widget/ImageView;
    const v11, 0x7f0a0008

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 80
    .local v4, "downloadButton":Landroid/widget/ImageView;
    const v11, 0x7f0a0009

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 82
    .local v1, "cancelDownloadButton":Landroid/widget/ImageView;
    const v11, 0x7f0a000a

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 84
    .local v2, "deleteButton":Landroid/widget/ImageView;
    const v11, 0x7f0a0006

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioButton;

    .line 86
    .local v6, "makeCurrentButton":Landroid/widget/RadioButton;
    const v11, 0x7f0a0002

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 88
    .local v5, "entryLayout":Landroid/widget/LinearLayout;
    const v11, 0x7f0a0007

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 91
    .local v3, "divider":Landroid/view/View;
    iget-object v11, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {p1, v11}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getDisplayName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v11, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {p1, v11}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getStatus(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    const/16 v11, 0x8

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 95
    const/16 v11, 0x8

    invoke-virtual {v4, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 96
    const/16 v11, 0x8

    invoke-virtual {v1, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 97
    const/16 v11, 0x8

    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 98
    const/16 v11, 0x8

    invoke-virtual {v6, v11}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 99
    const/4 v11, 0x0

    invoke-virtual {v5, v11}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 101
    iget-object v11, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {p1, v11}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->getStatus(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isInstalled()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 104
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isUpdatable()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 105
    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 106
    iget-object v11, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mActivity:Landroid/app/Activity;

    iget-object v12, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mListener:Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;

    invoke-static {v11, p1, v12}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntryListUIHelper;->getVoicesEntryOverflowListener(Landroid/content/Context;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;Lcom/google/android/tts/local/voicepack/ui/VoiceEntryActionsListener;)Landroid/view/View$OnClickListener;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isOneOfLocaleManyInstalledVoices()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 121
    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 122
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isDefaultForLocale()Z

    move-result v11

    invoke-virtual {v6, v11}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 124
    new-instance v11, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter$2;

    invoke-direct {v11, p0, p1}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter$2;-><init>(Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V

    invoke-virtual {v5, v11}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    :cond_1
    :goto_2
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isInstalled()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isRemovable()Z

    move-result v11

    if-nez v11, :cond_6

    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isUpdatable()Z

    move-result v11

    if-nez v11, :cond_6

    .line 152
    const/4 v11, 0x4

    invoke-virtual {v3, v11}, Landroid/view/View;->setVisibility(I)V

    .line 153
    const/4 v11, 0x4

    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 158
    :goto_3
    return-object v10

    .line 70
    .end local v1    # "cancelDownloadButton":Landroid/widget/ImageView;
    .end local v2    # "deleteButton":Landroid/widget/ImageView;
    .end local v3    # "divider":Landroid/view/View;
    .end local v4    # "downloadButton":Landroid/widget/ImageView;
    .end local v5    # "entryLayout":Landroid/widget/LinearLayout;
    .end local v6    # "makeCurrentButton":Landroid/widget/RadioButton;
    .end local v7    # "name":Landroid/widget/TextView;
    .end local v8    # "overflow":Landroid/widget/ImageView;
    .end local v9    # "status":Landroid/widget/TextView;
    .end local v10    # "view":Landroid/view/View;
    :cond_2
    iget-object v11, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v12, 0x7f030002

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v11, v12, v0, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .restart local v10    # "view":Landroid/view/View;
    goto/16 :goto_0

    .line 109
    .restart local v1    # "cancelDownloadButton":Landroid/widget/ImageView;
    .restart local v2    # "deleteButton":Landroid/widget/ImageView;
    .restart local v3    # "divider":Landroid/view/View;
    .restart local v4    # "downloadButton":Landroid/widget/ImageView;
    .restart local v5    # "entryLayout":Landroid/widget/LinearLayout;
    .restart local v6    # "makeCurrentButton":Landroid/widget/RadioButton;
    .restart local v7    # "name":Landroid/widget/TextView;
    .restart local v8    # "overflow":Landroid/widget/ImageView;
    .restart local v9    # "status":Landroid/widget/TextView;
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isRemovable()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 110
    const/4 v11, 0x0

    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 111
    new-instance v11, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter$1;

    invoke-direct {v11, p0, p1}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter$1;-><init>(Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V

    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 132
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;->isDownloading()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 133
    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 134
    new-instance v11, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter$3;

    invoke-direct {v11, p0, p1}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter$3;-><init>(Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V

    invoke-virtual {v1, v11}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 141
    :cond_5
    const/4 v11, 0x0

    invoke-virtual {v4, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 142
    new-instance v11, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter$4;

    invoke-direct {v11, p0, p1}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter$4;-><init>(Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;)V

    invoke-virtual {v4, v11}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 155
    :cond_6
    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mEntries:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 55
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No entries list!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 61
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 163
    invoke-virtual {p0, p1}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->getEntryView(Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 169
    const/4 v0, 0x0

    return v0
.end method

.method public setEntries(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/tts/local/voicepack/ui/VoiceEntry;>;"
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->mEntries:Ljava/util/List;

    .line 39
    invoke-virtual {p0}, Lcom/google/android/tts/local/voicepack/ui/MultipleVoicesAdapter;->notifyDataSetChanged()V

    .line 40
    return-void
.end method
