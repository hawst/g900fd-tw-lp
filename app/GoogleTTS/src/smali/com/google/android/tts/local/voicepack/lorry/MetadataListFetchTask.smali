.class Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;
.super Ljava/lang/Object;
.source "MetadataListFetchTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mConfig:Lcom/google/android/tts/settings/TtsConfig;

.field private final mContext:Landroid/content/Context;

.field private final mFetchUrl:Ljava/lang/String;

.field private final mForceFetch:Z

.field private final mMetadataManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

.field private final mUrlRewriter:Lcom/google/android/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;Lcom/google/android/tts/settings/TtsConfig;Lcom/google/android/common/base/Function;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fetchUrl"    # Ljava/lang/String;
    .param p3, "manager"    # Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;
    .param p4, "config"    # Lcom/google/android/tts/settings/TtsConfig;
    .param p6, "forceFetch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;",
            "Lcom/google/android/tts/settings/TtsConfig;",
            "Lcom/google/android/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p5, "urlRewriter":Lcom/google/android/common/base/Function;, "Lcom/google/android/common/base/Function<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mFetchUrl:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mMetadataManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    .line 44
    iput-object p4, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    .line 45
    iput-object p5, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mUrlRewriter:Lcom/google/android/common/base/Function;

    .line 46
    iput-boolean p6, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mForceFetch:Z

    .line 47
    return-void
.end method

.method private getCachedMetadataFilename()Ljava/lang/String;
    .locals 2

    .prologue
    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "voices-list.pb."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/tts/service/GoogleTTSApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/service/GoogleTTSApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/tts/service/GoogleTTSApplication;->getVersionCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getResponseBytes(Ljava/net/URL;)[B
    .locals 4
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 134
    const/4 v1, 0x0

    .line 136
    .local v1, "connection":Ljava/net/HttpURLConnection;
    :try_start_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v1, v0

    .line 137
    const-string v2, "GET"

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 138
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 139
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1

    .line 140
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/io/ByteStreams;->toByteArray(Ljava/io/InputStream;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 148
    if-eqz v1, :cond_0

    .line 149
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 153
    :cond_0
    :goto_0
    return-object v2

    .line 148
    :cond_1
    if-eqz v1, :cond_2

    .line 149
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 153
    :cond_2
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 142
    :catch_0
    move-exception v2

    .line 148
    if-eqz v1, :cond_2

    .line 149
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_1

    .line 144
    :catch_1
    move-exception v2

    .line 148
    if-eqz v1, :cond_2

    .line 149
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_1

    .line 148
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_3

    .line 149
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_3
    throw v2
.end method

.method private readCachedVoiceMetadataList()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    .locals 6

    .prologue
    .line 158
    const/4 v1, 0x0

    .line 163
    .local v1, "fileBytes":[B
    :try_start_0
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->getCachedMetadataFilename()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/common/io/Files;->toByteArray(Ljava/io/File;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    .line 172
    :goto_0
    if-nez v1, :cond_0

    .line 174
    :try_start_1
    iget-object v3, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    const-string v4, "voices-list.pb"

    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/io/ByteStreams;->toByteArray(Ljava/io/InputStream;)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    .line 184
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    .line 186
    :try_start_2
    invoke-static {v1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->parseFrom([B)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    :try_end_2
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v3

    .line 193
    :goto_2
    return-object v3

    .line 177
    :catch_0
    move-exception v2

    .line 178
    .local v2, "ioe":Ljava/io/IOException;
    sget-object v3, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->TAG:Ljava/lang/String;

    const-string v4, "Error reading bundled metadata."

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 187
    .end local v2    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 188
    .local v0, "e":Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
    sget-object v3, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->TAG:Ljava/lang/String;

    const-string v4, "Error parsing cached metadata: "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 193
    .end local v0    # "e":Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
    :cond_1
    invoke-static {}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->emptyVoiceMetadataList()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    move-result-object v3

    goto :goto_2

    .line 166
    :catch_2
    move-exception v3

    goto :goto_0
.end method

.method private removeOldCachedMetadata()V
    .locals 7

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->getCachedMetadataFilename()Ljava/lang/String;

    move-result-object v2

    .line 210
    .local v2, "currentCache":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v1, v0, v3

    .line 211
    .local v1, "cacheFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 210
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 214
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "voices-list.pb"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 215
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 218
    .end local v1    # "cacheFile":Ljava/io/File;
    :cond_2
    return-void
.end method

.method private updateMetadataFromNetwork(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)V
    .locals 9
    .param p1, "currentMetadata"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    .prologue
    .line 83
    const/4 v2, 0x0

    .line 85
    .local v2, "fetchUrl":Ljava/net/URL;
    :try_start_0
    new-instance v3, Ljava/net/URL;

    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mUrlRewriter:Lcom/google/android/common/base/Function;

    iget-object v7, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mFetchUrl:Ljava/lang/String;

    invoke-interface {v6, v7}, Lcom/google/android/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v3, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    .end local v2    # "fetchUrl":Ljava/net/URL;
    .local v3, "fetchUrl":Ljava/net/URL;
    invoke-direct {p0, v3}, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->getResponseBytes(Ljava/net/URL;)[B

    move-result-object v5

    .line 91
    .local v5, "responseBytes":[B
    if-nez v5, :cond_1

    .line 93
    invoke-virtual {v3}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/tts/local/voicepack/lorry/BandaidUtil;->isBandaidURL(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 94
    invoke-virtual {v3}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/tts/local/voicepack/lorry/BandaidUtil;->getFallbackURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    .local v1, "fallbackUrlString":Ljava/lang/String;
    :try_start_1
    new-instance v2, Ljava/net/URL;

    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mUrlRewriter:Lcom/google/android/common/base/Function;

    invoke-interface {v6, v1}, Lcom/google/android/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    .line 101
    .end local v3    # "fetchUrl":Ljava/net/URL;
    .restart local v2    # "fetchUrl":Ljava/net/URL;
    invoke-direct {p0, v2}, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->getResponseBytes(Ljava/net/URL;)[B

    move-result-object v5

    .line 103
    .end local v1    # "fallbackUrlString":Ljava/lang/String;
    :goto_0
    if-nez v5, :cond_2

    .line 131
    .end local v5    # "responseBytes":[B
    :cond_0
    :goto_1
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/net/MalformedURLException;
    goto :goto_1

    .line 97
    .end local v0    # "e":Ljava/net/MalformedURLException;
    .end local v2    # "fetchUrl":Ljava/net/URL;
    .restart local v1    # "fallbackUrlString":Ljava/lang/String;
    .restart local v3    # "fetchUrl":Ljava/net/URL;
    .restart local v5    # "responseBytes":[B
    :catch_1
    move-exception v0

    .restart local v0    # "e":Ljava/net/MalformedURLException;
    move-object v2, v3

    .line 99
    .end local v3    # "fetchUrl":Ljava/net/URL;
    .restart local v2    # "fetchUrl":Ljava/net/URL;
    goto :goto_1

    .end local v0    # "e":Ljava/net/MalformedURLException;
    .end local v1    # "fallbackUrlString":Ljava/lang/String;
    .end local v2    # "fetchUrl":Ljava/net/URL;
    .restart local v3    # "fetchUrl":Ljava/net/URL;
    :cond_1
    move-object v2, v3

    .line 108
    .end local v3    # "fetchUrl":Ljava/net/URL;
    .restart local v2    # "fetchUrl":Ljava/net/URL;
    :cond_2
    const/4 v4, 0x0

    .line 110
    .local v4, "fetched":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    :try_start_2
    invoke-static {v5}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->parseFrom([B)Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    :try_end_2
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v4

    .line 116
    if-eqz p1, :cond_3

    invoke-virtual {v4}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->getRevision()I

    move-result v6

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->getRevision()I

    move-result v7

    if-le v6, v7, :cond_0

    .line 118
    :cond_3
    invoke-direct {p0, v4}, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->writeCachedVoiceMetadataList(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 125
    iget-object v6, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mMetadataManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    invoke-virtual {v6, v4}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->setVoiceMetadataList(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)V

    goto :goto_1

    .line 111
    :catch_2
    move-exception v0

    .line 112
    .local v0, "e":Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
    sget-object v6, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error parsing protobuf response : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .end local v0    # "e":Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
    .end local v2    # "fetchUrl":Ljava/net/URL;
    .end local v4    # "fetched":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    .restart local v3    # "fetchUrl":Ljava/net/URL;
    :cond_4
    move-object v2, v3

    .end local v3    # "fetchUrl":Ljava/net/URL;
    .restart local v2    # "fetchUrl":Ljava/net/URL;
    goto :goto_0
.end method

.method private writeCachedVoiceMetadataList(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)Z
    .locals 4
    .param p1, "metadata"    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    .prologue
    .line 222
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->getCachedMetadataFilename()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 225
    .local v1, "metaDataCache":Ljava/io/File;
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;->toByteArray()[B

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/common/io/Files;->write([BLjava/io/File;)V

    .line 226
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->removeOldCachedMetadata()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    const/4 v2, 0x1

    .line 230
    :goto_0
    return v2

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->TAG:Ljava/lang/String;

    const-string v3, "Error updating cached metadata: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 230
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 50
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 51
    return-void
.end method

.method public run()V
    .locals 10

    .prologue
    .line 55
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-interface {v1}, Lcom/google/android/tts/settings/TtsConfig;->getMetadataUpdateTime()J

    move-result-wide v6

    .line 56
    .local v6, "metadataUpdateTime":J
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-interface {v1}, Lcom/google/android/tts/settings/TtsConfig;->getMetadataUpdateFrequencyMs()I

    move-result v1

    int-to-long v4, v1

    .line 57
    .local v4, "metadataUpdateFrequency":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 59
    .local v2, "currentTime":J
    invoke-direct {p0}, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->readCachedVoiceMetadataList()Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;

    move-result-object v0

    .line 62
    .local v0, "currentMetadata":Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mMetadataManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    invoke-virtual {v1, v0}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->setVoiceMetadataList(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)V

    .line 69
    iget-boolean v1, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mForceFetch:Z

    if-nez v1, :cond_0

    add-long v8, v6, v4

    cmp-long v1, v2, v8

    if-gtz v1, :cond_0

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-gez v1, :cond_1

    .line 72
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->updateMetadataFromNetwork(Lcom/google/android/tts/voicepack/VoiceMetadataProto$VoiceMetadataList;)V

    .line 73
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mConfig:Lcom/google/android/tts/settings/TtsConfig;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-interface {v1, v8, v9}, Lcom/google/android/tts/settings/TtsConfig;->setMetadataUpdateTime(J)V

    .line 79
    :cond_1
    iget-object v1, p0, Lcom/google/android/tts/local/voicepack/lorry/MetadataListFetchTask;->mMetadataManager:Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;

    invoke-virtual {v1}, Lcom/google/android/tts/local/voicepack/lorry/VoiceMetadataListManager;->markUpdateComplete()V

    .line 80
    return-void
.end method
