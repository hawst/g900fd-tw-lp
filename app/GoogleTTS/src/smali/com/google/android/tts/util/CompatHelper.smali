.class public Lcom/google/android/tts/util/CompatHelper;
.super Ljava/lang/Object;
.source "CompatHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static callbackError(Landroid/speech/tts/SynthesisCallback;I)V
    .locals 2
    .param p0, "callback"    # Landroid/speech/tts/SynthesisCallback;
    .param p1, "errorCode"    # I

    .prologue
    .line 13
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 14
    invoke-interface {p0, p1}, Landroid/speech/tts/SynthesisCallback;->error(I)V

    .line 18
    :goto_0
    return-void

    .line 16
    :cond_0
    invoke-interface {p0}, Landroid/speech/tts/SynthesisCallback;->error()V

    goto :goto_0
.end method
