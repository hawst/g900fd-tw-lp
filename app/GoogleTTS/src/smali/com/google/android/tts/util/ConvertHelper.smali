.class public Lcom/google/android/tts/util/ConvertHelper;
.super Ljava/lang/Object;
.source "ConvertHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static androidSpeechRateToSpeechDuration(F)F
    .locals 3
    .param p0, "androidSpeechRate"    # F

    .prologue
    .line 39
    const/high16 v0, 0x40000000    # 2.0f

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x3e800000    # 0.25f

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/tts/util/ConvertHelper;->scaleAndroidSpeechRate(FFFF)F

    move-result v0

    return v0
.end method

.method public static scaleAndroidSpeechRate(FFFF)F
    .locals 3
    .param p0, "speechRate"    # F
    .param p1, "slowValue"    # F
    .param p2, "stdValue"    # F
    .param p3, "fastValue"    # F

    .prologue
    const/high16 v2, 0x42c80000    # 100.0f

    const/high16 v1, 0x41a00000    # 20.0f

    .line 13
    cmpl-float v0, p0, v2

    if-ltz v0, :cond_1

    .line 14
    const/high16 v0, 0x43c80000    # 400.0f

    cmpl-float v0, p0, v0

    if-lez v0, :cond_0

    .line 15
    const/high16 p0, 0x43c80000    # 400.0f

    .line 18
    :cond_0
    sub-float v0, p0, v2

    const/high16 v1, 0x43960000    # 300.0f

    div-float/2addr v0, v1

    sub-float v1, p3, p2

    mul-float/2addr v0, v1

    add-float/2addr v0, p2

    .line 25
    :goto_0
    return v0

    .line 21
    :cond_1
    cmpg-float v0, p0, v1

    if-gez v0, :cond_2

    .line 22
    const/high16 p0, 0x41a00000    # 20.0f

    .line 25
    :cond_2
    sub-float v0, p0, v1

    const/high16 v1, 0x42a00000    # 80.0f

    div-float/2addr v0, v1

    sub-float v1, p2, p1

    mul-float/2addr v0, v1

    add-float/2addr v0, p1

    goto :goto_0
.end method

.method public static speechDurationToNetworkSpeed(F)F
    .locals 8
    .param p0, "speechDuration"    # F

    .prologue
    .line 49
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    float-to-double v6, p0

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-float v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public static speechPitchToNetworkPitch(F)F
    .locals 8
    .param p0, "speechPitch"    # F

    .prologue
    .line 59
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide v4, 0x3fd3333340000000L    # 0.30000001192092896

    float-to-double v6, p0

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method
