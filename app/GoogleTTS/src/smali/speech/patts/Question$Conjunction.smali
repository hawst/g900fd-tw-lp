.class public final enum Lspeech/patts/Question$Conjunction;
.super Ljava/lang/Enum;
.source "Question.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Question;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Conjunction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/Question$Conjunction;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/Question$Conjunction;

.field public static final enum AND:Lspeech/patts/Question$Conjunction;

.field public static final enum OR:Lspeech/patts/Question$Conjunction;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/Question$Conjunction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 63
    new-instance v0, Lspeech/patts/Question$Conjunction;

    const-string v1, "AND"

    invoke-direct {v0, v1, v3, v3, v2}, Lspeech/patts/Question$Conjunction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Question$Conjunction;->AND:Lspeech/patts/Question$Conjunction;

    .line 64
    new-instance v0, Lspeech/patts/Question$Conjunction;

    const-string v1, "OR"

    invoke-direct {v0, v1, v2, v2, v4}, Lspeech/patts/Question$Conjunction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Question$Conjunction;->OR:Lspeech/patts/Question$Conjunction;

    .line 61
    new-array v0, v4, [Lspeech/patts/Question$Conjunction;

    sget-object v1, Lspeech/patts/Question$Conjunction;->AND:Lspeech/patts/Question$Conjunction;

    aput-object v1, v0, v3

    sget-object v1, Lspeech/patts/Question$Conjunction;->OR:Lspeech/patts/Question$Conjunction;

    aput-object v1, v0, v2

    sput-object v0, Lspeech/patts/Question$Conjunction;->$VALUES:[Lspeech/patts/Question$Conjunction;

    .line 83
    new-instance v0, Lspeech/patts/Question$Conjunction$1;

    invoke-direct {v0}, Lspeech/patts/Question$Conjunction$1;-><init>()V

    sput-object v0, Lspeech/patts/Question$Conjunction;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 93
    iput p3, p0, Lspeech/patts/Question$Conjunction;->index:I

    .line 94
    iput p4, p0, Lspeech/patts/Question$Conjunction;->value:I

    .line 95
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/Question$Conjunction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 61
    const-class v0, Lspeech/patts/Question$Conjunction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/Question$Conjunction;

    return-object v0
.end method

.method public static values()[Lspeech/patts/Question$Conjunction;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lspeech/patts/Question$Conjunction;->$VALUES:[Lspeech/patts/Question$Conjunction;

    invoke-virtual {v0}, [Lspeech/patts/Question$Conjunction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/Question$Conjunction;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lspeech/patts/Question$Conjunction;->value:I

    return v0
.end method
