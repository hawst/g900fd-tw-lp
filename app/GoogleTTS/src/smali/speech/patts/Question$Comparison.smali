.class public final enum Lspeech/patts/Question$Comparison;
.super Ljava/lang/Enum;
.source "Question.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Question;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Comparison"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/Question$Comparison;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/Question$Comparison;

.field public static final enum EQ:Lspeech/patts/Question$Comparison;

.field public static final enum LT:Lspeech/patts/Question$Comparison;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/Question$Comparison;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 24
    new-instance v0, Lspeech/patts/Question$Comparison;

    const-string v1, "EQ"

    invoke-direct {v0, v1, v3, v3, v2}, Lspeech/patts/Question$Comparison;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Question$Comparison;->EQ:Lspeech/patts/Question$Comparison;

    .line 25
    new-instance v0, Lspeech/patts/Question$Comparison;

    const-string v1, "LT"

    invoke-direct {v0, v1, v2, v2, v4}, Lspeech/patts/Question$Comparison;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Question$Comparison;->LT:Lspeech/patts/Question$Comparison;

    .line 22
    new-array v0, v4, [Lspeech/patts/Question$Comparison;

    sget-object v1, Lspeech/patts/Question$Comparison;->EQ:Lspeech/patts/Question$Comparison;

    aput-object v1, v0, v3

    sget-object v1, Lspeech/patts/Question$Comparison;->LT:Lspeech/patts/Question$Comparison;

    aput-object v1, v0, v2

    sput-object v0, Lspeech/patts/Question$Comparison;->$VALUES:[Lspeech/patts/Question$Comparison;

    .line 44
    new-instance v0, Lspeech/patts/Question$Comparison$1;

    invoke-direct {v0}, Lspeech/patts/Question$Comparison$1;-><init>()V

    sput-object v0, Lspeech/patts/Question$Comparison;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 54
    iput p3, p0, Lspeech/patts/Question$Comparison;->index:I

    .line 55
    iput p4, p0, Lspeech/patts/Question$Comparison;->value:I

    .line 56
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/Question$Comparison;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lspeech/patts/Question$Comparison;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/Question$Comparison;

    return-object v0
.end method

.method public static values()[Lspeech/patts/Question$Comparison;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lspeech/patts/Question$Comparison;->$VALUES:[Lspeech/patts/Question$Comparison;

    invoke-virtual {v0}, [Lspeech/patts/Question$Comparison;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/Question$Comparison;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lspeech/patts/Question$Comparison;->value:I

    return v0
.end method
