.class public final enum Lspeech/patts/CostFunctionSetup$CFType;
.super Ljava/lang/Enum;
.source "CostFunctionSetup.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/CostFunctionSetup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CFType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/CostFunctionSetup$CFType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/CostFunctionSetup$CFType;

.field public static final enum BOOL:Lspeech/patts/CostFunctionSetup$CFType;

.field public static final enum EUCLIDEAN:Lspeech/patts/CostFunctionSetup$CFType;

.field public static final enum TABLE:Lspeech/patts/CostFunctionSetup$CFType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/CostFunctionSetup$CFType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 24
    new-instance v0, Lspeech/patts/CostFunctionSetup$CFType;

    const-string v1, "BOOL"

    invoke-direct {v0, v1, v4, v4, v2}, Lspeech/patts/CostFunctionSetup$CFType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/CostFunctionSetup$CFType;->BOOL:Lspeech/patts/CostFunctionSetup$CFType;

    .line 25
    new-instance v0, Lspeech/patts/CostFunctionSetup$CFType;

    const-string v1, "TABLE"

    invoke-direct {v0, v1, v2, v2, v3}, Lspeech/patts/CostFunctionSetup$CFType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/CostFunctionSetup$CFType;->TABLE:Lspeech/patts/CostFunctionSetup$CFType;

    .line 26
    new-instance v0, Lspeech/patts/CostFunctionSetup$CFType;

    const-string v1, "EUCLIDEAN"

    invoke-direct {v0, v1, v3, v3, v5}, Lspeech/patts/CostFunctionSetup$CFType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/CostFunctionSetup$CFType;->EUCLIDEAN:Lspeech/patts/CostFunctionSetup$CFType;

    .line 22
    new-array v0, v5, [Lspeech/patts/CostFunctionSetup$CFType;

    sget-object v1, Lspeech/patts/CostFunctionSetup$CFType;->BOOL:Lspeech/patts/CostFunctionSetup$CFType;

    aput-object v1, v0, v4

    sget-object v1, Lspeech/patts/CostFunctionSetup$CFType;->TABLE:Lspeech/patts/CostFunctionSetup$CFType;

    aput-object v1, v0, v2

    sget-object v1, Lspeech/patts/CostFunctionSetup$CFType;->EUCLIDEAN:Lspeech/patts/CostFunctionSetup$CFType;

    aput-object v1, v0, v3

    sput-object v0, Lspeech/patts/CostFunctionSetup$CFType;->$VALUES:[Lspeech/patts/CostFunctionSetup$CFType;

    .line 46
    new-instance v0, Lspeech/patts/CostFunctionSetup$CFType$1;

    invoke-direct {v0}, Lspeech/patts/CostFunctionSetup$CFType$1;-><init>()V

    sput-object v0, Lspeech/patts/CostFunctionSetup$CFType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput p3, p0, Lspeech/patts/CostFunctionSetup$CFType;->index:I

    .line 57
    iput p4, p0, Lspeech/patts/CostFunctionSetup$CFType;->value:I

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/CostFunctionSetup$CFType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lspeech/patts/CostFunctionSetup$CFType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunctionSetup$CFType;

    return-object v0
.end method

.method public static values()[Lspeech/patts/CostFunctionSetup$CFType;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lspeech/patts/CostFunctionSetup$CFType;->$VALUES:[Lspeech/patts/CostFunctionSetup$CFType;

    invoke-virtual {v0}, [Lspeech/patts/CostFunctionSetup$CFType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/CostFunctionSetup$CFType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lspeech/patts/CostFunctionSetup$CFType;->value:I

    return v0
.end method
