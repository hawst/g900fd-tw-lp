.class public final Lspeech/patts/RomanNumeralContextWords$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "RomanNumeralContextWords.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/RomanNumeralContextWords;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/RomanNumeralContextWords;",
        "Lspeech/patts/RomanNumeralContextWords$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/RomanNumeralContextWords;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/RomanNumeralContextWords$Builder;
    .locals 1

    .prologue
    .line 139
    invoke-static {}, Lspeech/patts/RomanNumeralContextWords$Builder;->create()Lspeech/patts/RomanNumeralContextWords$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/RomanNumeralContextWords$Builder;
    .locals 3

    .prologue
    .line 148
    new-instance v0, Lspeech/patts/RomanNumeralContextWords$Builder;

    invoke-direct {v0}, Lspeech/patts/RomanNumeralContextWords$Builder;-><init>()V

    .line 149
    .local v0, "builder":Lspeech/patts/RomanNumeralContextWords$Builder;
    new-instance v1, Lspeech/patts/RomanNumeralContextWords;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/RomanNumeralContextWords;-><init>(Lspeech/patts/RomanNumeralContextWords$1;)V

    iput-object v1, v0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    .line 150
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWords$Builder;->build()Lspeech/patts/RomanNumeralContextWords;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/RomanNumeralContextWords;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWords$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    invoke-static {v0}, Lspeech/patts/RomanNumeralContextWords$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 181
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWords$Builder;->buildPartial()Lspeech/patts/RomanNumeralContextWords;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/RomanNumeralContextWords;
    .locals 3

    .prologue
    .line 194
    iget-object v1, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    if-nez v1, :cond_0

    .line 195
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 198
    :cond_0
    iget-object v1, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    # getter for: Lspeech/patts/RomanNumeralContextWords;->context_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/RomanNumeralContextWords;->access$300(Lspeech/patts/RomanNumeralContextWords;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 199
    iget-object v1, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    iget-object v2, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    # getter for: Lspeech/patts/RomanNumeralContextWords;->context_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/RomanNumeralContextWords;->access$300(Lspeech/patts/RomanNumeralContextWords;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/RomanNumeralContextWords;->context_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/RomanNumeralContextWords;->access$302(Lspeech/patts/RomanNumeralContextWords;Ljava/util/List;)Ljava/util/List;

    .line 202
    :cond_1
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    .line 203
    .local v0, "returnMe":Lspeech/patts/RomanNumeralContextWords;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    .line 204
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWords$Builder;->clone()Lspeech/patts/RomanNumeralContextWords$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWords$Builder;->clone()Lspeech/patts/RomanNumeralContextWords$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWords$Builder;->clone()Lspeech/patts/RomanNumeralContextWords$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/RomanNumeralContextWords$Builder;
    .locals 2

    .prologue
    .line 167
    invoke-static {}, Lspeech/patts/RomanNumeralContextWords$Builder;->create()Lspeech/patts/RomanNumeralContextWords$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    invoke-virtual {v0, v1}, Lspeech/patts/RomanNumeralContextWords$Builder;->mergeFrom(Lspeech/patts/RomanNumeralContextWords;)Lspeech/patts/RomanNumeralContextWords$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    invoke-virtual {v0}, Lspeech/patts/RomanNumeralContextWords;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 139
    check-cast p1, Lspeech/patts/RomanNumeralContextWords;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/RomanNumeralContextWords$Builder;->mergeFrom(Lspeech/patts/RomanNumeralContextWords;)Lspeech/patts/RomanNumeralContextWords$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/RomanNumeralContextWords;)Lspeech/patts/RomanNumeralContextWords$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/RomanNumeralContextWords;

    .prologue
    .line 208
    invoke-static {}, Lspeech/patts/RomanNumeralContextWords;->getDefaultInstance()Lspeech/patts/RomanNumeralContextWords;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-object p0

    .line 209
    :cond_1
    # getter for: Lspeech/patts/RomanNumeralContextWords;->context_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/RomanNumeralContextWords;->access$300(Lspeech/patts/RomanNumeralContextWords;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    # getter for: Lspeech/patts/RomanNumeralContextWords;->context_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/RomanNumeralContextWords;->access$300(Lspeech/patts/RomanNumeralContextWords;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 211
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/RomanNumeralContextWords;->context_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/RomanNumeralContextWords;->access$302(Lspeech/patts/RomanNumeralContextWords;Ljava/util/List;)Ljava/util/List;

    .line 213
    :cond_2
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWords$Builder;->result:Lspeech/patts/RomanNumeralContextWords;

    # getter for: Lspeech/patts/RomanNumeralContextWords;->context_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/RomanNumeralContextWords;->access$300(Lspeech/patts/RomanNumeralContextWords;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/RomanNumeralContextWords;->context_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/RomanNumeralContextWords;->access$300(Lspeech/patts/RomanNumeralContextWords;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
