.class public final enum Lspeech/patts/LinguisticFeatureValType;
.super Ljava/lang/Enum;
.source "LinguisticFeatureValType.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/LinguisticFeatureValType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/LinguisticFeatureValType;

.field public static final enum BOOL:Lspeech/patts/LinguisticFeatureValType;

.field public static final enum FLOAT:Lspeech/patts/LinguisticFeatureValType;

.field public static final enum INT:Lspeech/patts/LinguisticFeatureValType;

.field public static final enum STRING:Lspeech/patts/LinguisticFeatureValType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/LinguisticFeatureValType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lspeech/patts/LinguisticFeatureValType;

    const-string v1, "BOOL"

    invoke-direct {v0, v1, v2, v2, v2}, Lspeech/patts/LinguisticFeatureValType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/LinguisticFeatureValType;->BOOL:Lspeech/patts/LinguisticFeatureValType;

    .line 8
    new-instance v0, Lspeech/patts/LinguisticFeatureValType;

    const-string v1, "INT"

    invoke-direct {v0, v1, v3, v3, v3}, Lspeech/patts/LinguisticFeatureValType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/LinguisticFeatureValType;->INT:Lspeech/patts/LinguisticFeatureValType;

    .line 9
    new-instance v0, Lspeech/patts/LinguisticFeatureValType;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v4, v4, v4}, Lspeech/patts/LinguisticFeatureValType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/LinguisticFeatureValType;->STRING:Lspeech/patts/LinguisticFeatureValType;

    .line 10
    new-instance v0, Lspeech/patts/LinguisticFeatureValType;

    const-string v1, "FLOAT"

    invoke-direct {v0, v1, v5, v5, v5}, Lspeech/patts/LinguisticFeatureValType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/LinguisticFeatureValType;->FLOAT:Lspeech/patts/LinguisticFeatureValType;

    .line 5
    const/4 v0, 0x4

    new-array v0, v0, [Lspeech/patts/LinguisticFeatureValType;

    sget-object v1, Lspeech/patts/LinguisticFeatureValType;->BOOL:Lspeech/patts/LinguisticFeatureValType;

    aput-object v1, v0, v2

    sget-object v1, Lspeech/patts/LinguisticFeatureValType;->INT:Lspeech/patts/LinguisticFeatureValType;

    aput-object v1, v0, v3

    sget-object v1, Lspeech/patts/LinguisticFeatureValType;->STRING:Lspeech/patts/LinguisticFeatureValType;

    aput-object v1, v0, v4

    sget-object v1, Lspeech/patts/LinguisticFeatureValType;->FLOAT:Lspeech/patts/LinguisticFeatureValType;

    aput-object v1, v0, v5

    sput-object v0, Lspeech/patts/LinguisticFeatureValType;->$VALUES:[Lspeech/patts/LinguisticFeatureValType;

    .line 31
    new-instance v0, Lspeech/patts/LinguisticFeatureValType$1;

    invoke-direct {v0}, Lspeech/patts/LinguisticFeatureValType$1;-><init>()V

    sput-object v0, Lspeech/patts/LinguisticFeatureValType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lspeech/patts/LinguisticFeatureValType;->index:I

    .line 42
    iput p4, p0, Lspeech/patts/LinguisticFeatureValType;->value:I

    .line 43
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/LinguisticFeatureValType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lspeech/patts/LinguisticFeatureValType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/LinguisticFeatureValType;

    return-object v0
.end method

.method public static values()[Lspeech/patts/LinguisticFeatureValType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lspeech/patts/LinguisticFeatureValType;->$VALUES:[Lspeech/patts/LinguisticFeatureValType;

    invoke-virtual {v0}, [Lspeech/patts/LinguisticFeatureValType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/LinguisticFeatureValType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lspeech/patts/LinguisticFeatureValType;->value:I

    return v0
.end method
