.class public final enum Lspeech/patts/CostFunction$CostFunctionType;
.super Ljava/lang/Enum;
.source "CostFunction.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/CostFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CostFunctionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/CostFunction$CostFunctionType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/CostFunction$CostFunctionType;

.field public static final enum EQUAL:Lspeech/patts/CostFunction$CostFunctionType;

.field public static final enum EUCLIDEAN:Lspeech/patts/CostFunction$CostFunctionType;

.field public static final enum SCALED_EUCLIDEAN:Lspeech/patts/CostFunction$CostFunctionType;

.field public static final enum SPARSE_TABLE:Lspeech/patts/CostFunction$CostFunctionType;

.field public static final enum TABLE:Lspeech/patts/CostFunction$CostFunctionType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/CostFunction$CostFunctionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 24
    new-instance v0, Lspeech/patts/CostFunction$CostFunctionType;

    const-string v1, "TABLE"

    invoke-direct {v0, v1, v5, v5, v3}, Lspeech/patts/CostFunction$CostFunctionType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/CostFunction$CostFunctionType;->TABLE:Lspeech/patts/CostFunction$CostFunctionType;

    .line 25
    new-instance v0, Lspeech/patts/CostFunction$CostFunctionType;

    const-string v1, "EUCLIDEAN"

    invoke-direct {v0, v1, v6, v6, v4}, Lspeech/patts/CostFunction$CostFunctionType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/CostFunction$CostFunctionType;->EUCLIDEAN:Lspeech/patts/CostFunction$CostFunctionType;

    .line 26
    new-instance v0, Lspeech/patts/CostFunction$CostFunctionType;

    const-string v1, "EQUAL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v3, v2}, Lspeech/patts/CostFunction$CostFunctionType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/CostFunction$CostFunctionType;->EQUAL:Lspeech/patts/CostFunction$CostFunctionType;

    .line 27
    new-instance v0, Lspeech/patts/CostFunction$CostFunctionType;

    const-string v1, "SPARSE_TABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v4, v4, v2}, Lspeech/patts/CostFunction$CostFunctionType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/CostFunction$CostFunctionType;->SPARSE_TABLE:Lspeech/patts/CostFunction$CostFunctionType;

    .line 28
    new-instance v0, Lspeech/patts/CostFunction$CostFunctionType;

    const-string v1, "SCALED_EUCLIDEAN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v7, v7, v2}, Lspeech/patts/CostFunction$CostFunctionType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/CostFunction$CostFunctionType;->SCALED_EUCLIDEAN:Lspeech/patts/CostFunction$CostFunctionType;

    .line 22
    const/4 v0, 0x5

    new-array v0, v0, [Lspeech/patts/CostFunction$CostFunctionType;

    sget-object v1, Lspeech/patts/CostFunction$CostFunctionType;->TABLE:Lspeech/patts/CostFunction$CostFunctionType;

    aput-object v1, v0, v5

    sget-object v1, Lspeech/patts/CostFunction$CostFunctionType;->EUCLIDEAN:Lspeech/patts/CostFunction$CostFunctionType;

    aput-object v1, v0, v6

    sget-object v1, Lspeech/patts/CostFunction$CostFunctionType;->EQUAL:Lspeech/patts/CostFunction$CostFunctionType;

    aput-object v1, v0, v3

    sget-object v1, Lspeech/patts/CostFunction$CostFunctionType;->SPARSE_TABLE:Lspeech/patts/CostFunction$CostFunctionType;

    aput-object v1, v0, v4

    sget-object v1, Lspeech/patts/CostFunction$CostFunctionType;->SCALED_EUCLIDEAN:Lspeech/patts/CostFunction$CostFunctionType;

    aput-object v1, v0, v7

    sput-object v0, Lspeech/patts/CostFunction$CostFunctionType;->$VALUES:[Lspeech/patts/CostFunction$CostFunctionType;

    .line 50
    new-instance v0, Lspeech/patts/CostFunction$CostFunctionType$1;

    invoke-direct {v0}, Lspeech/patts/CostFunction$CostFunctionType$1;-><init>()V

    sput-object v0, Lspeech/patts/CostFunction$CostFunctionType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    iput p3, p0, Lspeech/patts/CostFunction$CostFunctionType;->index:I

    .line 61
    iput p4, p0, Lspeech/patts/CostFunction$CostFunctionType;->value:I

    .line 62
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/CostFunction$CostFunctionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lspeech/patts/CostFunction$CostFunctionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction$CostFunctionType;

    return-object v0
.end method

.method public static values()[Lspeech/patts/CostFunction$CostFunctionType;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lspeech/patts/CostFunction$CostFunctionType;->$VALUES:[Lspeech/patts/CostFunction$CostFunctionType;

    invoke-virtual {v0}, [Lspeech/patts/CostFunction$CostFunctionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/CostFunction$CostFunctionType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lspeech/patts/CostFunction$CostFunctionType;->value:I

    return v0
.end method
