.class public final Lspeech/patts/SparseTableEntry;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SparseTableEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/SparseTableEntry$1;,
        Lspeech/patts/SparseTableEntry$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/SparseTableEntry;


# instance fields
.field private first_:Ljava/lang/String;

.field private hasFirst:Z

.field private hasSecond:Z

.field private hasWeight:Z

.field private memoizedSerializedSize:I

.field private second_:Ljava/lang/String;

.field private weight_:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 336
    new-instance v0, Lspeech/patts/SparseTableEntry;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/SparseTableEntry;-><init>(Z)V

    sput-object v0, Lspeech/patts/SparseTableEntry;->defaultInstance:Lspeech/patts/SparseTableEntry;

    .line 337
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 338
    sget-object v0, Lspeech/patts/SparseTableEntry;->defaultInstance:Lspeech/patts/SparseTableEntry;

    invoke-direct {v0}, Lspeech/patts/SparseTableEntry;->initFields()V

    .line 339
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/SparseTableEntry;->first_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/SparseTableEntry;->second_:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/SparseTableEntry;->weight_:F

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/SparseTableEntry;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/SparseTableEntry;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/SparseTableEntry$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/SparseTableEntry$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/SparseTableEntry;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/SparseTableEntry;->first_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/SparseTableEntry;->second_:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/SparseTableEntry;->weight_:F

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/SparseTableEntry;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lspeech/patts/SparseTableEntry;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/SparseTableEntry;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/SparseTableEntry;->hasFirst:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/SparseTableEntry;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/SparseTableEntry;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/SparseTableEntry;->first_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/SparseTableEntry;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/SparseTableEntry;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/SparseTableEntry;->hasSecond:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/SparseTableEntry;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/SparseTableEntry;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/SparseTableEntry;->second_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lspeech/patts/SparseTableEntry;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/SparseTableEntry;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/SparseTableEntry;->hasWeight:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/SparseTableEntry;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/SparseTableEntry;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/SparseTableEntry;->weight_:F

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/SparseTableEntry;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/SparseTableEntry;->defaultInstance:Lspeech/patts/SparseTableEntry;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public static newBuilder()Lspeech/patts/SparseTableEntry$Builder;
    .locals 1

    .prologue
    .line 155
    # invokes: Lspeech/patts/SparseTableEntry$Builder;->create()Lspeech/patts/SparseTableEntry$Builder;
    invoke-static {}, Lspeech/patts/SparseTableEntry$Builder;->access$100()Lspeech/patts/SparseTableEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/SparseTableEntry;)Lspeech/patts/SparseTableEntry$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/SparseTableEntry;

    .prologue
    .line 158
    invoke-static {}, Lspeech/patts/SparseTableEntry;->newBuilder()Lspeech/patts/SparseTableEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/SparseTableEntry$Builder;->mergeFrom(Lspeech/patts/SparseTableEntry;)Lspeech/patts/SparseTableEntry$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->getDefaultInstanceForType()Lspeech/patts/SparseTableEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/SparseTableEntry;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/SparseTableEntry;->defaultInstance:Lspeech/patts/SparseTableEntry;

    return-object v0
.end method

.method public getFirst()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/SparseTableEntry;->first_:Ljava/lang/String;

    return-object v0
.end method

.method public getSecond()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lspeech/patts/SparseTableEntry;->second_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 68
    iget v0, p0, Lspeech/patts/SparseTableEntry;->memoizedSerializedSize:I

    .line 69
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 85
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 71
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 72
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->hasFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 73
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->getFirst()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 76
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->hasSecond()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 77
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->getSecond()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 80
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->hasWeight()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 81
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->getWeight()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 84
    :cond_3
    iput v0, p0, Lspeech/patts/SparseTableEntry;->memoizedSerializedSize:I

    move v1, v0

    .line 85
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getWeight()F
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lspeech/patts/SparseTableEntry;->weight_:F

    return v0
.end method

.method public hasFirst()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/SparseTableEntry;->hasFirst:Z

    return v0
.end method

.method public hasSecond()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/SparseTableEntry;->hasSecond:Z

    return v0
.end method

.method public hasWeight()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lspeech/patts/SparseTableEntry;->hasWeight:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 46
    iget-boolean v1, p0, Lspeech/patts/SparseTableEntry;->hasFirst:Z

    if-nez v1, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v0

    .line 47
    :cond_1
    iget-boolean v1, p0, Lspeech/patts/SparseTableEntry;->hasSecond:Z

    if-eqz v1, :cond_0

    .line 48
    iget-boolean v1, p0, Lspeech/patts/SparseTableEntry;->hasWeight:Z

    if-eqz v1, :cond_0

    .line 49
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->toBuilder()Lspeech/patts/SparseTableEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/SparseTableEntry$Builder;
    .locals 1

    .prologue
    .line 160
    invoke-static {p0}, Lspeech/patts/SparseTableEntry;->newBuilder(Lspeech/patts/SparseTableEntry;)Lspeech/patts/SparseTableEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->getSerializedSize()I

    .line 55
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->hasFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->getFirst()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 58
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->hasSecond()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->getSecond()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 61
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->hasWeight()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry;->getWeight()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 64
    :cond_2
    return-void
.end method
