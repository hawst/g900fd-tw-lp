.class public final Lspeech/patts/CompactRankerModel;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "CompactRankerModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/CompactRankerModel$1;,
        Lspeech/patts/CompactRankerModel$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/CompactRankerModel;


# instance fields
.field private hasHashFunction:Z

.field private hashFunction_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private weightMemoizedSerializedSize:I

.field private weight_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 340
    new-instance v0, Lspeech/patts/CompactRankerModel;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/CompactRankerModel;-><init>(Z)V

    sput-object v0, Lspeech/patts/CompactRankerModel;->defaultInstance:Lspeech/patts/CompactRankerModel;

    .line 341
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 342
    sget-object v0, Lspeech/patts/CompactRankerModel;->defaultInstance:Lspeech/patts/CompactRankerModel;

    invoke-direct {v0}, Lspeech/patts/CompactRankerModel;->initFields()V

    .line 343
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;

    .line 33
    iput v1, p0, Lspeech/patts/CompactRankerModel;->weightMemoizedSerializedSize:I

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/CompactRankerModel;->hashFunction_:Ljava/lang/String;

    .line 63
    iput v1, p0, Lspeech/patts/CompactRankerModel;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/CompactRankerModel;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/CompactRankerModel$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/CompactRankerModel$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/CompactRankerModel;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, -0x1

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;

    .line 33
    iput v1, p0, Lspeech/patts/CompactRankerModel;->weightMemoizedSerializedSize:I

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/CompactRankerModel;->hashFunction_:Ljava/lang/String;

    .line 63
    iput v1, p0, Lspeech/patts/CompactRankerModel;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/CompactRankerModel;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CompactRankerModel;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/CompactRankerModel;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CompactRankerModel;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/CompactRankerModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CompactRankerModel;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/CompactRankerModel;->hasHashFunction:Z

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/CompactRankerModel;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CompactRankerModel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CompactRankerModel;->hashFunction_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/CompactRankerModel;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/CompactRankerModel;->defaultInstance:Lspeech/patts/CompactRankerModel;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method public static newBuilder()Lspeech/patts/CompactRankerModel$Builder;
    .locals 1

    .prologue
    .line 155
    # invokes: Lspeech/patts/CompactRankerModel$Builder;->create()Lspeech/patts/CompactRankerModel$Builder;
    invoke-static {}, Lspeech/patts/CompactRankerModel$Builder;->access$100()Lspeech/patts/CompactRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/CompactRankerModel;)Lspeech/patts/CompactRankerModel$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/CompactRankerModel;

    .prologue
    .line 158
    invoke-static {}, Lspeech/patts/CompactRankerModel;->newBuilder()Lspeech/patts/CompactRankerModel$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/CompactRankerModel$Builder;->mergeFrom(Lspeech/patts/CompactRankerModel;)Lspeech/patts/CompactRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel;->getDefaultInstanceForType()Lspeech/patts/CompactRankerModel;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/CompactRankerModel;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/CompactRankerModel;->defaultInstance:Lspeech/patts/CompactRankerModel;

    return-object v0
.end method

.method public getHashFunction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lspeech/patts/CompactRankerModel;->hashFunction_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 65
    iget v1, p0, Lspeech/patts/CompactRankerModel;->memoizedSerializedSize:I

    .line 66
    .local v1, "size":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 85
    .end local v1    # "size":I
    .local v2, "size":I
    :goto_0
    return v2

    .line 68
    .end local v2    # "size":I
    .restart local v1    # "size":I
    :cond_0
    const/4 v1, 0x0

    .line 70
    const/4 v0, 0x0

    .line 71
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel;->getWeightList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 72
    add-int/2addr v1, v0

    .line 73
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel;->getWeightList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 74
    add-int/lit8 v1, v1, 0x1

    .line 75
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 78
    :cond_1
    iput v0, p0, Lspeech/patts/CompactRankerModel;->weightMemoizedSerializedSize:I

    .line 80
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel;->hasHashFunction()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 81
    const/4 v3, 0x2

    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel;->getHashFunction()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 84
    :cond_2
    iput v1, p0, Lspeech/patts/CompactRankerModel;->memoizedSerializedSize:I

    move v2, v1

    .line 85
    .end local v1    # "size":I
    .restart local v2    # "size":I
    goto :goto_0
.end method

.method public getWeightList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;

    return-object v0
.end method

.method public hasHashFunction()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lspeech/patts/CompactRankerModel;->hasHashFunction:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel;->toBuilder()Lspeech/patts/CompactRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/CompactRankerModel$Builder;
    .locals 1

    .prologue
    .line 160
    invoke-static {p0}, Lspeech/patts/CompactRankerModel;->newBuilder(Lspeech/patts/CompactRankerModel;)Lspeech/patts/CompactRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel;->getSerializedSize()I

    .line 51
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel;->getWeightList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 52
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 53
    iget v2, p0, Lspeech/patts/CompactRankerModel;->weightMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 55
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel;->getWeightList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 56
    .local v0, "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_0

    .line 58
    .end local v0    # "element":F
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel;->hasHashFunction()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 59
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel;->getHashFunction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 61
    :cond_2
    return-void
.end method
