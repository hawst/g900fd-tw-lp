.class public final enum Lspeech/patts/Token$PauseLength;
.super Ljava/lang/Enum;
.source "Token.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Token;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PauseLength"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/Token$PauseLength;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/Token$PauseLength;

.field public static final enum PAUSE_LONG:Lspeech/patts/Token$PauseLength;

.field public static final enum PAUSE_MEDIUM:Lspeech/patts/Token$PauseLength;

.field public static final enum PAUSE_NONE:Lspeech/patts/Token$PauseLength;

.field public static final enum PAUSE_SHORT:Lspeech/patts/Token$PauseLength;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/Token$PauseLength;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 114
    new-instance v0, Lspeech/patts/Token$PauseLength;

    const-string v1, "PAUSE_NONE"

    invoke-direct {v0, v1, v2, v2, v2}, Lspeech/patts/Token$PauseLength;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$PauseLength;->PAUSE_NONE:Lspeech/patts/Token$PauseLength;

    .line 115
    new-instance v0, Lspeech/patts/Token$PauseLength;

    const-string v1, "PAUSE_SHORT"

    invoke-direct {v0, v1, v3, v3, v3}, Lspeech/patts/Token$PauseLength;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$PauseLength;->PAUSE_SHORT:Lspeech/patts/Token$PauseLength;

    .line 116
    new-instance v0, Lspeech/patts/Token$PauseLength;

    const-string v1, "PAUSE_MEDIUM"

    invoke-direct {v0, v1, v4, v4, v4}, Lspeech/patts/Token$PauseLength;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$PauseLength;->PAUSE_MEDIUM:Lspeech/patts/Token$PauseLength;

    .line 117
    new-instance v0, Lspeech/patts/Token$PauseLength;

    const-string v1, "PAUSE_LONG"

    invoke-direct {v0, v1, v5, v5, v5}, Lspeech/patts/Token$PauseLength;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$PauseLength;->PAUSE_LONG:Lspeech/patts/Token$PauseLength;

    .line 112
    const/4 v0, 0x4

    new-array v0, v0, [Lspeech/patts/Token$PauseLength;

    sget-object v1, Lspeech/patts/Token$PauseLength;->PAUSE_NONE:Lspeech/patts/Token$PauseLength;

    aput-object v1, v0, v2

    sget-object v1, Lspeech/patts/Token$PauseLength;->PAUSE_SHORT:Lspeech/patts/Token$PauseLength;

    aput-object v1, v0, v3

    sget-object v1, Lspeech/patts/Token$PauseLength;->PAUSE_MEDIUM:Lspeech/patts/Token$PauseLength;

    aput-object v1, v0, v4

    sget-object v1, Lspeech/patts/Token$PauseLength;->PAUSE_LONG:Lspeech/patts/Token$PauseLength;

    aput-object v1, v0, v5

    sput-object v0, Lspeech/patts/Token$PauseLength;->$VALUES:[Lspeech/patts/Token$PauseLength;

    .line 138
    new-instance v0, Lspeech/patts/Token$PauseLength$1;

    invoke-direct {v0}, Lspeech/patts/Token$PauseLength$1;-><init>()V

    sput-object v0, Lspeech/patts/Token$PauseLength;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 148
    iput p3, p0, Lspeech/patts/Token$PauseLength;->index:I

    .line 149
    iput p4, p0, Lspeech/patts/Token$PauseLength;->value:I

    .line 150
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/Token$PauseLength;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 112
    const-class v0, Lspeech/patts/Token$PauseLength;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/Token$PauseLength;

    return-object v0
.end method

.method public static values()[Lspeech/patts/Token$PauseLength;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lspeech/patts/Token$PauseLength;->$VALUES:[Lspeech/patts/Token$PauseLength;

    invoke-virtual {v0}, [Lspeech/patts/Token$PauseLength;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/Token$PauseLength;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lspeech/patts/Token$PauseLength;->value:I

    return v0
.end method
