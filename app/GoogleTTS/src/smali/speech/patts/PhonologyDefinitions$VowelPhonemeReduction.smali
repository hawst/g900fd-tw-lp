.class public final Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PhonologyDefinitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PhonologyDefinitions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VowelPhonemeReduction"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;


# instance fields
.field private hasStressedPhoneme:Z

.field private hasUnstressedPhoneme:Z

.field private hasVowelLetter:Z

.field private memoizedSerializedSize:I

.field private stressedPhoneme_:Ljava/lang/String;

.field private unstressedPhoneme_:Ljava/lang/String;

.field private vowelLetter_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 696
    new-instance v0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;-><init>(Z)V

    sput-object v0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->defaultInstance:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    .line 697
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 698
    sget-object v0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->defaultInstance:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    invoke-direct {v0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->initFields()V

    .line 699
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 365
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 382
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->vowelLetter_:Ljava/lang/String;

    .line 389
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->stressedPhoneme_:Ljava/lang/String;

    .line 396
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->unstressedPhoneme_:Ljava/lang/String;

    .line 423
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->memoizedSerializedSize:I

    .line 366
    invoke-direct {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->initFields()V

    .line 367
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/PhonologyDefinitions$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/PhonologyDefinitions$1;

    .prologue
    .line 362
    invoke-direct {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 368
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 382
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->vowelLetter_:Ljava/lang/String;

    .line 389
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->stressedPhoneme_:Ljava/lang/String;

    .line 396
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->unstressedPhoneme_:Ljava/lang/String;

    .line 423
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->memoizedSerializedSize:I

    .line 368
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    .param p1, "x1"    # Z

    .prologue
    .line 362
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasVowelLetter:Z

    return p1
.end method

.method static synthetic access$1102(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 362
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->vowelLetter_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    .param p1, "x1"    # Z

    .prologue
    .line 362
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasStressedPhoneme:Z

    return p1
.end method

.method static synthetic access$1302(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 362
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->stressedPhoneme_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1402(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    .param p1, "x1"    # Z

    .prologue
    .line 362
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasUnstressedPhoneme:Z

    return p1
.end method

.method static synthetic access$1502(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 362
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->unstressedPhoneme_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    .locals 1

    .prologue
    .line 372
    sget-object v0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->defaultInstance:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 401
    return-void
.end method

.method public static newBuilder()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    .locals 1

    .prologue
    .line 512
    # invokes: Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->create()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->access$800()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;)Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    .prologue
    .line 515
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->newBuilder()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->mergeFrom(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;)Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 362
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->getDefaultInstanceForType()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    .locals 1

    .prologue
    .line 376
    sget-object v0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->defaultInstance:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 425
    iget v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->memoizedSerializedSize:I

    .line 426
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 442
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 428
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 429
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasVowelLetter()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 430
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->getVowelLetter()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 433
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasStressedPhoneme()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 434
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->getStressedPhoneme()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 437
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasUnstressedPhoneme()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 438
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->getUnstressedPhoneme()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 441
    :cond_3
    iput v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->memoizedSerializedSize:I

    move v1, v0

    .line 442
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getStressedPhoneme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->stressedPhoneme_:Ljava/lang/String;

    return-object v0
.end method

.method public getUnstressedPhoneme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->unstressedPhoneme_:Ljava/lang/String;

    return-object v0
.end method

.method public getVowelLetter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->vowelLetter_:Ljava/lang/String;

    return-object v0
.end method

.method public hasStressedPhoneme()Z
    .locals 1

    .prologue
    .line 390
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasStressedPhoneme:Z

    return v0
.end method

.method public hasUnstressedPhoneme()Z
    .locals 1

    .prologue
    .line 397
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasUnstressedPhoneme:Z

    return v0
.end method

.method public hasVowelLetter()Z
    .locals 1

    .prologue
    .line 383
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasVowelLetter:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 403
    iget-boolean v1, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasVowelLetter:Z

    if-nez v1, :cond_1

    .line 406
    :cond_0
    :goto_0
    return v0

    .line 404
    :cond_1
    iget-boolean v1, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasStressedPhoneme:Z

    if-eqz v1, :cond_0

    .line 405
    iget-boolean v1, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasUnstressedPhoneme:Z

    if-eqz v1, :cond_0

    .line 406
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 362
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->toBuilder()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    .locals 1

    .prologue
    .line 517
    invoke-static {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->newBuilder(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;)Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 411
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->getSerializedSize()I

    .line 412
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasVowelLetter()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->getVowelLetter()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 415
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasStressedPhoneme()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->getStressedPhoneme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 418
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasUnstressedPhoneme()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 419
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->getUnstressedPhoneme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 421
    :cond_2
    return-void
.end method
