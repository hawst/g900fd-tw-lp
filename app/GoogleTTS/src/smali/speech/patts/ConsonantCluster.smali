.class public final Lspeech/patts/ConsonantCluster;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "ConsonantCluster.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/ConsonantCluster$1;,
        Lspeech/patts/ConsonantCluster$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/ConsonantCluster;


# instance fields
.field private group_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 288
    new-instance v0, Lspeech/patts/ConsonantCluster;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/ConsonantCluster;-><init>(Z)V

    sput-object v0, Lspeech/patts/ConsonantCluster;->defaultInstance:Lspeech/patts/ConsonantCluster;

    .line 289
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 290
    sget-object v0, Lspeech/patts/ConsonantCluster;->defaultInstance:Lspeech/patts/ConsonantCluster;

    invoke-direct {v0}, Lspeech/patts/ConsonantCluster;->initFields()V

    .line 291
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/ConsonantCluster;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/ConsonantCluster;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/ConsonantCluster$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/ConsonantCluster$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/ConsonantCluster;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/ConsonantCluster;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/ConsonantCluster;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ConsonantCluster;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/ConsonantCluster;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ConsonantCluster;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/ConsonantCluster;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/ConsonantCluster;->defaultInstance:Lspeech/patts/ConsonantCluster;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method

.method public static newBuilder()Lspeech/patts/ConsonantCluster$Builder;
    .locals 1

    .prologue
    .line 134
    # invokes: Lspeech/patts/ConsonantCluster$Builder;->create()Lspeech/patts/ConsonantCluster$Builder;
    invoke-static {}, Lspeech/patts/ConsonantCluster$Builder;->access$100()Lspeech/patts/ConsonantCluster$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/ConsonantCluster;)Lspeech/patts/ConsonantCluster$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/ConsonantCluster;

    .prologue
    .line 137
    invoke-static {}, Lspeech/patts/ConsonantCluster;->newBuilder()Lspeech/patts/ConsonantCluster$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/ConsonantCluster$Builder;->mergeFrom(Lspeech/patts/ConsonantCluster;)Lspeech/patts/ConsonantCluster$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/ConsonantCluster;->getDefaultInstanceForType()Lspeech/patts/ConsonantCluster;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/ConsonantCluster;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/ConsonantCluster;->defaultInstance:Lspeech/patts/ConsonantCluster;

    return-object v0
.end method

.method public getGroupList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 50
    iget v3, p0, Lspeech/patts/ConsonantCluster;->memoizedSerializedSize:I

    .line 51
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 64
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 53
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 55
    const/4 v0, 0x0

    .line 56
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/ConsonantCluster;->getGroupList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 57
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 59
    goto :goto_1

    .line 60
    .end local v1    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v3, v0

    .line 61
    invoke-virtual {p0}, Lspeech/patts/ConsonantCluster;->getGroupList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 63
    iput v3, p0, Lspeech/patts/ConsonantCluster;->memoizedSerializedSize:I

    move v4, v3

    .line 64
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/ConsonantCluster;->toBuilder()Lspeech/patts/ConsonantCluster$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/ConsonantCluster$Builder;
    .locals 1

    .prologue
    .line 139
    invoke-static {p0}, Lspeech/patts/ConsonantCluster;->newBuilder(Lspeech/patts/ConsonantCluster;)Lspeech/patts/ConsonantCluster$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0}, Lspeech/patts/ConsonantCluster;->getSerializedSize()I

    .line 43
    invoke-virtual {p0}, Lspeech/patts/ConsonantCluster;->getGroupList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 44
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 46
    .end local v0    # "element":Ljava/lang/String;
    :cond_0
    return-void
.end method
