.class public final Lspeech/patts/ConsonantCluster$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ConsonantCluster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/ConsonantCluster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/ConsonantCluster;",
        "Lspeech/patts/ConsonantCluster$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/ConsonantCluster;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/ConsonantCluster$Builder;
    .locals 1

    .prologue
    .line 141
    invoke-static {}, Lspeech/patts/ConsonantCluster$Builder;->create()Lspeech/patts/ConsonantCluster$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/ConsonantCluster$Builder;
    .locals 3

    .prologue
    .line 150
    new-instance v0, Lspeech/patts/ConsonantCluster$Builder;

    invoke-direct {v0}, Lspeech/patts/ConsonantCluster$Builder;-><init>()V

    .line 151
    .local v0, "builder":Lspeech/patts/ConsonantCluster$Builder;
    new-instance v1, Lspeech/patts/ConsonantCluster;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/ConsonantCluster;-><init>(Lspeech/patts/ConsonantCluster$1;)V

    iput-object v1, v0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    .line 152
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lspeech/patts/ConsonantCluster$Builder;->build()Lspeech/patts/ConsonantCluster;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/ConsonantCluster;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/ConsonantCluster$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    iget-object v0, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    invoke-static {v0}, Lspeech/patts/ConsonantCluster$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 183
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/ConsonantCluster$Builder;->buildPartial()Lspeech/patts/ConsonantCluster;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/ConsonantCluster;
    .locals 3

    .prologue
    .line 196
    iget-object v1, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    if-nez v1, :cond_0

    .line 197
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 200
    :cond_0
    iget-object v1, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    # getter for: Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/ConsonantCluster;->access$300(Lspeech/patts/ConsonantCluster;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 201
    iget-object v1, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    iget-object v2, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    # getter for: Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/ConsonantCluster;->access$300(Lspeech/patts/ConsonantCluster;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/ConsonantCluster;->access$302(Lspeech/patts/ConsonantCluster;Ljava/util/List;)Ljava/util/List;

    .line 204
    :cond_1
    iget-object v0, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    .line 205
    .local v0, "returnMe":Lspeech/patts/ConsonantCluster;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    .line 206
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lspeech/patts/ConsonantCluster$Builder;->clone()Lspeech/patts/ConsonantCluster$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lspeech/patts/ConsonantCluster$Builder;->clone()Lspeech/patts/ConsonantCluster$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 141
    invoke-virtual {p0}, Lspeech/patts/ConsonantCluster$Builder;->clone()Lspeech/patts/ConsonantCluster$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/ConsonantCluster$Builder;
    .locals 2

    .prologue
    .line 169
    invoke-static {}, Lspeech/patts/ConsonantCluster$Builder;->create()Lspeech/patts/ConsonantCluster$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    invoke-virtual {v0, v1}, Lspeech/patts/ConsonantCluster$Builder;->mergeFrom(Lspeech/patts/ConsonantCluster;)Lspeech/patts/ConsonantCluster$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    invoke-virtual {v0}, Lspeech/patts/ConsonantCluster;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 141
    check-cast p1, Lspeech/patts/ConsonantCluster;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/ConsonantCluster$Builder;->mergeFrom(Lspeech/patts/ConsonantCluster;)Lspeech/patts/ConsonantCluster$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/ConsonantCluster;)Lspeech/patts/ConsonantCluster$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/ConsonantCluster;

    .prologue
    .line 210
    invoke-static {}, Lspeech/patts/ConsonantCluster;->getDefaultInstance()Lspeech/patts/ConsonantCluster;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-object p0

    .line 211
    :cond_1
    # getter for: Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ConsonantCluster;->access$300(Lspeech/patts/ConsonantCluster;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    # getter for: Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ConsonantCluster;->access$300(Lspeech/patts/ConsonantCluster;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    iget-object v0, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/ConsonantCluster;->access$302(Lspeech/patts/ConsonantCluster;Ljava/util/List;)Ljava/util/List;

    .line 215
    :cond_2
    iget-object v0, p0, Lspeech/patts/ConsonantCluster$Builder;->result:Lspeech/patts/ConsonantCluster;

    # getter for: Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ConsonantCluster;->access$300(Lspeech/patts/ConsonantCluster;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/ConsonantCluster;->group_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ConsonantCluster;->access$300(Lspeech/patts/ConsonantCluster;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
