.class public final Lspeech/patts/UnitColumn;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "UnitColumn.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/UnitColumn$1;,
        Lspeech/patts/UnitColumn$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/UnitColumn;


# instance fields
.field private memoizedSerializedSize:I

.field private slot_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/UnitSlot;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 296
    new-instance v0, Lspeech/patts/UnitColumn;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/UnitColumn;-><init>(Z)V

    sput-object v0, Lspeech/patts/UnitColumn;->defaultInstance:Lspeech/patts/UnitColumn;

    .line 297
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 298
    sget-object v0, Lspeech/patts/UnitColumn;->defaultInstance:Lspeech/patts/UnitColumn;

    invoke-direct {v0}, Lspeech/patts/UnitColumn;->initFields()V

    .line 299
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/UnitColumn;->slot_:Ljava/util/List;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/UnitColumn;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/UnitColumn;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/UnitColumn$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/UnitColumn$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/UnitColumn;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/UnitColumn;->slot_:Ljava/util/List;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/UnitColumn;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/UnitColumn;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/UnitColumn;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/UnitColumn;->slot_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/UnitColumn;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UnitColumn;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/UnitColumn;->slot_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/UnitColumn;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/UnitColumn;->defaultInstance:Lspeech/patts/UnitColumn;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method

.method public static newBuilder()Lspeech/patts/UnitColumn$Builder;
    .locals 1

    .prologue
    .line 129
    # invokes: Lspeech/patts/UnitColumn$Builder;->create()Lspeech/patts/UnitColumn$Builder;
    invoke-static {}, Lspeech/patts/UnitColumn$Builder;->access$100()Lspeech/patts/UnitColumn$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/UnitColumn;)Lspeech/patts/UnitColumn$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/UnitColumn;

    .prologue
    .line 132
    invoke-static {}, Lspeech/patts/UnitColumn;->newBuilder()Lspeech/patts/UnitColumn$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/UnitColumn$Builder;->mergeFrom(Lspeech/patts/UnitColumn;)Lspeech/patts/UnitColumn$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/UnitColumn;->getDefaultInstanceForType()Lspeech/patts/UnitColumn;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/UnitColumn;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/UnitColumn;->defaultInstance:Lspeech/patts/UnitColumn;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 50
    iget v2, p0, Lspeech/patts/UnitColumn;->memoizedSerializedSize:I

    .line 51
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 59
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 53
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 54
    invoke-virtual {p0}, Lspeech/patts/UnitColumn;->getSlotList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/UnitSlot;

    .line 55
    .local v0, "element":Lspeech/patts/UnitSlot;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 57
    goto :goto_1

    .line 58
    .end local v0    # "element":Lspeech/patts/UnitSlot;
    :cond_1
    iput v2, p0, Lspeech/patts/UnitColumn;->memoizedSerializedSize:I

    move v3, v2

    .line 59
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getSlotList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/UnitSlot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/UnitColumn;->slot_:Ljava/util/List;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/UnitColumn;->toBuilder()Lspeech/patts/UnitColumn$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/UnitColumn$Builder;
    .locals 1

    .prologue
    .line 134
    invoke-static {p0}, Lspeech/patts/UnitColumn;->newBuilder(Lspeech/patts/UnitColumn;)Lspeech/patts/UnitColumn$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0}, Lspeech/patts/UnitColumn;->getSerializedSize()I

    .line 43
    invoke-virtual {p0}, Lspeech/patts/UnitColumn;->getSlotList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/UnitSlot;

    .line 44
    .local v0, "element":Lspeech/patts/UnitSlot;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 46
    .end local v0    # "element":Lspeech/patts/UnitSlot;
    :cond_0
    return-void
.end method
