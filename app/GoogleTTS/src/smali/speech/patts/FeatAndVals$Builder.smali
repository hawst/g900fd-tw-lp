.class public final Lspeech/patts/FeatAndVals$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FeatAndVals.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/FeatAndVals;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/FeatAndVals;",
        "Lspeech/patts/FeatAndVals$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/FeatAndVals;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/FeatAndVals$Builder;
    .locals 1

    .prologue
    .line 153
    invoke-static {}, Lspeech/patts/FeatAndVals$Builder;->create()Lspeech/patts/FeatAndVals$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/FeatAndVals$Builder;
    .locals 3

    .prologue
    .line 162
    new-instance v0, Lspeech/patts/FeatAndVals$Builder;

    invoke-direct {v0}, Lspeech/patts/FeatAndVals$Builder;-><init>()V

    .line 163
    .local v0, "builder":Lspeech/patts/FeatAndVals$Builder;
    new-instance v1, Lspeech/patts/FeatAndVals;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/FeatAndVals;-><init>(Lspeech/patts/FeatAndVals$1;)V

    iput-object v1, v0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    .line 164
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals$Builder;->build()Lspeech/patts/FeatAndVals;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/FeatAndVals;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/FeatAndVals$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    iget-object v0, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    invoke-static {v0}, Lspeech/patts/FeatAndVals$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 195
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals$Builder;->buildPartial()Lspeech/patts/FeatAndVals;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/FeatAndVals;
    .locals 3

    .prologue
    .line 208
    iget-object v1, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    if-nez v1, :cond_0

    .line 209
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 212
    :cond_0
    iget-object v1, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    # getter for: Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FeatAndVals;->access$300(Lspeech/patts/FeatAndVals;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 213
    iget-object v1, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    iget-object v2, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    # getter for: Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FeatAndVals;->access$300(Lspeech/patts/FeatAndVals;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FeatAndVals;->access$302(Lspeech/patts/FeatAndVals;Ljava/util/List;)Ljava/util/List;

    .line 216
    :cond_1
    iget-object v0, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    .line 217
    .local v0, "returnMe":Lspeech/patts/FeatAndVals;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    .line 218
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals$Builder;->clone()Lspeech/patts/FeatAndVals$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals$Builder;->clone()Lspeech/patts/FeatAndVals$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 153
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals$Builder;->clone()Lspeech/patts/FeatAndVals$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/FeatAndVals$Builder;
    .locals 2

    .prologue
    .line 181
    invoke-static {}, Lspeech/patts/FeatAndVals$Builder;->create()Lspeech/patts/FeatAndVals$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    invoke-virtual {v0, v1}, Lspeech/patts/FeatAndVals$Builder;->mergeFrom(Lspeech/patts/FeatAndVals;)Lspeech/patts/FeatAndVals$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    invoke-virtual {v0}, Lspeech/patts/FeatAndVals;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 153
    check-cast p1, Lspeech/patts/FeatAndVals;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/FeatAndVals$Builder;->mergeFrom(Lspeech/patts/FeatAndVals;)Lspeech/patts/FeatAndVals$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/FeatAndVals;)Lspeech/patts/FeatAndVals$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/FeatAndVals;

    .prologue
    .line 222
    invoke-static {}, Lspeech/patts/FeatAndVals;->getDefaultInstance()Lspeech/patts/FeatAndVals;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-object p0

    .line 223
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/FeatAndVals;->hasFeat()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 224
    invoke-virtual {p1}, Lspeech/patts/FeatAndVals;->getFeat()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FeatAndVals$Builder;->setFeat(Ljava/lang/String;)Lspeech/patts/FeatAndVals$Builder;

    .line 226
    :cond_2
    # getter for: Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatAndVals;->access$300(Lspeech/patts/FeatAndVals;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    # getter for: Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatAndVals;->access$300(Lspeech/patts/FeatAndVals;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 228
    iget-object v0, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FeatAndVals;->access$302(Lspeech/patts/FeatAndVals;Ljava/util/List;)Ljava/util/List;

    .line 230
    :cond_3
    iget-object v0, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    # getter for: Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatAndVals;->access$300(Lspeech/patts/FeatAndVals;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatAndVals;->access$300(Lspeech/patts/FeatAndVals;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public setFeat(Ljava/lang/String;)Lspeech/patts/FeatAndVals$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 280
    if-nez p1, :cond_0

    .line 281
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 283
    :cond_0
    iget-object v0, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FeatAndVals;->hasFeat:Z
    invoke-static {v0, v1}, Lspeech/patts/FeatAndVals;->access$402(Lspeech/patts/FeatAndVals;Z)Z

    .line 284
    iget-object v0, p0, Lspeech/patts/FeatAndVals$Builder;->result:Lspeech/patts/FeatAndVals;

    # setter for: Lspeech/patts/FeatAndVals;->feat_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/FeatAndVals;->access$502(Lspeech/patts/FeatAndVals;Ljava/lang/String;)Ljava/lang/String;

    .line 285
    return-object p0
.end method
