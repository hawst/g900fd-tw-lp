.class public final Lspeech/patts/BaseType2$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "BaseType2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/BaseType2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/BaseType2;",
        "Lspeech/patts/BaseType2$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/BaseType2;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/BaseType2$Builder;
    .locals 1

    .prologue
    .line 156
    invoke-static {}, Lspeech/patts/BaseType2$Builder;->create()Lspeech/patts/BaseType2$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/BaseType2$Builder;
    .locals 3

    .prologue
    .line 165
    new-instance v0, Lspeech/patts/BaseType2$Builder;

    invoke-direct {v0}, Lspeech/patts/BaseType2$Builder;-><init>()V

    .line 166
    .local v0, "builder":Lspeech/patts/BaseType2$Builder;
    new-instance v1, Lspeech/patts/BaseType2;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/BaseType2;-><init>(Lspeech/patts/BaseType2$1;)V

    iput-object v1, v0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    .line 167
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0}, Lspeech/patts/BaseType2$Builder;->build()Lspeech/patts/BaseType2;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/BaseType2;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/BaseType2$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    invoke-static {v0}, Lspeech/patts/BaseType2$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 198
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/BaseType2$Builder;->buildPartial()Lspeech/patts/BaseType2;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/BaseType2;
    .locals 3

    .prologue
    .line 211
    iget-object v1, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    if-nez v1, :cond_0

    .line 212
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 215
    :cond_0
    iget-object v1, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    # getter for: Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/BaseType2;->access$300(Lspeech/patts/BaseType2;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 216
    iget-object v1, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    iget-object v2, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    # getter for: Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/BaseType2;->access$300(Lspeech/patts/BaseType2;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/BaseType2;->access$302(Lspeech/patts/BaseType2;Ljava/util/List;)Ljava/util/List;

    .line 219
    :cond_1
    iget-object v0, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    .line 220
    .local v0, "returnMe":Lspeech/patts/BaseType2;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    .line 221
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0}, Lspeech/patts/BaseType2$Builder;->clone()Lspeech/patts/BaseType2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0}, Lspeech/patts/BaseType2$Builder;->clone()Lspeech/patts/BaseType2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 156
    invoke-virtual {p0}, Lspeech/patts/BaseType2$Builder;->clone()Lspeech/patts/BaseType2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/BaseType2$Builder;
    .locals 2

    .prologue
    .line 184
    invoke-static {}, Lspeech/patts/BaseType2$Builder;->create()Lspeech/patts/BaseType2$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    invoke-virtual {v0, v1}, Lspeech/patts/BaseType2$Builder;->mergeFrom(Lspeech/patts/BaseType2;)Lspeech/patts/BaseType2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    invoke-virtual {v0}, Lspeech/patts/BaseType2;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 156
    check-cast p1, Lspeech/patts/BaseType2;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/BaseType2$Builder;->mergeFrom(Lspeech/patts/BaseType2;)Lspeech/patts/BaseType2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/BaseType2;)Lspeech/patts/BaseType2$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/BaseType2;

    .prologue
    .line 225
    invoke-static {}, Lspeech/patts/BaseType2;->getDefaultInstance()Lspeech/patts/BaseType2;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-object p0

    .line 226
    :cond_1
    # getter for: Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/BaseType2;->access$300(Lspeech/patts/BaseType2;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 227
    iget-object v0, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    # getter for: Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/BaseType2;->access$300(Lspeech/patts/BaseType2;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 228
    iget-object v0, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/BaseType2;->access$302(Lspeech/patts/BaseType2;Ljava/util/List;)Ljava/util/List;

    .line 230
    :cond_2
    iget-object v0, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    # getter for: Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/BaseType2;->access$300(Lspeech/patts/BaseType2;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/BaseType2;->access$300(Lspeech/patts/BaseType2;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 232
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/BaseType2;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p1}, Lspeech/patts/BaseType2;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/BaseType2$Builder;->setName(Ljava/lang/String;)Lspeech/patts/BaseType2$Builder;

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)Lspeech/patts/BaseType2$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 317
    if-nez p1, :cond_0

    .line 318
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 320
    :cond_0
    iget-object v0, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/BaseType2;->hasName:Z
    invoke-static {v0, v1}, Lspeech/patts/BaseType2;->access$402(Lspeech/patts/BaseType2;Z)Z

    .line 321
    iget-object v0, p0, Lspeech/patts/BaseType2$Builder;->result:Lspeech/patts/BaseType2;

    # setter for: Lspeech/patts/BaseType2;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/BaseType2;->access$502(Lspeech/patts/BaseType2;Ljava/lang/String;)Ljava/lang/String;

    .line 322
    return-object p0
.end method
