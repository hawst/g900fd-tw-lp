.class public final Lspeech/patts/PronWord$Component;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PronWord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PronWord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Component"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/PronWord$Component$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/PronWord$Component;


# instance fields
.field private hasId:Z

.field private hasPhonemes:Z

.field private hasRule:Z

.field private id_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private phonemes_:Ljava/lang/String;

.field private rule_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 353
    new-instance v0, Lspeech/patts/PronWord$Component;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/PronWord$Component;-><init>(Z)V

    sput-object v0, Lspeech/patts/PronWord$Component;->defaultInstance:Lspeech/patts/PronWord$Component;

    .line 354
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 355
    sget-object v0, Lspeech/patts/PronWord$Component;->defaultInstance:Lspeech/patts/PronWord$Component;

    invoke-direct {v0}, Lspeech/patts/PronWord$Component;->initFields()V

    .line 356
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PronWord$Component;->rule_:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PronWord$Component;->id_:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PronWord$Component;->phonemes_:Ljava/lang/String;

    .line 80
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PronWord$Component;->memoizedSerializedSize:I

    .line 26
    invoke-direct {p0}, Lspeech/patts/PronWord$Component;->initFields()V

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/PronWord$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/PronWord$1;

    .prologue
    .line 22
    invoke-direct {p0}, Lspeech/patts/PronWord$Component;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PronWord$Component;->rule_:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PronWord$Component;->id_:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PronWord$Component;->phonemes_:Ljava/lang/String;

    .line 80
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PronWord$Component;->memoizedSerializedSize:I

    .line 28
    return-void
.end method

.method static synthetic access$302(Lspeech/patts/PronWord$Component;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PronWord$Component;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lspeech/patts/PronWord$Component;->hasRule:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/PronWord$Component;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PronWord$Component;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lspeech/patts/PronWord$Component;->rule_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/PronWord$Component;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PronWord$Component;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lspeech/patts/PronWord$Component;->hasId:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/PronWord$Component;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PronWord$Component;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lspeech/patts/PronWord$Component;->id_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lspeech/patts/PronWord$Component;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PronWord$Component;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lspeech/patts/PronWord$Component;->hasPhonemes:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/PronWord$Component;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PronWord$Component;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lspeech/patts/PronWord$Component;->phonemes_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/PronWord$Component;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lspeech/patts/PronWord$Component;->defaultInstance:Lspeech/patts/PronWord$Component;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public static newBuilder()Lspeech/patts/PronWord$Component$Builder;
    .locals 1

    .prologue
    .line 169
    # invokes: Lspeech/patts/PronWord$Component$Builder;->create()Lspeech/patts/PronWord$Component$Builder;
    invoke-static {}, Lspeech/patts/PronWord$Component$Builder;->access$100()Lspeech/patts/PronWord$Component$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/PronWord$Component;)Lspeech/patts/PronWord$Component$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/PronWord$Component;

    .prologue
    .line 172
    invoke-static {}, Lspeech/patts/PronWord$Component;->newBuilder()Lspeech/patts/PronWord$Component$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/PronWord$Component$Builder;->mergeFrom(Lspeech/patts/PronWord$Component;)Lspeech/patts/PronWord$Component$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->getDefaultInstanceForType()Lspeech/patts/PronWord$Component;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/PronWord$Component;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lspeech/patts/PronWord$Component;->defaultInstance:Lspeech/patts/PronWord$Component;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lspeech/patts/PronWord$Component;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getPhonemes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lspeech/patts/PronWord$Component;->phonemes_:Ljava/lang/String;

    return-object v0
.end method

.method public getRule()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lspeech/patts/PronWord$Component;->rule_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 82
    iget v0, p0, Lspeech/patts/PronWord$Component;->memoizedSerializedSize:I

    .line 83
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 99
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 85
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 86
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->hasRule()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 87
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->getRule()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 90
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->hasId()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 91
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 94
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->hasPhonemes()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 95
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->getPhonemes()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 98
    :cond_3
    iput v0, p0, Lspeech/patts/PronWord$Component;->memoizedSerializedSize:I

    move v1, v0

    .line 99
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lspeech/patts/PronWord$Component;->hasId:Z

    return v0
.end method

.method public hasPhonemes()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lspeech/patts/PronWord$Component;->hasPhonemes:Z

    return v0
.end method

.method public hasRule()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lspeech/patts/PronWord$Component;->hasRule:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->toBuilder()Lspeech/patts/PronWord$Component$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/PronWord$Component$Builder;
    .locals 1

    .prologue
    .line 174
    invoke-static {p0}, Lspeech/patts/PronWord$Component;->newBuilder(Lspeech/patts/PronWord$Component;)Lspeech/patts/PronWord$Component$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->getSerializedSize()I

    .line 69
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->hasRule()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->getRule()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 72
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->hasId()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 75
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->hasPhonemes()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/PronWord$Component;->getPhonemes()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 78
    :cond_2
    return-void
.end method
