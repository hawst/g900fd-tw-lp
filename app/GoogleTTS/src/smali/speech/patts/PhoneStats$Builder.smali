.class public final Lspeech/patts/PhoneStats$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PhoneStats.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PhoneStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/PhoneStats;",
        "Lspeech/patts/PhoneStats$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/PhoneStats;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/PhoneStats$Builder;
    .locals 1

    .prologue
    .line 187
    invoke-static {}, Lspeech/patts/PhoneStats$Builder;->create()Lspeech/patts/PhoneStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/PhoneStats$Builder;
    .locals 3

    .prologue
    .line 196
    new-instance v0, Lspeech/patts/PhoneStats$Builder;

    invoke-direct {v0}, Lspeech/patts/PhoneStats$Builder;-><init>()V

    .line 197
    .local v0, "builder":Lspeech/patts/PhoneStats$Builder;
    new-instance v1, Lspeech/patts/PhoneStats;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/PhoneStats;-><init>(Lspeech/patts/PhoneStats$1;)V

    iput-object v1, v0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    .line 198
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lspeech/patts/PhoneStats$Builder;->build()Lspeech/patts/PhoneStats;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/PhoneStats;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/PhoneStats$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    invoke-static {v0}, Lspeech/patts/PhoneStats$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 229
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PhoneStats$Builder;->buildPartial()Lspeech/patts/PhoneStats;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/PhoneStats;
    .locals 3

    .prologue
    .line 242
    iget-object v1, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    if-nez v1, :cond_0

    .line 243
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 246
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    .line 247
    .local v0, "returnMe":Lspeech/patts/PhoneStats;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    .line 248
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lspeech/patts/PhoneStats$Builder;->clone()Lspeech/patts/PhoneStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lspeech/patts/PhoneStats$Builder;->clone()Lspeech/patts/PhoneStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 187
    invoke-virtual {p0}, Lspeech/patts/PhoneStats$Builder;->clone()Lspeech/patts/PhoneStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/PhoneStats$Builder;
    .locals 2

    .prologue
    .line 215
    invoke-static {}, Lspeech/patts/PhoneStats$Builder;->create()Lspeech/patts/PhoneStats$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    invoke-virtual {v0, v1}, Lspeech/patts/PhoneStats$Builder;->mergeFrom(Lspeech/patts/PhoneStats;)Lspeech/patts/PhoneStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    invoke-virtual {v0}, Lspeech/patts/PhoneStats;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 187
    check-cast p1, Lspeech/patts/PhoneStats;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/PhoneStats$Builder;->mergeFrom(Lspeech/patts/PhoneStats;)Lspeech/patts/PhoneStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/PhoneStats;)Lspeech/patts/PhoneStats$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/PhoneStats;

    .prologue
    .line 252
    invoke-static {}, Lspeech/patts/PhoneStats;->getDefaultInstance()Lspeech/patts/PhoneStats;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-object p0

    .line 253
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/PhoneStats;->hasAbszdur()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 254
    invoke-virtual {p1}, Lspeech/patts/PhoneStats;->getAbszdur()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhoneStats$Builder;->setAbszdur(F)Lspeech/patts/PhoneStats$Builder;

    .line 256
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/PhoneStats;->hasZscore()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 257
    invoke-virtual {p1}, Lspeech/patts/PhoneStats;->getZscore()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhoneStats$Builder;->setZscore(F)Lspeech/patts/PhoneStats$Builder;

    .line 259
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/PhoneStats;->hasPhonScore()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 260
    invoke-virtual {p1}, Lspeech/patts/PhoneStats;->getPhonScore()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhoneStats$Builder;->setPhonScore(F)Lspeech/patts/PhoneStats$Builder;

    .line 262
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/PhoneStats;->hasCut()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 263
    invoke-virtual {p1}, Lspeech/patts/PhoneStats;->getCut()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhoneStats$Builder;->setCut(F)Lspeech/patts/PhoneStats$Builder;

    .line 265
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/PhoneStats;->hasScore()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    invoke-virtual {p1}, Lspeech/patts/PhoneStats;->getScore()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhoneStats$Builder;->setScore(F)Lspeech/patts/PhoneStats$Builder;

    goto :goto_0
.end method

.method public setAbszdur(F)Lspeech/patts/PhoneStats$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 319
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhoneStats;->hasAbszdur:Z
    invoke-static {v0, v1}, Lspeech/patts/PhoneStats;->access$302(Lspeech/patts/PhoneStats;Z)Z

    .line 320
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    # setter for: Lspeech/patts/PhoneStats;->abszdur_:F
    invoke-static {v0, p1}, Lspeech/patts/PhoneStats;->access$402(Lspeech/patts/PhoneStats;F)F

    .line 321
    return-object p0
.end method

.method public setCut(F)Lspeech/patts/PhoneStats$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 373
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhoneStats;->hasCut:Z
    invoke-static {v0, v1}, Lspeech/patts/PhoneStats;->access$902(Lspeech/patts/PhoneStats;Z)Z

    .line 374
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    # setter for: Lspeech/patts/PhoneStats;->cut_:F
    invoke-static {v0, p1}, Lspeech/patts/PhoneStats;->access$1002(Lspeech/patts/PhoneStats;F)F

    .line 375
    return-object p0
.end method

.method public setPhonScore(F)Lspeech/patts/PhoneStats$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 355
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhoneStats;->hasPhonScore:Z
    invoke-static {v0, v1}, Lspeech/patts/PhoneStats;->access$702(Lspeech/patts/PhoneStats;Z)Z

    .line 356
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    # setter for: Lspeech/patts/PhoneStats;->phonScore_:F
    invoke-static {v0, p1}, Lspeech/patts/PhoneStats;->access$802(Lspeech/patts/PhoneStats;F)F

    .line 357
    return-object p0
.end method

.method public setScore(F)Lspeech/patts/PhoneStats$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 391
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhoneStats;->hasScore:Z
    invoke-static {v0, v1}, Lspeech/patts/PhoneStats;->access$1102(Lspeech/patts/PhoneStats;Z)Z

    .line 392
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    # setter for: Lspeech/patts/PhoneStats;->score_:F
    invoke-static {v0, p1}, Lspeech/patts/PhoneStats;->access$1202(Lspeech/patts/PhoneStats;F)F

    .line 393
    return-object p0
.end method

.method public setZscore(F)Lspeech/patts/PhoneStats$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 337
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhoneStats;->hasZscore:Z
    invoke-static {v0, v1}, Lspeech/patts/PhoneStats;->access$502(Lspeech/patts/PhoneStats;Z)Z

    .line 338
    iget-object v0, p0, Lspeech/patts/PhoneStats$Builder;->result:Lspeech/patts/PhoneStats;

    # setter for: Lspeech/patts/PhoneStats;->zscore_:F
    invoke-static {v0, p1}, Lspeech/patts/PhoneStats;->access$602(Lspeech/patts/PhoneStats;F)F

    .line 339
    return-object p0
.end method
