.class public final Lspeech/patts/Syntax;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Syntax.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/Syntax$1;,
        Lspeech/patts/Syntax$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/Syntax;


# instance fields
.field private category_:Ljava/lang/String;

.field private containsNucleus_:Z

.field private dependencyData_:Lspeech/patts/DependencyData;

.field private hasCategory:Z

.field private hasContainsNucleus:Z

.field private hasDependencyData:Z

.field private hasLemma:Z

.field private hasName:Z

.field private hasTag:Z

.field private lemma_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private rosettaAnalyzerFeatures_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private tag_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 559
    new-instance v0, Lspeech/patts/Syntax;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/Syntax;-><init>(Z)V

    sput-object v0, Lspeech/patts/Syntax;->defaultInstance:Lspeech/patts/Syntax;

    .line 560
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 561
    sget-object v0, Lspeech/patts/Syntax;->defaultInstance:Lspeech/patts/Syntax;

    invoke-direct {v0}, Lspeech/patts/Syntax;->initFields()V

    .line 562
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Syntax;->name_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Syntax;->lemma_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Syntax;->category_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Syntax;->tag_:Ljava/lang/String;

    .line 52
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lspeech/patts/Syntax;->containsNucleus_:Z

    .line 109
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Syntax;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/Syntax;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/Syntax$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/Syntax$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/Syntax;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Syntax;->name_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Syntax;->lemma_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Syntax;->category_:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Syntax;->tag_:Ljava/lang/String;

    .line 52
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lspeech/patts/Syntax;->containsNucleus_:Z

    .line 109
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Syntax;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/Syntax;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Syntax;->hasTag:Z

    return p1
.end method

.method static synthetic access$1102(Lspeech/patts/Syntax;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Syntax;->tag_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lspeech/patts/Syntax;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Syntax;->hasDependencyData:Z

    return p1
.end method

.method static synthetic access$1300(Lspeech/patts/Syntax;)Lspeech/patts/DependencyData;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Syntax;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Syntax;->dependencyData_:Lspeech/patts/DependencyData;

    return-object v0
.end method

.method static synthetic access$1302(Lspeech/patts/Syntax;Lspeech/patts/DependencyData;)Lspeech/patts/DependencyData;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Lspeech/patts/DependencyData;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Syntax;->dependencyData_:Lspeech/patts/DependencyData;

    return-object p1
.end method

.method static synthetic access$1402(Lspeech/patts/Syntax;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Syntax;->hasContainsNucleus:Z

    return p1
.end method

.method static synthetic access$1502(Lspeech/patts/Syntax;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Syntax;->containsNucleus_:Z

    return p1
.end method

.method static synthetic access$300(Lspeech/patts/Syntax;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Syntax;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/Syntax;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/Syntax;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Syntax;->hasName:Z

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/Syntax;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Syntax;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lspeech/patts/Syntax;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Syntax;->hasLemma:Z

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/Syntax;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Syntax;->lemma_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lspeech/patts/Syntax;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Syntax;->hasCategory:Z

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/Syntax;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Syntax;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Syntax;->category_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/Syntax;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/Syntax;->defaultInstance:Lspeech/patts/Syntax;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Lspeech/patts/DependencyData;->getDefaultInstance()Lspeech/patts/DependencyData;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Syntax;->dependencyData_:Lspeech/patts/DependencyData;

    .line 78
    return-void
.end method

.method public static newBuilder()Lspeech/patts/Syntax$Builder;
    .locals 1

    .prologue
    .line 219
    # invokes: Lspeech/patts/Syntax$Builder;->create()Lspeech/patts/Syntax$Builder;
    invoke-static {}, Lspeech/patts/Syntax$Builder;->access$100()Lspeech/patts/Syntax$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/Syntax;)Lspeech/patts/Syntax$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/Syntax;

    .prologue
    .line 222
    invoke-static {}, Lspeech/patts/Syntax;->newBuilder()Lspeech/patts/Syntax$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/Syntax$Builder;->mergeFrom(Lspeech/patts/Syntax;)Lspeech/patts/Syntax$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lspeech/patts/Syntax;->category_:Ljava/lang/String;

    return-object v0
.end method

.method public getContainsNucleus()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lspeech/patts/Syntax;->containsNucleus_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Syntax;->getDefaultInstanceForType()Lspeech/patts/Syntax;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/Syntax;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/Syntax;->defaultInstance:Lspeech/patts/Syntax;

    return-object v0
.end method

.method public getDependencyData()Lspeech/patts/DependencyData;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lspeech/patts/Syntax;->dependencyData_:Lspeech/patts/DependencyData;

    return-object v0
.end method

.method public getLemma()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lspeech/patts/Syntax;->lemma_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/Syntax;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getRosettaAnalyzerFeaturesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 111
    iget v3, p0, Lspeech/patts/Syntax;->memoizedSerializedSize:I

    .line 112
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 149
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 114
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 115
    invoke-virtual {p0}, Lspeech/patts/Syntax;->hasName()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 116
    const/4 v5, 0x1

    invoke-virtual {p0}, Lspeech/patts/Syntax;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 119
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Syntax;->hasLemma()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 120
    const/4 v5, 0x2

    invoke-virtual {p0}, Lspeech/patts/Syntax;->getLemma()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 123
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Syntax;->hasCategory()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 124
    const/4 v5, 0x4

    invoke-virtual {p0}, Lspeech/patts/Syntax;->getCategory()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 127
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Syntax;->hasTag()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 128
    const/4 v5, 0x5

    invoke-virtual {p0}, Lspeech/patts/Syntax;->getTag()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 132
    :cond_4
    const/4 v0, 0x0

    .line 133
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/Syntax;->getRosettaAnalyzerFeaturesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 134
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 136
    goto :goto_1

    .line 137
    .end local v1    # "element":Ljava/lang/String;
    :cond_5
    add-int/2addr v3, v0

    .line 138
    invoke-virtual {p0}, Lspeech/patts/Syntax;->getRosettaAnalyzerFeaturesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 140
    invoke-virtual {p0}, Lspeech/patts/Syntax;->hasDependencyData()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 141
    const/16 v5, 0xd

    invoke-virtual {p0}, Lspeech/patts/Syntax;->getDependencyData()Lspeech/patts/DependencyData;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 144
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/Syntax;->hasContainsNucleus()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 145
    const/16 v5, 0xe

    invoke-virtual {p0}, Lspeech/patts/Syntax;->getContainsNucleus()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 148
    :cond_7
    iput v3, p0, Lspeech/patts/Syntax;->memoizedSerializedSize:I

    move v4, v3

    .line 149
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lspeech/patts/Syntax;->tag_:Ljava/lang/String;

    return-object v0
.end method

.method public hasCategory()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lspeech/patts/Syntax;->hasCategory:Z

    return v0
.end method

.method public hasContainsNucleus()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lspeech/patts/Syntax;->hasContainsNucleus:Z

    return v0
.end method

.method public hasDependencyData()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lspeech/patts/Syntax;->hasDependencyData:Z

    return v0
.end method

.method public hasLemma()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/Syntax;->hasLemma:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/Syntax;->hasName:Z

    return v0
.end method

.method public hasTag()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lspeech/patts/Syntax;->hasTag:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Syntax;->toBuilder()Lspeech/patts/Syntax$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/Syntax$Builder;
    .locals 1

    .prologue
    .line 224
    invoke-static {p0}, Lspeech/patts/Syntax;->newBuilder(Lspeech/patts/Syntax;)Lspeech/patts/Syntax$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p0}, Lspeech/patts/Syntax;->getSerializedSize()I

    .line 86
    invoke-virtual {p0}, Lspeech/patts/Syntax;->hasName()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 87
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/Syntax;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 89
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Syntax;->hasLemma()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/Syntax;->getLemma()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 92
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Syntax;->hasCategory()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 93
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/Syntax;->getCategory()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 95
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Syntax;->hasTag()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 96
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/Syntax;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 98
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Syntax;->getRosettaAnalyzerFeaturesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 99
    .local v0, "element":Ljava/lang/String;
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 101
    .end local v0    # "element":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Syntax;->hasDependencyData()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 102
    const/16 v2, 0xd

    invoke-virtual {p0}, Lspeech/patts/Syntax;->getDependencyData()Lspeech/patts/DependencyData;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 104
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/Syntax;->hasContainsNucleus()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 105
    const/16 v2, 0xe

    invoke-virtual {p0}, Lspeech/patts/Syntax;->getContainsNucleus()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 107
    :cond_6
    return-void
.end method
