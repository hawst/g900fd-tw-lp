.class public final Lspeech/patts/RomanNumeralContextWord;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "RomanNumeralContextWord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/RomanNumeralContextWord$1;,
        Lspeech/patts/RomanNumeralContextWord$Builder;,
        Lspeech/patts/RomanNumeralContextWord$SemioticClass;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/RomanNumeralContextWord;


# instance fields
.field private allProper_:Z

.field private hasAllProper:Z

.field private hasMaxDistanceAfter:Z

.field private hasMaxDistanceBefore:Z

.field private hasMinDistanceAfter:Z

.field private hasMinDistanceBefore:Z

.field private hasMorphosyntacticFeatures:Z

.field private hasSemclass:Z

.field private maxDistanceAfter_:I

.field private maxDistanceBefore_:I

.field private memoizedSerializedSize:I

.field private minDistanceAfter_:I

.field private minDistanceBefore_:I

.field private morphosyntacticFeatures_:Ljava/lang/String;

.field private semclass_:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

.field private spelling_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 622
    new-instance v0, Lspeech/patts/RomanNumeralContextWord;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/RomanNumeralContextWord;-><init>(Z)V

    sput-object v0, Lspeech/patts/RomanNumeralContextWord;->defaultInstance:Lspeech/patts/RomanNumeralContextWord;

    .line 623
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 624
    sget-object v0, Lspeech/patts/RomanNumeralContextWord;->defaultInstance:Lspeech/patts/RomanNumeralContextWord;

    invoke-direct {v0}, Lspeech/patts/RomanNumeralContextWord;->initFields()V

    .line 625
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;

    .line 93
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/RomanNumeralContextWord;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 100
    iput v1, p0, Lspeech/patts/RomanNumeralContextWord;->minDistanceBefore_:I

    .line 107
    iput v1, p0, Lspeech/patts/RomanNumeralContextWord;->maxDistanceBefore_:I

    .line 114
    iput v1, p0, Lspeech/patts/RomanNumeralContextWord;->minDistanceAfter_:I

    .line 121
    iput v1, p0, Lspeech/patts/RomanNumeralContextWord;->maxDistanceAfter_:I

    .line 128
    iput-boolean v1, p0, Lspeech/patts/RomanNumeralContextWord;->allProper_:Z

    .line 169
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/RomanNumeralContextWord;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/RomanNumeralContextWord;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/RomanNumeralContextWord$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/RomanNumeralContextWord$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/RomanNumeralContextWord;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;

    .line 93
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/RomanNumeralContextWord;->morphosyntacticFeatures_:Ljava/lang/String;

    .line 100
    iput v1, p0, Lspeech/patts/RomanNumeralContextWord;->minDistanceBefore_:I

    .line 107
    iput v1, p0, Lspeech/patts/RomanNumeralContextWord;->maxDistanceBefore_:I

    .line 114
    iput v1, p0, Lspeech/patts/RomanNumeralContextWord;->minDistanceAfter_:I

    .line 121
    iput v1, p0, Lspeech/patts/RomanNumeralContextWord;->maxDistanceAfter_:I

    .line 128
    iput-boolean v1, p0, Lspeech/patts/RomanNumeralContextWord;->allProper_:Z

    .line 169
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/RomanNumeralContextWord;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/RomanNumeralContextWord;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/RomanNumeralContextWord;->hasMaxDistanceBefore:Z

    return p1
.end method

.method static synthetic access$1102(Lspeech/patts/RomanNumeralContextWord;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/RomanNumeralContextWord;->maxDistanceBefore_:I

    return p1
.end method

.method static synthetic access$1202(Lspeech/patts/RomanNumeralContextWord;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/RomanNumeralContextWord;->hasMinDistanceAfter:Z

    return p1
.end method

.method static synthetic access$1302(Lspeech/patts/RomanNumeralContextWord;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/RomanNumeralContextWord;->minDistanceAfter_:I

    return p1
.end method

.method static synthetic access$1402(Lspeech/patts/RomanNumeralContextWord;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/RomanNumeralContextWord;->hasMaxDistanceAfter:Z

    return p1
.end method

.method static synthetic access$1502(Lspeech/patts/RomanNumeralContextWord;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/RomanNumeralContextWord;->maxDistanceAfter_:I

    return p1
.end method

.method static synthetic access$1602(Lspeech/patts/RomanNumeralContextWord;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/RomanNumeralContextWord;->hasAllProper:Z

    return p1
.end method

.method static synthetic access$1702(Lspeech/patts/RomanNumeralContextWord;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/RomanNumeralContextWord;->allProper_:Z

    return p1
.end method

.method static synthetic access$300(Lspeech/patts/RomanNumeralContextWord;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/RomanNumeralContextWord;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/RomanNumeralContextWord;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/RomanNumeralContextWord;->hasSemclass:Z

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/RomanNumeralContextWord;Lspeech/patts/RomanNumeralContextWord$SemioticClass;)Lspeech/patts/RomanNumeralContextWord$SemioticClass;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/RomanNumeralContextWord;->semclass_:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    return-object p1
.end method

.method static synthetic access$602(Lspeech/patts/RomanNumeralContextWord;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/RomanNumeralContextWord;->hasMorphosyntacticFeatures:Z

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/RomanNumeralContextWord;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/RomanNumeralContextWord;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lspeech/patts/RomanNumeralContextWord;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/RomanNumeralContextWord;->hasMinDistanceBefore:Z

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/RomanNumeralContextWord;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/RomanNumeralContextWord;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/RomanNumeralContextWord;->minDistanceBefore_:I

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/RomanNumeralContextWord;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/RomanNumeralContextWord;->defaultInstance:Lspeech/patts/RomanNumeralContextWord;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->CARDINAL:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    iput-object v0, p0, Lspeech/patts/RomanNumeralContextWord;->semclass_:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    .line 134
    return-void
.end method

.method public static newBuilder()Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 1

    .prologue
    .line 283
    # invokes: Lspeech/patts/RomanNumeralContextWord$Builder;->create()Lspeech/patts/RomanNumeralContextWord$Builder;
    invoke-static {}, Lspeech/patts/RomanNumeralContextWord$Builder;->access$100()Lspeech/patts/RomanNumeralContextWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/RomanNumeralContextWord;)Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/RomanNumeralContextWord;

    .prologue
    .line 286
    invoke-static {}, Lspeech/patts/RomanNumeralContextWord;->newBuilder()Lspeech/patts/RomanNumeralContextWord$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/RomanNumeralContextWord$Builder;->mergeFrom(Lspeech/patts/RomanNumeralContextWord;)Lspeech/patts/RomanNumeralContextWord$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAllProper()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lspeech/patts/RomanNumeralContextWord;->allProper_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getDefaultInstanceForType()Lspeech/patts/RomanNumeralContextWord;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/RomanNumeralContextWord;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/RomanNumeralContextWord;->defaultInstance:Lspeech/patts/RomanNumeralContextWord;

    return-object v0
.end method

.method public getMaxDistanceAfter()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lspeech/patts/RomanNumeralContextWord;->maxDistanceAfter_:I

    return v0
.end method

.method public getMaxDistanceBefore()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lspeech/patts/RomanNumeralContextWord;->maxDistanceBefore_:I

    return v0
.end method

.method public getMinDistanceAfter()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lspeech/patts/RomanNumeralContextWord;->minDistanceAfter_:I

    return v0
.end method

.method public getMinDistanceBefore()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lspeech/patts/RomanNumeralContextWord;->minDistanceBefore_:I

    return v0
.end method

.method public getMorphosyntacticFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord;->morphosyntacticFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getSemclass()Lspeech/patts/RomanNumeralContextWord$SemioticClass;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord;->semclass_:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 171
    iget v3, p0, Lspeech/patts/RomanNumeralContextWord;->memoizedSerializedSize:I

    .line 172
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 213
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 174
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 176
    const/4 v0, 0x0

    .line 177
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getSpellingList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 178
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 180
    goto :goto_1

    .line 181
    .end local v1    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v3, v0

    .line 182
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getSpellingList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 184
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasSemclass()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 185
    const/4 v5, 0x2

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getSemclass()Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    move-result-object v6

    invoke-virtual {v6}, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->getNumber()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 188
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasMinDistanceBefore()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 189
    const/4 v5, 0x3

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getMinDistanceBefore()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 192
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasMaxDistanceBefore()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 193
    const/4 v5, 0x4

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getMaxDistanceBefore()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 196
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasMinDistanceAfter()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 197
    const/4 v5, 0x5

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getMinDistanceAfter()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 200
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasMaxDistanceAfter()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 201
    const/4 v5, 0x6

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getMaxDistanceAfter()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 204
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasMorphosyntacticFeatures()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 205
    const/4 v5, 0x7

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 208
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasAllProper()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 209
    const/16 v5, 0x8

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getAllProper()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 212
    :cond_8
    iput v3, p0, Lspeech/patts/RomanNumeralContextWord;->memoizedSerializedSize:I

    move v4, v3

    .line 213
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getSpellingList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;

    return-object v0
.end method

.method public hasAllProper()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lspeech/patts/RomanNumeralContextWord;->hasAllProper:Z

    return v0
.end method

.method public hasMaxDistanceAfter()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lspeech/patts/RomanNumeralContextWord;->hasMaxDistanceAfter:Z

    return v0
.end method

.method public hasMaxDistanceBefore()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lspeech/patts/RomanNumeralContextWord;->hasMaxDistanceBefore:Z

    return v0
.end method

.method public hasMinDistanceAfter()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lspeech/patts/RomanNumeralContextWord;->hasMinDistanceAfter:Z

    return v0
.end method

.method public hasMinDistanceBefore()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lspeech/patts/RomanNumeralContextWord;->hasMinDistanceBefore:Z

    return v0
.end method

.method public hasMorphosyntacticFeatures()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lspeech/patts/RomanNumeralContextWord;->hasMorphosyntacticFeatures:Z

    return v0
.end method

.method public hasSemclass()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lspeech/patts/RomanNumeralContextWord;->hasSemclass:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lspeech/patts/RomanNumeralContextWord;->hasSemclass:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 137
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->toBuilder()Lspeech/patts/RomanNumeralContextWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 1

    .prologue
    .line 288
    invoke-static {p0}, Lspeech/patts/RomanNumeralContextWord;->newBuilder(Lspeech/patts/RomanNumeralContextWord;)Lspeech/patts/RomanNumeralContextWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getSerializedSize()I

    .line 143
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getSpellingList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 144
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 146
    .end local v0    # "element":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasSemclass()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 147
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getSemclass()Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 149
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasMinDistanceBefore()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 150
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getMinDistanceBefore()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 152
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasMaxDistanceBefore()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 153
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getMaxDistanceBefore()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 155
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasMinDistanceAfter()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 156
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getMinDistanceAfter()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 158
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasMaxDistanceAfter()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 159
    const/4 v2, 0x6

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getMaxDistanceAfter()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 161
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasMorphosyntacticFeatures()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 162
    const/4 v2, 0x7

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 164
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->hasAllProper()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 165
    const/16 v2, 0x8

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord;->getAllProper()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 167
    :cond_7
    return-void
.end method
