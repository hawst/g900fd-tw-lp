.class public final Lspeech/patts/AttributeMapProto$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AttributeMapProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/AttributeMapProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/AttributeMapProto;",
        "Lspeech/patts/AttributeMapProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/AttributeMapProto;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 568
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1000()Lspeech/patts/AttributeMapProto$Builder;
    .locals 1

    .prologue
    .line 562
    invoke-static {}, Lspeech/patts/AttributeMapProto$Builder;->create()Lspeech/patts/AttributeMapProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/AttributeMapProto$Builder;
    .locals 3

    .prologue
    .line 571
    new-instance v0, Lspeech/patts/AttributeMapProto$Builder;

    invoke-direct {v0}, Lspeech/patts/AttributeMapProto$Builder;-><init>()V

    .line 572
    .local v0, "builder":Lspeech/patts/AttributeMapProto$Builder;
    new-instance v1, Lspeech/patts/AttributeMapProto;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/AttributeMapProto;-><init>(Lspeech/patts/AttributeMapProto$1;)V

    iput-object v1, v0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    .line 573
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 562
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Builder;->build()Lspeech/patts/AttributeMapProto;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/AttributeMapProto;
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 602
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    invoke-static {v0}, Lspeech/patts/AttributeMapProto$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 604
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Builder;->buildPartial()Lspeech/patts/AttributeMapProto;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/AttributeMapProto;
    .locals 3

    .prologue
    .line 617
    iget-object v1, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    if-nez v1, :cond_0

    .line 618
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 621
    :cond_0
    iget-object v1, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/AttributeMapProto;->access$1200(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 622
    iget-object v1, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    iget-object v2, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/AttributeMapProto;->access$1200(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/AttributeMapProto;->access$1202(Lspeech/patts/AttributeMapProto;Ljava/util/List;)Ljava/util/List;

    .line 625
    :cond_1
    iget-object v1, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/AttributeMapProto;->access$1300(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 626
    iget-object v1, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    iget-object v2, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/AttributeMapProto;->access$1300(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/AttributeMapProto;->access$1302(Lspeech/patts/AttributeMapProto;Ljava/util/List;)Ljava/util/List;

    .line 629
    :cond_2
    iget-object v1, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/AttributeMapProto;->access$1400(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 630
    iget-object v1, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    iget-object v2, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/AttributeMapProto;->access$1400(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/AttributeMapProto;->access$1402(Lspeech/patts/AttributeMapProto;Ljava/util/List;)Ljava/util/List;

    .line 633
    :cond_3
    iget-object v1, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/AttributeMapProto;->access$1500(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 634
    iget-object v1, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    iget-object v2, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/AttributeMapProto;->access$1500(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/AttributeMapProto;->access$1502(Lspeech/patts/AttributeMapProto;Ljava/util/List;)Ljava/util/List;

    .line 637
    :cond_4
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    .line 638
    .local v0, "returnMe":Lspeech/patts/AttributeMapProto;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    .line 639
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 562
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Builder;->clone()Lspeech/patts/AttributeMapProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 562
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Builder;->clone()Lspeech/patts/AttributeMapProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 562
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Builder;->clone()Lspeech/patts/AttributeMapProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/AttributeMapProto$Builder;
    .locals 2

    .prologue
    .line 590
    invoke-static {}, Lspeech/patts/AttributeMapProto$Builder;->create()Lspeech/patts/AttributeMapProto$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    invoke-virtual {v0, v1}, Lspeech/patts/AttributeMapProto$Builder;->mergeFrom(Lspeech/patts/AttributeMapProto;)Lspeech/patts/AttributeMapProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    invoke-virtual {v0}, Lspeech/patts/AttributeMapProto;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 562
    check-cast p1, Lspeech/patts/AttributeMapProto;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/AttributeMapProto$Builder;->mergeFrom(Lspeech/patts/AttributeMapProto;)Lspeech/patts/AttributeMapProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/AttributeMapProto;)Lspeech/patts/AttributeMapProto$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/AttributeMapProto;

    .prologue
    .line 643
    invoke-static {}, Lspeech/patts/AttributeMapProto;->getDefaultInstance()Lspeech/patts/AttributeMapProto;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 671
    :cond_0
    :goto_0
    return-object p0

    .line 644
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/AttributeMapProto;->hasDefaultFeatures()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 645
    invoke-virtual {p1}, Lspeech/patts/AttributeMapProto;->getDefaultFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/AttributeMapProto$Builder;->setDefaultFeatures(Ljava/lang/String;)Lspeech/patts/AttributeMapProto$Builder;

    .line 647
    :cond_2
    # getter for: Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/AttributeMapProto;->access$1200(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 648
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/AttributeMapProto;->access$1200(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 649
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/AttributeMapProto;->access$1202(Lspeech/patts/AttributeMapProto;Ljava/util/List;)Ljava/util/List;

    .line 651
    :cond_3
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/AttributeMapProto;->access$1200(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/AttributeMapProto;->access$1200(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 653
    :cond_4
    # getter for: Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/AttributeMapProto;->access$1300(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 654
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/AttributeMapProto;->access$1300(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 655
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/AttributeMapProto;->access$1302(Lspeech/patts/AttributeMapProto;Ljava/util/List;)Ljava/util/List;

    .line 657
    :cond_5
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/AttributeMapProto;->access$1300(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/AttributeMapProto;->access$1300(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 659
    :cond_6
    # getter for: Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/AttributeMapProto;->access$1400(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 660
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/AttributeMapProto;->access$1400(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 661
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/AttributeMapProto;->access$1402(Lspeech/patts/AttributeMapProto;Ljava/util/List;)Ljava/util/List;

    .line 663
    :cond_7
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/AttributeMapProto;->access$1400(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/AttributeMapProto;->access$1400(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 665
    :cond_8
    # getter for: Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/AttributeMapProto;->access$1500(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 666
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/AttributeMapProto;->access$1500(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 667
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/AttributeMapProto;->access$1502(Lspeech/patts/AttributeMapProto;Ljava/util/List;)Ljava/util/List;

    .line 669
    :cond_9
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # getter for: Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/AttributeMapProto;->access$1500(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/AttributeMapProto;->access$1500(Lspeech/patts/AttributeMapProto;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setDefaultFeatures(Ljava/lang/String;)Lspeech/patts/AttributeMapProto$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 730
    if-nez p1, :cond_0

    .line 731
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 733
    :cond_0
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/AttributeMapProto;->hasDefaultFeatures:Z
    invoke-static {v0, v1}, Lspeech/patts/AttributeMapProto;->access$1602(Lspeech/patts/AttributeMapProto;Z)Z

    .line 734
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Builder;->result:Lspeech/patts/AttributeMapProto;

    # setter for: Lspeech/patts/AttributeMapProto;->defaultFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/AttributeMapProto;->access$1702(Lspeech/patts/AttributeMapProto;Ljava/lang/String;)Ljava/lang/String;

    .line 735
    return-object p0
.end method
