.class public final Lspeech/patts/PronWord$Component$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PronWord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PronWord$Component;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/PronWord$Component;",
        "Lspeech/patts/PronWord$Component$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/PronWord$Component;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/PronWord$Component$Builder;
    .locals 1

    .prologue
    .line 176
    invoke-static {}, Lspeech/patts/PronWord$Component$Builder;->create()Lspeech/patts/PronWord$Component$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/PronWord$Component$Builder;
    .locals 3

    .prologue
    .line 185
    new-instance v0, Lspeech/patts/PronWord$Component$Builder;

    invoke-direct {v0}, Lspeech/patts/PronWord$Component$Builder;-><init>()V

    .line 186
    .local v0, "builder":Lspeech/patts/PronWord$Component$Builder;
    new-instance v1, Lspeech/patts/PronWord$Component;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/PronWord$Component;-><init>(Lspeech/patts/PronWord$1;)V

    iput-object v1, v0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    .line 187
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component$Builder;->build()Lspeech/patts/PronWord$Component;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/PronWord$Component;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/PronWord$Component$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 216
    iget-object v0, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    invoke-static {v0}, Lspeech/patts/PronWord$Component$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 218
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component$Builder;->buildPartial()Lspeech/patts/PronWord$Component;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/PronWord$Component;
    .locals 3

    .prologue
    .line 231
    iget-object v1, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    if-nez v1, :cond_0

    .line 232
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 235
    :cond_0
    iget-object v0, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    .line 236
    .local v0, "returnMe":Lspeech/patts/PronWord$Component;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    .line 237
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component$Builder;->clone()Lspeech/patts/PronWord$Component$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component$Builder;->clone()Lspeech/patts/PronWord$Component$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 176
    invoke-virtual {p0}, Lspeech/patts/PronWord$Component$Builder;->clone()Lspeech/patts/PronWord$Component$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/PronWord$Component$Builder;
    .locals 2

    .prologue
    .line 204
    invoke-static {}, Lspeech/patts/PronWord$Component$Builder;->create()Lspeech/patts/PronWord$Component$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    invoke-virtual {v0, v1}, Lspeech/patts/PronWord$Component$Builder;->mergeFrom(Lspeech/patts/PronWord$Component;)Lspeech/patts/PronWord$Component$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    invoke-virtual {v0}, Lspeech/patts/PronWord$Component;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 176
    check-cast p1, Lspeech/patts/PronWord$Component;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/PronWord$Component$Builder;->mergeFrom(Lspeech/patts/PronWord$Component;)Lspeech/patts/PronWord$Component$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/PronWord$Component;)Lspeech/patts/PronWord$Component$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/PronWord$Component;

    .prologue
    .line 241
    invoke-static {}, Lspeech/patts/PronWord$Component;->getDefaultInstance()Lspeech/patts/PronWord$Component;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-object p0

    .line 242
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/PronWord$Component;->hasRule()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 243
    invoke-virtual {p1}, Lspeech/patts/PronWord$Component;->getRule()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PronWord$Component$Builder;->setRule(Ljava/lang/String;)Lspeech/patts/PronWord$Component$Builder;

    .line 245
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/PronWord$Component;->hasId()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 246
    invoke-virtual {p1}, Lspeech/patts/PronWord$Component;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PronWord$Component$Builder;->setId(Ljava/lang/String;)Lspeech/patts/PronWord$Component$Builder;

    .line 248
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/PronWord$Component;->hasPhonemes()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    invoke-virtual {p1}, Lspeech/patts/PronWord$Component;->getPhonemes()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PronWord$Component$Builder;->setPhonemes(Ljava/lang/String;)Lspeech/patts/PronWord$Component$Builder;

    goto :goto_0
.end method

.method public setId(Ljava/lang/String;)Lspeech/patts/PronWord$Component$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 315
    if-nez p1, :cond_0

    .line 316
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 318
    :cond_0
    iget-object v0, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PronWord$Component;->hasId:Z
    invoke-static {v0, v1}, Lspeech/patts/PronWord$Component;->access$502(Lspeech/patts/PronWord$Component;Z)Z

    .line 319
    iget-object v0, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    # setter for: Lspeech/patts/PronWord$Component;->id_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PronWord$Component;->access$602(Lspeech/patts/PronWord$Component;Ljava/lang/String;)Ljava/lang/String;

    .line 320
    return-object p0
.end method

.method public setPhonemes(Ljava/lang/String;)Lspeech/patts/PronWord$Component$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 336
    if-nez p1, :cond_0

    .line 337
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 339
    :cond_0
    iget-object v0, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PronWord$Component;->hasPhonemes:Z
    invoke-static {v0, v1}, Lspeech/patts/PronWord$Component;->access$702(Lspeech/patts/PronWord$Component;Z)Z

    .line 340
    iget-object v0, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    # setter for: Lspeech/patts/PronWord$Component;->phonemes_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PronWord$Component;->access$802(Lspeech/patts/PronWord$Component;Ljava/lang/String;)Ljava/lang/String;

    .line 341
    return-object p0
.end method

.method public setRule(Ljava/lang/String;)Lspeech/patts/PronWord$Component$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 294
    if-nez p1, :cond_0

    .line 295
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 297
    :cond_0
    iget-object v0, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PronWord$Component;->hasRule:Z
    invoke-static {v0, v1}, Lspeech/patts/PronWord$Component;->access$302(Lspeech/patts/PronWord$Component;Z)Z

    .line 298
    iget-object v0, p0, Lspeech/patts/PronWord$Component$Builder;->result:Lspeech/patts/PronWord$Component;

    # setter for: Lspeech/patts/PronWord$Component;->rule_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PronWord$Component;->access$402(Lspeech/patts/PronWord$Component;Ljava/lang/String;)Ljava/lang/String;

    .line 299
    return-object p0
.end method
