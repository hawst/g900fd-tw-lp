.class public final Lspeech/patts/CostFunction;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "CostFunction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/CostFunction$1;,
        Lspeech/patts/CostFunction$Builder;,
        Lspeech/patts/CostFunction$ValueType;,
        Lspeech/patts/CostFunction$CostFunctionType;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/CostFunction;


# instance fields
.field private acoustic_:Lspeech/patts/AcousticFeature;

.field private ctype_:Lspeech/patts/CostFunction$CostFunctionType;

.field private ftype_:Lspeech/patts/CostFunction$ValueType;

.field private hasAcoustic:Z

.field private hasCtype:Z

.field private hasFtype:Z

.field private hasLinguistic:Z

.field private hasQuestion:Z

.field private linguistic_:Lspeech/patts/NavigationFeature;

.field private memoizedSerializedSize:I

.field private question_:Lspeech/patts/Question;

.field private weight_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 670
    new-instance v0, Lspeech/patts/CostFunction;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/CostFunction;-><init>(Z)V

    sput-object v0, Lspeech/patts/CostFunction;->defaultInstance:Lspeech/patts/CostFunction;

    .line 671
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 672
    sget-object v0, Lspeech/patts/CostFunction;->defaultInstance:Lspeech/patts/CostFunction;

    invoke-direct {v0}, Lspeech/patts/CostFunction;->initFields()V

    .line 673
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 112
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunction;->weight_:Ljava/util/List;

    .line 199
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/CostFunction;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/CostFunction;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/CostFunction$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/CostFunction$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/CostFunction;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 112
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunction;->weight_:Ljava/util/List;

    .line 199
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/CostFunction;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/CostFunction;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/CostFunction;->hasAcoustic:Z

    return p1
.end method

.method static synthetic access$1100(Lspeech/patts/CostFunction;)Lspeech/patts/AcousticFeature;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunction;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunction;->acoustic_:Lspeech/patts/AcousticFeature;

    return-object v0
.end method

.method static synthetic access$1102(Lspeech/patts/CostFunction;Lspeech/patts/AcousticFeature;)Lspeech/patts/AcousticFeature;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunction;
    .param p1, "x1"    # Lspeech/patts/AcousticFeature;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunction;->acoustic_:Lspeech/patts/AcousticFeature;

    return-object p1
.end method

.method static synthetic access$1202(Lspeech/patts/CostFunction;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/CostFunction;->hasQuestion:Z

    return p1
.end method

.method static synthetic access$1300(Lspeech/patts/CostFunction;)Lspeech/patts/Question;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunction;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunction;->question_:Lspeech/patts/Question;

    return-object v0
.end method

.method static synthetic access$1302(Lspeech/patts/CostFunction;Lspeech/patts/Question;)Lspeech/patts/Question;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunction;
    .param p1, "x1"    # Lspeech/patts/Question;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunction;->question_:Lspeech/patts/Question;

    return-object p1
.end method

.method static synthetic access$300(Lspeech/patts/CostFunction;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunction;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunction;->weight_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/CostFunction;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunction;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunction;->weight_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/CostFunction;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/CostFunction;->hasCtype:Z

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/CostFunction;Lspeech/patts/CostFunction$CostFunctionType;)Lspeech/patts/CostFunction$CostFunctionType;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunction;
    .param p1, "x1"    # Lspeech/patts/CostFunction$CostFunctionType;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunction;->ctype_:Lspeech/patts/CostFunction$CostFunctionType;

    return-object p1
.end method

.method static synthetic access$602(Lspeech/patts/CostFunction;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/CostFunction;->hasFtype:Z

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/CostFunction;Lspeech/patts/CostFunction$ValueType;)Lspeech/patts/CostFunction$ValueType;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunction;
    .param p1, "x1"    # Lspeech/patts/CostFunction$ValueType;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunction;->ftype_:Lspeech/patts/CostFunction$ValueType;

    return-object p1
.end method

.method static synthetic access$802(Lspeech/patts/CostFunction;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunction;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/CostFunction;->hasLinguistic:Z

    return p1
.end method

.method static synthetic access$900(Lspeech/patts/CostFunction;)Lspeech/patts/NavigationFeature;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunction;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunction;->linguistic_:Lspeech/patts/NavigationFeature;

    return-object v0
.end method

.method static synthetic access$902(Lspeech/patts/CostFunction;Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunction;
    .param p1, "x1"    # Lspeech/patts/NavigationFeature;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunction;->linguistic_:Lspeech/patts/NavigationFeature;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/CostFunction;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/CostFunction;->defaultInstance:Lspeech/patts/CostFunction;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lspeech/patts/CostFunction$CostFunctionType;->TABLE:Lspeech/patts/CostFunction$CostFunctionType;

    iput-object v0, p0, Lspeech/patts/CostFunction;->ctype_:Lspeech/patts/CostFunction$CostFunctionType;

    .line 159
    sget-object v0, Lspeech/patts/CostFunction$ValueType;->BOOL:Lspeech/patts/CostFunction$ValueType;

    iput-object v0, p0, Lspeech/patts/CostFunction;->ftype_:Lspeech/patts/CostFunction$ValueType;

    .line 160
    invoke-static {}, Lspeech/patts/NavigationFeature;->getDefaultInstance()Lspeech/patts/NavigationFeature;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunction;->linguistic_:Lspeech/patts/NavigationFeature;

    .line 161
    invoke-static {}, Lspeech/patts/AcousticFeature;->getDefaultInstance()Lspeech/patts/AcousticFeature;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunction;->acoustic_:Lspeech/patts/AcousticFeature;

    .line 162
    invoke-static {}, Lspeech/patts/Question;->getDefaultInstance()Lspeech/patts/Question;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunction;->question_:Lspeech/patts/Question;

    .line 163
    return-void
.end method

.method public static newBuilder()Lspeech/patts/CostFunction$Builder;
    .locals 1

    .prologue
    .line 302
    # invokes: Lspeech/patts/CostFunction$Builder;->create()Lspeech/patts/CostFunction$Builder;
    invoke-static {}, Lspeech/patts/CostFunction$Builder;->access$100()Lspeech/patts/CostFunction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/CostFunction;)Lspeech/patts/CostFunction$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/CostFunction;

    .prologue
    .line 305
    invoke-static {}, Lspeech/patts/CostFunction;->newBuilder()Lspeech/patts/CostFunction$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/CostFunction$Builder;->mergeFrom(Lspeech/patts/CostFunction;)Lspeech/patts/CostFunction$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAcoustic()Lspeech/patts/AcousticFeature;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lspeech/patts/CostFunction;->acoustic_:Lspeech/patts/AcousticFeature;

    return-object v0
.end method

.method public getCtype()Lspeech/patts/CostFunction$CostFunctionType;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lspeech/patts/CostFunction;->ctype_:Lspeech/patts/CostFunction$CostFunctionType;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getDefaultInstanceForType()Lspeech/patts/CostFunction;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/CostFunction;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/CostFunction;->defaultInstance:Lspeech/patts/CostFunction;

    return-object v0
.end method

.method public getFtype()Lspeech/patts/CostFunction$ValueType;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lspeech/patts/CostFunction;->ftype_:Lspeech/patts/CostFunction$ValueType;

    return-object v0
.end method

.method public getLinguistic()Lspeech/patts/NavigationFeature;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lspeech/patts/CostFunction;->linguistic_:Lspeech/patts/NavigationFeature;

    return-object v0
.end method

.method public getQuestion()Lspeech/patts/Question;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lspeech/patts/CostFunction;->question_:Lspeech/patts/Question;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 201
    iget v1, p0, Lspeech/patts/CostFunction;->memoizedSerializedSize:I

    .line 202
    .local v1, "size":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 232
    .end local v1    # "size":I
    .local v2, "size":I
    :goto_0
    return v2

    .line 204
    .end local v2    # "size":I
    .restart local v1    # "size":I
    :cond_0
    const/4 v1, 0x0

    .line 206
    const/4 v0, 0x0

    .line 207
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getWeightList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 208
    add-int/2addr v1, v0

    .line 209
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getWeightList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v1, v3

    .line 211
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->hasFtype()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 212
    const/4 v3, 0x2

    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getFtype()Lspeech/patts/CostFunction$ValueType;

    move-result-object v4

    invoke-virtual {v4}, Lspeech/patts/CostFunction$ValueType;->getNumber()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 215
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->hasLinguistic()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 216
    const/4 v3, 0x3

    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getLinguistic()Lspeech/patts/NavigationFeature;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 219
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->hasAcoustic()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 220
    const/4 v3, 0x4

    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getAcoustic()Lspeech/patts/AcousticFeature;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 223
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->hasQuestion()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 224
    const/4 v3, 0x5

    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getQuestion()Lspeech/patts/Question;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 227
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->hasCtype()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 228
    const/4 v3, 0x6

    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getCtype()Lspeech/patts/CostFunction$CostFunctionType;

    move-result-object v4

    invoke-virtual {v4}, Lspeech/patts/CostFunction$CostFunctionType;->getNumber()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 231
    :cond_5
    iput v1, p0, Lspeech/patts/CostFunction;->memoizedSerializedSize:I

    move v2, v1

    .line 232
    .end local v1    # "size":I
    .restart local v2    # "size":I
    goto :goto_0
.end method

.method public getWeightList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lspeech/patts/CostFunction;->weight_:Ljava/util/List;

    return-object v0
.end method

.method public hasAcoustic()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lspeech/patts/CostFunction;->hasAcoustic:Z

    return v0
.end method

.method public hasCtype()Z
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lspeech/patts/CostFunction;->hasCtype:Z

    return v0
.end method

.method public hasFtype()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lspeech/patts/CostFunction;->hasFtype:Z

    return v0
.end method

.method public hasLinguistic()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lspeech/patts/CostFunction;->hasLinguistic:Z

    return v0
.end method

.method public hasQuestion()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lspeech/patts/CostFunction;->hasQuestion:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 165
    iget-boolean v1, p0, Lspeech/patts/CostFunction;->hasCtype:Z

    if-nez v1, :cond_1

    .line 173
    :cond_0
    :goto_0
    return v0

    .line 166
    :cond_1
    iget-boolean v1, p0, Lspeech/patts/CostFunction;->hasFtype:Z

    if-eqz v1, :cond_0

    .line 167
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->hasLinguistic()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 168
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getLinguistic()Lspeech/patts/NavigationFeature;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/NavigationFeature;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->hasQuestion()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 171
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getQuestion()Lspeech/patts/Question;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/Question;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->toBuilder()Lspeech/patts/CostFunction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/CostFunction$Builder;
    .locals 1

    .prologue
    .line 307
    invoke-static {p0}, Lspeech/patts/CostFunction;->newBuilder(Lspeech/patts/CostFunction;)Lspeech/patts/CostFunction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getSerializedSize()I

    .line 179
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getWeightList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 180
    .local v0, "element":F
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    goto :goto_0

    .line 182
    .end local v0    # "element":F
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->hasFtype()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 183
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getFtype()Lspeech/patts/CostFunction$ValueType;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/CostFunction$ValueType;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 185
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->hasLinguistic()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 186
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getLinguistic()Lspeech/patts/NavigationFeature;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 188
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->hasAcoustic()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 189
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getAcoustic()Lspeech/patts/AcousticFeature;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 191
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->hasQuestion()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 192
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getQuestion()Lspeech/patts/Question;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 194
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/CostFunction;->hasCtype()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 195
    const/4 v2, 0x6

    invoke-virtual {p0}, Lspeech/patts/CostFunction;->getCtype()Lspeech/patts/CostFunction$CostFunctionType;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/CostFunction$CostFunctionType;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 197
    :cond_5
    return-void
.end method
