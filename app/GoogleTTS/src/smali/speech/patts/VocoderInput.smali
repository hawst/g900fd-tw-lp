.class public final Lspeech/patts/VocoderInput;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "VocoderInput.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/VocoderInput$1;,
        Lspeech/patts/VocoderInput$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/VocoderInput;


# instance fields
.field private amplitudesMemoizedSerializedSize:I

.field private amplitudes_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private aperiodicitiesMemoizedSerializedSize:I

.field private aperiodicities_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private bandAperiodicitiesMemoizedSerializedSize:I

.field private bandAperiodicities_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private hasLogF0:Z

.field private hasVoiced:Z

.field private lineSpectralPairsMemoizedSerializedSize:I

.field private lineSpectralPairs_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private logF0_:F

.field private melCepstraMemoizedSerializedSize:I

.field private melCepstra_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private voiced_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 728
    new-instance v0, Lspeech/patts/VocoderInput;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/VocoderInput;-><init>(Z)V

    sput-object v0, Lspeech/patts/VocoderInput;->defaultInstance:Lspeech/patts/VocoderInput;

    .line 729
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 730
    sget-object v0, Lspeech/patts/VocoderInput;->defaultInstance:Lspeech/patts/VocoderInput;

    invoke-direct {v0}, Lspeech/patts/VocoderInput;->initFields()V

    .line 731
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lspeech/patts/VocoderInput;->voiced_:Z

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/VocoderInput;->logF0_:F

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;

    .line 47
    iput v1, p0, Lspeech/patts/VocoderInput;->amplitudesMemoizedSerializedSize:I

    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;

    .line 60
    iput v1, p0, Lspeech/patts/VocoderInput;->aperiodicitiesMemoizedSerializedSize:I

    .line 64
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;

    .line 73
    iput v1, p0, Lspeech/patts/VocoderInput;->melCepstraMemoizedSerializedSize:I

    .line 77
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;

    .line 86
    iput v1, p0, Lspeech/patts/VocoderInput;->lineSpectralPairsMemoizedSerializedSize:I

    .line 90
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;

    .line 99
    iput v1, p0, Lspeech/patts/VocoderInput;->bandAperiodicitiesMemoizedSerializedSize:I

    .line 153
    iput v1, p0, Lspeech/patts/VocoderInput;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/VocoderInput;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/VocoderInput$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/VocoderInput$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/VocoderInput;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, -0x1

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lspeech/patts/VocoderInput;->voiced_:Z

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/VocoderInput;->logF0_:F

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;

    .line 47
    iput v1, p0, Lspeech/patts/VocoderInput;->amplitudesMemoizedSerializedSize:I

    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;

    .line 60
    iput v1, p0, Lspeech/patts/VocoderInput;->aperiodicitiesMemoizedSerializedSize:I

    .line 64
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;

    .line 73
    iput v1, p0, Lspeech/patts/VocoderInput;->melCepstraMemoizedSerializedSize:I

    .line 77
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;

    .line 86
    iput v1, p0, Lspeech/patts/VocoderInput;->lineSpectralPairsMemoizedSerializedSize:I

    .line 90
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;

    .line 99
    iput v1, p0, Lspeech/patts/VocoderInput;->bandAperiodicitiesMemoizedSerializedSize:I

    .line 153
    iput v1, p0, Lspeech/patts/VocoderInput;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/VocoderInput;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/VocoderInput;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/VocoderInput;->hasLogF0:Z

    return p1
.end method

.method static synthetic access$1102(Lspeech/patts/VocoderInput;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/VocoderInput;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/VocoderInput;->logF0_:F

    return p1
.end method

.method static synthetic access$300(Lspeech/patts/VocoderInput;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/VocoderInput;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/VocoderInput;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lspeech/patts/VocoderInput;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/VocoderInput;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/VocoderInput;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lspeech/patts/VocoderInput;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/VocoderInput;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/VocoderInput;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lspeech/patts/VocoderInput;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/VocoderInput;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/VocoderInput;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lspeech/patts/VocoderInput;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/VocoderInput;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/VocoderInput;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$802(Lspeech/patts/VocoderInput;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/VocoderInput;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/VocoderInput;->hasVoiced:Z

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/VocoderInput;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/VocoderInput;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/VocoderInput;->voiced_:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/VocoderInput;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/VocoderInput;->defaultInstance:Lspeech/patts/VocoderInput;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public static newBuilder()Lspeech/patts/VocoderInput$Builder;
    .locals 1

    .prologue
    .line 293
    # invokes: Lspeech/patts/VocoderInput$Builder;->create()Lspeech/patts/VocoderInput$Builder;
    invoke-static {}, Lspeech/patts/VocoderInput$Builder;->access$100()Lspeech/patts/VocoderInput$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/VocoderInput;)Lspeech/patts/VocoderInput$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/VocoderInput;

    .prologue
    .line 296
    invoke-static {}, Lspeech/patts/VocoderInput;->newBuilder()Lspeech/patts/VocoderInput$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/VocoderInput$Builder;->mergeFrom(Lspeech/patts/VocoderInput;)Lspeech/patts/VocoderInput$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAmplitudesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;

    return-object v0
.end method

.method public getAperiodicitiesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;

    return-object v0
.end method

.method public getBandAperiodicitiesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getDefaultInstanceForType()Lspeech/patts/VocoderInput;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/VocoderInput;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/VocoderInput;->defaultInstance:Lspeech/patts/VocoderInput;

    return-object v0
.end method

.method public getLineSpectralPairsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;

    return-object v0
.end method

.method public getLogF0()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lspeech/patts/VocoderInput;->logF0_:F

    return v0
.end method

.method public getMelCepstraList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 155
    iget v1, p0, Lspeech/patts/VocoderInput;->memoizedSerializedSize:I

    .line 156
    .local v1, "size":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 223
    .end local v1    # "size":I
    .local v2, "size":I
    :goto_0
    return v2

    .line 158
    .end local v2    # "size":I
    .restart local v1    # "size":I
    :cond_0
    const/4 v1, 0x0

    .line 159
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->hasVoiced()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 160
    const/4 v3, 0x1

    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getVoiced()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v1, v3

    .line 163
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->hasLogF0()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 164
    const/4 v3, 0x2

    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getLogF0()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v1, v3

    .line 168
    :cond_2
    const/4 v0, 0x0

    .line 169
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getMelCepstraList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 170
    add-int/2addr v1, v0

    .line 171
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getMelCepstraList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 172
    add-int/lit8 v1, v1, 0x1

    .line 173
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 176
    :cond_3
    iput v0, p0, Lspeech/patts/VocoderInput;->melCepstraMemoizedSerializedSize:I

    .line 179
    const/4 v0, 0x0

    .line 180
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getLineSpectralPairsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 181
    add-int/2addr v1, v0

    .line 182
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getLineSpectralPairsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 183
    add-int/lit8 v1, v1, 0x1

    .line 184
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 187
    :cond_4
    iput v0, p0, Lspeech/patts/VocoderInput;->lineSpectralPairsMemoizedSerializedSize:I

    .line 190
    const/4 v0, 0x0

    .line 191
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getBandAperiodicitiesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 192
    add-int/2addr v1, v0

    .line 193
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getBandAperiodicitiesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 194
    add-int/lit8 v1, v1, 0x1

    .line 195
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 198
    :cond_5
    iput v0, p0, Lspeech/patts/VocoderInput;->bandAperiodicitiesMemoizedSerializedSize:I

    .line 201
    const/4 v0, 0x0

    .line 202
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getAmplitudesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 203
    add-int/2addr v1, v0

    .line 204
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getAmplitudesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 205
    add-int/lit8 v1, v1, 0x1

    .line 206
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 209
    :cond_6
    iput v0, p0, Lspeech/patts/VocoderInput;->amplitudesMemoizedSerializedSize:I

    .line 212
    const/4 v0, 0x0

    .line 213
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getAperiodicitiesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 214
    add-int/2addr v1, v0

    .line 215
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getAperiodicitiesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 216
    add-int/lit8 v1, v1, 0x1

    .line 217
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 220
    :cond_7
    iput v0, p0, Lspeech/patts/VocoderInput;->aperiodicitiesMemoizedSerializedSize:I

    .line 222
    iput v1, p0, Lspeech/patts/VocoderInput;->memoizedSerializedSize:I

    move v2, v1

    .line 223
    .end local v1    # "size":I
    .restart local v2    # "size":I
    goto/16 :goto_0
.end method

.method public getVoiced()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lspeech/patts/VocoderInput;->voiced_:Z

    return v0
.end method

.method public hasLogF0()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/VocoderInput;->hasLogF0:Z

    return v0
.end method

.method public hasVoiced()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/VocoderInput;->hasVoiced:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->toBuilder()Lspeech/patts/VocoderInput$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/VocoderInput$Builder;
    .locals 1

    .prologue
    .line 298
    invoke-static {p0}, Lspeech/patts/VocoderInput;->newBuilder(Lspeech/patts/VocoderInput;)Lspeech/patts/VocoderInput$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getSerializedSize()I

    .line 110
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->hasVoiced()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getVoiced()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 113
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->hasLogF0()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getLogF0()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 116
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getMelCepstraList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 117
    const/16 v2, 0x1a

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 118
    iget v2, p0, Lspeech/patts/VocoderInput;->melCepstraMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 120
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getMelCepstraList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 121
    .local v0, "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_0

    .line 123
    .end local v0    # "element":F
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getLineSpectralPairsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 124
    const/16 v2, 0x22

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 125
    iget v2, p0, Lspeech/patts/VocoderInput;->lineSpectralPairsMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 127
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getLineSpectralPairsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 128
    .restart local v0    # "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_1

    .line 130
    .end local v0    # "element":F
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getBandAperiodicitiesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_6

    .line 131
    const/16 v2, 0x2a

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 132
    iget v2, p0, Lspeech/patts/VocoderInput;->bandAperiodicitiesMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 134
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getBandAperiodicitiesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 135
    .restart local v0    # "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_2

    .line 137
    .end local v0    # "element":F
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getAmplitudesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_8

    .line 138
    const/16 v2, 0x32

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 139
    iget v2, p0, Lspeech/patts/VocoderInput;->amplitudesMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 141
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getAmplitudesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 142
    .restart local v0    # "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_3

    .line 144
    .end local v0    # "element":F
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getAperiodicitiesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_a

    .line 145
    const/16 v2, 0x3a

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 146
    iget v2, p0, Lspeech/patts/VocoderInput;->aperiodicitiesMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 148
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/VocoderInput;->getAperiodicitiesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 149
    .restart local v0    # "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_4

    .line 151
    .end local v0    # "element":F
    :cond_b
    return-void
.end method
