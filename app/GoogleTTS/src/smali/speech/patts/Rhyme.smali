.class public final Lspeech/patts/Rhyme;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Rhyme.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/Rhyme$1;,
        Lspeech/patts/Rhyme$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/Rhyme;


# instance fields
.field private coda_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasIgnoreSyllabification:Z

.field private hasVowel:Z

.field private ignoreSyllabification_:Z

.field private memoizedSerializedSize:I

.field private vowel_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 370
    new-instance v0, Lspeech/patts/Rhyme;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/Rhyme;-><init>(Z)V

    sput-object v0, Lspeech/patts/Rhyme;->defaultInstance:Lspeech/patts/Rhyme;

    .line 371
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 372
    sget-object v0, Lspeech/patts/Rhyme;->defaultInstance:Lspeech/patts/Rhyme;

    invoke-direct {v0}, Lspeech/patts/Rhyme;->initFields()V

    .line 373
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Rhyme;->vowel_:Ljava/lang/String;

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Rhyme;->coda_:Ljava/util/List;

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/patts/Rhyme;->ignoreSyllabification_:Z

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Rhyme;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/Rhyme;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/Rhyme$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/Rhyme$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/Rhyme;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Rhyme;->vowel_:Ljava/lang/String;

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Rhyme;->coda_:Ljava/util/List;

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/patts/Rhyme;->ignoreSyllabification_:Z

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Rhyme;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/Rhyme;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Rhyme;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Rhyme;->coda_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/Rhyme;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Rhyme;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Rhyme;->coda_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/Rhyme;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Rhyme;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Rhyme;->hasVowel:Z

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/Rhyme;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Rhyme;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Rhyme;->vowel_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lspeech/patts/Rhyme;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Rhyme;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Rhyme;->hasIgnoreSyllabification:Z

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/Rhyme;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Rhyme;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Rhyme;->ignoreSyllabification_:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/Rhyme;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/Rhyme;->defaultInstance:Lspeech/patts/Rhyme;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public static newBuilder()Lspeech/patts/Rhyme$Builder;
    .locals 1

    .prologue
    .line 163
    # invokes: Lspeech/patts/Rhyme$Builder;->create()Lspeech/patts/Rhyme$Builder;
    invoke-static {}, Lspeech/patts/Rhyme$Builder;->access$100()Lspeech/patts/Rhyme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/Rhyme;)Lspeech/patts/Rhyme$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/Rhyme;

    .prologue
    .line 166
    invoke-static {}, Lspeech/patts/Rhyme;->newBuilder()Lspeech/patts/Rhyme$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/Rhyme$Builder;->mergeFrom(Lspeech/patts/Rhyme;)Lspeech/patts/Rhyme$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCodaList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lspeech/patts/Rhyme;->coda_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Rhyme;->getDefaultInstanceForType()Lspeech/patts/Rhyme;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/Rhyme;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/Rhyme;->defaultInstance:Lspeech/patts/Rhyme;

    return-object v0
.end method

.method public getIgnoreSyllabification()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lspeech/patts/Rhyme;->ignoreSyllabification_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 71
    iget v3, p0, Lspeech/patts/Rhyme;->memoizedSerializedSize:I

    .line 72
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 93
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 74
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 75
    invoke-virtual {p0}, Lspeech/patts/Rhyme;->hasVowel()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 76
    const/4 v5, 0x1

    invoke-virtual {p0}, Lspeech/patts/Rhyme;->getVowel()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 80
    :cond_1
    const/4 v0, 0x0

    .line 81
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/Rhyme;->getCodaList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 82
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 84
    goto :goto_1

    .line 85
    .end local v1    # "element":Ljava/lang/String;
    :cond_2
    add-int/2addr v3, v0

    .line 86
    invoke-virtual {p0}, Lspeech/patts/Rhyme;->getCodaList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 88
    invoke-virtual {p0}, Lspeech/patts/Rhyme;->hasIgnoreSyllabification()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 89
    const/4 v5, 0x3

    invoke-virtual {p0}, Lspeech/patts/Rhyme;->getIgnoreSyllabification()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 92
    :cond_3
    iput v3, p0, Lspeech/patts/Rhyme;->memoizedSerializedSize:I

    move v4, v3

    .line 93
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto :goto_0
.end method

.method public getVowel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/Rhyme;->vowel_:Ljava/lang/String;

    return-object v0
.end method

.method public hasIgnoreSyllabification()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lspeech/patts/Rhyme;->hasIgnoreSyllabification:Z

    return v0
.end method

.method public hasVowel()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/Rhyme;->hasVowel:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lspeech/patts/Rhyme;->hasVowel:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 52
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Rhyme;->toBuilder()Lspeech/patts/Rhyme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/Rhyme$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-static {p0}, Lspeech/patts/Rhyme;->newBuilder(Lspeech/patts/Rhyme;)Lspeech/patts/Rhyme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p0}, Lspeech/patts/Rhyme;->getSerializedSize()I

    .line 58
    invoke-virtual {p0}, Lspeech/patts/Rhyme;->hasVowel()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/Rhyme;->getVowel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 61
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Rhyme;->getCodaList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 64
    .end local v0    # "element":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Rhyme;->hasIgnoreSyllabification()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 65
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/Rhyme;->getIgnoreSyllabification()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 67
    :cond_2
    return-void
.end method
