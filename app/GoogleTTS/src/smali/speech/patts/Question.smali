.class public final Lspeech/patts/Question;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Question.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/Question$1;,
        Lspeech/patts/Question$Builder;,
        Lspeech/patts/Question$Conjunction;,
        Lspeech/patts/Question$Comparison;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/Question;


# instance fields
.field private bval_:Z

.field private comp_:Lspeech/patts/Question$Comparison;

.field private conj_:Lspeech/patts/Question$Conjunction;

.field private fval_:F

.field private hasBval:Z

.field private hasComp:Z

.field private hasConj:Z

.field private hasFval:Z

.field private hasIval:Z

.field private hasNf:Z

.field private hasRhs:Z

.field private hasSval:Z

.field private ival_:I

.field private memoizedSerializedSize:I

.field private nf_:Lspeech/patts/NavigationFeature;

.field private q_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Question;",
            ">;"
        }
    .end annotation
.end field

.field private rhs_:Lspeech/patts/NavigationFeature;

.field private sval_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 764
    new-instance v0, Lspeech/patts/Question;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/Question;-><init>(Z)V

    sput-object v0, Lspeech/patts/Question;->defaultInstance:Lspeech/patts/Question;

    .line 765
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 766
    sget-object v0, Lspeech/patts/Question;->defaultInstance:Lspeech/patts/Question;

    invoke-direct {v0}, Lspeech/patts/Question;->initFields()V

    .line 767
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 117
    iput-boolean v0, p0, Lspeech/patts/Question;->bval_:Z

    .line 124
    iput v0, p0, Lspeech/patts/Question;->ival_:I

    .line 131
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Question;->sval_:Ljava/lang/String;

    .line 138
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/Question;->fval_:F

    .line 151
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Question;->q_:Ljava/util/List;

    .line 219
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Question;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/Question;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/Question$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/Question$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/Question;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 117
    iput-boolean v0, p0, Lspeech/patts/Question;->bval_:Z

    .line 124
    iput v0, p0, Lspeech/patts/Question;->ival_:I

    .line 131
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Question;->sval_:Ljava/lang/String;

    .line 138
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/Question;->fval_:F

    .line 151
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Question;->q_:Ljava/util/List;

    .line 219
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Question;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/Question;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Question;->hasIval:Z

    return p1
.end method

.method static synthetic access$1102(Lspeech/patts/Question;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Question;->ival_:I

    return p1
.end method

.method static synthetic access$1202(Lspeech/patts/Question;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Question;->hasSval:Z

    return p1
.end method

.method static synthetic access$1302(Lspeech/patts/Question;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Question;->sval_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1402(Lspeech/patts/Question;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Question;->hasFval:Z

    return p1
.end method

.method static synthetic access$1502(Lspeech/patts/Question;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Question;->fval_:F

    return p1
.end method

.method static synthetic access$1602(Lspeech/patts/Question;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Question;->hasComp:Z

    return p1
.end method

.method static synthetic access$1702(Lspeech/patts/Question;Lspeech/patts/Question$Comparison;)Lspeech/patts/Question$Comparison;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Lspeech/patts/Question$Comparison;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Question;->comp_:Lspeech/patts/Question$Comparison;

    return-object p1
.end method

.method static synthetic access$1802(Lspeech/patts/Question;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Question;->hasConj:Z

    return p1
.end method

.method static synthetic access$1902(Lspeech/patts/Question;Lspeech/patts/Question$Conjunction;)Lspeech/patts/Question$Conjunction;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Lspeech/patts/Question$Conjunction;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Question;->conj_:Lspeech/patts/Question$Conjunction;

    return-object p1
.end method

.method static synthetic access$300(Lspeech/patts/Question;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Question;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Question;->q_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/Question;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Question;->q_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/Question;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Question;->hasNf:Z

    return p1
.end method

.method static synthetic access$500(Lspeech/patts/Question;)Lspeech/patts/NavigationFeature;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Question;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Question;->nf_:Lspeech/patts/NavigationFeature;

    return-object v0
.end method

.method static synthetic access$502(Lspeech/patts/Question;Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Lspeech/patts/NavigationFeature;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Question;->nf_:Lspeech/patts/NavigationFeature;

    return-object p1
.end method

.method static synthetic access$602(Lspeech/patts/Question;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Question;->hasRhs:Z

    return p1
.end method

.method static synthetic access$700(Lspeech/patts/Question;)Lspeech/patts/NavigationFeature;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Question;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Question;->rhs_:Lspeech/patts/NavigationFeature;

    return-object v0
.end method

.method static synthetic access$702(Lspeech/patts/Question;Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Lspeech/patts/NavigationFeature;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Question;->rhs_:Lspeech/patts/NavigationFeature;

    return-object p1
.end method

.method static synthetic access$802(Lspeech/patts/Question;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Question;->hasBval:Z

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/Question;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Question;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Question;->bval_:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/Question;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/Question;->defaultInstance:Lspeech/patts/Question;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 169
    invoke-static {}, Lspeech/patts/NavigationFeature;->getDefaultInstance()Lspeech/patts/NavigationFeature;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Question;->nf_:Lspeech/patts/NavigationFeature;

    .line 170
    invoke-static {}, Lspeech/patts/NavigationFeature;->getDefaultInstance()Lspeech/patts/NavigationFeature;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Question;->rhs_:Lspeech/patts/NavigationFeature;

    .line 171
    sget-object v0, Lspeech/patts/Question$Comparison;->EQ:Lspeech/patts/Question$Comparison;

    iput-object v0, p0, Lspeech/patts/Question;->comp_:Lspeech/patts/Question$Comparison;

    .line 172
    sget-object v0, Lspeech/patts/Question$Conjunction;->AND:Lspeech/patts/Question$Conjunction;

    iput-object v0, p0, Lspeech/patts/Question;->conj_:Lspeech/patts/Question$Conjunction;

    .line 173
    return-void
.end method

.method public static newBuilder()Lspeech/patts/Question$Builder;
    .locals 1

    .prologue
    .line 332
    # invokes: Lspeech/patts/Question$Builder;->create()Lspeech/patts/Question$Builder;
    invoke-static {}, Lspeech/patts/Question$Builder;->access$100()Lspeech/patts/Question$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/Question;)Lspeech/patts/Question$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/Question;

    .prologue
    .line 335
    invoke-static {}, Lspeech/patts/Question;->newBuilder()Lspeech/patts/Question$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/Question$Builder;->mergeFrom(Lspeech/patts/Question;)Lspeech/patts/Question$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBval()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lspeech/patts/Question;->bval_:Z

    return v0
.end method

.method public getComp()Lspeech/patts/Question$Comparison;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lspeech/patts/Question;->comp_:Lspeech/patts/Question$Comparison;

    return-object v0
.end method

.method public getConj()Lspeech/patts/Question$Conjunction;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lspeech/patts/Question;->conj_:Lspeech/patts/Question$Conjunction;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Question;->getDefaultInstanceForType()Lspeech/patts/Question;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/Question;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/Question;->defaultInstance:Lspeech/patts/Question;

    return-object v0
.end method

.method public getFval()F
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lspeech/patts/Question;->fval_:F

    return v0
.end method

.method public getIval()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lspeech/patts/Question;->ival_:I

    return v0
.end method

.method public getNf()Lspeech/patts/NavigationFeature;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lspeech/patts/Question;->nf_:Lspeech/patts/NavigationFeature;

    return-object v0
.end method

.method public getQList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Question;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lspeech/patts/Question;->q_:Ljava/util/List;

    return-object v0
.end method

.method public getRhs()Lspeech/patts/NavigationFeature;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lspeech/patts/Question;->rhs_:Lspeech/patts/NavigationFeature;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 221
    iget v2, p0, Lspeech/patts/Question;->memoizedSerializedSize:I

    .line 222
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 262
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 224
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 225
    invoke-virtual {p0}, Lspeech/patts/Question;->hasNf()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 226
    const/4 v4, 0x1

    invoke-virtual {p0}, Lspeech/patts/Question;->getNf()Lspeech/patts/NavigationFeature;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 229
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Question;->hasBval()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 230
    const/4 v4, 0x2

    invoke-virtual {p0}, Lspeech/patts/Question;->getBval()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 233
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Question;->hasIval()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 234
    const/4 v4, 0x3

    invoke-virtual {p0}, Lspeech/patts/Question;->getIval()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 237
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Question;->hasSval()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 238
    const/4 v4, 0x4

    invoke-virtual {p0}, Lspeech/patts/Question;->getSval()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 241
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Question;->hasComp()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 242
    const/4 v4, 0x5

    invoke-virtual {p0}, Lspeech/patts/Question;->getComp()Lspeech/patts/Question$Comparison;

    move-result-object v5

    invoke-virtual {v5}, Lspeech/patts/Question$Comparison;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 245
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/Question;->getQList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Question;

    .line 246
    .local v0, "element":Lspeech/patts/Question;
    const/4 v4, 0x6

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 248
    goto :goto_1

    .line 249
    .end local v0    # "element":Lspeech/patts/Question;
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/Question;->hasConj()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 250
    const/4 v4, 0x7

    invoke-virtual {p0}, Lspeech/patts/Question;->getConj()Lspeech/patts/Question$Conjunction;

    move-result-object v5

    invoke-virtual {v5}, Lspeech/patts/Question$Conjunction;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 253
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/Question;->hasFval()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 254
    const/16 v4, 0x8

    invoke-virtual {p0}, Lspeech/patts/Question;->getFval()F

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v4

    add-int/2addr v2, v4

    .line 257
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/Question;->hasRhs()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 258
    const/16 v4, 0x9

    invoke-virtual {p0}, Lspeech/patts/Question;->getRhs()Lspeech/patts/NavigationFeature;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 261
    :cond_9
    iput v2, p0, Lspeech/patts/Question;->memoizedSerializedSize:I

    move v3, v2

    .line 262
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto/16 :goto_0
.end method

.method public getSval()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lspeech/patts/Question;->sval_:Ljava/lang/String;

    return-object v0
.end method

.method public hasBval()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lspeech/patts/Question;->hasBval:Z

    return v0
.end method

.method public hasComp()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lspeech/patts/Question;->hasComp:Z

    return v0
.end method

.method public hasConj()Z
    .locals 1

    .prologue
    .line 165
    iget-boolean v0, p0, Lspeech/patts/Question;->hasConj:Z

    return v0
.end method

.method public hasFval()Z
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lspeech/patts/Question;->hasFval:Z

    return v0
.end method

.method public hasIval()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lspeech/patts/Question;->hasIval:Z

    return v0
.end method

.method public hasNf()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lspeech/patts/Question;->hasNf:Z

    return v0
.end method

.method public hasRhs()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lspeech/patts/Question;->hasRhs:Z

    return v0
.end method

.method public hasSval()Z
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lspeech/patts/Question;->hasSval:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 175
    invoke-virtual {p0}, Lspeech/patts/Question;->hasNf()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 176
    invoke-virtual {p0}, Lspeech/patts/Question;->getNf()Lspeech/patts/NavigationFeature;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/NavigationFeature;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1

    .line 184
    :cond_0
    :goto_0
    return v2

    .line 178
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Question;->hasRhs()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 179
    invoke-virtual {p0}, Lspeech/patts/Question;->getRhs()Lspeech/patts/NavigationFeature;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/NavigationFeature;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 181
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Question;->getQList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Question;

    .line 182
    .local v0, "element":Lspeech/patts/Question;
    invoke-virtual {v0}, Lspeech/patts/Question;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_0

    .line 184
    .end local v0    # "element":Lspeech/patts/Question;
    :cond_4
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Question;->toBuilder()Lspeech/patts/Question$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/Question$Builder;
    .locals 1

    .prologue
    .line 337
    invoke-static {p0}, Lspeech/patts/Question;->newBuilder(Lspeech/patts/Question;)Lspeech/patts/Question$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 189
    invoke-virtual {p0}, Lspeech/patts/Question;->getSerializedSize()I

    .line 190
    invoke-virtual {p0}, Lspeech/patts/Question;->hasNf()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 191
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/Question;->getNf()Lspeech/patts/NavigationFeature;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 193
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Question;->hasBval()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 194
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/Question;->getBval()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 196
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Question;->hasIval()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 197
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/Question;->getIval()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 199
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Question;->hasSval()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 200
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/Question;->getSval()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 202
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Question;->hasComp()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 203
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/Question;->getComp()Lspeech/patts/Question$Comparison;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/Question$Comparison;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 205
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Question;->getQList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Question;

    .line 206
    .local v0, "element":Lspeech/patts/Question;
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 208
    .end local v0    # "element":Lspeech/patts/Question;
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/Question;->hasConj()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 209
    const/4 v2, 0x7

    invoke-virtual {p0}, Lspeech/patts/Question;->getConj()Lspeech/patts/Question$Conjunction;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/Question$Conjunction;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 211
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/Question;->hasFval()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 212
    const/16 v2, 0x8

    invoke-virtual {p0}, Lspeech/patts/Question;->getFval()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 214
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/Question;->hasRhs()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 215
    const/16 v2, 0x9

    invoke-virtual {p0}, Lspeech/patts/Question;->getRhs()Lspeech/patts/NavigationFeature;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 217
    :cond_8
    return-void
.end method
