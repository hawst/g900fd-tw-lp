.class public final Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FrequencyWarpAndWeightTransforms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/FrequencyWarpAndWeightTransforms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/FrequencyWarpAndWeightTransforms;",
        "Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/FrequencyWarpAndWeightTransforms;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->create()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
    .locals 3

    .prologue
    .line 177
    new-instance v0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    invoke-direct {v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;-><init>()V

    .line 178
    .local v0, "builder":Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
    new-instance v1, Lspeech/patts/FrequencyWarpAndWeightTransforms;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/FrequencyWarpAndWeightTransforms;-><init>(Lspeech/patts/FrequencyWarpAndWeightTransforms$1;)V

    iput-object v1, v0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .line 179
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->build()Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/FrequencyWarpAndWeightTransforms;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 210
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->buildPartial()Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/FrequencyWarpAndWeightTransforms;
    .locals 3

    .prologue
    .line 223
    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    if-nez v1, :cond_0

    .line 224
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 227
    :cond_0
    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$300(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 228
    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    iget-object v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$300(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$302(Lspeech/patts/FrequencyWarpAndWeightTransforms;Ljava/util/List;)Ljava/util/List;

    .line 231
    :cond_1
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .line 232
    .local v0, "returnMe":Lspeech/patts/FrequencyWarpAndWeightTransforms;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .line 233
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->clone()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->clone()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->clone()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
    .locals 2

    .prologue
    .line 196
    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->create()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    invoke-virtual {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->mergeFrom(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    invoke-virtual {v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 168
    check-cast p1, Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->mergeFrom(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .prologue
    .line 237
    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getDefaultInstance()Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-object p0

    .line 238
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasProsody()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 239
    invoke-virtual {p1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getProsody()Lspeech/patts/ProsodyAdaptation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->mergeProsody(Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    .line 241
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasGlobal()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 242
    invoke-virtual {p1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getGlobal()Lspeech/patts/FrequencyWarpAndWeightTransform;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->mergeGlobal(Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    .line 244
    :cond_3
    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$300(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$300(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 246
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$302(Lspeech/patts/FrequencyWarpAndWeightTransforms;Ljava/util/List;)Ljava/util/List;

    .line 248
    :cond_4
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$300(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$300(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeGlobal(Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/FrequencyWarpAndWeightTransform;

    .prologue
    .line 355
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    invoke-virtual {v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasGlobal()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->global_:Lspeech/patts/FrequencyWarpAndWeightTransform;
    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$700(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransform;

    move-result-object v0

    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getDefaultInstance()Lspeech/patts/FrequencyWarpAndWeightTransform;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 357
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->global_:Lspeech/patts/FrequencyWarpAndWeightTransform;
    invoke-static {v1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$700(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransform;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->newBuilder(Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->mergeFrom(Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->buildPartial()Lspeech/patts/FrequencyWarpAndWeightTransform;

    move-result-object v1

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->global_:Lspeech/patts/FrequencyWarpAndWeightTransform;
    invoke-static {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$702(Lspeech/patts/FrequencyWarpAndWeightTransforms;Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransform;

    .line 362
    :goto_0
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasGlobal:Z
    invoke-static {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$602(Lspeech/patts/FrequencyWarpAndWeightTransforms;Z)Z

    .line 363
    return-object p0

    .line 360
    :cond_0
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->global_:Lspeech/patts/FrequencyWarpAndWeightTransform;
    invoke-static {v0, p1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$702(Lspeech/patts/FrequencyWarpAndWeightTransforms;Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransform;

    goto :goto_0
.end method

.method public mergeProsody(Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/ProsodyAdaptation;

    .prologue
    .line 318
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    invoke-virtual {v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasProsody()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->prosody_:Lspeech/patts/ProsodyAdaptation;
    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$500(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/ProsodyAdaptation;

    move-result-object v0

    invoke-static {}, Lspeech/patts/ProsodyAdaptation;->getDefaultInstance()Lspeech/patts/ProsodyAdaptation;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 320
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->prosody_:Lspeech/patts/ProsodyAdaptation;
    invoke-static {v1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$500(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/ProsodyAdaptation;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/ProsodyAdaptation;->newBuilder(Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/ProsodyAdaptation$Builder;->mergeFrom(Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/ProsodyAdaptation$Builder;->buildPartial()Lspeech/patts/ProsodyAdaptation;

    move-result-object v1

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->prosody_:Lspeech/patts/ProsodyAdaptation;
    invoke-static {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$502(Lspeech/patts/FrequencyWarpAndWeightTransforms;Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/ProsodyAdaptation;

    .line 325
    :goto_0
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasProsody:Z
    invoke-static {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$402(Lspeech/patts/FrequencyWarpAndWeightTransforms;Z)Z

    .line 326
    return-object p0

    .line 323
    :cond_0
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransforms;->prosody_:Lspeech/patts/ProsodyAdaptation;
    invoke-static {v0, p1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->access$502(Lspeech/patts/FrequencyWarpAndWeightTransforms;Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/ProsodyAdaptation;

    goto :goto_0
.end method
