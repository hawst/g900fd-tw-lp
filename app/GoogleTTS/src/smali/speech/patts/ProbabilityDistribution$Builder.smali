.class public final Lspeech/patts/ProbabilityDistribution$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ProbabilityDistribution.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/ProbabilityDistribution;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/ProbabilityDistribution;",
        "Lspeech/patts/ProbabilityDistribution$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/ProbabilityDistribution;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/ProbabilityDistribution$Builder;
    .locals 1

    .prologue
    .line 245
    invoke-static {}, Lspeech/patts/ProbabilityDistribution$Builder;->create()Lspeech/patts/ProbabilityDistribution$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/ProbabilityDistribution$Builder;
    .locals 3

    .prologue
    .line 254
    new-instance v0, Lspeech/patts/ProbabilityDistribution$Builder;

    invoke-direct {v0}, Lspeech/patts/ProbabilityDistribution$Builder;-><init>()V

    .line 255
    .local v0, "builder":Lspeech/patts/ProbabilityDistribution$Builder;
    new-instance v1, Lspeech/patts/ProbabilityDistribution;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/ProbabilityDistribution;-><init>(Lspeech/patts/ProbabilityDistribution$1;)V

    iput-object v1, v0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    .line 256
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 245
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution$Builder;->build()Lspeech/patts/ProbabilityDistribution;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/ProbabilityDistribution;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 287
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution$Builder;->buildPartial()Lspeech/patts/ProbabilityDistribution;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/ProbabilityDistribution;
    .locals 3

    .prologue
    .line 300
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    if-nez v1, :cond_0

    .line 301
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 304
    :cond_0
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/ProbabilityDistribution;->access$300(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 305
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    iget-object v2, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/ProbabilityDistribution;->access$300(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/ProbabilityDistribution;->access$302(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;

    .line 308
    :cond_1
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/ProbabilityDistribution;->access$400(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 309
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    iget-object v2, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/ProbabilityDistribution;->access$400(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/ProbabilityDistribution;->access$402(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;

    .line 312
    :cond_2
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/ProbabilityDistribution;->access$500(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 313
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    iget-object v2, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/ProbabilityDistribution;->access$500(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/ProbabilityDistribution;->access$502(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;

    .line 316
    :cond_3
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/ProbabilityDistribution;->access$600(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 317
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    iget-object v2, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/ProbabilityDistribution;->access$600(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/ProbabilityDistribution;->access$602(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;

    .line 320
    :cond_4
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/ProbabilityDistribution;->access$700(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_5

    .line 321
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    iget-object v2, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/ProbabilityDistribution;->access$700(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/ProbabilityDistribution;->access$702(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;

    .line 324
    :cond_5
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/ProbabilityDistribution;->access$800(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_6

    .line 325
    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    iget-object v2, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/ProbabilityDistribution;->access$800(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/ProbabilityDistribution;->access$802(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;

    .line 328
    :cond_6
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    .line 329
    .local v0, "returnMe":Lspeech/patts/ProbabilityDistribution;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    .line 330
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 245
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution$Builder;->clone()Lspeech/patts/ProbabilityDistribution$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 245
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution$Builder;->clone()Lspeech/patts/ProbabilityDistribution$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 245
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution$Builder;->clone()Lspeech/patts/ProbabilityDistribution$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/ProbabilityDistribution$Builder;
    .locals 2

    .prologue
    .line 273
    invoke-static {}, Lspeech/patts/ProbabilityDistribution$Builder;->create()Lspeech/patts/ProbabilityDistribution$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    invoke-virtual {v0, v1}, Lspeech/patts/ProbabilityDistribution$Builder;->mergeFrom(Lspeech/patts/ProbabilityDistribution;)Lspeech/patts/ProbabilityDistribution$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    invoke-virtual {v0}, Lspeech/patts/ProbabilityDistribution;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 245
    check-cast p1, Lspeech/patts/ProbabilityDistribution;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/ProbabilityDistribution$Builder;->mergeFrom(Lspeech/patts/ProbabilityDistribution;)Lspeech/patts/ProbabilityDistribution$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/ProbabilityDistribution;)Lspeech/patts/ProbabilityDistribution$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/ProbabilityDistribution;

    .prologue
    .line 334
    invoke-static {}, Lspeech/patts/ProbabilityDistribution;->getDefaultInstance()Lspeech/patts/ProbabilityDistribution;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 374
    :cond_0
    :goto_0
    return-object p0

    .line 335
    :cond_1
    # getter for: Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProbabilityDistribution;->access$300(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 336
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution;->access$300(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/ProbabilityDistribution;->access$302(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;

    .line 339
    :cond_2
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution;->access$300(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProbabilityDistribution;->access$300(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 341
    :cond_3
    # getter for: Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProbabilityDistribution;->access$400(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 342
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution;->access$400(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 343
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/ProbabilityDistribution;->access$402(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;

    .line 345
    :cond_4
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution;->access$400(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProbabilityDistribution;->access$400(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 347
    :cond_5
    # getter for: Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProbabilityDistribution;->access$500(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 348
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution;->access$500(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 349
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/ProbabilityDistribution;->access$502(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;

    .line 351
    :cond_6
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution;->access$500(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProbabilityDistribution;->access$500(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 353
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/ProbabilityDistribution;->hasVoicingPosterior()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 354
    invoke-virtual {p1}, Lspeech/patts/ProbabilityDistribution;->getVoicingPosterior()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/ProbabilityDistribution$Builder;->setVoicingPosterior(F)Lspeech/patts/ProbabilityDistribution$Builder;

    .line 356
    :cond_8
    # getter for: Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProbabilityDistribution;->access$600(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 357
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution;->access$600(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 358
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/ProbabilityDistribution;->access$602(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;

    .line 360
    :cond_9
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution;->access$600(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProbabilityDistribution;->access$600(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 362
    :cond_a
    # getter for: Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProbabilityDistribution;->access$700(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 363
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution;->access$700(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 364
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/ProbabilityDistribution;->access$702(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;

    .line 366
    :cond_b
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution;->access$700(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProbabilityDistribution;->access$700(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 368
    :cond_c
    # getter for: Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProbabilityDistribution;->access$800(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 369
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution;->access$800(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 370
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/ProbabilityDistribution;->access$802(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;

    .line 372
    :cond_d
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # getter for: Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProbabilityDistribution;->access$800(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProbabilityDistribution;->access$800(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setVoicingPosterior(F)Lspeech/patts/ProbabilityDistribution$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 598
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/ProbabilityDistribution;->hasVoicingPosterior:Z
    invoke-static {v0, v1}, Lspeech/patts/ProbabilityDistribution;->access$902(Lspeech/patts/ProbabilityDistribution;Z)Z

    .line 599
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution$Builder;->result:Lspeech/patts/ProbabilityDistribution;

    # setter for: Lspeech/patts/ProbabilityDistribution;->voicingPosterior_:F
    invoke-static {v0, p1}, Lspeech/patts/ProbabilityDistribution;->access$1002(Lspeech/patts/ProbabilityDistribution;F)F

    .line 600
    return-object p0
.end method
