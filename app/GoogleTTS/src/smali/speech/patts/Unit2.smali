.class public final Lspeech/patts/Unit2;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Unit2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/Unit2$1;,
        Lspeech/patts/Unit2$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/Unit2;


# instance fields
.field private end_:F

.field private filename_:Ljava/lang/String;

.field private hasEnd:Z

.field private hasFilename:Z

.field private hasId:Z

.field private hasLeftJoin:Z

.field private hasNumPmToEnd:Z

.field private hasNumPmToMid:Z

.field private hasRightJoin:Z

.field private hasStart:Z

.field private hasStartPm:Z

.field private hasTarget:Z

.field private id_:I

.field private leftJoin_:Lspeech/patts/FeatureData;

.field private memoizedSerializedSize:I

.field private numPmToEnd_:I

.field private numPmToMid_:I

.field private rightJoin_:Lspeech/patts/FeatureData;

.field private startPm_:I

.field private start_:F

.field private target_:Lspeech/patts/FeatureData;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 684
    new-instance v0, Lspeech/patts/Unit2;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/Unit2;-><init>(Z)V

    sput-object v0, Lspeech/patts/Unit2;->defaultInstance:Lspeech/patts/Unit2;

    .line 685
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 686
    sget-object v0, Lspeech/patts/Unit2;->defaultInstance:Lspeech/patts/Unit2;

    invoke-direct {v0}, Lspeech/patts/Unit2;->initFields()V

    .line 687
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lspeech/patts/Unit2;->id_:I

    .line 32
    iput v0, p0, Lspeech/patts/Unit2;->startPm_:I

    .line 39
    iput v0, p0, Lspeech/patts/Unit2;->numPmToMid_:I

    .line 46
    iput v0, p0, Lspeech/patts/Unit2;->numPmToEnd_:I

    .line 74
    iput v1, p0, Lspeech/patts/Unit2;->start_:F

    .line 81
    iput v1, p0, Lspeech/patts/Unit2;->end_:F

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Unit2;->filename_:Ljava/lang/String;

    .line 142
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Unit2;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/Unit2;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/Unit2$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/Unit2$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/Unit2;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lspeech/patts/Unit2;->id_:I

    .line 32
    iput v0, p0, Lspeech/patts/Unit2;->startPm_:I

    .line 39
    iput v0, p0, Lspeech/patts/Unit2;->numPmToMid_:I

    .line 46
    iput v0, p0, Lspeech/patts/Unit2;->numPmToEnd_:I

    .line 74
    iput v1, p0, Lspeech/patts/Unit2;->start_:F

    .line 81
    iput v1, p0, Lspeech/patts/Unit2;->end_:F

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Unit2;->filename_:Ljava/lang/String;

    .line 142
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Unit2;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/Unit2;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Unit2;->numPmToEnd_:I

    return p1
.end method

.method static synthetic access$1102(Lspeech/patts/Unit2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Unit2;->hasLeftJoin:Z

    return p1
.end method

.method static synthetic access$1200(Lspeech/patts/Unit2;)Lspeech/patts/FeatureData;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Unit2;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Unit2;->leftJoin_:Lspeech/patts/FeatureData;

    return-object v0
.end method

.method static synthetic access$1202(Lspeech/patts/Unit2;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Lspeech/patts/FeatureData;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Unit2;->leftJoin_:Lspeech/patts/FeatureData;

    return-object p1
.end method

.method static synthetic access$1302(Lspeech/patts/Unit2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Unit2;->hasRightJoin:Z

    return p1
.end method

.method static synthetic access$1400(Lspeech/patts/Unit2;)Lspeech/patts/FeatureData;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Unit2;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Unit2;->rightJoin_:Lspeech/patts/FeatureData;

    return-object v0
.end method

.method static synthetic access$1402(Lspeech/patts/Unit2;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Lspeech/patts/FeatureData;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Unit2;->rightJoin_:Lspeech/patts/FeatureData;

    return-object p1
.end method

.method static synthetic access$1502(Lspeech/patts/Unit2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Unit2;->hasTarget:Z

    return p1
.end method

.method static synthetic access$1600(Lspeech/patts/Unit2;)Lspeech/patts/FeatureData;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Unit2;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Unit2;->target_:Lspeech/patts/FeatureData;

    return-object v0
.end method

.method static synthetic access$1602(Lspeech/patts/Unit2;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Lspeech/patts/FeatureData;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Unit2;->target_:Lspeech/patts/FeatureData;

    return-object p1
.end method

.method static synthetic access$1702(Lspeech/patts/Unit2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Unit2;->hasStart:Z

    return p1
.end method

.method static synthetic access$1802(Lspeech/patts/Unit2;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Unit2;->start_:F

    return p1
.end method

.method static synthetic access$1902(Lspeech/patts/Unit2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Unit2;->hasEnd:Z

    return p1
.end method

.method static synthetic access$2002(Lspeech/patts/Unit2;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Unit2;->end_:F

    return p1
.end method

.method static synthetic access$2102(Lspeech/patts/Unit2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Unit2;->hasFilename:Z

    return p1
.end method

.method static synthetic access$2202(Lspeech/patts/Unit2;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Unit2;->filename_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lspeech/patts/Unit2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Unit2;->hasId:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/Unit2;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Unit2;->id_:I

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/Unit2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Unit2;->hasStartPm:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/Unit2;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Unit2;->startPm_:I

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/Unit2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Unit2;->hasNumPmToMid:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/Unit2;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Unit2;->numPmToMid_:I

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/Unit2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Unit2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Unit2;->hasNumPmToEnd:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/Unit2;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/Unit2;->defaultInstance:Lspeech/patts/Unit2;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 93
    invoke-static {}, Lspeech/patts/FeatureData;->getDefaultInstance()Lspeech/patts/FeatureData;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Unit2;->leftJoin_:Lspeech/patts/FeatureData;

    .line 94
    invoke-static {}, Lspeech/patts/FeatureData;->getDefaultInstance()Lspeech/patts/FeatureData;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Unit2;->rightJoin_:Lspeech/patts/FeatureData;

    .line 95
    invoke-static {}, Lspeech/patts/FeatureData;->getDefaultInstance()Lspeech/patts/FeatureData;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Unit2;->target_:Lspeech/patts/FeatureData;

    .line 96
    return-void
.end method

.method public static newBuilder()Lspeech/patts/Unit2$Builder;
    .locals 1

    .prologue
    .line 259
    # invokes: Lspeech/patts/Unit2$Builder;->create()Lspeech/patts/Unit2$Builder;
    invoke-static {}, Lspeech/patts/Unit2$Builder;->access$100()Lspeech/patts/Unit2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/Unit2;)Lspeech/patts/Unit2$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/Unit2;

    .prologue
    .line 262
    invoke-static {}, Lspeech/patts/Unit2;->newBuilder()Lspeech/patts/Unit2$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/Unit2$Builder;->mergeFrom(Lspeech/patts/Unit2;)Lspeech/patts/Unit2$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Unit2;->getDefaultInstanceForType()Lspeech/patts/Unit2;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/Unit2;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/Unit2;->defaultInstance:Lspeech/patts/Unit2;

    return-object v0
.end method

.method public getEnd()F
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lspeech/patts/Unit2;->end_:F

    return v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lspeech/patts/Unit2;->filename_:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lspeech/patts/Unit2;->id_:I

    return v0
.end method

.method public getLeftJoin()Lspeech/patts/FeatureData;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lspeech/patts/Unit2;->leftJoin_:Lspeech/patts/FeatureData;

    return-object v0
.end method

.method public getNumPmToEnd()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lspeech/patts/Unit2;->numPmToEnd_:I

    return v0
.end method

.method public getNumPmToMid()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lspeech/patts/Unit2;->numPmToMid_:I

    return v0
.end method

.method public getRightJoin()Lspeech/patts/FeatureData;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lspeech/patts/Unit2;->rightJoin_:Lspeech/patts/FeatureData;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 144
    iget v0, p0, Lspeech/patts/Unit2;->memoizedSerializedSize:I

    .line 145
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 189
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 147
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 148
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 149
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 152
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasStartPm()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 153
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getStartPm()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 156
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasNumPmToMid()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 157
    const/4 v2, 0x6

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getNumPmToMid()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 160
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasNumPmToEnd()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 161
    const/4 v2, 0x7

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getNumPmToEnd()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 164
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasLeftJoin()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 165
    const/16 v2, 0x8

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getLeftJoin()Lspeech/patts/FeatureData;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 168
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasRightJoin()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 169
    const/16 v2, 0x9

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getRightJoin()Lspeech/patts/FeatureData;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 172
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasFilename()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 173
    const/16 v2, 0xa

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 176
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasTarget()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 177
    const/16 v2, 0xb

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getTarget()Lspeech/patts/FeatureData;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 180
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasStart()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 181
    const/16 v2, 0xc

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getStart()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 184
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasEnd()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 185
    const/16 v2, 0xe

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getEnd()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 188
    :cond_a
    iput v0, p0, Lspeech/patts/Unit2;->memoizedSerializedSize:I

    move v1, v0

    .line 189
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto/16 :goto_0
.end method

.method public getStart()F
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lspeech/patts/Unit2;->start_:F

    return v0
.end method

.method public getStartPm()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lspeech/patts/Unit2;->startPm_:I

    return v0
.end method

.method public getTarget()Lspeech/patts/FeatureData;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lspeech/patts/Unit2;->target_:Lspeech/patts/FeatureData;

    return-object v0
.end method

.method public hasEnd()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lspeech/patts/Unit2;->hasEnd:Z

    return v0
.end method

.method public hasFilename()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lspeech/patts/Unit2;->hasFilename:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/Unit2;->hasId:Z

    return v0
.end method

.method public hasLeftJoin()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lspeech/patts/Unit2;->hasLeftJoin:Z

    return v0
.end method

.method public hasNumPmToEnd()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lspeech/patts/Unit2;->hasNumPmToEnd:Z

    return v0
.end method

.method public hasNumPmToMid()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lspeech/patts/Unit2;->hasNumPmToMid:Z

    return v0
.end method

.method public hasRightJoin()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lspeech/patts/Unit2;->hasRightJoin:Z

    return v0
.end method

.method public hasStart()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lspeech/patts/Unit2;->hasStart:Z

    return v0
.end method

.method public hasStartPm()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/Unit2;->hasStartPm:Z

    return v0
.end method

.method public hasTarget()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lspeech/patts/Unit2;->hasTarget:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 98
    iget-boolean v1, p0, Lspeech/patts/Unit2;->hasId:Z

    if-nez v1, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 99
    :cond_1
    iget-boolean v1, p0, Lspeech/patts/Unit2;->hasStartPm:Z

    if-eqz v1, :cond_0

    .line 100
    iget-boolean v1, p0, Lspeech/patts/Unit2;->hasNumPmToMid:Z

    if-eqz v1, :cond_0

    .line 101
    iget-boolean v1, p0, Lspeech/patts/Unit2;->hasNumPmToEnd:Z

    if-eqz v1, :cond_0

    .line 102
    iget-boolean v1, p0, Lspeech/patts/Unit2;->hasLeftJoin:Z

    if-eqz v1, :cond_0

    .line 103
    iget-boolean v1, p0, Lspeech/patts/Unit2;->hasRightJoin:Z

    if-eqz v1, :cond_0

    .line 104
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Unit2;->toBuilder()Lspeech/patts/Unit2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/Unit2$Builder;
    .locals 1

    .prologue
    .line 264
    invoke-static {p0}, Lspeech/patts/Unit2;->newBuilder(Lspeech/patts/Unit2;)Lspeech/patts/Unit2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p0}, Lspeech/patts/Unit2;->getSerializedSize()I

    .line 110
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 113
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasStartPm()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    const/4 v0, 0x5

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getStartPm()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 116
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasNumPmToMid()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    const/4 v0, 0x6

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getNumPmToMid()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 119
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasNumPmToEnd()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 120
    const/4 v0, 0x7

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getNumPmToEnd()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 122
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasLeftJoin()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 123
    const/16 v0, 0x8

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getLeftJoin()Lspeech/patts/FeatureData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 125
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasRightJoin()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 126
    const/16 v0, 0x9

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getRightJoin()Lspeech/patts/FeatureData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 128
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasFilename()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 129
    const/16 v0, 0xa

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 131
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 132
    const/16 v0, 0xb

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getTarget()Lspeech/patts/FeatureData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 134
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasStart()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 135
    const/16 v0, 0xc

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getStart()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 137
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/Unit2;->hasEnd()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 138
    const/16 v0, 0xe

    invoke-virtual {p0}, Lspeech/patts/Unit2;->getEnd()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 140
    :cond_9
    return-void
.end method
