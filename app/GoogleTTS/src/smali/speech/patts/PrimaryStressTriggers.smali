.class public final Lspeech/patts/PrimaryStressTriggers;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PrimaryStressTriggers.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/PrimaryStressTriggers$1;,
        Lspeech/patts/PrimaryStressTriggers$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/PrimaryStressTriggers;


# instance fields
.field private endsWith_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private startsWith_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 366
    new-instance v0, Lspeech/patts/PrimaryStressTriggers;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/PrimaryStressTriggers;-><init>(Z)V

    sput-object v0, Lspeech/patts/PrimaryStressTriggers;->defaultInstance:Lspeech/patts/PrimaryStressTriggers;

    .line 367
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 368
    sget-object v0, Lspeech/patts/PrimaryStressTriggers;->defaultInstance:Lspeech/patts/PrimaryStressTriggers;

    invoke-direct {v0}, Lspeech/patts/PrimaryStressTriggers;->initFields()V

    .line 369
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PrimaryStressTriggers;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/PrimaryStressTriggers;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/PrimaryStressTriggers$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/PrimaryStressTriggers$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/PrimaryStressTriggers;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PrimaryStressTriggers;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PrimaryStressTriggers;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/PrimaryStressTriggers;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PrimaryStressTriggers;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PrimaryStressTriggers;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lspeech/patts/PrimaryStressTriggers;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PrimaryStressTriggers;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/PrimaryStressTriggers;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/PrimaryStressTriggers;->defaultInstance:Lspeech/patts/PrimaryStressTriggers;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 47
    return-void
.end method

.method public static newBuilder()Lspeech/patts/PrimaryStressTriggers$Builder;
    .locals 1

    .prologue
    .line 158
    # invokes: Lspeech/patts/PrimaryStressTriggers$Builder;->create()Lspeech/patts/PrimaryStressTriggers$Builder;
    invoke-static {}, Lspeech/patts/PrimaryStressTriggers$Builder;->access$100()Lspeech/patts/PrimaryStressTriggers$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/PrimaryStressTriggers;)Lspeech/patts/PrimaryStressTriggers$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/PrimaryStressTriggers;

    .prologue
    .line 161
    invoke-static {}, Lspeech/patts/PrimaryStressTriggers;->newBuilder()Lspeech/patts/PrimaryStressTriggers$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/PrimaryStressTriggers$Builder;->mergeFrom(Lspeech/patts/PrimaryStressTriggers;)Lspeech/patts/PrimaryStressTriggers$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers;->getDefaultInstanceForType()Lspeech/patts/PrimaryStressTriggers;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/PrimaryStressTriggers;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/PrimaryStressTriggers;->defaultInstance:Lspeech/patts/PrimaryStressTriggers;

    return-object v0
.end method

.method public getEndsWithList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 65
    iget v3, p0, Lspeech/patts/PrimaryStressTriggers;->memoizedSerializedSize:I

    .line 66
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 88
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 68
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 70
    const/4 v0, 0x0

    .line 71
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers;->getStartsWithList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 72
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 74
    goto :goto_1

    .line 75
    .end local v1    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v3, v0

    .line 76
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers;->getStartsWithList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 79
    const/4 v0, 0x0

    .line 80
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers;->getEndsWithList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 81
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 83
    goto :goto_2

    .line 84
    .end local v1    # "element":Ljava/lang/String;
    :cond_2
    add-int/2addr v3, v0

    .line 85
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers;->getEndsWithList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 87
    iput v3, p0, Lspeech/patts/PrimaryStressTriggers;->memoizedSerializedSize:I

    move v4, v3

    .line 88
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto :goto_0
.end method

.method public getStartsWithList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers;->toBuilder()Lspeech/patts/PrimaryStressTriggers$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/PrimaryStressTriggers$Builder;
    .locals 1

    .prologue
    .line 163
    invoke-static {p0}, Lspeech/patts/PrimaryStressTriggers;->newBuilder(Lspeech/patts/PrimaryStressTriggers;)Lspeech/patts/PrimaryStressTriggers$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers;->getSerializedSize()I

    .line 55
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers;->getStartsWithList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 56
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 58
    .end local v0    # "element":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers;->getEndsWithList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 59
    .restart local v0    # "element":Ljava/lang/String;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_1

    .line 61
    .end local v0    # "element":Ljava/lang/String;
    :cond_1
    return-void
.end method
