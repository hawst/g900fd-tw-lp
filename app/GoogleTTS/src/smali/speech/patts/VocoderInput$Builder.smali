.class public final Lspeech/patts/VocoderInput$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "VocoderInput.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/VocoderInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/VocoderInput;",
        "Lspeech/patts/VocoderInput$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/VocoderInput;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 306
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/VocoderInput$Builder;
    .locals 1

    .prologue
    .line 300
    invoke-static {}, Lspeech/patts/VocoderInput$Builder;->create()Lspeech/patts/VocoderInput$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/VocoderInput$Builder;
    .locals 3

    .prologue
    .line 309
    new-instance v0, Lspeech/patts/VocoderInput$Builder;

    invoke-direct {v0}, Lspeech/patts/VocoderInput$Builder;-><init>()V

    .line 310
    .local v0, "builder":Lspeech/patts/VocoderInput$Builder;
    new-instance v1, Lspeech/patts/VocoderInput;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/VocoderInput;-><init>(Lspeech/patts/VocoderInput$1;)V

    iput-object v1, v0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    .line 311
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 300
    invoke-virtual {p0}, Lspeech/patts/VocoderInput$Builder;->build()Lspeech/patts/VocoderInput;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/VocoderInput;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/VocoderInput$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 340
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    invoke-static {v0}, Lspeech/patts/VocoderInput$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 342
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/VocoderInput$Builder;->buildPartial()Lspeech/patts/VocoderInput;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/VocoderInput;
    .locals 3

    .prologue
    .line 355
    iget-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    if-nez v1, :cond_0

    .line 356
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 359
    :cond_0
    iget-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/VocoderInput;->access$300(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 360
    iget-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    iget-object v2, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/VocoderInput;->access$300(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/VocoderInput;->access$302(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;

    .line 363
    :cond_1
    iget-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/VocoderInput;->access$400(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 364
    iget-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    iget-object v2, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/VocoderInput;->access$400(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/VocoderInput;->access$402(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;

    .line 367
    :cond_2
    iget-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/VocoderInput;->access$500(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 368
    iget-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    iget-object v2, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/VocoderInput;->access$500(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/VocoderInput;->access$502(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;

    .line 371
    :cond_3
    iget-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/VocoderInput;->access$600(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 372
    iget-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    iget-object v2, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/VocoderInput;->access$600(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/VocoderInput;->access$602(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;

    .line 375
    :cond_4
    iget-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/VocoderInput;->access$700(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_5

    .line 376
    iget-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    iget-object v2, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/VocoderInput;->access$700(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/VocoderInput;->access$702(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;

    .line 379
    :cond_5
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    .line 380
    .local v0, "returnMe":Lspeech/patts/VocoderInput;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    .line 381
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 300
    invoke-virtual {p0}, Lspeech/patts/VocoderInput$Builder;->clone()Lspeech/patts/VocoderInput$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 300
    invoke-virtual {p0}, Lspeech/patts/VocoderInput$Builder;->clone()Lspeech/patts/VocoderInput$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 300
    invoke-virtual {p0}, Lspeech/patts/VocoderInput$Builder;->clone()Lspeech/patts/VocoderInput$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/VocoderInput$Builder;
    .locals 2

    .prologue
    .line 328
    invoke-static {}, Lspeech/patts/VocoderInput$Builder;->create()Lspeech/patts/VocoderInput$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    invoke-virtual {v0, v1}, Lspeech/patts/VocoderInput$Builder;->mergeFrom(Lspeech/patts/VocoderInput;)Lspeech/patts/VocoderInput$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    invoke-virtual {v0}, Lspeech/patts/VocoderInput;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 300
    check-cast p1, Lspeech/patts/VocoderInput;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/VocoderInput$Builder;->mergeFrom(Lspeech/patts/VocoderInput;)Lspeech/patts/VocoderInput$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/VocoderInput;)Lspeech/patts/VocoderInput$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/VocoderInput;

    .prologue
    .line 385
    invoke-static {}, Lspeech/patts/VocoderInput;->getDefaultInstance()Lspeech/patts/VocoderInput;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 422
    :cond_0
    :goto_0
    return-object p0

    .line 386
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/VocoderInput;->hasVoiced()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 387
    invoke-virtual {p1}, Lspeech/patts/VocoderInput;->getVoiced()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/VocoderInput$Builder;->setVoiced(Z)Lspeech/patts/VocoderInput$Builder;

    .line 389
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/VocoderInput;->hasLogF0()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 390
    invoke-virtual {p1}, Lspeech/patts/VocoderInput;->getLogF0()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/VocoderInput$Builder;->setLogF0(F)Lspeech/patts/VocoderInput$Builder;

    .line 392
    :cond_3
    # getter for: Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/VocoderInput;->access$300(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 393
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/VocoderInput;->access$300(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 394
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/VocoderInput;->access$302(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;

    .line 396
    :cond_4
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/VocoderInput;->access$300(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/VocoderInput;->amplitudes_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/VocoderInput;->access$300(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 398
    :cond_5
    # getter for: Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/VocoderInput;->access$400(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 399
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/VocoderInput;->access$400(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 400
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/VocoderInput;->access$402(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;

    .line 402
    :cond_6
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/VocoderInput;->access$400(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/VocoderInput;->aperiodicities_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/VocoderInput;->access$400(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 404
    :cond_7
    # getter for: Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/VocoderInput;->access$500(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 405
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/VocoderInput;->access$500(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 406
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/VocoderInput;->access$502(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;

    .line 408
    :cond_8
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/VocoderInput;->access$500(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/VocoderInput;->melCepstra_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/VocoderInput;->access$500(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 410
    :cond_9
    # getter for: Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/VocoderInput;->access$600(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 411
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/VocoderInput;->access$600(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 412
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/VocoderInput;->access$602(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;

    .line 414
    :cond_a
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/VocoderInput;->access$600(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/VocoderInput;->lineSpectralPairs_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/VocoderInput;->access$600(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 416
    :cond_b
    # getter for: Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/VocoderInput;->access$700(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/VocoderInput;->access$700(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 418
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/VocoderInput;->access$702(Lspeech/patts/VocoderInput;Ljava/util/List;)Ljava/util/List;

    .line 420
    :cond_c
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # getter for: Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/VocoderInput;->access$700(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/VocoderInput;->bandAperiodicities_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/VocoderInput;->access$700(Lspeech/patts/VocoderInput;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setLogF0(F)Lspeech/patts/VocoderInput$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 544
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/VocoderInput;->hasLogF0:Z
    invoke-static {v0, v1}, Lspeech/patts/VocoderInput;->access$1002(Lspeech/patts/VocoderInput;Z)Z

    .line 545
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # setter for: Lspeech/patts/VocoderInput;->logF0_:F
    invoke-static {v0, p1}, Lspeech/patts/VocoderInput;->access$1102(Lspeech/patts/VocoderInput;F)F

    .line 546
    return-object p0
.end method

.method public setVoiced(Z)Lspeech/patts/VocoderInput$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 526
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/VocoderInput;->hasVoiced:Z
    invoke-static {v0, v1}, Lspeech/patts/VocoderInput;->access$802(Lspeech/patts/VocoderInput;Z)Z

    .line 527
    iget-object v0, p0, Lspeech/patts/VocoderInput$Builder;->result:Lspeech/patts/VocoderInput;

    # setter for: Lspeech/patts/VocoderInput;->voiced_:Z
    invoke-static {v0, p1}, Lspeech/patts/VocoderInput;->access$902(Lspeech/patts/VocoderInput;Z)Z

    .line 528
    return-object p0
.end method
