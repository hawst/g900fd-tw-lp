.class public final Lspeech/patts/BaseType2;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "BaseType2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/BaseType2$1;,
        Lspeech/patts/BaseType2$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/BaseType2;


# instance fields
.field private hasName:Z

.field private memoizedSerializedSize:I

.field private modelId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private name_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 334
    new-instance v0, Lspeech/patts/BaseType2;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/BaseType2;-><init>(Z)V

    sput-object v0, Lspeech/patts/BaseType2;->defaultInstance:Lspeech/patts/BaseType2;

    .line 335
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 336
    sget-object v0, Lspeech/patts/BaseType2;->defaultInstance:Lspeech/patts/BaseType2;

    invoke-direct {v0}, Lspeech/patts/BaseType2;->initFields()V

    .line 337
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/BaseType2;->name_:Ljava/lang/String;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/BaseType2;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/BaseType2;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/BaseType2$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/BaseType2$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/BaseType2;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/BaseType2;->name_:Ljava/lang/String;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/BaseType2;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/BaseType2;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/BaseType2;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/BaseType2;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/BaseType2;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/BaseType2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/BaseType2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/BaseType2;->hasName:Z

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/BaseType2;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/BaseType2;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/BaseType2;->name_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/BaseType2;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/BaseType2;->defaultInstance:Lspeech/patts/BaseType2;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public static newBuilder()Lspeech/patts/BaseType2$Builder;
    .locals 1

    .prologue
    .line 149
    # invokes: Lspeech/patts/BaseType2$Builder;->create()Lspeech/patts/BaseType2$Builder;
    invoke-static {}, Lspeech/patts/BaseType2$Builder;->access$100()Lspeech/patts/BaseType2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/BaseType2;)Lspeech/patts/BaseType2$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/BaseType2;

    .prologue
    .line 152
    invoke-static {}, Lspeech/patts/BaseType2;->newBuilder()Lspeech/patts/BaseType2$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/BaseType2$Builder;->mergeFrom(Lspeech/patts/BaseType2;)Lspeech/patts/BaseType2$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/BaseType2;->getDefaultInstanceForType()Lspeech/patts/BaseType2;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/BaseType2;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/BaseType2;->defaultInstance:Lspeech/patts/BaseType2;

    return-object v0
.end method

.method public getModelIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/BaseType2;->modelId_:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lspeech/patts/BaseType2;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 61
    iget v3, p0, Lspeech/patts/BaseType2;->memoizedSerializedSize:I

    .line 62
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 79
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 64
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 66
    const/4 v0, 0x0

    .line 67
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/BaseType2;->getModelIdList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 68
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v0, v5

    .line 70
    goto :goto_1

    .line 71
    .end local v1    # "element":I
    :cond_1
    add-int/2addr v3, v0

    .line 72
    invoke-virtual {p0}, Lspeech/patts/BaseType2;->getModelIdList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 74
    invoke-virtual {p0}, Lspeech/patts/BaseType2;->hasName()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 75
    const/4 v5, 0x2

    invoke-virtual {p0}, Lspeech/patts/BaseType2;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 78
    :cond_2
    iput v3, p0, Lspeech/patts/BaseType2;->memoizedSerializedSize:I

    move v4, v3

    .line 79
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto :goto_0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lspeech/patts/BaseType2;->hasName:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lspeech/patts/BaseType2;->hasName:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 45
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/BaseType2;->toBuilder()Lspeech/patts/BaseType2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/BaseType2$Builder;
    .locals 1

    .prologue
    .line 154
    invoke-static {p0}, Lspeech/patts/BaseType2;->newBuilder(Lspeech/patts/BaseType2;)Lspeech/patts/BaseType2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lspeech/patts/BaseType2;->getSerializedSize()I

    .line 51
    invoke-virtual {p0}, Lspeech/patts/BaseType2;->getModelIdList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 52
    .local v0, "element":I
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    goto :goto_0

    .line 54
    .end local v0    # "element":I
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/BaseType2;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 55
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/BaseType2;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 57
    :cond_1
    return-void
.end method
