.class public final Lspeech/patts/MergeCompounds;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "MergeCompounds.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/MergeCompounds$1;,
        Lspeech/patts/MergeCompounds$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/MergeCompounds;


# instance fields
.field private hasOriginal:Z

.field private hasReplacement:Z

.field private memoizedSerializedSize:I

.field private original_:Ljava/lang/String;

.field private replacement_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 296
    new-instance v0, Lspeech/patts/MergeCompounds;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/MergeCompounds;-><init>(Z)V

    sput-object v0, Lspeech/patts/MergeCompounds;->defaultInstance:Lspeech/patts/MergeCompounds;

    .line 297
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 298
    sget-object v0, Lspeech/patts/MergeCompounds;->defaultInstance:Lspeech/patts/MergeCompounds;

    invoke-direct {v0}, Lspeech/patts/MergeCompounds;->initFields()V

    .line 299
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/MergeCompounds;->original_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/MergeCompounds;->replacement_:Ljava/lang/String;

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/MergeCompounds;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/MergeCompounds;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/MergeCompounds$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/MergeCompounds$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/MergeCompounds;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/MergeCompounds;->original_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/MergeCompounds;->replacement_:Ljava/lang/String;

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/MergeCompounds;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lspeech/patts/MergeCompounds;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/MergeCompounds;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/MergeCompounds;->hasOriginal:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/MergeCompounds;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/MergeCompounds;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/MergeCompounds;->original_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/MergeCompounds;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/MergeCompounds;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/MergeCompounds;->hasReplacement:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/MergeCompounds;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/MergeCompounds;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/MergeCompounds;->replacement_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/MergeCompounds;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/MergeCompounds;->defaultInstance:Lspeech/patts/MergeCompounds;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public static newBuilder()Lspeech/patts/MergeCompounds$Builder;
    .locals 1

    .prologue
    .line 140
    # invokes: Lspeech/patts/MergeCompounds$Builder;->create()Lspeech/patts/MergeCompounds$Builder;
    invoke-static {}, Lspeech/patts/MergeCompounds$Builder;->access$100()Lspeech/patts/MergeCompounds$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/MergeCompounds;)Lspeech/patts/MergeCompounds$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/MergeCompounds;

    .prologue
    .line 143
    invoke-static {}, Lspeech/patts/MergeCompounds;->newBuilder()Lspeech/patts/MergeCompounds$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/MergeCompounds$Builder;->mergeFrom(Lspeech/patts/MergeCompounds;)Lspeech/patts/MergeCompounds$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/MergeCompounds;->getDefaultInstanceForType()Lspeech/patts/MergeCompounds;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/MergeCompounds;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/MergeCompounds;->defaultInstance:Lspeech/patts/MergeCompounds;

    return-object v0
.end method

.method public getOriginal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/MergeCompounds;->original_:Ljava/lang/String;

    return-object v0
.end method

.method public getReplacement()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lspeech/patts/MergeCompounds;->replacement_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 57
    iget v0, p0, Lspeech/patts/MergeCompounds;->memoizedSerializedSize:I

    .line 58
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 70
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 60
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 61
    invoke-virtual {p0}, Lspeech/patts/MergeCompounds;->hasOriginal()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/MergeCompounds;->getOriginal()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 65
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/MergeCompounds;->hasReplacement()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 66
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/MergeCompounds;->getReplacement()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 69
    :cond_2
    iput v0, p0, Lspeech/patts/MergeCompounds;->memoizedSerializedSize:I

    move v1, v0

    .line 70
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasOriginal()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/MergeCompounds;->hasOriginal:Z

    return v0
.end method

.method public hasReplacement()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/MergeCompounds;->hasReplacement:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 39
    iget-boolean v1, p0, Lspeech/patts/MergeCompounds;->hasOriginal:Z

    if-nez v1, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v0

    .line 40
    :cond_1
    iget-boolean v1, p0, Lspeech/patts/MergeCompounds;->hasReplacement:Z

    if-eqz v1, :cond_0

    .line 41
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/MergeCompounds;->toBuilder()Lspeech/patts/MergeCompounds$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/MergeCompounds$Builder;
    .locals 1

    .prologue
    .line 145
    invoke-static {p0}, Lspeech/patts/MergeCompounds;->newBuilder(Lspeech/patts/MergeCompounds;)Lspeech/patts/MergeCompounds$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lspeech/patts/MergeCompounds;->getSerializedSize()I

    .line 47
    invoke-virtual {p0}, Lspeech/patts/MergeCompounds;->hasOriginal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/MergeCompounds;->getOriginal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 50
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/MergeCompounds;->hasReplacement()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/MergeCompounds;->getReplacement()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 53
    :cond_1
    return-void
.end method
