.class public final enum Lspeech/patts/Word$Type;
.super Ljava/lang/Enum;
.source "Word.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Word;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/Word$Type;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/Word$Type;

.field public static final enum EXL:Lspeech/patts/Word$Type;

.field public static final enum HOMOGRAPH:Lspeech/patts/Word$Type;

.field public static final enum WORD:Lspeech/patts/Word$Type;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/Word$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 24
    new-instance v0, Lspeech/patts/Word$Type;

    const-string v1, "WORD"

    invoke-direct {v0, v1, v4, v4, v3}, Lspeech/patts/Word$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Word$Type;->WORD:Lspeech/patts/Word$Type;

    .line 25
    new-instance v0, Lspeech/patts/Word$Type;

    const-string v1, "HOMOGRAPH"

    invoke-direct {v0, v1, v3, v3, v6}, Lspeech/patts/Word$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Word$Type;->HOMOGRAPH:Lspeech/patts/Word$Type;

    .line 26
    new-instance v0, Lspeech/patts/Word$Type;

    const-string v1, "EXL"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v5, v2}, Lspeech/patts/Word$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Word$Type;->EXL:Lspeech/patts/Word$Type;

    .line 22
    new-array v0, v6, [Lspeech/patts/Word$Type;

    sget-object v1, Lspeech/patts/Word$Type;->WORD:Lspeech/patts/Word$Type;

    aput-object v1, v0, v4

    sget-object v1, Lspeech/patts/Word$Type;->HOMOGRAPH:Lspeech/patts/Word$Type;

    aput-object v1, v0, v3

    sget-object v1, Lspeech/patts/Word$Type;->EXL:Lspeech/patts/Word$Type;

    aput-object v1, v0, v5

    sput-object v0, Lspeech/patts/Word$Type;->$VALUES:[Lspeech/patts/Word$Type;

    .line 46
    new-instance v0, Lspeech/patts/Word$Type$1;

    invoke-direct {v0}, Lspeech/patts/Word$Type$1;-><init>()V

    sput-object v0, Lspeech/patts/Word$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput p3, p0, Lspeech/patts/Word$Type;->index:I

    .line 57
    iput p4, p0, Lspeech/patts/Word$Type;->value:I

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/Word$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lspeech/patts/Word$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/Word$Type;

    return-object v0
.end method

.method public static values()[Lspeech/patts/Word$Type;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lspeech/patts/Word$Type;->$VALUES:[Lspeech/patts/Word$Type;

    invoke-virtual {v0}, [Lspeech/patts/Word$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/Word$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lspeech/patts/Word$Type;->value:I

    return v0
.end method
