.class public final Lspeech/patts/UnitSlot;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "UnitSlot.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/UnitSlot$1;,
        Lspeech/patts/UnitSlot$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/UnitSlot;


# instance fields
.field private bestBackIndex_:I

.field private hasBestBackIndex:Z

.field private hasRunningTotal:Z

.field private hasTargetCost:Z

.field private hasUnitId:Z

.field private memoizedSerializedSize:I

.field private runningTotal_:F

.field private targetCost_:F

.field private unitId_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 366
    new-instance v0, Lspeech/patts/UnitSlot;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/UnitSlot;-><init>(Z)V

    sput-object v0, Lspeech/patts/UnitSlot;->defaultInstance:Lspeech/patts/UnitSlot;

    .line 367
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 368
    sget-object v0, Lspeech/patts/UnitSlot;->defaultInstance:Lspeech/patts/UnitSlot;

    invoke-direct {v0}, Lspeech/patts/UnitSlot;->initFields()V

    .line 369
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lspeech/patts/UnitSlot;->targetCost_:F

    .line 32
    iput v0, p0, Lspeech/patts/UnitSlot;->runningTotal_:F

    .line 39
    iput v1, p0, Lspeech/patts/UnitSlot;->bestBackIndex_:I

    .line 46
    iput v1, p0, Lspeech/patts/UnitSlot;->unitId_:I

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/UnitSlot;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/UnitSlot;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/UnitSlot$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/UnitSlot$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/UnitSlot;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lspeech/patts/UnitSlot;->targetCost_:F

    .line 32
    iput v0, p0, Lspeech/patts/UnitSlot;->runningTotal_:F

    .line 39
    iput v1, p0, Lspeech/patts/UnitSlot;->bestBackIndex_:I

    .line 46
    iput v1, p0, Lspeech/patts/UnitSlot;->unitId_:I

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/UnitSlot;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/UnitSlot;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UnitSlot;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/UnitSlot;->unitId_:I

    return p1
.end method

.method static synthetic access$302(Lspeech/patts/UnitSlot;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UnitSlot;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/UnitSlot;->hasTargetCost:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/UnitSlot;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UnitSlot;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/UnitSlot;->targetCost_:F

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/UnitSlot;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UnitSlot;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/UnitSlot;->hasRunningTotal:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/UnitSlot;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UnitSlot;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/UnitSlot;->runningTotal_:F

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/UnitSlot;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UnitSlot;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/UnitSlot;->hasBestBackIndex:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/UnitSlot;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UnitSlot;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/UnitSlot;->bestBackIndex_:I

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/UnitSlot;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UnitSlot;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/UnitSlot;->hasUnitId:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/UnitSlot;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/UnitSlot;->defaultInstance:Lspeech/patts/UnitSlot;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public static newBuilder()Lspeech/patts/UnitSlot$Builder;
    .locals 1

    .prologue
    .line 166
    # invokes: Lspeech/patts/UnitSlot$Builder;->create()Lspeech/patts/UnitSlot$Builder;
    invoke-static {}, Lspeech/patts/UnitSlot$Builder;->access$100()Lspeech/patts/UnitSlot$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/UnitSlot;)Lspeech/patts/UnitSlot$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/UnitSlot;

    .prologue
    .line 169
    invoke-static {}, Lspeech/patts/UnitSlot;->newBuilder()Lspeech/patts/UnitSlot$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/UnitSlot$Builder;->mergeFrom(Lspeech/patts/UnitSlot;)Lspeech/patts/UnitSlot$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBestBackIndex()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lspeech/patts/UnitSlot;->bestBackIndex_:I

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->getDefaultInstanceForType()Lspeech/patts/UnitSlot;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/UnitSlot;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/UnitSlot;->defaultInstance:Lspeech/patts/UnitSlot;

    return-object v0
.end method

.method public getRunningTotal()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lspeech/patts/UnitSlot;->runningTotal_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 75
    iget v0, p0, Lspeech/patts/UnitSlot;->memoizedSerializedSize:I

    .line 76
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 96
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 78
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 79
    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->hasTargetCost()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 80
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->getTargetCost()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 83
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->hasRunningTotal()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 84
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->getRunningTotal()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 87
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->hasBestBackIndex()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 88
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->getBestBackIndex()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 91
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->hasUnitId()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 92
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->getUnitId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 95
    :cond_4
    iput v0, p0, Lspeech/patts/UnitSlot;->memoizedSerializedSize:I

    move v1, v0

    .line 96
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getTargetCost()F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lspeech/patts/UnitSlot;->targetCost_:F

    return v0
.end method

.method public getUnitId()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lspeech/patts/UnitSlot;->unitId_:I

    return v0
.end method

.method public hasBestBackIndex()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lspeech/patts/UnitSlot;->hasBestBackIndex:Z

    return v0
.end method

.method public hasRunningTotal()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/UnitSlot;->hasRunningTotal:Z

    return v0
.end method

.method public hasTargetCost()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/UnitSlot;->hasTargetCost:Z

    return v0
.end method

.method public hasUnitId()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lspeech/patts/UnitSlot;->hasUnitId:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->toBuilder()Lspeech/patts/UnitSlot$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/UnitSlot$Builder;
    .locals 1

    .prologue
    .line 171
    invoke-static {p0}, Lspeech/patts/UnitSlot;->newBuilder(Lspeech/patts/UnitSlot;)Lspeech/patts/UnitSlot$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->getSerializedSize()I

    .line 59
    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->hasTargetCost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->getTargetCost()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 62
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->hasRunningTotal()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->getRunningTotal()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 65
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->hasBestBackIndex()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->getBestBackIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 68
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->hasUnitId()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    const/4 v0, 0x4

    invoke-virtual {p0}, Lspeech/patts/UnitSlot;->getUnitId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 71
    :cond_3
    return-void
.end method
