.class public final Lspeech/patts/DependencyData$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "DependencyData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/DependencyData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/DependencyData;",
        "Lspeech/patts/DependencyData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/DependencyData;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 305
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/DependencyData$Builder;
    .locals 1

    .prologue
    .line 299
    invoke-static {}, Lspeech/patts/DependencyData$Builder;->create()Lspeech/patts/DependencyData$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/DependencyData$Builder;
    .locals 3

    .prologue
    .line 308
    new-instance v0, Lspeech/patts/DependencyData$Builder;

    invoke-direct {v0}, Lspeech/patts/DependencyData$Builder;-><init>()V

    .line 309
    .local v0, "builder":Lspeech/patts/DependencyData$Builder;
    new-instance v1, Lspeech/patts/DependencyData;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/DependencyData;-><init>(Lspeech/patts/DependencyData$1;)V

    iput-object v1, v0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    .line 310
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0}, Lspeech/patts/DependencyData$Builder;->build()Lspeech/patts/DependencyData;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/DependencyData;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/DependencyData$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    invoke-static {v0}, Lspeech/patts/DependencyData$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 341
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/DependencyData$Builder;->buildPartial()Lspeech/patts/DependencyData;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/DependencyData;
    .locals 3

    .prologue
    .line 354
    iget-object v1, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    if-nez v1, :cond_0

    .line 355
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 358
    :cond_0
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    .line 359
    .local v0, "returnMe":Lspeech/patts/DependencyData;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    .line 360
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0}, Lspeech/patts/DependencyData$Builder;->clone()Lspeech/patts/DependencyData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0}, Lspeech/patts/DependencyData$Builder;->clone()Lspeech/patts/DependencyData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 299
    invoke-virtual {p0}, Lspeech/patts/DependencyData$Builder;->clone()Lspeech/patts/DependencyData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/DependencyData$Builder;
    .locals 2

    .prologue
    .line 327
    invoke-static {}, Lspeech/patts/DependencyData$Builder;->create()Lspeech/patts/DependencyData$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    invoke-virtual {v0, v1}, Lspeech/patts/DependencyData$Builder;->mergeFrom(Lspeech/patts/DependencyData;)Lspeech/patts/DependencyData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    invoke-virtual {v0}, Lspeech/patts/DependencyData;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 299
    check-cast p1, Lspeech/patts/DependencyData;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/DependencyData$Builder;->mergeFrom(Lspeech/patts/DependencyData;)Lspeech/patts/DependencyData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/DependencyData;)Lspeech/patts/DependencyData$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/DependencyData;

    .prologue
    .line 364
    invoke-static {}, Lspeech/patts/DependencyData;->getDefaultInstance()Lspeech/patts/DependencyData;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 404
    :cond_0
    :goto_0
    return-object p0

    .line 365
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasHead()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 366
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getHead()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setHead(I)Lspeech/patts/DependencyData$Builder;

    .line 368
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasLabel()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 369
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setLabel(Ljava/lang/String;)Lspeech/patts/DependencyData$Builder;

    .line 371
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasDepth()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 372
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getDepth()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setDepth(I)Lspeech/patts/DependencyData$Builder;

    .line 374
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasOffsetFromHead()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 375
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getOffsetFromHead()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setOffsetFromHead(I)Lspeech/patts/DependencyData$Builder;

    .line 377
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasSizeToLeftLowerBound()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 378
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getSizeToLeftLowerBound()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setSizeToLeftLowerBound(I)Lspeech/patts/DependencyData$Builder;

    .line 380
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasSizeToLeftUpperBound()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 381
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getSizeToLeftUpperBound()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setSizeToLeftUpperBound(I)Lspeech/patts/DependencyData$Builder;

    .line 383
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasSizeToRightLowerBound()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 384
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getSizeToRightLowerBound()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setSizeToRightLowerBound(I)Lspeech/patts/DependencyData$Builder;

    .line 386
    :cond_8
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasSizeToRightUpperBound()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 387
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getSizeToRightUpperBound()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setSizeToRightUpperBound(I)Lspeech/patts/DependencyData$Builder;

    .line 389
    :cond_9
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasCrossingLinksToRight()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 390
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getCrossingLinksToRight()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setCrossingLinksToRight(I)Lspeech/patts/DependencyData$Builder;

    .line 392
    :cond_a
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasBoundaryStrengthToRight()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 393
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getBoundaryStrengthToRight()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setBoundaryStrengthToRight(F)Lspeech/patts/DependencyData$Builder;

    .line 395
    :cond_b
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasLinkStepsToRight()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 396
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getLinkStepsToRight()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setLinkStepsToRight(I)Lspeech/patts/DependencyData$Builder;

    .line 398
    :cond_c
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasWeightedCrossingLinksToRight()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 399
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getWeightedCrossingLinksToRight()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setWeightedCrossingLinksToRight(F)Lspeech/patts/DependencyData$Builder;

    .line 401
    :cond_d
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->hasWeightedBoundaryStrengthToRight()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    invoke-virtual {p1}, Lspeech/patts/DependencyData;->getWeightedBoundaryStrengthToRight()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/DependencyData$Builder;->setWeightedBoundaryStrengthToRight(F)Lspeech/patts/DependencyData$Builder;

    goto/16 :goto_0
.end method

.method public setBoundaryStrengthToRight(F)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 652
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasBoundaryStrengthToRight:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$2102(Lspeech/patts/DependencyData;Z)Z

    .line 653
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->boundaryStrengthToRight_:F
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$2202(Lspeech/patts/DependencyData;F)F

    .line 654
    return-object p0
.end method

.method public setCrossingLinksToRight(I)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 634
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasCrossingLinksToRight:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$1902(Lspeech/patts/DependencyData;Z)Z

    .line 635
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->crossingLinksToRight_:I
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$2002(Lspeech/patts/DependencyData;I)I

    .line 636
    return-object p0
.end method

.method public setDepth(I)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 526
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasDepth:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$702(Lspeech/patts/DependencyData;Z)Z

    .line 527
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->depth_:I
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$802(Lspeech/patts/DependencyData;I)I

    .line 528
    return-object p0
.end method

.method public setHead(I)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 487
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasHead:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$302(Lspeech/patts/DependencyData;Z)Z

    .line 488
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->head_:I
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$402(Lspeech/patts/DependencyData;I)I

    .line 489
    return-object p0
.end method

.method public setLabel(Ljava/lang/String;)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 505
    if-nez p1, :cond_0

    .line 506
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 508
    :cond_0
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasLabel:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$502(Lspeech/patts/DependencyData;Z)Z

    .line 509
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->label_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$602(Lspeech/patts/DependencyData;Ljava/lang/String;)Ljava/lang/String;

    .line 510
    return-object p0
.end method

.method public setLinkStepsToRight(I)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 670
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasLinkStepsToRight:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$2302(Lspeech/patts/DependencyData;Z)Z

    .line 671
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->linkStepsToRight_:I
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$2402(Lspeech/patts/DependencyData;I)I

    .line 672
    return-object p0
.end method

.method public setOffsetFromHead(I)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 544
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasOffsetFromHead:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$902(Lspeech/patts/DependencyData;Z)Z

    .line 545
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->offsetFromHead_:I
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$1002(Lspeech/patts/DependencyData;I)I

    .line 546
    return-object p0
.end method

.method public setSizeToLeftLowerBound(I)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 562
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasSizeToLeftLowerBound:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$1102(Lspeech/patts/DependencyData;Z)Z

    .line 563
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->sizeToLeftLowerBound_:I
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$1202(Lspeech/patts/DependencyData;I)I

    .line 564
    return-object p0
.end method

.method public setSizeToLeftUpperBound(I)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 580
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasSizeToLeftUpperBound:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$1302(Lspeech/patts/DependencyData;Z)Z

    .line 581
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->sizeToLeftUpperBound_:I
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$1402(Lspeech/patts/DependencyData;I)I

    .line 582
    return-object p0
.end method

.method public setSizeToRightLowerBound(I)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 598
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasSizeToRightLowerBound:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$1502(Lspeech/patts/DependencyData;Z)Z

    .line 599
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->sizeToRightLowerBound_:I
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$1602(Lspeech/patts/DependencyData;I)I

    .line 600
    return-object p0
.end method

.method public setSizeToRightUpperBound(I)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 616
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasSizeToRightUpperBound:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$1702(Lspeech/patts/DependencyData;Z)Z

    .line 617
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->sizeToRightUpperBound_:I
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$1802(Lspeech/patts/DependencyData;I)I

    .line 618
    return-object p0
.end method

.method public setWeightedBoundaryStrengthToRight(F)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 706
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasWeightedBoundaryStrengthToRight:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$2702(Lspeech/patts/DependencyData;Z)Z

    .line 707
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->weightedBoundaryStrengthToRight_:F
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$2802(Lspeech/patts/DependencyData;F)F

    .line 708
    return-object p0
.end method

.method public setWeightedCrossingLinksToRight(F)Lspeech/patts/DependencyData$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 688
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/DependencyData;->hasWeightedCrossingLinksToRight:Z
    invoke-static {v0, v1}, Lspeech/patts/DependencyData;->access$2502(Lspeech/patts/DependencyData;Z)Z

    .line 689
    iget-object v0, p0, Lspeech/patts/DependencyData$Builder;->result:Lspeech/patts/DependencyData;

    # setter for: Lspeech/patts/DependencyData;->weightedCrossingLinksToRight_:F
    invoke-static {v0, p1}, Lspeech/patts/DependencyData;->access$2602(Lspeech/patts/DependencyData;F)F

    .line 690
    return-object p0
.end method
