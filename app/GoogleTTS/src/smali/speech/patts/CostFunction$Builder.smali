.class public final Lspeech/patts/CostFunction$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "CostFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/CostFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/CostFunction;",
        "Lspeech/patts/CostFunction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/CostFunction;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/CostFunction$Builder;
    .locals 1

    .prologue
    .line 309
    invoke-static {}, Lspeech/patts/CostFunction$Builder;->create()Lspeech/patts/CostFunction$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/CostFunction$Builder;
    .locals 3

    .prologue
    .line 318
    new-instance v0, Lspeech/patts/CostFunction$Builder;

    invoke-direct {v0}, Lspeech/patts/CostFunction$Builder;-><init>()V

    .line 319
    .local v0, "builder":Lspeech/patts/CostFunction$Builder;
    new-instance v1, Lspeech/patts/CostFunction;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/CostFunction;-><init>(Lspeech/patts/CostFunction$1;)V

    iput-object v1, v0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    .line 320
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 309
    invoke-virtual {p0}, Lspeech/patts/CostFunction$Builder;->build()Lspeech/patts/CostFunction;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/CostFunction;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/CostFunction$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 349
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    invoke-static {v0}, Lspeech/patts/CostFunction$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 351
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/CostFunction$Builder;->buildPartial()Lspeech/patts/CostFunction;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/CostFunction;
    .locals 3

    .prologue
    .line 364
    iget-object v1, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    if-nez v1, :cond_0

    .line 365
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 368
    :cond_0
    iget-object v1, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # getter for: Lspeech/patts/CostFunction;->weight_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/CostFunction;->access$300(Lspeech/patts/CostFunction;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 369
    iget-object v1, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    iget-object v2, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # getter for: Lspeech/patts/CostFunction;->weight_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/CostFunction;->access$300(Lspeech/patts/CostFunction;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/CostFunction;->weight_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/CostFunction;->access$302(Lspeech/patts/CostFunction;Ljava/util/List;)Ljava/util/List;

    .line 372
    :cond_1
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    .line 373
    .local v0, "returnMe":Lspeech/patts/CostFunction;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    .line 374
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 309
    invoke-virtual {p0}, Lspeech/patts/CostFunction$Builder;->clone()Lspeech/patts/CostFunction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 309
    invoke-virtual {p0}, Lspeech/patts/CostFunction$Builder;->clone()Lspeech/patts/CostFunction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 309
    invoke-virtual {p0}, Lspeech/patts/CostFunction$Builder;->clone()Lspeech/patts/CostFunction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/CostFunction$Builder;
    .locals 2

    .prologue
    .line 337
    invoke-static {}, Lspeech/patts/CostFunction$Builder;->create()Lspeech/patts/CostFunction$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    invoke-virtual {v0, v1}, Lspeech/patts/CostFunction$Builder;->mergeFrom(Lspeech/patts/CostFunction;)Lspeech/patts/CostFunction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    invoke-virtual {v0}, Lspeech/patts/CostFunction;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeAcoustic(Lspeech/patts/AcousticFeature;)Lspeech/patts/CostFunction$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/AcousticFeature;

    .prologue
    .line 613
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    invoke-virtual {v0}, Lspeech/patts/CostFunction;->hasAcoustic()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # getter for: Lspeech/patts/CostFunction;->acoustic_:Lspeech/patts/AcousticFeature;
    invoke-static {v0}, Lspeech/patts/CostFunction;->access$1100(Lspeech/patts/CostFunction;)Lspeech/patts/AcousticFeature;

    move-result-object v0

    invoke-static {}, Lspeech/patts/AcousticFeature;->getDefaultInstance()Lspeech/patts/AcousticFeature;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 615
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    iget-object v1, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # getter for: Lspeech/patts/CostFunction;->acoustic_:Lspeech/patts/AcousticFeature;
    invoke-static {v1}, Lspeech/patts/CostFunction;->access$1100(Lspeech/patts/CostFunction;)Lspeech/patts/AcousticFeature;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/AcousticFeature;->newBuilder(Lspeech/patts/AcousticFeature;)Lspeech/patts/AcousticFeature$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/AcousticFeature$Builder;->mergeFrom(Lspeech/patts/AcousticFeature;)Lspeech/patts/AcousticFeature$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/AcousticFeature$Builder;->buildPartial()Lspeech/patts/AcousticFeature;

    move-result-object v1

    # setter for: Lspeech/patts/CostFunction;->acoustic_:Lspeech/patts/AcousticFeature;
    invoke-static {v0, v1}, Lspeech/patts/CostFunction;->access$1102(Lspeech/patts/CostFunction;Lspeech/patts/AcousticFeature;)Lspeech/patts/AcousticFeature;

    .line 620
    :goto_0
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/CostFunction;->hasAcoustic:Z
    invoke-static {v0, v1}, Lspeech/patts/CostFunction;->access$1002(Lspeech/patts/CostFunction;Z)Z

    .line 621
    return-object p0

    .line 618
    :cond_0
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # setter for: Lspeech/patts/CostFunction;->acoustic_:Lspeech/patts/AcousticFeature;
    invoke-static {v0, p1}, Lspeech/patts/CostFunction;->access$1102(Lspeech/patts/CostFunction;Lspeech/patts/AcousticFeature;)Lspeech/patts/AcousticFeature;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 309
    check-cast p1, Lspeech/patts/CostFunction;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/CostFunction$Builder;->mergeFrom(Lspeech/patts/CostFunction;)Lspeech/patts/CostFunction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/CostFunction;)Lspeech/patts/CostFunction$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/CostFunction;

    .prologue
    .line 378
    invoke-static {}, Lspeech/patts/CostFunction;->getDefaultInstance()Lspeech/patts/CostFunction;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 400
    :cond_0
    :goto_0
    return-object p0

    .line 379
    :cond_1
    # getter for: Lspeech/patts/CostFunction;->weight_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunction;->access$300(Lspeech/patts/CostFunction;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 380
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # getter for: Lspeech/patts/CostFunction;->weight_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunction;->access$300(Lspeech/patts/CostFunction;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 381
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/CostFunction;->weight_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/CostFunction;->access$302(Lspeech/patts/CostFunction;Ljava/util/List;)Ljava/util/List;

    .line 383
    :cond_2
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # getter for: Lspeech/patts/CostFunction;->weight_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunction;->access$300(Lspeech/patts/CostFunction;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/CostFunction;->weight_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunction;->access$300(Lspeech/patts/CostFunction;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 385
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/CostFunction;->hasCtype()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 386
    invoke-virtual {p1}, Lspeech/patts/CostFunction;->getCtype()Lspeech/patts/CostFunction$CostFunctionType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/CostFunction$Builder;->setCtype(Lspeech/patts/CostFunction$CostFunctionType;)Lspeech/patts/CostFunction$Builder;

    .line 388
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/CostFunction;->hasFtype()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 389
    invoke-virtual {p1}, Lspeech/patts/CostFunction;->getFtype()Lspeech/patts/CostFunction$ValueType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/CostFunction$Builder;->setFtype(Lspeech/patts/CostFunction$ValueType;)Lspeech/patts/CostFunction$Builder;

    .line 391
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/CostFunction;->hasLinguistic()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 392
    invoke-virtual {p1}, Lspeech/patts/CostFunction;->getLinguistic()Lspeech/patts/NavigationFeature;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/CostFunction$Builder;->mergeLinguistic(Lspeech/patts/NavigationFeature;)Lspeech/patts/CostFunction$Builder;

    .line 394
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/CostFunction;->hasAcoustic()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 395
    invoke-virtual {p1}, Lspeech/patts/CostFunction;->getAcoustic()Lspeech/patts/AcousticFeature;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/CostFunction$Builder;->mergeAcoustic(Lspeech/patts/AcousticFeature;)Lspeech/patts/CostFunction$Builder;

    .line 397
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/CostFunction;->hasQuestion()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    invoke-virtual {p1}, Lspeech/patts/CostFunction;->getQuestion()Lspeech/patts/Question;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/CostFunction$Builder;->mergeQuestion(Lspeech/patts/Question;)Lspeech/patts/CostFunction$Builder;

    goto :goto_0
.end method

.method public mergeLinguistic(Lspeech/patts/NavigationFeature;)Lspeech/patts/CostFunction$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/NavigationFeature;

    .prologue
    .line 576
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    invoke-virtual {v0}, Lspeech/patts/CostFunction;->hasLinguistic()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # getter for: Lspeech/patts/CostFunction;->linguistic_:Lspeech/patts/NavigationFeature;
    invoke-static {v0}, Lspeech/patts/CostFunction;->access$900(Lspeech/patts/CostFunction;)Lspeech/patts/NavigationFeature;

    move-result-object v0

    invoke-static {}, Lspeech/patts/NavigationFeature;->getDefaultInstance()Lspeech/patts/NavigationFeature;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 578
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    iget-object v1, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # getter for: Lspeech/patts/CostFunction;->linguistic_:Lspeech/patts/NavigationFeature;
    invoke-static {v1}, Lspeech/patts/CostFunction;->access$900(Lspeech/patts/CostFunction;)Lspeech/patts/NavigationFeature;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/NavigationFeature;->newBuilder(Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/NavigationFeature$Builder;->mergeFrom(Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/NavigationFeature$Builder;->buildPartial()Lspeech/patts/NavigationFeature;

    move-result-object v1

    # setter for: Lspeech/patts/CostFunction;->linguistic_:Lspeech/patts/NavigationFeature;
    invoke-static {v0, v1}, Lspeech/patts/CostFunction;->access$902(Lspeech/patts/CostFunction;Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature;

    .line 583
    :goto_0
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/CostFunction;->hasLinguistic:Z
    invoke-static {v0, v1}, Lspeech/patts/CostFunction;->access$802(Lspeech/patts/CostFunction;Z)Z

    .line 584
    return-object p0

    .line 581
    :cond_0
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # setter for: Lspeech/patts/CostFunction;->linguistic_:Lspeech/patts/NavigationFeature;
    invoke-static {v0, p1}, Lspeech/patts/CostFunction;->access$902(Lspeech/patts/CostFunction;Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature;

    goto :goto_0
.end method

.method public mergeQuestion(Lspeech/patts/Question;)Lspeech/patts/CostFunction$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/Question;

    .prologue
    .line 650
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    invoke-virtual {v0}, Lspeech/patts/CostFunction;->hasQuestion()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # getter for: Lspeech/patts/CostFunction;->question_:Lspeech/patts/Question;
    invoke-static {v0}, Lspeech/patts/CostFunction;->access$1300(Lspeech/patts/CostFunction;)Lspeech/patts/Question;

    move-result-object v0

    invoke-static {}, Lspeech/patts/Question;->getDefaultInstance()Lspeech/patts/Question;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 652
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    iget-object v1, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # getter for: Lspeech/patts/CostFunction;->question_:Lspeech/patts/Question;
    invoke-static {v1}, Lspeech/patts/CostFunction;->access$1300(Lspeech/patts/CostFunction;)Lspeech/patts/Question;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/Question;->newBuilder(Lspeech/patts/Question;)Lspeech/patts/Question$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/Question$Builder;->mergeFrom(Lspeech/patts/Question;)Lspeech/patts/Question$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/Question$Builder;->buildPartial()Lspeech/patts/Question;

    move-result-object v1

    # setter for: Lspeech/patts/CostFunction;->question_:Lspeech/patts/Question;
    invoke-static {v0, v1}, Lspeech/patts/CostFunction;->access$1302(Lspeech/patts/CostFunction;Lspeech/patts/Question;)Lspeech/patts/Question;

    .line 657
    :goto_0
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/CostFunction;->hasQuestion:Z
    invoke-static {v0, v1}, Lspeech/patts/CostFunction;->access$1202(Lspeech/patts/CostFunction;Z)Z

    .line 658
    return-object p0

    .line 655
    :cond_0
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # setter for: Lspeech/patts/CostFunction;->question_:Lspeech/patts/Question;
    invoke-static {v0, p1}, Lspeech/patts/CostFunction;->access$1302(Lspeech/patts/CostFunction;Lspeech/patts/Question;)Lspeech/patts/Question;

    goto :goto_0
.end method

.method public setCtype(Lspeech/patts/CostFunction$CostFunctionType;)Lspeech/patts/CostFunction$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/CostFunction$CostFunctionType;

    .prologue
    .line 521
    if-nez p1, :cond_0

    .line 522
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 524
    :cond_0
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/CostFunction;->hasCtype:Z
    invoke-static {v0, v1}, Lspeech/patts/CostFunction;->access$402(Lspeech/patts/CostFunction;Z)Z

    .line 525
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # setter for: Lspeech/patts/CostFunction;->ctype_:Lspeech/patts/CostFunction$CostFunctionType;
    invoke-static {v0, p1}, Lspeech/patts/CostFunction;->access$502(Lspeech/patts/CostFunction;Lspeech/patts/CostFunction$CostFunctionType;)Lspeech/patts/CostFunction$CostFunctionType;

    .line 526
    return-object p0
.end method

.method public setFtype(Lspeech/patts/CostFunction$ValueType;)Lspeech/patts/CostFunction$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/CostFunction$ValueType;

    .prologue
    .line 542
    if-nez p1, :cond_0

    .line 543
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 545
    :cond_0
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/CostFunction;->hasFtype:Z
    invoke-static {v0, v1}, Lspeech/patts/CostFunction;->access$602(Lspeech/patts/CostFunction;Z)Z

    .line 546
    iget-object v0, p0, Lspeech/patts/CostFunction$Builder;->result:Lspeech/patts/CostFunction;

    # setter for: Lspeech/patts/CostFunction;->ftype_:Lspeech/patts/CostFunction$ValueType;
    invoke-static {v0, p1}, Lspeech/patts/CostFunction;->access$702(Lspeech/patts/CostFunction;Lspeech/patts/CostFunction$ValueType;)Lspeech/patts/CostFunction$ValueType;

    .line 547
    return-object p0
.end method
