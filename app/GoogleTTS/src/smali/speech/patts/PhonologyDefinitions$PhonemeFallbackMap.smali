.class public final Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PhonologyDefinitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PhonologyDefinitions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PhonemeFallbackMap"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;


# instance fields
.field private group_:Ljava/lang/String;

.field private hasGroup:Z

.field private hasPhoneme:Z

.field private memoizedSerializedSize:I

.field private phoneme_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 354
    new-instance v0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;-><init>(Z)V

    sput-object v0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->defaultInstance:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    .line 355
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 356
    sget-object v0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->defaultInstance:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    invoke-direct {v0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->initFields()V

    .line 357
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 83
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->phoneme_:Ljava/lang/String;

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->group_:Ljava/lang/String;

    .line 113
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->memoizedSerializedSize:I

    .line 67
    invoke-direct {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->initFields()V

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/PhonologyDefinitions$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/PhonologyDefinitions$1;

    .prologue
    .line 63
    invoke-direct {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 83
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->phoneme_:Ljava/lang/String;

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->group_:Ljava/lang/String;

    .line 113
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->memoizedSerializedSize:I

    .line 69
    return-void
.end method

.method static synthetic access$302(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasPhoneme:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->phoneme_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasGroup:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->group_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->defaultInstance:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method public static newBuilder()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
    .locals 1

    .prologue
    .line 198
    # invokes: Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->create()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->access$100()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;)Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    .prologue
    .line 201
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->newBuilder()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->mergeFrom(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;)Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->getDefaultInstanceForType()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->defaultInstance:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    return-object v0
.end method

.method public getGroup()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->group_:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->phoneme_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 115
    iget v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->memoizedSerializedSize:I

    .line 116
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 128
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 118
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 119
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasPhoneme()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 120
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->getPhoneme()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 123
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasGroup()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 124
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->getGroup()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 127
    :cond_2
    iput v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->memoizedSerializedSize:I

    move v1, v0

    .line 128
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasGroup()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasGroup:Z

    return v0
.end method

.method public hasPhoneme()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasPhoneme:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 97
    iget-boolean v1, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasPhoneme:Z

    if-nez v1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v0

    .line 98
    :cond_1
    iget-boolean v1, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasGroup:Z

    if-eqz v1, :cond_0

    .line 99
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->toBuilder()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
    .locals 1

    .prologue
    .line 203
    invoke-static {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->newBuilder(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;)Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->getSerializedSize()I

    .line 105
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasPhoneme()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->getPhoneme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 108
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasGroup()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->getGroup()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 111
    :cond_1
    return-void
.end method
