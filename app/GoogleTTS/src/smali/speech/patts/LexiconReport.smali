.class public final Lspeech/patts/LexiconReport;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LexiconReport.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/LexiconReport$1;,
        Lspeech/patts/LexiconReport$Builder;,
        Lspeech/patts/LexiconReport$ReportLevel;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/LexiconReport;


# instance fields
.field private description_:Ljava/lang/String;

.field private full_:Ljava/lang/String;

.field private hasDescription:Z

.field private hasFull:Z

.field private hasId:Z

.field private hasLevel:Z

.field private hasLineNumber:Z

.field private hasPart:Z

.field private hasPosition:Z

.field private id_:Ljava/lang/String;

.field private level_:Lspeech/patts/LexiconReport$ReportLevel;

.field private lineNumber_:I

.field private memoizedSerializedSize:I

.field private part_:Ljava/lang/String;

.field private position_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 544
    new-instance v0, Lspeech/patts/LexiconReport;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/LexiconReport;-><init>(Z)V

    sput-object v0, Lspeech/patts/LexiconReport;->defaultInstance:Lspeech/patts/LexiconReport;

    .line 545
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 546
    sget-object v0, Lspeech/patts/LexiconReport;->defaultInstance:Lspeech/patts/LexiconReport;

    invoke-direct {v0}, Lspeech/patts/LexiconReport;->initFields()V

    .line 547
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/LexiconReport;->description_:Ljava/lang/String;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/LexiconReport;->id_:Ljava/lang/String;

    .line 80
    iput v1, p0, Lspeech/patts/LexiconReport;->position_:I

    .line 87
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/LexiconReport;->part_:Ljava/lang/String;

    .line 94
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/LexiconReport;->full_:Ljava/lang/String;

    .line 101
    iput v1, p0, Lspeech/patts/LexiconReport;->lineNumber_:I

    .line 145
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/LexiconReport;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/LexiconReport;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/LexiconReport$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/LexiconReport$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/LexiconReport;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/LexiconReport;->description_:Ljava/lang/String;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/LexiconReport;->id_:Ljava/lang/String;

    .line 80
    iput v1, p0, Lspeech/patts/LexiconReport;->position_:I

    .line 87
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/LexiconReport;->part_:Ljava/lang/String;

    .line 94
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/LexiconReport;->full_:Ljava/lang/String;

    .line 101
    iput v1, p0, Lspeech/patts/LexiconReport;->lineNumber_:I

    .line 145
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/LexiconReport;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/LexiconReport;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/LexiconReport;->part_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lspeech/patts/LexiconReport;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/LexiconReport;->hasFull:Z

    return p1
.end method

.method static synthetic access$1202(Lspeech/patts/LexiconReport;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/LexiconReport;->full_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1302(Lspeech/patts/LexiconReport;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/LexiconReport;->hasLineNumber:Z

    return p1
.end method

.method static synthetic access$1402(Lspeech/patts/LexiconReport;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/LexiconReport;->lineNumber_:I

    return p1
.end method

.method static synthetic access$1502(Lspeech/patts/LexiconReport;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/LexiconReport;->hasLevel:Z

    return p1
.end method

.method static synthetic access$1602(Lspeech/patts/LexiconReport;Lspeech/patts/LexiconReport$ReportLevel;)Lspeech/patts/LexiconReport$ReportLevel;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # Lspeech/patts/LexiconReport$ReportLevel;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/LexiconReport;->level_:Lspeech/patts/LexiconReport$ReportLevel;

    return-object p1
.end method

.method static synthetic access$302(Lspeech/patts/LexiconReport;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/LexiconReport;->hasDescription:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/LexiconReport;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/LexiconReport;->description_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/LexiconReport;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/LexiconReport;->hasId:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/LexiconReport;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/LexiconReport;->id_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lspeech/patts/LexiconReport;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/LexiconReport;->hasPosition:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/LexiconReport;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/LexiconReport;->position_:I

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/LexiconReport;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LexiconReport;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/LexiconReport;->hasPart:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/LexiconReport;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/LexiconReport;->defaultInstance:Lspeech/patts/LexiconReport;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lspeech/patts/LexiconReport$ReportLevel;->ERROR:Lspeech/patts/LexiconReport$ReportLevel;

    iput-object v0, p0, Lspeech/patts/LexiconReport;->level_:Lspeech/patts/LexiconReport$ReportLevel;

    .line 114
    return-void
.end method

.method public static newBuilder()Lspeech/patts/LexiconReport$Builder;
    .locals 1

    .prologue
    .line 250
    # invokes: Lspeech/patts/LexiconReport$Builder;->create()Lspeech/patts/LexiconReport$Builder;
    invoke-static {}, Lspeech/patts/LexiconReport$Builder;->access$100()Lspeech/patts/LexiconReport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/LexiconReport;)Lspeech/patts/LexiconReport$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/LexiconReport;

    .prologue
    .line 253
    invoke-static {}, Lspeech/patts/LexiconReport;->newBuilder()Lspeech/patts/LexiconReport$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/LexiconReport$Builder;->mergeFrom(Lspeech/patts/LexiconReport;)Lspeech/patts/LexiconReport$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getDefaultInstanceForType()Lspeech/patts/LexiconReport;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/LexiconReport;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/LexiconReport;->defaultInstance:Lspeech/patts/LexiconReport;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lspeech/patts/LexiconReport;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getFull()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lspeech/patts/LexiconReport;->full_:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lspeech/patts/LexiconReport;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getLevel()Lspeech/patts/LexiconReport$ReportLevel;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lspeech/patts/LexiconReport;->level_:Lspeech/patts/LexiconReport$ReportLevel;

    return-object v0
.end method

.method public getLineNumber()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lspeech/patts/LexiconReport;->lineNumber_:I

    return v0
.end method

.method public getPart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lspeech/patts/LexiconReport;->part_:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lspeech/patts/LexiconReport;->position_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 147
    iget v0, p0, Lspeech/patts/LexiconReport;->memoizedSerializedSize:I

    .line 148
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 180
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 150
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 151
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasDescription()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 152
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 155
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasId()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 156
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 159
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasPosition()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 160
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getPosition()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 163
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasPart()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 164
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getPart()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 167
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasFull()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 168
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getFull()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 171
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasLineNumber()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 172
    const/4 v2, 0x6

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getLineNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 175
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasLevel()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 176
    const/4 v2, 0x7

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getLevel()Lspeech/patts/LexiconReport$ReportLevel;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/LexiconReport$ReportLevel;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 179
    :cond_7
    iput v0, p0, Lspeech/patts/LexiconReport;->memoizedSerializedSize:I

    move v1, v0

    .line 180
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasDescription()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lspeech/patts/LexiconReport;->hasDescription:Z

    return v0
.end method

.method public hasFull()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lspeech/patts/LexiconReport;->hasFull:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lspeech/patts/LexiconReport;->hasId:Z

    return v0
.end method

.method public hasLevel()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lspeech/patts/LexiconReport;->hasLevel:Z

    return v0
.end method

.method public hasLineNumber()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lspeech/patts/LexiconReport;->hasLineNumber:Z

    return v0
.end method

.method public hasPart()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lspeech/patts/LexiconReport;->hasPart:Z

    return v0
.end method

.method public hasPosition()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lspeech/patts/LexiconReport;->hasPosition:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->toBuilder()Lspeech/patts/LexiconReport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/LexiconReport$Builder;
    .locals 1

    .prologue
    .line 255
    invoke-static {p0}, Lspeech/patts/LexiconReport;->newBuilder(Lspeech/patts/LexiconReport;)Lspeech/patts/LexiconReport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getSerializedSize()I

    .line 122
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasDescription()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 125
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasId()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 128
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasPosition()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 131
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasPart()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 132
    const/4 v0, 0x4

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getPart()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 134
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasFull()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135
    const/4 v0, 0x5

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getFull()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 137
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasLineNumber()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 138
    const/4 v0, 0x6

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getLineNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 140
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->hasLevel()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 141
    const/4 v0, 0x7

    invoke-virtual {p0}, Lspeech/patts/LexiconReport;->getLevel()Lspeech/patts/LexiconReport$ReportLevel;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/LexiconReport$ReportLevel;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 143
    :cond_6
    return-void
.end method
