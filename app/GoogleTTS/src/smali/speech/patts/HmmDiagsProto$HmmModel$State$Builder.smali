.class public final Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "HmmDiagsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/HmmDiagsProto$HmmModel$State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/HmmDiagsProto$HmmModel$State;",
        "Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/HmmDiagsProto$HmmModel$State;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
    .locals 1

    .prologue
    .line 179
    invoke-static {}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->create()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
    .locals 3

    .prologue
    .line 188
    new-instance v0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    invoke-direct {v0}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;-><init>()V

    .line 189
    .local v0, "builder":Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
    new-instance v1, Lspeech/patts/HmmDiagsProto$HmmModel$State;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/HmmDiagsProto$HmmModel$State;-><init>(Lspeech/patts/HmmDiagsProto$1;)V

    iput-object v1, v0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    .line 190
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->build()Lspeech/patts/HmmDiagsProto$HmmModel$State;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/HmmDiagsProto$HmmModel$State;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    invoke-static {v0}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 221
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->buildPartial()Lspeech/patts/HmmDiagsProto$HmmModel$State;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/HmmDiagsProto$HmmModel$State;
    .locals 3

    .prologue
    .line 234
    iget-object v1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    if-nez v1, :cond_0

    .line 235
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 238
    :cond_0
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    .line 239
    .local v0, "returnMe":Lspeech/patts/HmmDiagsProto$HmmModel$State;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    .line 240
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->clone()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->clone()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 179
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->clone()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
    .locals 2

    .prologue
    .line 207
    invoke-static {}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->create()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    invoke-virtual {v0, v1}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->mergeFrom(Lspeech/patts/HmmDiagsProto$HmmModel$State;)Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    invoke-virtual {v0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 179
    check-cast p1, Lspeech/patts/HmmDiagsProto$HmmModel$State;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->mergeFrom(Lspeech/patts/HmmDiagsProto$HmmModel$State;)Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/HmmDiagsProto$HmmModel$State;)Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/HmmDiagsProto$HmmModel$State;

    .prologue
    .line 244
    invoke-static {}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->getDefaultInstance()Lspeech/patts/HmmDiagsProto$HmmModel$State;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-object p0

    .line 245
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->hasNumFrames()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 246
    invoke-virtual {p1}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->getNumFrames()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->setNumFrames(I)Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    .line 248
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->hasVoiced()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    invoke-virtual {p1}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->getVoiced()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->setVoiced(Z)Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    goto :goto_0
.end method

.method public setNumFrames(I)Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 290
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HmmDiagsProto$HmmModel$State;->hasNumFrames:Z
    invoke-static {v0, v1}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->access$302(Lspeech/patts/HmmDiagsProto$HmmModel$State;Z)Z

    .line 291
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    # setter for: Lspeech/patts/HmmDiagsProto$HmmModel$State;->numFrames_:I
    invoke-static {v0, p1}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->access$402(Lspeech/patts/HmmDiagsProto$HmmModel$State;I)I

    .line 292
    return-object p0
.end method

.method public setVoiced(Z)Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 308
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HmmDiagsProto$HmmModel$State;->hasVoiced:Z
    invoke-static {v0, v1}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->access$502(Lspeech/patts/HmmDiagsProto$HmmModel$State;Z)Z

    .line 309
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    # setter for: Lspeech/patts/HmmDiagsProto$HmmModel$State;->voiced_:Z
    invoke-static {v0, p1}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->access$602(Lspeech/patts/HmmDiagsProto$HmmModel$State;Z)Z

    .line 310
    return-object p0
.end method
