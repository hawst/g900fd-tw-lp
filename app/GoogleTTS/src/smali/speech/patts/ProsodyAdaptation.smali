.class public final Lspeech/patts/ProsodyAdaptation;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "ProsodyAdaptation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/ProsodyAdaptation$1;,
        Lspeech/patts/ProsodyAdaptation$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/ProsodyAdaptation;


# instance fields
.field private hasSpeakingRateSource:Z

.field private hasSpeakingRateTarget:Z

.field private memoizedSerializedSize:I

.field private sourceF0MeanMemoizedSerializedSize:I

.field private sourceF0Mean_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private sourceF0StdMemoizedSerializedSize:I

.field private sourceF0Std_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private speakingRateSource_:D

.field private speakingRateTarget_:D

.field private targetF0MeanMemoizedSerializedSize:I

.field private targetF0Mean_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private targetF0StdMemoizedSerializedSize:I

.field private targetF0Std_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 640
    new-instance v0, Lspeech/patts/ProsodyAdaptation;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/ProsodyAdaptation;-><init>(Z)V

    sput-object v0, Lspeech/patts/ProsodyAdaptation;->defaultInstance:Lspeech/patts/ProsodyAdaptation;

    .line 641
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 642
    sget-object v0, Lspeech/patts/ProsodyAdaptation;->defaultInstance:Lspeech/patts/ProsodyAdaptation;

    invoke-direct {v0}, Lspeech/patts/ProsodyAdaptation;->initFields()V

    .line 643
    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, -0x1

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;

    .line 33
    iput v1, p0, Lspeech/patts/ProsodyAdaptation;->targetF0MeanMemoizedSerializedSize:I

    .line 37
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;

    .line 46
    iput v1, p0, Lspeech/patts/ProsodyAdaptation;->targetF0StdMemoizedSerializedSize:I

    .line 51
    iput-wide v2, p0, Lspeech/patts/ProsodyAdaptation;->speakingRateSource_:D

    .line 58
    iput-wide v2, p0, Lspeech/patts/ProsodyAdaptation;->speakingRateTarget_:D

    .line 64
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;

    .line 73
    iput v1, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0MeanMemoizedSerializedSize:I

    .line 77
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;

    .line 86
    iput v1, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0StdMemoizedSerializedSize:I

    .line 133
    iput v1, p0, Lspeech/patts/ProsodyAdaptation;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/ProsodyAdaptation;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/ProsodyAdaptation$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/ProsodyAdaptation$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/ProsodyAdaptation;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 4
    .param p1, "noInit"    # Z

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, -0x1

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;

    .line 33
    iput v1, p0, Lspeech/patts/ProsodyAdaptation;->targetF0MeanMemoizedSerializedSize:I

    .line 37
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;

    .line 46
    iput v1, p0, Lspeech/patts/ProsodyAdaptation;->targetF0StdMemoizedSerializedSize:I

    .line 51
    iput-wide v2, p0, Lspeech/patts/ProsodyAdaptation;->speakingRateSource_:D

    .line 58
    iput-wide v2, p0, Lspeech/patts/ProsodyAdaptation;->speakingRateTarget_:D

    .line 64
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;

    .line 73
    iput v1, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0MeanMemoizedSerializedSize:I

    .line 77
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;

    .line 86
    iput v1, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0StdMemoizedSerializedSize:I

    .line 133
    iput v1, p0, Lspeech/patts/ProsodyAdaptation;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/ProsodyAdaptation;D)D
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ProsodyAdaptation;
    .param p1, "x1"    # D

    .prologue
    .line 5
    iput-wide p1, p0, Lspeech/patts/ProsodyAdaptation;->speakingRateTarget_:D

    return-wide p1
.end method

.method static synthetic access$300(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ProsodyAdaptation;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/ProsodyAdaptation;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProsodyAdaptation;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ProsodyAdaptation;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lspeech/patts/ProsodyAdaptation;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProsodyAdaptation;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ProsodyAdaptation;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lspeech/patts/ProsodyAdaptation;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProsodyAdaptation;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ProsodyAdaptation;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lspeech/patts/ProsodyAdaptation;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProsodyAdaptation;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$702(Lspeech/patts/ProsodyAdaptation;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProsodyAdaptation;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/ProsodyAdaptation;->hasSpeakingRateSource:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/ProsodyAdaptation;D)D
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ProsodyAdaptation;
    .param p1, "x1"    # D

    .prologue
    .line 5
    iput-wide p1, p0, Lspeech/patts/ProsodyAdaptation;->speakingRateSource_:D

    return-wide p1
.end method

.method static synthetic access$902(Lspeech/patts/ProsodyAdaptation;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProsodyAdaptation;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/ProsodyAdaptation;->hasSpeakingRateTarget:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/ProsodyAdaptation;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/ProsodyAdaptation;->defaultInstance:Lspeech/patts/ProsodyAdaptation;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method public static newBuilder()Lspeech/patts/ProsodyAdaptation$Builder;
    .locals 1

    .prologue
    .line 262
    # invokes: Lspeech/patts/ProsodyAdaptation$Builder;->create()Lspeech/patts/ProsodyAdaptation$Builder;
    invoke-static {}, Lspeech/patts/ProsodyAdaptation$Builder;->access$100()Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/ProsodyAdaptation$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/ProsodyAdaptation;

    .prologue
    .line 265
    invoke-static {}, Lspeech/patts/ProsodyAdaptation;->newBuilder()Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/ProsodyAdaptation$Builder;->mergeFrom(Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getDefaultInstanceForType()Lspeech/patts/ProsodyAdaptation;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/ProsodyAdaptation;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/ProsodyAdaptation;->defaultInstance:Lspeech/patts/ProsodyAdaptation;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 135
    iget v1, p0, Lspeech/patts/ProsodyAdaptation;->memoizedSerializedSize:I

    .line 136
    .local v1, "size":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 192
    .end local v1    # "size":I
    .local v2, "size":I
    :goto_0
    return v2

    .line 138
    .end local v2    # "size":I
    .restart local v1    # "size":I
    :cond_0
    const/4 v1, 0x0

    .line 140
    const/4 v0, 0x0

    .line 141
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getTargetF0MeanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x8

    .line 142
    add-int/2addr v1, v0

    .line 143
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getTargetF0MeanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 144
    add-int/lit8 v1, v1, 0x1

    .line 145
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 148
    :cond_1
    iput v0, p0, Lspeech/patts/ProsodyAdaptation;->targetF0MeanMemoizedSerializedSize:I

    .line 151
    const/4 v0, 0x0

    .line 152
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getTargetF0StdList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x8

    .line 153
    add-int/2addr v1, v0

    .line 154
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getTargetF0StdList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 155
    add-int/lit8 v1, v1, 0x1

    .line 156
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 159
    :cond_2
    iput v0, p0, Lspeech/patts/ProsodyAdaptation;->targetF0StdMemoizedSerializedSize:I

    .line 161
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->hasSpeakingRateSource()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 162
    const/4 v3, 0x3

    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSpeakingRateSource()D

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeDoubleSize(ID)I

    move-result v3

    add-int/2addr v1, v3

    .line 165
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->hasSpeakingRateTarget()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 166
    const/4 v3, 0x4

    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSpeakingRateTarget()D

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeDoubleSize(ID)I

    move-result v3

    add-int/2addr v1, v3

    .line 170
    :cond_4
    const/4 v0, 0x0

    .line 171
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSourceF0MeanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x8

    .line 172
    add-int/2addr v1, v0

    .line 173
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSourceF0MeanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 174
    add-int/lit8 v1, v1, 0x1

    .line 175
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 178
    :cond_5
    iput v0, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0MeanMemoizedSerializedSize:I

    .line 181
    const/4 v0, 0x0

    .line 182
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSourceF0StdList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x8

    .line 183
    add-int/2addr v1, v0

    .line 184
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSourceF0StdList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 185
    add-int/lit8 v1, v1, 0x1

    .line 186
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 189
    :cond_6
    iput v0, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0StdMemoizedSerializedSize:I

    .line 191
    iput v1, p0, Lspeech/patts/ProsodyAdaptation;->memoizedSerializedSize:I

    move v2, v1

    .line 192
    .end local v1    # "size":I
    .restart local v2    # "size":I
    goto/16 :goto_0
.end method

.method public getSourceF0MeanList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;

    return-object v0
.end method

.method public getSourceF0StdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;

    return-object v0
.end method

.method public getSpeakingRateSource()D
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lspeech/patts/ProsodyAdaptation;->speakingRateSource_:D

    return-wide v0
.end method

.method public getSpeakingRateTarget()D
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lspeech/patts/ProsodyAdaptation;->speakingRateTarget_:D

    return-wide v0
.end method

.method public getTargetF0MeanList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;

    return-object v0
.end method

.method public getTargetF0StdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;

    return-object v0
.end method

.method public hasSpeakingRateSource()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lspeech/patts/ProsodyAdaptation;->hasSpeakingRateSource:Z

    return v0
.end method

.method public hasSpeakingRateTarget()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lspeech/patts/ProsodyAdaptation;->hasSpeakingRateTarget:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->toBuilder()Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/ProsodyAdaptation$Builder;
    .locals 1

    .prologue
    .line 267
    invoke-static {p0}, Lspeech/patts/ProsodyAdaptation;->newBuilder(Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSerializedSize()I

    .line 97
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getTargetF0MeanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 98
    const/16 v3, 0xa

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 99
    iget v3, p0, Lspeech/patts/ProsodyAdaptation;->targetF0MeanMemoizedSerializedSize:I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 101
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getTargetF0MeanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 102
    .local v0, "element":D
    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeDoubleNoTag(D)V

    goto :goto_0

    .line 104
    .end local v0    # "element":D
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getTargetF0StdList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 105
    const/16 v3, 0x12

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 106
    iget v3, p0, Lspeech/patts/ProsodyAdaptation;->targetF0StdMemoizedSerializedSize:I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 108
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getTargetF0StdList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 109
    .restart local v0    # "element":D
    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeDoubleNoTag(D)V

    goto :goto_1

    .line 111
    .end local v0    # "element":D
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->hasSpeakingRateSource()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 112
    const/4 v3, 0x3

    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSpeakingRateSource()D

    move-result-wide v4

    invoke-virtual {p1, v3, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->writeDouble(ID)V

    .line 114
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->hasSpeakingRateTarget()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 115
    const/4 v3, 0x4

    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSpeakingRateTarget()D

    move-result-wide v4

    invoke-virtual {p1, v3, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->writeDouble(ID)V

    .line 117
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSourceF0MeanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 118
    const/16 v3, 0x2a

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 119
    iget v3, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0MeanMemoizedSerializedSize:I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 121
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSourceF0MeanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 122
    .restart local v0    # "element":D
    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeDoubleNoTag(D)V

    goto :goto_2

    .line 124
    .end local v0    # "element":D
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSourceF0StdList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_8

    .line 125
    const/16 v3, 0x32

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 126
    iget v3, p0, Lspeech/patts/ProsodyAdaptation;->sourceF0StdMemoizedSerializedSize:I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 128
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation;->getSourceF0StdList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 129
    .restart local v0    # "element":D
    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeDoubleNoTag(D)V

    goto :goto_3

    .line 131
    .end local v0    # "element":D
    :cond_9
    return-void
.end method
