.class public final enum Lspeech/patts/CostFunction$ValueType;
.super Ljava/lang/Enum;
.source "CostFunction.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/CostFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ValueType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/CostFunction$ValueType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/CostFunction$ValueType;

.field public static final enum BOOL:Lspeech/patts/CostFunction$ValueType;

.field public static final enum FLOAT:Lspeech/patts/CostFunction$ValueType;

.field public static final enum INT:Lspeech/patts/CostFunction$ValueType;

.field public static final enum STRING:Lspeech/patts/CostFunction$ValueType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/CostFunction$ValueType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 69
    new-instance v0, Lspeech/patts/CostFunction$ValueType;

    const-string v1, "BOOL"

    invoke-direct {v0, v1, v5, v5, v2}, Lspeech/patts/CostFunction$ValueType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/CostFunction$ValueType;->BOOL:Lspeech/patts/CostFunction$ValueType;

    .line 70
    new-instance v0, Lspeech/patts/CostFunction$ValueType;

    const-string v1, "INT"

    invoke-direct {v0, v1, v2, v2, v3}, Lspeech/patts/CostFunction$ValueType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/CostFunction$ValueType;->INT:Lspeech/patts/CostFunction$ValueType;

    .line 71
    new-instance v0, Lspeech/patts/CostFunction$ValueType;

    const-string v1, "FLOAT"

    invoke-direct {v0, v1, v3, v3, v4}, Lspeech/patts/CostFunction$ValueType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/CostFunction$ValueType;->FLOAT:Lspeech/patts/CostFunction$ValueType;

    .line 72
    new-instance v0, Lspeech/patts/CostFunction$ValueType;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v4, v4, v6}, Lspeech/patts/CostFunction$ValueType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/CostFunction$ValueType;->STRING:Lspeech/patts/CostFunction$ValueType;

    .line 67
    new-array v0, v6, [Lspeech/patts/CostFunction$ValueType;

    sget-object v1, Lspeech/patts/CostFunction$ValueType;->BOOL:Lspeech/patts/CostFunction$ValueType;

    aput-object v1, v0, v5

    sget-object v1, Lspeech/patts/CostFunction$ValueType;->INT:Lspeech/patts/CostFunction$ValueType;

    aput-object v1, v0, v2

    sget-object v1, Lspeech/patts/CostFunction$ValueType;->FLOAT:Lspeech/patts/CostFunction$ValueType;

    aput-object v1, v0, v3

    sget-object v1, Lspeech/patts/CostFunction$ValueType;->STRING:Lspeech/patts/CostFunction$ValueType;

    aput-object v1, v0, v4

    sput-object v0, Lspeech/patts/CostFunction$ValueType;->$VALUES:[Lspeech/patts/CostFunction$ValueType;

    .line 93
    new-instance v0, Lspeech/patts/CostFunction$ValueType$1;

    invoke-direct {v0}, Lspeech/patts/CostFunction$ValueType$1;-><init>()V

    sput-object v0, Lspeech/patts/CostFunction$ValueType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 103
    iput p3, p0, Lspeech/patts/CostFunction$ValueType;->index:I

    .line 104
    iput p4, p0, Lspeech/patts/CostFunction$ValueType;->value:I

    .line 105
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/CostFunction$ValueType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 67
    const-class v0, Lspeech/patts/CostFunction$ValueType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction$ValueType;

    return-object v0
.end method

.method public static values()[Lspeech/patts/CostFunction$ValueType;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lspeech/patts/CostFunction$ValueType;->$VALUES:[Lspeech/patts/CostFunction$ValueType;

    invoke-virtual {v0}, [Lspeech/patts/CostFunction$ValueType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/CostFunction$ValueType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lspeech/patts/CostFunction$ValueType;->value:I

    return v0
.end method
