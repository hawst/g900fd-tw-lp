.class public final Lspeech/patts/HmmDiagsProto$HmmModel$State;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "HmmDiagsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/HmmDiagsProto$HmmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/HmmDiagsProto$HmmModel$State;


# instance fields
.field private hasNumFrames:Z

.field private hasVoiced:Z

.field private memoizedSerializedSize:I

.field private numFrames_:I

.field private voiced_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 322
    new-instance v0, Lspeech/patts/HmmDiagsProto$HmmModel$State;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/HmmDiagsProto$HmmModel$State;-><init>(Z)V

    sput-object v0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->defaultInstance:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    .line 323
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 324
    sget-object v0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->defaultInstance:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    invoke-direct {v0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->initFields()V

    .line 325
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 42
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 59
    iput v1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->numFrames_:I

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->voiced_:Z

    .line 87
    iput v1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->memoizedSerializedSize:I

    .line 43
    invoke-direct {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->initFields()V

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/HmmDiagsProto$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/HmmDiagsProto$1;

    .prologue
    .line 39
    invoke-direct {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, -0x1

    .line 45
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 59
    iput v1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->numFrames_:I

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->voiced_:Z

    .line 87
    iput v1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->memoizedSerializedSize:I

    .line 45
    return-void
.end method

.method static synthetic access$302(Lspeech/patts/HmmDiagsProto$HmmModel$State;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HmmDiagsProto$HmmModel$State;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->hasNumFrames:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/HmmDiagsProto$HmmModel$State;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HmmDiagsProto$HmmModel$State;
    .param p1, "x1"    # I

    .prologue
    .line 39
    iput p1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->numFrames_:I

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/HmmDiagsProto$HmmModel$State;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HmmDiagsProto$HmmModel$State;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->hasVoiced:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/HmmDiagsProto$HmmModel$State;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HmmDiagsProto$HmmModel$State;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->voiced_:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/HmmDiagsProto$HmmModel$State;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->defaultInstance:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public static newBuilder()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
    .locals 1

    .prologue
    .line 172
    # invokes: Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->create()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
    invoke-static {}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->access$100()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/HmmDiagsProto$HmmModel$State;)Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/HmmDiagsProto$HmmModel$State;

    .prologue
    .line 175
    invoke-static {}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->newBuilder()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;->mergeFrom(Lspeech/patts/HmmDiagsProto$HmmModel$State;)Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->getDefaultInstanceForType()Lspeech/patts/HmmDiagsProto$HmmModel$State;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/HmmDiagsProto$HmmModel$State;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->defaultInstance:Lspeech/patts/HmmDiagsProto$HmmModel$State;

    return-object v0
.end method

.method public getNumFrames()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->numFrames_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 89
    iget v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->memoizedSerializedSize:I

    .line 90
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 102
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 92
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 93
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->hasNumFrames()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 94
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->getNumFrames()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 97
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->hasVoiced()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 98
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->getVoiced()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 101
    :cond_2
    iput v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->memoizedSerializedSize:I

    move v1, v0

    .line 102
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getVoiced()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->voiced_:Z

    return v0
.end method

.method public hasNumFrames()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->hasNumFrames:Z

    return v0
.end method

.method public hasVoiced()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$State;->hasVoiced:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->toBuilder()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;
    .locals 1

    .prologue
    .line 177
    invoke-static {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->newBuilder(Lspeech/patts/HmmDiagsProto$HmmModel$State;)Lspeech/patts/HmmDiagsProto$HmmModel$State$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->getSerializedSize()I

    .line 79
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->hasNumFrames()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->getNumFrames()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 82
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->hasVoiced()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$State;->getVoiced()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 85
    :cond_1
    return-void
.end method
