.class public final Lspeech/patts/Voice$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Voice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Voice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/Voice;",
        "Lspeech/patts/Voice$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/Voice;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/Voice$Builder;
    .locals 1

    .prologue
    .line 225
    invoke-static {}, Lspeech/patts/Voice$Builder;->create()Lspeech/patts/Voice$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/Voice$Builder;
    .locals 3

    .prologue
    .line 234
    new-instance v0, Lspeech/patts/Voice$Builder;

    invoke-direct {v0}, Lspeech/patts/Voice$Builder;-><init>()V

    .line 235
    .local v0, "builder":Lspeech/patts/Voice$Builder;
    new-instance v1, Lspeech/patts/Voice;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/Voice;-><init>(Lspeech/patts/Voice$1;)V

    iput-object v1, v0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    .line 236
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lspeech/patts/Voice$Builder;->build()Lspeech/patts/Voice;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/Voice;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/Voice$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 265
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    invoke-static {v0}, Lspeech/patts/Voice$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 267
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Voice$Builder;->buildPartial()Lspeech/patts/Voice;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/Voice;
    .locals 3

    .prologue
    .line 280
    iget-object v1, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    if-nez v1, :cond_0

    .line 281
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 284
    :cond_0
    iget-object v1, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->baseType_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Voice;->access$300(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 285
    iget-object v1, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    iget-object v2, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->baseType_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Voice;->access$300(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Voice;->baseType_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Voice;->access$302(Lspeech/patts/Voice;Ljava/util/List;)Ljava/util/List;

    .line 288
    :cond_1
    iget-object v1, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->model_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Voice;->access$400(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 289
    iget-object v1, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    iget-object v2, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->model_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Voice;->access$400(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Voice;->model_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Voice;->access$402(Lspeech/patts/Voice;Ljava/util/List;)Ljava/util/List;

    .line 292
    :cond_2
    iget-object v1, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->unit_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Voice;->access$500(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 293
    iget-object v1, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    iget-object v2, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->unit_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Voice;->access$500(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Voice;->unit_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Voice;->access$502(Lspeech/patts/Voice;Ljava/util/List;)Ljava/util/List;

    .line 296
    :cond_3
    iget-object v1, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->filenames_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Voice;->access$600(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 297
    iget-object v1, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    iget-object v2, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->filenames_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Voice;->access$600(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Voice;->filenames_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Voice;->access$602(Lspeech/patts/Voice;Ljava/util/List;)Ljava/util/List;

    .line 300
    :cond_4
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    .line 301
    .local v0, "returnMe":Lspeech/patts/Voice;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    .line 302
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lspeech/patts/Voice$Builder;->clone()Lspeech/patts/Voice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lspeech/patts/Voice$Builder;->clone()Lspeech/patts/Voice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 225
    invoke-virtual {p0}, Lspeech/patts/Voice$Builder;->clone()Lspeech/patts/Voice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/Voice$Builder;
    .locals 2

    .prologue
    .line 253
    invoke-static {}, Lspeech/patts/Voice$Builder;->create()Lspeech/patts/Voice$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    invoke-virtual {v0, v1}, Lspeech/patts/Voice$Builder;->mergeFrom(Lspeech/patts/Voice;)Lspeech/patts/Voice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    invoke-virtual {v0}, Lspeech/patts/Voice;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeCostFunctions(Lspeech/patts/CostFunctions;)Lspeech/patts/Voice$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/CostFunctions;

    .prologue
    .line 602
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    invoke-virtual {v0}, Lspeech/patts/Voice;->hasCostFunctions()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->costFunctions_:Lspeech/patts/CostFunctions;
    invoke-static {v0}, Lspeech/patts/Voice;->access$800(Lspeech/patts/Voice;)Lspeech/patts/CostFunctions;

    move-result-object v0

    invoke-static {}, Lspeech/patts/CostFunctions;->getDefaultInstance()Lspeech/patts/CostFunctions;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 604
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    iget-object v1, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->costFunctions_:Lspeech/patts/CostFunctions;
    invoke-static {v1}, Lspeech/patts/Voice;->access$800(Lspeech/patts/Voice;)Lspeech/patts/CostFunctions;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/CostFunctions;->newBuilder(Lspeech/patts/CostFunctions;)Lspeech/patts/CostFunctions$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/CostFunctions$Builder;->mergeFrom(Lspeech/patts/CostFunctions;)Lspeech/patts/CostFunctions$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/CostFunctions$Builder;->buildPartial()Lspeech/patts/CostFunctions;

    move-result-object v1

    # setter for: Lspeech/patts/Voice;->costFunctions_:Lspeech/patts/CostFunctions;
    invoke-static {v0, v1}, Lspeech/patts/Voice;->access$802(Lspeech/patts/Voice;Lspeech/patts/CostFunctions;)Lspeech/patts/CostFunctions;

    .line 609
    :goto_0
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Voice;->hasCostFunctions:Z
    invoke-static {v0, v1}, Lspeech/patts/Voice;->access$702(Lspeech/patts/Voice;Z)Z

    .line 610
    return-object p0

    .line 607
    :cond_0
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # setter for: Lspeech/patts/Voice;->costFunctions_:Lspeech/patts/CostFunctions;
    invoke-static {v0, p1}, Lspeech/patts/Voice;->access$802(Lspeech/patts/Voice;Lspeech/patts/CostFunctions;)Lspeech/patts/CostFunctions;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 225
    check-cast p1, Lspeech/patts/Voice;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/Voice$Builder;->mergeFrom(Lspeech/patts/Voice;)Lspeech/patts/Voice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/Voice;)Lspeech/patts/Voice$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/Voice;

    .prologue
    .line 306
    invoke-static {}, Lspeech/patts/Voice;->getDefaultInstance()Lspeech/patts/Voice;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 334
    :cond_0
    :goto_0
    return-object p0

    .line 307
    :cond_1
    # getter for: Lspeech/patts/Voice;->baseType_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Voice;->access$300(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 308
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->baseType_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Voice;->access$300(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 309
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Voice;->baseType_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Voice;->access$302(Lspeech/patts/Voice;Ljava/util/List;)Ljava/util/List;

    .line 311
    :cond_2
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->baseType_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Voice;->access$300(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Voice;->baseType_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Voice;->access$300(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 313
    :cond_3
    # getter for: Lspeech/patts/Voice;->model_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Voice;->access$400(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 314
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->model_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Voice;->access$400(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 315
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Voice;->model_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Voice;->access$402(Lspeech/patts/Voice;Ljava/util/List;)Ljava/util/List;

    .line 317
    :cond_4
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->model_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Voice;->access$400(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Voice;->model_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Voice;->access$400(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 319
    :cond_5
    # getter for: Lspeech/patts/Voice;->unit_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Voice;->access$500(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 320
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->unit_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Voice;->access$500(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 321
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Voice;->unit_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Voice;->access$502(Lspeech/patts/Voice;Ljava/util/List;)Ljava/util/List;

    .line 323
    :cond_6
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->unit_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Voice;->access$500(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Voice;->unit_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Voice;->access$500(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 325
    :cond_7
    # getter for: Lspeech/patts/Voice;->filenames_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Voice;->access$600(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 326
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->filenames_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Voice;->access$600(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 327
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Voice;->filenames_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Voice;->access$602(Lspeech/patts/Voice;Ljava/util/List;)Ljava/util/List;

    .line 329
    :cond_8
    iget-object v0, p0, Lspeech/patts/Voice$Builder;->result:Lspeech/patts/Voice;

    # getter for: Lspeech/patts/Voice;->filenames_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Voice;->access$600(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Voice;->filenames_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Voice;->access$600(Lspeech/patts/Voice;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 331
    :cond_9
    invoke-virtual {p1}, Lspeech/patts/Voice;->hasCostFunctions()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    invoke-virtual {p1}, Lspeech/patts/Voice;->getCostFunctions()Lspeech/patts/CostFunctions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Voice$Builder;->mergeCostFunctions(Lspeech/patts/CostFunctions;)Lspeech/patts/Voice$Builder;

    goto/16 :goto_0
.end method
