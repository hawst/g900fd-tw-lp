.class public final Lspeech/patts/FrequencyWarpAndWeightTransforms;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FrequencyWarpAndWeightTransforms.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/FrequencyWarpAndWeightTransforms$1;,
        Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/FrequencyWarpAndWeightTransforms;


# instance fields
.field private global_:Lspeech/patts/FrequencyWarpAndWeightTransform;

.field private hasGlobal:Z

.field private hasProsody:Z

.field private local_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/FrequencyWarpAndWeightTransform;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private prosody_:Lspeech/patts/ProsodyAdaptation;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 426
    new-instance v0, Lspeech/patts/FrequencyWarpAndWeightTransforms;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransforms;-><init>(Z)V

    sput-object v0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->defaultInstance:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .line 427
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 428
    sget-object v0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->defaultInstance:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    invoke-direct {v0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->initFields()V

    .line 429
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/FrequencyWarpAndWeightTransforms$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransforms$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/FrequencyWarpAndWeightTransforms;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransforms;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/FrequencyWarpAndWeightTransforms;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransforms;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasProsody:Z

    return p1
.end method

.method static synthetic access$500(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/ProsodyAdaptation;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->prosody_:Lspeech/patts/ProsodyAdaptation;

    return-object v0
.end method

.method static synthetic access$502(Lspeech/patts/FrequencyWarpAndWeightTransforms;Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/ProsodyAdaptation;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransforms;
    .param p1, "x1"    # Lspeech/patts/ProsodyAdaptation;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->prosody_:Lspeech/patts/ProsodyAdaptation;

    return-object p1
.end method

.method static synthetic access$602(Lspeech/patts/FrequencyWarpAndWeightTransforms;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransforms;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasGlobal:Z

    return p1
.end method

.method static synthetic access$700(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransform;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->global_:Lspeech/patts/FrequencyWarpAndWeightTransform;

    return-object v0
.end method

.method static synthetic access$702(Lspeech/patts/FrequencyWarpAndWeightTransforms;Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransform;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransforms;
    .param p1, "x1"    # Lspeech/patts/FrequencyWarpAndWeightTransform;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->global_:Lspeech/patts/FrequencyWarpAndWeightTransform;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/FrequencyWarpAndWeightTransforms;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->defaultInstance:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lspeech/patts/ProsodyAdaptation;->getDefaultInstance()Lspeech/patts/ProsodyAdaptation;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->prosody_:Lspeech/patts/ProsodyAdaptation;

    .line 50
    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getDefaultInstance()Lspeech/patts/FrequencyWarpAndWeightTransform;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->global_:Lspeech/patts/FrequencyWarpAndWeightTransform;

    .line 51
    return-void
.end method

.method public static newBuilder()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
    .locals 1

    .prologue
    .line 161
    # invokes: Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->create()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->access$100()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/FrequencyWarpAndWeightTransforms;

    .prologue
    .line 164
    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->newBuilder()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;->mergeFrom(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getDefaultInstanceForType()Lspeech/patts/FrequencyWarpAndWeightTransforms;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/FrequencyWarpAndWeightTransforms;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->defaultInstance:Lspeech/patts/FrequencyWarpAndWeightTransforms;

    return-object v0
.end method

.method public getGlobal()Lspeech/patts/FrequencyWarpAndWeightTransform;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->global_:Lspeech/patts/FrequencyWarpAndWeightTransform;

    return-object v0
.end method

.method public getLocalList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/FrequencyWarpAndWeightTransform;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->local_:Ljava/util/List;

    return-object v0
.end method

.method public getProsody()Lspeech/patts/ProsodyAdaptation;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->prosody_:Lspeech/patts/ProsodyAdaptation;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 74
    iget v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->memoizedSerializedSize:I

    .line 75
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 91
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 77
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 78
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasProsody()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 79
    const/4 v4, 0x1

    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getProsody()Lspeech/patts/ProsodyAdaptation;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 82
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasGlobal()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 83
    const/4 v4, 0x2

    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getGlobal()Lspeech/patts/FrequencyWarpAndWeightTransform;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 86
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getLocalList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/FrequencyWarpAndWeightTransform;

    .line 87
    .local v0, "element":Lspeech/patts/FrequencyWarpAndWeightTransform;
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 89
    goto :goto_1

    .line 90
    .end local v0    # "element":Lspeech/patts/FrequencyWarpAndWeightTransform;
    :cond_3
    iput v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->memoizedSerializedSize:I

    move v3, v2

    .line 91
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public hasGlobal()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasGlobal:Z

    return v0
.end method

.method public hasProsody()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasProsody:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 53
    iget-boolean v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasProsody:Z

    if-nez v1, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    iget-boolean v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasGlobal:Z

    if-eqz v1, :cond_0

    .line 55
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->toBuilder()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;
    .locals 1

    .prologue
    .line 166
    invoke-static {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->newBuilder(Lspeech/patts/FrequencyWarpAndWeightTransforms;)Lspeech/patts/FrequencyWarpAndWeightTransforms$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getSerializedSize()I

    .line 61
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasProsody()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getProsody()Lspeech/patts/ProsodyAdaptation;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 64
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->hasGlobal()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 65
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getGlobal()Lspeech/patts/FrequencyWarpAndWeightTransform;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 67
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransforms;->getLocalList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/FrequencyWarpAndWeightTransform;

    .line 68
    .local v0, "element":Lspeech/patts/FrequencyWarpAndWeightTransform;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 70
    .end local v0    # "element":Lspeech/patts/FrequencyWarpAndWeightTransform;
    :cond_2
    return-void
.end method
