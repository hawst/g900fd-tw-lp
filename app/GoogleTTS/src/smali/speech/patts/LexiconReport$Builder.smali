.class public final Lspeech/patts/LexiconReport$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LexiconReport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/LexiconReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/LexiconReport;",
        "Lspeech/patts/LexiconReport$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/LexiconReport;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/LexiconReport$Builder;
    .locals 1

    .prologue
    .line 257
    invoke-static {}, Lspeech/patts/LexiconReport$Builder;->create()Lspeech/patts/LexiconReport$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/LexiconReport$Builder;
    .locals 3

    .prologue
    .line 266
    new-instance v0, Lspeech/patts/LexiconReport$Builder;

    invoke-direct {v0}, Lspeech/patts/LexiconReport$Builder;-><init>()V

    .line 267
    .local v0, "builder":Lspeech/patts/LexiconReport$Builder;
    new-instance v1, Lspeech/patts/LexiconReport;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/LexiconReport;-><init>(Lspeech/patts/LexiconReport$1;)V

    iput-object v1, v0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    .line 268
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lspeech/patts/LexiconReport$Builder;->build()Lspeech/patts/LexiconReport;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/LexiconReport;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/LexiconReport$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 297
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    invoke-static {v0}, Lspeech/patts/LexiconReport$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 299
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/LexiconReport$Builder;->buildPartial()Lspeech/patts/LexiconReport;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/LexiconReport;
    .locals 3

    .prologue
    .line 312
    iget-object v1, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    if-nez v1, :cond_0

    .line 313
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 316
    :cond_0
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    .line 317
    .local v0, "returnMe":Lspeech/patts/LexiconReport;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    .line 318
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lspeech/patts/LexiconReport$Builder;->clone()Lspeech/patts/LexiconReport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lspeech/patts/LexiconReport$Builder;->clone()Lspeech/patts/LexiconReport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 257
    invoke-virtual {p0}, Lspeech/patts/LexiconReport$Builder;->clone()Lspeech/patts/LexiconReport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/LexiconReport$Builder;
    .locals 2

    .prologue
    .line 285
    invoke-static {}, Lspeech/patts/LexiconReport$Builder;->create()Lspeech/patts/LexiconReport$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    invoke-virtual {v0, v1}, Lspeech/patts/LexiconReport$Builder;->mergeFrom(Lspeech/patts/LexiconReport;)Lspeech/patts/LexiconReport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    invoke-virtual {v0}, Lspeech/patts/LexiconReport;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 257
    check-cast p1, Lspeech/patts/LexiconReport;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/LexiconReport$Builder;->mergeFrom(Lspeech/patts/LexiconReport;)Lspeech/patts/LexiconReport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/LexiconReport;)Lspeech/patts/LexiconReport$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/LexiconReport;

    .prologue
    .line 322
    invoke-static {}, Lspeech/patts/LexiconReport;->getDefaultInstance()Lspeech/patts/LexiconReport;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 344
    :cond_0
    :goto_0
    return-object p0

    .line 323
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->hasDescription()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 324
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/LexiconReport$Builder;->setDescription(Ljava/lang/String;)Lspeech/patts/LexiconReport$Builder;

    .line 326
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->hasId()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 327
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/LexiconReport$Builder;->setId(Ljava/lang/String;)Lspeech/patts/LexiconReport$Builder;

    .line 329
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->hasPosition()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 330
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->getPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/LexiconReport$Builder;->setPosition(I)Lspeech/patts/LexiconReport$Builder;

    .line 332
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->hasPart()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 333
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->getPart()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/LexiconReport$Builder;->setPart(Ljava/lang/String;)Lspeech/patts/LexiconReport$Builder;

    .line 335
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->hasFull()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 336
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->getFull()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/LexiconReport$Builder;->setFull(Ljava/lang/String;)Lspeech/patts/LexiconReport$Builder;

    .line 338
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->hasLineNumber()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 339
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->getLineNumber()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/LexiconReport$Builder;->setLineNumber(I)Lspeech/patts/LexiconReport$Builder;

    .line 341
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->hasLevel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    invoke-virtual {p1}, Lspeech/patts/LexiconReport;->getLevel()Lspeech/patts/LexiconReport$ReportLevel;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/LexiconReport$Builder;->setLevel(Lspeech/patts/LexiconReport$ReportLevel;)Lspeech/patts/LexiconReport$Builder;

    goto :goto_0
.end method

.method public setDescription(Ljava/lang/String;)Lspeech/patts/LexiconReport$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 407
    if-nez p1, :cond_0

    .line 408
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 410
    :cond_0
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/LexiconReport;->hasDescription:Z
    invoke-static {v0, v1}, Lspeech/patts/LexiconReport;->access$302(Lspeech/patts/LexiconReport;Z)Z

    .line 411
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    # setter for: Lspeech/patts/LexiconReport;->description_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/LexiconReport;->access$402(Lspeech/patts/LexiconReport;Ljava/lang/String;)Ljava/lang/String;

    .line 412
    return-object p0
.end method

.method public setFull(Ljava/lang/String;)Lspeech/patts/LexiconReport$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 488
    if-nez p1, :cond_0

    .line 489
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 491
    :cond_0
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/LexiconReport;->hasFull:Z
    invoke-static {v0, v1}, Lspeech/patts/LexiconReport;->access$1102(Lspeech/patts/LexiconReport;Z)Z

    .line 492
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    # setter for: Lspeech/patts/LexiconReport;->full_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/LexiconReport;->access$1202(Lspeech/patts/LexiconReport;Ljava/lang/String;)Ljava/lang/String;

    .line 493
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lspeech/patts/LexiconReport$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 428
    if-nez p1, :cond_0

    .line 429
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 431
    :cond_0
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/LexiconReport;->hasId:Z
    invoke-static {v0, v1}, Lspeech/patts/LexiconReport;->access$502(Lspeech/patts/LexiconReport;Z)Z

    .line 432
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    # setter for: Lspeech/patts/LexiconReport;->id_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/LexiconReport;->access$602(Lspeech/patts/LexiconReport;Ljava/lang/String;)Ljava/lang/String;

    .line 433
    return-object p0
.end method

.method public setLevel(Lspeech/patts/LexiconReport$ReportLevel;)Lspeech/patts/LexiconReport$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/LexiconReport$ReportLevel;

    .prologue
    .line 527
    if-nez p1, :cond_0

    .line 528
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 530
    :cond_0
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/LexiconReport;->hasLevel:Z
    invoke-static {v0, v1}, Lspeech/patts/LexiconReport;->access$1502(Lspeech/patts/LexiconReport;Z)Z

    .line 531
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    # setter for: Lspeech/patts/LexiconReport;->level_:Lspeech/patts/LexiconReport$ReportLevel;
    invoke-static {v0, p1}, Lspeech/patts/LexiconReport;->access$1602(Lspeech/patts/LexiconReport;Lspeech/patts/LexiconReport$ReportLevel;)Lspeech/patts/LexiconReport$ReportLevel;

    .line 532
    return-object p0
.end method

.method public setLineNumber(I)Lspeech/patts/LexiconReport$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 509
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/LexiconReport;->hasLineNumber:Z
    invoke-static {v0, v1}, Lspeech/patts/LexiconReport;->access$1302(Lspeech/patts/LexiconReport;Z)Z

    .line 510
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    # setter for: Lspeech/patts/LexiconReport;->lineNumber_:I
    invoke-static {v0, p1}, Lspeech/patts/LexiconReport;->access$1402(Lspeech/patts/LexiconReport;I)I

    .line 511
    return-object p0
.end method

.method public setPart(Ljava/lang/String;)Lspeech/patts/LexiconReport$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 467
    if-nez p1, :cond_0

    .line 468
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 470
    :cond_0
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/LexiconReport;->hasPart:Z
    invoke-static {v0, v1}, Lspeech/patts/LexiconReport;->access$902(Lspeech/patts/LexiconReport;Z)Z

    .line 471
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    # setter for: Lspeech/patts/LexiconReport;->part_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/LexiconReport;->access$1002(Lspeech/patts/LexiconReport;Ljava/lang/String;)Ljava/lang/String;

    .line 472
    return-object p0
.end method

.method public setPosition(I)Lspeech/patts/LexiconReport$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 449
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/LexiconReport;->hasPosition:Z
    invoke-static {v0, v1}, Lspeech/patts/LexiconReport;->access$702(Lspeech/patts/LexiconReport;Z)Z

    .line 450
    iget-object v0, p0, Lspeech/patts/LexiconReport$Builder;->result:Lspeech/patts/LexiconReport;

    # setter for: Lspeech/patts/LexiconReport;->position_:I
    invoke-static {v0, p1}, Lspeech/patts/LexiconReport;->access$802(Lspeech/patts/LexiconReport;I)I

    .line 451
    return-object p0
.end method
