.class public final Lspeech/patts/NavigationFeature;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "NavigationFeature.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/NavigationFeature$1;,
        Lspeech/patts/NavigationFeature$Builder;,
        Lspeech/patts/NavigationFeature$DiphoneDirection;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/NavigationFeature;


# instance fields
.field private dependencyParent_:I

.field private diphoneDirection_:Lspeech/patts/NavigationFeature$DiphoneDirection;

.field private feature_:Ljava/lang/String;

.field private hasDependencyParent:Z

.field private hasDiphoneDirection:Z

.field private hasFeature:Z

.field private hasPosition:Z

.field private hasSPosition:Z

.field private hasStream:Z

.field private memoizedSerializedSize:I

.field private position_:I

.field private sPosition_:I

.field private stream_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 500
    new-instance v0, Lspeech/patts/NavigationFeature;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/NavigationFeature;-><init>(Z)V

    sput-object v0, Lspeech/patts/NavigationFeature;->defaultInstance:Lspeech/patts/NavigationFeature;

    .line 501
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 502
    sget-object v0, Lspeech/patts/NavigationFeature;->defaultInstance:Lspeech/patts/NavigationFeature;

    invoke-direct {v0}, Lspeech/patts/NavigationFeature;->initFields()V

    .line 503
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/NavigationFeature;->feature_:Ljava/lang/String;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/NavigationFeature;->stream_:Ljava/lang/String;

    .line 80
    iput v1, p0, Lspeech/patts/NavigationFeature;->position_:I

    .line 87
    iput v1, p0, Lspeech/patts/NavigationFeature;->sPosition_:I

    .line 94
    iput v1, p0, Lspeech/patts/NavigationFeature;->dependencyParent_:I

    .line 136
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/NavigationFeature;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/NavigationFeature;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/NavigationFeature$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/NavigationFeature$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/NavigationFeature;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/NavigationFeature;->feature_:Ljava/lang/String;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/NavigationFeature;->stream_:Ljava/lang/String;

    .line 80
    iput v1, p0, Lspeech/patts/NavigationFeature;->position_:I

    .line 87
    iput v1, p0, Lspeech/patts/NavigationFeature;->sPosition_:I

    .line 94
    iput v1, p0, Lspeech/patts/NavigationFeature;->dependencyParent_:I

    .line 136
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/NavigationFeature;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/NavigationFeature;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/NavigationFeature;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/NavigationFeature;->sPosition_:I

    return p1
.end method

.method static synthetic access$1102(Lspeech/patts/NavigationFeature;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/NavigationFeature;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/NavigationFeature;->hasDependencyParent:Z

    return p1
.end method

.method static synthetic access$1202(Lspeech/patts/NavigationFeature;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/NavigationFeature;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/NavigationFeature;->dependencyParent_:I

    return p1
.end method

.method static synthetic access$1302(Lspeech/patts/NavigationFeature;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/NavigationFeature;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/NavigationFeature;->hasDiphoneDirection:Z

    return p1
.end method

.method static synthetic access$1402(Lspeech/patts/NavigationFeature;Lspeech/patts/NavigationFeature$DiphoneDirection;)Lspeech/patts/NavigationFeature$DiphoneDirection;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/NavigationFeature;
    .param p1, "x1"    # Lspeech/patts/NavigationFeature$DiphoneDirection;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/NavigationFeature;->diphoneDirection_:Lspeech/patts/NavigationFeature$DiphoneDirection;

    return-object p1
.end method

.method static synthetic access$302(Lspeech/patts/NavigationFeature;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/NavigationFeature;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/NavigationFeature;->hasFeature:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/NavigationFeature;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/NavigationFeature;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/NavigationFeature;->feature_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/NavigationFeature;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/NavigationFeature;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/NavigationFeature;->hasStream:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/NavigationFeature;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/NavigationFeature;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/NavigationFeature;->stream_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lspeech/patts/NavigationFeature;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/NavigationFeature;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/NavigationFeature;->hasPosition:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/NavigationFeature;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/NavigationFeature;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/NavigationFeature;->position_:I

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/NavigationFeature;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/NavigationFeature;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/NavigationFeature;->hasSPosition:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/NavigationFeature;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/NavigationFeature;->defaultInstance:Lspeech/patts/NavigationFeature;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lspeech/patts/NavigationFeature$DiphoneDirection;->DIPHONE_UNSPECIFIED:Lspeech/patts/NavigationFeature$DiphoneDirection;

    iput-object v0, p0, Lspeech/patts/NavigationFeature;->diphoneDirection_:Lspeech/patts/NavigationFeature$DiphoneDirection;

    .line 107
    return-void
.end method

.method public static newBuilder()Lspeech/patts/NavigationFeature$Builder;
    .locals 1

    .prologue
    .line 237
    # invokes: Lspeech/patts/NavigationFeature$Builder;->create()Lspeech/patts/NavigationFeature$Builder;
    invoke-static {}, Lspeech/patts/NavigationFeature$Builder;->access$100()Lspeech/patts/NavigationFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/NavigationFeature;

    .prologue
    .line 240
    invoke-static {}, Lspeech/patts/NavigationFeature;->newBuilder()Lspeech/patts/NavigationFeature$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/NavigationFeature$Builder;->mergeFrom(Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getDefaultInstanceForType()Lspeech/patts/NavigationFeature;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/NavigationFeature;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/NavigationFeature;->defaultInstance:Lspeech/patts/NavigationFeature;

    return-object v0
.end method

.method public getDependencyParent()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lspeech/patts/NavigationFeature;->dependencyParent_:I

    return v0
.end method

.method public getDiphoneDirection()Lspeech/patts/NavigationFeature$DiphoneDirection;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lspeech/patts/NavigationFeature;->diphoneDirection_:Lspeech/patts/NavigationFeature$DiphoneDirection;

    return-object v0
.end method

.method public getFeature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lspeech/patts/NavigationFeature;->feature_:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lspeech/patts/NavigationFeature;->position_:I

    return v0
.end method

.method public getSPosition()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lspeech/patts/NavigationFeature;->sPosition_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 138
    iget v0, p0, Lspeech/patts/NavigationFeature;->memoizedSerializedSize:I

    .line 139
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 167
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 141
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 142
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->hasStream()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 143
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getStream()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 146
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->hasPosition()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 147
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getPosition()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 150
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->hasSPosition()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 151
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getSPosition()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 154
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->hasFeature()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 155
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getFeature()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 158
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->hasDiphoneDirection()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 159
    const/4 v2, 0x6

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getDiphoneDirection()Lspeech/patts/NavigationFeature$DiphoneDirection;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/NavigationFeature$DiphoneDirection;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 162
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->hasDependencyParent()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 163
    const/4 v2, 0x7

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getDependencyParent()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 166
    :cond_6
    iput v0, p0, Lspeech/patts/NavigationFeature;->memoizedSerializedSize:I

    move v1, v0

    .line 167
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getStream()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lspeech/patts/NavigationFeature;->stream_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDependencyParent()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lspeech/patts/NavigationFeature;->hasDependencyParent:Z

    return v0
.end method

.method public hasDiphoneDirection()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lspeech/patts/NavigationFeature;->hasDiphoneDirection:Z

    return v0
.end method

.method public hasFeature()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lspeech/patts/NavigationFeature;->hasFeature:Z

    return v0
.end method

.method public hasPosition()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lspeech/patts/NavigationFeature;->hasPosition:Z

    return v0
.end method

.method public hasSPosition()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lspeech/patts/NavigationFeature;->hasSPosition:Z

    return v0
.end method

.method public hasStream()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lspeech/patts/NavigationFeature;->hasStream:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lspeech/patts/NavigationFeature;->hasFeature:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 110
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->toBuilder()Lspeech/patts/NavigationFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/NavigationFeature$Builder;
    .locals 1

    .prologue
    .line 242
    invoke-static {p0}, Lspeech/patts/NavigationFeature;->newBuilder(Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getSerializedSize()I

    .line 116
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->hasStream()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getStream()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 119
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->hasPosition()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 122
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->hasSPosition()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getSPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 125
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->hasFeature()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 126
    const/4 v0, 0x4

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getFeature()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 128
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->hasDiphoneDirection()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 129
    const/4 v0, 0x6

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getDiphoneDirection()Lspeech/patts/NavigationFeature$DiphoneDirection;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/NavigationFeature$DiphoneDirection;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 131
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->hasDependencyParent()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 132
    const/4 v0, 0x7

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature;->getDependencyParent()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 134
    :cond_5
    return-void
.end method
