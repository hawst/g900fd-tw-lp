.class public final Lspeech/patts/SpeechParamSpec$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SpeechParamSpec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/SpeechParamSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/SpeechParamSpec;",
        "Lspeech/patts/SpeechParamSpec$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/SpeechParamSpec;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/SpeechParamSpec$Builder;
    .locals 1

    .prologue
    .line 151
    invoke-static {}, Lspeech/patts/SpeechParamSpec$Builder;->create()Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/SpeechParamSpec$Builder;
    .locals 3

    .prologue
    .line 160
    new-instance v0, Lspeech/patts/SpeechParamSpec$Builder;

    invoke-direct {v0}, Lspeech/patts/SpeechParamSpec$Builder;-><init>()V

    .line 161
    .local v0, "builder":Lspeech/patts/SpeechParamSpec$Builder;
    new-instance v1, Lspeech/patts/SpeechParamSpec;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/SpeechParamSpec;-><init>(Lspeech/patts/SpeechParamSpec$1;)V

    iput-object v1, v0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    .line 162
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lspeech/patts/SpeechParamSpec$Builder;->build()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/SpeechParamSpec;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/SpeechParamSpec$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    invoke-static {v0}, Lspeech/patts/SpeechParamSpec$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 193
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/SpeechParamSpec$Builder;->buildPartial()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/SpeechParamSpec;
    .locals 3

    .prologue
    .line 206
    iget-object v1, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    if-nez v1, :cond_0

    .line 207
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 210
    :cond_0
    iget-object v1, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    # getter for: Lspeech/patts/SpeechParamSpec;->windows_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/SpeechParamSpec;->access$300(Lspeech/patts/SpeechParamSpec;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 211
    iget-object v1, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    iget-object v2, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    # getter for: Lspeech/patts/SpeechParamSpec;->windows_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/SpeechParamSpec;->access$300(Lspeech/patts/SpeechParamSpec;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/SpeechParamSpec;->windows_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/SpeechParamSpec;->access$302(Lspeech/patts/SpeechParamSpec;Ljava/util/List;)Ljava/util/List;

    .line 214
    :cond_1
    iget-object v0, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    .line 215
    .local v0, "returnMe":Lspeech/patts/SpeechParamSpec;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    .line 216
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lspeech/patts/SpeechParamSpec$Builder;->clone()Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lspeech/patts/SpeechParamSpec$Builder;->clone()Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 151
    invoke-virtual {p0}, Lspeech/patts/SpeechParamSpec$Builder;->clone()Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/SpeechParamSpec$Builder;
    .locals 2

    .prologue
    .line 179
    invoke-static {}, Lspeech/patts/SpeechParamSpec$Builder;->create()Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    invoke-virtual {v0, v1}, Lspeech/patts/SpeechParamSpec$Builder;->mergeFrom(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    invoke-virtual {v0}, Lspeech/patts/SpeechParamSpec;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 151
    check-cast p1, Lspeech/patts/SpeechParamSpec;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/SpeechParamSpec$Builder;->mergeFrom(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 220
    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-object p0

    .line 221
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/SpeechParamSpec;->hasDimensionality()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 222
    invoke-virtual {p1}, Lspeech/patts/SpeechParamSpec;->getDimensionality()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/SpeechParamSpec$Builder;->setDimensionality(I)Lspeech/patts/SpeechParamSpec$Builder;

    .line 224
    :cond_2
    # getter for: Lspeech/patts/SpeechParamSpec;->windows_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/SpeechParamSpec;->access$300(Lspeech/patts/SpeechParamSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 225
    iget-object v0, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    # getter for: Lspeech/patts/SpeechParamSpec;->windows_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/SpeechParamSpec;->access$300(Lspeech/patts/SpeechParamSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 226
    iget-object v0, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/SpeechParamSpec;->windows_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/SpeechParamSpec;->access$302(Lspeech/patts/SpeechParamSpec;Ljava/util/List;)Ljava/util/List;

    .line 228
    :cond_3
    iget-object v0, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    # getter for: Lspeech/patts/SpeechParamSpec;->windows_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/SpeechParamSpec;->access$300(Lspeech/patts/SpeechParamSpec;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/SpeechParamSpec;->windows_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/SpeechParamSpec;->access$300(Lspeech/patts/SpeechParamSpec;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public setDimensionality(I)Lspeech/patts/SpeechParamSpec$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 271
    iget-object v0, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/SpeechParamSpec;->hasDimensionality:Z
    invoke-static {v0, v1}, Lspeech/patts/SpeechParamSpec;->access$402(Lspeech/patts/SpeechParamSpec;Z)Z

    .line 272
    iget-object v0, p0, Lspeech/patts/SpeechParamSpec$Builder;->result:Lspeech/patts/SpeechParamSpec;

    # setter for: Lspeech/patts/SpeechParamSpec;->dimensionality_:I
    invoke-static {v0, p1}, Lspeech/patts/SpeechParamSpec;->access$502(Lspeech/patts/SpeechParamSpec;I)I

    .line 273
    return-object p0
.end method
