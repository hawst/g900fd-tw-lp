.class public final Lspeech/patts/UtteranceStats;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "UtteranceStats.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/UtteranceStats$1;,
        Lspeech/patts/UtteranceStats$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/UtteranceStats;


# instance fields
.field private hasSentScore:Z

.field private hasSentScoreIdx:Z

.field private hasSentShortSegs:Z

.field private memoizedSerializedSize:I

.field private sentScoreIdx_:I

.field private sentScore_:F

.field private sentShortSegs_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 327
    new-instance v0, Lspeech/patts/UtteranceStats;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/UtteranceStats;-><init>(Z)V

    sput-object v0, Lspeech/patts/UtteranceStats;->defaultInstance:Lspeech/patts/UtteranceStats;

    .line 328
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 329
    sget-object v0, Lspeech/patts/UtteranceStats;->defaultInstance:Lspeech/patts/UtteranceStats;

    invoke-direct {v0}, Lspeech/patts/UtteranceStats;->initFields()V

    .line 330
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/UtteranceStats;->sentScore_:F

    .line 32
    iput v1, p0, Lspeech/patts/UtteranceStats;->sentScoreIdx_:I

    .line 39
    iput v1, p0, Lspeech/patts/UtteranceStats;->sentShortSegs_:I

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/UtteranceStats;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/UtteranceStats;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/UtteranceStats$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/UtteranceStats$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/UtteranceStats;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/UtteranceStats;->sentScore_:F

    .line 32
    iput v1, p0, Lspeech/patts/UtteranceStats;->sentScoreIdx_:I

    .line 39
    iput v1, p0, Lspeech/patts/UtteranceStats;->sentShortSegs_:I

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/UtteranceStats;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lspeech/patts/UtteranceStats;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UtteranceStats;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/UtteranceStats;->hasSentScore:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/UtteranceStats;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UtteranceStats;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/UtteranceStats;->sentScore_:F

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/UtteranceStats;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UtteranceStats;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/UtteranceStats;->hasSentScoreIdx:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/UtteranceStats;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UtteranceStats;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/UtteranceStats;->sentScoreIdx_:I

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/UtteranceStats;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UtteranceStats;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/UtteranceStats;->hasSentShortSegs:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/UtteranceStats;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/UtteranceStats;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/UtteranceStats;->sentShortSegs_:I

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/UtteranceStats;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/UtteranceStats;->defaultInstance:Lspeech/patts/UtteranceStats;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public static newBuilder()Lspeech/patts/UtteranceStats$Builder;
    .locals 1

    .prologue
    .line 152
    # invokes: Lspeech/patts/UtteranceStats$Builder;->create()Lspeech/patts/UtteranceStats$Builder;
    invoke-static {}, Lspeech/patts/UtteranceStats$Builder;->access$100()Lspeech/patts/UtteranceStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/UtteranceStats;)Lspeech/patts/UtteranceStats$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/UtteranceStats;

    .prologue
    .line 155
    invoke-static {}, Lspeech/patts/UtteranceStats;->newBuilder()Lspeech/patts/UtteranceStats$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/UtteranceStats$Builder;->mergeFrom(Lspeech/patts/UtteranceStats;)Lspeech/patts/UtteranceStats$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->getDefaultInstanceForType()Lspeech/patts/UtteranceStats;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/UtteranceStats;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/UtteranceStats;->defaultInstance:Lspeech/patts/UtteranceStats;

    return-object v0
.end method

.method public getSentScore()F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lspeech/patts/UtteranceStats;->sentScore_:F

    return v0
.end method

.method public getSentScoreIdx()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lspeech/patts/UtteranceStats;->sentScoreIdx_:I

    return v0
.end method

.method public getSentShortSegs()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lspeech/patts/UtteranceStats;->sentShortSegs_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 65
    iget v0, p0, Lspeech/patts/UtteranceStats;->memoizedSerializedSize:I

    .line 66
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 82
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 68
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 69
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->hasSentScore()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 70
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->getSentScore()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 73
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->hasSentScoreIdx()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 74
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->getSentScoreIdx()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 77
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->hasSentShortSegs()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 78
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->getSentShortSegs()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 81
    :cond_3
    iput v0, p0, Lspeech/patts/UtteranceStats;->memoizedSerializedSize:I

    move v1, v0

    .line 82
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasSentScore()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/UtteranceStats;->hasSentScore:Z

    return v0
.end method

.method public hasSentScoreIdx()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/UtteranceStats;->hasSentScoreIdx:Z

    return v0
.end method

.method public hasSentShortSegs()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lspeech/patts/UtteranceStats;->hasSentShortSegs:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->toBuilder()Lspeech/patts/UtteranceStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/UtteranceStats$Builder;
    .locals 1

    .prologue
    .line 157
    invoke-static {p0}, Lspeech/patts/UtteranceStats;->newBuilder(Lspeech/patts/UtteranceStats;)Lspeech/patts/UtteranceStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->getSerializedSize()I

    .line 52
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->hasSentScore()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->getSentScore()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 55
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->hasSentScoreIdx()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->getSentScoreIdx()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 58
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->hasSentShortSegs()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 59
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/UtteranceStats;->getSentShortSegs()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 61
    :cond_2
    return-void
.end method
