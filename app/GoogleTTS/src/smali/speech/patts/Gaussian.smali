.class public final Lspeech/patts/Gaussian;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Gaussian.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/Gaussian$1;,
        Lspeech/patts/Gaussian$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/Gaussian;


# instance fields
.field private covariancesMemoizedSerializedSize:I

.field private covariances_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private diagonal_:Z

.field private gconst_:F

.field private hasDiagonal:Z

.field private hasGconst:Z

.field private hasPrior:Z

.field private meansMemoizedSerializedSize:I

.field private means_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private prior_:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 503
    new-instance v0, Lspeech/patts/Gaussian;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/Gaussian;-><init>(Z)V

    sput-object v0, Lspeech/patts/Gaussian;->defaultInstance:Lspeech/patts/Gaussian;

    .line 504
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 505
    sget-object v0, Lspeech/patts/Gaussian;->defaultInstance:Lspeech/patts/Gaussian;

    invoke-direct {v0}, Lspeech/patts/Gaussian;->initFields()V

    .line 506
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lspeech/patts/Gaussian;->prior_:F

    .line 32
    iput v0, p0, Lspeech/patts/Gaussian;->gconst_:F

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/patts/Gaussian;->diagonal_:Z

    .line 45
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Gaussian;->means_:Ljava/util/List;

    .line 54
    iput v1, p0, Lspeech/patts/Gaussian;->meansMemoizedSerializedSize:I

    .line 58
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;

    .line 67
    iput v1, p0, Lspeech/patts/Gaussian;->covariancesMemoizedSerializedSize:I

    .line 103
    iput v1, p0, Lspeech/patts/Gaussian;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/Gaussian;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/Gaussian$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/Gaussian$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/Gaussian;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lspeech/patts/Gaussian;->prior_:F

    .line 32
    iput v0, p0, Lspeech/patts/Gaussian;->gconst_:F

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/patts/Gaussian;->diagonal_:Z

    .line 45
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Gaussian;->means_:Ljava/util/List;

    .line 54
    iput v1, p0, Lspeech/patts/Gaussian;->meansMemoizedSerializedSize:I

    .line 58
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;

    .line 67
    iput v1, p0, Lspeech/patts/Gaussian;->covariancesMemoizedSerializedSize:I

    .line 103
    iput v1, p0, Lspeech/patts/Gaussian;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/Gaussian;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Gaussian;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Gaussian;->diagonal_:Z

    return p1
.end method

.method static synthetic access$300(Lspeech/patts/Gaussian;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Gaussian;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Gaussian;->means_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/Gaussian;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Gaussian;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Gaussian;->means_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lspeech/patts/Gaussian;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Gaussian;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lspeech/patts/Gaussian;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Gaussian;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/Gaussian;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Gaussian;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Gaussian;->hasPrior:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/Gaussian;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Gaussian;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Gaussian;->prior_:F

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/Gaussian;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Gaussian;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Gaussian;->hasGconst:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/Gaussian;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Gaussian;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Gaussian;->gconst_:F

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/Gaussian;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Gaussian;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Gaussian;->hasDiagonal:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/Gaussian;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/Gaussian;->defaultInstance:Lspeech/patts/Gaussian;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public static newBuilder()Lspeech/patts/Gaussian$Builder;
    .locals 1

    .prologue
    .line 214
    # invokes: Lspeech/patts/Gaussian$Builder;->create()Lspeech/patts/Gaussian$Builder;
    invoke-static {}, Lspeech/patts/Gaussian$Builder;->access$100()Lspeech/patts/Gaussian$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/Gaussian;)Lspeech/patts/Gaussian$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/Gaussian;

    .prologue
    .line 217
    invoke-static {}, Lspeech/patts/Gaussian;->newBuilder()Lspeech/patts/Gaussian$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/Gaussian$Builder;->mergeFrom(Lspeech/patts/Gaussian;)Lspeech/patts/Gaussian$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCovariancesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getDefaultInstanceForType()Lspeech/patts/Gaussian;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/Gaussian;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/Gaussian;->defaultInstance:Lspeech/patts/Gaussian;

    return-object v0
.end method

.method public getDiagonal()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lspeech/patts/Gaussian;->diagonal_:Z

    return v0
.end method

.method public getGconst()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lspeech/patts/Gaussian;->gconst_:F

    return v0
.end method

.method public getMeansList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lspeech/patts/Gaussian;->means_:Ljava/util/List;

    return-object v0
.end method

.method public getPrior()F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lspeech/patts/Gaussian;->prior_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 105
    iget v1, p0, Lspeech/patts/Gaussian;->memoizedSerializedSize:I

    .line 106
    .local v1, "size":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 144
    .end local v1    # "size":I
    .local v2, "size":I
    :goto_0
    return v2

    .line 108
    .end local v2    # "size":I
    .restart local v1    # "size":I
    :cond_0
    const/4 v1, 0x0

    .line 109
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->hasPrior()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 110
    const/4 v3, 0x1

    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getPrior()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v1, v3

    .line 113
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->hasGconst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 114
    const/4 v3, 0x2

    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getGconst()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v1, v3

    .line 117
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->hasDiagonal()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 118
    const/4 v3, 0x3

    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getDiagonal()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v1, v3

    .line 122
    :cond_3
    const/4 v0, 0x0

    .line 123
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getMeansList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 124
    add-int/2addr v1, v0

    .line 125
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getMeansList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 126
    add-int/lit8 v1, v1, 0x1

    .line 127
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 130
    :cond_4
    iput v0, p0, Lspeech/patts/Gaussian;->meansMemoizedSerializedSize:I

    .line 133
    const/4 v0, 0x0

    .line 134
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getCovariancesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 135
    add-int/2addr v1, v0

    .line 136
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getCovariancesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 137
    add-int/lit8 v1, v1, 0x1

    .line 138
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 141
    :cond_5
    iput v0, p0, Lspeech/patts/Gaussian;->covariancesMemoizedSerializedSize:I

    .line 143
    iput v1, p0, Lspeech/patts/Gaussian;->memoizedSerializedSize:I

    move v2, v1

    .line 144
    .end local v1    # "size":I
    .restart local v2    # "size":I
    goto :goto_0
.end method

.method public hasDiagonal()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lspeech/patts/Gaussian;->hasDiagonal:Z

    return v0
.end method

.method public hasGconst()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/Gaussian;->hasGconst:Z

    return v0
.end method

.method public hasPrior()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/Gaussian;->hasPrior:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->toBuilder()Lspeech/patts/Gaussian$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/Gaussian$Builder;
    .locals 1

    .prologue
    .line 219
    invoke-static {p0}, Lspeech/patts/Gaussian;->newBuilder(Lspeech/patts/Gaussian;)Lspeech/patts/Gaussian$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getSerializedSize()I

    .line 78
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->hasPrior()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getPrior()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 81
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->hasGconst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 82
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getGconst()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 84
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->hasDiagonal()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 85
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getDiagonal()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 87
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getMeansList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 88
    const/16 v2, 0x22

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 89
    iget v2, p0, Lspeech/patts/Gaussian;->meansMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 91
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getMeansList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 92
    .local v0, "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_0

    .line 94
    .end local v0    # "element":F
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getCovariancesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 95
    const/16 v2, 0x2a

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 96
    iget v2, p0, Lspeech/patts/Gaussian;->covariancesMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 98
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/Gaussian;->getCovariancesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 99
    .restart local v0    # "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_1

    .line 101
    .end local v0    # "element":F
    :cond_6
    return-void
.end method
