.class public final Lspeech/patts/FeatAndVals;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FeatAndVals.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/FeatAndVals$1;,
        Lspeech/patts/FeatAndVals$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/FeatAndVals;


# instance fields
.field private feat_:Ljava/lang/String;

.field private hasFeat:Z

.field private memoizedSerializedSize:I

.field private weight_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 331
    new-instance v0, Lspeech/patts/FeatAndVals;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/FeatAndVals;-><init>(Z)V

    sput-object v0, Lspeech/patts/FeatAndVals;->defaultInstance:Lspeech/patts/FeatAndVals;

    .line 332
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 333
    sget-object v0, Lspeech/patts/FeatAndVals;->defaultInstance:Lspeech/patts/FeatAndVals;

    invoke-direct {v0}, Lspeech/patts/FeatAndVals;->initFields()V

    .line 334
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/FeatAndVals;->feat_:Ljava/lang/String;

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/FeatAndVals;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/FeatAndVals;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/FeatAndVals$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/FeatAndVals$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/FeatAndVals;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/FeatAndVals;->feat_:Ljava/lang/String;

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/FeatAndVals;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/FeatAndVals;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FeatAndVals;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/FeatAndVals;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FeatAndVals;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/FeatAndVals;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FeatAndVals;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FeatAndVals;->hasFeat:Z

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/FeatAndVals;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FeatAndVals;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FeatAndVals;->feat_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/FeatAndVals;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/FeatAndVals;->defaultInstance:Lspeech/patts/FeatAndVals;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public static newBuilder()Lspeech/patts/FeatAndVals$Builder;
    .locals 1

    .prologue
    .line 146
    # invokes: Lspeech/patts/FeatAndVals$Builder;->create()Lspeech/patts/FeatAndVals$Builder;
    invoke-static {}, Lspeech/patts/FeatAndVals$Builder;->access$100()Lspeech/patts/FeatAndVals$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/FeatAndVals;)Lspeech/patts/FeatAndVals$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/FeatAndVals;

    .prologue
    .line 149
    invoke-static {}, Lspeech/patts/FeatAndVals;->newBuilder()Lspeech/patts/FeatAndVals$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/FeatAndVals$Builder;->mergeFrom(Lspeech/patts/FeatAndVals;)Lspeech/patts/FeatAndVals$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals;->getDefaultInstanceForType()Lspeech/patts/FeatAndVals;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/FeatAndVals;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/FeatAndVals;->defaultInstance:Lspeech/patts/FeatAndVals;

    return-object v0
.end method

.method public getFeat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/FeatAndVals;->feat_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 61
    iget v1, p0, Lspeech/patts/FeatAndVals;->memoizedSerializedSize:I

    .line 62
    .local v1, "size":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 76
    .end local v1    # "size":I
    .local v2, "size":I
    :goto_0
    return v2

    .line 64
    .end local v2    # "size":I
    .restart local v1    # "size":I
    :cond_0
    const/4 v1, 0x0

    .line 65
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals;->hasFeat()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 66
    const/4 v3, 0x1

    invoke-virtual {p0}, Lspeech/patts/FeatAndVals;->getFeat()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 70
    :cond_1
    const/4 v0, 0x0

    .line 71
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals;->getWeightList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 72
    add-int/2addr v1, v0

    .line 73
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals;->getWeightList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v1, v3

    .line 75
    iput v1, p0, Lspeech/patts/FeatAndVals;->memoizedSerializedSize:I

    move v2, v1

    .line 76
    .end local v1    # "size":I
    .restart local v2    # "size":I
    goto :goto_0
.end method

.method public getWeightList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lspeech/patts/FeatAndVals;->weight_:Ljava/util/List;

    return-object v0
.end method

.method public hasFeat()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/FeatAndVals;->hasFeat:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lspeech/patts/FeatAndVals;->hasFeat:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 45
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals;->toBuilder()Lspeech/patts/FeatAndVals$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/FeatAndVals$Builder;
    .locals 1

    .prologue
    .line 151
    invoke-static {p0}, Lspeech/patts/FeatAndVals;->newBuilder(Lspeech/patts/FeatAndVals;)Lspeech/patts/FeatAndVals$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals;->getSerializedSize()I

    .line 51
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals;->hasFeat()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/FeatAndVals;->getFeat()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 54
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FeatAndVals;->getWeightList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 55
    .local v0, "element":F
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    goto :goto_0

    .line 57
    .end local v0    # "element":F
    :cond_1
    return-void
.end method
