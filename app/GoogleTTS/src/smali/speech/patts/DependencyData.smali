.class public final Lspeech/patts/DependencyData;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "DependencyData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/DependencyData$1;,
        Lspeech/patts/DependencyData$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/DependencyData;


# instance fields
.field private boundaryStrengthToRight_:F

.field private crossingLinksToRight_:I

.field private depth_:I

.field private hasBoundaryStrengthToRight:Z

.field private hasCrossingLinksToRight:Z

.field private hasDepth:Z

.field private hasHead:Z

.field private hasLabel:Z

.field private hasLinkStepsToRight:Z

.field private hasOffsetFromHead:Z

.field private hasSizeToLeftLowerBound:Z

.field private hasSizeToLeftUpperBound:Z

.field private hasSizeToRightLowerBound:Z

.field private hasSizeToRightUpperBound:Z

.field private hasWeightedBoundaryStrengthToRight:Z

.field private hasWeightedCrossingLinksToRight:Z

.field private head_:I

.field private label_:Ljava/lang/String;

.field private linkStepsToRight_:I

.field private memoizedSerializedSize:I

.field private offsetFromHead_:I

.field private sizeToLeftLowerBound_:I

.field private sizeToLeftUpperBound_:I

.field private sizeToRightLowerBound_:I

.field private sizeToRightUpperBound_:I

.field private weightedBoundaryStrengthToRight_:F

.field private weightedCrossingLinksToRight_:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 720
    new-instance v0, Lspeech/patts/DependencyData;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/DependencyData;-><init>(Z)V

    sput-object v0, Lspeech/patts/DependencyData;->defaultInstance:Lspeech/patts/DependencyData;

    .line 721
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 722
    sget-object v0, Lspeech/patts/DependencyData;->defaultInstance:Lspeech/patts/DependencyData;

    invoke-direct {v0}, Lspeech/patts/DependencyData;->initFields()V

    .line 723
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v1, p0, Lspeech/patts/DependencyData;->head_:I

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/DependencyData;->label_:Ljava/lang/String;

    .line 39
    iput v1, p0, Lspeech/patts/DependencyData;->depth_:I

    .line 46
    iput v1, p0, Lspeech/patts/DependencyData;->offsetFromHead_:I

    .line 53
    iput v1, p0, Lspeech/patts/DependencyData;->sizeToLeftLowerBound_:I

    .line 60
    iput v1, p0, Lspeech/patts/DependencyData;->sizeToLeftUpperBound_:I

    .line 67
    iput v1, p0, Lspeech/patts/DependencyData;->sizeToRightLowerBound_:I

    .line 74
    iput v1, p0, Lspeech/patts/DependencyData;->sizeToRightUpperBound_:I

    .line 81
    iput v1, p0, Lspeech/patts/DependencyData;->crossingLinksToRight_:I

    .line 88
    iput v2, p0, Lspeech/patts/DependencyData;->boundaryStrengthToRight_:F

    .line 95
    iput v1, p0, Lspeech/patts/DependencyData;->linkStepsToRight_:I

    .line 102
    iput v2, p0, Lspeech/patts/DependencyData;->weightedCrossingLinksToRight_:F

    .line 109
    iput v2, p0, Lspeech/patts/DependencyData;->weightedBoundaryStrengthToRight_:F

    .line 163
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/DependencyData;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/DependencyData;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/DependencyData$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/DependencyData$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/DependencyData;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3
    .param p1, "noInit"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v1, p0, Lspeech/patts/DependencyData;->head_:I

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/DependencyData;->label_:Ljava/lang/String;

    .line 39
    iput v1, p0, Lspeech/patts/DependencyData;->depth_:I

    .line 46
    iput v1, p0, Lspeech/patts/DependencyData;->offsetFromHead_:I

    .line 53
    iput v1, p0, Lspeech/patts/DependencyData;->sizeToLeftLowerBound_:I

    .line 60
    iput v1, p0, Lspeech/patts/DependencyData;->sizeToLeftUpperBound_:I

    .line 67
    iput v1, p0, Lspeech/patts/DependencyData;->sizeToRightLowerBound_:I

    .line 74
    iput v1, p0, Lspeech/patts/DependencyData;->sizeToRightUpperBound_:I

    .line 81
    iput v1, p0, Lspeech/patts/DependencyData;->crossingLinksToRight_:I

    .line 88
    iput v2, p0, Lspeech/patts/DependencyData;->boundaryStrengthToRight_:F

    .line 95
    iput v1, p0, Lspeech/patts/DependencyData;->linkStepsToRight_:I

    .line 102
    iput v2, p0, Lspeech/patts/DependencyData;->weightedCrossingLinksToRight_:F

    .line 109
    iput v2, p0, Lspeech/patts/DependencyData;->weightedBoundaryStrengthToRight_:F

    .line 163
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/DependencyData;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/DependencyData;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/DependencyData;->offsetFromHead_:I

    return p1
.end method

.method static synthetic access$1102(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasSizeToLeftLowerBound:Z

    return p1
.end method

.method static synthetic access$1202(Lspeech/patts/DependencyData;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/DependencyData;->sizeToLeftLowerBound_:I

    return p1
.end method

.method static synthetic access$1302(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasSizeToLeftUpperBound:Z

    return p1
.end method

.method static synthetic access$1402(Lspeech/patts/DependencyData;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/DependencyData;->sizeToLeftUpperBound_:I

    return p1
.end method

.method static synthetic access$1502(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasSizeToRightLowerBound:Z

    return p1
.end method

.method static synthetic access$1602(Lspeech/patts/DependencyData;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/DependencyData;->sizeToRightLowerBound_:I

    return p1
.end method

.method static synthetic access$1702(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasSizeToRightUpperBound:Z

    return p1
.end method

.method static synthetic access$1802(Lspeech/patts/DependencyData;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/DependencyData;->sizeToRightUpperBound_:I

    return p1
.end method

.method static synthetic access$1902(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasCrossingLinksToRight:Z

    return p1
.end method

.method static synthetic access$2002(Lspeech/patts/DependencyData;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/DependencyData;->crossingLinksToRight_:I

    return p1
.end method

.method static synthetic access$2102(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasBoundaryStrengthToRight:Z

    return p1
.end method

.method static synthetic access$2202(Lspeech/patts/DependencyData;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/DependencyData;->boundaryStrengthToRight_:F

    return p1
.end method

.method static synthetic access$2302(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasLinkStepsToRight:Z

    return p1
.end method

.method static synthetic access$2402(Lspeech/patts/DependencyData;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/DependencyData;->linkStepsToRight_:I

    return p1
.end method

.method static synthetic access$2502(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasWeightedCrossingLinksToRight:Z

    return p1
.end method

.method static synthetic access$2602(Lspeech/patts/DependencyData;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/DependencyData;->weightedCrossingLinksToRight_:F

    return p1
.end method

.method static synthetic access$2702(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasWeightedBoundaryStrengthToRight:Z

    return p1
.end method

.method static synthetic access$2802(Lspeech/patts/DependencyData;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/DependencyData;->weightedBoundaryStrengthToRight_:F

    return p1
.end method

.method static synthetic access$302(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasHead:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/DependencyData;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/DependencyData;->head_:I

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasLabel:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/DependencyData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/DependencyData;->label_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasDepth:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/DependencyData;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/DependencyData;->depth_:I

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/DependencyData;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/DependencyData;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/DependencyData;->hasOffsetFromHead:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/DependencyData;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/DependencyData;->defaultInstance:Lspeech/patts/DependencyData;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public static newBuilder()Lspeech/patts/DependencyData$Builder;
    .locals 1

    .prologue
    .line 292
    # invokes: Lspeech/patts/DependencyData$Builder;->create()Lspeech/patts/DependencyData$Builder;
    invoke-static {}, Lspeech/patts/DependencyData$Builder;->access$100()Lspeech/patts/DependencyData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/DependencyData;)Lspeech/patts/DependencyData$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/DependencyData;

    .prologue
    .line 295
    invoke-static {}, Lspeech/patts/DependencyData;->newBuilder()Lspeech/patts/DependencyData$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/DependencyData$Builder;->mergeFrom(Lspeech/patts/DependencyData;)Lspeech/patts/DependencyData$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBoundaryStrengthToRight()F
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lspeech/patts/DependencyData;->boundaryStrengthToRight_:F

    return v0
.end method

.method public getCrossingLinksToRight()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lspeech/patts/DependencyData;->crossingLinksToRight_:I

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getDefaultInstanceForType()Lspeech/patts/DependencyData;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/DependencyData;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/DependencyData;->defaultInstance:Lspeech/patts/DependencyData;

    return-object v0
.end method

.method public getDepth()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lspeech/patts/DependencyData;->depth_:I

    return v0
.end method

.method public getHead()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lspeech/patts/DependencyData;->head_:I

    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lspeech/patts/DependencyData;->label_:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkStepsToRight()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lspeech/patts/DependencyData;->linkStepsToRight_:I

    return v0
.end method

.method public getOffsetFromHead()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lspeech/patts/DependencyData;->offsetFromHead_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 165
    iget v0, p0, Lspeech/patts/DependencyData;->memoizedSerializedSize:I

    .line 166
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 222
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 168
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 169
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasHead()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 170
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getHead()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 173
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasLabel()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 174
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 177
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasDepth()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 178
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getDepth()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 181
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasOffsetFromHead()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 182
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getOffsetFromHead()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 185
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasSizeToLeftLowerBound()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 186
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getSizeToLeftLowerBound()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 189
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasSizeToLeftUpperBound()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 190
    const/4 v2, 0x6

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getSizeToLeftUpperBound()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 193
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasSizeToRightLowerBound()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 194
    const/4 v2, 0x7

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getSizeToRightLowerBound()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 197
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasSizeToRightUpperBound()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 198
    const/16 v2, 0x8

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getSizeToRightUpperBound()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 201
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasCrossingLinksToRight()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 202
    const/16 v2, 0x9

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getCrossingLinksToRight()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 205
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasBoundaryStrengthToRight()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 206
    const/16 v2, 0xa

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getBoundaryStrengthToRight()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 209
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasLinkStepsToRight()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 210
    const/16 v2, 0xb

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getLinkStepsToRight()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 213
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasWeightedCrossingLinksToRight()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 214
    const/16 v2, 0xc

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getWeightedCrossingLinksToRight()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 217
    :cond_c
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasWeightedBoundaryStrengthToRight()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 218
    const/16 v2, 0xd

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getWeightedBoundaryStrengthToRight()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 221
    :cond_d
    iput v0, p0, Lspeech/patts/DependencyData;->memoizedSerializedSize:I

    move v1, v0

    .line 222
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto/16 :goto_0
.end method

.method public getSizeToLeftLowerBound()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lspeech/patts/DependencyData;->sizeToLeftLowerBound_:I

    return v0
.end method

.method public getSizeToLeftUpperBound()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lspeech/patts/DependencyData;->sizeToLeftUpperBound_:I

    return v0
.end method

.method public getSizeToRightLowerBound()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lspeech/patts/DependencyData;->sizeToRightLowerBound_:I

    return v0
.end method

.method public getSizeToRightUpperBound()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lspeech/patts/DependencyData;->sizeToRightUpperBound_:I

    return v0
.end method

.method public getWeightedBoundaryStrengthToRight()F
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lspeech/patts/DependencyData;->weightedBoundaryStrengthToRight_:F

    return v0
.end method

.method public getWeightedCrossingLinksToRight()F
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lspeech/patts/DependencyData;->weightedCrossingLinksToRight_:F

    return v0
.end method

.method public hasBoundaryStrengthToRight()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasBoundaryStrengthToRight:Z

    return v0
.end method

.method public hasCrossingLinksToRight()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasCrossingLinksToRight:Z

    return v0
.end method

.method public hasDepth()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasDepth:Z

    return v0
.end method

.method public hasHead()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasHead:Z

    return v0
.end method

.method public hasLabel()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasLabel:Z

    return v0
.end method

.method public hasLinkStepsToRight()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasLinkStepsToRight:Z

    return v0
.end method

.method public hasOffsetFromHead()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasOffsetFromHead:Z

    return v0
.end method

.method public hasSizeToLeftLowerBound()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasSizeToLeftLowerBound:Z

    return v0
.end method

.method public hasSizeToLeftUpperBound()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasSizeToLeftUpperBound:Z

    return v0
.end method

.method public hasSizeToRightLowerBound()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasSizeToRightLowerBound:Z

    return v0
.end method

.method public hasSizeToRightUpperBound()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasSizeToRightUpperBound:Z

    return v0
.end method

.method public hasWeightedBoundaryStrengthToRight()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasWeightedBoundaryStrengthToRight:Z

    return v0
.end method

.method public hasWeightedCrossingLinksToRight()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lspeech/patts/DependencyData;->hasWeightedCrossingLinksToRight:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->toBuilder()Lspeech/patts/DependencyData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/DependencyData$Builder;
    .locals 1

    .prologue
    .line 297
    invoke-static {p0}, Lspeech/patts/DependencyData;->newBuilder(Lspeech/patts/DependencyData;)Lspeech/patts/DependencyData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getSerializedSize()I

    .line 122
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasHead()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getHead()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 125
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasLabel()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 128
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasDepth()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getDepth()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 131
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasOffsetFromHead()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 132
    const/4 v0, 0x4

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getOffsetFromHead()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 134
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasSizeToLeftLowerBound()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135
    const/4 v0, 0x5

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getSizeToLeftLowerBound()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 137
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasSizeToLeftUpperBound()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 138
    const/4 v0, 0x6

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getSizeToLeftUpperBound()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 140
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasSizeToRightLowerBound()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 141
    const/4 v0, 0x7

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getSizeToRightLowerBound()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 143
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasSizeToRightUpperBound()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 144
    const/16 v0, 0x8

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getSizeToRightUpperBound()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 146
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasCrossingLinksToRight()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 147
    const/16 v0, 0x9

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getCrossingLinksToRight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 149
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasBoundaryStrengthToRight()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 150
    const/16 v0, 0xa

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getBoundaryStrengthToRight()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 152
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasLinkStepsToRight()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 153
    const/16 v0, 0xb

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getLinkStepsToRight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 155
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasWeightedCrossingLinksToRight()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 156
    const/16 v0, 0xc

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getWeightedCrossingLinksToRight()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 158
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/DependencyData;->hasWeightedBoundaryStrengthToRight()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 159
    const/16 v0, 0xd

    invoke-virtual {p0}, Lspeech/patts/DependencyData;->getWeightedBoundaryStrengthToRight()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 161
    :cond_c
    return-void
.end method
