.class public final Lspeech/patts/AttributeMapProto;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AttributeMapProto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/AttributeMapProto$1;,
        Lspeech/patts/AttributeMapProto$Builder;,
        Lspeech/patts/AttributeMapProto$Mapping;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/AttributeMapProto;


# instance fields
.field private cases_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/AttributeMapProto$Mapping;",
            ">;"
        }
    .end annotation
.end field

.field private defaultFeatures_:Ljava/lang/String;

.field private definiteness_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/AttributeMapProto$Mapping;",
            ">;"
        }
    .end annotation
.end field

.field private genderNumbers_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/AttributeMapProto$Mapping;",
            ">;"
        }
    .end annotation
.end field

.field private hasDefaultFeatures:Z

.field private memoizedSerializedSize:I

.field private numbers_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/AttributeMapProto$Mapping;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 951
    new-instance v0, Lspeech/patts/AttributeMapProto;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/AttributeMapProto;-><init>(Z)V

    sput-object v0, Lspeech/patts/AttributeMapProto;->defaultInstance:Lspeech/patts/AttributeMapProto;

    .line 952
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 953
    sget-object v0, Lspeech/patts/AttributeMapProto;->defaultInstance:Lspeech/patts/AttributeMapProto;

    invoke-direct {v0}, Lspeech/patts/AttributeMapProto;->initFields()V

    .line 954
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 367
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/AttributeMapProto;->defaultFeatures_:Ljava/lang/String;

    .line 373
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;

    .line 385
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;

    .line 397
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;

    .line 409
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;

    .line 458
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/AttributeMapProto;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/AttributeMapProto;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/AttributeMapProto$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/AttributeMapProto$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/AttributeMapProto;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 367
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/AttributeMapProto;->defaultFeatures_:Ljava/lang/String;

    .line 373
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;

    .line 385
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;

    .line 397
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;

    .line 409
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;

    .line 458
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/AttributeMapProto;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1200(Lspeech/patts/AttributeMapProto;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1202(Lspeech/patts/AttributeMapProto;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1300(Lspeech/patts/AttributeMapProto;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1302(Lspeech/patts/AttributeMapProto;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1400(Lspeech/patts/AttributeMapProto;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1402(Lspeech/patts/AttributeMapProto;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1500(Lspeech/patts/AttributeMapProto;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1502(Lspeech/patts/AttributeMapProto;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1602(Lspeech/patts/AttributeMapProto;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/AttributeMapProto;->hasDefaultFeatures:Z

    return p1
.end method

.method static synthetic access$1702(Lspeech/patts/AttributeMapProto;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/AttributeMapProto;->defaultFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/AttributeMapProto;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/AttributeMapProto;->defaultInstance:Lspeech/patts/AttributeMapProto;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 420
    return-void
.end method

.method public static newBuilder()Lspeech/patts/AttributeMapProto$Builder;
    .locals 1

    .prologue
    .line 555
    # invokes: Lspeech/patts/AttributeMapProto$Builder;->create()Lspeech/patts/AttributeMapProto$Builder;
    invoke-static {}, Lspeech/patts/AttributeMapProto$Builder;->access$1000()Lspeech/patts/AttributeMapProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/AttributeMapProto;)Lspeech/patts/AttributeMapProto$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/AttributeMapProto;

    .prologue
    .line 558
    invoke-static {}, Lspeech/patts/AttributeMapProto;->newBuilder()Lspeech/patts/AttributeMapProto$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/AttributeMapProto$Builder;->mergeFrom(Lspeech/patts/AttributeMapProto;)Lspeech/patts/AttributeMapProto$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCasesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/AttributeMapProto$Mapping;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388
    iget-object v0, p0, Lspeech/patts/AttributeMapProto;->cases_:Ljava/util/List;

    return-object v0
.end method

.method public getDefaultFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lspeech/patts/AttributeMapProto;->defaultFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getDefaultInstanceForType()Lspeech/patts/AttributeMapProto;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/AttributeMapProto;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/AttributeMapProto;->defaultInstance:Lspeech/patts/AttributeMapProto;

    return-object v0
.end method

.method public getDefinitenessList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/AttributeMapProto$Mapping;",
            ">;"
        }
    .end annotation

    .prologue
    .line 400
    iget-object v0, p0, Lspeech/patts/AttributeMapProto;->definiteness_:Ljava/util/List;

    return-object v0
.end method

.method public getGenderNumbersList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/AttributeMapProto$Mapping;",
            ">;"
        }
    .end annotation

    .prologue
    .line 376
    iget-object v0, p0, Lspeech/patts/AttributeMapProto;->genderNumbers_:Ljava/util/List;

    return-object v0
.end method

.method public getNumbersList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/AttributeMapProto$Mapping;",
            ">;"
        }
    .end annotation

    .prologue
    .line 412
    iget-object v0, p0, Lspeech/patts/AttributeMapProto;->numbers_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 460
    iget v2, p0, Lspeech/patts/AttributeMapProto;->memoizedSerializedSize:I

    .line 461
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 485
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 463
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 464
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->hasDefaultFeatures()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 465
    const/4 v4, 0x1

    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getDefaultFeatures()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 468
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getGenderNumbersList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AttributeMapProto$Mapping;

    .line 469
    .local v0, "element":Lspeech/patts/AttributeMapProto$Mapping;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 471
    goto :goto_1

    .line 472
    .end local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getCasesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AttributeMapProto$Mapping;

    .line 473
    .restart local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 475
    goto :goto_2

    .line 476
    .end local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getDefinitenessList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AttributeMapProto$Mapping;

    .line 477
    .restart local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    const/4 v4, 0x4

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 479
    goto :goto_3

    .line 480
    .end local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getNumbersList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AttributeMapProto$Mapping;

    .line 481
    .restart local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    const/4 v4, 0x5

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 483
    goto :goto_4

    .line 484
    .end local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    :cond_5
    iput v2, p0, Lspeech/patts/AttributeMapProto;->memoizedSerializedSize:I

    move v3, v2

    .line 485
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto/16 :goto_0
.end method

.method public hasDefaultFeatures()Z
    .locals 1

    .prologue
    .line 368
    iget-boolean v0, p0, Lspeech/patts/AttributeMapProto;->hasDefaultFeatures:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 422
    iget-boolean v3, p0, Lspeech/patts/AttributeMapProto;->hasDefaultFeatures:Z

    if-nez v3, :cond_0

    .line 435
    :goto_0
    return v2

    .line 423
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getGenderNumbersList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AttributeMapProto$Mapping;

    .line 424
    .local v0, "element":Lspeech/patts/AttributeMapProto$Mapping;
    invoke-virtual {v0}, Lspeech/patts/AttributeMapProto$Mapping;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 426
    .end local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getCasesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AttributeMapProto$Mapping;

    .line 427
    .restart local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    invoke-virtual {v0}, Lspeech/patts/AttributeMapProto$Mapping;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_0

    .line 429
    .end local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getDefinitenessList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AttributeMapProto$Mapping;

    .line 430
    .restart local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    invoke-virtual {v0}, Lspeech/patts/AttributeMapProto$Mapping;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_5

    goto :goto_0

    .line 432
    .end local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getNumbersList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AttributeMapProto$Mapping;

    .line 433
    .restart local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    invoke-virtual {v0}, Lspeech/patts/AttributeMapProto$Mapping;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_7

    goto :goto_0

    .line 435
    .end local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    :cond_8
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->toBuilder()Lspeech/patts/AttributeMapProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/AttributeMapProto$Builder;
    .locals 1

    .prologue
    .line 560
    invoke-static {p0}, Lspeech/patts/AttributeMapProto;->newBuilder(Lspeech/patts/AttributeMapProto;)Lspeech/patts/AttributeMapProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 440
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getSerializedSize()I

    .line 441
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->hasDefaultFeatures()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 442
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getDefaultFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 444
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getGenderNumbersList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AttributeMapProto$Mapping;

    .line 445
    .local v0, "element":Lspeech/patts/AttributeMapProto$Mapping;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 447
    .end local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getCasesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AttributeMapProto$Mapping;

    .line 448
    .restart local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 450
    .end local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getDefinitenessList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AttributeMapProto$Mapping;

    .line 451
    .restart local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    .line 453
    .end local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto;->getNumbersList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AttributeMapProto$Mapping;

    .line 454
    .restart local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    .line 456
    .end local v0    # "element":Lspeech/patts/AttributeMapProto$Mapping;
    :cond_4
    return-void
.end method
