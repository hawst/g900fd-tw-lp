.class public final Lspeech/patts/StressDefinition;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "StressDefinition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/StressDefinition$1;,
        Lspeech/patts/StressDefinition$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/StressDefinition;


# instance fields
.field private hasStress:Z

.field private hasVowels:Z

.field private memoizedSerializedSize:I

.field private stress_:I

.field private vowels_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 293
    new-instance v0, Lspeech/patts/StressDefinition;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/StressDefinition;-><init>(Z)V

    sput-object v0, Lspeech/patts/StressDefinition;->defaultInstance:Lspeech/patts/StressDefinition;

    .line 294
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 295
    sget-object v0, Lspeech/patts/StressDefinition;->defaultInstance:Lspeech/patts/StressDefinition;

    invoke-direct {v0}, Lspeech/patts/StressDefinition;->initFields()V

    .line 296
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/StressDefinition;->stress_:I

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/StressDefinition;->vowels_:Ljava/lang/String;

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/StressDefinition;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/StressDefinition;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/StressDefinition$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/StressDefinition$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/StressDefinition;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/StressDefinition;->stress_:I

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/StressDefinition;->vowels_:Ljava/lang/String;

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/StressDefinition;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lspeech/patts/StressDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/StressDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/StressDefinition;->hasStress:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/StressDefinition;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/StressDefinition;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/StressDefinition;->stress_:I

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/StressDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/StressDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/StressDefinition;->hasVowels:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/StressDefinition;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/StressDefinition;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/StressDefinition;->vowels_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/StressDefinition;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/StressDefinition;->defaultInstance:Lspeech/patts/StressDefinition;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public static newBuilder()Lspeech/patts/StressDefinition$Builder;
    .locals 1

    .prologue
    .line 140
    # invokes: Lspeech/patts/StressDefinition$Builder;->create()Lspeech/patts/StressDefinition$Builder;
    invoke-static {}, Lspeech/patts/StressDefinition$Builder;->access$100()Lspeech/patts/StressDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/StressDefinition;)Lspeech/patts/StressDefinition$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/StressDefinition;

    .prologue
    .line 143
    invoke-static {}, Lspeech/patts/StressDefinition;->newBuilder()Lspeech/patts/StressDefinition$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/StressDefinition$Builder;->mergeFrom(Lspeech/patts/StressDefinition;)Lspeech/patts/StressDefinition$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/StressDefinition;->getDefaultInstanceForType()Lspeech/patts/StressDefinition;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/StressDefinition;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/StressDefinition;->defaultInstance:Lspeech/patts/StressDefinition;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 57
    iget v0, p0, Lspeech/patts/StressDefinition;->memoizedSerializedSize:I

    .line 58
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 70
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 60
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 61
    invoke-virtual {p0}, Lspeech/patts/StressDefinition;->hasStress()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/StressDefinition;->getStress()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 65
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/StressDefinition;->hasVowels()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 66
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/StressDefinition;->getVowels()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 69
    :cond_2
    iput v0, p0, Lspeech/patts/StressDefinition;->memoizedSerializedSize:I

    move v1, v0

    .line 70
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getStress()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lspeech/patts/StressDefinition;->stress_:I

    return v0
.end method

.method public getVowels()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lspeech/patts/StressDefinition;->vowels_:Ljava/lang/String;

    return-object v0
.end method

.method public hasStress()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/StressDefinition;->hasStress:Z

    return v0
.end method

.method public hasVowels()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/StressDefinition;->hasVowels:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 39
    iget-boolean v1, p0, Lspeech/patts/StressDefinition;->hasStress:Z

    if-nez v1, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v0

    .line 40
    :cond_1
    iget-boolean v1, p0, Lspeech/patts/StressDefinition;->hasVowels:Z

    if-eqz v1, :cond_0

    .line 41
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/StressDefinition;->toBuilder()Lspeech/patts/StressDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/StressDefinition$Builder;
    .locals 1

    .prologue
    .line 145
    invoke-static {p0}, Lspeech/patts/StressDefinition;->newBuilder(Lspeech/patts/StressDefinition;)Lspeech/patts/StressDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lspeech/patts/StressDefinition;->getSerializedSize()I

    .line 47
    invoke-virtual {p0}, Lspeech/patts/StressDefinition;->hasStress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/StressDefinition;->getStress()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 50
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/StressDefinition;->hasVowels()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/StressDefinition;->getVowels()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 53
    :cond_1
    return-void
.end method
