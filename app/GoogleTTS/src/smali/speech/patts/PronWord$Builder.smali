.class public final Lspeech/patts/PronWord$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PronWord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PronWord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/PronWord;",
        "Lspeech/patts/PronWord$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/PronWord;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 481
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1000()Lspeech/patts/PronWord$Builder;
    .locals 1

    .prologue
    .line 475
    invoke-static {}, Lspeech/patts/PronWord$Builder;->create()Lspeech/patts/PronWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/PronWord$Builder;
    .locals 3

    .prologue
    .line 484
    new-instance v0, Lspeech/patts/PronWord$Builder;

    invoke-direct {v0}, Lspeech/patts/PronWord$Builder;-><init>()V

    .line 485
    .local v0, "builder":Lspeech/patts/PronWord$Builder;
    new-instance v1, Lspeech/patts/PronWord;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/PronWord;-><init>(Lspeech/patts/PronWord$1;)V

    iput-object v1, v0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    .line 486
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 475
    invoke-virtual {p0}, Lspeech/patts/PronWord$Builder;->build()Lspeech/patts/PronWord;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/PronWord;
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/PronWord$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 515
    iget-object v0, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    invoke-static {v0}, Lspeech/patts/PronWord$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 517
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PronWord$Builder;->buildPartial()Lspeech/patts/PronWord;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/PronWord;
    .locals 3

    .prologue
    .line 530
    iget-object v1, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    if-nez v1, :cond_0

    .line 531
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 534
    :cond_0
    iget-object v1, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    # getter for: Lspeech/patts/PronWord;->component_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PronWord;->access$1200(Lspeech/patts/PronWord;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 535
    iget-object v1, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    iget-object v2, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    # getter for: Lspeech/patts/PronWord;->component_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PronWord;->access$1200(Lspeech/patts/PronWord;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PronWord;->component_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PronWord;->access$1202(Lspeech/patts/PronWord;Ljava/util/List;)Ljava/util/List;

    .line 538
    :cond_1
    iget-object v0, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    .line 539
    .local v0, "returnMe":Lspeech/patts/PronWord;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    .line 540
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 475
    invoke-virtual {p0}, Lspeech/patts/PronWord$Builder;->clone()Lspeech/patts/PronWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 475
    invoke-virtual {p0}, Lspeech/patts/PronWord$Builder;->clone()Lspeech/patts/PronWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 475
    invoke-virtual {p0}, Lspeech/patts/PronWord$Builder;->clone()Lspeech/patts/PronWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/PronWord$Builder;
    .locals 2

    .prologue
    .line 503
    invoke-static {}, Lspeech/patts/PronWord$Builder;->create()Lspeech/patts/PronWord$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    invoke-virtual {v0, v1}, Lspeech/patts/PronWord$Builder;->mergeFrom(Lspeech/patts/PronWord;)Lspeech/patts/PronWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    invoke-virtual {v0}, Lspeech/patts/PronWord;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 475
    check-cast p1, Lspeech/patts/PronWord;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/PronWord$Builder;->mergeFrom(Lspeech/patts/PronWord;)Lspeech/patts/PronWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/PronWord;)Lspeech/patts/PronWord$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/PronWord;

    .prologue
    .line 544
    invoke-static {}, Lspeech/patts/PronWord;->getDefaultInstance()Lspeech/patts/PronWord;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 551
    :cond_0
    :goto_0
    return-object p0

    .line 545
    :cond_1
    # getter for: Lspeech/patts/PronWord;->component_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PronWord;->access$1200(Lspeech/patts/PronWord;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 546
    iget-object v0, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    # getter for: Lspeech/patts/PronWord;->component_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PronWord;->access$1200(Lspeech/patts/PronWord;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 547
    iget-object v0, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PronWord;->component_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PronWord;->access$1202(Lspeech/patts/PronWord;Ljava/util/List;)Ljava/util/List;

    .line 549
    :cond_2
    iget-object v0, p0, Lspeech/patts/PronWord$Builder;->result:Lspeech/patts/PronWord;

    # getter for: Lspeech/patts/PronWord;->component_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PronWord;->access$1200(Lspeech/patts/PronWord;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PronWord;->component_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PronWord;->access$1200(Lspeech/patts/PronWord;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
