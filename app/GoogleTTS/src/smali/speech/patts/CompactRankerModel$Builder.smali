.class public final Lspeech/patts/CompactRankerModel$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "CompactRankerModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/CompactRankerModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/CompactRankerModel;",
        "Lspeech/patts/CompactRankerModel$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/CompactRankerModel;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/CompactRankerModel$Builder;
    .locals 1

    .prologue
    .line 162
    invoke-static {}, Lspeech/patts/CompactRankerModel$Builder;->create()Lspeech/patts/CompactRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/CompactRankerModel$Builder;
    .locals 3

    .prologue
    .line 171
    new-instance v0, Lspeech/patts/CompactRankerModel$Builder;

    invoke-direct {v0}, Lspeech/patts/CompactRankerModel$Builder;-><init>()V

    .line 172
    .local v0, "builder":Lspeech/patts/CompactRankerModel$Builder;
    new-instance v1, Lspeech/patts/CompactRankerModel;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/CompactRankerModel;-><init>(Lspeech/patts/CompactRankerModel$1;)V

    iput-object v1, v0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    .line 173
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel$Builder;->build()Lspeech/patts/CompactRankerModel;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/CompactRankerModel;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    iget-object v0, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    invoke-static {v0}, Lspeech/patts/CompactRankerModel$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 204
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel$Builder;->buildPartial()Lspeech/patts/CompactRankerModel;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/CompactRankerModel;
    .locals 3

    .prologue
    .line 217
    iget-object v1, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    if-nez v1, :cond_0

    .line 218
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 221
    :cond_0
    iget-object v1, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    # getter for: Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/CompactRankerModel;->access$300(Lspeech/patts/CompactRankerModel;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 222
    iget-object v1, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    iget-object v2, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    # getter for: Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/CompactRankerModel;->access$300(Lspeech/patts/CompactRankerModel;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/CompactRankerModel;->access$302(Lspeech/patts/CompactRankerModel;Ljava/util/List;)Ljava/util/List;

    .line 225
    :cond_1
    iget-object v0, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    .line 226
    .local v0, "returnMe":Lspeech/patts/CompactRankerModel;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    .line 227
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel$Builder;->clone()Lspeech/patts/CompactRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel$Builder;->clone()Lspeech/patts/CompactRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 162
    invoke-virtual {p0}, Lspeech/patts/CompactRankerModel$Builder;->clone()Lspeech/patts/CompactRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/CompactRankerModel$Builder;
    .locals 2

    .prologue
    .line 190
    invoke-static {}, Lspeech/patts/CompactRankerModel$Builder;->create()Lspeech/patts/CompactRankerModel$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    invoke-virtual {v0, v1}, Lspeech/patts/CompactRankerModel$Builder;->mergeFrom(Lspeech/patts/CompactRankerModel;)Lspeech/patts/CompactRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    invoke-virtual {v0}, Lspeech/patts/CompactRankerModel;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 162
    check-cast p1, Lspeech/patts/CompactRankerModel;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/CompactRankerModel$Builder;->mergeFrom(Lspeech/patts/CompactRankerModel;)Lspeech/patts/CompactRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/CompactRankerModel;)Lspeech/patts/CompactRankerModel$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/CompactRankerModel;

    .prologue
    .line 231
    invoke-static {}, Lspeech/patts/CompactRankerModel;->getDefaultInstance()Lspeech/patts/CompactRankerModel;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-object p0

    .line 232
    :cond_1
    # getter for: Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CompactRankerModel;->access$300(Lspeech/patts/CompactRankerModel;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 233
    iget-object v0, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    # getter for: Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CompactRankerModel;->access$300(Lspeech/patts/CompactRankerModel;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 234
    iget-object v0, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/CompactRankerModel;->access$302(Lspeech/patts/CompactRankerModel;Ljava/util/List;)Ljava/util/List;

    .line 236
    :cond_2
    iget-object v0, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    # getter for: Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CompactRankerModel;->access$300(Lspeech/patts/CompactRankerModel;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/CompactRankerModel;->weight_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CompactRankerModel;->access$300(Lspeech/patts/CompactRankerModel;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 238
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/CompactRankerModel;->hasHashFunction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    invoke-virtual {p1}, Lspeech/patts/CompactRankerModel;->getHashFunction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/CompactRankerModel$Builder;->setHashFunction(Ljava/lang/String;)Lspeech/patts/CompactRankerModel$Builder;

    goto :goto_0
.end method

.method public setHashFunction(Ljava/lang/String;)Lspeech/patts/CompactRankerModel$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 323
    if-nez p1, :cond_0

    .line 324
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 326
    :cond_0
    iget-object v0, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/CompactRankerModel;->hasHashFunction:Z
    invoke-static {v0, v1}, Lspeech/patts/CompactRankerModel;->access$402(Lspeech/patts/CompactRankerModel;Z)Z

    .line 327
    iget-object v0, p0, Lspeech/patts/CompactRankerModel$Builder;->result:Lspeech/patts/CompactRankerModel;

    # setter for: Lspeech/patts/CompactRankerModel;->hashFunction_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/CompactRankerModel;->access$502(Lspeech/patts/CompactRankerModel;Ljava/lang/String;)Ljava/lang/String;

    .line 328
    return-object p0
.end method
