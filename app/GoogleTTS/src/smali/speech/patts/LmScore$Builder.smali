.class public final Lspeech/patts/LmScore$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LmScore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/LmScore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/LmScore;",
        "Lspeech/patts/LmScore$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/LmScore;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/LmScore$Builder;
    .locals 1

    .prologue
    .line 211
    invoke-static {}, Lspeech/patts/LmScore$Builder;->create()Lspeech/patts/LmScore$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/LmScore$Builder;
    .locals 3

    .prologue
    .line 220
    new-instance v0, Lspeech/patts/LmScore$Builder;

    invoke-direct {v0}, Lspeech/patts/LmScore$Builder;-><init>()V

    .line 221
    .local v0, "builder":Lspeech/patts/LmScore$Builder;
    new-instance v1, Lspeech/patts/LmScore;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/LmScore;-><init>(Lspeech/patts/LmScore$1;)V

    iput-object v1, v0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    .line 222
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lspeech/patts/LmScore$Builder;->build()Lspeech/patts/LmScore;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/LmScore;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/LmScore$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 251
    iget-object v0, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    invoke-static {v0}, Lspeech/patts/LmScore$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 253
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/LmScore$Builder;->buildPartial()Lspeech/patts/LmScore;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/LmScore;
    .locals 3

    .prologue
    .line 266
    iget-object v1, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    if-nez v1, :cond_0

    .line 267
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 270
    :cond_0
    iget-object v0, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    .line 271
    .local v0, "returnMe":Lspeech/patts/LmScore;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    .line 272
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lspeech/patts/LmScore$Builder;->clone()Lspeech/patts/LmScore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lspeech/patts/LmScore$Builder;->clone()Lspeech/patts/LmScore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 211
    invoke-virtual {p0}, Lspeech/patts/LmScore$Builder;->clone()Lspeech/patts/LmScore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/LmScore$Builder;
    .locals 2

    .prologue
    .line 239
    invoke-static {}, Lspeech/patts/LmScore$Builder;->create()Lspeech/patts/LmScore$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    invoke-virtual {v0, v1}, Lspeech/patts/LmScore$Builder;->mergeFrom(Lspeech/patts/LmScore;)Lspeech/patts/LmScore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    invoke-virtual {v0}, Lspeech/patts/LmScore;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 211
    check-cast p1, Lspeech/patts/LmScore;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/LmScore$Builder;->mergeFrom(Lspeech/patts/LmScore;)Lspeech/patts/LmScore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/LmScore;)Lspeech/patts/LmScore$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/LmScore;

    .prologue
    .line 276
    invoke-static {}, Lspeech/patts/LmScore;->getDefaultInstance()Lspeech/patts/LmScore;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-object p0

    .line 277
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/LmScore;->hasType()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 278
    invoke-virtual {p1}, Lspeech/patts/LmScore;->getType()Lspeech/patts/LmScore$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/LmScore$Builder;->setType(Lspeech/patts/LmScore$Type;)Lspeech/patts/LmScore$Builder;

    .line 280
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/LmScore;->hasUnigram()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 281
    invoke-virtual {p1}, Lspeech/patts/LmScore;->getUnigram()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/LmScore$Builder;->setUnigram(F)Lspeech/patts/LmScore$Builder;

    .line 283
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/LmScore;->hasBigram()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 284
    invoke-virtual {p1}, Lspeech/patts/LmScore;->getBigram()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/LmScore$Builder;->setBigram(F)Lspeech/patts/LmScore$Builder;

    .line 286
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/LmScore;->hasTrigram()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    invoke-virtual {p1}, Lspeech/patts/LmScore;->getTrigram()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/LmScore$Builder;->setTrigram(F)Lspeech/patts/LmScore$Builder;

    goto :goto_0
.end method

.method public setBigram(F)Lspeech/patts/LmScore$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 379
    iget-object v0, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/LmScore;->hasBigram:Z
    invoke-static {v0, v1}, Lspeech/patts/LmScore;->access$702(Lspeech/patts/LmScore;Z)Z

    .line 380
    iget-object v0, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    # setter for: Lspeech/patts/LmScore;->bigram_:F
    invoke-static {v0, p1}, Lspeech/patts/LmScore;->access$802(Lspeech/patts/LmScore;F)F

    .line 381
    return-object p0
.end method

.method public setTrigram(F)Lspeech/patts/LmScore$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 397
    iget-object v0, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/LmScore;->hasTrigram:Z
    invoke-static {v0, v1}, Lspeech/patts/LmScore;->access$902(Lspeech/patts/LmScore;Z)Z

    .line 398
    iget-object v0, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    # setter for: Lspeech/patts/LmScore;->trigram_:F
    invoke-static {v0, p1}, Lspeech/patts/LmScore;->access$1002(Lspeech/patts/LmScore;F)F

    .line 399
    return-object p0
.end method

.method public setType(Lspeech/patts/LmScore$Type;)Lspeech/patts/LmScore$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/LmScore$Type;

    .prologue
    .line 340
    if-nez p1, :cond_0

    .line 341
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 343
    :cond_0
    iget-object v0, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/LmScore;->hasType:Z
    invoke-static {v0, v1}, Lspeech/patts/LmScore;->access$302(Lspeech/patts/LmScore;Z)Z

    .line 344
    iget-object v0, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    # setter for: Lspeech/patts/LmScore;->type_:Lspeech/patts/LmScore$Type;
    invoke-static {v0, p1}, Lspeech/patts/LmScore;->access$402(Lspeech/patts/LmScore;Lspeech/patts/LmScore$Type;)Lspeech/patts/LmScore$Type;

    .line 345
    return-object p0
.end method

.method public setUnigram(F)Lspeech/patts/LmScore$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 361
    iget-object v0, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/LmScore;->hasUnigram:Z
    invoke-static {v0, v1}, Lspeech/patts/LmScore;->access$502(Lspeech/patts/LmScore;Z)Z

    .line 362
    iget-object v0, p0, Lspeech/patts/LmScore$Builder;->result:Lspeech/patts/LmScore;

    # setter for: Lspeech/patts/LmScore;->unigram_:F
    invoke-static {v0, p1}, Lspeech/patts/LmScore;->access$602(Lspeech/patts/LmScore;F)F

    .line 363
    return-object p0
.end method
