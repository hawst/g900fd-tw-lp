.class public final enum Lspeech/patts/Token$Type;
.super Ljava/lang/Enum;
.source "Token.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Token;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/Token$Type;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/Token$Type;

.field public static final enum EXL:Lspeech/patts/Token$Type;

.field public static final enum HOMOGRAPH:Lspeech/patts/Token$Type;

.field public static final enum PUNCT:Lspeech/patts/Token$Type;

.field public static final enum SEMIOTIC_CLASS:Lspeech/patts/Token$Type;

.field public static final enum WORD:Lspeech/patts/Token$Type;

.field public static final enum WORD_NEEDS_VERBALIZATION:Lspeech/patts/Token$Type;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/Token$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 24
    new-instance v0, Lspeech/patts/Token$Type;

    const-string v1, "WORD"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/Token$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$Type;->WORD:Lspeech/patts/Token$Type;

    .line 25
    new-instance v0, Lspeech/patts/Token$Type;

    const-string v1, "SEMIOTIC_CLASS"

    invoke-direct {v0, v1, v4, v4, v5}, Lspeech/patts/Token$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$Type;->SEMIOTIC_CLASS:Lspeech/patts/Token$Type;

    .line 26
    new-instance v0, Lspeech/patts/Token$Type;

    const-string v1, "HOMOGRAPH"

    invoke-direct {v0, v1, v5, v5, v6}, Lspeech/patts/Token$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$Type;->HOMOGRAPH:Lspeech/patts/Token$Type;

    .line 27
    new-instance v0, Lspeech/patts/Token$Type;

    const-string v1, "EXL"

    invoke-direct {v0, v1, v6, v6, v7}, Lspeech/patts/Token$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$Type;->EXL:Lspeech/patts/Token$Type;

    .line 28
    new-instance v0, Lspeech/patts/Token$Type;

    const-string v1, "PUNCT"

    invoke-direct {v0, v1, v7, v7, v8}, Lspeech/patts/Token$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$Type;->PUNCT:Lspeech/patts/Token$Type;

    .line 29
    new-instance v0, Lspeech/patts/Token$Type;

    const-string v1, "WORD_NEEDS_VERBALIZATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v8, v2}, Lspeech/patts/Token$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$Type;->WORD_NEEDS_VERBALIZATION:Lspeech/patts/Token$Type;

    .line 22
    const/4 v0, 0x6

    new-array v0, v0, [Lspeech/patts/Token$Type;

    const/4 v1, 0x0

    sget-object v2, Lspeech/patts/Token$Type;->WORD:Lspeech/patts/Token$Type;

    aput-object v2, v0, v1

    sget-object v1, Lspeech/patts/Token$Type;->SEMIOTIC_CLASS:Lspeech/patts/Token$Type;

    aput-object v1, v0, v4

    sget-object v1, Lspeech/patts/Token$Type;->HOMOGRAPH:Lspeech/patts/Token$Type;

    aput-object v1, v0, v5

    sget-object v1, Lspeech/patts/Token$Type;->EXL:Lspeech/patts/Token$Type;

    aput-object v1, v0, v6

    sget-object v1, Lspeech/patts/Token$Type;->PUNCT:Lspeech/patts/Token$Type;

    aput-object v1, v0, v7

    sget-object v1, Lspeech/patts/Token$Type;->WORD_NEEDS_VERBALIZATION:Lspeech/patts/Token$Type;

    aput-object v1, v0, v8

    sput-object v0, Lspeech/patts/Token$Type;->$VALUES:[Lspeech/patts/Token$Type;

    .line 52
    new-instance v0, Lspeech/patts/Token$Type$1;

    invoke-direct {v0}, Lspeech/patts/Token$Type$1;-><init>()V

    sput-object v0, Lspeech/patts/Token$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 62
    iput p3, p0, Lspeech/patts/Token$Type;->index:I

    .line 63
    iput p4, p0, Lspeech/patts/Token$Type;->value:I

    .line 64
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/Token$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lspeech/patts/Token$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/Token$Type;

    return-object v0
.end method

.method public static values()[Lspeech/patts/Token$Type;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lspeech/patts/Token$Type;->$VALUES:[Lspeech/patts/Token$Type;

    invoke-virtual {v0}, [Lspeech/patts/Token$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/Token$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lspeech/patts/Token$Type;->value:I

    return v0
.end method
