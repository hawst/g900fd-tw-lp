.class public final Lspeech/patts/Word;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Word.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/Word$1;,
        Lspeech/patts/Word$Builder;,
        Lspeech/patts/Word$Type;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/Word;


# instance fields
.field private firstDaughter_:I

.field private hasFirstDaughter:Z

.field private hasId:Z

.field private hasIntonationType:Z

.field private hasLastDaughter:Z

.field private hasLexicalSyntax:Z

.field private hasLmScore:Z

.field private hasParent:Z

.field private hasPauseLength:Z

.field private hasProhibitsHead:Z

.field private hasProhibitsNucleus:Z

.field private hasProminence:Z

.field private hasSpelling:Z

.field private hasSpellingWithStress:Z

.field private hasSyntax:Z

.field private hasType:Z

.field private hasVariant:Z

.field private hasVariantForced:Z

.field private hasVoiceMod:Z

.field private id_:Ljava/lang/String;

.field private intonationType_:Lcom/google/speech/tts/IntonationType;

.field private lastDaughter_:I

.field private lexicalSyntax_:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

.field private lmScore_:Lspeech/patts/LmScore;

.field private memoizedSerializedSize:I

.field private parent_:I

.field private pauseLength_:F

.field private prohibitsHead_:Z

.field private prohibitsNucleus_:Z

.field private prominence_:Lcom/google/speech/tts/Prominence;

.field private spellingWithStress_:Ljava/lang/String;

.field private spelling_:Ljava/lang/String;

.field private syntax_:Lspeech/patts/Syntax;

.field private type_:Lspeech/patts/Word$Type;

.field private variantForced_:Z

.field private variant_:Ljava/lang/String;

.field private voiceMod_:Lcom/google/speech/tts/VoiceMod;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1092
    new-instance v0, Lspeech/patts/Word;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/Word;-><init>(Z)V

    sput-object v0, Lspeech/patts/Word;->defaultInstance:Lspeech/patts/Word;

    .line 1093
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 1094
    sget-object v0, Lspeech/patts/Word;->defaultInstance:Lspeech/patts/Word;

    invoke-direct {v0}, Lspeech/patts/Word;->initFields()V

    .line 1095
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 66
    iput v1, p0, Lspeech/patts/Word;->parent_:I

    .line 73
    iput v1, p0, Lspeech/patts/Word;->firstDaughter_:I

    .line 80
    iput v1, p0, Lspeech/patts/Word;->lastDaughter_:I

    .line 87
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Word;->id_:Ljava/lang/String;

    .line 94
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Word;->variant_:Ljava/lang/String;

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Word;->spelling_:Ljava/lang/String;

    .line 136
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/Word;->pauseLength_:F

    .line 150
    iput-boolean v1, p0, Lspeech/patts/Word;->variantForced_:Z

    .line 157
    iput-boolean v1, p0, Lspeech/patts/Word;->prohibitsNucleus_:Z

    .line 164
    iput-boolean v1, p0, Lspeech/patts/Word;->prohibitsHead_:Z

    .line 171
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Word;->spellingWithStress_:Ljava/lang/String;

    .line 264
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Word;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/Word;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/Word$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/Word$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/Word;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 66
    iput v1, p0, Lspeech/patts/Word;->parent_:I

    .line 73
    iput v1, p0, Lspeech/patts/Word;->firstDaughter_:I

    .line 80
    iput v1, p0, Lspeech/patts/Word;->lastDaughter_:I

    .line 87
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Word;->id_:Ljava/lang/String;

    .line 94
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Word;->variant_:Ljava/lang/String;

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Word;->spelling_:Ljava/lang/String;

    .line 136
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/Word;->pauseLength_:F

    .line 150
    iput-boolean v1, p0, Lspeech/patts/Word;->variantForced_:Z

    .line 157
    iput-boolean v1, p0, Lspeech/patts/Word;->prohibitsNucleus_:Z

    .line 164
    iput-boolean v1, p0, Lspeech/patts/Word;->prohibitsHead_:Z

    .line 171
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Word;->spellingWithStress_:Ljava/lang/String;

    .line 264
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Word;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/Word;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Word;->id_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasVariant:Z

    return p1
.end method

.method static synthetic access$1202(Lspeech/patts/Word;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Word;->variant_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1302(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasType:Z

    return p1
.end method

.method static synthetic access$1402(Lspeech/patts/Word;Lspeech/patts/Word$Type;)Lspeech/patts/Word$Type;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Lspeech/patts/Word$Type;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Word;->type_:Lspeech/patts/Word$Type;

    return-object p1
.end method

.method static synthetic access$1502(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasSpelling:Z

    return p1
.end method

.method static synthetic access$1602(Lspeech/patts/Word;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Word;->spelling_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1702(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasVoiceMod:Z

    return p1
.end method

.method static synthetic access$1800(Lspeech/patts/Word;)Lcom/google/speech/tts/VoiceMod;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Word;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Word;->voiceMod_:Lcom/google/speech/tts/VoiceMod;

    return-object v0
.end method

.method static synthetic access$1802(Lspeech/patts/Word;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Lcom/google/speech/tts/VoiceMod;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Word;->voiceMod_:Lcom/google/speech/tts/VoiceMod;

    return-object p1
.end method

.method static synthetic access$1902(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasSyntax:Z

    return p1
.end method

.method static synthetic access$2000(Lspeech/patts/Word;)Lspeech/patts/Syntax;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Word;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Word;->syntax_:Lspeech/patts/Syntax;

    return-object v0
.end method

.method static synthetic access$2002(Lspeech/patts/Word;Lspeech/patts/Syntax;)Lspeech/patts/Syntax;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Lspeech/patts/Syntax;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Word;->syntax_:Lspeech/patts/Syntax;

    return-object p1
.end method

.method static synthetic access$2102(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasLexicalSyntax:Z

    return p1
.end method

.method static synthetic access$2202(Lspeech/patts/Word;Lcom/google/speech/tts/LexiconProto$Entry$Syntax;)Lcom/google/speech/tts/LexiconProto$Entry$Syntax;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Word;->lexicalSyntax_:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    return-object p1
.end method

.method static synthetic access$2302(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasPauseLength:Z

    return p1
.end method

.method static synthetic access$2402(Lspeech/patts/Word;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Word;->pauseLength_:F

    return p1
.end method

.method static synthetic access$2502(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasIntonationType:Z

    return p1
.end method

.method static synthetic access$2602(Lspeech/patts/Word;Lcom/google/speech/tts/IntonationType;)Lcom/google/speech/tts/IntonationType;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Lcom/google/speech/tts/IntonationType;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Word;->intonationType_:Lcom/google/speech/tts/IntonationType;

    return-object p1
.end method

.method static synthetic access$2702(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasVariantForced:Z

    return p1
.end method

.method static synthetic access$2802(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->variantForced_:Z

    return p1
.end method

.method static synthetic access$2902(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasProhibitsNucleus:Z

    return p1
.end method

.method static synthetic access$3002(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->prohibitsNucleus_:Z

    return p1
.end method

.method static synthetic access$302(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasParent:Z

    return p1
.end method

.method static synthetic access$3102(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasProhibitsHead:Z

    return p1
.end method

.method static synthetic access$3202(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->prohibitsHead_:Z

    return p1
.end method

.method static synthetic access$3302(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasSpellingWithStress:Z

    return p1
.end method

.method static synthetic access$3402(Lspeech/patts/Word;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Word;->spellingWithStress_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3502(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasLmScore:Z

    return p1
.end method

.method static synthetic access$3600(Lspeech/patts/Word;)Lspeech/patts/LmScore;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Word;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Word;->lmScore_:Lspeech/patts/LmScore;

    return-object v0
.end method

.method static synthetic access$3602(Lspeech/patts/Word;Lspeech/patts/LmScore;)Lspeech/patts/LmScore;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Lspeech/patts/LmScore;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Word;->lmScore_:Lspeech/patts/LmScore;

    return-object p1
.end method

.method static synthetic access$3702(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasProminence:Z

    return p1
.end method

.method static synthetic access$3800(Lspeech/patts/Word;)Lcom/google/speech/tts/Prominence;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Word;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Word;->prominence_:Lcom/google/speech/tts/Prominence;

    return-object v0
.end method

.method static synthetic access$3802(Lspeech/patts/Word;Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Lcom/google/speech/tts/Prominence;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Word;->prominence_:Lcom/google/speech/tts/Prominence;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/Word;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Word;->parent_:I

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasFirstDaughter:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/Word;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Word;->firstDaughter_:I

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasLastDaughter:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/Word;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Word;->lastDaughter_:I

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Word;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Word;->hasId:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/Word;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/Word;->defaultInstance:Lspeech/patts/Word;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 190
    sget-object v0, Lspeech/patts/Word$Type;->WORD:Lspeech/patts/Word$Type;

    iput-object v0, p0, Lspeech/patts/Word;->type_:Lspeech/patts/Word$Type;

    .line 191
    invoke-static {}, Lcom/google/speech/tts/VoiceMod;->getDefaultInstance()Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Word;->voiceMod_:Lcom/google/speech/tts/VoiceMod;

    .line 192
    invoke-static {}, Lspeech/patts/Syntax;->getDefaultInstance()Lspeech/patts/Syntax;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Word;->syntax_:Lspeech/patts/Syntax;

    .line 193
    sget-object v0, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->UNSPECIFIED:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    iput-object v0, p0, Lspeech/patts/Word;->lexicalSyntax_:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 194
    sget-object v0, Lcom/google/speech/tts/IntonationType;->UNKNOWN:Lcom/google/speech/tts/IntonationType;

    iput-object v0, p0, Lspeech/patts/Word;->intonationType_:Lcom/google/speech/tts/IntonationType;

    .line 195
    invoke-static {}, Lspeech/patts/LmScore;->getDefaultInstance()Lspeech/patts/LmScore;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Word;->lmScore_:Lspeech/patts/LmScore;

    .line 196
    invoke-static {}, Lcom/google/speech/tts/Prominence;->getDefaultInstance()Lcom/google/speech/tts/Prominence;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Word;->prominence_:Lcom/google/speech/tts/Prominence;

    .line 197
    return-void
.end method

.method public static newBuilder()Lspeech/patts/Word$Builder;
    .locals 1

    .prologue
    .line 413
    # invokes: Lspeech/patts/Word$Builder;->create()Lspeech/patts/Word$Builder;
    invoke-static {}, Lspeech/patts/Word$Builder;->access$100()Lspeech/patts/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/Word;)Lspeech/patts/Word$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/Word;

    .prologue
    .line 416
    invoke-static {}, Lspeech/patts/Word;->newBuilder()Lspeech/patts/Word$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/Word$Builder;->mergeFrom(Lspeech/patts/Word;)Lspeech/patts/Word$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Word;->getDefaultInstanceForType()Lspeech/patts/Word;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/Word;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/Word;->defaultInstance:Lspeech/patts/Word;

    return-object v0
.end method

.method public getFirstDaughter()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lspeech/patts/Word;->firstDaughter_:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lspeech/patts/Word;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getIntonationType()Lcom/google/speech/tts/IntonationType;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lspeech/patts/Word;->intonationType_:Lcom/google/speech/tts/IntonationType;

    return-object v0
.end method

.method public getLastDaughter()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lspeech/patts/Word;->lastDaughter_:I

    return v0
.end method

.method public getLexicalSyntax()Lcom/google/speech/tts/LexiconProto$Entry$Syntax;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lspeech/patts/Word;->lexicalSyntax_:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    return-object v0
.end method

.method public getLmScore()Lspeech/patts/LmScore;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lspeech/patts/Word;->lmScore_:Lspeech/patts/LmScore;

    return-object v0
.end method

.method public getParent()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lspeech/patts/Word;->parent_:I

    return v0
.end method

.method public getPauseLength()F
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lspeech/patts/Word;->pauseLength_:F

    return v0
.end method

.method public getProhibitsHead()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lspeech/patts/Word;->prohibitsHead_:Z

    return v0
.end method

.method public getProhibitsNucleus()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lspeech/patts/Word;->prohibitsNucleus_:Z

    return v0
.end method

.method public getProminence()Lcom/google/speech/tts/Prominence;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lspeech/patts/Word;->prominence_:Lcom/google/speech/tts/Prominence;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 266
    iget v0, p0, Lspeech/patts/Word;->memoizedSerializedSize:I

    .line 267
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 343
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 269
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 270
    invoke-virtual {p0}, Lspeech/patts/Word;->hasParent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 271
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/Word;->getParent()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 274
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Word;->hasFirstDaughter()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 275
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/Word;->getFirstDaughter()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 278
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Word;->hasLastDaughter()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 279
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/Word;->getLastDaughter()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 282
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Word;->hasId()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 283
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/Word;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 286
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Word;->hasVariant()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 287
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/Word;->getVariant()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 290
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/Word;->hasType()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 291
    const/4 v2, 0x6

    invoke-virtual {p0}, Lspeech/patts/Word;->getType()Lspeech/patts/Word$Type;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/Word$Type;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 294
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/Word;->hasSpelling()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 295
    const/4 v2, 0x7

    invoke-virtual {p0}, Lspeech/patts/Word;->getSpelling()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 298
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/Word;->hasVoiceMod()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 299
    const/16 v2, 0x8

    invoke-virtual {p0}, Lspeech/patts/Word;->getVoiceMod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 302
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/Word;->hasSyntax()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 303
    const/16 v2, 0x9

    invoke-virtual {p0}, Lspeech/patts/Word;->getSyntax()Lspeech/patts/Syntax;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 306
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/Word;->hasLexicalSyntax()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 307
    const/16 v2, 0xa

    invoke-virtual {p0}, Lspeech/patts/Word;->getLexicalSyntax()Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 310
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/Word;->hasPauseLength()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 311
    const/16 v2, 0xb

    invoke-virtual {p0}, Lspeech/patts/Word;->getPauseLength()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 314
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/Word;->hasIntonationType()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 315
    const/16 v2, 0xc

    invoke-virtual {p0}, Lspeech/patts/Word;->getIntonationType()Lcom/google/speech/tts/IntonationType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/tts/IntonationType;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 318
    :cond_c
    invoke-virtual {p0}, Lspeech/patts/Word;->hasVariantForced()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 319
    const/16 v2, 0xd

    invoke-virtual {p0}, Lspeech/patts/Word;->getVariantForced()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 322
    :cond_d
    invoke-virtual {p0}, Lspeech/patts/Word;->hasProhibitsNucleus()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 323
    const/16 v2, 0xe

    invoke-virtual {p0}, Lspeech/patts/Word;->getProhibitsNucleus()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 326
    :cond_e
    invoke-virtual {p0}, Lspeech/patts/Word;->hasProhibitsHead()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 327
    const/16 v2, 0xf

    invoke-virtual {p0}, Lspeech/patts/Word;->getProhibitsHead()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 330
    :cond_f
    invoke-virtual {p0}, Lspeech/patts/Word;->hasSpellingWithStress()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 331
    const/16 v2, 0x10

    invoke-virtual {p0}, Lspeech/patts/Word;->getSpellingWithStress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 334
    :cond_10
    invoke-virtual {p0}, Lspeech/patts/Word;->hasLmScore()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 335
    const/16 v2, 0x11

    invoke-virtual {p0}, Lspeech/patts/Word;->getLmScore()Lspeech/patts/LmScore;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 338
    :cond_11
    invoke-virtual {p0}, Lspeech/patts/Word;->hasProminence()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 339
    const/16 v2, 0x12

    invoke-virtual {p0}, Lspeech/patts/Word;->getProminence()Lcom/google/speech/tts/Prominence;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 342
    :cond_12
    iput v0, p0, Lspeech/patts/Word;->memoizedSerializedSize:I

    move v1, v0

    .line 343
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto/16 :goto_0
.end method

.method public getSpelling()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lspeech/patts/Word;->spelling_:Ljava/lang/String;

    return-object v0
.end method

.method public getSpellingWithStress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lspeech/patts/Word;->spellingWithStress_:Ljava/lang/String;

    return-object v0
.end method

.method public getSyntax()Lspeech/patts/Syntax;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lspeech/patts/Word;->syntax_:Lspeech/patts/Syntax;

    return-object v0
.end method

.method public getType()Lspeech/patts/Word$Type;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lspeech/patts/Word;->type_:Lspeech/patts/Word$Type;

    return-object v0
.end method

.method public getVariant()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lspeech/patts/Word;->variant_:Ljava/lang/String;

    return-object v0
.end method

.method public getVariantForced()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lspeech/patts/Word;->variantForced_:Z

    return v0
.end method

.method public getVoiceMod()Lcom/google/speech/tts/VoiceMod;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lspeech/patts/Word;->voiceMod_:Lcom/google/speech/tts/VoiceMod;

    return-object v0
.end method

.method public hasFirstDaughter()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lspeech/patts/Word;->hasFirstDaughter:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lspeech/patts/Word;->hasId:Z

    return v0
.end method

.method public hasIntonationType()Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lspeech/patts/Word;->hasIntonationType:Z

    return v0
.end method

.method public hasLastDaughter()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lspeech/patts/Word;->hasLastDaughter:Z

    return v0
.end method

.method public hasLexicalSyntax()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lspeech/patts/Word;->hasLexicalSyntax:Z

    return v0
.end method

.method public hasLmScore()Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lspeech/patts/Word;->hasLmScore:Z

    return v0
.end method

.method public hasParent()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lspeech/patts/Word;->hasParent:Z

    return v0
.end method

.method public hasPauseLength()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lspeech/patts/Word;->hasPauseLength:Z

    return v0
.end method

.method public hasProhibitsHead()Z
    .locals 1

    .prologue
    .line 165
    iget-boolean v0, p0, Lspeech/patts/Word;->hasProhibitsHead:Z

    return v0
.end method

.method public hasProhibitsNucleus()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Lspeech/patts/Word;->hasProhibitsNucleus:Z

    return v0
.end method

.method public hasProminence()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lspeech/patts/Word;->hasProminence:Z

    return v0
.end method

.method public hasSpelling()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lspeech/patts/Word;->hasSpelling:Z

    return v0
.end method

.method public hasSpellingWithStress()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lspeech/patts/Word;->hasSpellingWithStress:Z

    return v0
.end method

.method public hasSyntax()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lspeech/patts/Word;->hasSyntax:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lspeech/patts/Word;->hasType:Z

    return v0
.end method

.method public hasVariant()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lspeech/patts/Word;->hasVariant:Z

    return v0
.end method

.method public hasVariantForced()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lspeech/patts/Word;->hasVariantForced:Z

    return v0
.end method

.method public hasVoiceMod()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lspeech/patts/Word;->hasVoiceMod:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lspeech/patts/Word;->hasVoiceMod()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    invoke-virtual {p0}, Lspeech/patts/Word;->getVoiceMod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/speech/tts/VoiceMod;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 202
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Word;->toBuilder()Lspeech/patts/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/Word$Builder;
    .locals 1

    .prologue
    .line 418
    invoke-static {p0}, Lspeech/patts/Word;->newBuilder(Lspeech/patts/Word;)Lspeech/patts/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    invoke-virtual {p0}, Lspeech/patts/Word;->getSerializedSize()I

    .line 208
    invoke-virtual {p0}, Lspeech/patts/Word;->hasParent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/Word;->getParent()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 211
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Word;->hasFirstDaughter()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/Word;->getFirstDaughter()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 214
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Word;->hasLastDaughter()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 215
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/Word;->getLastDaughter()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 217
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Word;->hasId()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 218
    const/4 v0, 0x4

    invoke-virtual {p0}, Lspeech/patts/Word;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 220
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Word;->hasVariant()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 221
    const/4 v0, 0x5

    invoke-virtual {p0}, Lspeech/patts/Word;->getVariant()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 223
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Word;->hasType()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 224
    const/4 v0, 0x6

    invoke-virtual {p0}, Lspeech/patts/Word;->getType()Lspeech/patts/Word$Type;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/Word$Type;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 226
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/Word;->hasSpelling()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 227
    const/4 v0, 0x7

    invoke-virtual {p0}, Lspeech/patts/Word;->getSpelling()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 229
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/Word;->hasVoiceMod()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 230
    const/16 v0, 0x8

    invoke-virtual {p0}, Lspeech/patts/Word;->getVoiceMod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 232
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/Word;->hasSyntax()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 233
    const/16 v0, 0x9

    invoke-virtual {p0}, Lspeech/patts/Word;->getSyntax()Lspeech/patts/Syntax;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 235
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/Word;->hasLexicalSyntax()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 236
    const/16 v0, 0xa

    invoke-virtual {p0}, Lspeech/patts/Word;->getLexicalSyntax()Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/LexiconProto$Entry$Syntax;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 238
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/Word;->hasPauseLength()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 239
    const/16 v0, 0xb

    invoke-virtual {p0}, Lspeech/patts/Word;->getPauseLength()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 241
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/Word;->hasIntonationType()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 242
    const/16 v0, 0xc

    invoke-virtual {p0}, Lspeech/patts/Word;->getIntonationType()Lcom/google/speech/tts/IntonationType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/IntonationType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 244
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/Word;->hasVariantForced()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 245
    const/16 v0, 0xd

    invoke-virtual {p0}, Lspeech/patts/Word;->getVariantForced()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 247
    :cond_c
    invoke-virtual {p0}, Lspeech/patts/Word;->hasProhibitsNucleus()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 248
    const/16 v0, 0xe

    invoke-virtual {p0}, Lspeech/patts/Word;->getProhibitsNucleus()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 250
    :cond_d
    invoke-virtual {p0}, Lspeech/patts/Word;->hasProhibitsHead()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 251
    const/16 v0, 0xf

    invoke-virtual {p0}, Lspeech/patts/Word;->getProhibitsHead()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 253
    :cond_e
    invoke-virtual {p0}, Lspeech/patts/Word;->hasSpellingWithStress()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 254
    const/16 v0, 0x10

    invoke-virtual {p0}, Lspeech/patts/Word;->getSpellingWithStress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 256
    :cond_f
    invoke-virtual {p0}, Lspeech/patts/Word;->hasLmScore()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 257
    const/16 v0, 0x11

    invoke-virtual {p0}, Lspeech/patts/Word;->getLmScore()Lspeech/patts/LmScore;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 259
    :cond_10
    invoke-virtual {p0}, Lspeech/patts/Word;->hasProminence()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 260
    const/16 v0, 0x12

    invoke-virtual {p0}, Lspeech/patts/Word;->getProminence()Lcom/google/speech/tts/Prominence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 262
    :cond_11
    return-void
.end method
