.class public final Lspeech/patts/CostFunctionSparseTable$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "CostFunctionSparseTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/CostFunctionSparseTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/CostFunctionSparseTable;",
        "Lspeech/patts/CostFunctionSparseTable$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/CostFunctionSparseTable;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/CostFunctionSparseTable$Builder;
    .locals 1

    .prologue
    .line 169
    invoke-static {}, Lspeech/patts/CostFunctionSparseTable$Builder;->create()Lspeech/patts/CostFunctionSparseTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/CostFunctionSparseTable$Builder;
    .locals 3

    .prologue
    .line 178
    new-instance v0, Lspeech/patts/CostFunctionSparseTable$Builder;

    invoke-direct {v0}, Lspeech/patts/CostFunctionSparseTable$Builder;-><init>()V

    .line 179
    .local v0, "builder":Lspeech/patts/CostFunctionSparseTable$Builder;
    new-instance v1, Lspeech/patts/CostFunctionSparseTable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/CostFunctionSparseTable;-><init>(Lspeech/patts/CostFunctionSparseTable$1;)V

    iput-object v1, v0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    .line 180
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable$Builder;->build()Lspeech/patts/CostFunctionSparseTable;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/CostFunctionSparseTable;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    invoke-static {v0}, Lspeech/patts/CostFunctionSparseTable$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 211
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable$Builder;->buildPartial()Lspeech/patts/CostFunctionSparseTable;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/CostFunctionSparseTable;
    .locals 3

    .prologue
    .line 224
    iget-object v1, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    if-nez v1, :cond_0

    .line 225
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 228
    :cond_0
    iget-object v1, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    # getter for: Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/CostFunctionSparseTable;->access$300(Lspeech/patts/CostFunctionSparseTable;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 229
    iget-object v1, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    iget-object v2, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    # getter for: Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/CostFunctionSparseTable;->access$300(Lspeech/patts/CostFunctionSparseTable;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/CostFunctionSparseTable;->access$302(Lspeech/patts/CostFunctionSparseTable;Ljava/util/List;)Ljava/util/List;

    .line 232
    :cond_1
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    .line 233
    .local v0, "returnMe":Lspeech/patts/CostFunctionSparseTable;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    .line 234
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable$Builder;->clone()Lspeech/patts/CostFunctionSparseTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable$Builder;->clone()Lspeech/patts/CostFunctionSparseTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 169
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable$Builder;->clone()Lspeech/patts/CostFunctionSparseTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/CostFunctionSparseTable$Builder;
    .locals 2

    .prologue
    .line 197
    invoke-static {}, Lspeech/patts/CostFunctionSparseTable$Builder;->create()Lspeech/patts/CostFunctionSparseTable$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    invoke-virtual {v0, v1}, Lspeech/patts/CostFunctionSparseTable$Builder;->mergeFrom(Lspeech/patts/CostFunctionSparseTable;)Lspeech/patts/CostFunctionSparseTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    invoke-virtual {v0}, Lspeech/patts/CostFunctionSparseTable;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 169
    check-cast p1, Lspeech/patts/CostFunctionSparseTable;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/CostFunctionSparseTable$Builder;->mergeFrom(Lspeech/patts/CostFunctionSparseTable;)Lspeech/patts/CostFunctionSparseTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/CostFunctionSparseTable;)Lspeech/patts/CostFunctionSparseTable$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/CostFunctionSparseTable;

    .prologue
    .line 238
    invoke-static {}, Lspeech/patts/CostFunctionSparseTable;->getDefaultInstance()Lspeech/patts/CostFunctionSparseTable;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-object p0

    .line 239
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/CostFunctionSparseTable;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 240
    invoke-virtual {p1}, Lspeech/patts/CostFunctionSparseTable;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/CostFunctionSparseTable$Builder;->setName(Ljava/lang/String;)Lspeech/patts/CostFunctionSparseTable$Builder;

    .line 242
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/CostFunctionSparseTable;->hasDefaultWeight()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 243
    invoke-virtual {p1}, Lspeech/patts/CostFunctionSparseTable;->getDefaultWeight()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/CostFunctionSparseTable$Builder;->setDefaultWeight(F)Lspeech/patts/CostFunctionSparseTable$Builder;

    .line 245
    :cond_3
    # getter for: Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctionSparseTable;->access$300(Lspeech/patts/CostFunctionSparseTable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    # getter for: Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctionSparseTable;->access$300(Lspeech/patts/CostFunctionSparseTable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 247
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/CostFunctionSparseTable;->access$302(Lspeech/patts/CostFunctionSparseTable;Ljava/util/List;)Ljava/util/List;

    .line 249
    :cond_4
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    # getter for: Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctionSparseTable;->access$300(Lspeech/patts/CostFunctionSparseTable;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctionSparseTable;->access$300(Lspeech/patts/CostFunctionSparseTable;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public setDefaultWeight(F)Lspeech/patts/CostFunctionSparseTable$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 317
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/CostFunctionSparseTable;->hasDefaultWeight:Z
    invoke-static {v0, v1}, Lspeech/patts/CostFunctionSparseTable;->access$602(Lspeech/patts/CostFunctionSparseTable;Z)Z

    .line 318
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    # setter for: Lspeech/patts/CostFunctionSparseTable;->defaultWeight_:F
    invoke-static {v0, p1}, Lspeech/patts/CostFunctionSparseTable;->access$702(Lspeech/patts/CostFunctionSparseTable;F)F

    .line 319
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lspeech/patts/CostFunctionSparseTable$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 296
    if-nez p1, :cond_0

    .line 297
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 299
    :cond_0
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/CostFunctionSparseTable;->hasName:Z
    invoke-static {v0, v1}, Lspeech/patts/CostFunctionSparseTable;->access$402(Lspeech/patts/CostFunctionSparseTable;Z)Z

    .line 300
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable$Builder;->result:Lspeech/patts/CostFunctionSparseTable;

    # setter for: Lspeech/patts/CostFunctionSparseTable;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/CostFunctionSparseTable;->access$502(Lspeech/patts/CostFunctionSparseTable;Ljava/lang/String;)Ljava/lang/String;

    .line 301
    return-object p0
.end method
