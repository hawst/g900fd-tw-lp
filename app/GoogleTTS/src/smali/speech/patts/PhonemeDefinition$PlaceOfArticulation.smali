.class public final enum Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;
.super Ljava/lang/Enum;
.source "PhonemeDefinition.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PhonemeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlaceOfArticulation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field public static final enum ALVEOLAR:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field public static final enum DENTAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field public static final enum GLOTTAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field public static final enum LABIAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field public static final enum LABIODENTAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field public static final enum PALATAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field public static final enum PHARYNGEAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field public static final enum POSTALVEOLAR:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field public static final enum RETROFLEX:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field public static final enum UNDEFINED_POA:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field public static final enum UVULAR:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field public static final enum VELAR:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 24
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    const-string v1, "UNDEFINED_POA"

    invoke-direct {v0, v1, v5, v5, v5}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->UNDEFINED_POA:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 25
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    const-string v1, "LABIAL"

    invoke-direct {v0, v1, v6, v6, v6}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->LABIAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 26
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    const-string v1, "DENTAL"

    invoke-direct {v0, v1, v7, v7, v7}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->DENTAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 27
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    const-string v1, "ALVEOLAR"

    invoke-direct {v0, v1, v8, v8, v8}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->ALVEOLAR:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 28
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    const-string v1, "POSTALVEOLAR"

    invoke-direct {v0, v1, v9, v9, v9}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->POSTALVEOLAR:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 29
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    const-string v1, "PALATAL"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->PALATAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 30
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    const-string v1, "VELAR"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->VELAR:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 31
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    const-string v1, "GLOTTAL"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->GLOTTAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 32
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    const-string v1, "RETROFLEX"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->RETROFLEX:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 33
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    const-string v1, "UVULAR"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->UVULAR:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 34
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    const-string v1, "LABIODENTAL"

    const/16 v2, 0xa

    const/16 v3, 0xa

    const/16 v4, 0xa

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->LABIODENTAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 35
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    const-string v1, "PHARYNGEAL"

    const/16 v2, 0xb

    const/16 v3, 0xb

    const/16 v4, 0xb

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->PHARYNGEAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 22
    const/16 v0, 0xc

    new-array v0, v0, [Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    sget-object v1, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->UNDEFINED_POA:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    aput-object v1, v0, v5

    sget-object v1, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->LABIAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    aput-object v1, v0, v6

    sget-object v1, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->DENTAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    aput-object v1, v0, v7

    sget-object v1, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->ALVEOLAR:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    aput-object v1, v0, v8

    sget-object v1, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->POSTALVEOLAR:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->PALATAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->VELAR:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->GLOTTAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->RETROFLEX:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->UVULAR:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->LABIODENTAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->PHARYNGEAL:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    aput-object v2, v0, v1

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->$VALUES:[Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 64
    new-instance v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation$1;

    invoke-direct {v0}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation$1;-><init>()V

    sput-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 74
    iput p3, p0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->index:I

    .line 75
    iput p4, p0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->value:I

    .line 76
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    return-object v0
.end method

.method public static values()[Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->$VALUES:[Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    invoke-virtual {v0}, [Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->value:I

    return v0
.end method
