.class public final Lspeech/patts/HomographDefinitions;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "HomographDefinitions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/HomographDefinitions$1;,
        Lspeech/patts/HomographDefinitions$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/HomographDefinitions;


# instance fields
.field private definition_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/HomographDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 299
    new-instance v0, Lspeech/patts/HomographDefinitions;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/HomographDefinitions;-><init>(Z)V

    sput-object v0, Lspeech/patts/HomographDefinitions;->defaultInstance:Lspeech/patts/HomographDefinitions;

    .line 300
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 301
    sget-object v0, Lspeech/patts/HomographDefinitions;->defaultInstance:Lspeech/patts/HomographDefinitions;

    invoke-direct {v0}, Lspeech/patts/HomographDefinitions;->initFields()V

    .line 302
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/HomographDefinitions;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/HomographDefinitions;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/HomographDefinitions$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/HomographDefinitions$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/HomographDefinitions;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/HomographDefinitions;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/HomographDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HomographDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/HomographDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/HomographDefinitions;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/HomographDefinitions;->defaultInstance:Lspeech/patts/HomographDefinitions;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method

.method public static newBuilder()Lspeech/patts/HomographDefinitions$Builder;
    .locals 1

    .prologue
    .line 132
    # invokes: Lspeech/patts/HomographDefinitions$Builder;->create()Lspeech/patts/HomographDefinitions$Builder;
    invoke-static {}, Lspeech/patts/HomographDefinitions$Builder;->access$100()Lspeech/patts/HomographDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/HomographDefinitions;)Lspeech/patts/HomographDefinitions$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/HomographDefinitions;

    .prologue
    .line 135
    invoke-static {}, Lspeech/patts/HomographDefinitions;->newBuilder()Lspeech/patts/HomographDefinitions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/HomographDefinitions$Builder;->mergeFrom(Lspeech/patts/HomographDefinitions;)Lspeech/patts/HomographDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/HomographDefinitions;->getDefaultInstanceForType()Lspeech/patts/HomographDefinitions;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/HomographDefinitions;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/HomographDefinitions;->defaultInstance:Lspeech/patts/HomographDefinitions;

    return-object v0
.end method

.method public getDefinitionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/HomographDefinition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 53
    iget v2, p0, Lspeech/patts/HomographDefinitions;->memoizedSerializedSize:I

    .line 54
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 62
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 56
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 57
    invoke-virtual {p0}, Lspeech/patts/HomographDefinitions;->getDefinitionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/HomographDefinition;

    .line 58
    .local v0, "element":Lspeech/patts/HomographDefinition;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 60
    goto :goto_1

    .line 61
    .end local v0    # "element":Lspeech/patts/HomographDefinition;
    :cond_1
    iput v2, p0, Lspeech/patts/HomographDefinitions;->memoizedSerializedSize:I

    move v3, v2

    .line 62
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    .line 37
    invoke-virtual {p0}, Lspeech/patts/HomographDefinitions;->getDefinitionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/HomographDefinition;

    .line 38
    .local v0, "element":Lspeech/patts/HomographDefinition;
    invoke-virtual {v0}, Lspeech/patts/HomographDefinition;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 40
    .end local v0    # "element":Lspeech/patts/HomographDefinition;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/HomographDefinitions;->toBuilder()Lspeech/patts/HomographDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/HomographDefinitions$Builder;
    .locals 1

    .prologue
    .line 137
    invoke-static {p0}, Lspeech/patts/HomographDefinitions;->newBuilder(Lspeech/patts/HomographDefinitions;)Lspeech/patts/HomographDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0}, Lspeech/patts/HomographDefinitions;->getSerializedSize()I

    .line 46
    invoke-virtual {p0}, Lspeech/patts/HomographDefinitions;->getDefinitionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/HomographDefinition;

    .line 47
    .local v0, "element":Lspeech/patts/HomographDefinition;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 49
    .end local v0    # "element":Lspeech/patts/HomographDefinition;
    :cond_0
    return-void
.end method
