.class public final Lspeech/patts/HmmDiagsProto$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "HmmDiagsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/HmmDiagsProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/HmmDiagsProto;",
        "Lspeech/patts/HmmDiagsProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/HmmDiagsProto;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 774
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1400()Lspeech/patts/HmmDiagsProto$Builder;
    .locals 1

    .prologue
    .line 768
    invoke-static {}, Lspeech/patts/HmmDiagsProto$Builder;->create()Lspeech/patts/HmmDiagsProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/HmmDiagsProto$Builder;
    .locals 3

    .prologue
    .line 777
    new-instance v0, Lspeech/patts/HmmDiagsProto$Builder;

    invoke-direct {v0}, Lspeech/patts/HmmDiagsProto$Builder;-><init>()V

    .line 778
    .local v0, "builder":Lspeech/patts/HmmDiagsProto$Builder;
    new-instance v1, Lspeech/patts/HmmDiagsProto;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/HmmDiagsProto;-><init>(Lspeech/patts/HmmDiagsProto$1;)V

    iput-object v1, v0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    .line 779
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 768
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$Builder;->build()Lspeech/patts/HmmDiagsProto;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/HmmDiagsProto;
    .locals 1

    .prologue
    .line 807
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 808
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    invoke-static {v0}, Lspeech/patts/HmmDiagsProto$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 810
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$Builder;->buildPartial()Lspeech/patts/HmmDiagsProto;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/HmmDiagsProto;
    .locals 3

    .prologue
    .line 823
    iget-object v1, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    if-nez v1, :cond_0

    .line 824
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 827
    :cond_0
    iget-object v1, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    # getter for: Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HmmDiagsProto;->access$1600(Lspeech/patts/HmmDiagsProto;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 828
    iget-object v1, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    iget-object v2, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    # getter for: Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HmmDiagsProto;->access$1600(Lspeech/patts/HmmDiagsProto;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HmmDiagsProto;->access$1602(Lspeech/patts/HmmDiagsProto;Ljava/util/List;)Ljava/util/List;

    .line 831
    :cond_1
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    .line 832
    .local v0, "returnMe":Lspeech/patts/HmmDiagsProto;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    .line 833
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 768
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$Builder;->clone()Lspeech/patts/HmmDiagsProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 768
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$Builder;->clone()Lspeech/patts/HmmDiagsProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 768
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$Builder;->clone()Lspeech/patts/HmmDiagsProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/HmmDiagsProto$Builder;
    .locals 2

    .prologue
    .line 796
    invoke-static {}, Lspeech/patts/HmmDiagsProto$Builder;->create()Lspeech/patts/HmmDiagsProto$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    invoke-virtual {v0, v1}, Lspeech/patts/HmmDiagsProto$Builder;->mergeFrom(Lspeech/patts/HmmDiagsProto;)Lspeech/patts/HmmDiagsProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 804
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    invoke-virtual {v0}, Lspeech/patts/HmmDiagsProto;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 768
    check-cast p1, Lspeech/patts/HmmDiagsProto;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/HmmDiagsProto$Builder;->mergeFrom(Lspeech/patts/HmmDiagsProto;)Lspeech/patts/HmmDiagsProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/HmmDiagsProto;)Lspeech/patts/HmmDiagsProto$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/HmmDiagsProto;

    .prologue
    .line 837
    invoke-static {}, Lspeech/patts/HmmDiagsProto;->getDefaultInstance()Lspeech/patts/HmmDiagsProto;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 844
    :cond_0
    :goto_0
    return-object p0

    .line 838
    :cond_1
    # getter for: Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HmmDiagsProto;->access$1600(Lspeech/patts/HmmDiagsProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 839
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    # getter for: Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HmmDiagsProto;->access$1600(Lspeech/patts/HmmDiagsProto;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 840
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HmmDiagsProto;->access$1602(Lspeech/patts/HmmDiagsProto;Ljava/util/List;)Ljava/util/List;

    .line 842
    :cond_2
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$Builder;->result:Lspeech/patts/HmmDiagsProto;

    # getter for: Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HmmDiagsProto;->access$1600(Lspeech/patts/HmmDiagsProto;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HmmDiagsProto;->access$1600(Lspeech/patts/HmmDiagsProto;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
