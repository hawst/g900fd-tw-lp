.class public final Lspeech/patts/Lattice$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Lattice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Lattice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/Lattice;",
        "Lspeech/patts/Lattice$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/Lattice;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/Lattice$Builder;
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Lspeech/patts/Lattice$Builder;->create()Lspeech/patts/Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/Lattice$Builder;
    .locals 3

    .prologue
    .line 145
    new-instance v0, Lspeech/patts/Lattice$Builder;

    invoke-direct {v0}, Lspeech/patts/Lattice$Builder;-><init>()V

    .line 146
    .local v0, "builder":Lspeech/patts/Lattice$Builder;
    new-instance v1, Lspeech/patts/Lattice;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/Lattice;-><init>(Lspeech/patts/Lattice$1;)V

    iput-object v1, v0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    .line 147
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lspeech/patts/Lattice$Builder;->build()Lspeech/patts/Lattice;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/Lattice;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/Lattice$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    invoke-static {v0}, Lspeech/patts/Lattice$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 178
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Lattice$Builder;->buildPartial()Lspeech/patts/Lattice;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/Lattice;
    .locals 3

    .prologue
    .line 191
    iget-object v1, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    if-nez v1, :cond_0

    .line 192
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 195
    :cond_0
    iget-object v1, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    # getter for: Lspeech/patts/Lattice;->column_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Lattice;->access$300(Lspeech/patts/Lattice;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 196
    iget-object v1, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    iget-object v2, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    # getter for: Lspeech/patts/Lattice;->column_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Lattice;->access$300(Lspeech/patts/Lattice;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Lattice;->column_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Lattice;->access$302(Lspeech/patts/Lattice;Ljava/util/List;)Ljava/util/List;

    .line 199
    :cond_1
    iget-object v0, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    .line 200
    .local v0, "returnMe":Lspeech/patts/Lattice;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    .line 201
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lspeech/patts/Lattice$Builder;->clone()Lspeech/patts/Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lspeech/patts/Lattice$Builder;->clone()Lspeech/patts/Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p0}, Lspeech/patts/Lattice$Builder;->clone()Lspeech/patts/Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/Lattice$Builder;
    .locals 2

    .prologue
    .line 164
    invoke-static {}, Lspeech/patts/Lattice$Builder;->create()Lspeech/patts/Lattice$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    invoke-virtual {v0, v1}, Lspeech/patts/Lattice$Builder;->mergeFrom(Lspeech/patts/Lattice;)Lspeech/patts/Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    invoke-virtual {v0}, Lspeech/patts/Lattice;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 136
    check-cast p1, Lspeech/patts/Lattice;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/Lattice$Builder;->mergeFrom(Lspeech/patts/Lattice;)Lspeech/patts/Lattice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/Lattice;)Lspeech/patts/Lattice$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/Lattice;

    .prologue
    .line 205
    invoke-static {}, Lspeech/patts/Lattice;->getDefaultInstance()Lspeech/patts/Lattice;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-object p0

    .line 206
    :cond_1
    # getter for: Lspeech/patts/Lattice;->column_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Lattice;->access$300(Lspeech/patts/Lattice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    iget-object v0, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    # getter for: Lspeech/patts/Lattice;->column_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Lattice;->access$300(Lspeech/patts/Lattice;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 208
    iget-object v0, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Lattice;->column_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Lattice;->access$302(Lspeech/patts/Lattice;Ljava/util/List;)Ljava/util/List;

    .line 210
    :cond_2
    iget-object v0, p0, Lspeech/patts/Lattice$Builder;->result:Lspeech/patts/Lattice;

    # getter for: Lspeech/patts/Lattice;->column_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Lattice;->access$300(Lspeech/patts/Lattice;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Lattice;->column_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Lattice;->access$300(Lspeech/patts/Lattice;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
