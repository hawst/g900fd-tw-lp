.class public final Lspeech/patts/AcousticFeature$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AcousticFeature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/AcousticFeature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/AcousticFeature;",
        "Lspeech/patts/AcousticFeature$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/AcousticFeature;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/AcousticFeature$Builder;
    .locals 1

    .prologue
    .line 213
    invoke-static {}, Lspeech/patts/AcousticFeature$Builder;->create()Lspeech/patts/AcousticFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/AcousticFeature$Builder;
    .locals 3

    .prologue
    .line 222
    new-instance v0, Lspeech/patts/AcousticFeature$Builder;

    invoke-direct {v0}, Lspeech/patts/AcousticFeature$Builder;-><init>()V

    .line 223
    .local v0, "builder":Lspeech/patts/AcousticFeature$Builder;
    new-instance v1, Lspeech/patts/AcousticFeature;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/AcousticFeature;-><init>(Lspeech/patts/AcousticFeature$1;)V

    iput-object v1, v0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    .line 224
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature$Builder;->build()Lspeech/patts/AcousticFeature;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/AcousticFeature;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/AcousticFeature$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    iget-object v0, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    invoke-static {v0}, Lspeech/patts/AcousticFeature$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 255
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature$Builder;->buildPartial()Lspeech/patts/AcousticFeature;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/AcousticFeature;
    .locals 3

    .prologue
    .line 268
    iget-object v1, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    if-nez v1, :cond_0

    .line 269
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 272
    :cond_0
    iget-object v0, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    .line 273
    .local v0, "returnMe":Lspeech/patts/AcousticFeature;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    .line 274
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature$Builder;->clone()Lspeech/patts/AcousticFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature$Builder;->clone()Lspeech/patts/AcousticFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 213
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature$Builder;->clone()Lspeech/patts/AcousticFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/AcousticFeature$Builder;
    .locals 2

    .prologue
    .line 241
    invoke-static {}, Lspeech/patts/AcousticFeature$Builder;->create()Lspeech/patts/AcousticFeature$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    invoke-virtual {v0, v1}, Lspeech/patts/AcousticFeature$Builder;->mergeFrom(Lspeech/patts/AcousticFeature;)Lspeech/patts/AcousticFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    invoke-virtual {v0}, Lspeech/patts/AcousticFeature;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 213
    check-cast p1, Lspeech/patts/AcousticFeature;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/AcousticFeature$Builder;->mergeFrom(Lspeech/patts/AcousticFeature;)Lspeech/patts/AcousticFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/AcousticFeature;)Lspeech/patts/AcousticFeature$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/AcousticFeature;

    .prologue
    .line 278
    invoke-static {}, Lspeech/patts/AcousticFeature;->getDefaultInstance()Lspeech/patts/AcousticFeature;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 291
    :cond_0
    :goto_0
    return-object p0

    .line 279
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/AcousticFeature;->hasFilename()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 280
    invoke-virtual {p1}, Lspeech/patts/AcousticFeature;->getFilename()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/AcousticFeature$Builder;->setFilename(Ljava/lang/String;)Lspeech/patts/AcousticFeature$Builder;

    .line 282
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/AcousticFeature;->hasSize()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 283
    invoke-virtual {p1}, Lspeech/patts/AcousticFeature;->getSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/AcousticFeature$Builder;->setSize(I)Lspeech/patts/AcousticFeature$Builder;

    .line 285
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/AcousticFeature;->hasOrient()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 286
    invoke-virtual {p1}, Lspeech/patts/AcousticFeature;->getOrient()Lspeech/patts/AcousticFeature$Orientation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/AcousticFeature$Builder;->setOrient(Lspeech/patts/AcousticFeature$Orientation;)Lspeech/patts/AcousticFeature$Builder;

    .line 288
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/AcousticFeature;->hasFeature()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {p1}, Lspeech/patts/AcousticFeature;->getFeature()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/AcousticFeature$Builder;->setFeature(Ljava/lang/String;)Lspeech/patts/AcousticFeature$Builder;

    goto :goto_0
.end method

.method public setFeature(Ljava/lang/String;)Lspeech/patts/AcousticFeature$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 402
    if-nez p1, :cond_0

    .line 403
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 405
    :cond_0
    iget-object v0, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/AcousticFeature;->hasFeature:Z
    invoke-static {v0, v1}, Lspeech/patts/AcousticFeature;->access$902(Lspeech/patts/AcousticFeature;Z)Z

    .line 406
    iget-object v0, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    # setter for: Lspeech/patts/AcousticFeature;->feature_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/AcousticFeature;->access$1002(Lspeech/patts/AcousticFeature;Ljava/lang/String;)Ljava/lang/String;

    .line 407
    return-object p0
.end method

.method public setFilename(Ljava/lang/String;)Lspeech/patts/AcousticFeature$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 342
    if-nez p1, :cond_0

    .line 343
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 345
    :cond_0
    iget-object v0, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/AcousticFeature;->hasFilename:Z
    invoke-static {v0, v1}, Lspeech/patts/AcousticFeature;->access$302(Lspeech/patts/AcousticFeature;Z)Z

    .line 346
    iget-object v0, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    # setter for: Lspeech/patts/AcousticFeature;->filename_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/AcousticFeature;->access$402(Lspeech/patts/AcousticFeature;Ljava/lang/String;)Ljava/lang/String;

    .line 347
    return-object p0
.end method

.method public setOrient(Lspeech/patts/AcousticFeature$Orientation;)Lspeech/patts/AcousticFeature$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/AcousticFeature$Orientation;

    .prologue
    .line 381
    if-nez p1, :cond_0

    .line 382
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 384
    :cond_0
    iget-object v0, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/AcousticFeature;->hasOrient:Z
    invoke-static {v0, v1}, Lspeech/patts/AcousticFeature;->access$702(Lspeech/patts/AcousticFeature;Z)Z

    .line 385
    iget-object v0, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    # setter for: Lspeech/patts/AcousticFeature;->orient_:Lspeech/patts/AcousticFeature$Orientation;
    invoke-static {v0, p1}, Lspeech/patts/AcousticFeature;->access$802(Lspeech/patts/AcousticFeature;Lspeech/patts/AcousticFeature$Orientation;)Lspeech/patts/AcousticFeature$Orientation;

    .line 386
    return-object p0
.end method

.method public setSize(I)Lspeech/patts/AcousticFeature$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 363
    iget-object v0, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/AcousticFeature;->hasSize:Z
    invoke-static {v0, v1}, Lspeech/patts/AcousticFeature;->access$502(Lspeech/patts/AcousticFeature;Z)Z

    .line 364
    iget-object v0, p0, Lspeech/patts/AcousticFeature$Builder;->result:Lspeech/patts/AcousticFeature;

    # setter for: Lspeech/patts/AcousticFeature;->size_:I
    invoke-static {v0, p1}, Lspeech/patts/AcousticFeature;->access$602(Lspeech/patts/AcousticFeature;I)I

    .line 365
    return-object p0
.end method
