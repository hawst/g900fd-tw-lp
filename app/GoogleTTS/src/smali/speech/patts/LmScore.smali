.class public final Lspeech/patts/LmScore;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LmScore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/LmScore$1;,
        Lspeech/patts/LmScore$Builder;,
        Lspeech/patts/LmScore$Type;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/LmScore;


# instance fields
.field private bigram_:F

.field private hasBigram:Z

.field private hasTrigram:Z

.field private hasType:Z

.field private hasUnigram:Z

.field private memoizedSerializedSize:I

.field private trigram_:F

.field private type_:Lspeech/patts/LmScore$Type;

.field private unigram_:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 411
    new-instance v0, Lspeech/patts/LmScore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/LmScore;-><init>(Z)V

    sput-object v0, Lspeech/patts/LmScore;->defaultInstance:Lspeech/patts/LmScore;

    .line 412
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 413
    sget-object v0, Lspeech/patts/LmScore;->defaultInstance:Lspeech/patts/LmScore;

    invoke-direct {v0}, Lspeech/patts/LmScore;->initFields()V

    .line 414
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 69
    iput v0, p0, Lspeech/patts/LmScore;->unigram_:F

    .line 76
    iput v0, p0, Lspeech/patts/LmScore;->bigram_:F

    .line 83
    iput v0, p0, Lspeech/patts/LmScore;->trigram_:F

    .line 111
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/LmScore;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/LmScore;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/LmScore$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/LmScore$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/LmScore;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 69
    iput v0, p0, Lspeech/patts/LmScore;->unigram_:F

    .line 76
    iput v0, p0, Lspeech/patts/LmScore;->bigram_:F

    .line 83
    iput v0, p0, Lspeech/patts/LmScore;->trigram_:F

    .line 111
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/LmScore;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/LmScore;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LmScore;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/LmScore;->trigram_:F

    return p1
.end method

.method static synthetic access$302(Lspeech/patts/LmScore;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LmScore;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/LmScore;->hasType:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/LmScore;Lspeech/patts/LmScore$Type;)Lspeech/patts/LmScore$Type;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LmScore;
    .param p1, "x1"    # Lspeech/patts/LmScore$Type;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/LmScore;->type_:Lspeech/patts/LmScore$Type;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/LmScore;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LmScore;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/LmScore;->hasUnigram:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/LmScore;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LmScore;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/LmScore;->unigram_:F

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/LmScore;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LmScore;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/LmScore;->hasBigram:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/LmScore;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LmScore;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/LmScore;->bigram_:F

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/LmScore;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LmScore;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/LmScore;->hasTrigram:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/LmScore;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/LmScore;->defaultInstance:Lspeech/patts/LmScore;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lspeech/patts/LmScore$Type;->PRODLM:Lspeech/patts/LmScore$Type;

    iput-object v0, p0, Lspeech/patts/LmScore;->type_:Lspeech/patts/LmScore$Type;

    .line 89
    return-void
.end method

.method public static newBuilder()Lspeech/patts/LmScore$Builder;
    .locals 1

    .prologue
    .line 204
    # invokes: Lspeech/patts/LmScore$Builder;->create()Lspeech/patts/LmScore$Builder;
    invoke-static {}, Lspeech/patts/LmScore$Builder;->access$100()Lspeech/patts/LmScore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/LmScore;)Lspeech/patts/LmScore$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/LmScore;

    .prologue
    .line 207
    invoke-static {}, Lspeech/patts/LmScore;->newBuilder()Lspeech/patts/LmScore$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/LmScore$Builder;->mergeFrom(Lspeech/patts/LmScore;)Lspeech/patts/LmScore$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBigram()F
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lspeech/patts/LmScore;->bigram_:F

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/LmScore;->getDefaultInstanceForType()Lspeech/patts/LmScore;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/LmScore;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/LmScore;->defaultInstance:Lspeech/patts/LmScore;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 113
    iget v0, p0, Lspeech/patts/LmScore;->memoizedSerializedSize:I

    .line 114
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 134
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 116
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 117
    invoke-virtual {p0}, Lspeech/patts/LmScore;->hasType()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 118
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/LmScore;->getType()Lspeech/patts/LmScore$Type;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/LmScore$Type;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 121
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/LmScore;->hasUnigram()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 122
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/LmScore;->getUnigram()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 125
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/LmScore;->hasBigram()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 126
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/LmScore;->getBigram()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 129
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/LmScore;->hasTrigram()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 130
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/LmScore;->getTrigram()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 133
    :cond_4
    iput v0, p0, Lspeech/patts/LmScore;->memoizedSerializedSize:I

    move v1, v0

    .line 134
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getTrigram()F
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lspeech/patts/LmScore;->trigram_:F

    return v0
.end method

.method public getType()Lspeech/patts/LmScore$Type;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lspeech/patts/LmScore;->type_:Lspeech/patts/LmScore$Type;

    return-object v0
.end method

.method public getUnigram()F
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lspeech/patts/LmScore;->unigram_:F

    return v0
.end method

.method public hasBigram()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lspeech/patts/LmScore;->hasBigram:Z

    return v0
.end method

.method public hasTrigram()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lspeech/patts/LmScore;->hasTrigram:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lspeech/patts/LmScore;->hasType:Z

    return v0
.end method

.method public hasUnigram()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lspeech/patts/LmScore;->hasUnigram:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/LmScore;->toBuilder()Lspeech/patts/LmScore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/LmScore$Builder;
    .locals 1

    .prologue
    .line 209
    invoke-static {p0}, Lspeech/patts/LmScore;->newBuilder(Lspeech/patts/LmScore;)Lspeech/patts/LmScore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0}, Lspeech/patts/LmScore;->getSerializedSize()I

    .line 97
    invoke-virtual {p0}, Lspeech/patts/LmScore;->hasType()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/LmScore;->getType()Lspeech/patts/LmScore$Type;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/LmScore$Type;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 100
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/LmScore;->hasUnigram()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/LmScore;->getUnigram()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 103
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/LmScore;->hasBigram()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 104
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/LmScore;->getBigram()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 106
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/LmScore;->hasTrigram()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 107
    const/4 v0, 0x4

    invoke-virtual {p0}, Lspeech/patts/LmScore;->getTrigram()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 109
    :cond_3
    return-void
.end method
