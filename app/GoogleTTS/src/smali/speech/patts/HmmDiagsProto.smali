.class public final Lspeech/patts/HmmDiagsProto;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "HmmDiagsProto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/HmmDiagsProto$1;,
        Lspeech/patts/HmmDiagsProto$Builder;,
        Lspeech/patts/HmmDiagsProto$HmmModel;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/HmmDiagsProto;


# instance fields
.field private memoizedSerializedSize:I

.field private model_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/HmmDiagsProto$HmmModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 928
    new-instance v0, Lspeech/patts/HmmDiagsProto;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/HmmDiagsProto;-><init>(Z)V

    sput-object v0, Lspeech/patts/HmmDiagsProto;->defaultInstance:Lspeech/patts/HmmDiagsProto;

    .line 929
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 930
    sget-object v0, Lspeech/patts/HmmDiagsProto;->defaultInstance:Lspeech/patts/HmmDiagsProto;

    invoke-direct {v0}, Lspeech/patts/HmmDiagsProto;->initFields()V

    .line 931
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 656
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;

    .line 680
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/HmmDiagsProto;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/HmmDiagsProto;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/HmmDiagsProto$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/HmmDiagsProto$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/HmmDiagsProto;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 656
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;

    .line 680
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/HmmDiagsProto;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1600(Lspeech/patts/HmmDiagsProto;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HmmDiagsProto;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1602(Lspeech/patts/HmmDiagsProto;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HmmDiagsProto;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/HmmDiagsProto;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/HmmDiagsProto;->defaultInstance:Lspeech/patts/HmmDiagsProto;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 667
    return-void
.end method

.method public static newBuilder()Lspeech/patts/HmmDiagsProto$Builder;
    .locals 1

    .prologue
    .line 761
    # invokes: Lspeech/patts/HmmDiagsProto$Builder;->create()Lspeech/patts/HmmDiagsProto$Builder;
    invoke-static {}, Lspeech/patts/HmmDiagsProto$Builder;->access$1400()Lspeech/patts/HmmDiagsProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/HmmDiagsProto;)Lspeech/patts/HmmDiagsProto$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/HmmDiagsProto;

    .prologue
    .line 764
    invoke-static {}, Lspeech/patts/HmmDiagsProto;->newBuilder()Lspeech/patts/HmmDiagsProto$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/HmmDiagsProto$Builder;->mergeFrom(Lspeech/patts/HmmDiagsProto;)Lspeech/patts/HmmDiagsProto$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto;->getDefaultInstanceForType()Lspeech/patts/HmmDiagsProto;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/HmmDiagsProto;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/HmmDiagsProto;->defaultInstance:Lspeech/patts/HmmDiagsProto;

    return-object v0
.end method

.method public getModelList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/HmmDiagsProto$HmmModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 659
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto;->model_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 682
    iget v2, p0, Lspeech/patts/HmmDiagsProto;->memoizedSerializedSize:I

    .line 683
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 691
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 685
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 686
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto;->getModelList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/HmmDiagsProto$HmmModel;

    .line 687
    .local v0, "element":Lspeech/patts/HmmDiagsProto$HmmModel;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 689
    goto :goto_1

    .line 690
    .end local v0    # "element":Lspeech/patts/HmmDiagsProto$HmmModel;
    :cond_1
    iput v2, p0, Lspeech/patts/HmmDiagsProto;->memoizedSerializedSize:I

    move v3, v2

    .line 691
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 669
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto;->toBuilder()Lspeech/patts/HmmDiagsProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/HmmDiagsProto$Builder;
    .locals 1

    .prologue
    .line 766
    invoke-static {p0}, Lspeech/patts/HmmDiagsProto;->newBuilder(Lspeech/patts/HmmDiagsProto;)Lspeech/patts/HmmDiagsProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 674
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto;->getSerializedSize()I

    .line 675
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto;->getModelList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/HmmDiagsProto$HmmModel;

    .line 676
    .local v0, "element":Lspeech/patts/HmmDiagsProto$HmmModel;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 678
    .end local v0    # "element":Lspeech/patts/HmmDiagsProto$HmmModel;
    :cond_0
    return-void
.end method
