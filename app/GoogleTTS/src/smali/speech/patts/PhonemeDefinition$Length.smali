.class public final enum Lspeech/patts/PhonemeDefinition$Length;
.super Ljava/lang/Enum;
.source "PhonemeDefinition.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PhonemeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Length"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/PhonemeDefinition$Length;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/PhonemeDefinition$Length;

.field public static final enum DIPHTHONG:Lspeech/patts/PhonemeDefinition$Length;

.field public static final enum LONG:Lspeech/patts/PhonemeDefinition$Length;

.field public static final enum SHORT:Lspeech/patts/PhonemeDefinition$Length;

.field public static final enum UNDEFINED_LENGTH:Lspeech/patts/PhonemeDefinition$Length;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/PhonemeDefinition$Length;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 236
    new-instance v0, Lspeech/patts/PhonemeDefinition$Length;

    const-string v1, "UNDEFINED_LENGTH"

    invoke-direct {v0, v1, v2, v2, v2}, Lspeech/patts/PhonemeDefinition$Length;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Length;->UNDEFINED_LENGTH:Lspeech/patts/PhonemeDefinition$Length;

    .line 237
    new-instance v0, Lspeech/patts/PhonemeDefinition$Length;

    const-string v1, "SHORT"

    invoke-direct {v0, v1, v3, v3, v3}, Lspeech/patts/PhonemeDefinition$Length;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Length;->SHORT:Lspeech/patts/PhonemeDefinition$Length;

    .line 238
    new-instance v0, Lspeech/patts/PhonemeDefinition$Length;

    const-string v1, "LONG"

    invoke-direct {v0, v1, v4, v4, v4}, Lspeech/patts/PhonemeDefinition$Length;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Length;->LONG:Lspeech/patts/PhonemeDefinition$Length;

    .line 239
    new-instance v0, Lspeech/patts/PhonemeDefinition$Length;

    const-string v1, "DIPHTHONG"

    invoke-direct {v0, v1, v5, v5, v5}, Lspeech/patts/PhonemeDefinition$Length;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Length;->DIPHTHONG:Lspeech/patts/PhonemeDefinition$Length;

    .line 234
    const/4 v0, 0x4

    new-array v0, v0, [Lspeech/patts/PhonemeDefinition$Length;

    sget-object v1, Lspeech/patts/PhonemeDefinition$Length;->UNDEFINED_LENGTH:Lspeech/patts/PhonemeDefinition$Length;

    aput-object v1, v0, v2

    sget-object v1, Lspeech/patts/PhonemeDefinition$Length;->SHORT:Lspeech/patts/PhonemeDefinition$Length;

    aput-object v1, v0, v3

    sget-object v1, Lspeech/patts/PhonemeDefinition$Length;->LONG:Lspeech/patts/PhonemeDefinition$Length;

    aput-object v1, v0, v4

    sget-object v1, Lspeech/patts/PhonemeDefinition$Length;->DIPHTHONG:Lspeech/patts/PhonemeDefinition$Length;

    aput-object v1, v0, v5

    sput-object v0, Lspeech/patts/PhonemeDefinition$Length;->$VALUES:[Lspeech/patts/PhonemeDefinition$Length;

    .line 260
    new-instance v0, Lspeech/patts/PhonemeDefinition$Length$1;

    invoke-direct {v0}, Lspeech/patts/PhonemeDefinition$Length$1;-><init>()V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Length;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 269
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 270
    iput p3, p0, Lspeech/patts/PhonemeDefinition$Length;->index:I

    .line 271
    iput p4, p0, Lspeech/patts/PhonemeDefinition$Length;->value:I

    .line 272
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/PhonemeDefinition$Length;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 234
    const-class v0, Lspeech/patts/PhonemeDefinition$Length;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonemeDefinition$Length;

    return-object v0
.end method

.method public static values()[Lspeech/patts/PhonemeDefinition$Length;
    .locals 1

    .prologue
    .line 234
    sget-object v0, Lspeech/patts/PhonemeDefinition$Length;->$VALUES:[Lspeech/patts/PhonemeDefinition$Length;

    invoke-virtual {v0}, [Lspeech/patts/PhonemeDefinition$Length;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/PhonemeDefinition$Length;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lspeech/patts/PhonemeDefinition$Length;->value:I

    return v0
.end method
