.class public final Lspeech/patts/PhoneStats;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PhoneStats.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/PhoneStats$1;,
        Lspeech/patts/PhoneStats$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/PhoneStats;


# instance fields
.field private abszdur_:F

.field private cut_:F

.field private hasAbszdur:Z

.field private hasCut:Z

.field private hasPhonScore:Z

.field private hasScore:Z

.field private hasZscore:Z

.field private memoizedSerializedSize:I

.field private phonScore_:F

.field private score_:F

.field private zscore_:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 405
    new-instance v0, Lspeech/patts/PhoneStats;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/PhoneStats;-><init>(Z)V

    sput-object v0, Lspeech/patts/PhoneStats;->defaultInstance:Lspeech/patts/PhoneStats;

    .line 406
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 407
    sget-object v0, Lspeech/patts/PhoneStats;->defaultInstance:Lspeech/patts/PhoneStats;

    invoke-direct {v0}, Lspeech/patts/PhoneStats;->initFields()V

    .line 408
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lspeech/patts/PhoneStats;->abszdur_:F

    .line 32
    iput v0, p0, Lspeech/patts/PhoneStats;->zscore_:F

    .line 39
    iput v0, p0, Lspeech/patts/PhoneStats;->phonScore_:F

    .line 46
    iput v0, p0, Lspeech/patts/PhoneStats;->cut_:F

    .line 53
    iput v0, p0, Lspeech/patts/PhoneStats;->score_:F

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PhoneStats;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/PhoneStats;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/PhoneStats$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/PhoneStats$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/PhoneStats;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    iput v0, p0, Lspeech/patts/PhoneStats;->abszdur_:F

    .line 32
    iput v0, p0, Lspeech/patts/PhoneStats;->zscore_:F

    .line 39
    iput v0, p0, Lspeech/patts/PhoneStats;->phonScore_:F

    .line 46
    iput v0, p0, Lspeech/patts/PhoneStats;->cut_:F

    .line 53
    iput v0, p0, Lspeech/patts/PhoneStats;->score_:F

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PhoneStats;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/PhoneStats;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhoneStats;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/PhoneStats;->cut_:F

    return p1
.end method

.method static synthetic access$1102(Lspeech/patts/PhoneStats;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhoneStats;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhoneStats;->hasScore:Z

    return p1
.end method

.method static synthetic access$1202(Lspeech/patts/PhoneStats;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhoneStats;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/PhoneStats;->score_:F

    return p1
.end method

.method static synthetic access$302(Lspeech/patts/PhoneStats;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhoneStats;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhoneStats;->hasAbszdur:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/PhoneStats;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhoneStats;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/PhoneStats;->abszdur_:F

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/PhoneStats;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhoneStats;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhoneStats;->hasZscore:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/PhoneStats;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhoneStats;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/PhoneStats;->zscore_:F

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/PhoneStats;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhoneStats;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhoneStats;->hasPhonScore:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/PhoneStats;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhoneStats;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/PhoneStats;->phonScore_:F

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/PhoneStats;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhoneStats;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhoneStats;->hasCut:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/PhoneStats;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/PhoneStats;->defaultInstance:Lspeech/patts/PhoneStats;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method public static newBuilder()Lspeech/patts/PhoneStats$Builder;
    .locals 1

    .prologue
    .line 180
    # invokes: Lspeech/patts/PhoneStats$Builder;->create()Lspeech/patts/PhoneStats$Builder;
    invoke-static {}, Lspeech/patts/PhoneStats$Builder;->access$100()Lspeech/patts/PhoneStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/PhoneStats;)Lspeech/patts/PhoneStats$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/PhoneStats;

    .prologue
    .line 183
    invoke-static {}, Lspeech/patts/PhoneStats;->newBuilder()Lspeech/patts/PhoneStats$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/PhoneStats$Builder;->mergeFrom(Lspeech/patts/PhoneStats;)Lspeech/patts/PhoneStats$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAbszdur()F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lspeech/patts/PhoneStats;->abszdur_:F

    return v0
.end method

.method public getCut()F
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lspeech/patts/PhoneStats;->cut_:F

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->getDefaultInstanceForType()Lspeech/patts/PhoneStats;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/PhoneStats;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/PhoneStats;->defaultInstance:Lspeech/patts/PhoneStats;

    return-object v0
.end method

.method public getPhonScore()F
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lspeech/patts/PhoneStats;->phonScore_:F

    return v0
.end method

.method public getScore()F
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lspeech/patts/PhoneStats;->score_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 85
    iget v0, p0, Lspeech/patts/PhoneStats;->memoizedSerializedSize:I

    .line 86
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 110
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 88
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 89
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->hasAbszdur()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->getAbszdur()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 93
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->hasZscore()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 94
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->getZscore()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 97
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->hasPhonScore()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 98
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->getPhonScore()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 101
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->hasCut()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 102
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->getCut()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 105
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->hasScore()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 106
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->getScore()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 109
    :cond_5
    iput v0, p0, Lspeech/patts/PhoneStats;->memoizedSerializedSize:I

    move v1, v0

    .line 110
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getZscore()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lspeech/patts/PhoneStats;->zscore_:F

    return v0
.end method

.method public hasAbszdur()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/PhoneStats;->hasAbszdur:Z

    return v0
.end method

.method public hasCut()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lspeech/patts/PhoneStats;->hasCut:Z

    return v0
.end method

.method public hasPhonScore()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lspeech/patts/PhoneStats;->hasPhonScore:Z

    return v0
.end method

.method public hasScore()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lspeech/patts/PhoneStats;->hasScore:Z

    return v0
.end method

.method public hasZscore()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/PhoneStats;->hasZscore:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->toBuilder()Lspeech/patts/PhoneStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/PhoneStats$Builder;
    .locals 1

    .prologue
    .line 185
    invoke-static {p0}, Lspeech/patts/PhoneStats;->newBuilder(Lspeech/patts/PhoneStats;)Lspeech/patts/PhoneStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->getSerializedSize()I

    .line 66
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->hasAbszdur()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->getAbszdur()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 69
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->hasZscore()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->getZscore()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 72
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->hasPhonScore()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->getPhonScore()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 75
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->hasCut()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 76
    const/4 v0, 0x4

    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->getCut()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 78
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->hasScore()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 79
    const/4 v0, 0x5

    invoke-virtual {p0}, Lspeech/patts/PhoneStats;->getScore()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 81
    :cond_4
    return-void
.end method
