.class public final Lspeech/patts/LinearRankerModel;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "LinearRankerModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/LinearRankerModel$1;,
        Lspeech/patts/LinearRankerModel$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/LinearRankerModel;


# instance fields
.field private featsAndVals_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/FeatAndVal;",
            ">;"
        }
    .end annotation
.end field

.field private hasSize:Z

.field private memoizedSerializedSize:I

.field private size_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 339
    new-instance v0, Lspeech/patts/LinearRankerModel;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/LinearRankerModel;-><init>(Z)V

    sput-object v0, Lspeech/patts/LinearRankerModel;->defaultInstance:Lspeech/patts/LinearRankerModel;

    .line 340
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 341
    sget-object v0, Lspeech/patts/LinearRankerModel;->defaultInstance:Lspeech/patts/LinearRankerModel;

    invoke-direct {v0}, Lspeech/patts/LinearRankerModel;->initFields()V

    .line 342
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lspeech/patts/LinearRankerModel;->size_:J

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/LinearRankerModel;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/LinearRankerModel;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/LinearRankerModel$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/LinearRankerModel$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/LinearRankerModel;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lspeech/patts/LinearRankerModel;->size_:J

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/LinearRankerModel;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/LinearRankerModel;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/LinearRankerModel;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/LinearRankerModel;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LinearRankerModel;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/LinearRankerModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/LinearRankerModel;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/LinearRankerModel;->hasSize:Z

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/LinearRankerModel;J)J
    .locals 1
    .param p0, "x0"    # Lspeech/patts/LinearRankerModel;
    .param p1, "x1"    # J

    .prologue
    .line 5
    iput-wide p1, p0, Lspeech/patts/LinearRankerModel;->size_:J

    return-wide p1
.end method

.method public static getDefaultInstance()Lspeech/patts/LinearRankerModel;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/LinearRankerModel;->defaultInstance:Lspeech/patts/LinearRankerModel;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public static newBuilder()Lspeech/patts/LinearRankerModel$Builder;
    .locals 1

    .prologue
    .line 147
    # invokes: Lspeech/patts/LinearRankerModel$Builder;->create()Lspeech/patts/LinearRankerModel$Builder;
    invoke-static {}, Lspeech/patts/LinearRankerModel$Builder;->access$100()Lspeech/patts/LinearRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/LinearRankerModel;)Lspeech/patts/LinearRankerModel$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/LinearRankerModel;

    .prologue
    .line 150
    invoke-static {}, Lspeech/patts/LinearRankerModel;->newBuilder()Lspeech/patts/LinearRankerModel$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/LinearRankerModel$Builder;->mergeFrom(Lspeech/patts/LinearRankerModel;)Lspeech/patts/LinearRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel;->getDefaultInstanceForType()Lspeech/patts/LinearRankerModel;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/LinearRankerModel;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/LinearRankerModel;->defaultInstance:Lspeech/patts/LinearRankerModel;

    return-object v0
.end method

.method public getFeatsAndValsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/FeatAndVal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    .line 64
    iget v2, p0, Lspeech/patts/LinearRankerModel;->memoizedSerializedSize:I

    .line 65
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 77
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 67
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 68
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel;->hasSize()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 69
    const/4 v4, 0x1

    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel;->getSize()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 72
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel;->getFeatsAndValsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/FeatAndVal;

    .line 73
    .local v0, "element":Lspeech/patts/FeatAndVal;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 75
    goto :goto_1

    .line 76
    .end local v0    # "element":Lspeech/patts/FeatAndVal;
    :cond_2
    iput v2, p0, Lspeech/patts/LinearRankerModel;->memoizedSerializedSize:I

    move v3, v2

    .line 77
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, Lspeech/patts/LinearRankerModel;->size_:J

    return-wide v0
.end method

.method public hasSize()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/LinearRankerModel;->hasSize:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 44
    iget-boolean v3, p0, Lspeech/patts/LinearRankerModel;->hasSize:Z

    if-nez v3, :cond_0

    .line 48
    :goto_0
    return v2

    .line 45
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel;->getFeatsAndValsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/FeatAndVal;

    .line 46
    .local v0, "element":Lspeech/patts/FeatAndVal;
    invoke-virtual {v0}, Lspeech/patts/FeatAndVal;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 48
    .end local v0    # "element":Lspeech/patts/FeatAndVal;
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel;->toBuilder()Lspeech/patts/LinearRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/LinearRankerModel$Builder;
    .locals 1

    .prologue
    .line 152
    invoke-static {p0}, Lspeech/patts/LinearRankerModel;->newBuilder(Lspeech/patts/LinearRankerModel;)Lspeech/patts/LinearRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel;->getSerializedSize()I

    .line 54
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel;->hasSize()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel;->getSize()J

    move-result-wide v4

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 57
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel;->getFeatsAndValsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/FeatAndVal;

    .line 58
    .local v0, "element":Lspeech/patts/FeatAndVal;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 60
    .end local v0    # "element":Lspeech/patts/FeatAndVal;
    :cond_1
    return-void
.end method
