.class public final Lspeech/patts/Token;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Token.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/Token$1;,
        Lspeech/patts/Token$Builder;,
        Lspeech/patts/Token$PauseLength;,
        Lspeech/patts/Token$State;,
        Lspeech/patts/Token$Type;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/Token;


# instance fields
.field private abbreviation_:Lcom/google/speech/tts/Abbreviation;

.field private address_:Lcom/google/speech/tts/Address;

.field private cardinal_:Lcom/google/speech/tts/Cardinal;

.field private concept_:Ljava/lang/String;

.field private connector_:Lcom/google/speech/tts/Connector;

.field private date_:Lcom/google/speech/tts/Date;

.field private decimal_:Lcom/google/speech/tts/Decimal;

.field private digit_:Ljava/lang/String;

.field private duplicator_:Lcom/google/speech/tts/Duplicator;

.field private electronic_:Lcom/google/speech/tts/Electronic;

.field private endIndex_:I

.field private firstDaughter_:I

.field private fraction_:Lcom/google/speech/tts/Fraction;

.field private hasAbbreviation:Z

.field private hasAddress:Z

.field private hasCardinal:Z

.field private hasConcept:Z

.field private hasConnector:Z

.field private hasDate:Z

.field private hasDecimal:Z

.field private hasDigit:Z

.field private hasDuplicator:Z

.field private hasElectronic:Z

.field private hasEndIndex:Z

.field private hasFirstDaughter:Z

.field private hasFraction:Z

.field private hasLastDaughter:Z

.field private hasLetters:Z

.field private hasMeasure:Z

.field private hasMoney:Z

.field private hasName:Z

.field private hasNextSpace:Z

.field private hasOrdinal:Z

.field private hasParent:Z

.field private hasPauseDuration:Z

.field private hasPauseLength:Z

.field private hasPercent:Z

.field private hasPhraseBreak:Z

.field private hasReservedForBackwardsCompatibility1:Z

.field private hasReservedForBackwardsCompatibility2:Z

.field private hasSemioticClass:Z

.field private hasSkip:Z

.field private hasSpelling:Z

.field private hasSpellingWithStress:Z

.field private hasStartIndex:Z

.field private hasSyntax:Z

.field private hasTelephone:Z

.field private hasTime:Z

.field private hasType:Z

.field private hasUserFeatures:Z

.field private hasVariant:Z

.field private hasVerbatim:Z

.field private hasVoiceMod:Z

.field private hasWordid:Z

.field private homograph_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private lastDaughter_:I

.field private letters_:Ljava/lang/String;

.field private measure_:Lcom/google/speech/tts/Measure;

.field private memoizedSerializedSize:I

.field private money_:Lcom/google/speech/tts/Money;

.field private name_:Ljava/lang/String;

.field private nextSpace_:Z

.field private ordinal_:Lcom/google/speech/tts/Ordinal;

.field private parent_:I

.field private pauseDuration_:F

.field private pauseLength_:Lspeech/patts/Token$PauseLength;

.field private percent_:Lcom/google/speech/tts/Decimal;

.field private phraseBreak_:Z

.field private reservedForBackwardsCompatibility1_:Z

.field private reservedForBackwardsCompatibility2_:Lcom/google/protobuf/ByteString;

.field private semioticClass_:Ljava/lang/String;

.field private skip_:Z

.field private spellingWithStress_:Ljava/lang/String;

.field private spelling_:Ljava/lang/String;

.field private startIndex_:I

.field private syntax_:Lspeech/patts/Syntax;

.field private telephone_:Lcom/google/speech/tts/Telephone;

.field private time_:Lcom/google/speech/tts/Time;

.field private type_:Lspeech/patts/Token$Type;

.field private userFeatures_:Ljava/lang/String;

.field private variant_:Ljava/lang/String;

.field private verbatim_:Ljava/lang/String;

.field private voiceMod_:Lcom/google/speech/tts/VoiceMod;

.field private wordid_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2521
    new-instance v0, Lspeech/patts/Token;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/Token;-><init>(Z)V

    sput-object v0, Lspeech/patts/Token;->defaultInstance:Lspeech/patts/Token;

    .line 2522
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 2523
    sget-object v0, Lspeech/patts/Token;->defaultInstance:Lspeech/patts/Token;

    invoke-direct {v0}, Lspeech/patts/Token;->initFields()V

    .line 2524
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 158
    iput v1, p0, Lspeech/patts/Token;->parent_:I

    .line 165
    iput v1, p0, Lspeech/patts/Token;->firstDaughter_:I

    .line 172
    iput v1, p0, Lspeech/patts/Token;->lastDaughter_:I

    .line 179
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->name_:Ljava/lang/String;

    .line 186
    iput-boolean v1, p0, Lspeech/patts/Token;->skip_:Z

    .line 193
    iput-boolean v1, p0, Lspeech/patts/Token;->nextSpace_:Z

    .line 207
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->wordid_:Ljava/lang/String;

    .line 214
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->variant_:Ljava/lang/String;

    .line 221
    iput-boolean v1, p0, Lspeech/patts/Token;->phraseBreak_:Z

    .line 228
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/Token;->pauseDuration_:F

    .line 241
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->homograph_:Ljava/util/List;

    .line 254
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->spelling_:Ljava/lang/String;

    .line 261
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->semioticClass_:Ljava/lang/String;

    .line 282
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->digit_:Ljava/lang/String;

    .line 359
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->verbatim_:Ljava/lang/String;

    .line 366
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->letters_:Ljava/lang/String;

    .line 380
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->concept_:Ljava/lang/String;

    .line 408
    iput v1, p0, Lspeech/patts/Token;->startIndex_:I

    .line 415
    iput v1, p0, Lspeech/patts/Token;->endIndex_:I

    .line 422
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->userFeatures_:Ljava/lang/String;

    .line 436
    iput-boolean v1, p0, Lspeech/patts/Token;->reservedForBackwardsCompatibility1_:Z

    .line 443
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lspeech/patts/Token;->reservedForBackwardsCompatibility2_:Lcom/google/protobuf/ByteString;

    .line 450
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->spellingWithStress_:Ljava/lang/String;

    .line 634
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Token;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/Token;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/Token$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/Token$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/Token;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 158
    iput v1, p0, Lspeech/patts/Token;->parent_:I

    .line 165
    iput v1, p0, Lspeech/patts/Token;->firstDaughter_:I

    .line 172
    iput v1, p0, Lspeech/patts/Token;->lastDaughter_:I

    .line 179
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->name_:Ljava/lang/String;

    .line 186
    iput-boolean v1, p0, Lspeech/patts/Token;->skip_:Z

    .line 193
    iput-boolean v1, p0, Lspeech/patts/Token;->nextSpace_:Z

    .line 207
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->wordid_:Ljava/lang/String;

    .line 214
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->variant_:Ljava/lang/String;

    .line 221
    iput-boolean v1, p0, Lspeech/patts/Token;->phraseBreak_:Z

    .line 228
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/Token;->pauseDuration_:F

    .line 241
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->homograph_:Ljava/util/List;

    .line 254
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->spelling_:Ljava/lang/String;

    .line 261
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->semioticClass_:Ljava/lang/String;

    .line 282
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->digit_:Ljava/lang/String;

    .line 359
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->verbatim_:Ljava/lang/String;

    .line 366
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->letters_:Ljava/lang/String;

    .line 380
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->concept_:Ljava/lang/String;

    .line 408
    iput v1, p0, Lspeech/patts/Token;->startIndex_:I

    .line 415
    iput v1, p0, Lspeech/patts/Token;->endIndex_:I

    .line 422
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->userFeatures_:Ljava/lang/String;

    .line 436
    iput-boolean v1, p0, Lspeech/patts/Token;->reservedForBackwardsCompatibility1_:Z

    .line 443
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lspeech/patts/Token;->reservedForBackwardsCompatibility2_:Lcom/google/protobuf/ByteString;

    .line 450
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Token;->spellingWithStress_:Ljava/lang/String;

    .line 634
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Token;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasName:Z

    return p1
.end method

.method static synthetic access$1102(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasSkip:Z

    return p1
.end method

.method static synthetic access$1302(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->skip_:Z

    return p1
.end method

.method static synthetic access$1402(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasNextSpace:Z

    return p1
.end method

.method static synthetic access$1502(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->nextSpace_:Z

    return p1
.end method

.method static synthetic access$1602(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasType:Z

    return p1
.end method

.method static synthetic access$1702(Lspeech/patts/Token;Lspeech/patts/Token$Type;)Lspeech/patts/Token$Type;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lspeech/patts/Token$Type;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->type_:Lspeech/patts/Token$Type;

    return-object p1
.end method

.method static synthetic access$1802(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasWordid:Z

    return p1
.end method

.method static synthetic access$1902(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->wordid_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2002(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasVariant:Z

    return p1
.end method

.method static synthetic access$2102(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->variant_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2202(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasPhraseBreak:Z

    return p1
.end method

.method static synthetic access$2302(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->phraseBreak_:Z

    return p1
.end method

.method static synthetic access$2402(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasPauseDuration:Z

    return p1
.end method

.method static synthetic access$2502(Lspeech/patts/Token;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Token;->pauseDuration_:F

    return p1
.end method

.method static synthetic access$2602(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasPauseLength:Z

    return p1
.end method

.method static synthetic access$2702(Lspeech/patts/Token;Lspeech/patts/Token$PauseLength;)Lspeech/patts/Token$PauseLength;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lspeech/patts/Token$PauseLength;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->pauseLength_:Lspeech/patts/Token$PauseLength;

    return-object p1
.end method

.method static synthetic access$2802(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasSpelling:Z

    return p1
.end method

.method static synthetic access$2902(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->spelling_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lspeech/patts/Token;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->homograph_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3002(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasSemioticClass:Z

    return p1
.end method

.method static synthetic access$302(Lspeech/patts/Token;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->homograph_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3102(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->semioticClass_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3202(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasCardinal:Z

    return p1
.end method

.method static synthetic access$3300(Lspeech/patts/Token;)Lcom/google/speech/tts/Cardinal;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->cardinal_:Lcom/google/speech/tts/Cardinal;

    return-object v0
.end method

.method static synthetic access$3302(Lspeech/patts/Token;Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Cardinal;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->cardinal_:Lcom/google/speech/tts/Cardinal;

    return-object p1
.end method

.method static synthetic access$3402(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasOrdinal:Z

    return p1
.end method

.method static synthetic access$3500(Lspeech/patts/Token;)Lcom/google/speech/tts/Ordinal;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->ordinal_:Lcom/google/speech/tts/Ordinal;

    return-object v0
.end method

.method static synthetic access$3502(Lspeech/patts/Token;Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Ordinal;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->ordinal_:Lcom/google/speech/tts/Ordinal;

    return-object p1
.end method

.method static synthetic access$3602(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasDigit:Z

    return p1
.end method

.method static synthetic access$3702(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->digit_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3802(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasDecimal:Z

    return p1
.end method

.method static synthetic access$3900(Lspeech/patts/Token;)Lcom/google/speech/tts/Decimal;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->decimal_:Lcom/google/speech/tts/Decimal;

    return-object v0
.end method

.method static synthetic access$3902(Lspeech/patts/Token;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->decimal_:Lcom/google/speech/tts/Decimal;

    return-object p1
.end method

.method static synthetic access$4002(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasFraction:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasParent:Z

    return p1
.end method

.method static synthetic access$4100(Lspeech/patts/Token;)Lcom/google/speech/tts/Fraction;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->fraction_:Lcom/google/speech/tts/Fraction;

    return-object v0
.end method

.method static synthetic access$4102(Lspeech/patts/Token;Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Fraction;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->fraction_:Lcom/google/speech/tts/Fraction;

    return-object p1
.end method

.method static synthetic access$4202(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasTime:Z

    return p1
.end method

.method static synthetic access$4300(Lspeech/patts/Token;)Lcom/google/speech/tts/Time;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->time_:Lcom/google/speech/tts/Time;

    return-object v0
.end method

.method static synthetic access$4302(Lspeech/patts/Token;Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Time;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->time_:Lcom/google/speech/tts/Time;

    return-object p1
.end method

.method static synthetic access$4402(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasMeasure:Z

    return p1
.end method

.method static synthetic access$4500(Lspeech/patts/Token;)Lcom/google/speech/tts/Measure;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->measure_:Lcom/google/speech/tts/Measure;

    return-object v0
.end method

.method static synthetic access$4502(Lspeech/patts/Token;Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Measure;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->measure_:Lcom/google/speech/tts/Measure;

    return-object p1
.end method

.method static synthetic access$4602(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasPercent:Z

    return p1
.end method

.method static synthetic access$4700(Lspeech/patts/Token;)Lcom/google/speech/tts/Decimal;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->percent_:Lcom/google/speech/tts/Decimal;

    return-object v0
.end method

.method static synthetic access$4702(Lspeech/patts/Token;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->percent_:Lcom/google/speech/tts/Decimal;

    return-object p1
.end method

.method static synthetic access$4802(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasDate:Z

    return p1
.end method

.method static synthetic access$4900(Lspeech/patts/Token;)Lcom/google/speech/tts/Date;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->date_:Lcom/google/speech/tts/Date;

    return-object v0
.end method

.method static synthetic access$4902(Lspeech/patts/Token;Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Date;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->date_:Lcom/google/speech/tts/Date;

    return-object p1
.end method

.method static synthetic access$5002(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasAddress:Z

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/Token;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Token;->parent_:I

    return p1
.end method

.method static synthetic access$5100(Lspeech/patts/Token;)Lcom/google/speech/tts/Address;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->address_:Lcom/google/speech/tts/Address;

    return-object v0
.end method

.method static synthetic access$5102(Lspeech/patts/Token;Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Address;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->address_:Lcom/google/speech/tts/Address;

    return-object p1
.end method

.method static synthetic access$5202(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasTelephone:Z

    return p1
.end method

.method static synthetic access$5300(Lspeech/patts/Token;)Lcom/google/speech/tts/Telephone;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->telephone_:Lcom/google/speech/tts/Telephone;

    return-object v0
.end method

.method static synthetic access$5302(Lspeech/patts/Token;Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Telephone;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->telephone_:Lcom/google/speech/tts/Telephone;

    return-object p1
.end method

.method static synthetic access$5402(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasMoney:Z

    return p1
.end method

.method static synthetic access$5500(Lspeech/patts/Token;)Lcom/google/speech/tts/Money;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->money_:Lcom/google/speech/tts/Money;

    return-object v0
.end method

.method static synthetic access$5502(Lspeech/patts/Token;Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Money;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->money_:Lcom/google/speech/tts/Money;

    return-object p1
.end method

.method static synthetic access$5602(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasElectronic:Z

    return p1
.end method

.method static synthetic access$5700(Lspeech/patts/Token;)Lcom/google/speech/tts/Electronic;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->electronic_:Lcom/google/speech/tts/Electronic;

    return-object v0
.end method

.method static synthetic access$5702(Lspeech/patts/Token;Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Electronic;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->electronic_:Lcom/google/speech/tts/Electronic;

    return-object p1
.end method

.method static synthetic access$5802(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasVerbatim:Z

    return p1
.end method

.method static synthetic access$5902(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->verbatim_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$6002(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasLetters:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasFirstDaughter:Z

    return p1
.end method

.method static synthetic access$6102(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->letters_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$6202(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasConnector:Z

    return p1
.end method

.method static synthetic access$6300(Lspeech/patts/Token;)Lcom/google/speech/tts/Connector;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->connector_:Lcom/google/speech/tts/Connector;

    return-object v0
.end method

.method static synthetic access$6302(Lspeech/patts/Token;Lcom/google/speech/tts/Connector;)Lcom/google/speech/tts/Connector;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Connector;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->connector_:Lcom/google/speech/tts/Connector;

    return-object p1
.end method

.method static synthetic access$6402(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasConcept:Z

    return p1
.end method

.method static synthetic access$6502(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->concept_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$6602(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasAbbreviation:Z

    return p1
.end method

.method static synthetic access$6700(Lspeech/patts/Token;)Lcom/google/speech/tts/Abbreviation;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->abbreviation_:Lcom/google/speech/tts/Abbreviation;

    return-object v0
.end method

.method static synthetic access$6702(Lspeech/patts/Token;Lcom/google/speech/tts/Abbreviation;)Lcom/google/speech/tts/Abbreviation;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Abbreviation;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->abbreviation_:Lcom/google/speech/tts/Abbreviation;

    return-object p1
.end method

.method static synthetic access$6802(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasDuplicator:Z

    return p1
.end method

.method static synthetic access$6900(Lspeech/patts/Token;)Lcom/google/speech/tts/Duplicator;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->duplicator_:Lcom/google/speech/tts/Duplicator;

    return-object v0
.end method

.method static synthetic access$6902(Lspeech/patts/Token;Lcom/google/speech/tts/Duplicator;)Lcom/google/speech/tts/Duplicator;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/Duplicator;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->duplicator_:Lcom/google/speech/tts/Duplicator;

    return-object p1
.end method

.method static synthetic access$7002(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasSyntax:Z

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/Token;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Token;->firstDaughter_:I

    return p1
.end method

.method static synthetic access$7100(Lspeech/patts/Token;)Lspeech/patts/Syntax;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->syntax_:Lspeech/patts/Syntax;

    return-object v0
.end method

.method static synthetic access$7102(Lspeech/patts/Token;Lspeech/patts/Syntax;)Lspeech/patts/Syntax;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lspeech/patts/Syntax;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->syntax_:Lspeech/patts/Syntax;

    return-object p1
.end method

.method static synthetic access$7202(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasStartIndex:Z

    return p1
.end method

.method static synthetic access$7302(Lspeech/patts/Token;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Token;->startIndex_:I

    return p1
.end method

.method static synthetic access$7402(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasEndIndex:Z

    return p1
.end method

.method static synthetic access$7502(Lspeech/patts/Token;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Token;->endIndex_:I

    return p1
.end method

.method static synthetic access$7602(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasUserFeatures:Z

    return p1
.end method

.method static synthetic access$7702(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->userFeatures_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$7802(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasVoiceMod:Z

    return p1
.end method

.method static synthetic access$7900(Lspeech/patts/Token;)Lcom/google/speech/tts/VoiceMod;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Token;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Token;->voiceMod_:Lcom/google/speech/tts/VoiceMod;

    return-object v0
.end method

.method static synthetic access$7902(Lspeech/patts/Token;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/speech/tts/VoiceMod;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->voiceMod_:Lcom/google/speech/tts/VoiceMod;

    return-object p1
.end method

.method static synthetic access$8002(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasReservedForBackwardsCompatibility1:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasLastDaughter:Z

    return p1
.end method

.method static synthetic access$8102(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->reservedForBackwardsCompatibility1_:Z

    return p1
.end method

.method static synthetic access$8202(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasReservedForBackwardsCompatibility2:Z

    return p1
.end method

.method static synthetic access$8302(Lspeech/patts/Token;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Lcom/google/protobuf/ByteString;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->reservedForBackwardsCompatibility2_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$8402(Lspeech/patts/Token;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Token;->hasSpellingWithStress:Z

    return p1
.end method

.method static synthetic access$8502(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Token;->spellingWithStress_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lspeech/patts/Token;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Token;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/Token;->lastDaughter_:I

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/Token;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/Token;->defaultInstance:Lspeech/patts/Token;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 455
    sget-object v0, Lspeech/patts/Token$Type;->WORD:Lspeech/patts/Token$Type;

    iput-object v0, p0, Lspeech/patts/Token;->type_:Lspeech/patts/Token$Type;

    .line 456
    sget-object v0, Lspeech/patts/Token$PauseLength;->PAUSE_NONE:Lspeech/patts/Token$PauseLength;

    iput-object v0, p0, Lspeech/patts/Token;->pauseLength_:Lspeech/patts/Token$PauseLength;

    .line 457
    invoke-static {}, Lcom/google/speech/tts/Cardinal;->getDefaultInstance()Lcom/google/speech/tts/Cardinal;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->cardinal_:Lcom/google/speech/tts/Cardinal;

    .line 458
    invoke-static {}, Lcom/google/speech/tts/Ordinal;->getDefaultInstance()Lcom/google/speech/tts/Ordinal;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->ordinal_:Lcom/google/speech/tts/Ordinal;

    .line 459
    invoke-static {}, Lcom/google/speech/tts/Decimal;->getDefaultInstance()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->decimal_:Lcom/google/speech/tts/Decimal;

    .line 460
    invoke-static {}, Lcom/google/speech/tts/Fraction;->getDefaultInstance()Lcom/google/speech/tts/Fraction;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->fraction_:Lcom/google/speech/tts/Fraction;

    .line 461
    invoke-static {}, Lcom/google/speech/tts/Time;->getDefaultInstance()Lcom/google/speech/tts/Time;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->time_:Lcom/google/speech/tts/Time;

    .line 462
    invoke-static {}, Lcom/google/speech/tts/Measure;->getDefaultInstance()Lcom/google/speech/tts/Measure;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->measure_:Lcom/google/speech/tts/Measure;

    .line 463
    invoke-static {}, Lcom/google/speech/tts/Decimal;->getDefaultInstance()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->percent_:Lcom/google/speech/tts/Decimal;

    .line 464
    invoke-static {}, Lcom/google/speech/tts/Date;->getDefaultInstance()Lcom/google/speech/tts/Date;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->date_:Lcom/google/speech/tts/Date;

    .line 465
    invoke-static {}, Lcom/google/speech/tts/Address;->getDefaultInstance()Lcom/google/speech/tts/Address;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->address_:Lcom/google/speech/tts/Address;

    .line 466
    invoke-static {}, Lcom/google/speech/tts/Telephone;->getDefaultInstance()Lcom/google/speech/tts/Telephone;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->telephone_:Lcom/google/speech/tts/Telephone;

    .line 467
    invoke-static {}, Lcom/google/speech/tts/Money;->getDefaultInstance()Lcom/google/speech/tts/Money;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->money_:Lcom/google/speech/tts/Money;

    .line 468
    invoke-static {}, Lcom/google/speech/tts/Electronic;->getDefaultInstance()Lcom/google/speech/tts/Electronic;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->electronic_:Lcom/google/speech/tts/Electronic;

    .line 469
    invoke-static {}, Lcom/google/speech/tts/Connector;->getDefaultInstance()Lcom/google/speech/tts/Connector;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->connector_:Lcom/google/speech/tts/Connector;

    .line 470
    invoke-static {}, Lcom/google/speech/tts/Abbreviation;->getDefaultInstance()Lcom/google/speech/tts/Abbreviation;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->abbreviation_:Lcom/google/speech/tts/Abbreviation;

    .line 471
    invoke-static {}, Lcom/google/speech/tts/Duplicator;->getDefaultInstance()Lcom/google/speech/tts/Duplicator;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->duplicator_:Lcom/google/speech/tts/Duplicator;

    .line 472
    invoke-static {}, Lspeech/patts/Syntax;->getDefaultInstance()Lspeech/patts/Syntax;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->syntax_:Lspeech/patts/Syntax;

    .line 473
    invoke-static {}, Lcom/google/speech/tts/VoiceMod;->getDefaultInstance()Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Token;->voiceMod_:Lcom/google/speech/tts/VoiceMod;

    .line 474
    return-void
.end method

.method public static newBuilder()Lspeech/patts/Token$Builder;
    .locals 1

    .prologue
    .line 884
    # invokes: Lspeech/patts/Token$Builder;->create()Lspeech/patts/Token$Builder;
    invoke-static {}, Lspeech/patts/Token$Builder;->access$100()Lspeech/patts/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/Token;)Lspeech/patts/Token$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/Token;

    .prologue
    .line 887
    invoke-static {}, Lspeech/patts/Token;->newBuilder()Lspeech/patts/Token$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/Token$Builder;->mergeFrom(Lspeech/patts/Token;)Lspeech/patts/Token$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAbbreviation()Lcom/google/speech/tts/Abbreviation;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lspeech/patts/Token;->abbreviation_:Lcom/google/speech/tts/Abbreviation;

    return-object v0
.end method

.method public getAddress()Lcom/google/speech/tts/Address;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lspeech/patts/Token;->address_:Lcom/google/speech/tts/Address;

    return-object v0
.end method

.method public getCardinal()Lcom/google/speech/tts/Cardinal;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lspeech/patts/Token;->cardinal_:Lcom/google/speech/tts/Cardinal;

    return-object v0
.end method

.method public getConcept()Ljava/lang/String;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lspeech/patts/Token;->concept_:Ljava/lang/String;

    return-object v0
.end method

.method public getConnector()Lcom/google/speech/tts/Connector;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lspeech/patts/Token;->connector_:Lcom/google/speech/tts/Connector;

    return-object v0
.end method

.method public getDate()Lcom/google/speech/tts/Date;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lspeech/patts/Token;->date_:Lcom/google/speech/tts/Date;

    return-object v0
.end method

.method public getDecimal()Lcom/google/speech/tts/Decimal;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lspeech/patts/Token;->decimal_:Lcom/google/speech/tts/Decimal;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Token;->getDefaultInstanceForType()Lspeech/patts/Token;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/Token;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/Token;->defaultInstance:Lspeech/patts/Token;

    return-object v0
.end method

.method public getDigit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lspeech/patts/Token;->digit_:Ljava/lang/String;

    return-object v0
.end method

.method public getDuplicator()Lcom/google/speech/tts/Duplicator;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lspeech/patts/Token;->duplicator_:Lcom/google/speech/tts/Duplicator;

    return-object v0
.end method

.method public getElectronic()Lcom/google/speech/tts/Electronic;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lspeech/patts/Token;->electronic_:Lcom/google/speech/tts/Electronic;

    return-object v0
.end method

.method public getEndIndex()I
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Lspeech/patts/Token;->endIndex_:I

    return v0
.end method

.method public getFirstDaughter()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lspeech/patts/Token;->firstDaughter_:I

    return v0
.end method

.method public getFraction()Lcom/google/speech/tts/Fraction;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lspeech/patts/Token;->fraction_:Lcom/google/speech/tts/Fraction;

    return-object v0
.end method

.method public getHomographList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 244
    iget-object v0, p0, Lspeech/patts/Token;->homograph_:Ljava/util/List;

    return-object v0
.end method

.method public getLastDaughter()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lspeech/patts/Token;->lastDaughter_:I

    return v0
.end method

.method public getLetters()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lspeech/patts/Token;->letters_:Ljava/lang/String;

    return-object v0
.end method

.method public getMeasure()Lcom/google/speech/tts/Measure;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lspeech/patts/Token;->measure_:Lcom/google/speech/tts/Measure;

    return-object v0
.end method

.method public getMoney()Lcom/google/speech/tts/Money;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lspeech/patts/Token;->money_:Lcom/google/speech/tts/Money;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lspeech/patts/Token;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getNextSpace()Z
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Lspeech/patts/Token;->nextSpace_:Z

    return v0
.end method

.method public getOrdinal()Lcom/google/speech/tts/Ordinal;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lspeech/patts/Token;->ordinal_:Lcom/google/speech/tts/Ordinal;

    return-object v0
.end method

.method public getParent()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lspeech/patts/Token;->parent_:I

    return v0
.end method

.method public getPauseDuration()F
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lspeech/patts/Token;->pauseDuration_:F

    return v0
.end method

.method public getPauseLength()Lspeech/patts/Token$PauseLength;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lspeech/patts/Token;->pauseLength_:Lspeech/patts/Token$PauseLength;

    return-object v0
.end method

.method public getPercent()Lcom/google/speech/tts/Decimal;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lspeech/patts/Token;->percent_:Lcom/google/speech/tts/Decimal;

    return-object v0
.end method

.method public getPhraseBreak()Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lspeech/patts/Token;->phraseBreak_:Z

    return v0
.end method

.method public getReservedForBackwardsCompatibility1()Z
    .locals 1

    .prologue
    .line 438
    iget-boolean v0, p0, Lspeech/patts/Token;->reservedForBackwardsCompatibility1_:Z

    return v0
.end method

.method public getReservedForBackwardsCompatibility2()Lcom/google/protobuf/ByteString;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lspeech/patts/Token;->reservedForBackwardsCompatibility2_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getSemioticClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lspeech/patts/Token;->semioticClass_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 636
    iget v3, p0, Lspeech/patts/Token;->memoizedSerializedSize:I

    .line 637
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 814
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 639
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 640
    invoke-virtual {p0}, Lspeech/patts/Token;->hasParent()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 641
    const/4 v5, 0x1

    invoke-virtual {p0}, Lspeech/patts/Token;->getParent()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 644
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Token;->hasFirstDaughter()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 645
    const/4 v5, 0x2

    invoke-virtual {p0}, Lspeech/patts/Token;->getFirstDaughter()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 648
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Token;->hasLastDaughter()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 649
    const/4 v5, 0x3

    invoke-virtual {p0}, Lspeech/patts/Token;->getLastDaughter()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 652
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Token;->hasName()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 653
    const/4 v5, 0x4

    invoke-virtual {p0}, Lspeech/patts/Token;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 656
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Token;->hasSkip()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 657
    const/4 v5, 0x5

    invoke-virtual {p0}, Lspeech/patts/Token;->getSkip()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 660
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/Token;->hasNextSpace()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 661
    const/4 v5, 0x6

    invoke-virtual {p0}, Lspeech/patts/Token;->getNextSpace()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 664
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/Token;->hasType()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 665
    const/4 v5, 0x7

    invoke-virtual {p0}, Lspeech/patts/Token;->getType()Lspeech/patts/Token$Type;

    move-result-object v6

    invoke-virtual {v6}, Lspeech/patts/Token$Type;->getNumber()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 668
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/Token;->hasWordid()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 669
    const/16 v5, 0x8

    invoke-virtual {p0}, Lspeech/patts/Token;->getWordid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 672
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/Token;->hasPhraseBreak()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 673
    const/16 v5, 0x9

    invoke-virtual {p0}, Lspeech/patts/Token;->getPhraseBreak()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 677
    :cond_9
    const/4 v0, 0x0

    .line 678
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/Token;->getHomographList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 679
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 681
    goto :goto_1

    .line 682
    .end local v1    # "element":Ljava/lang/String;
    :cond_a
    add-int/2addr v3, v0

    .line 683
    invoke-virtual {p0}, Lspeech/patts/Token;->getHomographList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 685
    invoke-virtual {p0}, Lspeech/patts/Token;->hasSpelling()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 686
    const/16 v5, 0xb

    invoke-virtual {p0}, Lspeech/patts/Token;->getSpelling()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 689
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/Token;->hasSemioticClass()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 690
    const/16 v5, 0xc

    invoke-virtual {p0}, Lspeech/patts/Token;->getSemioticClass()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 693
    :cond_c
    invoke-virtual {p0}, Lspeech/patts/Token;->hasCardinal()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 694
    const/16 v5, 0xd

    invoke-virtual {p0}, Lspeech/patts/Token;->getCardinal()Lcom/google/speech/tts/Cardinal;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 697
    :cond_d
    invoke-virtual {p0}, Lspeech/patts/Token;->hasOrdinal()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 698
    const/16 v5, 0xe

    invoke-virtual {p0}, Lspeech/patts/Token;->getOrdinal()Lcom/google/speech/tts/Ordinal;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 701
    :cond_e
    invoke-virtual {p0}, Lspeech/patts/Token;->hasDigit()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 702
    const/16 v5, 0xf

    invoke-virtual {p0}, Lspeech/patts/Token;->getDigit()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 705
    :cond_f
    invoke-virtual {p0}, Lspeech/patts/Token;->hasDecimal()Z

    move-result v5

    if-eqz v5, :cond_10

    .line 706
    const/16 v5, 0x10

    invoke-virtual {p0}, Lspeech/patts/Token;->getDecimal()Lcom/google/speech/tts/Decimal;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 709
    :cond_10
    invoke-virtual {p0}, Lspeech/patts/Token;->hasFraction()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 710
    const/16 v5, 0x11

    invoke-virtual {p0}, Lspeech/patts/Token;->getFraction()Lcom/google/speech/tts/Fraction;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 713
    :cond_11
    invoke-virtual {p0}, Lspeech/patts/Token;->hasTime()Z

    move-result v5

    if-eqz v5, :cond_12

    .line 714
    const/16 v5, 0x12

    invoke-virtual {p0}, Lspeech/patts/Token;->getTime()Lcom/google/speech/tts/Time;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 717
    :cond_12
    invoke-virtual {p0}, Lspeech/patts/Token;->hasMeasure()Z

    move-result v5

    if-eqz v5, :cond_13

    .line 718
    const/16 v5, 0x13

    invoke-virtual {p0}, Lspeech/patts/Token;->getMeasure()Lcom/google/speech/tts/Measure;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 721
    :cond_13
    invoke-virtual {p0}, Lspeech/patts/Token;->hasPercent()Z

    move-result v5

    if-eqz v5, :cond_14

    .line 722
    const/16 v5, 0x14

    invoke-virtual {p0}, Lspeech/patts/Token;->getPercent()Lcom/google/speech/tts/Decimal;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 725
    :cond_14
    invoke-virtual {p0}, Lspeech/patts/Token;->hasDate()Z

    move-result v5

    if-eqz v5, :cond_15

    .line 726
    const/16 v5, 0x15

    invoke-virtual {p0}, Lspeech/patts/Token;->getDate()Lcom/google/speech/tts/Date;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 729
    :cond_15
    invoke-virtual {p0}, Lspeech/patts/Token;->hasAddress()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 730
    const/16 v5, 0x16

    invoke-virtual {p0}, Lspeech/patts/Token;->getAddress()Lcom/google/speech/tts/Address;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 733
    :cond_16
    invoke-virtual {p0}, Lspeech/patts/Token;->hasTelephone()Z

    move-result v5

    if-eqz v5, :cond_17

    .line 734
    const/16 v5, 0x17

    invoke-virtual {p0}, Lspeech/patts/Token;->getTelephone()Lcom/google/speech/tts/Telephone;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 737
    :cond_17
    invoke-virtual {p0}, Lspeech/patts/Token;->hasMoney()Z

    move-result v5

    if-eqz v5, :cond_18

    .line 738
    const/16 v5, 0x18

    invoke-virtual {p0}, Lspeech/patts/Token;->getMoney()Lcom/google/speech/tts/Money;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 741
    :cond_18
    invoke-virtual {p0}, Lspeech/patts/Token;->hasElectronic()Z

    move-result v5

    if-eqz v5, :cond_19

    .line 742
    const/16 v5, 0x19

    invoke-virtual {p0}, Lspeech/patts/Token;->getElectronic()Lcom/google/speech/tts/Electronic;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 745
    :cond_19
    invoke-virtual {p0}, Lspeech/patts/Token;->hasVerbatim()Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 746
    const/16 v5, 0x1a

    invoke-virtual {p0}, Lspeech/patts/Token;->getVerbatim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 749
    :cond_1a
    invoke-virtual {p0}, Lspeech/patts/Token;->hasLetters()Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 750
    const/16 v5, 0x1b

    invoke-virtual {p0}, Lspeech/patts/Token;->getLetters()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 753
    :cond_1b
    invoke-virtual {p0}, Lspeech/patts/Token;->hasConnector()Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 754
    const/16 v5, 0x1c

    invoke-virtual {p0}, Lspeech/patts/Token;->getConnector()Lcom/google/speech/tts/Connector;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 757
    :cond_1c
    invoke-virtual {p0}, Lspeech/patts/Token;->hasConcept()Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 758
    const/16 v5, 0x1d

    invoke-virtual {p0}, Lspeech/patts/Token;->getConcept()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 761
    :cond_1d
    invoke-virtual {p0}, Lspeech/patts/Token;->hasVariant()Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 762
    const/16 v5, 0x1e

    invoke-virtual {p0}, Lspeech/patts/Token;->getVariant()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 765
    :cond_1e
    invoke-virtual {p0}, Lspeech/patts/Token;->hasPauseDuration()Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 766
    const/16 v5, 0x25

    invoke-virtual {p0}, Lspeech/patts/Token;->getPauseDuration()F

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v5

    add-int/2addr v3, v5

    .line 769
    :cond_1f
    invoke-virtual {p0}, Lspeech/patts/Token;->hasPauseLength()Z

    move-result v5

    if-eqz v5, :cond_20

    .line 770
    const/16 v5, 0x26

    invoke-virtual {p0}, Lspeech/patts/Token;->getPauseLength()Lspeech/patts/Token$PauseLength;

    move-result-object v6

    invoke-virtual {v6}, Lspeech/patts/Token$PauseLength;->getNumber()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 773
    :cond_20
    invoke-virtual {p0}, Lspeech/patts/Token;->hasSyntax()Z

    move-result v5

    if-eqz v5, :cond_21

    .line 774
    const/16 v5, 0x27

    invoke-virtual {p0}, Lspeech/patts/Token;->getSyntax()Lspeech/patts/Syntax;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 777
    :cond_21
    invoke-virtual {p0}, Lspeech/patts/Token;->hasStartIndex()Z

    move-result v5

    if-eqz v5, :cond_22

    .line 778
    const/16 v5, 0x28

    invoke-virtual {p0}, Lspeech/patts/Token;->getStartIndex()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 781
    :cond_22
    invoke-virtual {p0}, Lspeech/patts/Token;->hasEndIndex()Z

    move-result v5

    if-eqz v5, :cond_23

    .line 782
    const/16 v5, 0x29

    invoke-virtual {p0}, Lspeech/patts/Token;->getEndIndex()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 785
    :cond_23
    invoke-virtual {p0}, Lspeech/patts/Token;->hasUserFeatures()Z

    move-result v5

    if-eqz v5, :cond_24

    .line 786
    const/16 v5, 0x2a

    invoke-virtual {p0}, Lspeech/patts/Token;->getUserFeatures()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 789
    :cond_24
    invoke-virtual {p0}, Lspeech/patts/Token;->hasVoiceMod()Z

    move-result v5

    if-eqz v5, :cond_25

    .line 790
    const/16 v5, 0x2b

    invoke-virtual {p0}, Lspeech/patts/Token;->getVoiceMod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 793
    :cond_25
    invoke-virtual {p0}, Lspeech/patts/Token;->hasAbbreviation()Z

    move-result v5

    if-eqz v5, :cond_26

    .line 794
    const/16 v5, 0x2c

    invoke-virtual {p0}, Lspeech/patts/Token;->getAbbreviation()Lcom/google/speech/tts/Abbreviation;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 797
    :cond_26
    invoke-virtual {p0}, Lspeech/patts/Token;->hasReservedForBackwardsCompatibility1()Z

    move-result v5

    if-eqz v5, :cond_27

    .line 798
    const/16 v5, 0x2d

    invoke-virtual {p0}, Lspeech/patts/Token;->getReservedForBackwardsCompatibility1()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 801
    :cond_27
    invoke-virtual {p0}, Lspeech/patts/Token;->hasReservedForBackwardsCompatibility2()Z

    move-result v5

    if-eqz v5, :cond_28

    .line 802
    const/16 v5, 0x2e

    invoke-virtual {p0}, Lspeech/patts/Token;->getReservedForBackwardsCompatibility2()Lcom/google/protobuf/ByteString;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v5

    add-int/2addr v3, v5

    .line 805
    :cond_28
    invoke-virtual {p0}, Lspeech/patts/Token;->hasDuplicator()Z

    move-result v5

    if-eqz v5, :cond_29

    .line 806
    const/16 v5, 0x2f

    invoke-virtual {p0}, Lspeech/patts/Token;->getDuplicator()Lcom/google/speech/tts/Duplicator;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 809
    :cond_29
    invoke-virtual {p0}, Lspeech/patts/Token;->hasSpellingWithStress()Z

    move-result v5

    if-eqz v5, :cond_2a

    .line 810
    const/16 v5, 0x30

    invoke-virtual {p0}, Lspeech/patts/Token;->getSpellingWithStress()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 813
    :cond_2a
    iput v3, p0, Lspeech/patts/Token;->memoizedSerializedSize:I

    move v4, v3

    .line 814
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getSkip()Z
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lspeech/patts/Token;->skip_:Z

    return v0
.end method

.method public getSpelling()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lspeech/patts/Token;->spelling_:Ljava/lang/String;

    return-object v0
.end method

.method public getSpellingWithStress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lspeech/patts/Token;->spellingWithStress_:Ljava/lang/String;

    return-object v0
.end method

.method public getStartIndex()I
    .locals 1

    .prologue
    .line 410
    iget v0, p0, Lspeech/patts/Token;->startIndex_:I

    return v0
.end method

.method public getSyntax()Lspeech/patts/Syntax;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lspeech/patts/Token;->syntax_:Lspeech/patts/Syntax;

    return-object v0
.end method

.method public getTelephone()Lcom/google/speech/tts/Telephone;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lspeech/patts/Token;->telephone_:Lcom/google/speech/tts/Telephone;

    return-object v0
.end method

.method public getTime()Lcom/google/speech/tts/Time;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lspeech/patts/Token;->time_:Lcom/google/speech/tts/Time;

    return-object v0
.end method

.method public getType()Lspeech/patts/Token$Type;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lspeech/patts/Token;->type_:Lspeech/patts/Token$Type;

    return-object v0
.end method

.method public getUserFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lspeech/patts/Token;->userFeatures_:Ljava/lang/String;

    return-object v0
.end method

.method public getVariant()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lspeech/patts/Token;->variant_:Ljava/lang/String;

    return-object v0
.end method

.method public getVerbatim()Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lspeech/patts/Token;->verbatim_:Ljava/lang/String;

    return-object v0
.end method

.method public getVoiceMod()Lcom/google/speech/tts/VoiceMod;
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lspeech/patts/Token;->voiceMod_:Lcom/google/speech/tts/VoiceMod;

    return-object v0
.end method

.method public getWordid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lspeech/patts/Token;->wordid_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAbbreviation()Z
    .locals 1

    .prologue
    .line 388
    iget-boolean v0, p0, Lspeech/patts/Token;->hasAbbreviation:Z

    return v0
.end method

.method public hasAddress()Z
    .locals 1

    .prologue
    .line 332
    iget-boolean v0, p0, Lspeech/patts/Token;->hasAddress:Z

    return v0
.end method

.method public hasCardinal()Z
    .locals 1

    .prologue
    .line 269
    iget-boolean v0, p0, Lspeech/patts/Token;->hasCardinal:Z

    return v0
.end method

.method public hasConcept()Z
    .locals 1

    .prologue
    .line 381
    iget-boolean v0, p0, Lspeech/patts/Token;->hasConcept:Z

    return v0
.end method

.method public hasConnector()Z
    .locals 1

    .prologue
    .line 374
    iget-boolean v0, p0, Lspeech/patts/Token;->hasConnector:Z

    return v0
.end method

.method public hasDate()Z
    .locals 1

    .prologue
    .line 325
    iget-boolean v0, p0, Lspeech/patts/Token;->hasDate:Z

    return v0
.end method

.method public hasDecimal()Z
    .locals 1

    .prologue
    .line 290
    iget-boolean v0, p0, Lspeech/patts/Token;->hasDecimal:Z

    return v0
.end method

.method public hasDigit()Z
    .locals 1

    .prologue
    .line 283
    iget-boolean v0, p0, Lspeech/patts/Token;->hasDigit:Z

    return v0
.end method

.method public hasDuplicator()Z
    .locals 1

    .prologue
    .line 395
    iget-boolean v0, p0, Lspeech/patts/Token;->hasDuplicator:Z

    return v0
.end method

.method public hasElectronic()Z
    .locals 1

    .prologue
    .line 353
    iget-boolean v0, p0, Lspeech/patts/Token;->hasElectronic:Z

    return v0
.end method

.method public hasEndIndex()Z
    .locals 1

    .prologue
    .line 416
    iget-boolean v0, p0, Lspeech/patts/Token;->hasEndIndex:Z

    return v0
.end method

.method public hasFirstDaughter()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lspeech/patts/Token;->hasFirstDaughter:Z

    return v0
.end method

.method public hasFraction()Z
    .locals 1

    .prologue
    .line 297
    iget-boolean v0, p0, Lspeech/patts/Token;->hasFraction:Z

    return v0
.end method

.method public hasLastDaughter()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lspeech/patts/Token;->hasLastDaughter:Z

    return v0
.end method

.method public hasLetters()Z
    .locals 1

    .prologue
    .line 367
    iget-boolean v0, p0, Lspeech/patts/Token;->hasLetters:Z

    return v0
.end method

.method public hasMeasure()Z
    .locals 1

    .prologue
    .line 311
    iget-boolean v0, p0, Lspeech/patts/Token;->hasMeasure:Z

    return v0
.end method

.method public hasMoney()Z
    .locals 1

    .prologue
    .line 346
    iget-boolean v0, p0, Lspeech/patts/Token;->hasMoney:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lspeech/patts/Token;->hasName:Z

    return v0
.end method

.method public hasNextSpace()Z
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lspeech/patts/Token;->hasNextSpace:Z

    return v0
.end method

.method public hasOrdinal()Z
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Lspeech/patts/Token;->hasOrdinal:Z

    return v0
.end method

.method public hasParent()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lspeech/patts/Token;->hasParent:Z

    return v0
.end method

.method public hasPauseDuration()Z
    .locals 1

    .prologue
    .line 229
    iget-boolean v0, p0, Lspeech/patts/Token;->hasPauseDuration:Z

    return v0
.end method

.method public hasPauseLength()Z
    .locals 1

    .prologue
    .line 236
    iget-boolean v0, p0, Lspeech/patts/Token;->hasPauseLength:Z

    return v0
.end method

.method public hasPercent()Z
    .locals 1

    .prologue
    .line 318
    iget-boolean v0, p0, Lspeech/patts/Token;->hasPercent:Z

    return v0
.end method

.method public hasPhraseBreak()Z
    .locals 1

    .prologue
    .line 222
    iget-boolean v0, p0, Lspeech/patts/Token;->hasPhraseBreak:Z

    return v0
.end method

.method public hasReservedForBackwardsCompatibility1()Z
    .locals 1

    .prologue
    .line 437
    iget-boolean v0, p0, Lspeech/patts/Token;->hasReservedForBackwardsCompatibility1:Z

    return v0
.end method

.method public hasReservedForBackwardsCompatibility2()Z
    .locals 1

    .prologue
    .line 444
    iget-boolean v0, p0, Lspeech/patts/Token;->hasReservedForBackwardsCompatibility2:Z

    return v0
.end method

.method public hasSemioticClass()Z
    .locals 1

    .prologue
    .line 262
    iget-boolean v0, p0, Lspeech/patts/Token;->hasSemioticClass:Z

    return v0
.end method

.method public hasSkip()Z
    .locals 1

    .prologue
    .line 187
    iget-boolean v0, p0, Lspeech/patts/Token;->hasSkip:Z

    return v0
.end method

.method public hasSpelling()Z
    .locals 1

    .prologue
    .line 255
    iget-boolean v0, p0, Lspeech/patts/Token;->hasSpelling:Z

    return v0
.end method

.method public hasSpellingWithStress()Z
    .locals 1

    .prologue
    .line 451
    iget-boolean v0, p0, Lspeech/patts/Token;->hasSpellingWithStress:Z

    return v0
.end method

.method public hasStartIndex()Z
    .locals 1

    .prologue
    .line 409
    iget-boolean v0, p0, Lspeech/patts/Token;->hasStartIndex:Z

    return v0
.end method

.method public hasSyntax()Z
    .locals 1

    .prologue
    .line 402
    iget-boolean v0, p0, Lspeech/patts/Token;->hasSyntax:Z

    return v0
.end method

.method public hasTelephone()Z
    .locals 1

    .prologue
    .line 339
    iget-boolean v0, p0, Lspeech/patts/Token;->hasTelephone:Z

    return v0
.end method

.method public hasTime()Z
    .locals 1

    .prologue
    .line 304
    iget-boolean v0, p0, Lspeech/patts/Token;->hasTime:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    .prologue
    .line 201
    iget-boolean v0, p0, Lspeech/patts/Token;->hasType:Z

    return v0
.end method

.method public hasUserFeatures()Z
    .locals 1

    .prologue
    .line 423
    iget-boolean v0, p0, Lspeech/patts/Token;->hasUserFeatures:Z

    return v0
.end method

.method public hasVariant()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lspeech/patts/Token;->hasVariant:Z

    return v0
.end method

.method public hasVerbatim()Z
    .locals 1

    .prologue
    .line 360
    iget-boolean v0, p0, Lspeech/patts/Token;->hasVerbatim:Z

    return v0
.end method

.method public hasVoiceMod()Z
    .locals 1

    .prologue
    .line 430
    iget-boolean v0, p0, Lspeech/patts/Token;->hasVoiceMod:Z

    return v0
.end method

.method public hasWordid()Z
    .locals 1

    .prologue
    .line 208
    iget-boolean v0, p0, Lspeech/patts/Token;->hasWordid:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 476
    invoke-virtual {p0}, Lspeech/patts/Token;->hasCardinal()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 477
    invoke-virtual {p0}, Lspeech/patts/Token;->getCardinal()Lcom/google/speech/tts/Cardinal;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Cardinal;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 500
    :cond_0
    :goto_0
    return v0

    .line 479
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Token;->hasOrdinal()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 480
    invoke-virtual {p0}, Lspeech/patts/Token;->getOrdinal()Lcom/google/speech/tts/Ordinal;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Ordinal;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 482
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Token;->hasFraction()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 483
    invoke-virtual {p0}, Lspeech/patts/Token;->getFraction()Lcom/google/speech/tts/Fraction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Fraction;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 485
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Token;->hasMeasure()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 486
    invoke-virtual {p0}, Lspeech/patts/Token;->getMeasure()Lcom/google/speech/tts/Measure;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Measure;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 488
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Token;->hasMoney()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 489
    invoke-virtual {p0}, Lspeech/patts/Token;->getMoney()Lcom/google/speech/tts/Money;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Money;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 491
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/Token;->hasAbbreviation()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 492
    invoke-virtual {p0}, Lspeech/patts/Token;->getAbbreviation()Lcom/google/speech/tts/Abbreviation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Abbreviation;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 494
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/Token;->hasDuplicator()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 495
    invoke-virtual {p0}, Lspeech/patts/Token;->getDuplicator()Lcom/google/speech/tts/Duplicator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Duplicator;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 497
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/Token;->hasVoiceMod()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 498
    invoke-virtual {p0}, Lspeech/patts/Token;->getVoiceMod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/VoiceMod;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 500
    :cond_8
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Token;->toBuilder()Lspeech/patts/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/Token$Builder;
    .locals 1

    .prologue
    .line 889
    invoke-static {p0}, Lspeech/patts/Token;->newBuilder(Lspeech/patts/Token;)Lspeech/patts/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 505
    invoke-virtual {p0}, Lspeech/patts/Token;->getSerializedSize()I

    .line 506
    invoke-virtual {p0}, Lspeech/patts/Token;->hasParent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 507
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/Token;->getParent()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 509
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Token;->hasFirstDaughter()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 510
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/Token;->getFirstDaughter()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 512
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Token;->hasLastDaughter()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 513
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/Token;->getLastDaughter()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 515
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Token;->hasName()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 516
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/Token;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 518
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Token;->hasSkip()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 519
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/Token;->getSkip()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 521
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Token;->hasNextSpace()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 522
    const/4 v2, 0x6

    invoke-virtual {p0}, Lspeech/patts/Token;->getNextSpace()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 524
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/Token;->hasType()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 525
    const/4 v2, 0x7

    invoke-virtual {p0}, Lspeech/patts/Token;->getType()Lspeech/patts/Token$Type;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/Token$Type;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 527
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/Token;->hasWordid()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 528
    const/16 v2, 0x8

    invoke-virtual {p0}, Lspeech/patts/Token;->getWordid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 530
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/Token;->hasPhraseBreak()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 531
    const/16 v2, 0x9

    invoke-virtual {p0}, Lspeech/patts/Token;->getPhraseBreak()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 533
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/Token;->getHomographList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 534
    .local v0, "element":Ljava/lang/String;
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 536
    .end local v0    # "element":Ljava/lang/String;
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/Token;->hasSpelling()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 537
    const/16 v2, 0xb

    invoke-virtual {p0}, Lspeech/patts/Token;->getSpelling()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 539
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/Token;->hasSemioticClass()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 540
    const/16 v2, 0xc

    invoke-virtual {p0}, Lspeech/patts/Token;->getSemioticClass()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 542
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/Token;->hasCardinal()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 543
    const/16 v2, 0xd

    invoke-virtual {p0}, Lspeech/patts/Token;->getCardinal()Lcom/google/speech/tts/Cardinal;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 545
    :cond_c
    invoke-virtual {p0}, Lspeech/patts/Token;->hasOrdinal()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 546
    const/16 v2, 0xe

    invoke-virtual {p0}, Lspeech/patts/Token;->getOrdinal()Lcom/google/speech/tts/Ordinal;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 548
    :cond_d
    invoke-virtual {p0}, Lspeech/patts/Token;->hasDigit()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 549
    const/16 v2, 0xf

    invoke-virtual {p0}, Lspeech/patts/Token;->getDigit()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 551
    :cond_e
    invoke-virtual {p0}, Lspeech/patts/Token;->hasDecimal()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 552
    const/16 v2, 0x10

    invoke-virtual {p0}, Lspeech/patts/Token;->getDecimal()Lcom/google/speech/tts/Decimal;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 554
    :cond_f
    invoke-virtual {p0}, Lspeech/patts/Token;->hasFraction()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 555
    const/16 v2, 0x11

    invoke-virtual {p0}, Lspeech/patts/Token;->getFraction()Lcom/google/speech/tts/Fraction;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 557
    :cond_10
    invoke-virtual {p0}, Lspeech/patts/Token;->hasTime()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 558
    const/16 v2, 0x12

    invoke-virtual {p0}, Lspeech/patts/Token;->getTime()Lcom/google/speech/tts/Time;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 560
    :cond_11
    invoke-virtual {p0}, Lspeech/patts/Token;->hasMeasure()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 561
    const/16 v2, 0x13

    invoke-virtual {p0}, Lspeech/patts/Token;->getMeasure()Lcom/google/speech/tts/Measure;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 563
    :cond_12
    invoke-virtual {p0}, Lspeech/patts/Token;->hasPercent()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 564
    const/16 v2, 0x14

    invoke-virtual {p0}, Lspeech/patts/Token;->getPercent()Lcom/google/speech/tts/Decimal;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 566
    :cond_13
    invoke-virtual {p0}, Lspeech/patts/Token;->hasDate()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 567
    const/16 v2, 0x15

    invoke-virtual {p0}, Lspeech/patts/Token;->getDate()Lcom/google/speech/tts/Date;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 569
    :cond_14
    invoke-virtual {p0}, Lspeech/patts/Token;->hasAddress()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 570
    const/16 v2, 0x16

    invoke-virtual {p0}, Lspeech/patts/Token;->getAddress()Lcom/google/speech/tts/Address;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 572
    :cond_15
    invoke-virtual {p0}, Lspeech/patts/Token;->hasTelephone()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 573
    const/16 v2, 0x17

    invoke-virtual {p0}, Lspeech/patts/Token;->getTelephone()Lcom/google/speech/tts/Telephone;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 575
    :cond_16
    invoke-virtual {p0}, Lspeech/patts/Token;->hasMoney()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 576
    const/16 v2, 0x18

    invoke-virtual {p0}, Lspeech/patts/Token;->getMoney()Lcom/google/speech/tts/Money;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 578
    :cond_17
    invoke-virtual {p0}, Lspeech/patts/Token;->hasElectronic()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 579
    const/16 v2, 0x19

    invoke-virtual {p0}, Lspeech/patts/Token;->getElectronic()Lcom/google/speech/tts/Electronic;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 581
    :cond_18
    invoke-virtual {p0}, Lspeech/patts/Token;->hasVerbatim()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 582
    const/16 v2, 0x1a

    invoke-virtual {p0}, Lspeech/patts/Token;->getVerbatim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 584
    :cond_19
    invoke-virtual {p0}, Lspeech/patts/Token;->hasLetters()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 585
    const/16 v2, 0x1b

    invoke-virtual {p0}, Lspeech/patts/Token;->getLetters()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 587
    :cond_1a
    invoke-virtual {p0}, Lspeech/patts/Token;->hasConnector()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 588
    const/16 v2, 0x1c

    invoke-virtual {p0}, Lspeech/patts/Token;->getConnector()Lcom/google/speech/tts/Connector;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 590
    :cond_1b
    invoke-virtual {p0}, Lspeech/patts/Token;->hasConcept()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 591
    const/16 v2, 0x1d

    invoke-virtual {p0}, Lspeech/patts/Token;->getConcept()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 593
    :cond_1c
    invoke-virtual {p0}, Lspeech/patts/Token;->hasVariant()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 594
    const/16 v2, 0x1e

    invoke-virtual {p0}, Lspeech/patts/Token;->getVariant()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 596
    :cond_1d
    invoke-virtual {p0}, Lspeech/patts/Token;->hasPauseDuration()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 597
    const/16 v2, 0x25

    invoke-virtual {p0}, Lspeech/patts/Token;->getPauseDuration()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 599
    :cond_1e
    invoke-virtual {p0}, Lspeech/patts/Token;->hasPauseLength()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 600
    const/16 v2, 0x26

    invoke-virtual {p0}, Lspeech/patts/Token;->getPauseLength()Lspeech/patts/Token$PauseLength;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/Token$PauseLength;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 602
    :cond_1f
    invoke-virtual {p0}, Lspeech/patts/Token;->hasSyntax()Z

    move-result v2

    if-eqz v2, :cond_20

    .line 603
    const/16 v2, 0x27

    invoke-virtual {p0}, Lspeech/patts/Token;->getSyntax()Lspeech/patts/Syntax;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 605
    :cond_20
    invoke-virtual {p0}, Lspeech/patts/Token;->hasStartIndex()Z

    move-result v2

    if-eqz v2, :cond_21

    .line 606
    const/16 v2, 0x28

    invoke-virtual {p0}, Lspeech/patts/Token;->getStartIndex()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 608
    :cond_21
    invoke-virtual {p0}, Lspeech/patts/Token;->hasEndIndex()Z

    move-result v2

    if-eqz v2, :cond_22

    .line 609
    const/16 v2, 0x29

    invoke-virtual {p0}, Lspeech/patts/Token;->getEndIndex()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 611
    :cond_22
    invoke-virtual {p0}, Lspeech/patts/Token;->hasUserFeatures()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 612
    const/16 v2, 0x2a

    invoke-virtual {p0}, Lspeech/patts/Token;->getUserFeatures()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 614
    :cond_23
    invoke-virtual {p0}, Lspeech/patts/Token;->hasVoiceMod()Z

    move-result v2

    if-eqz v2, :cond_24

    .line 615
    const/16 v2, 0x2b

    invoke-virtual {p0}, Lspeech/patts/Token;->getVoiceMod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 617
    :cond_24
    invoke-virtual {p0}, Lspeech/patts/Token;->hasAbbreviation()Z

    move-result v2

    if-eqz v2, :cond_25

    .line 618
    const/16 v2, 0x2c

    invoke-virtual {p0}, Lspeech/patts/Token;->getAbbreviation()Lcom/google/speech/tts/Abbreviation;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 620
    :cond_25
    invoke-virtual {p0}, Lspeech/patts/Token;->hasReservedForBackwardsCompatibility1()Z

    move-result v2

    if-eqz v2, :cond_26

    .line 621
    const/16 v2, 0x2d

    invoke-virtual {p0}, Lspeech/patts/Token;->getReservedForBackwardsCompatibility1()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 623
    :cond_26
    invoke-virtual {p0}, Lspeech/patts/Token;->hasReservedForBackwardsCompatibility2()Z

    move-result v2

    if-eqz v2, :cond_27

    .line 624
    const/16 v2, 0x2e

    invoke-virtual {p0}, Lspeech/patts/Token;->getReservedForBackwardsCompatibility2()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 626
    :cond_27
    invoke-virtual {p0}, Lspeech/patts/Token;->hasDuplicator()Z

    move-result v2

    if-eqz v2, :cond_28

    .line 627
    const/16 v2, 0x2f

    invoke-virtual {p0}, Lspeech/patts/Token;->getDuplicator()Lcom/google/speech/tts/Duplicator;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 629
    :cond_28
    invoke-virtual {p0}, Lspeech/patts/Token;->hasSpellingWithStress()Z

    move-result v2

    if-eqz v2, :cond_29

    .line 630
    const/16 v2, 0x30

    invoke-virtual {p0}, Lspeech/patts/Token;->getSpellingWithStress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 632
    :cond_29
    return-void
.end method
