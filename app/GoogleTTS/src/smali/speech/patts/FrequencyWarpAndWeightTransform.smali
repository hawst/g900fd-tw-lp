.class public final Lspeech/patts/FrequencyWarpAndWeightTransform;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FrequencyWarpAndWeightTransform.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/FrequencyWarpAndWeightTransform$1;,
        Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/FrequencyWarpAndWeightTransform;


# instance fields
.field private frequencyWeightingMemoizedSerializedSize:I

.field private frequencyWeighting_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private hasLabel:Z

.field private hasLinearFactor:Z

.field private label_:Ljava/lang/String;

.field private linearFactor_:D

.field private memoizedSerializedSize:I

.field private sourceFrequencyMemoizedSerializedSize:I

.field private sourceFrequency_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private sourceMcepClassMeanMemoizedSerializedSize:I

.field private sourceMcepClassMean_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private targetFrequencyMemoizedSerializedSize:I

.field private targetFrequency_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 643
    new-instance v0, Lspeech/patts/FrequencyWarpAndWeightTransform;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransform;-><init>(Z)V

    sput-object v0, Lspeech/patts/FrequencyWarpAndWeightTransform;->defaultInstance:Lspeech/patts/FrequencyWarpAndWeightTransform;

    .line 644
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 645
    sget-object v0, Lspeech/patts/FrequencyWarpAndWeightTransform;->defaultInstance:Lspeech/patts/FrequencyWarpAndWeightTransform;

    invoke-direct {v0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->initFields()V

    .line 646
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->label_:Ljava/lang/String;

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->linearFactor_:D

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;

    .line 47
    iput v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequencyMemoizedSerializedSize:I

    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;

    .line 60
    iput v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequencyMemoizedSerializedSize:I

    .line 64
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;

    .line 73
    iput v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeightingMemoizedSerializedSize:I

    .line 77
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;

    .line 86
    iput v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMeanMemoizedSerializedSize:I

    .line 133
    iput v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/FrequencyWarpAndWeightTransform$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3
    .param p1, "noInit"    # Z

    .prologue
    const/4 v2, -0x1

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->label_:Ljava/lang/String;

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->linearFactor_:D

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;

    .line 47
    iput v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequencyMemoizedSerializedSize:I

    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;

    .line 60
    iput v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequencyMemoizedSerializedSize:I

    .line 64
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;

    .line 73
    iput v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeightingMemoizedSerializedSize:I

    .line 77
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;

    .line 86
    iput v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMeanMemoizedSerializedSize:I

    .line 133
    iput v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/FrequencyWarpAndWeightTransform;D)D
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform;
    .param p1, "x1"    # D

    .prologue
    .line 5
    iput-wide p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->linearFactor_:D

    return-wide p1
.end method

.method static synthetic access$300(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$702(Lspeech/patts/FrequencyWarpAndWeightTransform;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->hasLabel:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->label_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lspeech/patts/FrequencyWarpAndWeightTransform;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrequencyWarpAndWeightTransform;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->hasLinearFactor:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/FrequencyWarpAndWeightTransform;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/FrequencyWarpAndWeightTransform;->defaultInstance:Lspeech/patts/FrequencyWarpAndWeightTransform;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method public static newBuilder()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
    .locals 1

    .prologue
    .line 262
    # invokes: Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->create()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->access$100()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/FrequencyWarpAndWeightTransform;

    .prologue
    .line 265
    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransform;->newBuilder()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->mergeFrom(Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getDefaultInstanceForType()Lspeech/patts/FrequencyWarpAndWeightTransform;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/FrequencyWarpAndWeightTransform;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/FrequencyWarpAndWeightTransform;->defaultInstance:Lspeech/patts/FrequencyWarpAndWeightTransform;

    return-object v0
.end method

.method public getFrequencyWeightingList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->label_:Ljava/lang/String;

    return-object v0
.end method

.method public getLinearFactor()D
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->linearFactor_:D

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 135
    iget v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->memoizedSerializedSize:I

    .line 136
    .local v1, "size":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 192
    .end local v1    # "size":I
    .local v2, "size":I
    :goto_0
    return v2

    .line 138
    .end local v2    # "size":I
    .restart local v1    # "size":I
    :cond_0
    const/4 v1, 0x0

    .line 139
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->hasLabel()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 140
    const/4 v3, 0x1

    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getLabel()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 143
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->hasLinearFactor()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 144
    const/4 v3, 0x2

    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getLinearFactor()D

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeDoubleSize(ID)I

    move-result v3

    add-int/2addr v1, v3

    .line 148
    :cond_2
    const/4 v0, 0x0

    .line 149
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getSourceFrequencyList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x8

    .line 150
    add-int/2addr v1, v0

    .line 151
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getSourceFrequencyList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 152
    add-int/lit8 v1, v1, 0x1

    .line 153
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 156
    :cond_3
    iput v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequencyMemoizedSerializedSize:I

    .line 159
    const/4 v0, 0x0

    .line 160
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getTargetFrequencyList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x8

    .line 161
    add-int/2addr v1, v0

    .line 162
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getTargetFrequencyList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 163
    add-int/lit8 v1, v1, 0x1

    .line 164
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 167
    :cond_4
    iput v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequencyMemoizedSerializedSize:I

    .line 170
    const/4 v0, 0x0

    .line 171
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getFrequencyWeightingList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x8

    .line 172
    add-int/2addr v1, v0

    .line 173
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getFrequencyWeightingList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 174
    add-int/lit8 v1, v1, 0x1

    .line 175
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 178
    :cond_5
    iput v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeightingMemoizedSerializedSize:I

    .line 181
    const/4 v0, 0x0

    .line 182
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getSourceMcepClassMeanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x8

    .line 183
    add-int/2addr v1, v0

    .line 184
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getSourceMcepClassMeanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 185
    add-int/lit8 v1, v1, 0x1

    .line 186
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 189
    :cond_6
    iput v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMeanMemoizedSerializedSize:I

    .line 191
    iput v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->memoizedSerializedSize:I

    move v2, v1

    .line 192
    .end local v1    # "size":I
    .restart local v2    # "size":I
    goto/16 :goto_0
.end method

.method public getSourceFrequencyList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;

    return-object v0
.end method

.method public getSourceMcepClassMeanList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;

    return-object v0
.end method

.method public getTargetFrequencyList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;

    return-object v0
.end method

.method public hasLabel()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->hasLabel:Z

    return v0
.end method

.method public hasLinearFactor()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->hasLinearFactor:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->toBuilder()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
    .locals 1

    .prologue
    .line 267
    invoke-static {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->newBuilder(Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getSerializedSize()I

    .line 97
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->hasLabel()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 98
    const/4 v3, 0x1

    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getLabel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 100
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->hasLinearFactor()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 101
    const/4 v3, 0x2

    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getLinearFactor()D

    move-result-wide v4

    invoke-virtual {p1, v3, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->writeDouble(ID)V

    .line 103
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getSourceFrequencyList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 104
    const/16 v3, 0x1a

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 105
    iget v3, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequencyMemoizedSerializedSize:I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 107
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getSourceFrequencyList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 108
    .local v0, "element":D
    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeDoubleNoTag(D)V

    goto :goto_0

    .line 110
    .end local v0    # "element":D
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getTargetFrequencyList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 111
    const/16 v3, 0x22

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 112
    iget v3, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequencyMemoizedSerializedSize:I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 114
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getTargetFrequencyList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 115
    .restart local v0    # "element":D
    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeDoubleNoTag(D)V

    goto :goto_1

    .line 117
    .end local v0    # "element":D
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getFrequencyWeightingList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 118
    const/16 v3, 0x2a

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 119
    iget v3, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeightingMemoizedSerializedSize:I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 121
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getFrequencyWeightingList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 122
    .restart local v0    # "element":D
    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeDoubleNoTag(D)V

    goto :goto_2

    .line 124
    .end local v0    # "element":D
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getSourceMcepClassMeanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_8

    .line 125
    const/16 v3, 0x32

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 126
    iget v3, p0, Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMeanMemoizedSerializedSize:I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 128
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getSourceMcepClassMeanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 129
    .restart local v0    # "element":D
    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeDoubleNoTag(D)V

    goto :goto_3

    .line 131
    .end local v0    # "element":D
    :cond_9
    return-void
.end method
