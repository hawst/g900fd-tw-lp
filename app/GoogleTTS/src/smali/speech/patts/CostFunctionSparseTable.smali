.class public final Lspeech/patts/CostFunctionSparseTable;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "CostFunctionSparseTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/CostFunctionSparseTable$1;,
        Lspeech/patts/CostFunctionSparseTable$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/CostFunctionSparseTable;


# instance fields
.field private defaultWeight_:F

.field private entry_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/SparseTableEntry;",
            ">;"
        }
    .end annotation
.end field

.field private hasDefaultWeight:Z

.field private hasName:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 382
    new-instance v0, Lspeech/patts/CostFunctionSparseTable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/CostFunctionSparseTable;-><init>(Z)V

    sput-object v0, Lspeech/patts/CostFunctionSparseTable;->defaultInstance:Lspeech/patts/CostFunctionSparseTable;

    .line 383
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 384
    sget-object v0, Lspeech/patts/CostFunctionSparseTable;->defaultInstance:Lspeech/patts/CostFunctionSparseTable;

    invoke-direct {v0}, Lspeech/patts/CostFunctionSparseTable;->initFields()V

    .line 385
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/CostFunctionSparseTable;->name_:Ljava/lang/String;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/CostFunctionSparseTable;->defaultWeight_:F

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/CostFunctionSparseTable;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/CostFunctionSparseTable;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/CostFunctionSparseTable$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/CostFunctionSparseTable$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/CostFunctionSparseTable;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/CostFunctionSparseTable;->name_:Ljava/lang/String;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/CostFunctionSparseTable;->defaultWeight_:F

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/CostFunctionSparseTable;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/CostFunctionSparseTable;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunctionSparseTable;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/CostFunctionSparseTable;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctionSparseTable;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/CostFunctionSparseTable;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctionSparseTable;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/CostFunctionSparseTable;->hasName:Z

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/CostFunctionSparseTable;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctionSparseTable;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctionSparseTable;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lspeech/patts/CostFunctionSparseTable;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctionSparseTable;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/CostFunctionSparseTable;->hasDefaultWeight:Z

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/CostFunctionSparseTable;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctionSparseTable;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/CostFunctionSparseTable;->defaultWeight_:F

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/CostFunctionSparseTable;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/CostFunctionSparseTable;->defaultInstance:Lspeech/patts/CostFunctionSparseTable;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public static newBuilder()Lspeech/patts/CostFunctionSparseTable$Builder;
    .locals 1

    .prologue
    .line 162
    # invokes: Lspeech/patts/CostFunctionSparseTable$Builder;->create()Lspeech/patts/CostFunctionSparseTable$Builder;
    invoke-static {}, Lspeech/patts/CostFunctionSparseTable$Builder;->access$100()Lspeech/patts/CostFunctionSparseTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/CostFunctionSparseTable;)Lspeech/patts/CostFunctionSparseTable$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/CostFunctionSparseTable;

    .prologue
    .line 165
    invoke-static {}, Lspeech/patts/CostFunctionSparseTable;->newBuilder()Lspeech/patts/CostFunctionSparseTable$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/CostFunctionSparseTable$Builder;->mergeFrom(Lspeech/patts/CostFunctionSparseTable;)Lspeech/patts/CostFunctionSparseTable$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->getDefaultInstanceForType()Lspeech/patts/CostFunctionSparseTable;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/CostFunctionSparseTable;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/CostFunctionSparseTable;->defaultInstance:Lspeech/patts/CostFunctionSparseTable;

    return-object v0
.end method

.method public getDefaultWeight()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lspeech/patts/CostFunctionSparseTable;->defaultWeight_:F

    return v0
.end method

.method public getEntryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/SparseTableEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable;->entry_:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/CostFunctionSparseTable;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 75
    iget v2, p0, Lspeech/patts/CostFunctionSparseTable;->memoizedSerializedSize:I

    .line 76
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 92
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 78
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 79
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->hasName()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 80
    const/4 v4, 0x1

    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 83
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->hasDefaultWeight()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 84
    const/4 v4, 0x2

    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->getDefaultWeight()F

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v4

    add-int/2addr v2, v4

    .line 87
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->getEntryList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/SparseTableEntry;

    .line 88
    .local v0, "element":Lspeech/patts/SparseTableEntry;
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 90
    goto :goto_1

    .line 91
    .end local v0    # "element":Lspeech/patts/SparseTableEntry;
    :cond_3
    iput v2, p0, Lspeech/patts/CostFunctionSparseTable;->memoizedSerializedSize:I

    move v3, v2

    .line 92
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public hasDefaultWeight()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/CostFunctionSparseTable;->hasDefaultWeight:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/CostFunctionSparseTable;->hasName:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 51
    iget-boolean v3, p0, Lspeech/patts/CostFunctionSparseTable;->hasName:Z

    if-nez v3, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v2

    .line 52
    :cond_1
    iget-boolean v3, p0, Lspeech/patts/CostFunctionSparseTable;->hasDefaultWeight:Z

    if-eqz v3, :cond_0

    .line 53
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->getEntryList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/SparseTableEntry;

    .line 54
    .local v0, "element":Lspeech/patts/SparseTableEntry;
    invoke-virtual {v0}, Lspeech/patts/SparseTableEntry;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 56
    .end local v0    # "element":Lspeech/patts/SparseTableEntry;
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->toBuilder()Lspeech/patts/CostFunctionSparseTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/CostFunctionSparseTable$Builder;
    .locals 1

    .prologue
    .line 167
    invoke-static {p0}, Lspeech/patts/CostFunctionSparseTable;->newBuilder(Lspeech/patts/CostFunctionSparseTable;)Lspeech/patts/CostFunctionSparseTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->getSerializedSize()I

    .line 62
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->hasName()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 65
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->hasDefaultWeight()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 66
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->getDefaultWeight()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 68
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSparseTable;->getEntryList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/SparseTableEntry;

    .line 69
    .local v0, "element":Lspeech/patts/SparseTableEntry;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 71
    .end local v0    # "element":Lspeech/patts/SparseTableEntry;
    :cond_2
    return-void
.end method
