.class public final Lspeech/patts/UtteranceStats$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "UtteranceStats.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/UtteranceStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/UtteranceStats;",
        "Lspeech/patts/UtteranceStats$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/UtteranceStats;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/UtteranceStats$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-static {}, Lspeech/patts/UtteranceStats$Builder;->create()Lspeech/patts/UtteranceStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/UtteranceStats$Builder;
    .locals 3

    .prologue
    .line 168
    new-instance v0, Lspeech/patts/UtteranceStats$Builder;

    invoke-direct {v0}, Lspeech/patts/UtteranceStats$Builder;-><init>()V

    .line 169
    .local v0, "builder":Lspeech/patts/UtteranceStats$Builder;
    new-instance v1, Lspeech/patts/UtteranceStats;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/UtteranceStats;-><init>(Lspeech/patts/UtteranceStats$1;)V

    iput-object v1, v0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    .line 170
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats$Builder;->build()Lspeech/patts/UtteranceStats;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/UtteranceStats;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/UtteranceStats$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    invoke-static {v0}, Lspeech/patts/UtteranceStats$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 201
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats$Builder;->buildPartial()Lspeech/patts/UtteranceStats;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/UtteranceStats;
    .locals 3

    .prologue
    .line 214
    iget-object v1, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    if-nez v1, :cond_0

    .line 215
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 218
    :cond_0
    iget-object v0, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    .line 219
    .local v0, "returnMe":Lspeech/patts/UtteranceStats;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    .line 220
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats$Builder;->clone()Lspeech/patts/UtteranceStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats$Builder;->clone()Lspeech/patts/UtteranceStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 159
    invoke-virtual {p0}, Lspeech/patts/UtteranceStats$Builder;->clone()Lspeech/patts/UtteranceStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/UtteranceStats$Builder;
    .locals 2

    .prologue
    .line 187
    invoke-static {}, Lspeech/patts/UtteranceStats$Builder;->create()Lspeech/patts/UtteranceStats$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    invoke-virtual {v0, v1}, Lspeech/patts/UtteranceStats$Builder;->mergeFrom(Lspeech/patts/UtteranceStats;)Lspeech/patts/UtteranceStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    invoke-virtual {v0}, Lspeech/patts/UtteranceStats;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 159
    check-cast p1, Lspeech/patts/UtteranceStats;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/UtteranceStats$Builder;->mergeFrom(Lspeech/patts/UtteranceStats;)Lspeech/patts/UtteranceStats$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/UtteranceStats;)Lspeech/patts/UtteranceStats$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/UtteranceStats;

    .prologue
    .line 224
    invoke-static {}, Lspeech/patts/UtteranceStats;->getDefaultInstance()Lspeech/patts/UtteranceStats;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 234
    :cond_0
    :goto_0
    return-object p0

    .line 225
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/UtteranceStats;->hasSentScore()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 226
    invoke-virtual {p1}, Lspeech/patts/UtteranceStats;->getSentScore()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/UtteranceStats$Builder;->setSentScore(F)Lspeech/patts/UtteranceStats$Builder;

    .line 228
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/UtteranceStats;->hasSentScoreIdx()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 229
    invoke-virtual {p1}, Lspeech/patts/UtteranceStats;->getSentScoreIdx()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/UtteranceStats$Builder;->setSentScoreIdx(I)Lspeech/patts/UtteranceStats$Builder;

    .line 231
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/UtteranceStats;->hasSentShortSegs()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p1}, Lspeech/patts/UtteranceStats;->getSentShortSegs()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/UtteranceStats$Builder;->setSentShortSegs(I)Lspeech/patts/UtteranceStats$Builder;

    goto :goto_0
.end method

.method public setSentScore(F)Lspeech/patts/UtteranceStats$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 277
    iget-object v0, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/UtteranceStats;->hasSentScore:Z
    invoke-static {v0, v1}, Lspeech/patts/UtteranceStats;->access$302(Lspeech/patts/UtteranceStats;Z)Z

    .line 278
    iget-object v0, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    # setter for: Lspeech/patts/UtteranceStats;->sentScore_:F
    invoke-static {v0, p1}, Lspeech/patts/UtteranceStats;->access$402(Lspeech/patts/UtteranceStats;F)F

    .line 279
    return-object p0
.end method

.method public setSentScoreIdx(I)Lspeech/patts/UtteranceStats$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 295
    iget-object v0, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/UtteranceStats;->hasSentScoreIdx:Z
    invoke-static {v0, v1}, Lspeech/patts/UtteranceStats;->access$502(Lspeech/patts/UtteranceStats;Z)Z

    .line 296
    iget-object v0, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    # setter for: Lspeech/patts/UtteranceStats;->sentScoreIdx_:I
    invoke-static {v0, p1}, Lspeech/patts/UtteranceStats;->access$602(Lspeech/patts/UtteranceStats;I)I

    .line 297
    return-object p0
.end method

.method public setSentShortSegs(I)Lspeech/patts/UtteranceStats$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 313
    iget-object v0, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/UtteranceStats;->hasSentShortSegs:Z
    invoke-static {v0, v1}, Lspeech/patts/UtteranceStats;->access$702(Lspeech/patts/UtteranceStats;Z)Z

    .line 314
    iget-object v0, p0, Lspeech/patts/UtteranceStats$Builder;->result:Lspeech/patts/UtteranceStats;

    # setter for: Lspeech/patts/UtteranceStats;->sentShortSegs_:I
    invoke-static {v0, p1}, Lspeech/patts/UtteranceStats;->access$802(Lspeech/patts/UtteranceStats;I)I

    .line 315
    return-object p0
.end method
