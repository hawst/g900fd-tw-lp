.class public final Lspeech/patts/HomographDefinitions$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "HomographDefinitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/HomographDefinitions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/HomographDefinitions;",
        "Lspeech/patts/HomographDefinitions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/HomographDefinitions;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/HomographDefinitions$Builder;
    .locals 1

    .prologue
    .line 139
    invoke-static {}, Lspeech/patts/HomographDefinitions$Builder;->create()Lspeech/patts/HomographDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/HomographDefinitions$Builder;
    .locals 3

    .prologue
    .line 148
    new-instance v0, Lspeech/patts/HomographDefinitions$Builder;

    invoke-direct {v0}, Lspeech/patts/HomographDefinitions$Builder;-><init>()V

    .line 149
    .local v0, "builder":Lspeech/patts/HomographDefinitions$Builder;
    new-instance v1, Lspeech/patts/HomographDefinitions;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/HomographDefinitions;-><init>(Lspeech/patts/HomographDefinitions$1;)V

    iput-object v1, v0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    .line 150
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lspeech/patts/HomographDefinitions$Builder;->build()Lspeech/patts/HomographDefinitions;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/HomographDefinitions;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/HomographDefinitions$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    invoke-static {v0}, Lspeech/patts/HomographDefinitions$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 181
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/HomographDefinitions$Builder;->buildPartial()Lspeech/patts/HomographDefinitions;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/HomographDefinitions;
    .locals 3

    .prologue
    .line 194
    iget-object v1, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    if-nez v1, :cond_0

    .line 195
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 198
    :cond_0
    iget-object v1, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    # getter for: Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HomographDefinitions;->access$300(Lspeech/patts/HomographDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 199
    iget-object v1, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    iget-object v2, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    # getter for: Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HomographDefinitions;->access$300(Lspeech/patts/HomographDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HomographDefinitions;->access$302(Lspeech/patts/HomographDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 202
    :cond_1
    iget-object v0, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    .line 203
    .local v0, "returnMe":Lspeech/patts/HomographDefinitions;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    .line 204
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lspeech/patts/HomographDefinitions$Builder;->clone()Lspeech/patts/HomographDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lspeech/patts/HomographDefinitions$Builder;->clone()Lspeech/patts/HomographDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p0}, Lspeech/patts/HomographDefinitions$Builder;->clone()Lspeech/patts/HomographDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/HomographDefinitions$Builder;
    .locals 2

    .prologue
    .line 167
    invoke-static {}, Lspeech/patts/HomographDefinitions$Builder;->create()Lspeech/patts/HomographDefinitions$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    invoke-virtual {v0, v1}, Lspeech/patts/HomographDefinitions$Builder;->mergeFrom(Lspeech/patts/HomographDefinitions;)Lspeech/patts/HomographDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    invoke-virtual {v0}, Lspeech/patts/HomographDefinitions;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 139
    check-cast p1, Lspeech/patts/HomographDefinitions;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/HomographDefinitions$Builder;->mergeFrom(Lspeech/patts/HomographDefinitions;)Lspeech/patts/HomographDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/HomographDefinitions;)Lspeech/patts/HomographDefinitions$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/HomographDefinitions;

    .prologue
    .line 208
    invoke-static {}, Lspeech/patts/HomographDefinitions;->getDefaultInstance()Lspeech/patts/HomographDefinitions;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-object p0

    .line 209
    :cond_1
    # getter for: Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinitions;->access$300(Lspeech/patts/HomographDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    # getter for: Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinitions;->access$300(Lspeech/patts/HomographDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 211
    iget-object v0, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinitions;->access$302(Lspeech/patts/HomographDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 213
    :cond_2
    iget-object v0, p0, Lspeech/patts/HomographDefinitions$Builder;->result:Lspeech/patts/HomographDefinitions;

    # getter for: Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinitions;->access$300(Lspeech/patts/HomographDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HomographDefinitions;->definition_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinitions;->access$300(Lspeech/patts/HomographDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
