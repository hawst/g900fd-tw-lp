.class public final Lspeech/patts/LinearRankerModel$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LinearRankerModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/LinearRankerModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/LinearRankerModel;",
        "Lspeech/patts/LinearRankerModel$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/LinearRankerModel;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/LinearRankerModel$Builder;
    .locals 1

    .prologue
    .line 154
    invoke-static {}, Lspeech/patts/LinearRankerModel$Builder;->create()Lspeech/patts/LinearRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/LinearRankerModel$Builder;
    .locals 3

    .prologue
    .line 163
    new-instance v0, Lspeech/patts/LinearRankerModel$Builder;

    invoke-direct {v0}, Lspeech/patts/LinearRankerModel$Builder;-><init>()V

    .line 164
    .local v0, "builder":Lspeech/patts/LinearRankerModel$Builder;
    new-instance v1, Lspeech/patts/LinearRankerModel;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/LinearRankerModel;-><init>(Lspeech/patts/LinearRankerModel$1;)V

    iput-object v1, v0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    .line 165
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel$Builder;->build()Lspeech/patts/LinearRankerModel;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/LinearRankerModel;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    iget-object v0, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    invoke-static {v0}, Lspeech/patts/LinearRankerModel$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 196
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel$Builder;->buildPartial()Lspeech/patts/LinearRankerModel;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/LinearRankerModel;
    .locals 3

    .prologue
    .line 209
    iget-object v1, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    if-nez v1, :cond_0

    .line 210
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 213
    :cond_0
    iget-object v1, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    # getter for: Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/LinearRankerModel;->access$300(Lspeech/patts/LinearRankerModel;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 214
    iget-object v1, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    iget-object v2, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    # getter for: Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/LinearRankerModel;->access$300(Lspeech/patts/LinearRankerModel;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/LinearRankerModel;->access$302(Lspeech/patts/LinearRankerModel;Ljava/util/List;)Ljava/util/List;

    .line 217
    :cond_1
    iget-object v0, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    .line 218
    .local v0, "returnMe":Lspeech/patts/LinearRankerModel;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    .line 219
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel$Builder;->clone()Lspeech/patts/LinearRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel$Builder;->clone()Lspeech/patts/LinearRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 154
    invoke-virtual {p0}, Lspeech/patts/LinearRankerModel$Builder;->clone()Lspeech/patts/LinearRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/LinearRankerModel$Builder;
    .locals 2

    .prologue
    .line 182
    invoke-static {}, Lspeech/patts/LinearRankerModel$Builder;->create()Lspeech/patts/LinearRankerModel$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    invoke-virtual {v0, v1}, Lspeech/patts/LinearRankerModel$Builder;->mergeFrom(Lspeech/patts/LinearRankerModel;)Lspeech/patts/LinearRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    invoke-virtual {v0}, Lspeech/patts/LinearRankerModel;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 154
    check-cast p1, Lspeech/patts/LinearRankerModel;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/LinearRankerModel$Builder;->mergeFrom(Lspeech/patts/LinearRankerModel;)Lspeech/patts/LinearRankerModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/LinearRankerModel;)Lspeech/patts/LinearRankerModel$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/LinearRankerModel;

    .prologue
    .line 223
    invoke-static {}, Lspeech/patts/LinearRankerModel;->getDefaultInstance()Lspeech/patts/LinearRankerModel;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-object p0

    .line 224
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/LinearRankerModel;->hasSize()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 225
    invoke-virtual {p1}, Lspeech/patts/LinearRankerModel;->getSize()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lspeech/patts/LinearRankerModel$Builder;->setSize(J)Lspeech/patts/LinearRankerModel$Builder;

    .line 227
    :cond_2
    # getter for: Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/LinearRankerModel;->access$300(Lspeech/patts/LinearRankerModel;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    iget-object v0, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    # getter for: Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/LinearRankerModel;->access$300(Lspeech/patts/LinearRankerModel;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 229
    iget-object v0, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/LinearRankerModel;->access$302(Lspeech/patts/LinearRankerModel;Ljava/util/List;)Ljava/util/List;

    .line 231
    :cond_3
    iget-object v0, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    # getter for: Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/LinearRankerModel;->access$300(Lspeech/patts/LinearRankerModel;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/LinearRankerModel;->featsAndVals_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/LinearRankerModel;->access$300(Lspeech/patts/LinearRankerModel;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public setSize(J)Lspeech/patts/LinearRankerModel$Builder;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 274
    iget-object v0, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/LinearRankerModel;->hasSize:Z
    invoke-static {v0, v1}, Lspeech/patts/LinearRankerModel;->access$402(Lspeech/patts/LinearRankerModel;Z)Z

    .line 275
    iget-object v0, p0, Lspeech/patts/LinearRankerModel$Builder;->result:Lspeech/patts/LinearRankerModel;

    # setter for: Lspeech/patts/LinearRankerModel;->size_:J
    invoke-static {v0, p1, p2}, Lspeech/patts/LinearRankerModel;->access$502(Lspeech/patts/LinearRankerModel;J)J

    .line 276
    return-object p0
.end method
