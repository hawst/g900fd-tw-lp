.class public final Lspeech/patts/PronWord;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PronWord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/PronWord$1;,
        Lspeech/patts/PronWord$Builder;,
        Lspeech/patts/PronWord$Component;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/PronWord;


# instance fields
.field private component_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/PronWord$Component;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 635
    new-instance v0, Lspeech/patts/PronWord;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/PronWord;-><init>(Z)V

    sput-object v0, Lspeech/patts/PronWord;->defaultInstance:Lspeech/patts/PronWord;

    .line 636
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 637
    sget-object v0, Lspeech/patts/PronWord;->defaultInstance:Lspeech/patts/PronWord;

    invoke-direct {v0}, Lspeech/patts/PronWord;->initFields()V

    .line 638
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 363
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PronWord;->component_:Ljava/util/List;

    .line 387
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PronWord;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/PronWord;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/PronWord$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/PronWord$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/PronWord;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 363
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PronWord;->component_:Ljava/util/List;

    .line 387
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PronWord;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1200(Lspeech/patts/PronWord;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PronWord;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PronWord;->component_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1202(Lspeech/patts/PronWord;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PronWord;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PronWord;->component_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/PronWord;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/PronWord;->defaultInstance:Lspeech/patts/PronWord;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 374
    return-void
.end method

.method public static newBuilder()Lspeech/patts/PronWord$Builder;
    .locals 1

    .prologue
    .line 468
    # invokes: Lspeech/patts/PronWord$Builder;->create()Lspeech/patts/PronWord$Builder;
    invoke-static {}, Lspeech/patts/PronWord$Builder;->access$1000()Lspeech/patts/PronWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/PronWord;)Lspeech/patts/PronWord$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/PronWord;

    .prologue
    .line 471
    invoke-static {}, Lspeech/patts/PronWord;->newBuilder()Lspeech/patts/PronWord$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/PronWord$Builder;->mergeFrom(Lspeech/patts/PronWord;)Lspeech/patts/PronWord$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getComponentList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/PronWord$Component;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366
    iget-object v0, p0, Lspeech/patts/PronWord;->component_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/PronWord;->getDefaultInstanceForType()Lspeech/patts/PronWord;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/PronWord;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/PronWord;->defaultInstance:Lspeech/patts/PronWord;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 389
    iget v2, p0, Lspeech/patts/PronWord;->memoizedSerializedSize:I

    .line 390
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 398
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 392
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 393
    invoke-virtual {p0}, Lspeech/patts/PronWord;->getComponentList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/PronWord$Component;

    .line 394
    .local v0, "element":Lspeech/patts/PronWord$Component;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 396
    goto :goto_1

    .line 397
    .end local v0    # "element":Lspeech/patts/PronWord$Component;
    :cond_1
    iput v2, p0, Lspeech/patts/PronWord;->memoizedSerializedSize:I

    move v3, v2

    .line 398
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/PronWord;->toBuilder()Lspeech/patts/PronWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/PronWord$Builder;
    .locals 1

    .prologue
    .line 473
    invoke-static {p0}, Lspeech/patts/PronWord;->newBuilder(Lspeech/patts/PronWord;)Lspeech/patts/PronWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 381
    invoke-virtual {p0}, Lspeech/patts/PronWord;->getSerializedSize()I

    .line 382
    invoke-virtual {p0}, Lspeech/patts/PronWord;->getComponentList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/PronWord$Component;

    .line 383
    .local v0, "element":Lspeech/patts/PronWord$Component;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 385
    .end local v0    # "element":Lspeech/patts/PronWord$Component;
    :cond_0
    return-void
.end method
