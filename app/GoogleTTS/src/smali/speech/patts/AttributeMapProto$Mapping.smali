.class public final Lspeech/patts/AttributeMapProto$Mapping;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AttributeMapProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/AttributeMapProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Mapping"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/AttributeMapProto$Mapping$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/AttributeMapProto$Mapping;


# instance fields
.field private category_:Ljava/lang/String;

.field private dest_:Ljava/lang/String;

.field private hasCategory:Z

.field private hasDest:Z

.field private hasSource:Z

.field private memoizedSerializedSize:I

.field private source_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 356
    new-instance v0, Lspeech/patts/AttributeMapProto$Mapping;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/AttributeMapProto$Mapping;-><init>(Z)V

    sput-object v0, Lspeech/patts/AttributeMapProto$Mapping;->defaultInstance:Lspeech/patts/AttributeMapProto$Mapping;

    .line 357
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 358
    sget-object v0, Lspeech/patts/AttributeMapProto$Mapping;->defaultInstance:Lspeech/patts/AttributeMapProto$Mapping;

    invoke-direct {v0}, Lspeech/patts/AttributeMapProto$Mapping;->initFields()V

    .line 359
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->category_:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->source_:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->dest_:Ljava/lang/String;

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->memoizedSerializedSize:I

    .line 26
    invoke-direct {p0}, Lspeech/patts/AttributeMapProto$Mapping;->initFields()V

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/AttributeMapProto$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/AttributeMapProto$1;

    .prologue
    .line 22
    invoke-direct {p0}, Lspeech/patts/AttributeMapProto$Mapping;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->category_:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->source_:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->dest_:Ljava/lang/String;

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->memoizedSerializedSize:I

    .line 28
    return-void
.end method

.method static synthetic access$302(Lspeech/patts/AttributeMapProto$Mapping;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto$Mapping;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lspeech/patts/AttributeMapProto$Mapping;->hasCategory:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/AttributeMapProto$Mapping;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto$Mapping;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lspeech/patts/AttributeMapProto$Mapping;->category_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/AttributeMapProto$Mapping;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto$Mapping;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lspeech/patts/AttributeMapProto$Mapping;->hasSource:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/AttributeMapProto$Mapping;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto$Mapping;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lspeech/patts/AttributeMapProto$Mapping;->source_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lspeech/patts/AttributeMapProto$Mapping;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto$Mapping;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lspeech/patts/AttributeMapProto$Mapping;->hasDest:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/AttributeMapProto$Mapping;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AttributeMapProto$Mapping;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lspeech/patts/AttributeMapProto$Mapping;->dest_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/AttributeMapProto$Mapping;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lspeech/patts/AttributeMapProto$Mapping;->defaultInstance:Lspeech/patts/AttributeMapProto$Mapping;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public static newBuilder()Lspeech/patts/AttributeMapProto$Mapping$Builder;
    .locals 1

    .prologue
    .line 172
    # invokes: Lspeech/patts/AttributeMapProto$Mapping$Builder;->create()Lspeech/patts/AttributeMapProto$Mapping$Builder;
    invoke-static {}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->access$100()Lspeech/patts/AttributeMapProto$Mapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/AttributeMapProto$Mapping;)Lspeech/patts/AttributeMapProto$Mapping$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/AttributeMapProto$Mapping;

    .prologue
    .line 175
    invoke-static {}, Lspeech/patts/AttributeMapProto$Mapping;->newBuilder()Lspeech/patts/AttributeMapProto$Mapping$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->mergeFrom(Lspeech/patts/AttributeMapProto$Mapping;)Lspeech/patts/AttributeMapProto$Mapping$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->category_:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->getDefaultInstanceForType()Lspeech/patts/AttributeMapProto$Mapping;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/AttributeMapProto$Mapping;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lspeech/patts/AttributeMapProto$Mapping;->defaultInstance:Lspeech/patts/AttributeMapProto$Mapping;

    return-object v0
.end method

.method public getDest()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->dest_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 85
    iget v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->memoizedSerializedSize:I

    .line 86
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 102
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 88
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 89
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->hasCategory()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->getCategory()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 93
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->hasSource()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 94
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->getSource()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 97
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->hasDest()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 98
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->getDest()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 101
    :cond_3
    iput v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->memoizedSerializedSize:I

    move v1, v0

    .line 102
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->source_:Ljava/lang/String;

    return-object v0
.end method

.method public hasCategory()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->hasCategory:Z

    return v0
.end method

.method public hasDest()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->hasDest:Z

    return v0
.end method

.method public hasSource()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lspeech/patts/AttributeMapProto$Mapping;->hasSource:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 63
    iget-boolean v1, p0, Lspeech/patts/AttributeMapProto$Mapping;->hasCategory:Z

    if-nez v1, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 64
    :cond_1
    iget-boolean v1, p0, Lspeech/patts/AttributeMapProto$Mapping;->hasSource:Z

    if-eqz v1, :cond_0

    .line 65
    iget-boolean v1, p0, Lspeech/patts/AttributeMapProto$Mapping;->hasDest:Z

    if-eqz v1, :cond_0

    .line 66
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->toBuilder()Lspeech/patts/AttributeMapProto$Mapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/AttributeMapProto$Mapping$Builder;
    .locals 1

    .prologue
    .line 177
    invoke-static {p0}, Lspeech/patts/AttributeMapProto$Mapping;->newBuilder(Lspeech/patts/AttributeMapProto$Mapping;)Lspeech/patts/AttributeMapProto$Mapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->getSerializedSize()I

    .line 72
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->hasCategory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->getCategory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 75
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->hasSource()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->getSource()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 78
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->hasDest()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping;->getDest()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 81
    :cond_2
    return-void
.end method
