.class public final Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FrequencyWarpAndWeightTransform.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/FrequencyWarpAndWeightTransform;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/FrequencyWarpAndWeightTransform;",
        "Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/FrequencyWarpAndWeightTransform;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
    .locals 1

    .prologue
    .line 269
    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->create()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
    .locals 3

    .prologue
    .line 278
    new-instance v0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    invoke-direct {v0}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;-><init>()V

    .line 279
    .local v0, "builder":Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
    new-instance v1, Lspeech/patts/FrequencyWarpAndWeightTransform;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/FrequencyWarpAndWeightTransform;-><init>(Lspeech/patts/FrequencyWarpAndWeightTransform$1;)V

    iput-object v1, v0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    .line 280
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 269
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->build()Lspeech/patts/FrequencyWarpAndWeightTransform;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/FrequencyWarpAndWeightTransform;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 309
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 311
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->buildPartial()Lspeech/patts/FrequencyWarpAndWeightTransform;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/FrequencyWarpAndWeightTransform;
    .locals 3

    .prologue
    .line 324
    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    if-nez v1, :cond_0

    .line 325
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 328
    :cond_0
    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$300(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 329
    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    iget-object v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$300(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$302(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/util/List;)Ljava/util/List;

    .line 332
    :cond_1
    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$400(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 333
    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    iget-object v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$400(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$402(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/util/List;)Ljava/util/List;

    .line 336
    :cond_2
    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$500(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 337
    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    iget-object v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$500(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$502(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/util/List;)Ljava/util/List;

    .line 340
    :cond_3
    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$600(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 341
    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    iget-object v2, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$600(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$602(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/util/List;)Ljava/util/List;

    .line 344
    :cond_4
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    .line 345
    .local v0, "returnMe":Lspeech/patts/FrequencyWarpAndWeightTransform;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    .line 346
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 269
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->clone()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 269
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->clone()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 269
    invoke-virtual {p0}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->clone()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
    .locals 2

    .prologue
    .line 297
    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->create()Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    invoke-virtual {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->mergeFrom(Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    invoke-virtual {v0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 269
    check-cast p1, Lspeech/patts/FrequencyWarpAndWeightTransform;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->mergeFrom(Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/FrequencyWarpAndWeightTransform;)Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/FrequencyWarpAndWeightTransform;

    .prologue
    .line 350
    invoke-static {}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getDefaultInstance()Lspeech/patts/FrequencyWarpAndWeightTransform;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 381
    :cond_0
    :goto_0
    return-object p0

    .line 351
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->hasLabel()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 352
    invoke-virtual {p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->setLabel(Ljava/lang/String;)Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    .line 354
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->hasLinearFactor()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 355
    invoke-virtual {p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->getLinearFactor()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->setLinearFactor(D)Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;

    .line 357
    :cond_3
    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$300(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 358
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$300(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 359
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$302(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/util/List;)Ljava/util/List;

    .line 361
    :cond_4
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$300(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceFrequency_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$300(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 363
    :cond_5
    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$400(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 364
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$400(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 365
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$402(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/util/List;)Ljava/util/List;

    .line 367
    :cond_6
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$400(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->targetFrequency_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$400(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 369
    :cond_7
    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$500(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 370
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$500(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 371
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$502(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/util/List;)Ljava/util/List;

    .line 373
    :cond_8
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$500(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->frequencyWeighting_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$500(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 375
    :cond_9
    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$600(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 376
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$600(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 377
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$602(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/util/List;)Ljava/util/List;

    .line 379
    :cond_a
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$600(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->sourceMcepClassMean_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$600(Lspeech/patts/FrequencyWarpAndWeightTransform;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setLabel(Ljava/lang/String;)Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 472
    if-nez p1, :cond_0

    .line 473
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 475
    :cond_0
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->hasLabel:Z
    invoke-static {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$702(Lspeech/patts/FrequencyWarpAndWeightTransform;Z)Z

    .line 476
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->label_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$802(Lspeech/patts/FrequencyWarpAndWeightTransform;Ljava/lang/String;)Ljava/lang/String;

    .line 477
    return-object p0
.end method

.method public setLinearFactor(D)Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;
    .locals 3
    .param p1, "value"    # D

    .prologue
    .line 493
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->hasLinearFactor:Z
    invoke-static {v0, v1}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$902(Lspeech/patts/FrequencyWarpAndWeightTransform;Z)Z

    .line 494
    iget-object v0, p0, Lspeech/patts/FrequencyWarpAndWeightTransform$Builder;->result:Lspeech/patts/FrequencyWarpAndWeightTransform;

    # setter for: Lspeech/patts/FrequencyWarpAndWeightTransform;->linearFactor_:D
    invoke-static {v0, p1, p2}, Lspeech/patts/FrequencyWarpAndWeightTransform;->access$1002(Lspeech/patts/FrequencyWarpAndWeightTransform;D)D

    .line 495
    return-object p0
.end method
