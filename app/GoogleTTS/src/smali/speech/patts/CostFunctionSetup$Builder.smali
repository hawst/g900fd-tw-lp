.class public final Lspeech/patts/CostFunctionSetup$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "CostFunctionSetup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/CostFunctionSetup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/CostFunctionSetup;",
        "Lspeech/patts/CostFunctionSetup$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/CostFunctionSetup;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/CostFunctionSetup$Builder;
    .locals 1

    .prologue
    .line 173
    invoke-static {}, Lspeech/patts/CostFunctionSetup$Builder;->create()Lspeech/patts/CostFunctionSetup$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/CostFunctionSetup$Builder;
    .locals 3

    .prologue
    .line 182
    new-instance v0, Lspeech/patts/CostFunctionSetup$Builder;

    invoke-direct {v0}, Lspeech/patts/CostFunctionSetup$Builder;-><init>()V

    .line 183
    .local v0, "builder":Lspeech/patts/CostFunctionSetup$Builder;
    new-instance v1, Lspeech/patts/CostFunctionSetup;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/CostFunctionSetup;-><init>(Lspeech/patts/CostFunctionSetup$1;)V

    iput-object v1, v0, Lspeech/patts/CostFunctionSetup$Builder;->result:Lspeech/patts/CostFunctionSetup;

    .line 184
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup$Builder;->build()Lspeech/patts/CostFunctionSetup;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/CostFunctionSetup;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lspeech/patts/CostFunctionSetup$Builder;->result:Lspeech/patts/CostFunctionSetup;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    iget-object v0, p0, Lspeech/patts/CostFunctionSetup$Builder;->result:Lspeech/patts/CostFunctionSetup;

    invoke-static {v0}, Lspeech/patts/CostFunctionSetup$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 215
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup$Builder;->buildPartial()Lspeech/patts/CostFunctionSetup;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/CostFunctionSetup;
    .locals 3

    .prologue
    .line 228
    iget-object v1, p0, Lspeech/patts/CostFunctionSetup$Builder;->result:Lspeech/patts/CostFunctionSetup;

    if-nez v1, :cond_0

    .line 229
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 232
    :cond_0
    iget-object v0, p0, Lspeech/patts/CostFunctionSetup$Builder;->result:Lspeech/patts/CostFunctionSetup;

    .line 233
    .local v0, "returnMe":Lspeech/patts/CostFunctionSetup;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/CostFunctionSetup$Builder;->result:Lspeech/patts/CostFunctionSetup;

    .line 234
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup$Builder;->clone()Lspeech/patts/CostFunctionSetup$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup$Builder;->clone()Lspeech/patts/CostFunctionSetup$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 173
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup$Builder;->clone()Lspeech/patts/CostFunctionSetup$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/CostFunctionSetup$Builder;
    .locals 2

    .prologue
    .line 201
    invoke-static {}, Lspeech/patts/CostFunctionSetup$Builder;->create()Lspeech/patts/CostFunctionSetup$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/CostFunctionSetup$Builder;->result:Lspeech/patts/CostFunctionSetup;

    invoke-virtual {v0, v1}, Lspeech/patts/CostFunctionSetup$Builder;->mergeFrom(Lspeech/patts/CostFunctionSetup;)Lspeech/patts/CostFunctionSetup$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lspeech/patts/CostFunctionSetup$Builder;->result:Lspeech/patts/CostFunctionSetup;

    invoke-virtual {v0}, Lspeech/patts/CostFunctionSetup;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 173
    check-cast p1, Lspeech/patts/CostFunctionSetup;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/CostFunctionSetup$Builder;->mergeFrom(Lspeech/patts/CostFunctionSetup;)Lspeech/patts/CostFunctionSetup$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/CostFunctionSetup;)Lspeech/patts/CostFunctionSetup$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/CostFunctionSetup;

    .prologue
    .line 238
    invoke-static {}, Lspeech/patts/CostFunctionSetup;->getDefaultInstance()Lspeech/patts/CostFunctionSetup;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-object p0

    .line 239
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/CostFunctionSetup;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    invoke-virtual {p1}, Lspeech/patts/CostFunctionSetup;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/CostFunctionSetup$Builder;->setName(Ljava/lang/String;)Lspeech/patts/CostFunctionSetup$Builder;

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)Lspeech/patts/CostFunctionSetup$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 277
    if-nez p1, :cond_0

    .line 278
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 280
    :cond_0
    iget-object v0, p0, Lspeech/patts/CostFunctionSetup$Builder;->result:Lspeech/patts/CostFunctionSetup;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/CostFunctionSetup;->hasName:Z
    invoke-static {v0, v1}, Lspeech/patts/CostFunctionSetup;->access$302(Lspeech/patts/CostFunctionSetup;Z)Z

    .line 281
    iget-object v0, p0, Lspeech/patts/CostFunctionSetup$Builder;->result:Lspeech/patts/CostFunctionSetup;

    # setter for: Lspeech/patts/CostFunctionSetup;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/CostFunctionSetup;->access$402(Lspeech/patts/CostFunctionSetup;Ljava/lang/String;)Ljava/lang/String;

    .line 282
    return-object p0
.end method
