.class public final Lspeech/patts/Model2$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Model2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Model2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/Model2;",
        "Lspeech/patts/Model2$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/Model2;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/Model2$Builder;
    .locals 1

    .prologue
    .line 172
    invoke-static {}, Lspeech/patts/Model2$Builder;->create()Lspeech/patts/Model2$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/Model2$Builder;
    .locals 3

    .prologue
    .line 181
    new-instance v0, Lspeech/patts/Model2$Builder;

    invoke-direct {v0}, Lspeech/patts/Model2$Builder;-><init>()V

    .line 182
    .local v0, "builder":Lspeech/patts/Model2$Builder;
    new-instance v1, Lspeech/patts/Model2;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/Model2;-><init>(Lspeech/patts/Model2$1;)V

    iput-object v1, v0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    .line 183
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Lspeech/patts/Model2$Builder;->build()Lspeech/patts/Model2;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/Model2;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/Model2$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    invoke-static {v0}, Lspeech/patts/Model2$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 214
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Model2$Builder;->buildPartial()Lspeech/patts/Model2;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/Model2;
    .locals 3

    .prologue
    .line 227
    iget-object v1, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    if-nez v1, :cond_0

    .line 228
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 231
    :cond_0
    iget-object v1, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    # getter for: Lspeech/patts/Model2;->unitId_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Model2;->access$300(Lspeech/patts/Model2;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 232
    iget-object v1, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    iget-object v2, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    # getter for: Lspeech/patts/Model2;->unitId_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Model2;->access$300(Lspeech/patts/Model2;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Model2;->unitId_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Model2;->access$302(Lspeech/patts/Model2;Ljava/util/List;)Ljava/util/List;

    .line 235
    :cond_1
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    .line 236
    .local v0, "returnMe":Lspeech/patts/Model2;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    .line 237
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Lspeech/patts/Model2$Builder;->clone()Lspeech/patts/Model2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Lspeech/patts/Model2$Builder;->clone()Lspeech/patts/Model2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 172
    invoke-virtual {p0}, Lspeech/patts/Model2$Builder;->clone()Lspeech/patts/Model2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/Model2$Builder;
    .locals 2

    .prologue
    .line 200
    invoke-static {}, Lspeech/patts/Model2$Builder;->create()Lspeech/patts/Model2$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    invoke-virtual {v0, v1}, Lspeech/patts/Model2$Builder;->mergeFrom(Lspeech/patts/Model2;)Lspeech/patts/Model2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    invoke-virtual {v0}, Lspeech/patts/Model2;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 172
    check-cast p1, Lspeech/patts/Model2;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/Model2$Builder;->mergeFrom(Lspeech/patts/Model2;)Lspeech/patts/Model2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/Model2;)Lspeech/patts/Model2$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/Model2;

    .prologue
    .line 241
    invoke-static {}, Lspeech/patts/Model2;->getDefaultInstance()Lspeech/patts/Model2;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-object p0

    .line 242
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/Model2;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 243
    invoke-virtual {p1}, Lspeech/patts/Model2;->getTarget()Lspeech/patts/FeatureData;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Model2$Builder;->mergeTarget(Lspeech/patts/FeatureData;)Lspeech/patts/Model2$Builder;

    .line 245
    :cond_2
    # getter for: Lspeech/patts/Model2;->unitId_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Model2;->access$300(Lspeech/patts/Model2;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 246
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    # getter for: Lspeech/patts/Model2;->unitId_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Model2;->access$300(Lspeech/patts/Model2;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 247
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Model2;->unitId_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Model2;->access$302(Lspeech/patts/Model2;Ljava/util/List;)Ljava/util/List;

    .line 249
    :cond_3
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    # getter for: Lspeech/patts/Model2;->unitId_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Model2;->access$300(Lspeech/patts/Model2;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Model2;->unitId_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Model2;->access$300(Lspeech/patts/Model2;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 251
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/Model2;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {p1}, Lspeech/patts/Model2;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Model2$Builder;->setName(Ljava/lang/String;)Lspeech/patts/Model2$Builder;

    goto :goto_0
.end method

.method public mergeTarget(Lspeech/patts/FeatureData;)Lspeech/patts/Model2$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/FeatureData;

    .prologue
    .line 324
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    invoke-virtual {v0}, Lspeech/patts/Model2;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    # getter for: Lspeech/patts/Model2;->target_:Lspeech/patts/FeatureData;
    invoke-static {v0}, Lspeech/patts/Model2;->access$500(Lspeech/patts/Model2;)Lspeech/patts/FeatureData;

    move-result-object v0

    invoke-static {}, Lspeech/patts/FeatureData;->getDefaultInstance()Lspeech/patts/FeatureData;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 326
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    iget-object v1, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    # getter for: Lspeech/patts/Model2;->target_:Lspeech/patts/FeatureData;
    invoke-static {v1}, Lspeech/patts/Model2;->access$500(Lspeech/patts/Model2;)Lspeech/patts/FeatureData;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/FeatureData;->newBuilder(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/FeatureData$Builder;->mergeFrom(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/FeatureData$Builder;->buildPartial()Lspeech/patts/FeatureData;

    move-result-object v1

    # setter for: Lspeech/patts/Model2;->target_:Lspeech/patts/FeatureData;
    invoke-static {v0, v1}, Lspeech/patts/Model2;->access$502(Lspeech/patts/Model2;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;

    .line 331
    :goto_0
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Model2;->hasTarget:Z
    invoke-static {v0, v1}, Lspeech/patts/Model2;->access$402(Lspeech/patts/Model2;Z)Z

    .line 332
    return-object p0

    .line 329
    :cond_0
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    # setter for: Lspeech/patts/Model2;->target_:Lspeech/patts/FeatureData;
    invoke-static {v0, p1}, Lspeech/patts/Model2;->access$502(Lspeech/patts/Model2;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)Lspeech/patts/Model2$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 382
    if-nez p1, :cond_0

    .line 383
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 385
    :cond_0
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Model2;->hasName:Z
    invoke-static {v0, v1}, Lspeech/patts/Model2;->access$602(Lspeech/patts/Model2;Z)Z

    .line 386
    iget-object v0, p0, Lspeech/patts/Model2$Builder;->result:Lspeech/patts/Model2;

    # setter for: Lspeech/patts/Model2;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Model2;->access$702(Lspeech/patts/Model2;Ljava/lang/String;)Ljava/lang/String;

    .line 387
    return-object p0
.end method
