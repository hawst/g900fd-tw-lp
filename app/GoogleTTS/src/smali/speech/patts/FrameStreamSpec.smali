.class public final Lspeech/patts/FrameStreamSpec;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FrameStreamSpec.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/FrameStreamSpec$1;,
        Lspeech/patts/FrameStreamSpec$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/FrameStreamSpec;


# instance fields
.field private amplitudeSpec_:Lspeech/patts/SpeechParamSpec;

.field private aperiodicitySpec_:Lspeech/patts/SpeechParamSpec;

.field private bandAperiodicitySpec_:Lspeech/patts/SpeechParamSpec;

.field private dspProcType_:Ljava/lang/String;

.field private frameShift_:F

.field private frequencyWarping_:F

.field private hasAmplitudeSpec:Z

.field private hasAperiodicitySpec:Z

.field private hasBandAperiodicitySpec:Z

.field private hasDspProcType:Z

.field private hasFrameShift:Z

.field private hasFrequencyWarping:Z

.field private hasLineSpectralPairSpec:Z

.field private hasLogF0Spec:Z

.field private hasMelCepstrumSpec:Z

.field private hasMgcGammaStage:Z

.field private hasSampleRate:Z

.field private hasUseLinearAp:Z

.field private hasVoicingSpec:Z

.field private lineSpectralPairSpec_:Lspeech/patts/SpeechParamSpec;

.field private logF0Spec_:Lspeech/patts/SpeechParamSpec;

.field private melCepstrumSpec_:Lspeech/patts/SpeechParamSpec;

.field private memoizedSerializedSize:I

.field private mgcGammaStage_:I

.field private sampleRate_:I

.field private useLinearAp_:Z

.field private voicingSpec_:Lspeech/patts/SpeechParamSpec;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 916
    new-instance v0, Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/FrameStreamSpec;-><init>(Z)V

    sput-object v0, Lspeech/patts/FrameStreamSpec;->defaultInstance:Lspeech/patts/FrameStreamSpec;

    .line 917
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 918
    sget-object v0, Lspeech/patts/FrameStreamSpec;->defaultInstance:Lspeech/patts/FrameStreamSpec;

    invoke-direct {v0}, Lspeech/patts/FrameStreamSpec;->initFields()V

    .line 919
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/FrameStreamSpec;->frameShift_:F

    .line 32
    const/16 v0, 0x3e80

    iput v0, p0, Lspeech/patts/FrameStreamSpec;->sampleRate_:I

    .line 39
    const v0, 0x3ed70a3d    # 0.42f

    iput v0, p0, Lspeech/patts/FrameStreamSpec;->frequencyWarping_:F

    .line 46
    iput-boolean v1, p0, Lspeech/patts/FrameStreamSpec;->useLinearAp_:Z

    .line 53
    iput v1, p0, Lspeech/patts/FrameStreamSpec;->mgcGammaStage_:I

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/FrameStreamSpec;->dspProcType_:Ljava/lang/String;

    .line 191
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/FrameStreamSpec;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/FrameStreamSpec;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/FrameStreamSpec$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/FrameStreamSpec$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/FrameStreamSpec;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/FrameStreamSpec;->frameShift_:F

    .line 32
    const/16 v0, 0x3e80

    iput v0, p0, Lspeech/patts/FrameStreamSpec;->sampleRate_:I

    .line 39
    const v0, 0x3ed70a3d    # 0.42f

    iput v0, p0, Lspeech/patts/FrameStreamSpec;->frequencyWarping_:F

    .line 46
    iput-boolean v1, p0, Lspeech/patts/FrameStreamSpec;->useLinearAp_:Z

    .line 53
    iput v1, p0, Lspeech/patts/FrameStreamSpec;->mgcGammaStage_:I

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/FrameStreamSpec;->dspProcType_:Ljava/lang/String;

    .line 191
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/FrameStreamSpec;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->useLinearAp_:Z

    return p1
.end method

.method static synthetic access$1102(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasMgcGammaStage:Z

    return p1
.end method

.method static synthetic access$1202(Lspeech/patts/FrameStreamSpec;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/FrameStreamSpec;->mgcGammaStage_:I

    return p1
.end method

.method static synthetic access$1302(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasDspProcType:Z

    return p1
.end method

.method static synthetic access$1402(Lspeech/patts/FrameStreamSpec;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrameStreamSpec;->dspProcType_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1502(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasAmplitudeSpec:Z

    return p1
.end method

.method static synthetic access$1600(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->amplitudeSpec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method static synthetic access$1602(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrameStreamSpec;->amplitudeSpec_:Lspeech/patts/SpeechParamSpec;

    return-object p1
.end method

.method static synthetic access$1702(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasAperiodicitySpec:Z

    return p1
.end method

.method static synthetic access$1800(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->aperiodicitySpec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method static synthetic access$1802(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrameStreamSpec;->aperiodicitySpec_:Lspeech/patts/SpeechParamSpec;

    return-object p1
.end method

.method static synthetic access$1902(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasLogF0Spec:Z

    return p1
.end method

.method static synthetic access$2000(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->logF0Spec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method static synthetic access$2002(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrameStreamSpec;->logF0Spec_:Lspeech/patts/SpeechParamSpec;

    return-object p1
.end method

.method static synthetic access$2102(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasVoicingSpec:Z

    return p1
.end method

.method static synthetic access$2200(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->voicingSpec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method static synthetic access$2202(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrameStreamSpec;->voicingSpec_:Lspeech/patts/SpeechParamSpec;

    return-object p1
.end method

.method static synthetic access$2302(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasMelCepstrumSpec:Z

    return p1
.end method

.method static synthetic access$2400(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->melCepstrumSpec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method static synthetic access$2402(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrameStreamSpec;->melCepstrumSpec_:Lspeech/patts/SpeechParamSpec;

    return-object p1
.end method

.method static synthetic access$2502(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasLineSpectralPairSpec:Z

    return p1
.end method

.method static synthetic access$2600(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->lineSpectralPairSpec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method static synthetic access$2602(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrameStreamSpec;->lineSpectralPairSpec_:Lspeech/patts/SpeechParamSpec;

    return-object p1
.end method

.method static synthetic access$2702(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasBandAperiodicitySpec:Z

    return p1
.end method

.method static synthetic access$2800(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->bandAperiodicitySpec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method static synthetic access$2802(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FrameStreamSpec;->bandAperiodicitySpec_:Lspeech/patts/SpeechParamSpec;

    return-object p1
.end method

.method static synthetic access$302(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasFrameShift:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/FrameStreamSpec;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/FrameStreamSpec;->frameShift_:F

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasSampleRate:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/FrameStreamSpec;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/FrameStreamSpec;->sampleRate_:I

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasFrequencyWarping:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/FrameStreamSpec;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/FrameStreamSpec;->frequencyWarping_:F

    return p1
.end method

.method static synthetic access$902(Lspeech/patts/FrameStreamSpec;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FrameStreamSpec;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/FrameStreamSpec;->hasUseLinearAp:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/FrameStreamSpec;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/FrameStreamSpec;->defaultInstance:Lspeech/patts/FrameStreamSpec;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 114
    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrameStreamSpec;->amplitudeSpec_:Lspeech/patts/SpeechParamSpec;

    .line 115
    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrameStreamSpec;->aperiodicitySpec_:Lspeech/patts/SpeechParamSpec;

    .line 116
    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrameStreamSpec;->logF0Spec_:Lspeech/patts/SpeechParamSpec;

    .line 117
    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrameStreamSpec;->voicingSpec_:Lspeech/patts/SpeechParamSpec;

    .line 118
    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrameStreamSpec;->melCepstrumSpec_:Lspeech/patts/SpeechParamSpec;

    .line 119
    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrameStreamSpec;->lineSpectralPairSpec_:Lspeech/patts/SpeechParamSpec;

    .line 120
    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FrameStreamSpec;->bandAperiodicitySpec_:Lspeech/patts/SpeechParamSpec;

    .line 121
    return-void
.end method

.method public static newBuilder()Lspeech/patts/FrameStreamSpec$Builder;
    .locals 1

    .prologue
    .line 320
    # invokes: Lspeech/patts/FrameStreamSpec$Builder;->create()Lspeech/patts/FrameStreamSpec$Builder;
    invoke-static {}, Lspeech/patts/FrameStreamSpec$Builder;->access$100()Lspeech/patts/FrameStreamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/FrameStreamSpec;

    .prologue
    .line 323
    invoke-static {}, Lspeech/patts/FrameStreamSpec;->newBuilder()Lspeech/patts/FrameStreamSpec$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/FrameStreamSpec$Builder;->mergeFrom(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/FrameStreamSpec$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAmplitudeSpec()Lspeech/patts/SpeechParamSpec;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->amplitudeSpec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method public getAperiodicitySpec()Lspeech/patts/SpeechParamSpec;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->aperiodicitySpec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method public getBandAperiodicitySpec()Lspeech/patts/SpeechParamSpec;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->bandAperiodicitySpec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getDefaultInstanceForType()Lspeech/patts/FrameStreamSpec;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/FrameStreamSpec;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/FrameStreamSpec;->defaultInstance:Lspeech/patts/FrameStreamSpec;

    return-object v0
.end method

.method public getDspProcType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->dspProcType_:Ljava/lang/String;

    return-object v0
.end method

.method public getFrameShift()F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lspeech/patts/FrameStreamSpec;->frameShift_:F

    return v0
.end method

.method public getFrequencyWarping()F
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lspeech/patts/FrameStreamSpec;->frequencyWarping_:F

    return v0
.end method

.method public getLineSpectralPairSpec()Lspeech/patts/SpeechParamSpec;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->lineSpectralPairSpec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method public getLogF0Spec()Lspeech/patts/SpeechParamSpec;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->logF0Spec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method public getMelCepstrumSpec()Lspeech/patts/SpeechParamSpec;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->melCepstrumSpec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method public getMgcGammaStage()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lspeech/patts/FrameStreamSpec;->mgcGammaStage_:I

    return v0
.end method

.method public getSampleRate()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lspeech/patts/FrameStreamSpec;->sampleRate_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 193
    iget v0, p0, Lspeech/patts/FrameStreamSpec;->memoizedSerializedSize:I

    .line 194
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 250
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 196
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 197
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasFrameShift()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 198
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getFrameShift()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 201
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasMelCepstrumSpec()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 202
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getMelCepstrumSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 205
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasLineSpectralPairSpec()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 206
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getLineSpectralPairSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 209
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasBandAperiodicitySpec()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 210
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getBandAperiodicitySpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 213
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasLogF0Spec()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 214
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getLogF0Spec()Lspeech/patts/SpeechParamSpec;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 217
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasVoicingSpec()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 218
    const/4 v2, 0x6

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getVoicingSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 221
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasSampleRate()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 222
    const/4 v2, 0x7

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getSampleRate()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 225
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasFrequencyWarping()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 226
    const/16 v2, 0x8

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getFrequencyWarping()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 229
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasUseLinearAp()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 230
    const/16 v2, 0x9

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getUseLinearAp()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 233
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasAmplitudeSpec()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 234
    const/16 v2, 0xa

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getAmplitudeSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 237
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasAperiodicitySpec()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 238
    const/16 v2, 0xb

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getAperiodicitySpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 241
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasMgcGammaStage()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 242
    const/16 v2, 0xc

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getMgcGammaStage()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 245
    :cond_c
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasDspProcType()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 246
    const/16 v2, 0xd

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getDspProcType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 249
    :cond_d
    iput v0, p0, Lspeech/patts/FrameStreamSpec;->memoizedSerializedSize:I

    move v1, v0

    .line 250
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto/16 :goto_0
.end method

.method public getUseLinearAp()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->useLinearAp_:Z

    return v0
.end method

.method public getVoicingSpec()Lspeech/patts/SpeechParamSpec;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec;->voicingSpec_:Lspeech/patts/SpeechParamSpec;

    return-object v0
.end method

.method public hasAmplitudeSpec()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasAmplitudeSpec:Z

    return v0
.end method

.method public hasAperiodicitySpec()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasAperiodicitySpec:Z

    return v0
.end method

.method public hasBandAperiodicitySpec()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasBandAperiodicitySpec:Z

    return v0
.end method

.method public hasDspProcType()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasDspProcType:Z

    return v0
.end method

.method public hasFrameShift()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasFrameShift:Z

    return v0
.end method

.method public hasFrequencyWarping()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasFrequencyWarping:Z

    return v0
.end method

.method public hasLineSpectralPairSpec()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasLineSpectralPairSpec:Z

    return v0
.end method

.method public hasLogF0Spec()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasLogF0Spec:Z

    return v0
.end method

.method public hasMelCepstrumSpec()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasMelCepstrumSpec:Z

    return v0
.end method

.method public hasMgcGammaStage()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasMgcGammaStage:Z

    return v0
.end method

.method public hasSampleRate()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasSampleRate:Z

    return v0
.end method

.method public hasUseLinearAp()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasUseLinearAp:Z

    return v0
.end method

.method public hasVoicingSpec()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lspeech/patts/FrameStreamSpec;->hasVoicingSpec:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 123
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasAmplitudeSpec()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getAmplitudeSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return v0

    .line 126
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasAperiodicitySpec()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 127
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getAperiodicitySpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasLogF0Spec()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 130
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getLogF0Spec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasVoicingSpec()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 133
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getVoicingSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasMelCepstrumSpec()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 136
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getMelCepstrumSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasLineSpectralPairSpec()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 139
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getLineSpectralPairSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasBandAperiodicitySpec()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 142
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getBandAperiodicitySpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    :cond_7
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->toBuilder()Lspeech/patts/FrameStreamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/FrameStreamSpec$Builder;
    .locals 1

    .prologue
    .line 325
    invoke-static {p0}, Lspeech/patts/FrameStreamSpec;->newBuilder(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/FrameStreamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getSerializedSize()I

    .line 150
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasFrameShift()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getFrameShift()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 153
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasMelCepstrumSpec()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getMelCepstrumSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 156
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasLineSpectralPairSpec()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getLineSpectralPairSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 159
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasBandAperiodicitySpec()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 160
    const/4 v0, 0x4

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getBandAperiodicitySpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 162
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasLogF0Spec()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 163
    const/4 v0, 0x5

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getLogF0Spec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 165
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasVoicingSpec()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 166
    const/4 v0, 0x6

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getVoicingSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 168
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasSampleRate()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 169
    const/4 v0, 0x7

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getSampleRate()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 171
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasFrequencyWarping()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 172
    const/16 v0, 0x8

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getFrequencyWarping()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 174
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasUseLinearAp()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 175
    const/16 v0, 0x9

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getUseLinearAp()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 177
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasAmplitudeSpec()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 178
    const/16 v0, 0xa

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getAmplitudeSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 180
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasAperiodicitySpec()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 181
    const/16 v0, 0xb

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getAperiodicitySpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 183
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasMgcGammaStage()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 184
    const/16 v0, 0xc

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getMgcGammaStage()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 186
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->hasDspProcType()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 187
    const/16 v0, 0xd

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec;->getDspProcType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 189
    :cond_c
    return-void
.end method
