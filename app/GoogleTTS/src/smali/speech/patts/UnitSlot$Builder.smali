.class public final Lspeech/patts/UnitSlot$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "UnitSlot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/UnitSlot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/UnitSlot;",
        "Lspeech/patts/UnitSlot$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/UnitSlot;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/UnitSlot$Builder;
    .locals 1

    .prologue
    .line 173
    invoke-static {}, Lspeech/patts/UnitSlot$Builder;->create()Lspeech/patts/UnitSlot$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/UnitSlot$Builder;
    .locals 3

    .prologue
    .line 182
    new-instance v0, Lspeech/patts/UnitSlot$Builder;

    invoke-direct {v0}, Lspeech/patts/UnitSlot$Builder;-><init>()V

    .line 183
    .local v0, "builder":Lspeech/patts/UnitSlot$Builder;
    new-instance v1, Lspeech/patts/UnitSlot;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/UnitSlot;-><init>(Lspeech/patts/UnitSlot$1;)V

    iput-object v1, v0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    .line 184
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0}, Lspeech/patts/UnitSlot$Builder;->build()Lspeech/patts/UnitSlot;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/UnitSlot;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/UnitSlot$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    iget-object v0, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    invoke-static {v0}, Lspeech/patts/UnitSlot$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 215
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/UnitSlot$Builder;->buildPartial()Lspeech/patts/UnitSlot;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/UnitSlot;
    .locals 3

    .prologue
    .line 228
    iget-object v1, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    if-nez v1, :cond_0

    .line 229
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 232
    :cond_0
    iget-object v0, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    .line 233
    .local v0, "returnMe":Lspeech/patts/UnitSlot;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    .line 234
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0}, Lspeech/patts/UnitSlot$Builder;->clone()Lspeech/patts/UnitSlot$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0}, Lspeech/patts/UnitSlot$Builder;->clone()Lspeech/patts/UnitSlot$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 173
    invoke-virtual {p0}, Lspeech/patts/UnitSlot$Builder;->clone()Lspeech/patts/UnitSlot$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/UnitSlot$Builder;
    .locals 2

    .prologue
    .line 201
    invoke-static {}, Lspeech/patts/UnitSlot$Builder;->create()Lspeech/patts/UnitSlot$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    invoke-virtual {v0, v1}, Lspeech/patts/UnitSlot$Builder;->mergeFrom(Lspeech/patts/UnitSlot;)Lspeech/patts/UnitSlot$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    invoke-virtual {v0}, Lspeech/patts/UnitSlot;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 173
    check-cast p1, Lspeech/patts/UnitSlot;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/UnitSlot$Builder;->mergeFrom(Lspeech/patts/UnitSlot;)Lspeech/patts/UnitSlot$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/UnitSlot;)Lspeech/patts/UnitSlot$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/UnitSlot;

    .prologue
    .line 238
    invoke-static {}, Lspeech/patts/UnitSlot;->getDefaultInstance()Lspeech/patts/UnitSlot;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-object p0

    .line 239
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/UnitSlot;->hasTargetCost()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 240
    invoke-virtual {p1}, Lspeech/patts/UnitSlot;->getTargetCost()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/UnitSlot$Builder;->setTargetCost(F)Lspeech/patts/UnitSlot$Builder;

    .line 242
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/UnitSlot;->hasRunningTotal()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 243
    invoke-virtual {p1}, Lspeech/patts/UnitSlot;->getRunningTotal()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/UnitSlot$Builder;->setRunningTotal(F)Lspeech/patts/UnitSlot$Builder;

    .line 245
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/UnitSlot;->hasBestBackIndex()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 246
    invoke-virtual {p1}, Lspeech/patts/UnitSlot;->getBestBackIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/UnitSlot$Builder;->setBestBackIndex(I)Lspeech/patts/UnitSlot$Builder;

    .line 248
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/UnitSlot;->hasUnitId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    invoke-virtual {p1}, Lspeech/patts/UnitSlot;->getUnitId()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/UnitSlot$Builder;->setUnitId(I)Lspeech/patts/UnitSlot$Builder;

    goto :goto_0
.end method

.method public setBestBackIndex(I)Lspeech/patts/UnitSlot$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 334
    iget-object v0, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/UnitSlot;->hasBestBackIndex:Z
    invoke-static {v0, v1}, Lspeech/patts/UnitSlot;->access$702(Lspeech/patts/UnitSlot;Z)Z

    .line 335
    iget-object v0, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    # setter for: Lspeech/patts/UnitSlot;->bestBackIndex_:I
    invoke-static {v0, p1}, Lspeech/patts/UnitSlot;->access$802(Lspeech/patts/UnitSlot;I)I

    .line 336
    return-object p0
.end method

.method public setRunningTotal(F)Lspeech/patts/UnitSlot$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 316
    iget-object v0, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/UnitSlot;->hasRunningTotal:Z
    invoke-static {v0, v1}, Lspeech/patts/UnitSlot;->access$502(Lspeech/patts/UnitSlot;Z)Z

    .line 317
    iget-object v0, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    # setter for: Lspeech/patts/UnitSlot;->runningTotal_:F
    invoke-static {v0, p1}, Lspeech/patts/UnitSlot;->access$602(Lspeech/patts/UnitSlot;F)F

    .line 318
    return-object p0
.end method

.method public setTargetCost(F)Lspeech/patts/UnitSlot$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 298
    iget-object v0, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/UnitSlot;->hasTargetCost:Z
    invoke-static {v0, v1}, Lspeech/patts/UnitSlot;->access$302(Lspeech/patts/UnitSlot;Z)Z

    .line 299
    iget-object v0, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    # setter for: Lspeech/patts/UnitSlot;->targetCost_:F
    invoke-static {v0, p1}, Lspeech/patts/UnitSlot;->access$402(Lspeech/patts/UnitSlot;F)F

    .line 300
    return-object p0
.end method

.method public setUnitId(I)Lspeech/patts/UnitSlot$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 352
    iget-object v0, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/UnitSlot;->hasUnitId:Z
    invoke-static {v0, v1}, Lspeech/patts/UnitSlot;->access$902(Lspeech/patts/UnitSlot;Z)Z

    .line 353
    iget-object v0, p0, Lspeech/patts/UnitSlot$Builder;->result:Lspeech/patts/UnitSlot;

    # setter for: Lspeech/patts/UnitSlot;->unitId_:I
    invoke-static {v0, p1}, Lspeech/patts/UnitSlot;->access$1002(Lspeech/patts/UnitSlot;I)I

    .line 354
    return-object p0
.end method
