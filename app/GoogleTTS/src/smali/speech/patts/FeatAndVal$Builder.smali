.class public final Lspeech/patts/FeatAndVal$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FeatAndVal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/FeatAndVal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/FeatAndVal;",
        "Lspeech/patts/FeatAndVal$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/FeatAndVal;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/FeatAndVal$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-static {}, Lspeech/patts/FeatAndVal$Builder;->create()Lspeech/patts/FeatAndVal$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/FeatAndVal$Builder;
    .locals 3

    .prologue
    .line 156
    new-instance v0, Lspeech/patts/FeatAndVal$Builder;

    invoke-direct {v0}, Lspeech/patts/FeatAndVal$Builder;-><init>()V

    .line 157
    .local v0, "builder":Lspeech/patts/FeatAndVal$Builder;
    new-instance v1, Lspeech/patts/FeatAndVal;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/FeatAndVal;-><init>(Lspeech/patts/FeatAndVal$1;)V

    iput-object v1, v0, Lspeech/patts/FeatAndVal$Builder;->result:Lspeech/patts/FeatAndVal;

    .line 158
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lspeech/patts/FeatAndVal$Builder;->build()Lspeech/patts/FeatAndVal;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/FeatAndVal;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lspeech/patts/FeatAndVal$Builder;->result:Lspeech/patts/FeatAndVal;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/FeatAndVal$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lspeech/patts/FeatAndVal$Builder;->result:Lspeech/patts/FeatAndVal;

    invoke-static {v0}, Lspeech/patts/FeatAndVal$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 189
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FeatAndVal$Builder;->buildPartial()Lspeech/patts/FeatAndVal;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/FeatAndVal;
    .locals 3

    .prologue
    .line 202
    iget-object v1, p0, Lspeech/patts/FeatAndVal$Builder;->result:Lspeech/patts/FeatAndVal;

    if-nez v1, :cond_0

    .line 203
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 206
    :cond_0
    iget-object v0, p0, Lspeech/patts/FeatAndVal$Builder;->result:Lspeech/patts/FeatAndVal;

    .line 207
    .local v0, "returnMe":Lspeech/patts/FeatAndVal;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/FeatAndVal$Builder;->result:Lspeech/patts/FeatAndVal;

    .line 208
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lspeech/patts/FeatAndVal$Builder;->clone()Lspeech/patts/FeatAndVal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lspeech/patts/FeatAndVal$Builder;->clone()Lspeech/patts/FeatAndVal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 147
    invoke-virtual {p0}, Lspeech/patts/FeatAndVal$Builder;->clone()Lspeech/patts/FeatAndVal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/FeatAndVal$Builder;
    .locals 2

    .prologue
    .line 175
    invoke-static {}, Lspeech/patts/FeatAndVal$Builder;->create()Lspeech/patts/FeatAndVal$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/FeatAndVal$Builder;->result:Lspeech/patts/FeatAndVal;

    invoke-virtual {v0, v1}, Lspeech/patts/FeatAndVal$Builder;->mergeFrom(Lspeech/patts/FeatAndVal;)Lspeech/patts/FeatAndVal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lspeech/patts/FeatAndVal$Builder;->result:Lspeech/patts/FeatAndVal;

    invoke-virtual {v0}, Lspeech/patts/FeatAndVal;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 147
    check-cast p1, Lspeech/patts/FeatAndVal;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/FeatAndVal$Builder;->mergeFrom(Lspeech/patts/FeatAndVal;)Lspeech/patts/FeatAndVal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/FeatAndVal;)Lspeech/patts/FeatAndVal$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/FeatAndVal;

    .prologue
    .line 212
    invoke-static {}, Lspeech/patts/FeatAndVal;->getDefaultInstance()Lspeech/patts/FeatAndVal;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-object p0

    .line 213
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/FeatAndVal;->hasFeat()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    invoke-virtual {p1}, Lspeech/patts/FeatAndVal;->getFeat()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FeatAndVal$Builder;->setFeat(Ljava/lang/String;)Lspeech/patts/FeatAndVal$Builder;

    .line 216
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/FeatAndVal;->hasWeight()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {p1}, Lspeech/patts/FeatAndVal;->getWeight()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/FeatAndVal$Builder;->setWeight(F)Lspeech/patts/FeatAndVal$Builder;

    goto :goto_0
.end method

.method public setFeat(Ljava/lang/String;)Lspeech/patts/FeatAndVal$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 258
    if-nez p1, :cond_0

    .line 259
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 261
    :cond_0
    iget-object v0, p0, Lspeech/patts/FeatAndVal$Builder;->result:Lspeech/patts/FeatAndVal;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FeatAndVal;->hasFeat:Z
    invoke-static {v0, v1}, Lspeech/patts/FeatAndVal;->access$302(Lspeech/patts/FeatAndVal;Z)Z

    .line 262
    iget-object v0, p0, Lspeech/patts/FeatAndVal$Builder;->result:Lspeech/patts/FeatAndVal;

    # setter for: Lspeech/patts/FeatAndVal;->feat_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/FeatAndVal;->access$402(Lspeech/patts/FeatAndVal;Ljava/lang/String;)Ljava/lang/String;

    .line 263
    return-object p0
.end method

.method public setWeight(F)Lspeech/patts/FeatAndVal$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 279
    iget-object v0, p0, Lspeech/patts/FeatAndVal$Builder;->result:Lspeech/patts/FeatAndVal;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FeatAndVal;->hasWeight:Z
    invoke-static {v0, v1}, Lspeech/patts/FeatAndVal;->access$502(Lspeech/patts/FeatAndVal;Z)Z

    .line 280
    iget-object v0, p0, Lspeech/patts/FeatAndVal$Builder;->result:Lspeech/patts/FeatAndVal;

    # setter for: Lspeech/patts/FeatAndVal;->weight_:F
    invoke-static {v0, p1}, Lspeech/patts/FeatAndVal;->access$602(Lspeech/patts/FeatAndVal;F)F

    .line 281
    return-object p0
.end method
