.class public final Lspeech/patts/StressDefinition$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "StressDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/StressDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/StressDefinition;",
        "Lspeech/patts/StressDefinition$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/StressDefinition;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/StressDefinition$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-static {}, Lspeech/patts/StressDefinition$Builder;->create()Lspeech/patts/StressDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/StressDefinition$Builder;
    .locals 3

    .prologue
    .line 156
    new-instance v0, Lspeech/patts/StressDefinition$Builder;

    invoke-direct {v0}, Lspeech/patts/StressDefinition$Builder;-><init>()V

    .line 157
    .local v0, "builder":Lspeech/patts/StressDefinition$Builder;
    new-instance v1, Lspeech/patts/StressDefinition;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/StressDefinition;-><init>(Lspeech/patts/StressDefinition$1;)V

    iput-object v1, v0, Lspeech/patts/StressDefinition$Builder;->result:Lspeech/patts/StressDefinition;

    .line 158
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lspeech/patts/StressDefinition$Builder;->build()Lspeech/patts/StressDefinition;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/StressDefinition;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lspeech/patts/StressDefinition$Builder;->result:Lspeech/patts/StressDefinition;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/StressDefinition$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lspeech/patts/StressDefinition$Builder;->result:Lspeech/patts/StressDefinition;

    invoke-static {v0}, Lspeech/patts/StressDefinition$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 189
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/StressDefinition$Builder;->buildPartial()Lspeech/patts/StressDefinition;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/StressDefinition;
    .locals 3

    .prologue
    .line 202
    iget-object v1, p0, Lspeech/patts/StressDefinition$Builder;->result:Lspeech/patts/StressDefinition;

    if-nez v1, :cond_0

    .line 203
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 206
    :cond_0
    iget-object v0, p0, Lspeech/patts/StressDefinition$Builder;->result:Lspeech/patts/StressDefinition;

    .line 207
    .local v0, "returnMe":Lspeech/patts/StressDefinition;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/StressDefinition$Builder;->result:Lspeech/patts/StressDefinition;

    .line 208
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lspeech/patts/StressDefinition$Builder;->clone()Lspeech/patts/StressDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lspeech/patts/StressDefinition$Builder;->clone()Lspeech/patts/StressDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 147
    invoke-virtual {p0}, Lspeech/patts/StressDefinition$Builder;->clone()Lspeech/patts/StressDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/StressDefinition$Builder;
    .locals 2

    .prologue
    .line 175
    invoke-static {}, Lspeech/patts/StressDefinition$Builder;->create()Lspeech/patts/StressDefinition$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/StressDefinition$Builder;->result:Lspeech/patts/StressDefinition;

    invoke-virtual {v0, v1}, Lspeech/patts/StressDefinition$Builder;->mergeFrom(Lspeech/patts/StressDefinition;)Lspeech/patts/StressDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lspeech/patts/StressDefinition$Builder;->result:Lspeech/patts/StressDefinition;

    invoke-virtual {v0}, Lspeech/patts/StressDefinition;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 147
    check-cast p1, Lspeech/patts/StressDefinition;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/StressDefinition$Builder;->mergeFrom(Lspeech/patts/StressDefinition;)Lspeech/patts/StressDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/StressDefinition;)Lspeech/patts/StressDefinition$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/StressDefinition;

    .prologue
    .line 212
    invoke-static {}, Lspeech/patts/StressDefinition;->getDefaultInstance()Lspeech/patts/StressDefinition;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-object p0

    .line 213
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/StressDefinition;->hasStress()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    invoke-virtual {p1}, Lspeech/patts/StressDefinition;->getStress()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/StressDefinition$Builder;->setStress(I)Lspeech/patts/StressDefinition$Builder;

    .line 216
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/StressDefinition;->hasVowels()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {p1}, Lspeech/patts/StressDefinition;->getVowels()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/StressDefinition$Builder;->setVowels(Ljava/lang/String;)Lspeech/patts/StressDefinition$Builder;

    goto :goto_0
.end method

.method public setStress(I)Lspeech/patts/StressDefinition$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 258
    iget-object v0, p0, Lspeech/patts/StressDefinition$Builder;->result:Lspeech/patts/StressDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/StressDefinition;->hasStress:Z
    invoke-static {v0, v1}, Lspeech/patts/StressDefinition;->access$302(Lspeech/patts/StressDefinition;Z)Z

    .line 259
    iget-object v0, p0, Lspeech/patts/StressDefinition$Builder;->result:Lspeech/patts/StressDefinition;

    # setter for: Lspeech/patts/StressDefinition;->stress_:I
    invoke-static {v0, p1}, Lspeech/patts/StressDefinition;->access$402(Lspeech/patts/StressDefinition;I)I

    .line 260
    return-object p0
.end method

.method public setVowels(Ljava/lang/String;)Lspeech/patts/StressDefinition$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 276
    if-nez p1, :cond_0

    .line 277
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 279
    :cond_0
    iget-object v0, p0, Lspeech/patts/StressDefinition$Builder;->result:Lspeech/patts/StressDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/StressDefinition;->hasVowels:Z
    invoke-static {v0, v1}, Lspeech/patts/StressDefinition;->access$502(Lspeech/patts/StressDefinition;Z)Z

    .line 280
    iget-object v0, p0, Lspeech/patts/StressDefinition$Builder;->result:Lspeech/patts/StressDefinition;

    # setter for: Lspeech/patts/StressDefinition;->vowels_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/StressDefinition;->access$602(Lspeech/patts/StressDefinition;Ljava/lang/String;)Ljava/lang/String;

    .line 281
    return-object p0
.end method
