.class public final Lspeech/patts/Unit2$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Unit2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Unit2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/Unit2;",
        "Lspeech/patts/Unit2$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/Unit2;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 272
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/Unit2$Builder;
    .locals 1

    .prologue
    .line 266
    invoke-static {}, Lspeech/patts/Unit2$Builder;->create()Lspeech/patts/Unit2$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/Unit2$Builder;
    .locals 3

    .prologue
    .line 275
    new-instance v0, Lspeech/patts/Unit2$Builder;

    invoke-direct {v0}, Lspeech/patts/Unit2$Builder;-><init>()V

    .line 276
    .local v0, "builder":Lspeech/patts/Unit2$Builder;
    new-instance v1, Lspeech/patts/Unit2;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/Unit2;-><init>(Lspeech/patts/Unit2$1;)V

    iput-object v1, v0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    .line 277
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 266
    invoke-virtual {p0}, Lspeech/patts/Unit2$Builder;->build()Lspeech/patts/Unit2;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/Unit2;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/Unit2$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 306
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    invoke-static {v0}, Lspeech/patts/Unit2$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 308
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Unit2$Builder;->buildPartial()Lspeech/patts/Unit2;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/Unit2;
    .locals 3

    .prologue
    .line 321
    iget-object v1, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    if-nez v1, :cond_0

    .line 322
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 325
    :cond_0
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    .line 326
    .local v0, "returnMe":Lspeech/patts/Unit2;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    .line 327
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 266
    invoke-virtual {p0}, Lspeech/patts/Unit2$Builder;->clone()Lspeech/patts/Unit2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 266
    invoke-virtual {p0}, Lspeech/patts/Unit2$Builder;->clone()Lspeech/patts/Unit2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 266
    invoke-virtual {p0}, Lspeech/patts/Unit2$Builder;->clone()Lspeech/patts/Unit2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/Unit2$Builder;
    .locals 2

    .prologue
    .line 294
    invoke-static {}, Lspeech/patts/Unit2$Builder;->create()Lspeech/patts/Unit2$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    invoke-virtual {v0, v1}, Lspeech/patts/Unit2$Builder;->mergeFrom(Lspeech/patts/Unit2;)Lspeech/patts/Unit2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    invoke-virtual {v0}, Lspeech/patts/Unit2;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 266
    check-cast p1, Lspeech/patts/Unit2;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/Unit2$Builder;->mergeFrom(Lspeech/patts/Unit2;)Lspeech/patts/Unit2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/Unit2;)Lspeech/patts/Unit2$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/Unit2;

    .prologue
    .line 331
    invoke-static {}, Lspeech/patts/Unit2;->getDefaultInstance()Lspeech/patts/Unit2;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-object p0

    .line 332
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/Unit2;->hasId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 333
    invoke-virtual {p1}, Lspeech/patts/Unit2;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Unit2$Builder;->setId(I)Lspeech/patts/Unit2$Builder;

    .line 335
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/Unit2;->hasStartPm()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 336
    invoke-virtual {p1}, Lspeech/patts/Unit2;->getStartPm()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Unit2$Builder;->setStartPm(I)Lspeech/patts/Unit2$Builder;

    .line 338
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/Unit2;->hasNumPmToMid()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 339
    invoke-virtual {p1}, Lspeech/patts/Unit2;->getNumPmToMid()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Unit2$Builder;->setNumPmToMid(I)Lspeech/patts/Unit2$Builder;

    .line 341
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/Unit2;->hasNumPmToEnd()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 342
    invoke-virtual {p1}, Lspeech/patts/Unit2;->getNumPmToEnd()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Unit2$Builder;->setNumPmToEnd(I)Lspeech/patts/Unit2$Builder;

    .line 344
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/Unit2;->hasLeftJoin()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 345
    invoke-virtual {p1}, Lspeech/patts/Unit2;->getLeftJoin()Lspeech/patts/FeatureData;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Unit2$Builder;->mergeLeftJoin(Lspeech/patts/FeatureData;)Lspeech/patts/Unit2$Builder;

    .line 347
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/Unit2;->hasRightJoin()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 348
    invoke-virtual {p1}, Lspeech/patts/Unit2;->getRightJoin()Lspeech/patts/FeatureData;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Unit2$Builder;->mergeRightJoin(Lspeech/patts/FeatureData;)Lspeech/patts/Unit2$Builder;

    .line 350
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/Unit2;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 351
    invoke-virtual {p1}, Lspeech/patts/Unit2;->getTarget()Lspeech/patts/FeatureData;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Unit2$Builder;->mergeTarget(Lspeech/patts/FeatureData;)Lspeech/patts/Unit2$Builder;

    .line 353
    :cond_8
    invoke-virtual {p1}, Lspeech/patts/Unit2;->hasStart()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 354
    invoke-virtual {p1}, Lspeech/patts/Unit2;->getStart()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Unit2$Builder;->setStart(F)Lspeech/patts/Unit2$Builder;

    .line 356
    :cond_9
    invoke-virtual {p1}, Lspeech/patts/Unit2;->hasEnd()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 357
    invoke-virtual {p1}, Lspeech/patts/Unit2;->getEnd()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Unit2$Builder;->setEnd(F)Lspeech/patts/Unit2$Builder;

    .line 359
    :cond_a
    invoke-virtual {p1}, Lspeech/patts/Unit2;->hasFilename()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    invoke-virtual {p1}, Lspeech/patts/Unit2;->getFilename()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Unit2$Builder;->setFilename(Ljava/lang/String;)Lspeech/patts/Unit2$Builder;

    goto/16 :goto_0
.end method

.method public mergeLeftJoin(Lspeech/patts/FeatureData;)Lspeech/patts/Unit2$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/FeatureData;

    .prologue
    .line 533
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    invoke-virtual {v0}, Lspeech/patts/Unit2;->hasLeftJoin()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # getter for: Lspeech/patts/Unit2;->leftJoin_:Lspeech/patts/FeatureData;
    invoke-static {v0}, Lspeech/patts/Unit2;->access$1200(Lspeech/patts/Unit2;)Lspeech/patts/FeatureData;

    move-result-object v0

    invoke-static {}, Lspeech/patts/FeatureData;->getDefaultInstance()Lspeech/patts/FeatureData;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 535
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    iget-object v1, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # getter for: Lspeech/patts/Unit2;->leftJoin_:Lspeech/patts/FeatureData;
    invoke-static {v1}, Lspeech/patts/Unit2;->access$1200(Lspeech/patts/Unit2;)Lspeech/patts/FeatureData;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/FeatureData;->newBuilder(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/FeatureData$Builder;->mergeFrom(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/FeatureData$Builder;->buildPartial()Lspeech/patts/FeatureData;

    move-result-object v1

    # setter for: Lspeech/patts/Unit2;->leftJoin_:Lspeech/patts/FeatureData;
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$1202(Lspeech/patts/Unit2;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;

    .line 540
    :goto_0
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Unit2;->hasLeftJoin:Z
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$1102(Lspeech/patts/Unit2;Z)Z

    .line 541
    return-object p0

    .line 538
    :cond_0
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # setter for: Lspeech/patts/Unit2;->leftJoin_:Lspeech/patts/FeatureData;
    invoke-static {v0, p1}, Lspeech/patts/Unit2;->access$1202(Lspeech/patts/Unit2;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;

    goto :goto_0
.end method

.method public mergeRightJoin(Lspeech/patts/FeatureData;)Lspeech/patts/Unit2$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/FeatureData;

    .prologue
    .line 570
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    invoke-virtual {v0}, Lspeech/patts/Unit2;->hasRightJoin()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # getter for: Lspeech/patts/Unit2;->rightJoin_:Lspeech/patts/FeatureData;
    invoke-static {v0}, Lspeech/patts/Unit2;->access$1400(Lspeech/patts/Unit2;)Lspeech/patts/FeatureData;

    move-result-object v0

    invoke-static {}, Lspeech/patts/FeatureData;->getDefaultInstance()Lspeech/patts/FeatureData;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 572
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    iget-object v1, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # getter for: Lspeech/patts/Unit2;->rightJoin_:Lspeech/patts/FeatureData;
    invoke-static {v1}, Lspeech/patts/Unit2;->access$1400(Lspeech/patts/Unit2;)Lspeech/patts/FeatureData;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/FeatureData;->newBuilder(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/FeatureData$Builder;->mergeFrom(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/FeatureData$Builder;->buildPartial()Lspeech/patts/FeatureData;

    move-result-object v1

    # setter for: Lspeech/patts/Unit2;->rightJoin_:Lspeech/patts/FeatureData;
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$1402(Lspeech/patts/Unit2;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;

    .line 577
    :goto_0
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Unit2;->hasRightJoin:Z
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$1302(Lspeech/patts/Unit2;Z)Z

    .line 578
    return-object p0

    .line 575
    :cond_0
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # setter for: Lspeech/patts/Unit2;->rightJoin_:Lspeech/patts/FeatureData;
    invoke-static {v0, p1}, Lspeech/patts/Unit2;->access$1402(Lspeech/patts/Unit2;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;

    goto :goto_0
.end method

.method public mergeTarget(Lspeech/patts/FeatureData;)Lspeech/patts/Unit2$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/FeatureData;

    .prologue
    .line 607
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    invoke-virtual {v0}, Lspeech/patts/Unit2;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # getter for: Lspeech/patts/Unit2;->target_:Lspeech/patts/FeatureData;
    invoke-static {v0}, Lspeech/patts/Unit2;->access$1600(Lspeech/patts/Unit2;)Lspeech/patts/FeatureData;

    move-result-object v0

    invoke-static {}, Lspeech/patts/FeatureData;->getDefaultInstance()Lspeech/patts/FeatureData;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 609
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    iget-object v1, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # getter for: Lspeech/patts/Unit2;->target_:Lspeech/patts/FeatureData;
    invoke-static {v1}, Lspeech/patts/Unit2;->access$1600(Lspeech/patts/Unit2;)Lspeech/patts/FeatureData;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/FeatureData;->newBuilder(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/FeatureData$Builder;->mergeFrom(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/FeatureData$Builder;->buildPartial()Lspeech/patts/FeatureData;

    move-result-object v1

    # setter for: Lspeech/patts/Unit2;->target_:Lspeech/patts/FeatureData;
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$1602(Lspeech/patts/Unit2;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;

    .line 614
    :goto_0
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Unit2;->hasTarget:Z
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$1502(Lspeech/patts/Unit2;Z)Z

    .line 615
    return-object p0

    .line 612
    :cond_0
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # setter for: Lspeech/patts/Unit2;->target_:Lspeech/patts/FeatureData;
    invoke-static {v0, p1}, Lspeech/patts/Unit2;->access$1602(Lspeech/patts/Unit2;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;

    goto :goto_0
.end method

.method public setEnd(F)Lspeech/patts/Unit2$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 649
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Unit2;->hasEnd:Z
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$1902(Lspeech/patts/Unit2;Z)Z

    .line 650
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # setter for: Lspeech/patts/Unit2;->end_:F
    invoke-static {v0, p1}, Lspeech/patts/Unit2;->access$2002(Lspeech/patts/Unit2;F)F

    .line 651
    return-object p0
.end method

.method public setFilename(Ljava/lang/String;)Lspeech/patts/Unit2$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 667
    if-nez p1, :cond_0

    .line 668
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 670
    :cond_0
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Unit2;->hasFilename:Z
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$2102(Lspeech/patts/Unit2;Z)Z

    .line 671
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # setter for: Lspeech/patts/Unit2;->filename_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Unit2;->access$2202(Lspeech/patts/Unit2;Ljava/lang/String;)Ljava/lang/String;

    .line 672
    return-object p0
.end method

.method public setId(I)Lspeech/patts/Unit2$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 448
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Unit2;->hasId:Z
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$302(Lspeech/patts/Unit2;Z)Z

    .line 449
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # setter for: Lspeech/patts/Unit2;->id_:I
    invoke-static {v0, p1}, Lspeech/patts/Unit2;->access$402(Lspeech/patts/Unit2;I)I

    .line 450
    return-object p0
.end method

.method public setNumPmToEnd(I)Lspeech/patts/Unit2$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 502
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Unit2;->hasNumPmToEnd:Z
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$902(Lspeech/patts/Unit2;Z)Z

    .line 503
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # setter for: Lspeech/patts/Unit2;->numPmToEnd_:I
    invoke-static {v0, p1}, Lspeech/patts/Unit2;->access$1002(Lspeech/patts/Unit2;I)I

    .line 504
    return-object p0
.end method

.method public setNumPmToMid(I)Lspeech/patts/Unit2$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 484
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Unit2;->hasNumPmToMid:Z
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$702(Lspeech/patts/Unit2;Z)Z

    .line 485
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # setter for: Lspeech/patts/Unit2;->numPmToMid_:I
    invoke-static {v0, p1}, Lspeech/patts/Unit2;->access$802(Lspeech/patts/Unit2;I)I

    .line 486
    return-object p0
.end method

.method public setStart(F)Lspeech/patts/Unit2$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 631
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Unit2;->hasStart:Z
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$1702(Lspeech/patts/Unit2;Z)Z

    .line 632
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # setter for: Lspeech/patts/Unit2;->start_:F
    invoke-static {v0, p1}, Lspeech/patts/Unit2;->access$1802(Lspeech/patts/Unit2;F)F

    .line 633
    return-object p0
.end method

.method public setStartPm(I)Lspeech/patts/Unit2$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 466
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Unit2;->hasStartPm:Z
    invoke-static {v0, v1}, Lspeech/patts/Unit2;->access$502(Lspeech/patts/Unit2;Z)Z

    .line 467
    iget-object v0, p0, Lspeech/patts/Unit2$Builder;->result:Lspeech/patts/Unit2;

    # setter for: Lspeech/patts/Unit2;->startPm_:I
    invoke-static {v0, p1}, Lspeech/patts/Unit2;->access$602(Lspeech/patts/Unit2;I)I

    .line 468
    return-object p0
.end method
