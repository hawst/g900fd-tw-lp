.class public final enum Lspeech/patts/RomanNumeralContextWord$SemioticClass;
.super Ljava/lang/Enum;
.source "RomanNumeralContextWord.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/RomanNumeralContextWord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SemioticClass"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/RomanNumeralContextWord$SemioticClass;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/RomanNumeralContextWord$SemioticClass;

.field public static final enum CARDINAL:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

.field public static final enum CARDINAL_FEM:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

.field public static final enum DEFINITE_ORDINAL:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

.field public static final enum ORDINAL:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

.field public static final enum ORDINAL_FEM:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

.field public static final enum ORDINAL_MAS:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

.field public static final enum YEAR:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/RomanNumeralContextWord$SemioticClass;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 24
    new-instance v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    const-string v1, "CARDINAL"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v5}, Lspeech/patts/RomanNumeralContextWord$SemioticClass;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->CARDINAL:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    .line 25
    new-instance v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    const-string v1, "ORDINAL"

    invoke-direct {v0, v1, v5, v5, v6}, Lspeech/patts/RomanNumeralContextWord$SemioticClass;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->ORDINAL:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    .line 26
    new-instance v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    const-string v1, "YEAR"

    invoke-direct {v0, v1, v6, v6, v8}, Lspeech/patts/RomanNumeralContextWord$SemioticClass;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->YEAR:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    .line 27
    new-instance v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    const-string v1, "DEFINITE_ORDINAL"

    invoke-direct {v0, v1, v7, v7, v7}, Lspeech/patts/RomanNumeralContextWord$SemioticClass;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->DEFINITE_ORDINAL:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    .line 28
    new-instance v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    const-string v1, "ORDINAL_MAS"

    invoke-direct {v0, v1, v8, v8, v9}, Lspeech/patts/RomanNumeralContextWord$SemioticClass;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->ORDINAL_MAS:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    .line 29
    new-instance v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    const-string v1, "ORDINAL_FEM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v9, v9, v2}, Lspeech/patts/RomanNumeralContextWord$SemioticClass;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->ORDINAL_FEM:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    .line 30
    new-instance v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    const-string v1, "CARDINAL_FEM"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/RomanNumeralContextWord$SemioticClass;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->CARDINAL_FEM:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    .line 22
    const/4 v0, 0x7

    new-array v0, v0, [Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    const/4 v1, 0x0

    sget-object v2, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->CARDINAL:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    aput-object v2, v0, v1

    sget-object v1, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->ORDINAL:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    aput-object v1, v0, v5

    sget-object v1, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->YEAR:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    aput-object v1, v0, v6

    sget-object v1, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->DEFINITE_ORDINAL:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    aput-object v1, v0, v7

    sget-object v1, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->ORDINAL_MAS:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    aput-object v1, v0, v8

    sget-object v1, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->ORDINAL_FEM:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    aput-object v1, v0, v9

    const/4 v1, 0x6

    sget-object v2, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->CARDINAL_FEM:Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    aput-object v2, v0, v1

    sput-object v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->$VALUES:[Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    .line 54
    new-instance v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass$1;

    invoke-direct {v0}, Lspeech/patts/RomanNumeralContextWord$SemioticClass$1;-><init>()V

    sput-object v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    iput p3, p0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->index:I

    .line 65
    iput p4, p0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->value:I

    .line 66
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/RomanNumeralContextWord$SemioticClass;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    return-object v0
.end method

.method public static values()[Lspeech/patts/RomanNumeralContextWord$SemioticClass;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->$VALUES:[Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    invoke-virtual {v0}, [Lspeech/patts/RomanNumeralContextWord$SemioticClass;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lspeech/patts/RomanNumeralContextWord$SemioticClass;->value:I

    return v0
.end method
