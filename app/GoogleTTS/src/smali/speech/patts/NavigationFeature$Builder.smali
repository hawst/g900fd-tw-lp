.class public final Lspeech/patts/NavigationFeature$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "NavigationFeature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/NavigationFeature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/NavigationFeature;",
        "Lspeech/patts/NavigationFeature$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/NavigationFeature;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 250
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/NavigationFeature$Builder;
    .locals 1

    .prologue
    .line 244
    invoke-static {}, Lspeech/patts/NavigationFeature$Builder;->create()Lspeech/patts/NavigationFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/NavigationFeature$Builder;
    .locals 3

    .prologue
    .line 253
    new-instance v0, Lspeech/patts/NavigationFeature$Builder;

    invoke-direct {v0}, Lspeech/patts/NavigationFeature$Builder;-><init>()V

    .line 254
    .local v0, "builder":Lspeech/patts/NavigationFeature$Builder;
    new-instance v1, Lspeech/patts/NavigationFeature;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/NavigationFeature;-><init>(Lspeech/patts/NavigationFeature$1;)V

    iput-object v1, v0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    .line 255
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 244
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature$Builder;->build()Lspeech/patts/NavigationFeature;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/NavigationFeature;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/NavigationFeature$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    invoke-static {v0}, Lspeech/patts/NavigationFeature$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 286
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature$Builder;->buildPartial()Lspeech/patts/NavigationFeature;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/NavigationFeature;
    .locals 3

    .prologue
    .line 299
    iget-object v1, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    if-nez v1, :cond_0

    .line 300
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 303
    :cond_0
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    .line 304
    .local v0, "returnMe":Lspeech/patts/NavigationFeature;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    .line 305
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 244
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature$Builder;->clone()Lspeech/patts/NavigationFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 244
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature$Builder;->clone()Lspeech/patts/NavigationFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 244
    invoke-virtual {p0}, Lspeech/patts/NavigationFeature$Builder;->clone()Lspeech/patts/NavigationFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/NavigationFeature$Builder;
    .locals 2

    .prologue
    .line 272
    invoke-static {}, Lspeech/patts/NavigationFeature$Builder;->create()Lspeech/patts/NavigationFeature$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    invoke-virtual {v0, v1}, Lspeech/patts/NavigationFeature$Builder;->mergeFrom(Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    invoke-virtual {v0}, Lspeech/patts/NavigationFeature;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 244
    check-cast p1, Lspeech/patts/NavigationFeature;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/NavigationFeature$Builder;->mergeFrom(Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/NavigationFeature;

    .prologue
    .line 309
    invoke-static {}, Lspeech/patts/NavigationFeature;->getDefaultInstance()Lspeech/patts/NavigationFeature;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-object p0

    .line 310
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/NavigationFeature;->hasFeature()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 311
    invoke-virtual {p1}, Lspeech/patts/NavigationFeature;->getFeature()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/NavigationFeature$Builder;->setFeature(Ljava/lang/String;)Lspeech/patts/NavigationFeature$Builder;

    .line 313
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/NavigationFeature;->hasStream()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 314
    invoke-virtual {p1}, Lspeech/patts/NavigationFeature;->getStream()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/NavigationFeature$Builder;->setStream(Ljava/lang/String;)Lspeech/patts/NavigationFeature$Builder;

    .line 316
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/NavigationFeature;->hasPosition()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 317
    invoke-virtual {p1}, Lspeech/patts/NavigationFeature;->getPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/NavigationFeature$Builder;->setPosition(I)Lspeech/patts/NavigationFeature$Builder;

    .line 319
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/NavigationFeature;->hasSPosition()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 320
    invoke-virtual {p1}, Lspeech/patts/NavigationFeature;->getSPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/NavigationFeature$Builder;->setSPosition(I)Lspeech/patts/NavigationFeature$Builder;

    .line 322
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/NavigationFeature;->hasDependencyParent()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 323
    invoke-virtual {p1}, Lspeech/patts/NavigationFeature;->getDependencyParent()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/NavigationFeature$Builder;->setDependencyParent(I)Lspeech/patts/NavigationFeature$Builder;

    .line 325
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/NavigationFeature;->hasDiphoneDirection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    invoke-virtual {p1}, Lspeech/patts/NavigationFeature;->getDiphoneDirection()Lspeech/patts/NavigationFeature$DiphoneDirection;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/NavigationFeature$Builder;->setDiphoneDirection(Lspeech/patts/NavigationFeature$DiphoneDirection;)Lspeech/patts/NavigationFeature$Builder;

    goto :goto_0
.end method

.method public setDependencyParent(I)Lspeech/patts/NavigationFeature$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 465
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/NavigationFeature;->hasDependencyParent:Z
    invoke-static {v0, v1}, Lspeech/patts/NavigationFeature;->access$1102(Lspeech/patts/NavigationFeature;Z)Z

    .line 466
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    # setter for: Lspeech/patts/NavigationFeature;->dependencyParent_:I
    invoke-static {v0, p1}, Lspeech/patts/NavigationFeature;->access$1202(Lspeech/patts/NavigationFeature;I)I

    .line 467
    return-object p0
.end method

.method public setDiphoneDirection(Lspeech/patts/NavigationFeature$DiphoneDirection;)Lspeech/patts/NavigationFeature$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/NavigationFeature$DiphoneDirection;

    .prologue
    .line 483
    if-nez p1, :cond_0

    .line 484
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 486
    :cond_0
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/NavigationFeature;->hasDiphoneDirection:Z
    invoke-static {v0, v1}, Lspeech/patts/NavigationFeature;->access$1302(Lspeech/patts/NavigationFeature;Z)Z

    .line 487
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    # setter for: Lspeech/patts/NavigationFeature;->diphoneDirection_:Lspeech/patts/NavigationFeature$DiphoneDirection;
    invoke-static {v0, p1}, Lspeech/patts/NavigationFeature;->access$1402(Lspeech/patts/NavigationFeature;Lspeech/patts/NavigationFeature$DiphoneDirection;)Lspeech/patts/NavigationFeature$DiphoneDirection;

    .line 488
    return-object p0
.end method

.method public setFeature(Ljava/lang/String;)Lspeech/patts/NavigationFeature$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 387
    if-nez p1, :cond_0

    .line 388
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 390
    :cond_0
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/NavigationFeature;->hasFeature:Z
    invoke-static {v0, v1}, Lspeech/patts/NavigationFeature;->access$302(Lspeech/patts/NavigationFeature;Z)Z

    .line 391
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    # setter for: Lspeech/patts/NavigationFeature;->feature_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/NavigationFeature;->access$402(Lspeech/patts/NavigationFeature;Ljava/lang/String;)Ljava/lang/String;

    .line 392
    return-object p0
.end method

.method public setPosition(I)Lspeech/patts/NavigationFeature$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 429
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/NavigationFeature;->hasPosition:Z
    invoke-static {v0, v1}, Lspeech/patts/NavigationFeature;->access$702(Lspeech/patts/NavigationFeature;Z)Z

    .line 430
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    # setter for: Lspeech/patts/NavigationFeature;->position_:I
    invoke-static {v0, p1}, Lspeech/patts/NavigationFeature;->access$802(Lspeech/patts/NavigationFeature;I)I

    .line 431
    return-object p0
.end method

.method public setSPosition(I)Lspeech/patts/NavigationFeature$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 447
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/NavigationFeature;->hasSPosition:Z
    invoke-static {v0, v1}, Lspeech/patts/NavigationFeature;->access$902(Lspeech/patts/NavigationFeature;Z)Z

    .line 448
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    # setter for: Lspeech/patts/NavigationFeature;->sPosition_:I
    invoke-static {v0, p1}, Lspeech/patts/NavigationFeature;->access$1002(Lspeech/patts/NavigationFeature;I)I

    .line 449
    return-object p0
.end method

.method public setStream(Ljava/lang/String;)Lspeech/patts/NavigationFeature$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 408
    if-nez p1, :cond_0

    .line 409
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 411
    :cond_0
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/NavigationFeature;->hasStream:Z
    invoke-static {v0, v1}, Lspeech/patts/NavigationFeature;->access$502(Lspeech/patts/NavigationFeature;Z)Z

    .line 412
    iget-object v0, p0, Lspeech/patts/NavigationFeature$Builder;->result:Lspeech/patts/NavigationFeature;

    # setter for: Lspeech/patts/NavigationFeature;->stream_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/NavigationFeature;->access$602(Lspeech/patts/NavigationFeature;Ljava/lang/String;)Ljava/lang/String;

    .line 413
    return-object p0
.end method
