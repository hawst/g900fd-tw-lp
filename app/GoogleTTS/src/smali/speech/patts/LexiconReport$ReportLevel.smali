.class public final enum Lspeech/patts/LexiconReport$ReportLevel;
.super Ljava/lang/Enum;
.source "LexiconReport.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/LexiconReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReportLevel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/LexiconReport$ReportLevel;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/LexiconReport$ReportLevel;

.field public static final enum ERROR:Lspeech/patts/LexiconReport$ReportLevel;

.field public static final enum INFO:Lspeech/patts/LexiconReport$ReportLevel;

.field public static final enum WARNING:Lspeech/patts/LexiconReport$ReportLevel;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/LexiconReport$ReportLevel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 24
    new-instance v0, Lspeech/patts/LexiconReport$ReportLevel;

    const-string v1, "INFO"

    invoke-direct {v0, v1, v4, v4, v2}, Lspeech/patts/LexiconReport$ReportLevel;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/LexiconReport$ReportLevel;->INFO:Lspeech/patts/LexiconReport$ReportLevel;

    .line 25
    new-instance v0, Lspeech/patts/LexiconReport$ReportLevel;

    const-string v1, "WARNING"

    invoke-direct {v0, v1, v2, v2, v3}, Lspeech/patts/LexiconReport$ReportLevel;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/LexiconReport$ReportLevel;->WARNING:Lspeech/patts/LexiconReport$ReportLevel;

    .line 26
    new-instance v0, Lspeech/patts/LexiconReport$ReportLevel;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3, v3, v5}, Lspeech/patts/LexiconReport$ReportLevel;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/LexiconReport$ReportLevel;->ERROR:Lspeech/patts/LexiconReport$ReportLevel;

    .line 22
    new-array v0, v5, [Lspeech/patts/LexiconReport$ReportLevel;

    sget-object v1, Lspeech/patts/LexiconReport$ReportLevel;->INFO:Lspeech/patts/LexiconReport$ReportLevel;

    aput-object v1, v0, v4

    sget-object v1, Lspeech/patts/LexiconReport$ReportLevel;->WARNING:Lspeech/patts/LexiconReport$ReportLevel;

    aput-object v1, v0, v2

    sget-object v1, Lspeech/patts/LexiconReport$ReportLevel;->ERROR:Lspeech/patts/LexiconReport$ReportLevel;

    aput-object v1, v0, v3

    sput-object v0, Lspeech/patts/LexiconReport$ReportLevel;->$VALUES:[Lspeech/patts/LexiconReport$ReportLevel;

    .line 46
    new-instance v0, Lspeech/patts/LexiconReport$ReportLevel$1;

    invoke-direct {v0}, Lspeech/patts/LexiconReport$ReportLevel$1;-><init>()V

    sput-object v0, Lspeech/patts/LexiconReport$ReportLevel;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput p3, p0, Lspeech/patts/LexiconReport$ReportLevel;->index:I

    .line 57
    iput p4, p0, Lspeech/patts/LexiconReport$ReportLevel;->value:I

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/LexiconReport$ReportLevel;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lspeech/patts/LexiconReport$ReportLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/LexiconReport$ReportLevel;

    return-object v0
.end method

.method public static values()[Lspeech/patts/LexiconReport$ReportLevel;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lspeech/patts/LexiconReport$ReportLevel;->$VALUES:[Lspeech/patts/LexiconReport$ReportLevel;

    invoke-virtual {v0}, [Lspeech/patts/LexiconReport$ReportLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/LexiconReport$ReportLevel;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lspeech/patts/LexiconReport$ReportLevel;->value:I

    return v0
.end method
