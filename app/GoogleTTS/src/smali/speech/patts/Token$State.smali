.class public final enum Lspeech/patts/Token$State;
.super Ljava/lang/Enum;
.source "Token.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Token;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/Token$State;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/Token$State;

.field public static final enum CLASSIFIED:Lspeech/patts/Token$State;

.field public static final enum PARSED:Lspeech/patts/Token$State;

.field public static final enum RAW:Lspeech/patts/Token$State;

.field public static final enum VERBALIZED:Lspeech/patts/Token$State;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/Token$State;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 71
    new-instance v0, Lspeech/patts/Token$State;

    const-string v1, "RAW"

    invoke-direct {v0, v1, v5, v5, v2}, Lspeech/patts/Token$State;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$State;->RAW:Lspeech/patts/Token$State;

    .line 72
    new-instance v0, Lspeech/patts/Token$State;

    const-string v1, "CLASSIFIED"

    invoke-direct {v0, v1, v2, v2, v3}, Lspeech/patts/Token$State;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$State;->CLASSIFIED:Lspeech/patts/Token$State;

    .line 73
    new-instance v0, Lspeech/patts/Token$State;

    const-string v1, "PARSED"

    invoke-direct {v0, v1, v3, v3, v4}, Lspeech/patts/Token$State;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$State;->PARSED:Lspeech/patts/Token$State;

    .line 74
    new-instance v0, Lspeech/patts/Token$State;

    const-string v1, "VERBALIZED"

    invoke-direct {v0, v1, v4, v4, v6}, Lspeech/patts/Token$State;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/Token$State;->VERBALIZED:Lspeech/patts/Token$State;

    .line 69
    new-array v0, v6, [Lspeech/patts/Token$State;

    sget-object v1, Lspeech/patts/Token$State;->RAW:Lspeech/patts/Token$State;

    aput-object v1, v0, v5

    sget-object v1, Lspeech/patts/Token$State;->CLASSIFIED:Lspeech/patts/Token$State;

    aput-object v1, v0, v2

    sget-object v1, Lspeech/patts/Token$State;->PARSED:Lspeech/patts/Token$State;

    aput-object v1, v0, v3

    sget-object v1, Lspeech/patts/Token$State;->VERBALIZED:Lspeech/patts/Token$State;

    aput-object v1, v0, v4

    sput-object v0, Lspeech/patts/Token$State;->$VALUES:[Lspeech/patts/Token$State;

    .line 95
    new-instance v0, Lspeech/patts/Token$State$1;

    invoke-direct {v0}, Lspeech/patts/Token$State$1;-><init>()V

    sput-object v0, Lspeech/patts/Token$State;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 105
    iput p3, p0, Lspeech/patts/Token$State;->index:I

    .line 106
    iput p4, p0, Lspeech/patts/Token$State;->value:I

    .line 107
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/Token$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 69
    const-class v0, Lspeech/patts/Token$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/Token$State;

    return-object v0
.end method

.method public static values()[Lspeech/patts/Token$State;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lspeech/patts/Token$State;->$VALUES:[Lspeech/patts/Token$State;

    invoke-virtual {v0}, [Lspeech/patts/Token$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/Token$State;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lspeech/patts/Token$State;->value:I

    return v0
.end method
