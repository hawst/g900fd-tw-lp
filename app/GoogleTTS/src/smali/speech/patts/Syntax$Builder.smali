.class public final Lspeech/patts/Syntax$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Syntax.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Syntax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/Syntax;",
        "Lspeech/patts/Syntax$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/Syntax;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/Syntax$Builder;
    .locals 1

    .prologue
    .line 226
    invoke-static {}, Lspeech/patts/Syntax$Builder;->create()Lspeech/patts/Syntax$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/Syntax$Builder;
    .locals 3

    .prologue
    .line 235
    new-instance v0, Lspeech/patts/Syntax$Builder;

    invoke-direct {v0}, Lspeech/patts/Syntax$Builder;-><init>()V

    .line 236
    .local v0, "builder":Lspeech/patts/Syntax$Builder;
    new-instance v1, Lspeech/patts/Syntax;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/Syntax;-><init>(Lspeech/patts/Syntax$1;)V

    iput-object v1, v0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    .line 237
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lspeech/patts/Syntax$Builder;->build()Lspeech/patts/Syntax;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/Syntax;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/Syntax$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    invoke-static {v0}, Lspeech/patts/Syntax$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 268
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Syntax$Builder;->buildPartial()Lspeech/patts/Syntax;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/Syntax;
    .locals 3

    .prologue
    .line 281
    iget-object v1, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    if-nez v1, :cond_0

    .line 282
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 285
    :cond_0
    iget-object v1, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    # getter for: Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Syntax;->access$300(Lspeech/patts/Syntax;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 286
    iget-object v1, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    iget-object v2, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    # getter for: Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Syntax;->access$300(Lspeech/patts/Syntax;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Syntax;->access$302(Lspeech/patts/Syntax;Ljava/util/List;)Ljava/util/List;

    .line 289
    :cond_1
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    .line 290
    .local v0, "returnMe":Lspeech/patts/Syntax;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    .line 291
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lspeech/patts/Syntax$Builder;->clone()Lspeech/patts/Syntax$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lspeech/patts/Syntax$Builder;->clone()Lspeech/patts/Syntax$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 226
    invoke-virtual {p0}, Lspeech/patts/Syntax$Builder;->clone()Lspeech/patts/Syntax$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/Syntax$Builder;
    .locals 2

    .prologue
    .line 254
    invoke-static {}, Lspeech/patts/Syntax$Builder;->create()Lspeech/patts/Syntax$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    invoke-virtual {v0, v1}, Lspeech/patts/Syntax$Builder;->mergeFrom(Lspeech/patts/Syntax;)Lspeech/patts/Syntax$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    invoke-virtual {v0}, Lspeech/patts/Syntax;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeDependencyData(Lspeech/patts/DependencyData;)Lspeech/patts/Syntax$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/DependencyData;

    .prologue
    .line 521
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    invoke-virtual {v0}, Lspeech/patts/Syntax;->hasDependencyData()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    # getter for: Lspeech/patts/Syntax;->dependencyData_:Lspeech/patts/DependencyData;
    invoke-static {v0}, Lspeech/patts/Syntax;->access$1300(Lspeech/patts/Syntax;)Lspeech/patts/DependencyData;

    move-result-object v0

    invoke-static {}, Lspeech/patts/DependencyData;->getDefaultInstance()Lspeech/patts/DependencyData;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 523
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    iget-object v1, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    # getter for: Lspeech/patts/Syntax;->dependencyData_:Lspeech/patts/DependencyData;
    invoke-static {v1}, Lspeech/patts/Syntax;->access$1300(Lspeech/patts/Syntax;)Lspeech/patts/DependencyData;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/DependencyData;->newBuilder(Lspeech/patts/DependencyData;)Lspeech/patts/DependencyData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/DependencyData$Builder;->mergeFrom(Lspeech/patts/DependencyData;)Lspeech/patts/DependencyData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/DependencyData$Builder;->buildPartial()Lspeech/patts/DependencyData;

    move-result-object v1

    # setter for: Lspeech/patts/Syntax;->dependencyData_:Lspeech/patts/DependencyData;
    invoke-static {v0, v1}, Lspeech/patts/Syntax;->access$1302(Lspeech/patts/Syntax;Lspeech/patts/DependencyData;)Lspeech/patts/DependencyData;

    .line 528
    :goto_0
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Syntax;->hasDependencyData:Z
    invoke-static {v0, v1}, Lspeech/patts/Syntax;->access$1202(Lspeech/patts/Syntax;Z)Z

    .line 529
    return-object p0

    .line 526
    :cond_0
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    # setter for: Lspeech/patts/Syntax;->dependencyData_:Lspeech/patts/DependencyData;
    invoke-static {v0, p1}, Lspeech/patts/Syntax;->access$1302(Lspeech/patts/Syntax;Lspeech/patts/DependencyData;)Lspeech/patts/DependencyData;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 226
    check-cast p1, Lspeech/patts/Syntax;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/Syntax$Builder;->mergeFrom(Lspeech/patts/Syntax;)Lspeech/patts/Syntax$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/Syntax;)Lspeech/patts/Syntax$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/Syntax;

    .prologue
    .line 295
    invoke-static {}, Lspeech/patts/Syntax;->getDefaultInstance()Lspeech/patts/Syntax;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 320
    :cond_0
    :goto_0
    return-object p0

    .line 296
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/Syntax;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 297
    invoke-virtual {p1}, Lspeech/patts/Syntax;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Syntax$Builder;->setName(Ljava/lang/String;)Lspeech/patts/Syntax$Builder;

    .line 299
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/Syntax;->hasLemma()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 300
    invoke-virtual {p1}, Lspeech/patts/Syntax;->getLemma()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Syntax$Builder;->setLemma(Ljava/lang/String;)Lspeech/patts/Syntax$Builder;

    .line 302
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/Syntax;->hasCategory()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 303
    invoke-virtual {p1}, Lspeech/patts/Syntax;->getCategory()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Syntax$Builder;->setCategory(Ljava/lang/String;)Lspeech/patts/Syntax$Builder;

    .line 305
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/Syntax;->hasTag()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 306
    invoke-virtual {p1}, Lspeech/patts/Syntax;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Syntax$Builder;->setTag(Ljava/lang/String;)Lspeech/patts/Syntax$Builder;

    .line 308
    :cond_5
    # getter for: Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Syntax;->access$300(Lspeech/patts/Syntax;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 309
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    # getter for: Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Syntax;->access$300(Lspeech/patts/Syntax;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 310
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Syntax;->access$302(Lspeech/patts/Syntax;Ljava/util/List;)Ljava/util/List;

    .line 312
    :cond_6
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    # getter for: Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Syntax;->access$300(Lspeech/patts/Syntax;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Syntax;->rosettaAnalyzerFeatures_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Syntax;->access$300(Lspeech/patts/Syntax;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 314
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/Syntax;->hasDependencyData()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 315
    invoke-virtual {p1}, Lspeech/patts/Syntax;->getDependencyData()Lspeech/patts/DependencyData;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Syntax$Builder;->mergeDependencyData(Lspeech/patts/DependencyData;)Lspeech/patts/Syntax$Builder;

    .line 317
    :cond_8
    invoke-virtual {p1}, Lspeech/patts/Syntax;->hasContainsNucleus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    invoke-virtual {p1}, Lspeech/patts/Syntax;->getContainsNucleus()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Syntax$Builder;->setContainsNucleus(Z)Lspeech/patts/Syntax$Builder;

    goto :goto_0
.end method

.method public setCategory(Ljava/lang/String;)Lspeech/patts/Syntax$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 426
    if-nez p1, :cond_0

    .line 427
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 429
    :cond_0
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Syntax;->hasCategory:Z
    invoke-static {v0, v1}, Lspeech/patts/Syntax;->access$802(Lspeech/patts/Syntax;Z)Z

    .line 430
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    # setter for: Lspeech/patts/Syntax;->category_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Syntax;->access$902(Lspeech/patts/Syntax;Ljava/lang/String;)Ljava/lang/String;

    .line 431
    return-object p0
.end method

.method public setContainsNucleus(Z)Lspeech/patts/Syntax$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 545
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Syntax;->hasContainsNucleus:Z
    invoke-static {v0, v1}, Lspeech/patts/Syntax;->access$1402(Lspeech/patts/Syntax;Z)Z

    .line 546
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    # setter for: Lspeech/patts/Syntax;->containsNucleus_:Z
    invoke-static {v0, p1}, Lspeech/patts/Syntax;->access$1502(Lspeech/patts/Syntax;Z)Z

    .line 547
    return-object p0
.end method

.method public setLemma(Ljava/lang/String;)Lspeech/patts/Syntax$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 405
    if-nez p1, :cond_0

    .line 406
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 408
    :cond_0
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Syntax;->hasLemma:Z
    invoke-static {v0, v1}, Lspeech/patts/Syntax;->access$602(Lspeech/patts/Syntax;Z)Z

    .line 409
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    # setter for: Lspeech/patts/Syntax;->lemma_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Syntax;->access$702(Lspeech/patts/Syntax;Ljava/lang/String;)Ljava/lang/String;

    .line 410
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lspeech/patts/Syntax$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 384
    if-nez p1, :cond_0

    .line 385
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 387
    :cond_0
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Syntax;->hasName:Z
    invoke-static {v0, v1}, Lspeech/patts/Syntax;->access$402(Lspeech/patts/Syntax;Z)Z

    .line 388
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    # setter for: Lspeech/patts/Syntax;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Syntax;->access$502(Lspeech/patts/Syntax;Ljava/lang/String;)Ljava/lang/String;

    .line 389
    return-object p0
.end method

.method public setTag(Ljava/lang/String;)Lspeech/patts/Syntax$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 447
    if-nez p1, :cond_0

    .line 448
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 450
    :cond_0
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Syntax;->hasTag:Z
    invoke-static {v0, v1}, Lspeech/patts/Syntax;->access$1002(Lspeech/patts/Syntax;Z)Z

    .line 451
    iget-object v0, p0, Lspeech/patts/Syntax$Builder;->result:Lspeech/patts/Syntax;

    # setter for: Lspeech/patts/Syntax;->tag_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Syntax;->access$1102(Lspeech/patts/Syntax;Ljava/lang/String;)Ljava/lang/String;

    .line 452
    return-object p0
.end method
