.class public final Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PhonologyDefinitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;",
        "Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 211
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
    .locals 1

    .prologue
    .line 205
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->create()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
    .locals 3

    .prologue
    .line 214
    new-instance v0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    invoke-direct {v0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;-><init>()V

    .line 215
    .local v0, "builder":Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
    new-instance v1, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;-><init>(Lspeech/patts/PhonologyDefinitions$1;)V

    iput-object v1, v0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    .line 216
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->build()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 247
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->buildPartial()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    .locals 3

    .prologue
    .line 260
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    if-nez v1, :cond_0

    .line 261
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 264
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    .line 265
    .local v0, "returnMe":Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    .line 266
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->clone()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->clone()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 205
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->clone()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
    .locals 2

    .prologue
    .line 233
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->create()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    invoke-virtual {v0, v1}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->mergeFrom(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;)Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    invoke-virtual {v0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 205
    check-cast p1, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->mergeFrom(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;)Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;)Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    .prologue
    .line 270
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->getDefaultInstance()Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 277
    :cond_0
    :goto_0
    return-object p0

    .line 271
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasPhoneme()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 272
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->getPhoneme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->setPhoneme(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    .line 274
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasGroup()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->getGroup()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->setGroup(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;

    goto :goto_0
.end method

.method public setGroup(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 337
    if-nez p1, :cond_0

    .line 338
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 340
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasGroup:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->access$502(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;Z)Z

    .line 341
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    # setter for: Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->group_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->access$602(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;Ljava/lang/String;)Ljava/lang/String;

    .line 342
    return-object p0
.end method

.method public setPhoneme(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 316
    if-nez p1, :cond_0

    .line 317
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 319
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->hasPhoneme:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->access$302(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;Z)Z

    .line 320
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap$Builder;->result:Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    # setter for: Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->phoneme_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->access$402(Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;Ljava/lang/String;)Ljava/lang/String;

    .line 321
    return-object p0
.end method
