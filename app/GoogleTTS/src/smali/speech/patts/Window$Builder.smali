.class public final Lspeech/patts/Window$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Window.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Window;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/Window;",
        "Lspeech/patts/Window$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/Window;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/Window$Builder;
    .locals 1

    .prologue
    .line 148
    invoke-static {}, Lspeech/patts/Window$Builder;->create()Lspeech/patts/Window$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/Window$Builder;
    .locals 3

    .prologue
    .line 157
    new-instance v0, Lspeech/patts/Window$Builder;

    invoke-direct {v0}, Lspeech/patts/Window$Builder;-><init>()V

    .line 158
    .local v0, "builder":Lspeech/patts/Window$Builder;
    new-instance v1, Lspeech/patts/Window;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/Window;-><init>(Lspeech/patts/Window$1;)V

    iput-object v1, v0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    .line 159
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lspeech/patts/Window$Builder;->build()Lspeech/patts/Window;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/Window;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/Window$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 188
    iget-object v0, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    invoke-static {v0}, Lspeech/patts/Window$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 190
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Window$Builder;->buildPartial()Lspeech/patts/Window;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/Window;
    .locals 3

    .prologue
    .line 203
    iget-object v1, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    if-nez v1, :cond_0

    .line 204
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 207
    :cond_0
    iget-object v1, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    # getter for: Lspeech/patts/Window;->coefficients_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Window;->access$300(Lspeech/patts/Window;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 208
    iget-object v1, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    iget-object v2, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    # getter for: Lspeech/patts/Window;->coefficients_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Window;->access$300(Lspeech/patts/Window;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Window;->coefficients_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Window;->access$302(Lspeech/patts/Window;Ljava/util/List;)Ljava/util/List;

    .line 211
    :cond_1
    iget-object v0, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    .line 212
    .local v0, "returnMe":Lspeech/patts/Window;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    .line 213
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lspeech/patts/Window$Builder;->clone()Lspeech/patts/Window$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lspeech/patts/Window$Builder;->clone()Lspeech/patts/Window$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 148
    invoke-virtual {p0}, Lspeech/patts/Window$Builder;->clone()Lspeech/patts/Window$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/Window$Builder;
    .locals 2

    .prologue
    .line 176
    invoke-static {}, Lspeech/patts/Window$Builder;->create()Lspeech/patts/Window$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    invoke-virtual {v0, v1}, Lspeech/patts/Window$Builder;->mergeFrom(Lspeech/patts/Window;)Lspeech/patts/Window$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    invoke-virtual {v0}, Lspeech/patts/Window;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 148
    check-cast p1, Lspeech/patts/Window;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/Window$Builder;->mergeFrom(Lspeech/patts/Window;)Lspeech/patts/Window$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/Window;)Lspeech/patts/Window$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/Window;

    .prologue
    .line 217
    invoke-static {}, Lspeech/patts/Window;->getDefaultInstance()Lspeech/patts/Window;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-object p0

    .line 218
    :cond_1
    # getter for: Lspeech/patts/Window;->coefficients_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Window;->access$300(Lspeech/patts/Window;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    iget-object v0, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    # getter for: Lspeech/patts/Window;->coefficients_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Window;->access$300(Lspeech/patts/Window;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 220
    iget-object v0, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Window;->coefficients_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Window;->access$302(Lspeech/patts/Window;Ljava/util/List;)Ljava/util/List;

    .line 222
    :cond_2
    iget-object v0, p0, Lspeech/patts/Window$Builder;->result:Lspeech/patts/Window;

    # getter for: Lspeech/patts/Window;->coefficients_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Window;->access$300(Lspeech/patts/Window;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Window;->coefficients_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Window;->access$300(Lspeech/patts/Window;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
