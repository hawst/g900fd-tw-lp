.class public final Lspeech/patts/CostFunctionTable$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "CostFunctionTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/CostFunctionTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/CostFunctionTable;",
        "Lspeech/patts/CostFunctionTable$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/CostFunctionTable;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/CostFunctionTable$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-static {}, Lspeech/patts/CostFunctionTable$Builder;->create()Lspeech/patts/CostFunctionTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/CostFunctionTable$Builder;
    .locals 3

    .prologue
    .line 177
    new-instance v0, Lspeech/patts/CostFunctionTable$Builder;

    invoke-direct {v0}, Lspeech/patts/CostFunctionTable$Builder;-><init>()V

    .line 178
    .local v0, "builder":Lspeech/patts/CostFunctionTable$Builder;
    new-instance v1, Lspeech/patts/CostFunctionTable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/CostFunctionTable;-><init>(Lspeech/patts/CostFunctionTable$1;)V

    iput-object v1, v0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    .line 179
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable$Builder;->build()Lspeech/patts/CostFunctionTable;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/CostFunctionTable;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    invoke-static {v0}, Lspeech/patts/CostFunctionTable$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 210
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable$Builder;->buildPartial()Lspeech/patts/CostFunctionTable;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/CostFunctionTable;
    .locals 3

    .prologue
    .line 223
    iget-object v1, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    if-nez v1, :cond_0

    .line 224
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 227
    :cond_0
    iget-object v1, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    # getter for: Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/CostFunctionTable;->access$300(Lspeech/patts/CostFunctionTable;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 228
    iget-object v1, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    iget-object v2, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    # getter for: Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/CostFunctionTable;->access$300(Lspeech/patts/CostFunctionTable;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/CostFunctionTable;->access$302(Lspeech/patts/CostFunctionTable;Ljava/util/List;)Ljava/util/List;

    .line 231
    :cond_1
    iget-object v0, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    .line 232
    .local v0, "returnMe":Lspeech/patts/CostFunctionTable;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    .line 233
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable$Builder;->clone()Lspeech/patts/CostFunctionTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable$Builder;->clone()Lspeech/patts/CostFunctionTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable$Builder;->clone()Lspeech/patts/CostFunctionTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/CostFunctionTable$Builder;
    .locals 2

    .prologue
    .line 196
    invoke-static {}, Lspeech/patts/CostFunctionTable$Builder;->create()Lspeech/patts/CostFunctionTable$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    invoke-virtual {v0, v1}, Lspeech/patts/CostFunctionTable$Builder;->mergeFrom(Lspeech/patts/CostFunctionTable;)Lspeech/patts/CostFunctionTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    invoke-virtual {v0}, Lspeech/patts/CostFunctionTable;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 168
    check-cast p1, Lspeech/patts/CostFunctionTable;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/CostFunctionTable$Builder;->mergeFrom(Lspeech/patts/CostFunctionTable;)Lspeech/patts/CostFunctionTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/CostFunctionTable;)Lspeech/patts/CostFunctionTable$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/CostFunctionTable;

    .prologue
    .line 237
    invoke-static {}, Lspeech/patts/CostFunctionTable;->getDefaultInstance()Lspeech/patts/CostFunctionTable;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-object p0

    .line 238
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/CostFunctionTable;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 239
    invoke-virtual {p1}, Lspeech/patts/CostFunctionTable;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/CostFunctionTable$Builder;->setName(Ljava/lang/String;)Lspeech/patts/CostFunctionTable$Builder;

    .line 241
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/CostFunctionTable;->hasSize()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 242
    invoke-virtual {p1}, Lspeech/patts/CostFunctionTable;->getSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/CostFunctionTable$Builder;->setSize(I)Lspeech/patts/CostFunctionTable$Builder;

    .line 244
    :cond_3
    # getter for: Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctionTable;->access$300(Lspeech/patts/CostFunctionTable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    iget-object v0, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    # getter for: Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctionTable;->access$300(Lspeech/patts/CostFunctionTable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 246
    iget-object v0, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/CostFunctionTable;->access$302(Lspeech/patts/CostFunctionTable;Ljava/util/List;)Ljava/util/List;

    .line 248
    :cond_4
    iget-object v0, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    # getter for: Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctionTable;->access$300(Lspeech/patts/CostFunctionTable;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctionTable;->access$300(Lspeech/patts/CostFunctionTable;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)Lspeech/patts/CostFunctionTable$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 302
    if-nez p1, :cond_0

    .line 303
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 305
    :cond_0
    iget-object v0, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/CostFunctionTable;->hasName:Z
    invoke-static {v0, v1}, Lspeech/patts/CostFunctionTable;->access$402(Lspeech/patts/CostFunctionTable;Z)Z

    .line 306
    iget-object v0, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    # setter for: Lspeech/patts/CostFunctionTable;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/CostFunctionTable;->access$502(Lspeech/patts/CostFunctionTable;Ljava/lang/String;)Ljava/lang/String;

    .line 307
    return-object p0
.end method

.method public setSize(I)Lspeech/patts/CostFunctionTable$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 323
    iget-object v0, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/CostFunctionTable;->hasSize:Z
    invoke-static {v0, v1}, Lspeech/patts/CostFunctionTable;->access$602(Lspeech/patts/CostFunctionTable;Z)Z

    .line 324
    iget-object v0, p0, Lspeech/patts/CostFunctionTable$Builder;->result:Lspeech/patts/CostFunctionTable;

    # setter for: Lspeech/patts/CostFunctionTable;->size_:I
    invoke-static {v0, p1}, Lspeech/patts/CostFunctionTable;->access$702(Lspeech/patts/CostFunctionTable;I)I

    .line 325
    return-object p0
.end method
