.class public final Lspeech/patts/ExcludeSpeech$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ExcludeSpeech.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/ExcludeSpeech;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/ExcludeSpeech;",
        "Lspeech/patts/ExcludeSpeech$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/ExcludeSpeech;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/ExcludeSpeech$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-static {}, Lspeech/patts/ExcludeSpeech$Builder;->create()Lspeech/patts/ExcludeSpeech$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/ExcludeSpeech$Builder;
    .locals 3

    .prologue
    .line 168
    new-instance v0, Lspeech/patts/ExcludeSpeech$Builder;

    invoke-direct {v0}, Lspeech/patts/ExcludeSpeech$Builder;-><init>()V

    .line 169
    .local v0, "builder":Lspeech/patts/ExcludeSpeech$Builder;
    new-instance v1, Lspeech/patts/ExcludeSpeech;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/ExcludeSpeech;-><init>(Lspeech/patts/ExcludeSpeech$1;)V

    iput-object v1, v0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    .line 170
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lspeech/patts/ExcludeSpeech$Builder;->build()Lspeech/patts/ExcludeSpeech;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/ExcludeSpeech;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/ExcludeSpeech$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    invoke-static {v0}, Lspeech/patts/ExcludeSpeech$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 201
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/ExcludeSpeech$Builder;->buildPartial()Lspeech/patts/ExcludeSpeech;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/ExcludeSpeech;
    .locals 3

    .prologue
    .line 214
    iget-object v1, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    if-nez v1, :cond_0

    .line 215
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 218
    :cond_0
    iget-object v0, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    .line 219
    .local v0, "returnMe":Lspeech/patts/ExcludeSpeech;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    .line 220
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lspeech/patts/ExcludeSpeech$Builder;->clone()Lspeech/patts/ExcludeSpeech$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lspeech/patts/ExcludeSpeech$Builder;->clone()Lspeech/patts/ExcludeSpeech$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 159
    invoke-virtual {p0}, Lspeech/patts/ExcludeSpeech$Builder;->clone()Lspeech/patts/ExcludeSpeech$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/ExcludeSpeech$Builder;
    .locals 2

    .prologue
    .line 187
    invoke-static {}, Lspeech/patts/ExcludeSpeech$Builder;->create()Lspeech/patts/ExcludeSpeech$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    invoke-virtual {v0, v1}, Lspeech/patts/ExcludeSpeech$Builder;->mergeFrom(Lspeech/patts/ExcludeSpeech;)Lspeech/patts/ExcludeSpeech$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    invoke-virtual {v0}, Lspeech/patts/ExcludeSpeech;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 159
    check-cast p1, Lspeech/patts/ExcludeSpeech;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/ExcludeSpeech$Builder;->mergeFrom(Lspeech/patts/ExcludeSpeech;)Lspeech/patts/ExcludeSpeech$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/ExcludeSpeech;)Lspeech/patts/ExcludeSpeech$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/ExcludeSpeech;

    .prologue
    .line 224
    invoke-static {}, Lspeech/patts/ExcludeSpeech;->getDefaultInstance()Lspeech/patts/ExcludeSpeech;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 234
    :cond_0
    :goto_0
    return-object p0

    .line 225
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/ExcludeSpeech;->hasFilename()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 226
    invoke-virtual {p1}, Lspeech/patts/ExcludeSpeech;->getFilename()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/ExcludeSpeech$Builder;->setFilename(Ljava/lang/String;)Lspeech/patts/ExcludeSpeech$Builder;

    .line 228
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/ExcludeSpeech;->hasStart()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 229
    invoke-virtual {p1}, Lspeech/patts/ExcludeSpeech;->getStart()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/ExcludeSpeech$Builder;->setStart(F)Lspeech/patts/ExcludeSpeech$Builder;

    .line 231
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/ExcludeSpeech;->hasEnd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p1}, Lspeech/patts/ExcludeSpeech;->getEnd()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/ExcludeSpeech$Builder;->setEnd(F)Lspeech/patts/ExcludeSpeech$Builder;

    goto :goto_0
.end method

.method public setEnd(F)Lspeech/patts/ExcludeSpeech$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 316
    iget-object v0, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/ExcludeSpeech;->hasEnd:Z
    invoke-static {v0, v1}, Lspeech/patts/ExcludeSpeech;->access$702(Lspeech/patts/ExcludeSpeech;Z)Z

    .line 317
    iget-object v0, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    # setter for: Lspeech/patts/ExcludeSpeech;->end_:F
    invoke-static {v0, p1}, Lspeech/patts/ExcludeSpeech;->access$802(Lspeech/patts/ExcludeSpeech;F)F

    .line 318
    return-object p0
.end method

.method public setFilename(Ljava/lang/String;)Lspeech/patts/ExcludeSpeech$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 277
    if-nez p1, :cond_0

    .line 278
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 280
    :cond_0
    iget-object v0, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/ExcludeSpeech;->hasFilename:Z
    invoke-static {v0, v1}, Lspeech/patts/ExcludeSpeech;->access$302(Lspeech/patts/ExcludeSpeech;Z)Z

    .line 281
    iget-object v0, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    # setter for: Lspeech/patts/ExcludeSpeech;->filename_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/ExcludeSpeech;->access$402(Lspeech/patts/ExcludeSpeech;Ljava/lang/String;)Ljava/lang/String;

    .line 282
    return-object p0
.end method

.method public setStart(F)Lspeech/patts/ExcludeSpeech$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 298
    iget-object v0, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/ExcludeSpeech;->hasStart:Z
    invoke-static {v0, v1}, Lspeech/patts/ExcludeSpeech;->access$502(Lspeech/patts/ExcludeSpeech;Z)Z

    .line 299
    iget-object v0, p0, Lspeech/patts/ExcludeSpeech$Builder;->result:Lspeech/patts/ExcludeSpeech;

    # setter for: Lspeech/patts/ExcludeSpeech;->start_:F
    invoke-static {v0, p1}, Lspeech/patts/ExcludeSpeech;->access$602(Lspeech/patts/ExcludeSpeech;F)F

    .line 300
    return-object p0
.end method
