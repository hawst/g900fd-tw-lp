.class public final Lspeech/patts/PrimaryStressTriggers$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PrimaryStressTriggers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PrimaryStressTriggers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/PrimaryStressTriggers;",
        "Lspeech/patts/PrimaryStressTriggers$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/PrimaryStressTriggers;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/PrimaryStressTriggers$Builder;
    .locals 1

    .prologue
    .line 165
    invoke-static {}, Lspeech/patts/PrimaryStressTriggers$Builder;->create()Lspeech/patts/PrimaryStressTriggers$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/PrimaryStressTriggers$Builder;
    .locals 3

    .prologue
    .line 174
    new-instance v0, Lspeech/patts/PrimaryStressTriggers$Builder;

    invoke-direct {v0}, Lspeech/patts/PrimaryStressTriggers$Builder;-><init>()V

    .line 175
    .local v0, "builder":Lspeech/patts/PrimaryStressTriggers$Builder;
    new-instance v1, Lspeech/patts/PrimaryStressTriggers;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/PrimaryStressTriggers;-><init>(Lspeech/patts/PrimaryStressTriggers$1;)V

    iput-object v1, v0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    .line 176
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers$Builder;->build()Lspeech/patts/PrimaryStressTriggers;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/PrimaryStressTriggers;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    invoke-static {v0}, Lspeech/patts/PrimaryStressTriggers$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 207
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers$Builder;->buildPartial()Lspeech/patts/PrimaryStressTriggers;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/PrimaryStressTriggers;
    .locals 3

    .prologue
    .line 220
    iget-object v1, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    if-nez v1, :cond_0

    .line 221
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 224
    :cond_0
    iget-object v1, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    # getter for: Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PrimaryStressTriggers;->access$300(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 225
    iget-object v1, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    iget-object v2, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    # getter for: Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PrimaryStressTriggers;->access$300(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PrimaryStressTriggers;->access$302(Lspeech/patts/PrimaryStressTriggers;Ljava/util/List;)Ljava/util/List;

    .line 228
    :cond_1
    iget-object v1, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    # getter for: Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PrimaryStressTriggers;->access$400(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 229
    iget-object v1, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    iget-object v2, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    # getter for: Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PrimaryStressTriggers;->access$400(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PrimaryStressTriggers;->access$402(Lspeech/patts/PrimaryStressTriggers;Ljava/util/List;)Ljava/util/List;

    .line 232
    :cond_2
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    .line 233
    .local v0, "returnMe":Lspeech/patts/PrimaryStressTriggers;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    .line 234
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers$Builder;->clone()Lspeech/patts/PrimaryStressTriggers$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers$Builder;->clone()Lspeech/patts/PrimaryStressTriggers$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p0}, Lspeech/patts/PrimaryStressTriggers$Builder;->clone()Lspeech/patts/PrimaryStressTriggers$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/PrimaryStressTriggers$Builder;
    .locals 2

    .prologue
    .line 193
    invoke-static {}, Lspeech/patts/PrimaryStressTriggers$Builder;->create()Lspeech/patts/PrimaryStressTriggers$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    invoke-virtual {v0, v1}, Lspeech/patts/PrimaryStressTriggers$Builder;->mergeFrom(Lspeech/patts/PrimaryStressTriggers;)Lspeech/patts/PrimaryStressTriggers$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    invoke-virtual {v0}, Lspeech/patts/PrimaryStressTriggers;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 165
    check-cast p1, Lspeech/patts/PrimaryStressTriggers;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/PrimaryStressTriggers$Builder;->mergeFrom(Lspeech/patts/PrimaryStressTriggers;)Lspeech/patts/PrimaryStressTriggers$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/PrimaryStressTriggers;)Lspeech/patts/PrimaryStressTriggers$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/PrimaryStressTriggers;

    .prologue
    .line 238
    invoke-static {}, Lspeech/patts/PrimaryStressTriggers;->getDefaultInstance()Lspeech/patts/PrimaryStressTriggers;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-object p0

    .line 239
    :cond_1
    # getter for: Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PrimaryStressTriggers;->access$300(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 240
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    # getter for: Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PrimaryStressTriggers;->access$300(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 241
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PrimaryStressTriggers;->access$302(Lspeech/patts/PrimaryStressTriggers;Ljava/util/List;)Ljava/util/List;

    .line 243
    :cond_2
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    # getter for: Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PrimaryStressTriggers;->access$300(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PrimaryStressTriggers;->startsWith_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PrimaryStressTriggers;->access$300(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 245
    :cond_3
    # getter for: Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PrimaryStressTriggers;->access$400(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    # getter for: Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PrimaryStressTriggers;->access$400(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 247
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PrimaryStressTriggers;->access$402(Lspeech/patts/PrimaryStressTriggers;Ljava/util/List;)Ljava/util/List;

    .line 249
    :cond_4
    iget-object v0, p0, Lspeech/patts/PrimaryStressTriggers$Builder;->result:Lspeech/patts/PrimaryStressTriggers;

    # getter for: Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PrimaryStressTriggers;->access$400(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PrimaryStressTriggers;->endsWith_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PrimaryStressTriggers;->access$400(Lspeech/patts/PrimaryStressTriggers;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
