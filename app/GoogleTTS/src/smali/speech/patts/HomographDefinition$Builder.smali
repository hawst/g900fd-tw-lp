.class public final Lspeech/patts/HomographDefinition$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "HomographDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/HomographDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/HomographDefinition;",
        "Lspeech/patts/HomographDefinition$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/HomographDefinition;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 508
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/HomographDefinition$Builder;
    .locals 1

    .prologue
    .line 502
    invoke-static {}, Lspeech/patts/HomographDefinition$Builder;->create()Lspeech/patts/HomographDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/HomographDefinition$Builder;
    .locals 3

    .prologue
    .line 511
    new-instance v0, Lspeech/patts/HomographDefinition$Builder;

    invoke-direct {v0}, Lspeech/patts/HomographDefinition$Builder;-><init>()V

    .line 512
    .local v0, "builder":Lspeech/patts/HomographDefinition$Builder;
    new-instance v1, Lspeech/patts/HomographDefinition;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/HomographDefinition;-><init>(Lspeech/patts/HomographDefinition$1;)V

    iput-object v1, v0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    .line 513
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 502
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition$Builder;->build()Lspeech/patts/HomographDefinition;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/HomographDefinition;
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    invoke-static {v0}, Lspeech/patts/HomographDefinition$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 544
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition$Builder;->buildPartial()Lspeech/patts/HomographDefinition;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/HomographDefinition;
    .locals 3

    .prologue
    .line 557
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    if-nez v1, :cond_0

    .line 558
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 561
    :cond_0
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HomographDefinition;->access$300(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 562
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    iget-object v2, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HomographDefinition;->access$300(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HomographDefinition;->access$302(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 565
    :cond_1
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HomographDefinition;->access$400(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 566
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    iget-object v2, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HomographDefinition;->access$400(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HomographDefinition;->access$402(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 569
    :cond_2
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HomographDefinition;->access$500(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 570
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    iget-object v2, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HomographDefinition;->access$500(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HomographDefinition;->access$502(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 573
    :cond_3
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HomographDefinition;->access$600(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 574
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    iget-object v2, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HomographDefinition;->access$600(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HomographDefinition;->access$602(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 577
    :cond_4
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HomographDefinition;->access$700(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_5

    .line 578
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    iget-object v2, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HomographDefinition;->access$700(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HomographDefinition;->access$702(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 581
    :cond_5
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HomographDefinition;->access$800(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_6

    .line 582
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    iget-object v2, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HomographDefinition;->access$800(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HomographDefinition;->access$802(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 585
    :cond_6
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HomographDefinition;->access$900(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_7

    .line 586
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    iget-object v2, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HomographDefinition;->access$900(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HomographDefinition;->access$902(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 589
    :cond_7
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HomographDefinition;->access$1000(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_8

    .line 590
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    iget-object v2, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HomographDefinition;->access$1000(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HomographDefinition;->access$1002(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 593
    :cond_8
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HomographDefinition;->access$1100(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_9

    .line 594
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    iget-object v2, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HomographDefinition;->access$1100(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HomographDefinition;->access$1102(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 597
    :cond_9
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HomographDefinition;->access$1200(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_a

    .line 598
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    iget-object v2, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HomographDefinition;->access$1200(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HomographDefinition;->access$1202(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 601
    :cond_a
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HomographDefinition;->access$1300(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_b

    .line 602
    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    iget-object v2, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HomographDefinition;->access$1300(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HomographDefinition;->access$1302(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 605
    :cond_b
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    .line 606
    .local v0, "returnMe":Lspeech/patts/HomographDefinition;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    .line 607
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 502
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition$Builder;->clone()Lspeech/patts/HomographDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 502
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition$Builder;->clone()Lspeech/patts/HomographDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 502
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition$Builder;->clone()Lspeech/patts/HomographDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/HomographDefinition$Builder;
    .locals 2

    .prologue
    .line 530
    invoke-static {}, Lspeech/patts/HomographDefinition$Builder;->create()Lspeech/patts/HomographDefinition$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    invoke-virtual {v0, v1}, Lspeech/patts/HomographDefinition$Builder;->mergeFrom(Lspeech/patts/HomographDefinition;)Lspeech/patts/HomographDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    invoke-virtual {v0}, Lspeech/patts/HomographDefinition;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 502
    check-cast p1, Lspeech/patts/HomographDefinition;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/HomographDefinition$Builder;->mergeFrom(Lspeech/patts/HomographDefinition;)Lspeech/patts/HomographDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/HomographDefinition;)Lspeech/patts/HomographDefinition$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 611
    invoke-static {}, Lspeech/patts/HomographDefinition;->getDefaultInstance()Lspeech/patts/HomographDefinition;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 705
    :cond_0
    :goto_0
    return-object p0

    .line 612
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->hasSpelling()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 613
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->getSpelling()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/HomographDefinition$Builder;->setSpelling(Ljava/lang/String;)Lspeech/patts/HomographDefinition$Builder;

    .line 615
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->hasWordid()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 616
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->getWordid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/HomographDefinition$Builder;->setWordid(Ljava/lang/String;)Lspeech/patts/HomographDefinition$Builder;

    .line 618
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->hasTranslationRule()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 619
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->getTranslationRule()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/HomographDefinition$Builder;->setTranslationRule(Ljava/lang/String;)Lspeech/patts/HomographDefinition$Builder;

    .line 621
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->hasSentenceInitial()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 622
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->getSentenceInitial()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/HomographDefinition$Builder;->setSentenceInitial(Z)Lspeech/patts/HomographDefinition$Builder;

    .line 624
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->hasSentenceFinal()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 625
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->getSentenceFinal()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/HomographDefinition$Builder;->setSentenceFinal(Z)Lspeech/patts/HomographDefinition$Builder;

    .line 627
    :cond_6
    # getter for: Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$300(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 628
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$300(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 629
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$302(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 631
    :cond_7
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$300(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$300(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 633
    :cond_8
    # getter for: Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$400(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 634
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$400(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 635
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$402(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 637
    :cond_9
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$400(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$400(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 639
    :cond_a
    # getter for: Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$500(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 640
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$500(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 641
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$502(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 643
    :cond_b
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$500(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$500(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 645
    :cond_c
    # getter for: Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$600(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 646
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$600(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 647
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$602(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 649
    :cond_d
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$600(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$600(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 651
    :cond_e
    # getter for: Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$700(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 652
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$700(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 653
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$702(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 655
    :cond_f
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$700(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$700(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 657
    :cond_10
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->hasBeforeProper()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 658
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->getBeforeProper()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/HomographDefinition$Builder;->setBeforeProper(Z)Lspeech/patts/HomographDefinition$Builder;

    .line 660
    :cond_11
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->hasFollowProper()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 661
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->getFollowProper()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/HomographDefinition$Builder;->setFollowProper(Z)Lspeech/patts/HomographDefinition$Builder;

    .line 663
    :cond_12
    # getter for: Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$800(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_14

    .line 664
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$800(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 665
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$802(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 667
    :cond_13
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$800(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$800(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 669
    :cond_14
    # getter for: Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$900(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_16

    .line 670
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$900(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 671
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$902(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 673
    :cond_15
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$900(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$900(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 675
    :cond_16
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->hasText()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 676
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/HomographDefinition$Builder;->setText(Ljava/lang/String;)Lspeech/patts/HomographDefinition$Builder;

    .line 678
    :cond_17
    # getter for: Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$1000(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_19

    .line 679
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$1000(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 680
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$1002(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 682
    :cond_18
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$1000(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$1000(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 684
    :cond_19
    # getter for: Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$1100(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 685
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$1100(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 686
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$1102(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 688
    :cond_1a
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$1100(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$1100(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 690
    :cond_1b
    # getter for: Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$1200(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1d

    .line 691
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$1200(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 692
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$1202(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 694
    :cond_1c
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$1200(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$1200(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 696
    :cond_1d
    # getter for: Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$1300(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1f

    .line 697
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$1300(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 698
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$1302(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;

    .line 700
    :cond_1e
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # getter for: Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HomographDefinition;->access$1300(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HomographDefinition;->access$1300(Lspeech/patts/HomographDefinition;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 702
    :cond_1f
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->hasIsDefault()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 703
    invoke-virtual {p1}, Lspeech/patts/HomographDefinition;->getIsDefault()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/HomographDefinition$Builder;->setIsDefault(Z)Lspeech/patts/HomographDefinition$Builder;

    goto/16 :goto_0
.end method

.method public setBeforeProper(Z)Lspeech/patts/HomographDefinition$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1128
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HomographDefinition;->hasBeforeProper:Z
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$2402(Lspeech/patts/HomographDefinition;Z)Z

    .line 1129
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # setter for: Lspeech/patts/HomographDefinition;->beforeProper_:Z
    invoke-static {v0, p1}, Lspeech/patts/HomographDefinition;->access$2502(Lspeech/patts/HomographDefinition;Z)Z

    .line 1130
    return-object p0
.end method

.method public setFollowProper(Z)Lspeech/patts/HomographDefinition$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1146
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HomographDefinition;->hasFollowProper:Z
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$2602(Lspeech/patts/HomographDefinition;Z)Z

    .line 1147
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # setter for: Lspeech/patts/HomographDefinition;->followProper_:Z
    invoke-static {v0, p1}, Lspeech/patts/HomographDefinition;->access$2702(Lspeech/patts/HomographDefinition;Z)Z

    .line 1148
    return-object p0
.end method

.method public setIsDefault(Z)Lspeech/patts/HomographDefinition$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1441
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HomographDefinition;->hasIsDefault:Z
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$3002(Lspeech/patts/HomographDefinition;Z)Z

    .line 1442
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # setter for: Lspeech/patts/HomographDefinition;->isDefault_:Z
    invoke-static {v0, p1}, Lspeech/patts/HomographDefinition;->access$3102(Lspeech/patts/HomographDefinition;Z)Z

    .line 1443
    return-object p0
.end method

.method public setSentenceFinal(Z)Lspeech/patts/HomographDefinition$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 910
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HomographDefinition;->hasSentenceFinal:Z
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$2202(Lspeech/patts/HomographDefinition;Z)Z

    .line 911
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # setter for: Lspeech/patts/HomographDefinition;->sentenceFinal_:Z
    invoke-static {v0, p1}, Lspeech/patts/HomographDefinition;->access$2302(Lspeech/patts/HomographDefinition;Z)Z

    .line 912
    return-object p0
.end method

.method public setSentenceInitial(Z)Lspeech/patts/HomographDefinition$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 892
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HomographDefinition;->hasSentenceInitial:Z
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$2002(Lspeech/patts/HomographDefinition;Z)Z

    .line 893
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # setter for: Lspeech/patts/HomographDefinition;->sentenceInitial_:Z
    invoke-static {v0, p1}, Lspeech/patts/HomographDefinition;->access$2102(Lspeech/patts/HomographDefinition;Z)Z

    .line 894
    return-object p0
.end method

.method public setSpelling(Ljava/lang/String;)Lspeech/patts/HomographDefinition$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 829
    if-nez p1, :cond_0

    .line 830
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 832
    :cond_0
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HomographDefinition;->hasSpelling:Z
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$1402(Lspeech/patts/HomographDefinition;Z)Z

    .line 833
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # setter for: Lspeech/patts/HomographDefinition;->spelling_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/HomographDefinition;->access$1502(Lspeech/patts/HomographDefinition;Ljava/lang/String;)Ljava/lang/String;

    .line 834
    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lspeech/patts/HomographDefinition$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1255
    if-nez p1, :cond_0

    .line 1256
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1258
    :cond_0
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HomographDefinition;->hasText:Z
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$2802(Lspeech/patts/HomographDefinition;Z)Z

    .line 1259
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # setter for: Lspeech/patts/HomographDefinition;->text_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/HomographDefinition;->access$2902(Lspeech/patts/HomographDefinition;Ljava/lang/String;)Ljava/lang/String;

    .line 1260
    return-object p0
.end method

.method public setTranslationRule(Ljava/lang/String;)Lspeech/patts/HomographDefinition$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 871
    if-nez p1, :cond_0

    .line 872
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 874
    :cond_0
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HomographDefinition;->hasTranslationRule:Z
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$1802(Lspeech/patts/HomographDefinition;Z)Z

    .line 875
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # setter for: Lspeech/patts/HomographDefinition;->translationRule_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/HomographDefinition;->access$1902(Lspeech/patts/HomographDefinition;Ljava/lang/String;)Ljava/lang/String;

    .line 876
    return-object p0
.end method

.method public setWordid(Ljava/lang/String;)Lspeech/patts/HomographDefinition$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 850
    if-nez p1, :cond_0

    .line 851
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 853
    :cond_0
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HomographDefinition;->hasWordid:Z
    invoke-static {v0, v1}, Lspeech/patts/HomographDefinition;->access$1602(Lspeech/patts/HomographDefinition;Z)Z

    .line 854
    iget-object v0, p0, Lspeech/patts/HomographDefinition$Builder;->result:Lspeech/patts/HomographDefinition;

    # setter for: Lspeech/patts/HomographDefinition;->wordid_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/HomographDefinition;->access$1702(Lspeech/patts/HomographDefinition;Ljava/lang/String;)Ljava/lang/String;

    .line 855
    return-object p0
.end method
