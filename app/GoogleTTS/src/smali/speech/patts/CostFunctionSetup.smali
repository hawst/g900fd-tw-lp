.class public final Lspeech/patts/CostFunctionSetup;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "CostFunctionSetup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/CostFunctionSetup$1;,
        Lspeech/patts/CostFunctionSetup$Builder;,
        Lspeech/patts/CostFunctionSetup$CFType;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/CostFunctionSetup;


# instance fields
.field private hasName:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 294
    new-instance v0, Lspeech/patts/CostFunctionSetup;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/CostFunctionSetup;-><init>(Z)V

    sput-object v0, Lspeech/patts/CostFunctionSetup;->defaultInstance:Lspeech/patts/CostFunctionSetup;

    .line 295
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 296
    sget-object v0, Lspeech/patts/CostFunctionSetup;->defaultInstance:Lspeech/patts/CostFunctionSetup;

    invoke-direct {v0}, Lspeech/patts/CostFunctionSetup;->initFields()V

    .line 297
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/CostFunctionSetup;->name_:Ljava/lang/String;

    .line 85
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/CostFunctionSetup;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/CostFunctionSetup;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/CostFunctionSetup$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/CostFunctionSetup$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/CostFunctionSetup;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/CostFunctionSetup;->name_:Ljava/lang/String;

    .line 85
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/CostFunctionSetup;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lspeech/patts/CostFunctionSetup;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctionSetup;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/CostFunctionSetup;->hasName:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/CostFunctionSetup;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctionSetup;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctionSetup;->name_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/CostFunctionSetup;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/CostFunctionSetup;->defaultInstance:Lspeech/patts/CostFunctionSetup;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public static newBuilder()Lspeech/patts/CostFunctionSetup$Builder;
    .locals 1

    .prologue
    .line 166
    # invokes: Lspeech/patts/CostFunctionSetup$Builder;->create()Lspeech/patts/CostFunctionSetup$Builder;
    invoke-static {}, Lspeech/patts/CostFunctionSetup$Builder;->access$100()Lspeech/patts/CostFunctionSetup$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/CostFunctionSetup;)Lspeech/patts/CostFunctionSetup$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/CostFunctionSetup;

    .prologue
    .line 169
    invoke-static {}, Lspeech/patts/CostFunctionSetup;->newBuilder()Lspeech/patts/CostFunctionSetup$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/CostFunctionSetup$Builder;->mergeFrom(Lspeech/patts/CostFunctionSetup;)Lspeech/patts/CostFunctionSetup$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup;->getDefaultInstanceForType()Lspeech/patts/CostFunctionSetup;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/CostFunctionSetup;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/CostFunctionSetup;->defaultInstance:Lspeech/patts/CostFunctionSetup;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lspeech/patts/CostFunctionSetup;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 87
    iget v0, p0, Lspeech/patts/CostFunctionSetup;->memoizedSerializedSize:I

    .line 88
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 96
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 90
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 91
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 92
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 95
    :cond_1
    iput v0, p0, Lspeech/patts/CostFunctionSetup;->memoizedSerializedSize:I

    move v1, v0

    .line 96
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lspeech/patts/CostFunctionSetup;->hasName:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lspeech/patts/CostFunctionSetup;->hasName:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 74
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup;->toBuilder()Lspeech/patts/CostFunctionSetup$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/CostFunctionSetup$Builder;
    .locals 1

    .prologue
    .line 171
    invoke-static {p0}, Lspeech/patts/CostFunctionSetup;->newBuilder(Lspeech/patts/CostFunctionSetup;)Lspeech/patts/CostFunctionSetup$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup;->getSerializedSize()I

    .line 80
    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/CostFunctionSetup;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 83
    :cond_0
    return-void
.end method
