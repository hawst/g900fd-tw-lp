.class public final Lspeech/patts/AcousticFeature;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "AcousticFeature.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/AcousticFeature$1;,
        Lspeech/patts/AcousticFeature$Builder;,
        Lspeech/patts/AcousticFeature$Orientation;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/AcousticFeature;


# instance fields
.field private feature_:Ljava/lang/String;

.field private filename_:Ljava/lang/String;

.field private hasFeature:Z

.field private hasFilename:Z

.field private hasOrient:Z

.field private hasSize:Z

.field private memoizedSerializedSize:I

.field private orient_:Lspeech/patts/AcousticFeature$Orientation;

.field private size_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 419
    new-instance v0, Lspeech/patts/AcousticFeature;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/AcousticFeature;-><init>(Z)V

    sput-object v0, Lspeech/patts/AcousticFeature;->defaultInstance:Lspeech/patts/AcousticFeature;

    .line 420
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 421
    sget-object v0, Lspeech/patts/AcousticFeature;->defaultInstance:Lspeech/patts/AcousticFeature;

    invoke-direct {v0}, Lspeech/patts/AcousticFeature;->initFields()V

    .line 422
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/AcousticFeature;->filename_:Ljava/lang/String;

    .line 71
    const/4 v0, 0x1

    iput v0, p0, Lspeech/patts/AcousticFeature;->size_:I

    .line 85
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/AcousticFeature;->feature_:Ljava/lang/String;

    .line 113
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/AcousticFeature;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/AcousticFeature;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/AcousticFeature$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/AcousticFeature$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/AcousticFeature;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/AcousticFeature;->filename_:Ljava/lang/String;

    .line 71
    const/4 v0, 0x1

    iput v0, p0, Lspeech/patts/AcousticFeature;->size_:I

    .line 85
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/AcousticFeature;->feature_:Ljava/lang/String;

    .line 113
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/AcousticFeature;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/AcousticFeature;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AcousticFeature;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/AcousticFeature;->feature_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lspeech/patts/AcousticFeature;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AcousticFeature;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/AcousticFeature;->hasFilename:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/AcousticFeature;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AcousticFeature;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/AcousticFeature;->filename_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/AcousticFeature;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AcousticFeature;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/AcousticFeature;->hasSize:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/AcousticFeature;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AcousticFeature;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/AcousticFeature;->size_:I

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/AcousticFeature;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AcousticFeature;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/AcousticFeature;->hasOrient:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/AcousticFeature;Lspeech/patts/AcousticFeature$Orientation;)Lspeech/patts/AcousticFeature$Orientation;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AcousticFeature;
    .param p1, "x1"    # Lspeech/patts/AcousticFeature$Orientation;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/AcousticFeature;->orient_:Lspeech/patts/AcousticFeature$Orientation;

    return-object p1
.end method

.method static synthetic access$902(Lspeech/patts/AcousticFeature;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/AcousticFeature;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/AcousticFeature;->hasFeature:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/AcousticFeature;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/AcousticFeature;->defaultInstance:Lspeech/patts/AcousticFeature;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lspeech/patts/AcousticFeature$Orientation;->EDGE:Lspeech/patts/AcousticFeature$Orientation;

    iput-object v0, p0, Lspeech/patts/AcousticFeature;->orient_:Lspeech/patts/AcousticFeature$Orientation;

    .line 91
    return-void
.end method

.method public static newBuilder()Lspeech/patts/AcousticFeature$Builder;
    .locals 1

    .prologue
    .line 206
    # invokes: Lspeech/patts/AcousticFeature$Builder;->create()Lspeech/patts/AcousticFeature$Builder;
    invoke-static {}, Lspeech/patts/AcousticFeature$Builder;->access$100()Lspeech/patts/AcousticFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/AcousticFeature;)Lspeech/patts/AcousticFeature$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/AcousticFeature;

    .prologue
    .line 209
    invoke-static {}, Lspeech/patts/AcousticFeature;->newBuilder()Lspeech/patts/AcousticFeature$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/AcousticFeature$Builder;->mergeFrom(Lspeech/patts/AcousticFeature;)Lspeech/patts/AcousticFeature$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->getDefaultInstanceForType()Lspeech/patts/AcousticFeature;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/AcousticFeature;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/AcousticFeature;->defaultInstance:Lspeech/patts/AcousticFeature;

    return-object v0
.end method

.method public getFeature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lspeech/patts/AcousticFeature;->feature_:Ljava/lang/String;

    return-object v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lspeech/patts/AcousticFeature;->filename_:Ljava/lang/String;

    return-object v0
.end method

.method public getOrient()Lspeech/patts/AcousticFeature$Orientation;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lspeech/patts/AcousticFeature;->orient_:Lspeech/patts/AcousticFeature$Orientation;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 115
    iget v0, p0, Lspeech/patts/AcousticFeature;->memoizedSerializedSize:I

    .line 116
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 136
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 118
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 119
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->hasFilename()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 120
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 123
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->hasSize()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 124
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->getSize()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 127
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->hasOrient()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 128
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->getOrient()Lspeech/patts/AcousticFeature$Orientation;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/AcousticFeature$Orientation;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 131
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->hasFeature()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 132
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->getFeature()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 135
    :cond_4
    iput v0, p0, Lspeech/patts/AcousticFeature;->memoizedSerializedSize:I

    move v1, v0

    .line 136
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lspeech/patts/AcousticFeature;->size_:I

    return v0
.end method

.method public hasFeature()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lspeech/patts/AcousticFeature;->hasFeature:Z

    return v0
.end method

.method public hasFilename()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lspeech/patts/AcousticFeature;->hasFilename:Z

    return v0
.end method

.method public hasOrient()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lspeech/patts/AcousticFeature;->hasOrient:Z

    return v0
.end method

.method public hasSize()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lspeech/patts/AcousticFeature;->hasSize:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->toBuilder()Lspeech/patts/AcousticFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/AcousticFeature$Builder;
    .locals 1

    .prologue
    .line 211
    invoke-static {p0}, Lspeech/patts/AcousticFeature;->newBuilder(Lspeech/patts/AcousticFeature;)Lspeech/patts/AcousticFeature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->getSerializedSize()I

    .line 99
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->hasFilename()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 102
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->hasSize()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->getSize()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 105
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->hasOrient()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 106
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->getOrient()Lspeech/patts/AcousticFeature$Orientation;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/AcousticFeature$Orientation;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 108
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->hasFeature()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 109
    const/4 v0, 0x4

    invoke-virtual {p0}, Lspeech/patts/AcousticFeature;->getFeature()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 111
    :cond_3
    return-void
.end method
