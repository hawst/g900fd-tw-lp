.class public final Lspeech/patts/PhonemeDefinition$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PhonemeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PhonemeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/PhonemeDefinition;",
        "Lspeech/patts/PhonemeDefinition$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/PhonemeDefinition;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 596
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/PhonemeDefinition$Builder;
    .locals 1

    .prologue
    .line 590
    invoke-static {}, Lspeech/patts/PhonemeDefinition$Builder;->create()Lspeech/patts/PhonemeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/PhonemeDefinition$Builder;
    .locals 3

    .prologue
    .line 599
    new-instance v0, Lspeech/patts/PhonemeDefinition$Builder;

    invoke-direct {v0}, Lspeech/patts/PhonemeDefinition$Builder;-><init>()V

    .line 600
    .local v0, "builder":Lspeech/patts/PhonemeDefinition$Builder;
    new-instance v1, Lspeech/patts/PhonemeDefinition;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/PhonemeDefinition;-><init>(Lspeech/patts/PhonemeDefinition$1;)V

    iput-object v1, v0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    .line 601
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 590
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition$Builder;->build()Lspeech/patts/PhonemeDefinition;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/PhonemeDefinition;
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 630
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    invoke-static {v0}, Lspeech/patts/PhonemeDefinition$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 632
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition$Builder;->buildPartial()Lspeech/patts/PhonemeDefinition;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/PhonemeDefinition;
    .locals 3

    .prologue
    .line 645
    iget-object v1, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    if-nez v1, :cond_0

    .line 646
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 649
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    .line 650
    .local v0, "returnMe":Lspeech/patts/PhonemeDefinition;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    .line 651
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 590
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition$Builder;->clone()Lspeech/patts/PhonemeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 590
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition$Builder;->clone()Lspeech/patts/PhonemeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 590
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition$Builder;->clone()Lspeech/patts/PhonemeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2

    .prologue
    .line 618
    invoke-static {}, Lspeech/patts/PhonemeDefinition$Builder;->create()Lspeech/patts/PhonemeDefinition$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    invoke-virtual {v0, v1}, Lspeech/patts/PhonemeDefinition$Builder;->mergeFrom(Lspeech/patts/PhonemeDefinition;)Lspeech/patts/PhonemeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    invoke-virtual {v0}, Lspeech/patts/PhonemeDefinition;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 590
    check-cast p1, Lspeech/patts/PhonemeDefinition;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/PhonemeDefinition$Builder;->mergeFrom(Lspeech/patts/PhonemeDefinition;)Lspeech/patts/PhonemeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/PhonemeDefinition;)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/PhonemeDefinition;

    .prologue
    .line 655
    invoke-static {}, Lspeech/patts/PhonemeDefinition;->getDefaultInstance()Lspeech/patts/PhonemeDefinition;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 701
    :cond_0
    :goto_0
    return-object p0

    .line 656
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 657
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setName(Ljava/lang/String;)Lspeech/patts/PhonemeDefinition$Builder;

    .line 659
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasHeight()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 660
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getHeight()Lspeech/patts/PhonemeDefinition$Height;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setHeight(Lspeech/patts/PhonemeDefinition$Height;)Lspeech/patts/PhonemeDefinition$Builder;

    .line 662
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasPosition()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 663
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getPosition()Lspeech/patts/PhonemeDefinition$Position;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setPosition(Lspeech/patts/PhonemeDefinition$Position;)Lspeech/patts/PhonemeDefinition$Builder;

    .line 665
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasLength()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 666
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getLength()Lspeech/patts/PhonemeDefinition$Length;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setLength(Lspeech/patts/PhonemeDefinition$Length;)Lspeech/patts/PhonemeDefinition$Builder;

    .line 668
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasPoa()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 669
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getPoa()Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setPoa(Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;)Lspeech/patts/PhonemeDefinition$Builder;

    .line 671
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasManner()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 672
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getManner()Lspeech/patts/PhonemeDefinition$Manner;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setManner(Lspeech/patts/PhonemeDefinition$Manner;)Lspeech/patts/PhonemeDefinition$Builder;

    .line 674
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasRounded()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 675
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getRounded()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setRounded(Z)Lspeech/patts/PhonemeDefinition$Builder;

    .line 677
    :cond_8
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasVoiced()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 678
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getVoiced()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setVoiced(Z)Lspeech/patts/PhonemeDefinition$Builder;

    .line 680
    :cond_9
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasSchwa()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 681
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getSchwa()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setSchwa(Z)Lspeech/patts/PhonemeDefinition$Builder;

    .line 683
    :cond_a
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasVowel()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 684
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getVowel()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setVowel(Z)Lspeech/patts/PhonemeDefinition$Builder;

    .line 686
    :cond_b
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasClosedSyllable()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 687
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getClosedSyllable()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setClosedSyllable(Z)Lspeech/patts/PhonemeDefinition$Builder;

    .line 689
    :cond_c
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasArchi()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 690
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getArchi()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setArchi(Z)Lspeech/patts/PhonemeDefinition$Builder;

    .line 692
    :cond_d
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasSecondaryPoa()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 693
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getSecondaryPoa()Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setSecondaryPoa(Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;)Lspeech/patts/PhonemeDefinition$Builder;

    .line 695
    :cond_e
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasSecondaryManner()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 696
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getSecondaryManner()Lspeech/patts/PhonemeDefinition$Manner;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setSecondaryManner(Lspeech/patts/PhonemeDefinition$Manner;)Lspeech/patts/PhonemeDefinition$Builder;

    .line 698
    :cond_f
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->hasSyllabicConsonant()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    invoke-virtual {p1}, Lspeech/patts/PhonemeDefinition;->getSyllabicConsonant()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonemeDefinition$Builder;->setSyllabicConsonant(Z)Lspeech/patts/PhonemeDefinition$Builder;

    goto/16 :goto_0
.end method

.method public setArchi(Z)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1036
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasArchi:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$2502(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 1037
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->archi_:Z
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$2602(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 1038
    return-object p0
.end method

.method public setClosedSyllable(Z)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1018
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasClosedSyllable:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$2302(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 1019
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->closedSyllable_:Z
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$2402(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 1020
    return-object p0
.end method

.method public setHeight(Lspeech/patts/PhonemeDefinition$Height;)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/PhonemeDefinition$Height;

    .prologue
    .line 841
    if-nez p1, :cond_0

    .line 842
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 844
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasHeight:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$502(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 845
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->height_:Lspeech/patts/PhonemeDefinition$Height;
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$602(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$Height;)Lspeech/patts/PhonemeDefinition$Height;

    .line 846
    return-object p0
.end method

.method public setLength(Lspeech/patts/PhonemeDefinition$Length;)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/PhonemeDefinition$Length;

    .prologue
    .line 883
    if-nez p1, :cond_0

    .line 884
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 886
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasLength:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$902(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 887
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->length_:Lspeech/patts/PhonemeDefinition$Length;
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$1002(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$Length;)Lspeech/patts/PhonemeDefinition$Length;

    .line 888
    return-object p0
.end method

.method public setManner(Lspeech/patts/PhonemeDefinition$Manner;)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/PhonemeDefinition$Manner;

    .prologue
    .line 925
    if-nez p1, :cond_0

    .line 926
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 928
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasManner:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$1302(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 929
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->manner_:Lspeech/patts/PhonemeDefinition$Manner;
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$1402(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$Manner;)Lspeech/patts/PhonemeDefinition$Manner;

    .line 930
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 820
    if-nez p1, :cond_0

    .line 821
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 823
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasName:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$302(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 824
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$402(Lspeech/patts/PhonemeDefinition;Ljava/lang/String;)Ljava/lang/String;

    .line 825
    return-object p0
.end method

.method public setPoa(Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .prologue
    .line 904
    if-nez p1, :cond_0

    .line 905
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 907
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasPoa:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$1102(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 908
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->poa_:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$1202(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;)Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 909
    return-object p0
.end method

.method public setPosition(Lspeech/patts/PhonemeDefinition$Position;)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/PhonemeDefinition$Position;

    .prologue
    .line 862
    if-nez p1, :cond_0

    .line 863
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 865
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasPosition:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$702(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 866
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->position_:Lspeech/patts/PhonemeDefinition$Position;
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$802(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$Position;)Lspeech/patts/PhonemeDefinition$Position;

    .line 867
    return-object p0
.end method

.method public setRounded(Z)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 946
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasRounded:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$1502(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 947
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->rounded_:Z
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$1602(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 948
    return-object p0
.end method

.method public setSchwa(Z)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 982
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasSchwa:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$1902(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 983
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->schwa_:Z
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$2002(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 984
    return-object p0
.end method

.method public setSecondaryManner(Lspeech/patts/PhonemeDefinition$Manner;)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/PhonemeDefinition$Manner;

    .prologue
    .line 1075
    if-nez p1, :cond_0

    .line 1076
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1078
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasSecondaryManner:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$2902(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 1079
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->secondaryManner_:Lspeech/patts/PhonemeDefinition$Manner;
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$3002(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$Manner;)Lspeech/patts/PhonemeDefinition$Manner;

    .line 1080
    return-object p0
.end method

.method public setSecondaryPoa(Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .prologue
    .line 1054
    if-nez p1, :cond_0

    .line 1055
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1057
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasSecondaryPoa:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$2702(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 1058
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->secondaryPoa_:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$2802(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;)Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 1059
    return-object p0
.end method

.method public setSyllabicConsonant(Z)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1096
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasSyllabicConsonant:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$3102(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 1097
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->syllabicConsonant_:Z
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$3202(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 1098
    return-object p0
.end method

.method public setVoiced(Z)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 964
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasVoiced:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$1702(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 965
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->voiced_:Z
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$1802(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 966
    return-object p0
.end method

.method public setVowel(Z)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1000
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonemeDefinition;->hasVowel:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonemeDefinition;->access$2102(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 1001
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition$Builder;->result:Lspeech/patts/PhonemeDefinition;

    # setter for: Lspeech/patts/PhonemeDefinition;->vowel_:Z
    invoke-static {v0, p1}, Lspeech/patts/PhonemeDefinition;->access$2202(Lspeech/patts/PhonemeDefinition;Z)Z

    .line 1002
    return-object p0
.end method
