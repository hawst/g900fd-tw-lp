.class public final Lspeech/patts/Voice;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Voice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/Voice$1;,
        Lspeech/patts/Voice$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/Voice;


# instance fields
.field private baseType_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/BaseType2;",
            ">;"
        }
    .end annotation
.end field

.field private costFunctions_:Lspeech/patts/CostFunctions;

.field private filenames_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasCostFunctions:Z

.field private memoizedSerializedSize:I

.field private model_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Model2;",
            ">;"
        }
    .end annotation
.end field

.field private unit_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Unit2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 622
    new-instance v0, Lspeech/patts/Voice;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/Voice;-><init>(Z)V

    sput-object v0, Lspeech/patts/Voice;->defaultInstance:Lspeech/patts/Voice;

    .line 623
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 624
    sget-object v0, Lspeech/patts/Voice;->defaultInstance:Lspeech/patts/Voice;

    invoke-direct {v0}, Lspeech/patts/Voice;->initFields()V

    .line 625
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Voice;->baseType_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Voice;->model_:Ljava/util/List;

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Voice;->unit_:Ljava/util/List;

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Voice;->filenames_:Ljava/util/List;

    .line 116
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Voice;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/Voice;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/Voice$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/Voice$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/Voice;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Voice;->baseType_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Voice;->model_:Ljava/util/List;

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Voice;->unit_:Ljava/util/List;

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Voice;->filenames_:Ljava/util/List;

    .line 116
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Voice;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/Voice;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Voice;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Voice;->baseType_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/Voice;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Voice;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Voice;->baseType_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lspeech/patts/Voice;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Voice;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Voice;->model_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lspeech/patts/Voice;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Voice;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Voice;->model_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lspeech/patts/Voice;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Voice;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Voice;->unit_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lspeech/patts/Voice;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Voice;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Voice;->unit_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lspeech/patts/Voice;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Voice;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Voice;->filenames_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lspeech/patts/Voice;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Voice;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Voice;->filenames_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$702(Lspeech/patts/Voice;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Voice;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Voice;->hasCostFunctions:Z

    return p1
.end method

.method static synthetic access$800(Lspeech/patts/Voice;)Lspeech/patts/CostFunctions;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Voice;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Voice;->costFunctions_:Lspeech/patts/CostFunctions;

    return-object v0
.end method

.method static synthetic access$802(Lspeech/patts/Voice;Lspeech/patts/CostFunctions;)Lspeech/patts/CostFunctions;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Voice;
    .param p1, "x1"    # Lspeech/patts/CostFunctions;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Voice;->costFunctions_:Lspeech/patts/CostFunctions;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/Voice;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/Voice;->defaultInstance:Lspeech/patts/Voice;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 78
    invoke-static {}, Lspeech/patts/CostFunctions;->getDefaultInstance()Lspeech/patts/CostFunctions;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Voice;->costFunctions_:Lspeech/patts/CostFunctions;

    .line 79
    return-void
.end method

.method public static newBuilder()Lspeech/patts/Voice$Builder;
    .locals 1

    .prologue
    .line 218
    # invokes: Lspeech/patts/Voice$Builder;->create()Lspeech/patts/Voice$Builder;
    invoke-static {}, Lspeech/patts/Voice$Builder;->access$100()Lspeech/patts/Voice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/Voice;)Lspeech/patts/Voice$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/Voice;

    .prologue
    .line 221
    invoke-static {}, Lspeech/patts/Voice;->newBuilder()Lspeech/patts/Voice$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/Voice$Builder;->mergeFrom(Lspeech/patts/Voice;)Lspeech/patts/Voice$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBaseTypeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/BaseType2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/Voice;->baseType_:Ljava/util/List;

    return-object v0
.end method

.method public getCostFunctions()Lspeech/patts/CostFunctions;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lspeech/patts/Voice;->costFunctions_:Lspeech/patts/CostFunctions;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Voice;->getDefaultInstanceForType()Lspeech/patts/Voice;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/Voice;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/Voice;->defaultInstance:Lspeech/patts/Voice;

    return-object v0
.end method

.method public getFilenamesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lspeech/patts/Voice;->filenames_:Ljava/util/List;

    return-object v0
.end method

.method public getModelList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Model2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lspeech/patts/Voice;->model_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 118
    iget v3, p0, Lspeech/patts/Voice;->memoizedSerializedSize:I

    .line 119
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 148
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 121
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 123
    const/4 v0, 0x0

    .line 124
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/Voice;->getFilenamesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 125
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 127
    goto :goto_1

    .line 128
    .end local v1    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v3, v0

    .line 129
    invoke-virtual {p0}, Lspeech/patts/Voice;->getFilenamesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 131
    invoke-virtual {p0}, Lspeech/patts/Voice;->hasCostFunctions()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 132
    const/4 v5, 0x3

    invoke-virtual {p0}, Lspeech/patts/Voice;->getCostFunctions()Lspeech/patts/CostFunctions;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 135
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Voice;->getBaseTypeList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/BaseType2;

    .line 136
    .local v1, "element":Lspeech/patts/BaseType2;
    const/4 v5, 0x7

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 138
    goto :goto_2

    .line 139
    .end local v1    # "element":Lspeech/patts/BaseType2;
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Voice;->getModelList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/Model2;

    .line 140
    .local v1, "element":Lspeech/patts/Model2;
    const/16 v5, 0x8

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 142
    goto :goto_3

    .line 143
    .end local v1    # "element":Lspeech/patts/Model2;
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Voice;->getUnitList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/Unit2;

    .line 144
    .local v1, "element":Lspeech/patts/Unit2;
    const/16 v5, 0x9

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 146
    goto :goto_4

    .line 147
    .end local v1    # "element":Lspeech/patts/Unit2;
    :cond_5
    iput v3, p0, Lspeech/patts/Voice;->memoizedSerializedSize:I

    move v4, v3

    .line 148
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getUnitList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Unit2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lspeech/patts/Voice;->unit_:Ljava/util/List;

    return-object v0
.end method

.method public hasCostFunctions()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lspeech/patts/Voice;->hasCostFunctions:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-virtual {p0}, Lspeech/patts/Voice;->getBaseTypeList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/BaseType2;

    .line 82
    .local v0, "element":Lspeech/patts/BaseType2;
    invoke-virtual {v0}, Lspeech/patts/BaseType2;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 93
    .end local v0    # "element":Lspeech/patts/BaseType2;
    :cond_1
    :goto_0
    return v2

    .line 84
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Voice;->getModelList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Model2;

    .line 85
    .local v0, "element":Lspeech/patts/Model2;
    invoke-virtual {v0}, Lspeech/patts/Model2;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_0

    .line 87
    .end local v0    # "element":Lspeech/patts/Model2;
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/Voice;->getUnitList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Unit2;

    .line 88
    .local v0, "element":Lspeech/patts/Unit2;
    invoke-virtual {v0}, Lspeech/patts/Unit2;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_5

    goto :goto_0

    .line 90
    .end local v0    # "element":Lspeech/patts/Unit2;
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/Voice;->hasCostFunctions()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 91
    invoke-virtual {p0}, Lspeech/patts/Voice;->getCostFunctions()Lspeech/patts/CostFunctions;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/CostFunctions;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 93
    :cond_7
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Voice;->toBuilder()Lspeech/patts/Voice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/Voice$Builder;
    .locals 1

    .prologue
    .line 223
    invoke-static {p0}, Lspeech/patts/Voice;->newBuilder(Lspeech/patts/Voice;)Lspeech/patts/Voice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    invoke-virtual {p0}, Lspeech/patts/Voice;->getSerializedSize()I

    .line 99
    invoke-virtual {p0}, Lspeech/patts/Voice;->getFilenamesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 100
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 102
    .end local v0    # "element":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Voice;->hasCostFunctions()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 103
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/Voice;->getCostFunctions()Lspeech/patts/CostFunctions;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 105
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Voice;->getBaseTypeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/BaseType2;

    .line 106
    .local v0, "element":Lspeech/patts/BaseType2;
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 108
    .end local v0    # "element":Lspeech/patts/BaseType2;
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/Voice;->getModelList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Model2;

    .line 109
    .local v0, "element":Lspeech/patts/Model2;
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    .line 111
    .end local v0    # "element":Lspeech/patts/Model2;
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/Voice;->getUnitList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Unit2;

    .line 112
    .local v0, "element":Lspeech/patts/Unit2;
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    .line 114
    .end local v0    # "element":Lspeech/patts/Unit2;
    :cond_4
    return-void
.end method
