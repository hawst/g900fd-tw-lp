.class public final Lspeech/patts/HomographContext$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "HomographContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/HomographContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/HomographContext;",
        "Lspeech/patts/HomographContext$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/HomographContext;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/HomographContext$Builder;
    .locals 1

    .prologue
    .line 160
    invoke-static {}, Lspeech/patts/HomographContext$Builder;->create()Lspeech/patts/HomographContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/HomographContext$Builder;
    .locals 3

    .prologue
    .line 169
    new-instance v0, Lspeech/patts/HomographContext$Builder;

    invoke-direct {v0}, Lspeech/patts/HomographContext$Builder;-><init>()V

    .line 170
    .local v0, "builder":Lspeech/patts/HomographContext$Builder;
    new-instance v1, Lspeech/patts/HomographContext;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/HomographContext;-><init>(Lspeech/patts/HomographContext$1;)V

    iput-object v1, v0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    .line 171
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lspeech/patts/HomographContext$Builder;->build()Lspeech/patts/HomographContext;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/HomographContext;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/HomographContext$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    iget-object v0, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    invoke-static {v0}, Lspeech/patts/HomographContext$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 202
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/HomographContext$Builder;->buildPartial()Lspeech/patts/HomographContext;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/HomographContext;
    .locals 3

    .prologue
    .line 215
    iget-object v1, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    if-nez v1, :cond_0

    .line 216
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 219
    :cond_0
    iget-object v0, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    .line 220
    .local v0, "returnMe":Lspeech/patts/HomographContext;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    .line 221
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lspeech/patts/HomographContext$Builder;->clone()Lspeech/patts/HomographContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lspeech/patts/HomographContext$Builder;->clone()Lspeech/patts/HomographContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 160
    invoke-virtual {p0}, Lspeech/patts/HomographContext$Builder;->clone()Lspeech/patts/HomographContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/HomographContext$Builder;
    .locals 2

    .prologue
    .line 188
    invoke-static {}, Lspeech/patts/HomographContext$Builder;->create()Lspeech/patts/HomographContext$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    invoke-virtual {v0, v1}, Lspeech/patts/HomographContext$Builder;->mergeFrom(Lspeech/patts/HomographContext;)Lspeech/patts/HomographContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    invoke-virtual {v0}, Lspeech/patts/HomographContext;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 160
    check-cast p1, Lspeech/patts/HomographContext;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/HomographContext$Builder;->mergeFrom(Lspeech/patts/HomographContext;)Lspeech/patts/HomographContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/HomographContext;)Lspeech/patts/HomographContext$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/HomographContext;

    .prologue
    .line 225
    invoke-static {}, Lspeech/patts/HomographContext;->getDefaultInstance()Lspeech/patts/HomographContext;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-object p0

    .line 226
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/HomographContext;->hasPattern()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 227
    invoke-virtual {p1}, Lspeech/patts/HomographContext;->getPattern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/HomographContext$Builder;->setPattern(Ljava/lang/String;)Lspeech/patts/HomographContext$Builder;

    .line 229
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/HomographContext;->hasIndex()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 230
    invoke-virtual {p1}, Lspeech/patts/HomographContext;->getIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/HomographContext$Builder;->setIndex(I)Lspeech/patts/HomographContext$Builder;

    .line 232
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/HomographContext;->hasFullMatchOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p1}, Lspeech/patts/HomographContext;->getFullMatchOnly()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/HomographContext$Builder;->setFullMatchOnly(Z)Lspeech/patts/HomographContext$Builder;

    goto :goto_0
.end method

.method public setFullMatchOnly(Z)Lspeech/patts/HomographContext$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 317
    iget-object v0, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HomographContext;->hasFullMatchOnly:Z
    invoke-static {v0, v1}, Lspeech/patts/HomographContext;->access$702(Lspeech/patts/HomographContext;Z)Z

    .line 318
    iget-object v0, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    # setter for: Lspeech/patts/HomographContext;->fullMatchOnly_:Z
    invoke-static {v0, p1}, Lspeech/patts/HomographContext;->access$802(Lspeech/patts/HomographContext;Z)Z

    .line 319
    return-object p0
.end method

.method public setIndex(I)Lspeech/patts/HomographContext$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 299
    iget-object v0, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HomographContext;->hasIndex:Z
    invoke-static {v0, v1}, Lspeech/patts/HomographContext;->access$502(Lspeech/patts/HomographContext;Z)Z

    .line 300
    iget-object v0, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    # setter for: Lspeech/patts/HomographContext;->index_:I
    invoke-static {v0, p1}, Lspeech/patts/HomographContext;->access$602(Lspeech/patts/HomographContext;I)I

    .line 301
    return-object p0
.end method

.method public setPattern(Ljava/lang/String;)Lspeech/patts/HomographContext$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 278
    if-nez p1, :cond_0

    .line 279
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 281
    :cond_0
    iget-object v0, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HomographContext;->hasPattern:Z
    invoke-static {v0, v1}, Lspeech/patts/HomographContext;->access$302(Lspeech/patts/HomographContext;Z)Z

    .line 282
    iget-object v0, p0, Lspeech/patts/HomographContext$Builder;->result:Lspeech/patts/HomographContext;

    # setter for: Lspeech/patts/HomographContext;->pattern_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/HomographContext;->access$402(Lspeech/patts/HomographContext;Ljava/lang/String;)Ljava/lang/String;

    .line 283
    return-object p0
.end method
