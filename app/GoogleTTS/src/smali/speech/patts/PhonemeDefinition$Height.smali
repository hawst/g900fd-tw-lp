.class public final enum Lspeech/patts/PhonemeDefinition$Height;
.super Ljava/lang/Enum;
.source "PhonemeDefinition.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PhonemeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Height"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/PhonemeDefinition$Height;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/PhonemeDefinition$Height;

.field public static final enum CLOSE:Lspeech/patts/PhonemeDefinition$Height;

.field public static final enum CLOSEMID:Lspeech/patts/PhonemeDefinition$Height;

.field public static final enum MID:Lspeech/patts/PhonemeDefinition$Height;

.field public static final enum NEARCLOSE:Lspeech/patts/PhonemeDefinition$Height;

.field public static final enum NEAROPEN:Lspeech/patts/PhonemeDefinition$Height;

.field public static final enum OPEN:Lspeech/patts/PhonemeDefinition$Height;

.field public static final enum OPENMID:Lspeech/patts/PhonemeDefinition$Height;

.field public static final enum UNDEFINED_HEIGHT:Lspeech/patts/PhonemeDefinition$Height;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/PhonemeDefinition$Height;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 138
    new-instance v0, Lspeech/patts/PhonemeDefinition$Height;

    const-string v1, "UNDEFINED_HEIGHT"

    invoke-direct {v0, v1, v5, v5, v5}, Lspeech/patts/PhonemeDefinition$Height;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Height;->UNDEFINED_HEIGHT:Lspeech/patts/PhonemeDefinition$Height;

    .line 139
    new-instance v0, Lspeech/patts/PhonemeDefinition$Height;

    const-string v1, "CLOSE"

    invoke-direct {v0, v1, v6, v6, v6}, Lspeech/patts/PhonemeDefinition$Height;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Height;->CLOSE:Lspeech/patts/PhonemeDefinition$Height;

    .line 140
    new-instance v0, Lspeech/patts/PhonemeDefinition$Height;

    const-string v1, "NEARCLOSE"

    invoke-direct {v0, v1, v7, v7, v7}, Lspeech/patts/PhonemeDefinition$Height;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Height;->NEARCLOSE:Lspeech/patts/PhonemeDefinition$Height;

    .line 141
    new-instance v0, Lspeech/patts/PhonemeDefinition$Height;

    const-string v1, "CLOSEMID"

    invoke-direct {v0, v1, v8, v8, v8}, Lspeech/patts/PhonemeDefinition$Height;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Height;->CLOSEMID:Lspeech/patts/PhonemeDefinition$Height;

    .line 142
    new-instance v0, Lspeech/patts/PhonemeDefinition$Height;

    const-string v1, "MID"

    invoke-direct {v0, v1, v9, v9, v9}, Lspeech/patts/PhonemeDefinition$Height;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Height;->MID:Lspeech/patts/PhonemeDefinition$Height;

    .line 143
    new-instance v0, Lspeech/patts/PhonemeDefinition$Height;

    const-string v1, "OPENMID"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$Height;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Height;->OPENMID:Lspeech/patts/PhonemeDefinition$Height;

    .line 144
    new-instance v0, Lspeech/patts/PhonemeDefinition$Height;

    const-string v1, "NEAROPEN"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$Height;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Height;->NEAROPEN:Lspeech/patts/PhonemeDefinition$Height;

    .line 145
    new-instance v0, Lspeech/patts/PhonemeDefinition$Height;

    const-string v1, "OPEN"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$Height;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Height;->OPEN:Lspeech/patts/PhonemeDefinition$Height;

    .line 136
    const/16 v0, 0x8

    new-array v0, v0, [Lspeech/patts/PhonemeDefinition$Height;

    sget-object v1, Lspeech/patts/PhonemeDefinition$Height;->UNDEFINED_HEIGHT:Lspeech/patts/PhonemeDefinition$Height;

    aput-object v1, v0, v5

    sget-object v1, Lspeech/patts/PhonemeDefinition$Height;->CLOSE:Lspeech/patts/PhonemeDefinition$Height;

    aput-object v1, v0, v6

    sget-object v1, Lspeech/patts/PhonemeDefinition$Height;->NEARCLOSE:Lspeech/patts/PhonemeDefinition$Height;

    aput-object v1, v0, v7

    sget-object v1, Lspeech/patts/PhonemeDefinition$Height;->CLOSEMID:Lspeech/patts/PhonemeDefinition$Height;

    aput-object v1, v0, v8

    sget-object v1, Lspeech/patts/PhonemeDefinition$Height;->MID:Lspeech/patts/PhonemeDefinition$Height;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lspeech/patts/PhonemeDefinition$Height;->OPENMID:Lspeech/patts/PhonemeDefinition$Height;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lspeech/patts/PhonemeDefinition$Height;->NEAROPEN:Lspeech/patts/PhonemeDefinition$Height;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lspeech/patts/PhonemeDefinition$Height;->OPEN:Lspeech/patts/PhonemeDefinition$Height;

    aput-object v2, v0, v1

    sput-object v0, Lspeech/patts/PhonemeDefinition$Height;->$VALUES:[Lspeech/patts/PhonemeDefinition$Height;

    .line 170
    new-instance v0, Lspeech/patts/PhonemeDefinition$Height$1;

    invoke-direct {v0}, Lspeech/patts/PhonemeDefinition$Height$1;-><init>()V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Height;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 179
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 180
    iput p3, p0, Lspeech/patts/PhonemeDefinition$Height;->index:I

    .line 181
    iput p4, p0, Lspeech/patts/PhonemeDefinition$Height;->value:I

    .line 182
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/PhonemeDefinition$Height;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 136
    const-class v0, Lspeech/patts/PhonemeDefinition$Height;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonemeDefinition$Height;

    return-object v0
.end method

.method public static values()[Lspeech/patts/PhonemeDefinition$Height;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lspeech/patts/PhonemeDefinition$Height;->$VALUES:[Lspeech/patts/PhonemeDefinition$Height;

    invoke-virtual {v0}, [Lspeech/patts/PhonemeDefinition$Height;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/PhonemeDefinition$Height;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lspeech/patts/PhonemeDefinition$Height;->value:I

    return v0
.end method
