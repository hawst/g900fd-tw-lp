.class public final Lspeech/patts/Rhyme$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Rhyme.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Rhyme;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/Rhyme;",
        "Lspeech/patts/Rhyme$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/Rhyme;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/Rhyme$Builder;
    .locals 1

    .prologue
    .line 170
    invoke-static {}, Lspeech/patts/Rhyme$Builder;->create()Lspeech/patts/Rhyme$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/Rhyme$Builder;
    .locals 3

    .prologue
    .line 179
    new-instance v0, Lspeech/patts/Rhyme$Builder;

    invoke-direct {v0}, Lspeech/patts/Rhyme$Builder;-><init>()V

    .line 180
    .local v0, "builder":Lspeech/patts/Rhyme$Builder;
    new-instance v1, Lspeech/patts/Rhyme;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/Rhyme;-><init>(Lspeech/patts/Rhyme$1;)V

    iput-object v1, v0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    .line 181
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 170
    invoke-virtual {p0}, Lspeech/patts/Rhyme$Builder;->build()Lspeech/patts/Rhyme;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/Rhyme;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/Rhyme$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    invoke-static {v0}, Lspeech/patts/Rhyme$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 212
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Rhyme$Builder;->buildPartial()Lspeech/patts/Rhyme;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/Rhyme;
    .locals 3

    .prologue
    .line 225
    iget-object v1, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    if-nez v1, :cond_0

    .line 226
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 229
    :cond_0
    iget-object v1, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    # getter for: Lspeech/patts/Rhyme;->coda_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Rhyme;->access$300(Lspeech/patts/Rhyme;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 230
    iget-object v1, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    iget-object v2, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    # getter for: Lspeech/patts/Rhyme;->coda_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Rhyme;->access$300(Lspeech/patts/Rhyme;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Rhyme;->coda_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Rhyme;->access$302(Lspeech/patts/Rhyme;Ljava/util/List;)Ljava/util/List;

    .line 233
    :cond_1
    iget-object v0, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    .line 234
    .local v0, "returnMe":Lspeech/patts/Rhyme;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    .line 235
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 170
    invoke-virtual {p0}, Lspeech/patts/Rhyme$Builder;->clone()Lspeech/patts/Rhyme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 170
    invoke-virtual {p0}, Lspeech/patts/Rhyme$Builder;->clone()Lspeech/patts/Rhyme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-virtual {p0}, Lspeech/patts/Rhyme$Builder;->clone()Lspeech/patts/Rhyme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/Rhyme$Builder;
    .locals 2

    .prologue
    .line 198
    invoke-static {}, Lspeech/patts/Rhyme$Builder;->create()Lspeech/patts/Rhyme$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    invoke-virtual {v0, v1}, Lspeech/patts/Rhyme$Builder;->mergeFrom(Lspeech/patts/Rhyme;)Lspeech/patts/Rhyme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    invoke-virtual {v0}, Lspeech/patts/Rhyme;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 170
    check-cast p1, Lspeech/patts/Rhyme;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/Rhyme$Builder;->mergeFrom(Lspeech/patts/Rhyme;)Lspeech/patts/Rhyme$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/Rhyme;)Lspeech/patts/Rhyme$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/Rhyme;

    .prologue
    .line 239
    invoke-static {}, Lspeech/patts/Rhyme;->getDefaultInstance()Lspeech/patts/Rhyme;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 252
    :cond_0
    :goto_0
    return-object p0

    .line 240
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/Rhyme;->hasVowel()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 241
    invoke-virtual {p1}, Lspeech/patts/Rhyme;->getVowel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Rhyme$Builder;->setVowel(Ljava/lang/String;)Lspeech/patts/Rhyme$Builder;

    .line 243
    :cond_2
    # getter for: Lspeech/patts/Rhyme;->coda_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Rhyme;->access$300(Lspeech/patts/Rhyme;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 244
    iget-object v0, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    # getter for: Lspeech/patts/Rhyme;->coda_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Rhyme;->access$300(Lspeech/patts/Rhyme;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 245
    iget-object v0, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Rhyme;->coda_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Rhyme;->access$302(Lspeech/patts/Rhyme;Ljava/util/List;)Ljava/util/List;

    .line 247
    :cond_3
    iget-object v0, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    # getter for: Lspeech/patts/Rhyme;->coda_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Rhyme;->access$300(Lspeech/patts/Rhyme;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Rhyme;->coda_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Rhyme;->access$300(Lspeech/patts/Rhyme;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 249
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/Rhyme;->hasIgnoreSyllabification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    invoke-virtual {p1}, Lspeech/patts/Rhyme;->getIgnoreSyllabification()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Rhyme$Builder;->setIgnoreSyllabification(Z)Lspeech/patts/Rhyme$Builder;

    goto :goto_0
.end method

.method public setIgnoreSyllabification(Z)Lspeech/patts/Rhyme$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 356
    iget-object v0, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Rhyme;->hasIgnoreSyllabification:Z
    invoke-static {v0, v1}, Lspeech/patts/Rhyme;->access$602(Lspeech/patts/Rhyme;Z)Z

    .line 357
    iget-object v0, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    # setter for: Lspeech/patts/Rhyme;->ignoreSyllabification_:Z
    invoke-static {v0, p1}, Lspeech/patts/Rhyme;->access$702(Lspeech/patts/Rhyme;Z)Z

    .line 358
    return-object p0
.end method

.method public setVowel(Ljava/lang/String;)Lspeech/patts/Rhyme$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 295
    if-nez p1, :cond_0

    .line 296
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 298
    :cond_0
    iget-object v0, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Rhyme;->hasVowel:Z
    invoke-static {v0, v1}, Lspeech/patts/Rhyme;->access$402(Lspeech/patts/Rhyme;Z)Z

    .line 299
    iget-object v0, p0, Lspeech/patts/Rhyme$Builder;->result:Lspeech/patts/Rhyme;

    # setter for: Lspeech/patts/Rhyme;->vowel_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Rhyme;->access$502(Lspeech/patts/Rhyme;Ljava/lang/String;)Ljava/lang/String;

    .line 300
    return-object p0
.end method
