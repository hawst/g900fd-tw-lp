.class public final Lspeech/patts/PhonologyDefinitions;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PhonologyDefinitions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/PhonologyDefinitions$1;,
        Lspeech/patts/PhonologyDefinitions$Builder;,
        Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;,
        Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;,
        Lspeech/patts/PhonologyDefinitions$StressPosition;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/PhonologyDefinitions;


# instance fields
.field private bannedRhyme_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Rhyme;",
            ">;"
        }
    .end annotation
.end field

.field private coda_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/ConsonantCluster;",
            ">;"
        }
    .end annotation
.end field

.field private compoundMergeRules_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/MergeCompounds;",
            ">;"
        }
    .end annotation
.end field

.field private compoundMergeSameConsonants_:Z

.field private compoundStressPosition_:Lspeech/patts/PhonologyDefinitions$StressPosition;

.field private foreignCoda_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/ConsonantCluster;",
            ">;"
        }
    .end annotation
.end field

.field private foreignOnset_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/ConsonantCluster;",
            ">;"
        }
    .end annotation
.end field

.field private foreignPhonemeDef_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/PhonemeDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private hasCompoundMergeSameConsonants:Z

.field private hasCompoundStressPosition:Z

.field private hasLanguage:Z

.field private hasLegalizingVowel:Z

.field private hasMaxCodaOrder:Z

.field private hasMaxOnsetOrder:Z

.field private hasOrthographicStressLetters:Z

.field private hasRegion:Z

.field private hasSinglePrimaryStressRequired:Z

.field private hasSpellingDefinition:Z

.field private hasSpellingStressMark:Z

.field private hasVowelLetters:Z

.field private hasWordidDefinition:Z

.field private language_:Ljava/lang/String;

.field private legalGeminate_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private legalizingVowel_:Ljava/lang/String;

.field private maxCodaOrder_:I

.field private maxOnsetOrder_:I

.field private memoizedSerializedSize:I

.field private onset_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/ConsonantCluster;",
            ">;"
        }
    .end annotation
.end field

.field private orthographicStressLetters_:Ljava/lang/String;

.field private permittedRhyme_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Rhyme;",
            ">;"
        }
    .end annotation
.end field

.field private phonemeDef_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/PhonemeDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private phonemeFallbackMap_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;",
            ">;"
        }
    .end annotation
.end field

.field private primaryStressTriggers_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/PrimaryStressTriggers;",
            ">;"
        }
    .end annotation
.end field

.field private primaryStress_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private region_:Ljava/lang/String;

.field private singlePrimaryStressRequired_:Z

.field private spellingDefinition_:Ljava/lang/String;

.field private spellingStressMark_:Ljava/lang/String;

.field private stressDef_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/StressDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private vowelLetters_:Ljava/lang/String;

.field private vowelPhonemeReduction_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;",
            ">;"
        }
    .end annotation
.end field

.field private wordidDefinition_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2737
    new-instance v0, Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/PhonologyDefinitions;-><init>(Z)V

    sput-object v0, Lspeech/patts/PhonologyDefinitions;->defaultInstance:Lspeech/patts/PhonologyDefinitions;

    .line 2738
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 2739
    sget-object v0, Lspeech/patts/PhonologyDefinitions;->defaultInstance:Lspeech/patts/PhonologyDefinitions;

    invoke-direct {v0}, Lspeech/patts/PhonologyDefinitions;->initFields()V

    .line 2740
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 707
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->language_:Ljava/lang/String;

    .line 714
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->region_:Ljava/lang/String;

    .line 720
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;

    .line 733
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->wordidDefinition_:Ljava/lang/String;

    .line 740
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->spellingDefinition_:Ljava/lang/String;

    .line 747
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->legalizingVowel_:Ljava/lang/String;

    .line 754
    const/4 v0, 0x3

    iput v0, p0, Lspeech/patts/PhonologyDefinitions;->maxOnsetOrder_:I

    .line 760
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;

    .line 773
    const/4 v0, 0x4

    iput v0, p0, Lspeech/patts/PhonologyDefinitions;->maxCodaOrder_:I

    .line 779
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;

    .line 791
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;

    .line 803
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;

    .line 815
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;

    .line 827
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;

    .line 839
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;

    .line 851
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;

    .line 864
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->orthographicStressLetters_:Ljava/lang/String;

    .line 870
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;

    .line 883
    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->singlePrimaryStressRequired_:Z

    .line 889
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;

    .line 901
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;

    .line 913
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;

    .line 933
    const/4 v0, 0x0

    iput-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->compoundMergeSameConsonants_:Z

    .line 939
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;

    .line 952
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->spellingStressMark_:Ljava/lang/String;

    .line 959
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->vowelLetters_:Ljava/lang/String;

    .line 965
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;

    .line 1100
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PhonologyDefinitions;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/PhonologyDefinitions;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/PhonologyDefinitions$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/PhonologyDefinitions$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/PhonologyDefinitions;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 707
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->language_:Ljava/lang/String;

    .line 714
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->region_:Ljava/lang/String;

    .line 720
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;

    .line 733
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->wordidDefinition_:Ljava/lang/String;

    .line 740
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->spellingDefinition_:Ljava/lang/String;

    .line 747
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->legalizingVowel_:Ljava/lang/String;

    .line 754
    const/4 v0, 0x3

    iput v0, p0, Lspeech/patts/PhonologyDefinitions;->maxOnsetOrder_:I

    .line 760
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;

    .line 773
    const/4 v0, 0x4

    iput v0, p0, Lspeech/patts/PhonologyDefinitions;->maxCodaOrder_:I

    .line 779
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;

    .line 791
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;

    .line 803
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;

    .line 815
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;

    .line 827
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;

    .line 839
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;

    .line 851
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;

    .line 864
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->orthographicStressLetters_:Ljava/lang/String;

    .line 870
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;

    .line 883
    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->singlePrimaryStressRequired_:Z

    .line 889
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;

    .line 901
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;

    .line 913
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;

    .line 933
    const/4 v0, 0x0

    iput-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->compoundMergeSameConsonants_:Z

    .line 939
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;

    .line 952
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->spellingStressMark_:Ljava/lang/String;

    .line 959
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->vowelLetters_:Ljava/lang/String;

    .line 965
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;

    .line 1100
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PhonologyDefinitions;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1902(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2002(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2102(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2202(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2302(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2400(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2402(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2500(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2502(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2600(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2602(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2700(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2702(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2800(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2802(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2902(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3002(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3102(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3202(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3302(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3402(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasLanguage:Z

    return p1
.end method

.method static synthetic access$3502(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->language_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3602(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasRegion:Z

    return p1
.end method

.method static synthetic access$3702(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->region_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3802(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasWordidDefinition:Z

    return p1
.end method

.method static synthetic access$3902(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->wordidDefinition_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4002(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasSpellingDefinition:Z

    return p1
.end method

.method static synthetic access$4102(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->spellingDefinition_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4202(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasLegalizingVowel:Z

    return p1
.end method

.method static synthetic access$4302(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->legalizingVowel_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4402(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasMaxOnsetOrder:Z

    return p1
.end method

.method static synthetic access$4502(Lspeech/patts/PhonologyDefinitions;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/PhonologyDefinitions;->maxOnsetOrder_:I

    return p1
.end method

.method static synthetic access$4602(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasMaxCodaOrder:Z

    return p1
.end method

.method static synthetic access$4702(Lspeech/patts/PhonologyDefinitions;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/PhonologyDefinitions;->maxCodaOrder_:I

    return p1
.end method

.method static synthetic access$4802(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasOrthographicStressLetters:Z

    return p1
.end method

.method static synthetic access$4902(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->orthographicStressLetters_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5002(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasSinglePrimaryStressRequired:Z

    return p1
.end method

.method static synthetic access$5102(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->singlePrimaryStressRequired_:Z

    return p1
.end method

.method static synthetic access$5202(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasCompoundStressPosition:Z

    return p1
.end method

.method static synthetic access$5302(Lspeech/patts/PhonologyDefinitions;Lspeech/patts/PhonologyDefinitions$StressPosition;)Lspeech/patts/PhonologyDefinitions$StressPosition;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Lspeech/patts/PhonologyDefinitions$StressPosition;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->compoundStressPosition_:Lspeech/patts/PhonologyDefinitions$StressPosition;

    return-object p1
.end method

.method static synthetic access$5402(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasCompoundMergeSameConsonants:Z

    return p1
.end method

.method static synthetic access$5502(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->compoundMergeSameConsonants_:Z

    return p1
.end method

.method static synthetic access$5602(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasSpellingStressMark:Z

    return p1
.end method

.method static synthetic access$5702(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->spellingStressMark_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5802(Lspeech/patts/PhonologyDefinitions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonologyDefinitions;->hasVowelLetters:Z

    return p1
.end method

.method static synthetic access$5902(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonologyDefinitions;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonologyDefinitions;->vowelLetters_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/PhonologyDefinitions;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/PhonologyDefinitions;->defaultInstance:Lspeech/patts/PhonologyDefinitions;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 976
    sget-object v0, Lspeech/patts/PhonologyDefinitions$StressPosition;->STRESS_FIRST_WORD:Lspeech/patts/PhonologyDefinitions$StressPosition;

    iput-object v0, p0, Lspeech/patts/PhonologyDefinitions;->compoundStressPosition_:Lspeech/patts/PhonologyDefinitions$StressPosition;

    .line 977
    return-void
.end method

.method public static newBuilder()Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 1

    .prologue
    .line 1299
    # invokes: Lspeech/patts/PhonologyDefinitions$Builder;->create()Lspeech/patts/PhonologyDefinitions$Builder;
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$Builder;->access$1700()Lspeech/patts/PhonologyDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/PhonologyDefinitions;)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 1302
    invoke-static {}, Lspeech/patts/PhonologyDefinitions;->newBuilder()Lspeech/patts/PhonologyDefinitions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/PhonologyDefinitions$Builder;->mergeFrom(Lspeech/patts/PhonologyDefinitions;)Lspeech/patts/PhonologyDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBannedRhymeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Rhyme;",
            ">;"
        }
    .end annotation

    .prologue
    .line 818
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;

    return-object v0
.end method

.method public getCodaList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/ConsonantCluster;",
            ">;"
        }
    .end annotation

    .prologue
    .line 782
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;

    return-object v0
.end method

.method public getCompoundMergeRulesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/MergeCompounds;",
            ">;"
        }
    .end annotation

    .prologue
    .line 942
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;

    return-object v0
.end method

.method public getCompoundMergeSameConsonants()Z
    .locals 1

    .prologue
    .line 935
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->compoundMergeSameConsonants_:Z

    return v0
.end method

.method public getCompoundStressPosition()Lspeech/patts/PhonologyDefinitions$StressPosition;
    .locals 1

    .prologue
    .line 928
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->compoundStressPosition_:Lspeech/patts/PhonologyDefinitions$StressPosition;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getDefaultInstanceForType()Lspeech/patts/PhonologyDefinitions;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/PhonologyDefinitions;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/PhonologyDefinitions;->defaultInstance:Lspeech/patts/PhonologyDefinitions;

    return-object v0
.end method

.method public getForeignCodaList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/ConsonantCluster;",
            ">;"
        }
    .end annotation

    .prologue
    .line 806
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;

    return-object v0
.end method

.method public getForeignOnsetList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/ConsonantCluster;",
            ">;"
        }
    .end annotation

    .prologue
    .line 794
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;

    return-object v0
.end method

.method public getForeignPhonemeDefList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/PhonemeDefinition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 842
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->language_:Ljava/lang/String;

    return-object v0
.end method

.method public getLegalGeminateList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 904
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;

    return-object v0
.end method

.method public getLegalizingVowel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->legalizingVowel_:Ljava/lang/String;

    return-object v0
.end method

.method public getMaxCodaOrder()I
    .locals 1

    .prologue
    .line 775
    iget v0, p0, Lspeech/patts/PhonologyDefinitions;->maxCodaOrder_:I

    return v0
.end method

.method public getMaxOnsetOrder()I
    .locals 1

    .prologue
    .line 756
    iget v0, p0, Lspeech/patts/PhonologyDefinitions;->maxOnsetOrder_:I

    return v0
.end method

.method public getOnsetList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/ConsonantCluster;",
            ">;"
        }
    .end annotation

    .prologue
    .line 763
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;

    return-object v0
.end method

.method public getOrthographicStressLetters()Ljava/lang/String;
    .locals 1

    .prologue
    .line 866
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->orthographicStressLetters_:Ljava/lang/String;

    return-object v0
.end method

.method public getPermittedRhymeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Rhyme;",
            ">;"
        }
    .end annotation

    .prologue
    .line 830
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;

    return-object v0
.end method

.method public getPhonemeDefList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/PhonemeDefinition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 723
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;

    return-object v0
.end method

.method public getPhonemeFallbackMapList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 916
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;

    return-object v0
.end method

.method public getPrimaryStressList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 892
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;

    return-object v0
.end method

.method public getPrimaryStressTriggersList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/PrimaryStressTriggers;",
            ">;"
        }
    .end annotation

    .prologue
    .line 873
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;

    return-object v0
.end method

.method public getRegion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 716
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->region_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 1102
    iget v3, p0, Lspeech/patts/PhonologyDefinitions;->memoizedSerializedSize:I

    .line 1103
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 1229
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 1105
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 1106
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasLanguage()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1107
    const/4 v5, 0x1

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1110
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasRegion()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1111
    const/4 v5, 0x2

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getRegion()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1114
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPhonemeDefList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/PhonemeDefinition;

    .line 1115
    .local v1, "element":Lspeech/patts/PhonemeDefinition;
    const/4 v5, 0x3

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1117
    goto :goto_1

    .line 1118
    .end local v1    # "element":Lspeech/patts/PhonemeDefinition;
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasWordidDefinition()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1119
    const/4 v5, 0x4

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getWordidDefinition()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1122
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasSpellingDefinition()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1123
    const/4 v5, 0x5

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getSpellingDefinition()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1126
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasLegalizingVowel()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1127
    const/4 v5, 0x6

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getLegalizingVowel()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1130
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getOnsetList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/ConsonantCluster;

    .line 1131
    .local v1, "element":Lspeech/patts/ConsonantCluster;
    const/4 v5, 0x7

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1133
    goto :goto_2

    .line 1134
    .end local v1    # "element":Lspeech/patts/ConsonantCluster;
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getCodaList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/ConsonantCluster;

    .line 1135
    .restart local v1    # "element":Lspeech/patts/ConsonantCluster;
    const/16 v5, 0x8

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1137
    goto :goto_3

    .line 1138
    .end local v1    # "element":Lspeech/patts/ConsonantCluster;
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getForeignOnsetList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/ConsonantCluster;

    .line 1139
    .restart local v1    # "element":Lspeech/patts/ConsonantCluster;
    const/16 v5, 0x9

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1141
    goto :goto_4

    .line 1142
    .end local v1    # "element":Lspeech/patts/ConsonantCluster;
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getForeignCodaList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/ConsonantCluster;

    .line 1143
    .restart local v1    # "element":Lspeech/patts/ConsonantCluster;
    const/16 v5, 0xa

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1145
    goto :goto_5

    .line 1146
    .end local v1    # "element":Lspeech/patts/ConsonantCluster;
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getForeignPhonemeDefList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/PhonemeDefinition;

    .line 1147
    .local v1, "element":Lspeech/patts/PhonemeDefinition;
    const/16 v5, 0xf

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1149
    goto :goto_6

    .line 1150
    .end local v1    # "element":Lspeech/patts/PhonemeDefinition;
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getStressDefList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/StressDefinition;

    .line 1151
    .local v1, "element":Lspeech/patts/StressDefinition;
    const/16 v5, 0x12

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1153
    goto :goto_7

    .line 1154
    .end local v1    # "element":Lspeech/patts/StressDefinition;
    :cond_c
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasMaxOnsetOrder()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1155
    const/16 v5, 0x13

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getMaxOnsetOrder()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 1158
    :cond_d
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasMaxCodaOrder()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1159
    const/16 v5, 0x14

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getMaxCodaOrder()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 1162
    :cond_e
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasOrthographicStressLetters()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 1163
    const/16 v5, 0x15

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getOrthographicStressLetters()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1166
    :cond_f
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPrimaryStressTriggersList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_10

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/PrimaryStressTriggers;

    .line 1167
    .local v1, "element":Lspeech/patts/PrimaryStressTriggers;
    const/16 v5, 0x16

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1169
    goto :goto_8

    .line 1170
    .end local v1    # "element":Lspeech/patts/PrimaryStressTriggers;
    :cond_10
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getBannedRhymeList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_11

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/Rhyme;

    .line 1171
    .local v1, "element":Lspeech/patts/Rhyme;
    const/16 v5, 0x17

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1173
    goto :goto_9

    .line 1174
    .end local v1    # "element":Lspeech/patts/Rhyme;
    :cond_11
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPermittedRhymeList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_12

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/Rhyme;

    .line 1175
    .restart local v1    # "element":Lspeech/patts/Rhyme;
    const/16 v5, 0x18

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1177
    goto :goto_a

    .line 1178
    .end local v1    # "element":Lspeech/patts/Rhyme;
    :cond_12
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPhonemeFallbackMapList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_13

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    .line 1179
    .local v1, "element":Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    const/16 v5, 0x19

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1181
    goto :goto_b

    .line 1182
    .end local v1    # "element":Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    :cond_13
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasCompoundStressPosition()Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1183
    const/16 v5, 0x1a

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getCompoundStressPosition()Lspeech/patts/PhonologyDefinitions$StressPosition;

    move-result-object v6

    invoke-virtual {v6}, Lspeech/patts/PhonologyDefinitions$StressPosition;->getNumber()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 1186
    :cond_14
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasCompoundMergeSameConsonants()Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1187
    const/16 v5, 0x1b

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getCompoundMergeSameConsonants()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 1190
    :cond_15
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getCompoundMergeRulesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_16

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/MergeCompounds;

    .line 1191
    .local v1, "element":Lspeech/patts/MergeCompounds;
    const/16 v5, 0x1c

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1193
    goto :goto_c

    .line 1194
    .end local v1    # "element":Lspeech/patts/MergeCompounds;
    :cond_16
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasSinglePrimaryStressRequired()Z

    move-result v5

    if-eqz v5, :cond_17

    .line 1195
    const/16 v5, 0x1d

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getSinglePrimaryStressRequired()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 1199
    :cond_17
    const/4 v0, 0x0

    .line 1200
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getLegalGeminateList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_18

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1201
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1203
    goto :goto_d

    .line 1204
    .end local v1    # "element":Ljava/lang/String;
    :cond_18
    add-int/2addr v3, v0

    .line 1205
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getLegalGeminateList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    .line 1208
    const/4 v0, 0x0

    .line 1209
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPrimaryStressList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_19

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1210
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v0, v5

    .line 1212
    goto :goto_e

    .line 1213
    .end local v1    # "element":I
    :cond_19
    add-int/2addr v3, v0

    .line 1214
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPrimaryStressList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    .line 1216
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasSpellingStressMark()Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1217
    const/16 v5, 0x20

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getSpellingStressMark()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1220
    :cond_1a
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasVowelLetters()Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 1221
    const/16 v5, 0x21

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getVowelLetters()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1224
    :cond_1b
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getVowelPhonemeReductionList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    .line 1225
    .local v1, "element":Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    const/16 v5, 0x22

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1227
    goto :goto_f

    .line 1228
    .end local v1    # "element":Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    :cond_1c
    iput v3, p0, Lspeech/patts/PhonologyDefinitions;->memoizedSerializedSize:I

    move v4, v3

    .line 1229
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getSinglePrimaryStressRequired()Z
    .locals 1

    .prologue
    .line 885
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->singlePrimaryStressRequired_:Z

    return v0
.end method

.method public getSpellingDefinition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 742
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->spellingDefinition_:Ljava/lang/String;

    return-object v0
.end method

.method public getSpellingStressMark()Ljava/lang/String;
    .locals 1

    .prologue
    .line 954
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->spellingStressMark_:Ljava/lang/String;

    return-object v0
.end method

.method public getStressDefList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/StressDefinition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 854
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;

    return-object v0
.end method

.method public getVowelLetters()Ljava/lang/String;
    .locals 1

    .prologue
    .line 961
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->vowelLetters_:Ljava/lang/String;

    return-object v0
.end method

.method public getVowelPhonemeReductionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 968
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;

    return-object v0
.end method

.method public getWordidDefinition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 735
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions;->wordidDefinition_:Ljava/lang/String;

    return-object v0
.end method

.method public hasCompoundMergeSameConsonants()Z
    .locals 1

    .prologue
    .line 934
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasCompoundMergeSameConsonants:Z

    return v0
.end method

.method public hasCompoundStressPosition()Z
    .locals 1

    .prologue
    .line 927
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasCompoundStressPosition:Z

    return v0
.end method

.method public hasLanguage()Z
    .locals 1

    .prologue
    .line 708
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasLanguage:Z

    return v0
.end method

.method public hasLegalizingVowel()Z
    .locals 1

    .prologue
    .line 748
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasLegalizingVowel:Z

    return v0
.end method

.method public hasMaxCodaOrder()Z
    .locals 1

    .prologue
    .line 774
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasMaxCodaOrder:Z

    return v0
.end method

.method public hasMaxOnsetOrder()Z
    .locals 1

    .prologue
    .line 755
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasMaxOnsetOrder:Z

    return v0
.end method

.method public hasOrthographicStressLetters()Z
    .locals 1

    .prologue
    .line 865
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasOrthographicStressLetters:Z

    return v0
.end method

.method public hasRegion()Z
    .locals 1

    .prologue
    .line 715
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasRegion:Z

    return v0
.end method

.method public hasSinglePrimaryStressRequired()Z
    .locals 1

    .prologue
    .line 884
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasSinglePrimaryStressRequired:Z

    return v0
.end method

.method public hasSpellingDefinition()Z
    .locals 1

    .prologue
    .line 741
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasSpellingDefinition:Z

    return v0
.end method

.method public hasSpellingStressMark()Z
    .locals 1

    .prologue
    .line 953
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasSpellingStressMark:Z

    return v0
.end method

.method public hasVowelLetters()Z
    .locals 1

    .prologue
    .line 960
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasVowelLetters:Z

    return v0
.end method

.method public hasWordidDefinition()Z
    .locals 1

    .prologue
    .line 734
    iget-boolean v0, p0, Lspeech/patts/PhonologyDefinitions;->hasWordidDefinition:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 979
    iget-boolean v3, p0, Lspeech/patts/PhonologyDefinitions;->hasLanguage:Z

    if-nez v3, :cond_1

    .line 1008
    :cond_0
    :goto_0
    return v2

    .line 980
    :cond_1
    iget-boolean v3, p0, Lspeech/patts/PhonologyDefinitions;->hasRegion:Z

    if-eqz v3, :cond_0

    .line 981
    iget-boolean v3, p0, Lspeech/patts/PhonologyDefinitions;->hasWordidDefinition:Z

    if-eqz v3, :cond_0

    .line 982
    iget-boolean v3, p0, Lspeech/patts/PhonologyDefinitions;->hasSpellingDefinition:Z

    if-eqz v3, :cond_0

    .line 983
    iget-boolean v3, p0, Lspeech/patts/PhonologyDefinitions;->hasLegalizingVowel:Z

    if-eqz v3, :cond_0

    .line 984
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPhonemeDefList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonemeDefinition;

    .line 985
    .local v0, "element":Lspeech/patts/PhonemeDefinition;
    invoke-virtual {v0}, Lspeech/patts/PhonemeDefinition;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 987
    .end local v0    # "element":Lspeech/patts/PhonemeDefinition;
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getBannedRhymeList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Rhyme;

    .line 988
    .local v0, "element":Lspeech/patts/Rhyme;
    invoke-virtual {v0}, Lspeech/patts/Rhyme;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_4

    goto :goto_0

    .line 990
    .end local v0    # "element":Lspeech/patts/Rhyme;
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPermittedRhymeList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Rhyme;

    .line 991
    .restart local v0    # "element":Lspeech/patts/Rhyme;
    invoke-virtual {v0}, Lspeech/patts/Rhyme;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_6

    goto :goto_0

    .line 993
    .end local v0    # "element":Lspeech/patts/Rhyme;
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getForeignPhonemeDefList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonemeDefinition;

    .line 994
    .local v0, "element":Lspeech/patts/PhonemeDefinition;
    invoke-virtual {v0}, Lspeech/patts/PhonemeDefinition;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_8

    goto :goto_0

    .line 996
    .end local v0    # "element":Lspeech/patts/PhonemeDefinition;
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getStressDefList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/StressDefinition;

    .line 997
    .local v0, "element":Lspeech/patts/StressDefinition;
    invoke-virtual {v0}, Lspeech/patts/StressDefinition;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_a

    goto/16 :goto_0

    .line 999
    .end local v0    # "element":Lspeech/patts/StressDefinition;
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPhonemeFallbackMapList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    .line 1000
    .local v0, "element":Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    invoke-virtual {v0}, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_c

    goto/16 :goto_0

    .line 1002
    .end local v0    # "element":Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    :cond_d
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getCompoundMergeRulesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/MergeCompounds;

    .line 1003
    .local v0, "element":Lspeech/patts/MergeCompounds;
    invoke-virtual {v0}, Lspeech/patts/MergeCompounds;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_e

    goto/16 :goto_0

    .line 1005
    .end local v0    # "element":Lspeech/patts/MergeCompounds;
    :cond_f
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getVowelPhonemeReductionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    .line 1006
    .local v0, "element":Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    invoke-virtual {v0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_10

    goto/16 :goto_0

    .line 1008
    .end local v0    # "element":Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    :cond_11
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->toBuilder()Lspeech/patts/PhonologyDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 1

    .prologue
    .line 1304
    invoke-static {p0}, Lspeech/patts/PhonologyDefinitions;->newBuilder(Lspeech/patts/PhonologyDefinitions;)Lspeech/patts/PhonologyDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1013
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getSerializedSize()I

    .line 1014
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasLanguage()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1015
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 1017
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasRegion()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1018
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getRegion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 1020
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPhonemeDefList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonemeDefinition;

    .line 1021
    .local v0, "element":Lspeech/patts/PhonemeDefinition;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 1023
    .end local v0    # "element":Lspeech/patts/PhonemeDefinition;
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasWordidDefinition()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1024
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getWordidDefinition()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 1026
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasSpellingDefinition()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1027
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getSpellingDefinition()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 1029
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasLegalizingVowel()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1030
    const/4 v2, 0x6

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getLegalizingVowel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 1032
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getOnsetList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/ConsonantCluster;

    .line 1033
    .local v0, "element":Lspeech/patts/ConsonantCluster;
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 1035
    .end local v0    # "element":Lspeech/patts/ConsonantCluster;
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getCodaList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/ConsonantCluster;

    .line 1036
    .restart local v0    # "element":Lspeech/patts/ConsonantCluster;
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    .line 1038
    .end local v0    # "element":Lspeech/patts/ConsonantCluster;
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getForeignOnsetList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/ConsonantCluster;

    .line 1039
    .restart local v0    # "element":Lspeech/patts/ConsonantCluster;
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    .line 1041
    .end local v0    # "element":Lspeech/patts/ConsonantCluster;
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getForeignCodaList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/ConsonantCluster;

    .line 1042
    .restart local v0    # "element":Lspeech/patts/ConsonantCluster;
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_4

    .line 1044
    .end local v0    # "element":Lspeech/patts/ConsonantCluster;
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getForeignPhonemeDefList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonemeDefinition;

    .line 1045
    .local v0, "element":Lspeech/patts/PhonemeDefinition;
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_5

    .line 1047
    .end local v0    # "element":Lspeech/patts/PhonemeDefinition;
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getStressDefList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/StressDefinition;

    .line 1048
    .local v0, "element":Lspeech/patts/StressDefinition;
    const/16 v2, 0x12

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_6

    .line 1050
    .end local v0    # "element":Lspeech/patts/StressDefinition;
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasMaxOnsetOrder()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1051
    const/16 v2, 0x13

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getMaxOnsetOrder()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 1053
    :cond_c
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasMaxCodaOrder()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1054
    const/16 v2, 0x14

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getMaxCodaOrder()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 1056
    :cond_d
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasOrthographicStressLetters()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1057
    const/16 v2, 0x15

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getOrthographicStressLetters()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 1059
    :cond_e
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPrimaryStressTriggersList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/PrimaryStressTriggers;

    .line 1060
    .local v0, "element":Lspeech/patts/PrimaryStressTriggers;
    const/16 v2, 0x16

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_7

    .line 1062
    .end local v0    # "element":Lspeech/patts/PrimaryStressTriggers;
    :cond_f
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getBannedRhymeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Rhyme;

    .line 1063
    .local v0, "element":Lspeech/patts/Rhyme;
    const/16 v2, 0x17

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_8

    .line 1065
    .end local v0    # "element":Lspeech/patts/Rhyme;
    :cond_10
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPermittedRhymeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Rhyme;

    .line 1066
    .restart local v0    # "element":Lspeech/patts/Rhyme;
    const/16 v2, 0x18

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_9

    .line 1068
    .end local v0    # "element":Lspeech/patts/Rhyme;
    :cond_11
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPhonemeFallbackMapList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;

    .line 1069
    .local v0, "element":Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    const/16 v2, 0x19

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_a

    .line 1071
    .end local v0    # "element":Lspeech/patts/PhonologyDefinitions$PhonemeFallbackMap;
    :cond_12
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasCompoundStressPosition()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1072
    const/16 v2, 0x1a

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getCompoundStressPosition()Lspeech/patts/PhonologyDefinitions$StressPosition;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/PhonologyDefinitions$StressPosition;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 1074
    :cond_13
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasCompoundMergeSameConsonants()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1075
    const/16 v2, 0x1b

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getCompoundMergeSameConsonants()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 1077
    :cond_14
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getCompoundMergeRulesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/MergeCompounds;

    .line 1078
    .local v0, "element":Lspeech/patts/MergeCompounds;
    const/16 v2, 0x1c

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_b

    .line 1080
    .end local v0    # "element":Lspeech/patts/MergeCompounds;
    :cond_15
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasSinglePrimaryStressRequired()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1081
    const/16 v2, 0x1d

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getSinglePrimaryStressRequired()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 1083
    :cond_16
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getLegalGeminateList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1084
    .local v0, "element":Ljava/lang/String;
    const/16 v2, 0x1e

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_c

    .line 1086
    .end local v0    # "element":Ljava/lang/String;
    :cond_17
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getPrimaryStressList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1087
    .local v0, "element":I
    const/16 v2, 0x1f

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    goto :goto_d

    .line 1089
    .end local v0    # "element":I
    :cond_18
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasSpellingStressMark()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1090
    const/16 v2, 0x20

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getSpellingStressMark()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 1092
    :cond_19
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->hasVowelLetters()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1093
    const/16 v2, 0x21

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getVowelLetters()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 1095
    :cond_1a
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions;->getVowelPhonemeReductionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    .line 1096
    .local v0, "element":Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    const/16 v2, 0x22

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_e

    .line 1098
    .end local v0    # "element":Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    :cond_1b
    return-void
.end method
