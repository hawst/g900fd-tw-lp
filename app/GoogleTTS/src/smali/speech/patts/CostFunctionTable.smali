.class public final Lspeech/patts/CostFunctionTable;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "CostFunctionTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/CostFunctionTable$1;,
        Lspeech/patts/CostFunctionTable$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/CostFunctionTable;


# instance fields
.field private hasName:Z

.field private hasSize:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private size_:I

.field private weights_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 371
    new-instance v0, Lspeech/patts/CostFunctionTable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/CostFunctionTable;-><init>(Z)V

    sput-object v0, Lspeech/patts/CostFunctionTable;->defaultInstance:Lspeech/patts/CostFunctionTable;

    .line 372
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 373
    sget-object v0, Lspeech/patts/CostFunctionTable;->defaultInstance:Lspeech/patts/CostFunctionTable;

    invoke-direct {v0}, Lspeech/patts/CostFunctionTable;->initFields()V

    .line 374
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/CostFunctionTable;->name_:Ljava/lang/String;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/CostFunctionTable;->size_:I

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;

    .line 70
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/CostFunctionTable;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/CostFunctionTable;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/CostFunctionTable$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/CostFunctionTable$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/CostFunctionTable;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/CostFunctionTable;->name_:Ljava/lang/String;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/CostFunctionTable;->size_:I

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;

    .line 70
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/CostFunctionTable;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/CostFunctionTable;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunctionTable;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/CostFunctionTable;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctionTable;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/CostFunctionTable;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctionTable;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/CostFunctionTable;->hasName:Z

    return p1
.end method

.method static synthetic access$502(Lspeech/patts/CostFunctionTable;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctionTable;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctionTable;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lspeech/patts/CostFunctionTable;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctionTable;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/CostFunctionTable;->hasSize:Z

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/CostFunctionTable;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctionTable;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/CostFunctionTable;->size_:I

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/CostFunctionTable;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/CostFunctionTable;->defaultInstance:Lspeech/patts/CostFunctionTable;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public static newBuilder()Lspeech/patts/CostFunctionTable$Builder;
    .locals 1

    .prologue
    .line 161
    # invokes: Lspeech/patts/CostFunctionTable$Builder;->create()Lspeech/patts/CostFunctionTable$Builder;
    invoke-static {}, Lspeech/patts/CostFunctionTable$Builder;->access$100()Lspeech/patts/CostFunctionTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/CostFunctionTable;)Lspeech/patts/CostFunctionTable$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/CostFunctionTable;

    .prologue
    .line 164
    invoke-static {}, Lspeech/patts/CostFunctionTable;->newBuilder()Lspeech/patts/CostFunctionTable$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/CostFunctionTable$Builder;->mergeFrom(Lspeech/patts/CostFunctionTable;)Lspeech/patts/CostFunctionTable$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->getDefaultInstanceForType()Lspeech/patts/CostFunctionTable;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/CostFunctionTable;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/CostFunctionTable;->defaultInstance:Lspeech/patts/CostFunctionTable;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/CostFunctionTable;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 72
    iget v1, p0, Lspeech/patts/CostFunctionTable;->memoizedSerializedSize:I

    .line 73
    .local v1, "size":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 91
    .end local v1    # "size":I
    .local v2, "size":I
    :goto_0
    return v2

    .line 75
    .end local v2    # "size":I
    .restart local v1    # "size":I
    :cond_0
    const/4 v1, 0x0

    .line 76
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->hasName()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 77
    const/4 v3, 0x1

    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 80
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->hasSize()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 81
    const/4 v3, 0x2

    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->getSize()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 85
    :cond_2
    const/4 v0, 0x0

    .line 86
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->getWeightsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 87
    add-int/2addr v1, v0

    .line 88
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->getWeightsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v1, v3

    .line 90
    iput v1, p0, Lspeech/patts/CostFunctionTable;->memoizedSerializedSize:I

    move v2, v1

    .line 91
    .end local v1    # "size":I
    .restart local v2    # "size":I
    goto :goto_0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lspeech/patts/CostFunctionTable;->size_:I

    return v0
.end method

.method public getWeightsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lspeech/patts/CostFunctionTable;->weights_:Ljava/util/List;

    return-object v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/CostFunctionTable;->hasName:Z

    return v0
.end method

.method public hasSize()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/CostFunctionTable;->hasSize:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 51
    iget-boolean v1, p0, Lspeech/patts/CostFunctionTable;->hasName:Z

    if-nez v1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v0

    .line 52
    :cond_1
    iget-boolean v1, p0, Lspeech/patts/CostFunctionTable;->hasSize:Z

    if-eqz v1, :cond_0

    .line 53
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->toBuilder()Lspeech/patts/CostFunctionTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/CostFunctionTable$Builder;
    .locals 1

    .prologue
    .line 166
    invoke-static {p0}, Lspeech/patts/CostFunctionTable;->newBuilder(Lspeech/patts/CostFunctionTable;)Lspeech/patts/CostFunctionTable$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->getSerializedSize()I

    .line 59
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->hasName()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 62
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->hasSize()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 63
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->getSize()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 65
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/CostFunctionTable;->getWeightsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 66
    .local v0, "element":F
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    goto :goto_0

    .line 68
    .end local v0    # "element":F
    :cond_2
    return-void
.end method
