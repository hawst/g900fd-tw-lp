.class public final Lspeech/patts/MergeCompounds$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "MergeCompounds.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/MergeCompounds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/MergeCompounds;",
        "Lspeech/patts/MergeCompounds$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/MergeCompounds;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/MergeCompounds$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-static {}, Lspeech/patts/MergeCompounds$Builder;->create()Lspeech/patts/MergeCompounds$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/MergeCompounds$Builder;
    .locals 3

    .prologue
    .line 156
    new-instance v0, Lspeech/patts/MergeCompounds$Builder;

    invoke-direct {v0}, Lspeech/patts/MergeCompounds$Builder;-><init>()V

    .line 157
    .local v0, "builder":Lspeech/patts/MergeCompounds$Builder;
    new-instance v1, Lspeech/patts/MergeCompounds;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/MergeCompounds;-><init>(Lspeech/patts/MergeCompounds$1;)V

    iput-object v1, v0, Lspeech/patts/MergeCompounds$Builder;->result:Lspeech/patts/MergeCompounds;

    .line 158
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lspeech/patts/MergeCompounds$Builder;->build()Lspeech/patts/MergeCompounds;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/MergeCompounds;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lspeech/patts/MergeCompounds$Builder;->result:Lspeech/patts/MergeCompounds;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/MergeCompounds$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lspeech/patts/MergeCompounds$Builder;->result:Lspeech/patts/MergeCompounds;

    invoke-static {v0}, Lspeech/patts/MergeCompounds$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 189
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/MergeCompounds$Builder;->buildPartial()Lspeech/patts/MergeCompounds;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/MergeCompounds;
    .locals 3

    .prologue
    .line 202
    iget-object v1, p0, Lspeech/patts/MergeCompounds$Builder;->result:Lspeech/patts/MergeCompounds;

    if-nez v1, :cond_0

    .line 203
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 206
    :cond_0
    iget-object v0, p0, Lspeech/patts/MergeCompounds$Builder;->result:Lspeech/patts/MergeCompounds;

    .line 207
    .local v0, "returnMe":Lspeech/patts/MergeCompounds;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/MergeCompounds$Builder;->result:Lspeech/patts/MergeCompounds;

    .line 208
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lspeech/patts/MergeCompounds$Builder;->clone()Lspeech/patts/MergeCompounds$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lspeech/patts/MergeCompounds$Builder;->clone()Lspeech/patts/MergeCompounds$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 147
    invoke-virtual {p0}, Lspeech/patts/MergeCompounds$Builder;->clone()Lspeech/patts/MergeCompounds$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/MergeCompounds$Builder;
    .locals 2

    .prologue
    .line 175
    invoke-static {}, Lspeech/patts/MergeCompounds$Builder;->create()Lspeech/patts/MergeCompounds$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/MergeCompounds$Builder;->result:Lspeech/patts/MergeCompounds;

    invoke-virtual {v0, v1}, Lspeech/patts/MergeCompounds$Builder;->mergeFrom(Lspeech/patts/MergeCompounds;)Lspeech/patts/MergeCompounds$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lspeech/patts/MergeCompounds$Builder;->result:Lspeech/patts/MergeCompounds;

    invoke-virtual {v0}, Lspeech/patts/MergeCompounds;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 147
    check-cast p1, Lspeech/patts/MergeCompounds;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/MergeCompounds$Builder;->mergeFrom(Lspeech/patts/MergeCompounds;)Lspeech/patts/MergeCompounds$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/MergeCompounds;)Lspeech/patts/MergeCompounds$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/MergeCompounds;

    .prologue
    .line 212
    invoke-static {}, Lspeech/patts/MergeCompounds;->getDefaultInstance()Lspeech/patts/MergeCompounds;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-object p0

    .line 213
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/MergeCompounds;->hasOriginal()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    invoke-virtual {p1}, Lspeech/patts/MergeCompounds;->getOriginal()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/MergeCompounds$Builder;->setOriginal(Ljava/lang/String;)Lspeech/patts/MergeCompounds$Builder;

    .line 216
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/MergeCompounds;->hasReplacement()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {p1}, Lspeech/patts/MergeCompounds;->getReplacement()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/MergeCompounds$Builder;->setReplacement(Ljava/lang/String;)Lspeech/patts/MergeCompounds$Builder;

    goto :goto_0
.end method

.method public setOriginal(Ljava/lang/String;)Lspeech/patts/MergeCompounds$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 258
    if-nez p1, :cond_0

    .line 259
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 261
    :cond_0
    iget-object v0, p0, Lspeech/patts/MergeCompounds$Builder;->result:Lspeech/patts/MergeCompounds;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/MergeCompounds;->hasOriginal:Z
    invoke-static {v0, v1}, Lspeech/patts/MergeCompounds;->access$302(Lspeech/patts/MergeCompounds;Z)Z

    .line 262
    iget-object v0, p0, Lspeech/patts/MergeCompounds$Builder;->result:Lspeech/patts/MergeCompounds;

    # setter for: Lspeech/patts/MergeCompounds;->original_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/MergeCompounds;->access$402(Lspeech/patts/MergeCompounds;Ljava/lang/String;)Ljava/lang/String;

    .line 263
    return-object p0
.end method

.method public setReplacement(Ljava/lang/String;)Lspeech/patts/MergeCompounds$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 279
    if-nez p1, :cond_0

    .line 280
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 282
    :cond_0
    iget-object v0, p0, Lspeech/patts/MergeCompounds$Builder;->result:Lspeech/patts/MergeCompounds;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/MergeCompounds;->hasReplacement:Z
    invoke-static {v0, v1}, Lspeech/patts/MergeCompounds;->access$502(Lspeech/patts/MergeCompounds;Z)Z

    .line 283
    iget-object v0, p0, Lspeech/patts/MergeCompounds$Builder;->result:Lspeech/patts/MergeCompounds;

    # setter for: Lspeech/patts/MergeCompounds;->replacement_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/MergeCompounds;->access$602(Lspeech/patts/MergeCompounds;Ljava/lang/String;)Ljava/lang/String;

    .line 284
    return-object p0
.end method
