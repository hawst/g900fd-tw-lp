.class public final Lspeech/patts/HmmDiagsProto$HmmModel$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "HmmDiagsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/HmmDiagsProto$HmmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/HmmDiagsProto$HmmModel;",
        "Lspeech/patts/HmmDiagsProto$HmmModel$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/HmmDiagsProto$HmmModel;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$800()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;
    .locals 1

    .prologue
    .line 458
    invoke-static {}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->create()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;
    .locals 3

    .prologue
    .line 467
    new-instance v0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    invoke-direct {v0}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;-><init>()V

    .line 468
    .local v0, "builder":Lspeech/patts/HmmDiagsProto$HmmModel$Builder;
    new-instance v1, Lspeech/patts/HmmDiagsProto$HmmModel;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/HmmDiagsProto$HmmModel;-><init>(Lspeech/patts/HmmDiagsProto$1;)V

    iput-object v1, v0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    .line 469
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 458
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->build()Lspeech/patts/HmmDiagsProto$HmmModel;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/HmmDiagsProto$HmmModel;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 498
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    invoke-static {v0}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 500
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->buildPartial()Lspeech/patts/HmmDiagsProto$HmmModel;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/HmmDiagsProto$HmmModel;
    .locals 3

    .prologue
    .line 513
    iget-object v1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    if-nez v1, :cond_0

    .line 514
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 517
    :cond_0
    iget-object v1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    # getter for: Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/HmmDiagsProto$HmmModel;->access$1000(Lspeech/patts/HmmDiagsProto$HmmModel;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 518
    iget-object v1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    iget-object v2, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    # getter for: Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/HmmDiagsProto$HmmModel;->access$1000(Lspeech/patts/HmmDiagsProto$HmmModel;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/HmmDiagsProto$HmmModel;->access$1002(Lspeech/patts/HmmDiagsProto$HmmModel;Ljava/util/List;)Ljava/util/List;

    .line 521
    :cond_1
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    .line 522
    .local v0, "returnMe":Lspeech/patts/HmmDiagsProto$HmmModel;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    .line 523
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 458
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->clone()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 458
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->clone()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 458
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->clone()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;
    .locals 2

    .prologue
    .line 486
    invoke-static {}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->create()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    invoke-virtual {v0, v1}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->mergeFrom(Lspeech/patts/HmmDiagsProto$HmmModel;)Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    invoke-virtual {v0}, Lspeech/patts/HmmDiagsProto$HmmModel;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 458
    check-cast p1, Lspeech/patts/HmmDiagsProto$HmmModel;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->mergeFrom(Lspeech/patts/HmmDiagsProto$HmmModel;)Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/HmmDiagsProto$HmmModel;)Lspeech/patts/HmmDiagsProto$HmmModel$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/HmmDiagsProto$HmmModel;

    .prologue
    .line 527
    invoke-static {}, Lspeech/patts/HmmDiagsProto$HmmModel;->getDefaultInstance()Lspeech/patts/HmmDiagsProto$HmmModel;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 537
    :cond_0
    :goto_0
    return-object p0

    .line 528
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/HmmDiagsProto$HmmModel;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 529
    invoke-virtual {p1}, Lspeech/patts/HmmDiagsProto$HmmModel;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->setName(Ljava/lang/String;)Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    .line 531
    :cond_2
    # getter for: Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HmmDiagsProto$HmmModel;->access$1000(Lspeech/patts/HmmDiagsProto$HmmModel;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 532
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    # getter for: Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HmmDiagsProto$HmmModel;->access$1000(Lspeech/patts/HmmDiagsProto$HmmModel;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 533
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/HmmDiagsProto$HmmModel;->access$1002(Lspeech/patts/HmmDiagsProto$HmmModel;Ljava/util/List;)Ljava/util/List;

    .line 535
    :cond_3
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    # getter for: Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/HmmDiagsProto$HmmModel;->access$1000(Lspeech/patts/HmmDiagsProto$HmmModel;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/HmmDiagsProto$HmmModel;->access$1000(Lspeech/patts/HmmDiagsProto$HmmModel;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)Lspeech/patts/HmmDiagsProto$HmmModel$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 578
    if-nez p1, :cond_0

    .line 579
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 581
    :cond_0
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/HmmDiagsProto$HmmModel;->hasName:Z
    invoke-static {v0, v1}, Lspeech/patts/HmmDiagsProto$HmmModel;->access$1102(Lspeech/patts/HmmDiagsProto$HmmModel;Z)Z

    .line 582
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->result:Lspeech/patts/HmmDiagsProto$HmmModel;

    # setter for: Lspeech/patts/HmmDiagsProto$HmmModel;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/HmmDiagsProto$HmmModel;->access$1202(Lspeech/patts/HmmDiagsProto$HmmModel;Ljava/lang/String;)Ljava/lang/String;

    .line 583
    return-object p0
.end method
