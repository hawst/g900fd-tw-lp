.class public final Lspeech/patts/AttributeMapProto$Mapping$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "AttributeMapProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/AttributeMapProto$Mapping;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/AttributeMapProto$Mapping;",
        "Lspeech/patts/AttributeMapProto$Mapping$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/AttributeMapProto$Mapping;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/AttributeMapProto$Mapping$Builder;
    .locals 1

    .prologue
    .line 179
    invoke-static {}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->create()Lspeech/patts/AttributeMapProto$Mapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/AttributeMapProto$Mapping$Builder;
    .locals 3

    .prologue
    .line 188
    new-instance v0, Lspeech/patts/AttributeMapProto$Mapping$Builder;

    invoke-direct {v0}, Lspeech/patts/AttributeMapProto$Mapping$Builder;-><init>()V

    .line 189
    .local v0, "builder":Lspeech/patts/AttributeMapProto$Mapping$Builder;
    new-instance v1, Lspeech/patts/AttributeMapProto$Mapping;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/AttributeMapProto$Mapping;-><init>(Lspeech/patts/AttributeMapProto$1;)V

    iput-object v1, v0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    .line 190
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->build()Lspeech/patts/AttributeMapProto$Mapping;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/AttributeMapProto$Mapping;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    invoke-static {v0}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 221
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->buildPartial()Lspeech/patts/AttributeMapProto$Mapping;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/AttributeMapProto$Mapping;
    .locals 3

    .prologue
    .line 234
    iget-object v1, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    if-nez v1, :cond_0

    .line 235
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 238
    :cond_0
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    .line 239
    .local v0, "returnMe":Lspeech/patts/AttributeMapProto$Mapping;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    .line 240
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->clone()Lspeech/patts/AttributeMapProto$Mapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->clone()Lspeech/patts/AttributeMapProto$Mapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 179
    invoke-virtual {p0}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->clone()Lspeech/patts/AttributeMapProto$Mapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/AttributeMapProto$Mapping$Builder;
    .locals 2

    .prologue
    .line 207
    invoke-static {}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->create()Lspeech/patts/AttributeMapProto$Mapping$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    invoke-virtual {v0, v1}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->mergeFrom(Lspeech/patts/AttributeMapProto$Mapping;)Lspeech/patts/AttributeMapProto$Mapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    invoke-virtual {v0}, Lspeech/patts/AttributeMapProto$Mapping;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 179
    check-cast p1, Lspeech/patts/AttributeMapProto$Mapping;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->mergeFrom(Lspeech/patts/AttributeMapProto$Mapping;)Lspeech/patts/AttributeMapProto$Mapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/AttributeMapProto$Mapping;)Lspeech/patts/AttributeMapProto$Mapping$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/AttributeMapProto$Mapping;

    .prologue
    .line 244
    invoke-static {}, Lspeech/patts/AttributeMapProto$Mapping;->getDefaultInstance()Lspeech/patts/AttributeMapProto$Mapping;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-object p0

    .line 245
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/AttributeMapProto$Mapping;->hasCategory()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 246
    invoke-virtual {p1}, Lspeech/patts/AttributeMapProto$Mapping;->getCategory()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->setCategory(Ljava/lang/String;)Lspeech/patts/AttributeMapProto$Mapping$Builder;

    .line 248
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/AttributeMapProto$Mapping;->hasSource()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 249
    invoke-virtual {p1}, Lspeech/patts/AttributeMapProto$Mapping;->getSource()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->setSource(Ljava/lang/String;)Lspeech/patts/AttributeMapProto$Mapping$Builder;

    .line 251
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/AttributeMapProto$Mapping;->hasDest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {p1}, Lspeech/patts/AttributeMapProto$Mapping;->getDest()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/AttributeMapProto$Mapping$Builder;->setDest(Ljava/lang/String;)Lspeech/patts/AttributeMapProto$Mapping$Builder;

    goto :goto_0
.end method

.method public setCategory(Ljava/lang/String;)Lspeech/patts/AttributeMapProto$Mapping$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 297
    if-nez p1, :cond_0

    .line 298
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 300
    :cond_0
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/AttributeMapProto$Mapping;->hasCategory:Z
    invoke-static {v0, v1}, Lspeech/patts/AttributeMapProto$Mapping;->access$302(Lspeech/patts/AttributeMapProto$Mapping;Z)Z

    .line 301
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    # setter for: Lspeech/patts/AttributeMapProto$Mapping;->category_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/AttributeMapProto$Mapping;->access$402(Lspeech/patts/AttributeMapProto$Mapping;Ljava/lang/String;)Ljava/lang/String;

    .line 302
    return-object p0
.end method

.method public setDest(Ljava/lang/String;)Lspeech/patts/AttributeMapProto$Mapping$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 339
    if-nez p1, :cond_0

    .line 340
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 342
    :cond_0
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/AttributeMapProto$Mapping;->hasDest:Z
    invoke-static {v0, v1}, Lspeech/patts/AttributeMapProto$Mapping;->access$702(Lspeech/patts/AttributeMapProto$Mapping;Z)Z

    .line 343
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    # setter for: Lspeech/patts/AttributeMapProto$Mapping;->dest_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/AttributeMapProto$Mapping;->access$802(Lspeech/patts/AttributeMapProto$Mapping;Ljava/lang/String;)Ljava/lang/String;

    .line 344
    return-object p0
.end method

.method public setSource(Ljava/lang/String;)Lspeech/patts/AttributeMapProto$Mapping$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 318
    if-nez p1, :cond_0

    .line 319
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 321
    :cond_0
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/AttributeMapProto$Mapping;->hasSource:Z
    invoke-static {v0, v1}, Lspeech/patts/AttributeMapProto$Mapping;->access$502(Lspeech/patts/AttributeMapProto$Mapping;Z)Z

    .line 322
    iget-object v0, p0, Lspeech/patts/AttributeMapProto$Mapping$Builder;->result:Lspeech/patts/AttributeMapProto$Mapping;

    # setter for: Lspeech/patts/AttributeMapProto$Mapping;->source_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/AttributeMapProto$Mapping;->access$602(Lspeech/patts/AttributeMapProto$Mapping;Ljava/lang/String;)Ljava/lang/String;

    .line 323
    return-object p0
.end method
