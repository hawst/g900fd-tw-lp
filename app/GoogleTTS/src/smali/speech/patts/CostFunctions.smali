.class public final Lspeech/patts/CostFunctions;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "CostFunctions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/CostFunctions$1;,
        Lspeech/patts/CostFunctions$Builder;,
        Lspeech/patts/CostFunctions$UnitType;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/CostFunctions;


# instance fields
.field private basetypeJoin_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunction;",
            ">;"
        }
    .end annotation
.end field

.field private basetypeTarget_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunction;",
            ">;"
        }
    .end annotation
.end field

.field private hasUnitType:Z

.field private memoizedSerializedSize:I

.field private modelJoin_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunction;",
            ">;"
        }
    .end annotation
.end field

.field private modelTarget_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunction;",
            ">;"
        }
    .end annotation
.end field

.field private sparseTable_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunctionSparseTable;",
            ">;"
        }
    .end annotation
.end field

.field private table_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunctionTable;",
            ">;"
        }
    .end annotation
.end field

.field private unitJoin_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunction;",
            ">;"
        }
    .end annotation
.end field

.field private unitTarget_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunction;",
            ">;"
        }
    .end annotation
.end field

.field private unitType_:Lspeech/patts/CostFunctions$UnitType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1008
    new-instance v0, Lspeech/patts/CostFunctions;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/CostFunctions;-><init>(Z)V

    sput-object v0, Lspeech/patts/CostFunctions;->defaultInstance:Lspeech/patts/CostFunctions;

    .line 1009
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 1010
    sget-object v0, Lspeech/patts/CostFunctions;->defaultInstance:Lspeech/patts/CostFunctions;

    invoke-direct {v0}, Lspeech/patts/CostFunctions;->initFields()V

    .line 1011
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 63
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;

    .line 75
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;

    .line 87
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;

    .line 99
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;

    .line 111
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;

    .line 123
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;

    .line 135
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->table_:Ljava/util/List;

    .line 147
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;

    .line 227
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/CostFunctions;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/CostFunctions;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/CostFunctions$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/CostFunctions$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/CostFunctions;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 63
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;

    .line 75
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;

    .line 87
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;

    .line 99
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;

    .line 111
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;

    .line 123
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;

    .line 135
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->table_:Ljava/util/List;

    .line 147
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;

    .line 227
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/CostFunctions;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1000(Lspeech/patts/CostFunctions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunctions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1002(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1102(Lspeech/patts/CostFunctions;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctions;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/CostFunctions;->hasUnitType:Z

    return p1
.end method

.method static synthetic access$1202(Lspeech/patts/CostFunctions;Lspeech/patts/CostFunctions$UnitType;)Lspeech/patts/CostFunctions$UnitType;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctions;
    .param p1, "x1"    # Lspeech/patts/CostFunctions$UnitType;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctions;->unitType_:Lspeech/patts/CostFunctions$UnitType;

    return-object p1
.end method

.method static synthetic access$300(Lspeech/patts/CostFunctions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunctions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lspeech/patts/CostFunctions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunctions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lspeech/patts/CostFunctions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunctions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lspeech/patts/CostFunctions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunctions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lspeech/patts/CostFunctions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunctions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$800(Lspeech/patts/CostFunctions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunctions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$802(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$900(Lspeech/patts/CostFunctions;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/CostFunctions;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/CostFunctions;->table_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$902(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/CostFunctions;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/CostFunctions;->table_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/CostFunctions;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/CostFunctions;->defaultInstance:Lspeech/patts/CostFunctions;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lspeech/patts/CostFunctions$UnitType;->PHONE:Lspeech/patts/CostFunctions$UnitType;

    iput-object v0, p0, Lspeech/patts/CostFunctions;->unitType_:Lspeech/patts/CostFunctions$UnitType;

    .line 166
    return-void
.end method

.method public static newBuilder()Lspeech/patts/CostFunctions$Builder;
    .locals 1

    .prologue
    .line 340
    # invokes: Lspeech/patts/CostFunctions$Builder;->create()Lspeech/patts/CostFunctions$Builder;
    invoke-static {}, Lspeech/patts/CostFunctions$Builder;->access$100()Lspeech/patts/CostFunctions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/CostFunctions;)Lspeech/patts/CostFunctions$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/CostFunctions;

    .prologue
    .line 343
    invoke-static {}, Lspeech/patts/CostFunctions;->newBuilder()Lspeech/patts/CostFunctions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/CostFunctions$Builder;->mergeFrom(Lspeech/patts/CostFunctions;)Lspeech/patts/CostFunctions$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBasetypeJoinList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;

    return-object v0
.end method

.method public getBasetypeTargetList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getDefaultInstanceForType()Lspeech/patts/CostFunctions;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/CostFunctions;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/CostFunctions;->defaultInstance:Lspeech/patts/CostFunctions;

    return-object v0
.end method

.method public getModelJoinList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;

    return-object v0
.end method

.method public getModelTargetList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 229
    iget v2, p0, Lspeech/patts/CostFunctions;->memoizedSerializedSize:I

    .line 230
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 270
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 232
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 233
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getBasetypeTargetList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 234
    .local v0, "element":Lspeech/patts/CostFunction;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 236
    goto :goto_1

    .line 237
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getBasetypeJoinList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 238
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 240
    goto :goto_2

    .line 241
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getModelTargetList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 242
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 244
    goto :goto_3

    .line 245
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getModelJoinList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 246
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    const/4 v4, 0x4

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 248
    goto :goto_4

    .line 249
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getUnitTargetList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 250
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    const/4 v4, 0x5

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 252
    goto :goto_5

    .line 253
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getUnitJoinList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 254
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    const/4 v4, 0x6

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 256
    goto :goto_6

    .line 257
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getTableList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunctionTable;

    .line 258
    .local v0, "element":Lspeech/patts/CostFunctionTable;
    const/4 v4, 0x7

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 260
    goto :goto_7

    .line 261
    .end local v0    # "element":Lspeech/patts/CostFunctionTable;
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getSparseTableList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunctionSparseTable;

    .line 262
    .local v0, "element":Lspeech/patts/CostFunctionSparseTable;
    const/16 v4, 0x8

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 264
    goto :goto_8

    .line 265
    .end local v0    # "element":Lspeech/patts/CostFunctionSparseTable;
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->hasUnitType()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 266
    const/16 v4, 0x9

    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getUnitType()Lspeech/patts/CostFunctions$UnitType;

    move-result-object v5

    invoke-virtual {v5}, Lspeech/patts/CostFunctions$UnitType;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 269
    :cond_9
    iput v2, p0, Lspeech/patts/CostFunctions;->memoizedSerializedSize:I

    move v3, v2

    .line 270
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto/16 :goto_0
.end method

.method public getSparseTableList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunctionSparseTable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;

    return-object v0
.end method

.method public getTableList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunctionTable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lspeech/patts/CostFunctions;->table_:Ljava/util/List;

    return-object v0
.end method

.method public getUnitJoinList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;

    return-object v0
.end method

.method public getUnitTargetList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/CostFunction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;

    return-object v0
.end method

.method public getUnitType()Lspeech/patts/CostFunctions$UnitType;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lspeech/patts/CostFunctions;->unitType_:Lspeech/patts/CostFunctions$UnitType;

    return-object v0
.end method

.method public hasUnitType()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lspeech/patts/CostFunctions;->hasUnitType:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 168
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getBasetypeTargetList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 169
    .local v0, "element":Lspeech/patts/CostFunction;
    invoke-virtual {v0}, Lspeech/patts/CostFunction;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 192
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :goto_0
    return v2

    .line 171
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getBasetypeJoinList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 172
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    invoke-virtual {v0}, Lspeech/patts/CostFunction;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 174
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getModelTargetList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 175
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    invoke-virtual {v0}, Lspeech/patts/CostFunction;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_4

    goto :goto_0

    .line 177
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getModelJoinList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 178
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    invoke-virtual {v0}, Lspeech/patts/CostFunction;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_6

    goto :goto_0

    .line 180
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getUnitTargetList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 181
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    invoke-virtual {v0}, Lspeech/patts/CostFunction;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_8

    goto :goto_0

    .line 183
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getUnitJoinList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 184
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    invoke-virtual {v0}, Lspeech/patts/CostFunction;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_a

    goto/16 :goto_0

    .line 186
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getTableList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunctionTable;

    .line 187
    .local v0, "element":Lspeech/patts/CostFunctionTable;
    invoke-virtual {v0}, Lspeech/patts/CostFunctionTable;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_c

    goto/16 :goto_0

    .line 189
    .end local v0    # "element":Lspeech/patts/CostFunctionTable;
    :cond_d
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getSparseTableList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunctionSparseTable;

    .line 190
    .local v0, "element":Lspeech/patts/CostFunctionSparseTable;
    invoke-virtual {v0}, Lspeech/patts/CostFunctionSparseTable;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_e

    goto/16 :goto_0

    .line 192
    .end local v0    # "element":Lspeech/patts/CostFunctionSparseTable;
    :cond_f
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->toBuilder()Lspeech/patts/CostFunctions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/CostFunctions$Builder;
    .locals 1

    .prologue
    .line 345
    invoke-static {p0}, Lspeech/patts/CostFunctions;->newBuilder(Lspeech/patts/CostFunctions;)Lspeech/patts/CostFunctions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getSerializedSize()I

    .line 198
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getBasetypeTargetList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 199
    .local v0, "element":Lspeech/patts/CostFunction;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 201
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getBasetypeJoinList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 202
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 204
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getModelTargetList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 205
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    .line 207
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getModelJoinList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 208
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    .line 210
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getUnitTargetList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 211
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_4

    .line 213
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getUnitJoinList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunction;

    .line 214
    .restart local v0    # "element":Lspeech/patts/CostFunction;
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_5

    .line 216
    .end local v0    # "element":Lspeech/patts/CostFunction;
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getTableList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunctionTable;

    .line 217
    .local v0, "element":Lspeech/patts/CostFunctionTable;
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_6

    .line 219
    .end local v0    # "element":Lspeech/patts/CostFunctionTable;
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getSparseTableList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/CostFunctionSparseTable;

    .line 220
    .local v0, "element":Lspeech/patts/CostFunctionSparseTable;
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_7

    .line 222
    .end local v0    # "element":Lspeech/patts/CostFunctionSparseTable;
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->hasUnitType()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 223
    const/16 v2, 0x9

    invoke-virtual {p0}, Lspeech/patts/CostFunctions;->getUnitType()Lspeech/patts/CostFunctions$UnitType;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/CostFunctions$UnitType;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 225
    :cond_8
    return-void
.end method
