.class public final Lspeech/patts/SparseTableEntry$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SparseTableEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/SparseTableEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/SparseTableEntry;",
        "Lspeech/patts/SparseTableEntry$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/SparseTableEntry;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/SparseTableEntry$Builder;
    .locals 1

    .prologue
    .line 162
    invoke-static {}, Lspeech/patts/SparseTableEntry$Builder;->create()Lspeech/patts/SparseTableEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/SparseTableEntry$Builder;
    .locals 3

    .prologue
    .line 171
    new-instance v0, Lspeech/patts/SparseTableEntry$Builder;

    invoke-direct {v0}, Lspeech/patts/SparseTableEntry$Builder;-><init>()V

    .line 172
    .local v0, "builder":Lspeech/patts/SparseTableEntry$Builder;
    new-instance v1, Lspeech/patts/SparseTableEntry;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/SparseTableEntry;-><init>(Lspeech/patts/SparseTableEntry$1;)V

    iput-object v1, v0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    .line 173
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry$Builder;->build()Lspeech/patts/SparseTableEntry;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/SparseTableEntry;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    iget-object v0, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    invoke-static {v0}, Lspeech/patts/SparseTableEntry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 204
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry$Builder;->buildPartial()Lspeech/patts/SparseTableEntry;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/SparseTableEntry;
    .locals 3

    .prologue
    .line 217
    iget-object v1, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    if-nez v1, :cond_0

    .line 218
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 221
    :cond_0
    iget-object v0, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    .line 222
    .local v0, "returnMe":Lspeech/patts/SparseTableEntry;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    .line 223
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry$Builder;->clone()Lspeech/patts/SparseTableEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry$Builder;->clone()Lspeech/patts/SparseTableEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 162
    invoke-virtual {p0}, Lspeech/patts/SparseTableEntry$Builder;->clone()Lspeech/patts/SparseTableEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/SparseTableEntry$Builder;
    .locals 2

    .prologue
    .line 190
    invoke-static {}, Lspeech/patts/SparseTableEntry$Builder;->create()Lspeech/patts/SparseTableEntry$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    invoke-virtual {v0, v1}, Lspeech/patts/SparseTableEntry$Builder;->mergeFrom(Lspeech/patts/SparseTableEntry;)Lspeech/patts/SparseTableEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    invoke-virtual {v0}, Lspeech/patts/SparseTableEntry;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 162
    check-cast p1, Lspeech/patts/SparseTableEntry;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/SparseTableEntry$Builder;->mergeFrom(Lspeech/patts/SparseTableEntry;)Lspeech/patts/SparseTableEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/SparseTableEntry;)Lspeech/patts/SparseTableEntry$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/SparseTableEntry;

    .prologue
    .line 227
    invoke-static {}, Lspeech/patts/SparseTableEntry;->getDefaultInstance()Lspeech/patts/SparseTableEntry;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 237
    :cond_0
    :goto_0
    return-object p0

    .line 228
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/SparseTableEntry;->hasFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 229
    invoke-virtual {p1}, Lspeech/patts/SparseTableEntry;->getFirst()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/SparseTableEntry$Builder;->setFirst(Ljava/lang/String;)Lspeech/patts/SparseTableEntry$Builder;

    .line 231
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/SparseTableEntry;->hasSecond()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 232
    invoke-virtual {p1}, Lspeech/patts/SparseTableEntry;->getSecond()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/SparseTableEntry$Builder;->setSecond(Ljava/lang/String;)Lspeech/patts/SparseTableEntry$Builder;

    .line 234
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/SparseTableEntry;->hasWeight()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    invoke-virtual {p1}, Lspeech/patts/SparseTableEntry;->getWeight()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/SparseTableEntry$Builder;->setWeight(F)Lspeech/patts/SparseTableEntry$Builder;

    goto :goto_0
.end method

.method public setFirst(Ljava/lang/String;)Lspeech/patts/SparseTableEntry$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 280
    if-nez p1, :cond_0

    .line 281
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 283
    :cond_0
    iget-object v0, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/SparseTableEntry;->hasFirst:Z
    invoke-static {v0, v1}, Lspeech/patts/SparseTableEntry;->access$302(Lspeech/patts/SparseTableEntry;Z)Z

    .line 284
    iget-object v0, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    # setter for: Lspeech/patts/SparseTableEntry;->first_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/SparseTableEntry;->access$402(Lspeech/patts/SparseTableEntry;Ljava/lang/String;)Ljava/lang/String;

    .line 285
    return-object p0
.end method

.method public setSecond(Ljava/lang/String;)Lspeech/patts/SparseTableEntry$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 301
    if-nez p1, :cond_0

    .line 302
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 304
    :cond_0
    iget-object v0, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/SparseTableEntry;->hasSecond:Z
    invoke-static {v0, v1}, Lspeech/patts/SparseTableEntry;->access$502(Lspeech/patts/SparseTableEntry;Z)Z

    .line 305
    iget-object v0, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    # setter for: Lspeech/patts/SparseTableEntry;->second_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/SparseTableEntry;->access$602(Lspeech/patts/SparseTableEntry;Ljava/lang/String;)Ljava/lang/String;

    .line 306
    return-object p0
.end method

.method public setWeight(F)Lspeech/patts/SparseTableEntry$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 322
    iget-object v0, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/SparseTableEntry;->hasWeight:Z
    invoke-static {v0, v1}, Lspeech/patts/SparseTableEntry;->access$702(Lspeech/patts/SparseTableEntry;Z)Z

    .line 323
    iget-object v0, p0, Lspeech/patts/SparseTableEntry$Builder;->result:Lspeech/patts/SparseTableEntry;

    # setter for: Lspeech/patts/SparseTableEntry;->weight_:F
    invoke-static {v0, p1}, Lspeech/patts/SparseTableEntry;->access$802(Lspeech/patts/SparseTableEntry;F)F

    .line 324
    return-object p0
.end method
