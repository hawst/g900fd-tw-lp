.class public final Lspeech/patts/FeatureSet;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FeatureSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/FeatureSet$1;,
        Lspeech/patts/FeatureSet$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/FeatureSet;


# instance fields
.field private acoustic_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/AcousticFeature;",
            ">;"
        }
    .end annotation
.end field

.field private custom_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private nf_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/NavigationFeature;",
            ">;"
        }
    .end annotation
.end field

.field private question_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Question;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 552
    new-instance v0, Lspeech/patts/FeatureSet;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/FeatureSet;-><init>(Z)V

    sput-object v0, Lspeech/patts/FeatureSet;->defaultInstance:Lspeech/patts/FeatureSet;

    .line 553
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 554
    sget-object v0, Lspeech/patts/FeatureSet;->defaultInstance:Lspeech/patts/FeatureSet;

    invoke-direct {v0}, Lspeech/patts/FeatureSet;->initFields()V

    .line 555
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureSet;->question_:Ljava/util/List;

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;

    .line 99
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/FeatureSet;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/FeatureSet;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/FeatureSet$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/FeatureSet$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/FeatureSet;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureSet;->question_:Ljava/util/List;

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;

    .line 99
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/FeatureSet;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/FeatureSet;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FeatureSet;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/FeatureSet;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FeatureSet;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lspeech/patts/FeatureSet;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FeatureSet;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FeatureSet;->question_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lspeech/patts/FeatureSet;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FeatureSet;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FeatureSet;->question_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lspeech/patts/FeatureSet;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FeatureSet;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lspeech/patts/FeatureSet;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FeatureSet;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lspeech/patts/FeatureSet;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FeatureSet;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lspeech/patts/FeatureSet;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FeatureSet;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/FeatureSet;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/FeatureSet;->defaultInstance:Lspeech/patts/FeatureSet;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public static newBuilder()Lspeech/patts/FeatureSet$Builder;
    .locals 1

    .prologue
    .line 197
    # invokes: Lspeech/patts/FeatureSet$Builder;->create()Lspeech/patts/FeatureSet$Builder;
    invoke-static {}, Lspeech/patts/FeatureSet$Builder;->access$100()Lspeech/patts/FeatureSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/FeatureSet;)Lspeech/patts/FeatureSet$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/FeatureSet;

    .prologue
    .line 200
    invoke-static {}, Lspeech/patts/FeatureSet;->newBuilder()Lspeech/patts/FeatureSet$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/FeatureSet$Builder;->mergeFrom(Lspeech/patts/FeatureSet;)Lspeech/patts/FeatureSet$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAcousticList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/AcousticFeature;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;

    return-object v0
.end method

.method public getCustomList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getDefaultInstanceForType()Lspeech/patts/FeatureSet;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/FeatureSet;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/FeatureSet;->defaultInstance:Lspeech/patts/FeatureSet;

    return-object v0
.end method

.method public getNfList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/NavigationFeature;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;

    return-object v0
.end method

.method public getQuestionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Question;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lspeech/patts/FeatureSet;->question_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 101
    iget v3, p0, Lspeech/patts/FeatureSet;->memoizedSerializedSize:I

    .line 102
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 127
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 104
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 105
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getNfList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/NavigationFeature;

    .line 106
    .local v1, "element":Lspeech/patts/NavigationFeature;
    const/4 v5, 0x1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 108
    goto :goto_1

    .line 109
    .end local v1    # "element":Lspeech/patts/NavigationFeature;
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getQuestionList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/Question;

    .line 110
    .local v1, "element":Lspeech/patts/Question;
    const/4 v5, 0x2

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 112
    goto :goto_2

    .line 114
    .end local v1    # "element":Lspeech/patts/Question;
    :cond_2
    const/4 v0, 0x0

    .line 115
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getCustomList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 116
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 118
    goto :goto_3

    .line 119
    .end local v1    # "element":Ljava/lang/String;
    :cond_3
    add-int/2addr v3, v0

    .line 120
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getCustomList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 122
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getAcousticList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lspeech/patts/AcousticFeature;

    .line 123
    .local v1, "element":Lspeech/patts/AcousticFeature;
    const/4 v5, 0x4

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 125
    goto :goto_4

    .line 126
    .end local v1    # "element":Lspeech/patts/AcousticFeature;
    :cond_4
    iput v3, p0, Lspeech/patts/FeatureSet;->memoizedSerializedSize:I

    move v4, v3

    .line 127
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 73
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getNfList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/NavigationFeature;

    .line 74
    .local v0, "element":Lspeech/patts/NavigationFeature;
    invoke-virtual {v0}, Lspeech/patts/NavigationFeature;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 79
    .end local v0    # "element":Lspeech/patts/NavigationFeature;
    :goto_0
    return v2

    .line 76
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getQuestionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Question;

    .line 77
    .local v0, "element":Lspeech/patts/Question;
    invoke-virtual {v0}, Lspeech/patts/Question;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 79
    .end local v0    # "element":Lspeech/patts/Question;
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->toBuilder()Lspeech/patts/FeatureSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/FeatureSet$Builder;
    .locals 1

    .prologue
    .line 202
    invoke-static {p0}, Lspeech/patts/FeatureSet;->newBuilder(Lspeech/patts/FeatureSet;)Lspeech/patts/FeatureSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getSerializedSize()I

    .line 85
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getNfList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/NavigationFeature;

    .line 86
    .local v0, "element":Lspeech/patts/NavigationFeature;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 88
    .end local v0    # "element":Lspeech/patts/NavigationFeature;
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getQuestionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Question;

    .line 89
    .local v0, "element":Lspeech/patts/Question;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 91
    .end local v0    # "element":Lspeech/patts/Question;
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getCustomList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 92
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_2

    .line 94
    .end local v0    # "element":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/FeatureSet;->getAcousticList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/AcousticFeature;

    .line 95
    .local v0, "element":Lspeech/patts/AcousticFeature;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    .line 97
    .end local v0    # "element":Lspeech/patts/AcousticFeature;
    :cond_3
    return-void
.end method
