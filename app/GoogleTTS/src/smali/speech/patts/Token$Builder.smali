.class public final Lspeech/patts/Token$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Token.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Token;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/Token;",
        "Lspeech/patts/Token$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/Token;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 897
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/Token$Builder;
    .locals 1

    .prologue
    .line 891
    invoke-static {}, Lspeech/patts/Token$Builder;->create()Lspeech/patts/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/Token$Builder;
    .locals 3

    .prologue
    .line 900
    new-instance v0, Lspeech/patts/Token$Builder;

    invoke-direct {v0}, Lspeech/patts/Token$Builder;-><init>()V

    .line 901
    .local v0, "builder":Lspeech/patts/Token$Builder;
    new-instance v1, Lspeech/patts/Token;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/Token;-><init>(Lspeech/patts/Token$1;)V

    iput-object v1, v0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    .line 902
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 891
    invoke-virtual {p0}, Lspeech/patts/Token$Builder;->build()Lspeech/patts/Token;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/Token;
    .locals 1

    .prologue
    .line 930
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/Token$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 931
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-static {v0}, Lspeech/patts/Token$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 933
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Token$Builder;->buildPartial()Lspeech/patts/Token;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/Token;
    .locals 3

    .prologue
    .line 946
    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    if-nez v1, :cond_0

    .line 947
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 950
    :cond_0
    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->homograph_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Token;->access$300(Lspeech/patts/Token;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 951
    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v2, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->homograph_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Token;->access$300(Lspeech/patts/Token;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Token;->homograph_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Token;->access$302(Lspeech/patts/Token;Ljava/util/List;)Ljava/util/List;

    .line 954
    :cond_1
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    .line 955
    .local v0, "returnMe":Lspeech/patts/Token;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    .line 956
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 891
    invoke-virtual {p0}, Lspeech/patts/Token$Builder;->clone()Lspeech/patts/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 891
    invoke-virtual {p0}, Lspeech/patts/Token$Builder;->clone()Lspeech/patts/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 891
    invoke-virtual {p0}, Lspeech/patts/Token$Builder;->clone()Lspeech/patts/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/Token$Builder;
    .locals 2

    .prologue
    .line 919
    invoke-static {}, Lspeech/patts/Token$Builder;->create()Lspeech/patts/Token$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0, v1}, Lspeech/patts/Token$Builder;->mergeFrom(Lspeech/patts/Token;)Lspeech/patts/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 927
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeAbbreviation(Lcom/google/speech/tts/Abbreviation;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Abbreviation;

    .prologue
    .line 2273
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasAbbreviation()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->abbreviation_:Lcom/google/speech/tts/Abbreviation;
    invoke-static {v0}, Lspeech/patts/Token;->access$6700(Lspeech/patts/Token;)Lcom/google/speech/tts/Abbreviation;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Abbreviation;->getDefaultInstance()Lcom/google/speech/tts/Abbreviation;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2275
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->abbreviation_:Lcom/google/speech/tts/Abbreviation;
    invoke-static {v1}, Lspeech/patts/Token;->access$6700(Lspeech/patts/Token;)Lcom/google/speech/tts/Abbreviation;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Abbreviation;->newBuilder(Lcom/google/speech/tts/Abbreviation;)Lcom/google/speech/tts/Abbreviation$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Abbreviation$Builder;->mergeFrom(Lcom/google/speech/tts/Abbreviation;)Lcom/google/speech/tts/Abbreviation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Abbreviation$Builder;->buildPartial()Lcom/google/speech/tts/Abbreviation;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->abbreviation_:Lcom/google/speech/tts/Abbreviation;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$6702(Lspeech/patts/Token;Lcom/google/speech/tts/Abbreviation;)Lcom/google/speech/tts/Abbreviation;

    .line 2280
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasAbbreviation:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$6602(Lspeech/patts/Token;Z)Z

    .line 2281
    return-object p0

    .line 2278
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->abbreviation_:Lcom/google/speech/tts/Abbreviation;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$6702(Lspeech/patts/Token;Lcom/google/speech/tts/Abbreviation;)Lcom/google/speech/tts/Abbreviation;

    goto :goto_0
.end method

.method public mergeAddress(Lcom/google/speech/tts/Address;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Address;

    .prologue
    .line 2025
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasAddress()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->address_:Lcom/google/speech/tts/Address;
    invoke-static {v0}, Lspeech/patts/Token;->access$5100(Lspeech/patts/Token;)Lcom/google/speech/tts/Address;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Address;->getDefaultInstance()Lcom/google/speech/tts/Address;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2027
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->address_:Lcom/google/speech/tts/Address;
    invoke-static {v1}, Lspeech/patts/Token;->access$5100(Lspeech/patts/Token;)Lcom/google/speech/tts/Address;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Address;->newBuilder(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Address$Builder;->mergeFrom(Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Address$Builder;->buildPartial()Lcom/google/speech/tts/Address;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->address_:Lcom/google/speech/tts/Address;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$5102(Lspeech/patts/Token;Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address;

    .line 2032
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasAddress:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$5002(Lspeech/patts/Token;Z)Z

    .line 2033
    return-object p0

    .line 2030
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->address_:Lcom/google/speech/tts/Address;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$5102(Lspeech/patts/Token;Lcom/google/speech/tts/Address;)Lcom/google/speech/tts/Address;

    goto :goto_0
.end method

.method public mergeCardinal(Lcom/google/speech/tts/Cardinal;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Cardinal;

    .prologue
    .line 1708
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasCardinal()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v0}, Lspeech/patts/Token;->access$3300(Lspeech/patts/Token;)Lcom/google/speech/tts/Cardinal;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Cardinal;->getDefaultInstance()Lcom/google/speech/tts/Cardinal;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1710
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v1}, Lspeech/patts/Token;->access$3300(Lspeech/patts/Token;)Lcom/google/speech/tts/Cardinal;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Cardinal;->newBuilder(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Cardinal$Builder;->mergeFrom(Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Cardinal$Builder;->buildPartial()Lcom/google/speech/tts/Cardinal;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$3302(Lspeech/patts/Token;Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal;

    .line 1715
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasCardinal:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$3202(Lspeech/patts/Token;Z)Z

    .line 1716
    return-object p0

    .line 1713
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->cardinal_:Lcom/google/speech/tts/Cardinal;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$3302(Lspeech/patts/Token;Lcom/google/speech/tts/Cardinal;)Lcom/google/speech/tts/Cardinal;

    goto :goto_0
.end method

.method public mergeConnector(Lcom/google/speech/tts/Connector;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Connector;

    .prologue
    .line 2215
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasConnector()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->connector_:Lcom/google/speech/tts/Connector;
    invoke-static {v0}, Lspeech/patts/Token;->access$6300(Lspeech/patts/Token;)Lcom/google/speech/tts/Connector;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Connector;->getDefaultInstance()Lcom/google/speech/tts/Connector;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2217
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->connector_:Lcom/google/speech/tts/Connector;
    invoke-static {v1}, Lspeech/patts/Token;->access$6300(Lspeech/patts/Token;)Lcom/google/speech/tts/Connector;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Connector;->newBuilder(Lcom/google/speech/tts/Connector;)Lcom/google/speech/tts/Connector$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Connector$Builder;->mergeFrom(Lcom/google/speech/tts/Connector;)Lcom/google/speech/tts/Connector$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Connector$Builder;->buildPartial()Lcom/google/speech/tts/Connector;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->connector_:Lcom/google/speech/tts/Connector;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$6302(Lspeech/patts/Token;Lcom/google/speech/tts/Connector;)Lcom/google/speech/tts/Connector;

    .line 2222
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasConnector:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$6202(Lspeech/patts/Token;Z)Z

    .line 2223
    return-object p0

    .line 2220
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->connector_:Lcom/google/speech/tts/Connector;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$6302(Lspeech/patts/Token;Lcom/google/speech/tts/Connector;)Lcom/google/speech/tts/Connector;

    goto :goto_0
.end method

.method public mergeDate(Lcom/google/speech/tts/Date;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Date;

    .prologue
    .line 1988
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->date_:Lcom/google/speech/tts/Date;
    invoke-static {v0}, Lspeech/patts/Token;->access$4900(Lspeech/patts/Token;)Lcom/google/speech/tts/Date;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Date;->getDefaultInstance()Lcom/google/speech/tts/Date;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1990
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->date_:Lcom/google/speech/tts/Date;
    invoke-static {v1}, Lspeech/patts/Token;->access$4900(Lspeech/patts/Token;)Lcom/google/speech/tts/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Date;->newBuilder(Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Date$Builder;->mergeFrom(Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Date$Builder;->buildPartial()Lcom/google/speech/tts/Date;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->date_:Lcom/google/speech/tts/Date;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$4902(Lspeech/patts/Token;Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date;

    .line 1995
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasDate:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$4802(Lspeech/patts/Token;Z)Z

    .line 1996
    return-object p0

    .line 1993
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->date_:Lcom/google/speech/tts/Date;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$4902(Lspeech/patts/Token;Lcom/google/speech/tts/Date;)Lcom/google/speech/tts/Date;

    goto :goto_0
.end method

.method public mergeDecimal(Lcom/google/speech/tts/Decimal;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 1803
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasDecimal()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0}, Lspeech/patts/Token;->access$3900(Lspeech/patts/Token;)Lcom/google/speech/tts/Decimal;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Decimal;->getDefaultInstance()Lcom/google/speech/tts/Decimal;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1805
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v1}, Lspeech/patts/Token;->access$3900(Lspeech/patts/Token;)Lcom/google/speech/tts/Decimal;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Decimal;->newBuilder(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Decimal$Builder;->mergeFrom(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Decimal$Builder;->buildPartial()Lcom/google/speech/tts/Decimal;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$3902(Lspeech/patts/Token;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;

    .line 1810
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasDecimal:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$3802(Lspeech/patts/Token;Z)Z

    .line 1811
    return-object p0

    .line 1808
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->decimal_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$3902(Lspeech/patts/Token;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;

    goto :goto_0
.end method

.method public mergeDuplicator(Lcom/google/speech/tts/Duplicator;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Duplicator;

    .prologue
    .line 2310
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasDuplicator()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->duplicator_:Lcom/google/speech/tts/Duplicator;
    invoke-static {v0}, Lspeech/patts/Token;->access$6900(Lspeech/patts/Token;)Lcom/google/speech/tts/Duplicator;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Duplicator;->getDefaultInstance()Lcom/google/speech/tts/Duplicator;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2312
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->duplicator_:Lcom/google/speech/tts/Duplicator;
    invoke-static {v1}, Lspeech/patts/Token;->access$6900(Lspeech/patts/Token;)Lcom/google/speech/tts/Duplicator;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Duplicator;->newBuilder(Lcom/google/speech/tts/Duplicator;)Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Duplicator$Builder;->mergeFrom(Lcom/google/speech/tts/Duplicator;)Lcom/google/speech/tts/Duplicator$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Duplicator$Builder;->buildPartial()Lcom/google/speech/tts/Duplicator;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->duplicator_:Lcom/google/speech/tts/Duplicator;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$6902(Lspeech/patts/Token;Lcom/google/speech/tts/Duplicator;)Lcom/google/speech/tts/Duplicator;

    .line 2317
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasDuplicator:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$6802(Lspeech/patts/Token;Z)Z

    .line 2318
    return-object p0

    .line 2315
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->duplicator_:Lcom/google/speech/tts/Duplicator;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$6902(Lspeech/patts/Token;Lcom/google/speech/tts/Duplicator;)Lcom/google/speech/tts/Duplicator;

    goto :goto_0
.end method

.method public mergeElectronic(Lcom/google/speech/tts/Electronic;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Electronic;

    .prologue
    .line 2136
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasElectronic()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->electronic_:Lcom/google/speech/tts/Electronic;
    invoke-static {v0}, Lspeech/patts/Token;->access$5700(Lspeech/patts/Token;)Lcom/google/speech/tts/Electronic;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Electronic;->getDefaultInstance()Lcom/google/speech/tts/Electronic;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2138
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->electronic_:Lcom/google/speech/tts/Electronic;
    invoke-static {v1}, Lspeech/patts/Token;->access$5700(Lspeech/patts/Token;)Lcom/google/speech/tts/Electronic;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Electronic;->newBuilder(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Electronic$Builder;->mergeFrom(Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Electronic$Builder;->buildPartial()Lcom/google/speech/tts/Electronic;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->electronic_:Lcom/google/speech/tts/Electronic;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$5702(Lspeech/patts/Token;Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic;

    .line 2143
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasElectronic:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$5602(Lspeech/patts/Token;Z)Z

    .line 2144
    return-object p0

    .line 2141
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->electronic_:Lcom/google/speech/tts/Electronic;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$5702(Lspeech/patts/Token;Lcom/google/speech/tts/Electronic;)Lcom/google/speech/tts/Electronic;

    goto :goto_0
.end method

.method public mergeFraction(Lcom/google/speech/tts/Fraction;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Fraction;

    .prologue
    .line 1840
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v0}, Lspeech/patts/Token;->access$4100(Lspeech/patts/Token;)Lcom/google/speech/tts/Fraction;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Fraction;->getDefaultInstance()Lcom/google/speech/tts/Fraction;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1842
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v1}, Lspeech/patts/Token;->access$4100(Lspeech/patts/Token;)Lcom/google/speech/tts/Fraction;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Fraction;->newBuilder(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Fraction$Builder;->mergeFrom(Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Fraction$Builder;->buildPartial()Lcom/google/speech/tts/Fraction;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$4102(Lspeech/patts/Token;Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction;

    .line 1847
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasFraction:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$4002(Lspeech/patts/Token;Z)Z

    .line 1848
    return-object p0

    .line 1845
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->fraction_:Lcom/google/speech/tts/Fraction;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$4102(Lspeech/patts/Token;Lcom/google/speech/tts/Fraction;)Lcom/google/speech/tts/Fraction;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 891
    check-cast p1, Lspeech/patts/Token;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/Token$Builder;->mergeFrom(Lspeech/patts/Token;)Lspeech/patts/Token$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/Token;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/Token;

    .prologue
    .line 960
    invoke-static {}, Lspeech/patts/Token;->getDefaultInstance()Lspeech/patts/Token;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1090
    :cond_0
    :goto_0
    return-object p0

    .line 961
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/Token;->hasParent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 962
    invoke-virtual {p1}, Lspeech/patts/Token;->getParent()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setParent(I)Lspeech/patts/Token$Builder;

    .line 964
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/Token;->hasFirstDaughter()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 965
    invoke-virtual {p1}, Lspeech/patts/Token;->getFirstDaughter()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setFirstDaughter(I)Lspeech/patts/Token$Builder;

    .line 967
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/Token;->hasLastDaughter()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 968
    invoke-virtual {p1}, Lspeech/patts/Token;->getLastDaughter()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setLastDaughter(I)Lspeech/patts/Token$Builder;

    .line 970
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/Token;->hasName()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 971
    invoke-virtual {p1}, Lspeech/patts/Token;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setName(Ljava/lang/String;)Lspeech/patts/Token$Builder;

    .line 973
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/Token;->hasSkip()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 974
    invoke-virtual {p1}, Lspeech/patts/Token;->getSkip()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setSkip(Z)Lspeech/patts/Token$Builder;

    .line 976
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/Token;->hasNextSpace()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 977
    invoke-virtual {p1}, Lspeech/patts/Token;->getNextSpace()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setNextSpace(Z)Lspeech/patts/Token$Builder;

    .line 979
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/Token;->hasType()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 980
    invoke-virtual {p1}, Lspeech/patts/Token;->getType()Lspeech/patts/Token$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setType(Lspeech/patts/Token$Type;)Lspeech/patts/Token$Builder;

    .line 982
    :cond_8
    invoke-virtual {p1}, Lspeech/patts/Token;->hasWordid()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 983
    invoke-virtual {p1}, Lspeech/patts/Token;->getWordid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setWordid(Ljava/lang/String;)Lspeech/patts/Token$Builder;

    .line 985
    :cond_9
    invoke-virtual {p1}, Lspeech/patts/Token;->hasVariant()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 986
    invoke-virtual {p1}, Lspeech/patts/Token;->getVariant()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setVariant(Ljava/lang/String;)Lspeech/patts/Token$Builder;

    .line 988
    :cond_a
    invoke-virtual {p1}, Lspeech/patts/Token;->hasPhraseBreak()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 989
    invoke-virtual {p1}, Lspeech/patts/Token;->getPhraseBreak()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setPhraseBreak(Z)Lspeech/patts/Token$Builder;

    .line 991
    :cond_b
    invoke-virtual {p1}, Lspeech/patts/Token;->hasPauseDuration()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 992
    invoke-virtual {p1}, Lspeech/patts/Token;->getPauseDuration()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setPauseDuration(F)Lspeech/patts/Token$Builder;

    .line 994
    :cond_c
    invoke-virtual {p1}, Lspeech/patts/Token;->hasPauseLength()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 995
    invoke-virtual {p1}, Lspeech/patts/Token;->getPauseLength()Lspeech/patts/Token$PauseLength;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setPauseLength(Lspeech/patts/Token$PauseLength;)Lspeech/patts/Token$Builder;

    .line 997
    :cond_d
    # getter for: Lspeech/patts/Token;->homograph_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Token;->access$300(Lspeech/patts/Token;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 998
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->homograph_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Token;->access$300(Lspeech/patts/Token;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 999
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Token;->homograph_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$302(Lspeech/patts/Token;Ljava/util/List;)Ljava/util/List;

    .line 1001
    :cond_e
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->homograph_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Token;->access$300(Lspeech/patts/Token;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Token;->homograph_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Token;->access$300(Lspeech/patts/Token;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1003
    :cond_f
    invoke-virtual {p1}, Lspeech/patts/Token;->hasSpelling()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1004
    invoke-virtual {p1}, Lspeech/patts/Token;->getSpelling()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setSpelling(Ljava/lang/String;)Lspeech/patts/Token$Builder;

    .line 1006
    :cond_10
    invoke-virtual {p1}, Lspeech/patts/Token;->hasSemioticClass()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1007
    invoke-virtual {p1}, Lspeech/patts/Token;->getSemioticClass()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setSemioticClass(Ljava/lang/String;)Lspeech/patts/Token$Builder;

    .line 1009
    :cond_11
    invoke-virtual {p1}, Lspeech/patts/Token;->hasCardinal()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1010
    invoke-virtual {p1}, Lspeech/patts/Token;->getCardinal()Lcom/google/speech/tts/Cardinal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeCardinal(Lcom/google/speech/tts/Cardinal;)Lspeech/patts/Token$Builder;

    .line 1012
    :cond_12
    invoke-virtual {p1}, Lspeech/patts/Token;->hasOrdinal()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1013
    invoke-virtual {p1}, Lspeech/patts/Token;->getOrdinal()Lcom/google/speech/tts/Ordinal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeOrdinal(Lcom/google/speech/tts/Ordinal;)Lspeech/patts/Token$Builder;

    .line 1015
    :cond_13
    invoke-virtual {p1}, Lspeech/patts/Token;->hasDigit()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1016
    invoke-virtual {p1}, Lspeech/patts/Token;->getDigit()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setDigit(Ljava/lang/String;)Lspeech/patts/Token$Builder;

    .line 1018
    :cond_14
    invoke-virtual {p1}, Lspeech/patts/Token;->hasDecimal()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1019
    invoke-virtual {p1}, Lspeech/patts/Token;->getDecimal()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeDecimal(Lcom/google/speech/tts/Decimal;)Lspeech/patts/Token$Builder;

    .line 1021
    :cond_15
    invoke-virtual {p1}, Lspeech/patts/Token;->hasFraction()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1022
    invoke-virtual {p1}, Lspeech/patts/Token;->getFraction()Lcom/google/speech/tts/Fraction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeFraction(Lcom/google/speech/tts/Fraction;)Lspeech/patts/Token$Builder;

    .line 1024
    :cond_16
    invoke-virtual {p1}, Lspeech/patts/Token;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1025
    invoke-virtual {p1}, Lspeech/patts/Token;->getTime()Lcom/google/speech/tts/Time;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeTime(Lcom/google/speech/tts/Time;)Lspeech/patts/Token$Builder;

    .line 1027
    :cond_17
    invoke-virtual {p1}, Lspeech/patts/Token;->hasMeasure()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1028
    invoke-virtual {p1}, Lspeech/patts/Token;->getMeasure()Lcom/google/speech/tts/Measure;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeMeasure(Lcom/google/speech/tts/Measure;)Lspeech/patts/Token$Builder;

    .line 1030
    :cond_18
    invoke-virtual {p1}, Lspeech/patts/Token;->hasPercent()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1031
    invoke-virtual {p1}, Lspeech/patts/Token;->getPercent()Lcom/google/speech/tts/Decimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergePercent(Lcom/google/speech/tts/Decimal;)Lspeech/patts/Token$Builder;

    .line 1033
    :cond_19
    invoke-virtual {p1}, Lspeech/patts/Token;->hasDate()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1034
    invoke-virtual {p1}, Lspeech/patts/Token;->getDate()Lcom/google/speech/tts/Date;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeDate(Lcom/google/speech/tts/Date;)Lspeech/patts/Token$Builder;

    .line 1036
    :cond_1a
    invoke-virtual {p1}, Lspeech/patts/Token;->hasAddress()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1037
    invoke-virtual {p1}, Lspeech/patts/Token;->getAddress()Lcom/google/speech/tts/Address;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeAddress(Lcom/google/speech/tts/Address;)Lspeech/patts/Token$Builder;

    .line 1039
    :cond_1b
    invoke-virtual {p1}, Lspeech/patts/Token;->hasTelephone()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1040
    invoke-virtual {p1}, Lspeech/patts/Token;->getTelephone()Lcom/google/speech/tts/Telephone;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeTelephone(Lcom/google/speech/tts/Telephone;)Lspeech/patts/Token$Builder;

    .line 1042
    :cond_1c
    invoke-virtual {p1}, Lspeech/patts/Token;->hasMoney()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1043
    invoke-virtual {p1}, Lspeech/patts/Token;->getMoney()Lcom/google/speech/tts/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeMoney(Lcom/google/speech/tts/Money;)Lspeech/patts/Token$Builder;

    .line 1045
    :cond_1d
    invoke-virtual {p1}, Lspeech/patts/Token;->hasElectronic()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1046
    invoke-virtual {p1}, Lspeech/patts/Token;->getElectronic()Lcom/google/speech/tts/Electronic;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeElectronic(Lcom/google/speech/tts/Electronic;)Lspeech/patts/Token$Builder;

    .line 1048
    :cond_1e
    invoke-virtual {p1}, Lspeech/patts/Token;->hasVerbatim()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1049
    invoke-virtual {p1}, Lspeech/patts/Token;->getVerbatim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setVerbatim(Ljava/lang/String;)Lspeech/patts/Token$Builder;

    .line 1051
    :cond_1f
    invoke-virtual {p1}, Lspeech/patts/Token;->hasLetters()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 1052
    invoke-virtual {p1}, Lspeech/patts/Token;->getLetters()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setLetters(Ljava/lang/String;)Lspeech/patts/Token$Builder;

    .line 1054
    :cond_20
    invoke-virtual {p1}, Lspeech/patts/Token;->hasConnector()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 1055
    invoke-virtual {p1}, Lspeech/patts/Token;->getConnector()Lcom/google/speech/tts/Connector;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeConnector(Lcom/google/speech/tts/Connector;)Lspeech/patts/Token$Builder;

    .line 1057
    :cond_21
    invoke-virtual {p1}, Lspeech/patts/Token;->hasConcept()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 1058
    invoke-virtual {p1}, Lspeech/patts/Token;->getConcept()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setConcept(Ljava/lang/String;)Lspeech/patts/Token$Builder;

    .line 1060
    :cond_22
    invoke-virtual {p1}, Lspeech/patts/Token;->hasAbbreviation()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 1061
    invoke-virtual {p1}, Lspeech/patts/Token;->getAbbreviation()Lcom/google/speech/tts/Abbreviation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeAbbreviation(Lcom/google/speech/tts/Abbreviation;)Lspeech/patts/Token$Builder;

    .line 1063
    :cond_23
    invoke-virtual {p1}, Lspeech/patts/Token;->hasDuplicator()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 1064
    invoke-virtual {p1}, Lspeech/patts/Token;->getDuplicator()Lcom/google/speech/tts/Duplicator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeDuplicator(Lcom/google/speech/tts/Duplicator;)Lspeech/patts/Token$Builder;

    .line 1066
    :cond_24
    invoke-virtual {p1}, Lspeech/patts/Token;->hasSyntax()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 1067
    invoke-virtual {p1}, Lspeech/patts/Token;->getSyntax()Lspeech/patts/Syntax;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeSyntax(Lspeech/patts/Syntax;)Lspeech/patts/Token$Builder;

    .line 1069
    :cond_25
    invoke-virtual {p1}, Lspeech/patts/Token;->hasStartIndex()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 1070
    invoke-virtual {p1}, Lspeech/patts/Token;->getStartIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setStartIndex(I)Lspeech/patts/Token$Builder;

    .line 1072
    :cond_26
    invoke-virtual {p1}, Lspeech/patts/Token;->hasEndIndex()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 1073
    invoke-virtual {p1}, Lspeech/patts/Token;->getEndIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setEndIndex(I)Lspeech/patts/Token$Builder;

    .line 1075
    :cond_27
    invoke-virtual {p1}, Lspeech/patts/Token;->hasUserFeatures()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 1076
    invoke-virtual {p1}, Lspeech/patts/Token;->getUserFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setUserFeatures(Ljava/lang/String;)Lspeech/patts/Token$Builder;

    .line 1078
    :cond_28
    invoke-virtual {p1}, Lspeech/patts/Token;->hasVoiceMod()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 1079
    invoke-virtual {p1}, Lspeech/patts/Token;->getVoiceMod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->mergeVoiceMod(Lcom/google/speech/tts/VoiceMod;)Lspeech/patts/Token$Builder;

    .line 1081
    :cond_29
    invoke-virtual {p1}, Lspeech/patts/Token;->hasReservedForBackwardsCompatibility1()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 1082
    invoke-virtual {p1}, Lspeech/patts/Token;->getReservedForBackwardsCompatibility1()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setReservedForBackwardsCompatibility1(Z)Lspeech/patts/Token$Builder;

    .line 1084
    :cond_2a
    invoke-virtual {p1}, Lspeech/patts/Token;->hasReservedForBackwardsCompatibility2()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 1085
    invoke-virtual {p1}, Lspeech/patts/Token;->getReservedForBackwardsCompatibility2()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setReservedForBackwardsCompatibility2(Lcom/google/protobuf/ByteString;)Lspeech/patts/Token$Builder;

    .line 1087
    :cond_2b
    invoke-virtual {p1}, Lspeech/patts/Token;->hasSpellingWithStress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1088
    invoke-virtual {p1}, Lspeech/patts/Token;->getSpellingWithStress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Token$Builder;->setSpellingWithStress(Ljava/lang/String;)Lspeech/patts/Token$Builder;

    goto/16 :goto_0
.end method

.method public mergeMeasure(Lcom/google/speech/tts/Measure;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Measure;

    .prologue
    .line 1914
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasMeasure()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->measure_:Lcom/google/speech/tts/Measure;
    invoke-static {v0}, Lspeech/patts/Token;->access$4500(Lspeech/patts/Token;)Lcom/google/speech/tts/Measure;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Measure;->getDefaultInstance()Lcom/google/speech/tts/Measure;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1916
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->measure_:Lcom/google/speech/tts/Measure;
    invoke-static {v1}, Lspeech/patts/Token;->access$4500(Lspeech/patts/Token;)Lcom/google/speech/tts/Measure;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Measure;->newBuilder(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Measure$Builder;->mergeFrom(Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Measure$Builder;->buildPartial()Lcom/google/speech/tts/Measure;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->measure_:Lcom/google/speech/tts/Measure;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$4502(Lspeech/patts/Token;Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure;

    .line 1921
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasMeasure:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$4402(Lspeech/patts/Token;Z)Z

    .line 1922
    return-object p0

    .line 1919
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->measure_:Lcom/google/speech/tts/Measure;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$4502(Lspeech/patts/Token;Lcom/google/speech/tts/Measure;)Lcom/google/speech/tts/Measure;

    goto :goto_0
.end method

.method public mergeMoney(Lcom/google/speech/tts/Money;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Money;

    .prologue
    .line 2099
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasMoney()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->money_:Lcom/google/speech/tts/Money;
    invoke-static {v0}, Lspeech/patts/Token;->access$5500(Lspeech/patts/Token;)Lcom/google/speech/tts/Money;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Money;->getDefaultInstance()Lcom/google/speech/tts/Money;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2101
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->money_:Lcom/google/speech/tts/Money;
    invoke-static {v1}, Lspeech/patts/Token;->access$5500(Lspeech/patts/Token;)Lcom/google/speech/tts/Money;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Money;->newBuilder(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Money$Builder;->mergeFrom(Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Money$Builder;->buildPartial()Lcom/google/speech/tts/Money;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->money_:Lcom/google/speech/tts/Money;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$5502(Lspeech/patts/Token;Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money;

    .line 2106
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasMoney:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$5402(Lspeech/patts/Token;Z)Z

    .line 2107
    return-object p0

    .line 2104
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->money_:Lcom/google/speech/tts/Money;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$5502(Lspeech/patts/Token;Lcom/google/speech/tts/Money;)Lcom/google/speech/tts/Money;

    goto :goto_0
.end method

.method public mergeOrdinal(Lcom/google/speech/tts/Ordinal;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Ordinal;

    .prologue
    .line 1745
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasOrdinal()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->ordinal_:Lcom/google/speech/tts/Ordinal;
    invoke-static {v0}, Lspeech/patts/Token;->access$3500(Lspeech/patts/Token;)Lcom/google/speech/tts/Ordinal;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Ordinal;->getDefaultInstance()Lcom/google/speech/tts/Ordinal;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1747
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->ordinal_:Lcom/google/speech/tts/Ordinal;
    invoke-static {v1}, Lspeech/patts/Token;->access$3500(Lspeech/patts/Token;)Lcom/google/speech/tts/Ordinal;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Ordinal;->newBuilder(Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Ordinal$Builder;->mergeFrom(Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Ordinal$Builder;->buildPartial()Lcom/google/speech/tts/Ordinal;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->ordinal_:Lcom/google/speech/tts/Ordinal;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$3502(Lspeech/patts/Token;Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal;

    .line 1752
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasOrdinal:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$3402(Lspeech/patts/Token;Z)Z

    .line 1753
    return-object p0

    .line 1750
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->ordinal_:Lcom/google/speech/tts/Ordinal;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$3502(Lspeech/patts/Token;Lcom/google/speech/tts/Ordinal;)Lcom/google/speech/tts/Ordinal;

    goto :goto_0
.end method

.method public mergePercent(Lcom/google/speech/tts/Decimal;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Decimal;

    .prologue
    .line 1951
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasPercent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->percent_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0}, Lspeech/patts/Token;->access$4700(Lspeech/patts/Token;)Lcom/google/speech/tts/Decimal;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Decimal;->getDefaultInstance()Lcom/google/speech/tts/Decimal;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1953
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->percent_:Lcom/google/speech/tts/Decimal;
    invoke-static {v1}, Lspeech/patts/Token;->access$4700(Lspeech/patts/Token;)Lcom/google/speech/tts/Decimal;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Decimal;->newBuilder(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Decimal$Builder;->mergeFrom(Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Decimal$Builder;->buildPartial()Lcom/google/speech/tts/Decimal;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->percent_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$4702(Lspeech/patts/Token;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;

    .line 1958
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasPercent:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$4602(Lspeech/patts/Token;Z)Z

    .line 1959
    return-object p0

    .line 1956
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->percent_:Lcom/google/speech/tts/Decimal;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$4702(Lspeech/patts/Token;Lcom/google/speech/tts/Decimal;)Lcom/google/speech/tts/Decimal;

    goto :goto_0
.end method

.method public mergeSyntax(Lspeech/patts/Syntax;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/Syntax;

    .prologue
    .line 2347
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasSyntax()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->syntax_:Lspeech/patts/Syntax;
    invoke-static {v0}, Lspeech/patts/Token;->access$7100(Lspeech/patts/Token;)Lspeech/patts/Syntax;

    move-result-object v0

    invoke-static {}, Lspeech/patts/Syntax;->getDefaultInstance()Lspeech/patts/Syntax;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2349
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->syntax_:Lspeech/patts/Syntax;
    invoke-static {v1}, Lspeech/patts/Token;->access$7100(Lspeech/patts/Token;)Lspeech/patts/Syntax;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/Syntax;->newBuilder(Lspeech/patts/Syntax;)Lspeech/patts/Syntax$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/Syntax$Builder;->mergeFrom(Lspeech/patts/Syntax;)Lspeech/patts/Syntax$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/Syntax$Builder;->buildPartial()Lspeech/patts/Syntax;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->syntax_:Lspeech/patts/Syntax;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$7102(Lspeech/patts/Token;Lspeech/patts/Syntax;)Lspeech/patts/Syntax;

    .line 2354
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasSyntax:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$7002(Lspeech/patts/Token;Z)Z

    .line 2355
    return-object p0

    .line 2352
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->syntax_:Lspeech/patts/Syntax;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$7102(Lspeech/patts/Token;Lspeech/patts/Syntax;)Lspeech/patts/Syntax;

    goto :goto_0
.end method

.method public mergeTelephone(Lcom/google/speech/tts/Telephone;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Telephone;

    .prologue
    .line 2062
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasTelephone()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->telephone_:Lcom/google/speech/tts/Telephone;
    invoke-static {v0}, Lspeech/patts/Token;->access$5300(Lspeech/patts/Token;)Lcom/google/speech/tts/Telephone;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Telephone;->getDefaultInstance()Lcom/google/speech/tts/Telephone;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2064
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->telephone_:Lcom/google/speech/tts/Telephone;
    invoke-static {v1}, Lspeech/patts/Token;->access$5300(Lspeech/patts/Token;)Lcom/google/speech/tts/Telephone;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Telephone;->newBuilder(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Telephone$Builder;->mergeFrom(Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Telephone$Builder;->buildPartial()Lcom/google/speech/tts/Telephone;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->telephone_:Lcom/google/speech/tts/Telephone;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$5302(Lspeech/patts/Token;Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone;

    .line 2069
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasTelephone:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$5202(Lspeech/patts/Token;Z)Z

    .line 2070
    return-object p0

    .line 2067
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->telephone_:Lcom/google/speech/tts/Telephone;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$5302(Lspeech/patts/Token;Lcom/google/speech/tts/Telephone;)Lcom/google/speech/tts/Telephone;

    goto :goto_0
.end method

.method public mergeTime(Lcom/google/speech/tts/Time;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Time;

    .prologue
    .line 1877
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->time_:Lcom/google/speech/tts/Time;
    invoke-static {v0}, Lspeech/patts/Token;->access$4300(Lspeech/patts/Token;)Lcom/google/speech/tts/Time;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Time;->getDefaultInstance()Lcom/google/speech/tts/Time;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1879
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->time_:Lcom/google/speech/tts/Time;
    invoke-static {v1}, Lspeech/patts/Token;->access$4300(Lspeech/patts/Token;)Lcom/google/speech/tts/Time;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Time;->newBuilder(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Time$Builder;->mergeFrom(Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Time$Builder;->buildPartial()Lcom/google/speech/tts/Time;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->time_:Lcom/google/speech/tts/Time;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$4302(Lspeech/patts/Token;Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time;

    .line 1884
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasTime:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$4202(Lspeech/patts/Token;Z)Z

    .line 1885
    return-object p0

    .line 1882
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->time_:Lcom/google/speech/tts/Time;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$4302(Lspeech/patts/Token;Lcom/google/speech/tts/Time;)Lcom/google/speech/tts/Time;

    goto :goto_0
.end method

.method public mergeVoiceMod(Lcom/google/speech/tts/VoiceMod;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/VoiceMod;

    .prologue
    .line 2441
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    invoke-virtual {v0}, Lspeech/patts/Token;->hasVoiceMod()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->voiceMod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0}, Lspeech/patts/Token;->access$7900(Lspeech/patts/Token;)Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/VoiceMod;->getDefaultInstance()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2443
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    iget-object v1, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # getter for: Lspeech/patts/Token;->voiceMod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v1}, Lspeech/patts/Token;->access$7900(Lspeech/patts/Token;)Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/VoiceMod;->newBuilder(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/VoiceMod$Builder;->mergeFrom(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/VoiceMod$Builder;->buildPartial()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    # setter for: Lspeech/patts/Token;->voiceMod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$7902(Lspeech/patts/Token;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;

    .line 2448
    :goto_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasVoiceMod:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$7802(Lspeech/patts/Token;Z)Z

    .line 2449
    return-object p0

    .line 2446
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->voiceMod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$7902(Lspeech/patts/Token;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;

    goto :goto_0
.end method

.method public setConcept(Ljava/lang/String;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2239
    if-nez p1, :cond_0

    .line 2240
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2242
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasConcept:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$6402(Lspeech/patts/Token;Z)Z

    .line 2243
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->concept_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$6502(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;

    .line 2244
    return-object p0
.end method

.method public setDigit(Ljava/lang/String;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1769
    if-nez p1, :cond_0

    .line 1770
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1772
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasDigit:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$3602(Lspeech/patts/Token;Z)Z

    .line 1773
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->digit_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$3702(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;

    .line 1774
    return-object p0
.end method

.method public setEndIndex(I)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 2389
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasEndIndex:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$7402(Lspeech/patts/Token;Z)Z

    .line 2390
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->endIndex_:I
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$7502(Lspeech/patts/Token;I)I

    .line 2391
    return-object p0
.end method

.method public setFirstDaughter(I)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1400
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasFirstDaughter:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$602(Lspeech/patts/Token;Z)Z

    .line 1401
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->firstDaughter_:I
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$702(Lspeech/patts/Token;I)I

    .line 1402
    return-object p0
.end method

.method public setLastDaughter(I)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1418
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasLastDaughter:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$802(Lspeech/patts/Token;Z)Z

    .line 1419
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->lastDaughter_:I
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$902(Lspeech/patts/Token;I)I

    .line 1420
    return-object p0
.end method

.method public setLetters(Ljava/lang/String;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2181
    if-nez p1, :cond_0

    .line 2182
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2184
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasLetters:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$6002(Lspeech/patts/Token;Z)Z

    .line 2185
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->letters_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$6102(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;

    .line 2186
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1436
    if-nez p1, :cond_0

    .line 1437
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1439
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasName:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$1002(Lspeech/patts/Token;Z)Z

    .line 1440
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$1102(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;

    .line 1441
    return-object p0
.end method

.method public setNextSpace(Z)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1475
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasNextSpace:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$1402(Lspeech/patts/Token;Z)Z

    .line 1476
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->nextSpace_:Z
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$1502(Lspeech/patts/Token;Z)Z

    .line 1477
    return-object p0
.end method

.method public setParent(I)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1382
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasParent:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$402(Lspeech/patts/Token;Z)Z

    .line 1383
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->parent_:I
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$502(Lspeech/patts/Token;I)I

    .line 1384
    return-object p0
.end method

.method public setPauseDuration(F)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 1574
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasPauseDuration:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$2402(Lspeech/patts/Token;Z)Z

    .line 1575
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->pauseDuration_:F
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$2502(Lspeech/patts/Token;F)F

    .line 1576
    return-object p0
.end method

.method public setPauseLength(Lspeech/patts/Token$PauseLength;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/Token$PauseLength;

    .prologue
    .line 1592
    if-nez p1, :cond_0

    .line 1593
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1595
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasPauseLength:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$2602(Lspeech/patts/Token;Z)Z

    .line 1596
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->pauseLength_:Lspeech/patts/Token$PauseLength;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$2702(Lspeech/patts/Token;Lspeech/patts/Token$PauseLength;)Lspeech/patts/Token$PauseLength;

    .line 1597
    return-object p0
.end method

.method public setPhraseBreak(Z)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1556
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasPhraseBreak:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$2202(Lspeech/patts/Token;Z)Z

    .line 1557
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->phraseBreak_:Z
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$2302(Lspeech/patts/Token;Z)Z

    .line 1558
    return-object p0
.end method

.method public setReservedForBackwardsCompatibility1(Z)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2465
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasReservedForBackwardsCompatibility1:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$8002(Lspeech/patts/Token;Z)Z

    .line 2466
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->reservedForBackwardsCompatibility1_:Z
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$8102(Lspeech/patts/Token;Z)Z

    .line 2467
    return-object p0
.end method

.method public setReservedForBackwardsCompatibility2(Lcom/google/protobuf/ByteString;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/protobuf/ByteString;

    .prologue
    .line 2483
    if-nez p1, :cond_0

    .line 2484
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2486
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasReservedForBackwardsCompatibility2:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$8202(Lspeech/patts/Token;Z)Z

    .line 2487
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->reservedForBackwardsCompatibility2_:Lcom/google/protobuf/ByteString;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$8302(Lspeech/patts/Token;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    .line 2488
    return-object p0
.end method

.method public setSemioticClass(Ljava/lang/String;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1674
    if-nez p1, :cond_0

    .line 1675
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1677
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasSemioticClass:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$3002(Lspeech/patts/Token;Z)Z

    .line 1678
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->semioticClass_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$3102(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;

    .line 1679
    return-object p0
.end method

.method public setSkip(Z)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1457
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasSkip:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$1202(Lspeech/patts/Token;Z)Z

    .line 1458
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->skip_:Z
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$1302(Lspeech/patts/Token;Z)Z

    .line 1459
    return-object p0
.end method

.method public setSpelling(Ljava/lang/String;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1653
    if-nez p1, :cond_0

    .line 1654
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1656
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasSpelling:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$2802(Lspeech/patts/Token;Z)Z

    .line 1657
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->spelling_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$2902(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;

    .line 1658
    return-object p0
.end method

.method public setSpellingWithStress(Ljava/lang/String;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2504
    if-nez p1, :cond_0

    .line 2505
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2507
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasSpellingWithStress:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$8402(Lspeech/patts/Token;Z)Z

    .line 2508
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->spellingWithStress_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$8502(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;

    .line 2509
    return-object p0
.end method

.method public setStartIndex(I)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 2371
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasStartIndex:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$7202(Lspeech/patts/Token;Z)Z

    .line 2372
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->startIndex_:I
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$7302(Lspeech/patts/Token;I)I

    .line 2373
    return-object p0
.end method

.method public setType(Lspeech/patts/Token$Type;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/Token$Type;

    .prologue
    .line 1493
    if-nez p1, :cond_0

    .line 1494
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1496
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasType:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$1602(Lspeech/patts/Token;Z)Z

    .line 1497
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->type_:Lspeech/patts/Token$Type;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$1702(Lspeech/patts/Token;Lspeech/patts/Token$Type;)Lspeech/patts/Token$Type;

    .line 1498
    return-object p0
.end method

.method public setUserFeatures(Ljava/lang/String;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2407
    if-nez p1, :cond_0

    .line 2408
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2410
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasUserFeatures:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$7602(Lspeech/patts/Token;Z)Z

    .line 2411
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->userFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$7702(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;

    .line 2412
    return-object p0
.end method

.method public setVariant(Ljava/lang/String;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1535
    if-nez p1, :cond_0

    .line 1536
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1538
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasVariant:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$2002(Lspeech/patts/Token;Z)Z

    .line 1539
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->variant_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$2102(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;

    .line 1540
    return-object p0
.end method

.method public setVerbatim(Ljava/lang/String;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2160
    if-nez p1, :cond_0

    .line 2161
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2163
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasVerbatim:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$5802(Lspeech/patts/Token;Z)Z

    .line 2164
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->verbatim_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$5902(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;

    .line 2165
    return-object p0
.end method

.method public setWordid(Ljava/lang/String;)Lspeech/patts/Token$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1514
    if-nez p1, :cond_0

    .line 1515
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1517
    :cond_0
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Token;->hasWordid:Z
    invoke-static {v0, v1}, Lspeech/patts/Token;->access$1802(Lspeech/patts/Token;Z)Z

    .line 1518
    iget-object v0, p0, Lspeech/patts/Token$Builder;->result:Lspeech/patts/Token;

    # setter for: Lspeech/patts/Token;->wordid_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Token;->access$1902(Lspeech/patts/Token;Ljava/lang/String;)Ljava/lang/String;

    .line 1519
    return-object p0
.end method
