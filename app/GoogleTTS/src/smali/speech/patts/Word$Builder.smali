.class public final Lspeech/patts/Word$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Word.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Word;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/Word;",
        "Lspeech/patts/Word$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/Word;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 426
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/Word$Builder;
    .locals 1

    .prologue
    .line 420
    invoke-static {}, Lspeech/patts/Word$Builder;->create()Lspeech/patts/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/Word$Builder;
    .locals 3

    .prologue
    .line 429
    new-instance v0, Lspeech/patts/Word$Builder;

    invoke-direct {v0}, Lspeech/patts/Word$Builder;-><init>()V

    .line 430
    .local v0, "builder":Lspeech/patts/Word$Builder;
    new-instance v1, Lspeech/patts/Word;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/Word;-><init>(Lspeech/patts/Word$1;)V

    iput-object v1, v0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    .line 431
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 420
    invoke-virtual {p0}, Lspeech/patts/Word$Builder;->build()Lspeech/patts/Word;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/Word;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/Word$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 460
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    invoke-static {v0}, Lspeech/patts/Word$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 462
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Word$Builder;->buildPartial()Lspeech/patts/Word;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/Word;
    .locals 3

    .prologue
    .line 475
    iget-object v1, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    if-nez v1, :cond_0

    .line 476
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 479
    :cond_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    .line 480
    .local v0, "returnMe":Lspeech/patts/Word;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    .line 481
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 420
    invoke-virtual {p0}, Lspeech/patts/Word$Builder;->clone()Lspeech/patts/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 420
    invoke-virtual {p0}, Lspeech/patts/Word$Builder;->clone()Lspeech/patts/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 420
    invoke-virtual {p0}, Lspeech/patts/Word$Builder;->clone()Lspeech/patts/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/Word$Builder;
    .locals 2

    .prologue
    .line 448
    invoke-static {}, Lspeech/patts/Word$Builder;->create()Lspeech/patts/Word$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    invoke-virtual {v0, v1}, Lspeech/patts/Word$Builder;->mergeFrom(Lspeech/patts/Word;)Lspeech/patts/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    invoke-virtual {v0}, Lspeech/patts/Word;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 420
    check-cast p1, Lspeech/patts/Word;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/Word$Builder;->mergeFrom(Lspeech/patts/Word;)Lspeech/patts/Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/Word;)Lspeech/patts/Word$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/Word;

    .prologue
    .line 485
    invoke-static {}, Lspeech/patts/Word;->getDefaultInstance()Lspeech/patts/Word;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 540
    :cond_0
    :goto_0
    return-object p0

    .line 486
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/Word;->hasParent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 487
    invoke-virtual {p1}, Lspeech/patts/Word;->getParent()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setParent(I)Lspeech/patts/Word$Builder;

    .line 489
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/Word;->hasFirstDaughter()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 490
    invoke-virtual {p1}, Lspeech/patts/Word;->getFirstDaughter()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setFirstDaughter(I)Lspeech/patts/Word$Builder;

    .line 492
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/Word;->hasLastDaughter()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 493
    invoke-virtual {p1}, Lspeech/patts/Word;->getLastDaughter()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setLastDaughter(I)Lspeech/patts/Word$Builder;

    .line 495
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/Word;->hasId()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 496
    invoke-virtual {p1}, Lspeech/patts/Word;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setId(Ljava/lang/String;)Lspeech/patts/Word$Builder;

    .line 498
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/Word;->hasVariant()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 499
    invoke-virtual {p1}, Lspeech/patts/Word;->getVariant()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setVariant(Ljava/lang/String;)Lspeech/patts/Word$Builder;

    .line 501
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/Word;->hasType()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 502
    invoke-virtual {p1}, Lspeech/patts/Word;->getType()Lspeech/patts/Word$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setType(Lspeech/patts/Word$Type;)Lspeech/patts/Word$Builder;

    .line 504
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/Word;->hasSpelling()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 505
    invoke-virtual {p1}, Lspeech/patts/Word;->getSpelling()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setSpelling(Ljava/lang/String;)Lspeech/patts/Word$Builder;

    .line 507
    :cond_8
    invoke-virtual {p1}, Lspeech/patts/Word;->hasVoiceMod()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 508
    invoke-virtual {p1}, Lspeech/patts/Word;->getVoiceMod()Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->mergeVoiceMod(Lcom/google/speech/tts/VoiceMod;)Lspeech/patts/Word$Builder;

    .line 510
    :cond_9
    invoke-virtual {p1}, Lspeech/patts/Word;->hasSyntax()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 511
    invoke-virtual {p1}, Lspeech/patts/Word;->getSyntax()Lspeech/patts/Syntax;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->mergeSyntax(Lspeech/patts/Syntax;)Lspeech/patts/Word$Builder;

    .line 513
    :cond_a
    invoke-virtual {p1}, Lspeech/patts/Word;->hasLexicalSyntax()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 514
    invoke-virtual {p1}, Lspeech/patts/Word;->getLexicalSyntax()Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setLexicalSyntax(Lcom/google/speech/tts/LexiconProto$Entry$Syntax;)Lspeech/patts/Word$Builder;

    .line 516
    :cond_b
    invoke-virtual {p1}, Lspeech/patts/Word;->hasPauseLength()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 517
    invoke-virtual {p1}, Lspeech/patts/Word;->getPauseLength()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setPauseLength(F)Lspeech/patts/Word$Builder;

    .line 519
    :cond_c
    invoke-virtual {p1}, Lspeech/patts/Word;->hasIntonationType()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 520
    invoke-virtual {p1}, Lspeech/patts/Word;->getIntonationType()Lcom/google/speech/tts/IntonationType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setIntonationType(Lcom/google/speech/tts/IntonationType;)Lspeech/patts/Word$Builder;

    .line 522
    :cond_d
    invoke-virtual {p1}, Lspeech/patts/Word;->hasVariantForced()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 523
    invoke-virtual {p1}, Lspeech/patts/Word;->getVariantForced()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setVariantForced(Z)Lspeech/patts/Word$Builder;

    .line 525
    :cond_e
    invoke-virtual {p1}, Lspeech/patts/Word;->hasProhibitsNucleus()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 526
    invoke-virtual {p1}, Lspeech/patts/Word;->getProhibitsNucleus()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setProhibitsNucleus(Z)Lspeech/patts/Word$Builder;

    .line 528
    :cond_f
    invoke-virtual {p1}, Lspeech/patts/Word;->hasProhibitsHead()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 529
    invoke-virtual {p1}, Lspeech/patts/Word;->getProhibitsHead()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setProhibitsHead(Z)Lspeech/patts/Word$Builder;

    .line 531
    :cond_10
    invoke-virtual {p1}, Lspeech/patts/Word;->hasSpellingWithStress()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 532
    invoke-virtual {p1}, Lspeech/patts/Word;->getSpellingWithStress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->setSpellingWithStress(Ljava/lang/String;)Lspeech/patts/Word$Builder;

    .line 534
    :cond_11
    invoke-virtual {p1}, Lspeech/patts/Word;->hasLmScore()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 535
    invoke-virtual {p1}, Lspeech/patts/Word;->getLmScore()Lspeech/patts/LmScore;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->mergeLmScore(Lspeech/patts/LmScore;)Lspeech/patts/Word$Builder;

    .line 537
    :cond_12
    invoke-virtual {p1}, Lspeech/patts/Word;->hasProminence()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538
    invoke-virtual {p1}, Lspeech/patts/Word;->getProminence()Lcom/google/speech/tts/Prominence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Word$Builder;->mergeProminence(Lcom/google/speech/tts/Prominence;)Lspeech/patts/Word$Builder;

    goto/16 :goto_0
.end method

.method public mergeLmScore(Lspeech/patts/LmScore;)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/LmScore;

    .prologue
    .line 1035
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    invoke-virtual {v0}, Lspeech/patts/Word;->hasLmScore()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # getter for: Lspeech/patts/Word;->lmScore_:Lspeech/patts/LmScore;
    invoke-static {v0}, Lspeech/patts/Word;->access$3600(Lspeech/patts/Word;)Lspeech/patts/LmScore;

    move-result-object v0

    invoke-static {}, Lspeech/patts/LmScore;->getDefaultInstance()Lspeech/patts/LmScore;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1037
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    iget-object v1, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # getter for: Lspeech/patts/Word;->lmScore_:Lspeech/patts/LmScore;
    invoke-static {v1}, Lspeech/patts/Word;->access$3600(Lspeech/patts/Word;)Lspeech/patts/LmScore;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/LmScore;->newBuilder(Lspeech/patts/LmScore;)Lspeech/patts/LmScore$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/LmScore$Builder;->mergeFrom(Lspeech/patts/LmScore;)Lspeech/patts/LmScore$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/LmScore$Builder;->buildPartial()Lspeech/patts/LmScore;

    move-result-object v1

    # setter for: Lspeech/patts/Word;->lmScore_:Lspeech/patts/LmScore;
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$3602(Lspeech/patts/Word;Lspeech/patts/LmScore;)Lspeech/patts/LmScore;

    .line 1042
    :goto_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasLmScore:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$3502(Lspeech/patts/Word;Z)Z

    .line 1043
    return-object p0

    .line 1040
    :cond_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->lmScore_:Lspeech/patts/LmScore;
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$3602(Lspeech/patts/Word;Lspeech/patts/LmScore;)Lspeech/patts/LmScore;

    goto :goto_0
.end method

.method public mergeProminence(Lcom/google/speech/tts/Prominence;)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/Prominence;

    .prologue
    .line 1072
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    invoke-virtual {v0}, Lspeech/patts/Word;->hasProminence()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # getter for: Lspeech/patts/Word;->prominence_:Lcom/google/speech/tts/Prominence;
    invoke-static {v0}, Lspeech/patts/Word;->access$3800(Lspeech/patts/Word;)Lcom/google/speech/tts/Prominence;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/Prominence;->getDefaultInstance()Lcom/google/speech/tts/Prominence;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1074
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    iget-object v1, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # getter for: Lspeech/patts/Word;->prominence_:Lcom/google/speech/tts/Prominence;
    invoke-static {v1}, Lspeech/patts/Word;->access$3800(Lspeech/patts/Word;)Lcom/google/speech/tts/Prominence;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/Prominence;->newBuilder(Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/Prominence$Builder;->mergeFrom(Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/Prominence$Builder;->buildPartial()Lcom/google/speech/tts/Prominence;

    move-result-object v1

    # setter for: Lspeech/patts/Word;->prominence_:Lcom/google/speech/tts/Prominence;
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$3802(Lspeech/patts/Word;Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence;

    .line 1079
    :goto_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasProminence:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$3702(Lspeech/patts/Word;Z)Z

    .line 1080
    return-object p0

    .line 1077
    :cond_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->prominence_:Lcom/google/speech/tts/Prominence;
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$3802(Lspeech/patts/Word;Lcom/google/speech/tts/Prominence;)Lcom/google/speech/tts/Prominence;

    goto :goto_0
.end method

.method public mergeSyntax(Lspeech/patts/Syntax;)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/Syntax;

    .prologue
    .line 863
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    invoke-virtual {v0}, Lspeech/patts/Word;->hasSyntax()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # getter for: Lspeech/patts/Word;->syntax_:Lspeech/patts/Syntax;
    invoke-static {v0}, Lspeech/patts/Word;->access$2000(Lspeech/patts/Word;)Lspeech/patts/Syntax;

    move-result-object v0

    invoke-static {}, Lspeech/patts/Syntax;->getDefaultInstance()Lspeech/patts/Syntax;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 865
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    iget-object v1, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # getter for: Lspeech/patts/Word;->syntax_:Lspeech/patts/Syntax;
    invoke-static {v1}, Lspeech/patts/Word;->access$2000(Lspeech/patts/Word;)Lspeech/patts/Syntax;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/Syntax;->newBuilder(Lspeech/patts/Syntax;)Lspeech/patts/Syntax$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/Syntax$Builder;->mergeFrom(Lspeech/patts/Syntax;)Lspeech/patts/Syntax$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/Syntax$Builder;->buildPartial()Lspeech/patts/Syntax;

    move-result-object v1

    # setter for: Lspeech/patts/Word;->syntax_:Lspeech/patts/Syntax;
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$2002(Lspeech/patts/Word;Lspeech/patts/Syntax;)Lspeech/patts/Syntax;

    .line 870
    :goto_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasSyntax:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$1902(Lspeech/patts/Word;Z)Z

    .line 871
    return-object p0

    .line 868
    :cond_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->syntax_:Lspeech/patts/Syntax;
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$2002(Lspeech/patts/Word;Lspeech/patts/Syntax;)Lspeech/patts/Syntax;

    goto :goto_0
.end method

.method public mergeVoiceMod(Lcom/google/speech/tts/VoiceMod;)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/VoiceMod;

    .prologue
    .line 826
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    invoke-virtual {v0}, Lspeech/patts/Word;->hasVoiceMod()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # getter for: Lspeech/patts/Word;->voiceMod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0}, Lspeech/patts/Word;->access$1800(Lspeech/patts/Word;)Lcom/google/speech/tts/VoiceMod;

    move-result-object v0

    invoke-static {}, Lcom/google/speech/tts/VoiceMod;->getDefaultInstance()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 828
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    iget-object v1, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # getter for: Lspeech/patts/Word;->voiceMod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v1}, Lspeech/patts/Word;->access$1800(Lspeech/patts/Word;)Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/tts/VoiceMod;->newBuilder(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/tts/VoiceMod$Builder;->mergeFrom(Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/tts/VoiceMod$Builder;->buildPartial()Lcom/google/speech/tts/VoiceMod;

    move-result-object v1

    # setter for: Lspeech/patts/Word;->voiceMod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$1802(Lspeech/patts/Word;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;

    .line 833
    :goto_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasVoiceMod:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$1702(Lspeech/patts/Word;Z)Z

    .line 834
    return-object p0

    .line 831
    :cond_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->voiceMod_:Lcom/google/speech/tts/VoiceMod;
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$1802(Lspeech/patts/Word;Lcom/google/speech/tts/VoiceMod;)Lcom/google/speech/tts/VoiceMod;

    goto :goto_0
.end method

.method public setFirstDaughter(I)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 693
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasFirstDaughter:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$502(Lspeech/patts/Word;Z)Z

    .line 694
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->firstDaughter_:I
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$602(Lspeech/patts/Word;I)I

    .line 695
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 729
    if-nez p1, :cond_0

    .line 730
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 732
    :cond_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasId:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$902(Lspeech/patts/Word;Z)Z

    .line 733
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->id_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$1002(Lspeech/patts/Word;Ljava/lang/String;)Ljava/lang/String;

    .line 734
    return-object p0
.end method

.method public setIntonationType(Lcom/google/speech/tts/IntonationType;)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/IntonationType;

    .prologue
    .line 926
    if-nez p1, :cond_0

    .line 927
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 929
    :cond_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasIntonationType:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$2502(Lspeech/patts/Word;Z)Z

    .line 930
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->intonationType_:Lcom/google/speech/tts/IntonationType;
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$2602(Lspeech/patts/Word;Lcom/google/speech/tts/IntonationType;)Lcom/google/speech/tts/IntonationType;

    .line 931
    return-object p0
.end method

.method public setLastDaughter(I)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 711
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasLastDaughter:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$702(Lspeech/patts/Word;Z)Z

    .line 712
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->lastDaughter_:I
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$802(Lspeech/patts/Word;I)I

    .line 713
    return-object p0
.end method

.method public setLexicalSyntax(Lcom/google/speech/tts/LexiconProto$Entry$Syntax;)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .prologue
    .line 887
    if-nez p1, :cond_0

    .line 888
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 890
    :cond_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasLexicalSyntax:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$2102(Lspeech/patts/Word;Z)Z

    .line 891
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->lexicalSyntax_:Lcom/google/speech/tts/LexiconProto$Entry$Syntax;
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$2202(Lspeech/patts/Word;Lcom/google/speech/tts/LexiconProto$Entry$Syntax;)Lcom/google/speech/tts/LexiconProto$Entry$Syntax;

    .line 892
    return-object p0
.end method

.method public setParent(I)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 675
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasParent:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$302(Lspeech/patts/Word;Z)Z

    .line 676
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->parent_:I
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$402(Lspeech/patts/Word;I)I

    .line 677
    return-object p0
.end method

.method public setPauseLength(F)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 908
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasPauseLength:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$2302(Lspeech/patts/Word;Z)Z

    .line 909
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->pauseLength_:F
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$2402(Lspeech/patts/Word;F)F

    .line 910
    return-object p0
.end method

.method public setProhibitsHead(Z)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 983
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasProhibitsHead:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$3102(Lspeech/patts/Word;Z)Z

    .line 984
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->prohibitsHead_:Z
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$3202(Lspeech/patts/Word;Z)Z

    .line 985
    return-object p0
.end method

.method public setProhibitsNucleus(Z)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 965
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasProhibitsNucleus:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$2902(Lspeech/patts/Word;Z)Z

    .line 966
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->prohibitsNucleus_:Z
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$3002(Lspeech/patts/Word;Z)Z

    .line 967
    return-object p0
.end method

.method public setSpelling(Ljava/lang/String;)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 792
    if-nez p1, :cond_0

    .line 793
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 795
    :cond_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasSpelling:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$1502(Lspeech/patts/Word;Z)Z

    .line 796
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->spelling_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$1602(Lspeech/patts/Word;Ljava/lang/String;)Ljava/lang/String;

    .line 797
    return-object p0
.end method

.method public setSpellingWithStress(Ljava/lang/String;)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1001
    if-nez p1, :cond_0

    .line 1002
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1004
    :cond_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasSpellingWithStress:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$3302(Lspeech/patts/Word;Z)Z

    .line 1005
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->spellingWithStress_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$3402(Lspeech/patts/Word;Ljava/lang/String;)Ljava/lang/String;

    .line 1006
    return-object p0
.end method

.method public setType(Lspeech/patts/Word$Type;)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/Word$Type;

    .prologue
    .line 771
    if-nez p1, :cond_0

    .line 772
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 774
    :cond_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasType:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$1302(Lspeech/patts/Word;Z)Z

    .line 775
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->type_:Lspeech/patts/Word$Type;
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$1402(Lspeech/patts/Word;Lspeech/patts/Word$Type;)Lspeech/patts/Word$Type;

    .line 776
    return-object p0
.end method

.method public setVariant(Ljava/lang/String;)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 750
    if-nez p1, :cond_0

    .line 751
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 753
    :cond_0
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasVariant:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$1102(Lspeech/patts/Word;Z)Z

    .line 754
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->variant_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$1202(Lspeech/patts/Word;Ljava/lang/String;)Ljava/lang/String;

    .line 755
    return-object p0
.end method

.method public setVariantForced(Z)Lspeech/patts/Word$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 947
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Word;->hasVariantForced:Z
    invoke-static {v0, v1}, Lspeech/patts/Word;->access$2702(Lspeech/patts/Word;Z)Z

    .line 948
    iget-object v0, p0, Lspeech/patts/Word$Builder;->result:Lspeech/patts/Word;

    # setter for: Lspeech/patts/Word;->variantForced_:Z
    invoke-static {v0, p1}, Lspeech/patts/Word;->access$2802(Lspeech/patts/Word;Z)Z

    .line 949
    return-object p0
.end method
