.class public final enum Lspeech/patts/NavigationFeature$DiphoneDirection;
.super Ljava/lang/Enum;
.source "NavigationFeature.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/NavigationFeature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DiphoneDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/NavigationFeature$DiphoneDirection;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/NavigationFeature$DiphoneDirection;

.field public static final enum DIPHONE_LEFT:Lspeech/patts/NavigationFeature$DiphoneDirection;

.field public static final enum DIPHONE_RIGHT:Lspeech/patts/NavigationFeature$DiphoneDirection;

.field public static final enum DIPHONE_UNSPECIFIED:Lspeech/patts/NavigationFeature$DiphoneDirection;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/NavigationFeature$DiphoneDirection;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lspeech/patts/NavigationFeature$DiphoneDirection;

    const-string v1, "DIPHONE_UNSPECIFIED"

    invoke-direct {v0, v1, v2, v2, v2}, Lspeech/patts/NavigationFeature$DiphoneDirection;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/NavigationFeature$DiphoneDirection;->DIPHONE_UNSPECIFIED:Lspeech/patts/NavigationFeature$DiphoneDirection;

    .line 25
    new-instance v0, Lspeech/patts/NavigationFeature$DiphoneDirection;

    const-string v1, "DIPHONE_LEFT"

    invoke-direct {v0, v1, v3, v3, v3}, Lspeech/patts/NavigationFeature$DiphoneDirection;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/NavigationFeature$DiphoneDirection;->DIPHONE_LEFT:Lspeech/patts/NavigationFeature$DiphoneDirection;

    .line 26
    new-instance v0, Lspeech/patts/NavigationFeature$DiphoneDirection;

    const-string v1, "DIPHONE_RIGHT"

    invoke-direct {v0, v1, v4, v4, v4}, Lspeech/patts/NavigationFeature$DiphoneDirection;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/NavigationFeature$DiphoneDirection;->DIPHONE_RIGHT:Lspeech/patts/NavigationFeature$DiphoneDirection;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Lspeech/patts/NavigationFeature$DiphoneDirection;

    sget-object v1, Lspeech/patts/NavigationFeature$DiphoneDirection;->DIPHONE_UNSPECIFIED:Lspeech/patts/NavigationFeature$DiphoneDirection;

    aput-object v1, v0, v2

    sget-object v1, Lspeech/patts/NavigationFeature$DiphoneDirection;->DIPHONE_LEFT:Lspeech/patts/NavigationFeature$DiphoneDirection;

    aput-object v1, v0, v3

    sget-object v1, Lspeech/patts/NavigationFeature$DiphoneDirection;->DIPHONE_RIGHT:Lspeech/patts/NavigationFeature$DiphoneDirection;

    aput-object v1, v0, v4

    sput-object v0, Lspeech/patts/NavigationFeature$DiphoneDirection;->$VALUES:[Lspeech/patts/NavigationFeature$DiphoneDirection;

    .line 46
    new-instance v0, Lspeech/patts/NavigationFeature$DiphoneDirection$1;

    invoke-direct {v0}, Lspeech/patts/NavigationFeature$DiphoneDirection$1;-><init>()V

    sput-object v0, Lspeech/patts/NavigationFeature$DiphoneDirection;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput p3, p0, Lspeech/patts/NavigationFeature$DiphoneDirection;->index:I

    .line 57
    iput p4, p0, Lspeech/patts/NavigationFeature$DiphoneDirection;->value:I

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/NavigationFeature$DiphoneDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lspeech/patts/NavigationFeature$DiphoneDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/NavigationFeature$DiphoneDirection;

    return-object v0
.end method

.method public static values()[Lspeech/patts/NavigationFeature$DiphoneDirection;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lspeech/patts/NavigationFeature$DiphoneDirection;->$VALUES:[Lspeech/patts/NavigationFeature$DiphoneDirection;

    invoke-virtual {v0}, [Lspeech/patts/NavigationFeature$DiphoneDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/NavigationFeature$DiphoneDirection;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lspeech/patts/NavigationFeature$DiphoneDirection;->value:I

    return v0
.end method
