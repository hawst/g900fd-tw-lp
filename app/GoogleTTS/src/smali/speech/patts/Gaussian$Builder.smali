.class public final Lspeech/patts/Gaussian$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Gaussian.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Gaussian;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/Gaussian;",
        "Lspeech/patts/Gaussian$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/Gaussian;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/Gaussian$Builder;
    .locals 1

    .prologue
    .line 221
    invoke-static {}, Lspeech/patts/Gaussian$Builder;->create()Lspeech/patts/Gaussian$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/Gaussian$Builder;
    .locals 3

    .prologue
    .line 230
    new-instance v0, Lspeech/patts/Gaussian$Builder;

    invoke-direct {v0}, Lspeech/patts/Gaussian$Builder;-><init>()V

    .line 231
    .local v0, "builder":Lspeech/patts/Gaussian$Builder;
    new-instance v1, Lspeech/patts/Gaussian;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/Gaussian;-><init>(Lspeech/patts/Gaussian$1;)V

    iput-object v1, v0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    .line 232
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 221
    invoke-virtual {p0}, Lspeech/patts/Gaussian$Builder;->build()Lspeech/patts/Gaussian;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/Gaussian;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/Gaussian$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    invoke-static {v0}, Lspeech/patts/Gaussian$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 263
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Gaussian$Builder;->buildPartial()Lspeech/patts/Gaussian;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/Gaussian;
    .locals 3

    .prologue
    .line 276
    iget-object v1, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    if-nez v1, :cond_0

    .line 277
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 280
    :cond_0
    iget-object v1, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    # getter for: Lspeech/patts/Gaussian;->means_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Gaussian;->access$300(Lspeech/patts/Gaussian;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 281
    iget-object v1, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    iget-object v2, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    # getter for: Lspeech/patts/Gaussian;->means_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Gaussian;->access$300(Lspeech/patts/Gaussian;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Gaussian;->means_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Gaussian;->access$302(Lspeech/patts/Gaussian;Ljava/util/List;)Ljava/util/List;

    .line 284
    :cond_1
    iget-object v1, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    # getter for: Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Gaussian;->access$400(Lspeech/patts/Gaussian;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 285
    iget-object v1, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    iget-object v2, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    # getter for: Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Gaussian;->access$400(Lspeech/patts/Gaussian;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Gaussian;->access$402(Lspeech/patts/Gaussian;Ljava/util/List;)Ljava/util/List;

    .line 288
    :cond_2
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    .line 289
    .local v0, "returnMe":Lspeech/patts/Gaussian;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    .line 290
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 221
    invoke-virtual {p0}, Lspeech/patts/Gaussian$Builder;->clone()Lspeech/patts/Gaussian$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 221
    invoke-virtual {p0}, Lspeech/patts/Gaussian$Builder;->clone()Lspeech/patts/Gaussian$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 221
    invoke-virtual {p0}, Lspeech/patts/Gaussian$Builder;->clone()Lspeech/patts/Gaussian$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/Gaussian$Builder;
    .locals 2

    .prologue
    .line 249
    invoke-static {}, Lspeech/patts/Gaussian$Builder;->create()Lspeech/patts/Gaussian$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    invoke-virtual {v0, v1}, Lspeech/patts/Gaussian$Builder;->mergeFrom(Lspeech/patts/Gaussian;)Lspeech/patts/Gaussian$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    invoke-virtual {v0}, Lspeech/patts/Gaussian;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 221
    check-cast p1, Lspeech/patts/Gaussian;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/Gaussian$Builder;->mergeFrom(Lspeech/patts/Gaussian;)Lspeech/patts/Gaussian$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/Gaussian;)Lspeech/patts/Gaussian$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/Gaussian;

    .prologue
    .line 294
    invoke-static {}, Lspeech/patts/Gaussian;->getDefaultInstance()Lspeech/patts/Gaussian;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 316
    :cond_0
    :goto_0
    return-object p0

    .line 295
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/Gaussian;->hasPrior()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 296
    invoke-virtual {p1}, Lspeech/patts/Gaussian;->getPrior()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Gaussian$Builder;->setPrior(F)Lspeech/patts/Gaussian$Builder;

    .line 298
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/Gaussian;->hasGconst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 299
    invoke-virtual {p1}, Lspeech/patts/Gaussian;->getGconst()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Gaussian$Builder;->setGconst(F)Lspeech/patts/Gaussian$Builder;

    .line 301
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/Gaussian;->hasDiagonal()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 302
    invoke-virtual {p1}, Lspeech/patts/Gaussian;->getDiagonal()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Gaussian$Builder;->setDiagonal(Z)Lspeech/patts/Gaussian$Builder;

    .line 304
    :cond_4
    # getter for: Lspeech/patts/Gaussian;->means_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Gaussian;->access$300(Lspeech/patts/Gaussian;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 305
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    # getter for: Lspeech/patts/Gaussian;->means_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Gaussian;->access$300(Lspeech/patts/Gaussian;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 306
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Gaussian;->means_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Gaussian;->access$302(Lspeech/patts/Gaussian;Ljava/util/List;)Ljava/util/List;

    .line 308
    :cond_5
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    # getter for: Lspeech/patts/Gaussian;->means_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Gaussian;->access$300(Lspeech/patts/Gaussian;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Gaussian;->means_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Gaussian;->access$300(Lspeech/patts/Gaussian;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 310
    :cond_6
    # getter for: Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Gaussian;->access$400(Lspeech/patts/Gaussian;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    # getter for: Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Gaussian;->access$400(Lspeech/patts/Gaussian;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 312
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Gaussian;->access$402(Lspeech/patts/Gaussian;Ljava/util/List;)Ljava/util/List;

    .line 314
    :cond_7
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    # getter for: Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Gaussian;->access$400(Lspeech/patts/Gaussian;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Gaussian;->covariances_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Gaussian;->access$400(Lspeech/patts/Gaussian;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setDiagonal(Z)Lspeech/patts/Gaussian$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 421
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Gaussian;->hasDiagonal:Z
    invoke-static {v0, v1}, Lspeech/patts/Gaussian;->access$902(Lspeech/patts/Gaussian;Z)Z

    .line 422
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    # setter for: Lspeech/patts/Gaussian;->diagonal_:Z
    invoke-static {v0, p1}, Lspeech/patts/Gaussian;->access$1002(Lspeech/patts/Gaussian;Z)Z

    .line 423
    return-object p0
.end method

.method public setGconst(F)Lspeech/patts/Gaussian$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 403
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Gaussian;->hasGconst:Z
    invoke-static {v0, v1}, Lspeech/patts/Gaussian;->access$702(Lspeech/patts/Gaussian;Z)Z

    .line 404
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    # setter for: Lspeech/patts/Gaussian;->gconst_:F
    invoke-static {v0, p1}, Lspeech/patts/Gaussian;->access$802(Lspeech/patts/Gaussian;F)F

    .line 405
    return-object p0
.end method

.method public setPrior(F)Lspeech/patts/Gaussian$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 385
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Gaussian;->hasPrior:Z
    invoke-static {v0, v1}, Lspeech/patts/Gaussian;->access$502(Lspeech/patts/Gaussian;Z)Z

    .line 386
    iget-object v0, p0, Lspeech/patts/Gaussian$Builder;->result:Lspeech/patts/Gaussian;

    # setter for: Lspeech/patts/Gaussian;->prior_:F
    invoke-static {v0, p1}, Lspeech/patts/Gaussian;->access$602(Lspeech/patts/Gaussian;F)F

    .line 387
    return-object p0
.end method
