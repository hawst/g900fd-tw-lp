.class public final Lspeech/patts/Question$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Question.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/Question;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/Question;",
        "Lspeech/patts/Question$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/Question;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 345
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/Question$Builder;
    .locals 1

    .prologue
    .line 339
    invoke-static {}, Lspeech/patts/Question$Builder;->create()Lspeech/patts/Question$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/Question$Builder;
    .locals 3

    .prologue
    .line 348
    new-instance v0, Lspeech/patts/Question$Builder;

    invoke-direct {v0}, Lspeech/patts/Question$Builder;-><init>()V

    .line 349
    .local v0, "builder":Lspeech/patts/Question$Builder;
    new-instance v1, Lspeech/patts/Question;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/Question;-><init>(Lspeech/patts/Question$1;)V

    iput-object v1, v0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    .line 350
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 339
    invoke-virtual {p0}, Lspeech/patts/Question$Builder;->build()Lspeech/patts/Question;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/Question;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/Question$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    invoke-static {v0}, Lspeech/patts/Question$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 381
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Question$Builder;->buildPartial()Lspeech/patts/Question;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/Question;
    .locals 3

    .prologue
    .line 394
    iget-object v1, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    if-nez v1, :cond_0

    .line 395
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 398
    :cond_0
    iget-object v1, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # getter for: Lspeech/patts/Question;->q_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/Question;->access$300(Lspeech/patts/Question;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 399
    iget-object v1, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    iget-object v2, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # getter for: Lspeech/patts/Question;->q_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/Question;->access$300(Lspeech/patts/Question;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/Question;->q_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/Question;->access$302(Lspeech/patts/Question;Ljava/util/List;)Ljava/util/List;

    .line 402
    :cond_1
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    .line 403
    .local v0, "returnMe":Lspeech/patts/Question;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    .line 404
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 339
    invoke-virtual {p0}, Lspeech/patts/Question$Builder;->clone()Lspeech/patts/Question$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 339
    invoke-virtual {p0}, Lspeech/patts/Question$Builder;->clone()Lspeech/patts/Question$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 339
    invoke-virtual {p0}, Lspeech/patts/Question$Builder;->clone()Lspeech/patts/Question$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/Question$Builder;
    .locals 2

    .prologue
    .line 367
    invoke-static {}, Lspeech/patts/Question$Builder;->create()Lspeech/patts/Question$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    invoke-virtual {v0, v1}, Lspeech/patts/Question$Builder;->mergeFrom(Lspeech/patts/Question;)Lspeech/patts/Question$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    invoke-virtual {v0}, Lspeech/patts/Question;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 339
    check-cast p1, Lspeech/patts/Question;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/Question$Builder;->mergeFrom(Lspeech/patts/Question;)Lspeech/patts/Question$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/Question;)Lspeech/patts/Question$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/Question;

    .prologue
    .line 408
    invoke-static {}, Lspeech/patts/Question;->getDefaultInstance()Lspeech/patts/Question;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 439
    :cond_0
    :goto_0
    return-object p0

    .line 409
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/Question;->hasNf()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 410
    invoke-virtual {p1}, Lspeech/patts/Question;->getNf()Lspeech/patts/NavigationFeature;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Question$Builder;->mergeNf(Lspeech/patts/NavigationFeature;)Lspeech/patts/Question$Builder;

    .line 412
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/Question;->hasRhs()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 413
    invoke-virtual {p1}, Lspeech/patts/Question;->getRhs()Lspeech/patts/NavigationFeature;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Question$Builder;->mergeRhs(Lspeech/patts/NavigationFeature;)Lspeech/patts/Question$Builder;

    .line 415
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/Question;->hasBval()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 416
    invoke-virtual {p1}, Lspeech/patts/Question;->getBval()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Question$Builder;->setBval(Z)Lspeech/patts/Question$Builder;

    .line 418
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/Question;->hasIval()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 419
    invoke-virtual {p1}, Lspeech/patts/Question;->getIval()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Question$Builder;->setIval(I)Lspeech/patts/Question$Builder;

    .line 421
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/Question;->hasSval()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 422
    invoke-virtual {p1}, Lspeech/patts/Question;->getSval()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Question$Builder;->setSval(Ljava/lang/String;)Lspeech/patts/Question$Builder;

    .line 424
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/Question;->hasFval()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 425
    invoke-virtual {p1}, Lspeech/patts/Question;->getFval()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/Question$Builder;->setFval(F)Lspeech/patts/Question$Builder;

    .line 427
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/Question;->hasComp()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 428
    invoke-virtual {p1}, Lspeech/patts/Question;->getComp()Lspeech/patts/Question$Comparison;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Question$Builder;->setComp(Lspeech/patts/Question$Comparison;)Lspeech/patts/Question$Builder;

    .line 430
    :cond_8
    # getter for: Lspeech/patts/Question;->q_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Question;->access$300(Lspeech/patts/Question;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 431
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # getter for: Lspeech/patts/Question;->q_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Question;->access$300(Lspeech/patts/Question;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 432
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/Question;->q_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/Question;->access$302(Lspeech/patts/Question;Ljava/util/List;)Ljava/util/List;

    .line 434
    :cond_9
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # getter for: Lspeech/patts/Question;->q_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/Question;->access$300(Lspeech/patts/Question;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/Question;->q_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/Question;->access$300(Lspeech/patts/Question;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 436
    :cond_a
    invoke-virtual {p1}, Lspeech/patts/Question;->hasConj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 437
    invoke-virtual {p1}, Lspeech/patts/Question;->getConj()Lspeech/patts/Question$Conjunction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/Question$Builder;->setConj(Lspeech/patts/Question$Conjunction;)Lspeech/patts/Question$Builder;

    goto/16 :goto_0
.end method

.method public mergeNf(Lspeech/patts/NavigationFeature;)Lspeech/patts/Question$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/NavigationFeature;

    .prologue
    .line 539
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    invoke-virtual {v0}, Lspeech/patts/Question;->hasNf()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # getter for: Lspeech/patts/Question;->nf_:Lspeech/patts/NavigationFeature;
    invoke-static {v0}, Lspeech/patts/Question;->access$500(Lspeech/patts/Question;)Lspeech/patts/NavigationFeature;

    move-result-object v0

    invoke-static {}, Lspeech/patts/NavigationFeature;->getDefaultInstance()Lspeech/patts/NavigationFeature;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 541
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    iget-object v1, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # getter for: Lspeech/patts/Question;->nf_:Lspeech/patts/NavigationFeature;
    invoke-static {v1}, Lspeech/patts/Question;->access$500(Lspeech/patts/Question;)Lspeech/patts/NavigationFeature;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/NavigationFeature;->newBuilder(Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/NavigationFeature$Builder;->mergeFrom(Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/NavigationFeature$Builder;->buildPartial()Lspeech/patts/NavigationFeature;

    move-result-object v1

    # setter for: Lspeech/patts/Question;->nf_:Lspeech/patts/NavigationFeature;
    invoke-static {v0, v1}, Lspeech/patts/Question;->access$502(Lspeech/patts/Question;Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature;

    .line 546
    :goto_0
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Question;->hasNf:Z
    invoke-static {v0, v1}, Lspeech/patts/Question;->access$402(Lspeech/patts/Question;Z)Z

    .line 547
    return-object p0

    .line 544
    :cond_0
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # setter for: Lspeech/patts/Question;->nf_:Lspeech/patts/NavigationFeature;
    invoke-static {v0, p1}, Lspeech/patts/Question;->access$502(Lspeech/patts/Question;Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature;

    goto :goto_0
.end method

.method public mergeRhs(Lspeech/patts/NavigationFeature;)Lspeech/patts/Question$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/NavigationFeature;

    .prologue
    .line 576
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    invoke-virtual {v0}, Lspeech/patts/Question;->hasRhs()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # getter for: Lspeech/patts/Question;->rhs_:Lspeech/patts/NavigationFeature;
    invoke-static {v0}, Lspeech/patts/Question;->access$700(Lspeech/patts/Question;)Lspeech/patts/NavigationFeature;

    move-result-object v0

    invoke-static {}, Lspeech/patts/NavigationFeature;->getDefaultInstance()Lspeech/patts/NavigationFeature;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 578
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    iget-object v1, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # getter for: Lspeech/patts/Question;->rhs_:Lspeech/patts/NavigationFeature;
    invoke-static {v1}, Lspeech/patts/Question;->access$700(Lspeech/patts/Question;)Lspeech/patts/NavigationFeature;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/NavigationFeature;->newBuilder(Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/NavigationFeature$Builder;->mergeFrom(Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/NavigationFeature$Builder;->buildPartial()Lspeech/patts/NavigationFeature;

    move-result-object v1

    # setter for: Lspeech/patts/Question;->rhs_:Lspeech/patts/NavigationFeature;
    invoke-static {v0, v1}, Lspeech/patts/Question;->access$702(Lspeech/patts/Question;Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature;

    .line 583
    :goto_0
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Question;->hasRhs:Z
    invoke-static {v0, v1}, Lspeech/patts/Question;->access$602(Lspeech/patts/Question;Z)Z

    .line 584
    return-object p0

    .line 581
    :cond_0
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # setter for: Lspeech/patts/Question;->rhs_:Lspeech/patts/NavigationFeature;
    invoke-static {v0, p1}, Lspeech/patts/Question;->access$702(Lspeech/patts/Question;Lspeech/patts/NavigationFeature;)Lspeech/patts/NavigationFeature;

    goto :goto_0
.end method

.method public setBval(Z)Lspeech/patts/Question$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 600
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Question;->hasBval:Z
    invoke-static {v0, v1}, Lspeech/patts/Question;->access$802(Lspeech/patts/Question;Z)Z

    .line 601
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # setter for: Lspeech/patts/Question;->bval_:Z
    invoke-static {v0, p1}, Lspeech/patts/Question;->access$902(Lspeech/patts/Question;Z)Z

    .line 602
    return-object p0
.end method

.method public setComp(Lspeech/patts/Question$Comparison;)Lspeech/patts/Question$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/Question$Comparison;

    .prologue
    .line 675
    if-nez p1, :cond_0

    .line 676
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 678
    :cond_0
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Question;->hasComp:Z
    invoke-static {v0, v1}, Lspeech/patts/Question;->access$1602(Lspeech/patts/Question;Z)Z

    .line 679
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # setter for: Lspeech/patts/Question;->comp_:Lspeech/patts/Question$Comparison;
    invoke-static {v0, p1}, Lspeech/patts/Question;->access$1702(Lspeech/patts/Question;Lspeech/patts/Question$Comparison;)Lspeech/patts/Question$Comparison;

    .line 680
    return-object p0
.end method

.method public setConj(Lspeech/patts/Question$Conjunction;)Lspeech/patts/Question$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/Question$Conjunction;

    .prologue
    .line 747
    if-nez p1, :cond_0

    .line 748
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 750
    :cond_0
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Question;->hasConj:Z
    invoke-static {v0, v1}, Lspeech/patts/Question;->access$1802(Lspeech/patts/Question;Z)Z

    .line 751
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # setter for: Lspeech/patts/Question;->conj_:Lspeech/patts/Question$Conjunction;
    invoke-static {v0, p1}, Lspeech/patts/Question;->access$1902(Lspeech/patts/Question;Lspeech/patts/Question$Conjunction;)Lspeech/patts/Question$Conjunction;

    .line 752
    return-object p0
.end method

.method public setFval(F)Lspeech/patts/Question$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 657
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Question;->hasFval:Z
    invoke-static {v0, v1}, Lspeech/patts/Question;->access$1402(Lspeech/patts/Question;Z)Z

    .line 658
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # setter for: Lspeech/patts/Question;->fval_:F
    invoke-static {v0, p1}, Lspeech/patts/Question;->access$1502(Lspeech/patts/Question;F)F

    .line 659
    return-object p0
.end method

.method public setIval(I)Lspeech/patts/Question$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 618
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Question;->hasIval:Z
    invoke-static {v0, v1}, Lspeech/patts/Question;->access$1002(Lspeech/patts/Question;Z)Z

    .line 619
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # setter for: Lspeech/patts/Question;->ival_:I
    invoke-static {v0, p1}, Lspeech/patts/Question;->access$1102(Lspeech/patts/Question;I)I

    .line 620
    return-object p0
.end method

.method public setSval(Ljava/lang/String;)Lspeech/patts/Question$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 636
    if-nez p1, :cond_0

    .line 637
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 639
    :cond_0
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/Question;->hasSval:Z
    invoke-static {v0, v1}, Lspeech/patts/Question;->access$1202(Lspeech/patts/Question;Z)Z

    .line 640
    iget-object v0, p0, Lspeech/patts/Question$Builder;->result:Lspeech/patts/Question;

    # setter for: Lspeech/patts/Question;->sval_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/Question;->access$1302(Lspeech/patts/Question;Ljava/lang/String;)Ljava/lang/String;

    .line 641
    return-object p0
.end method
