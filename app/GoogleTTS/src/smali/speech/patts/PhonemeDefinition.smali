.class public final Lspeech/patts/PhonemeDefinition;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PhonemeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/PhonemeDefinition$1;,
        Lspeech/patts/PhonemeDefinition$Builder;,
        Lspeech/patts/PhonemeDefinition$Length;,
        Lspeech/patts/PhonemeDefinition$Position;,
        Lspeech/patts/PhonemeDefinition$Height;,
        Lspeech/patts/PhonemeDefinition$Manner;,
        Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/PhonemeDefinition;


# instance fields
.field private archi_:Z

.field private closedSyllable_:Z

.field private hasArchi:Z

.field private hasClosedSyllable:Z

.field private hasHeight:Z

.field private hasLength:Z

.field private hasManner:Z

.field private hasName:Z

.field private hasPoa:Z

.field private hasPosition:Z

.field private hasRounded:Z

.field private hasSchwa:Z

.field private hasSecondaryManner:Z

.field private hasSecondaryPoa:Z

.field private hasSyllabicConsonant:Z

.field private hasVoiced:Z

.field private hasVowel:Z

.field private height_:Lspeech/patts/PhonemeDefinition$Height;

.field private length_:Lspeech/patts/PhonemeDefinition$Length;

.field private manner_:Lspeech/patts/PhonemeDefinition$Manner;

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private poa_:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field private position_:Lspeech/patts/PhonemeDefinition$Position;

.field private rounded_:Z

.field private schwa_:Z

.field private secondaryManner_:Lspeech/patts/PhonemeDefinition$Manner;

.field private secondaryPoa_:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

.field private syllabicConsonant_:Z

.field private voiced_:Z

.field private vowel_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1110
    new-instance v0, Lspeech/patts/PhonemeDefinition;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/PhonemeDefinition;-><init>(Z)V

    sput-object v0, Lspeech/patts/PhonemeDefinition;->defaultInstance:Lspeech/patts/PhonemeDefinition;

    .line 1111
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 1112
    sget-object v0, Lspeech/patts/PhonemeDefinition;->defaultInstance:Lspeech/patts/PhonemeDefinition;

    invoke-direct {v0}, Lspeech/patts/PhonemeDefinition;->initFields()V

    .line 1113
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 280
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonemeDefinition;->name_:Ljava/lang/String;

    .line 322
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->rounded_:Z

    .line 329
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->voiced_:Z

    .line 336
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->schwa_:Z

    .line 343
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->vowel_:Z

    .line 350
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->closedSyllable_:Z

    .line 357
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->archi_:Z

    .line 378
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->syllabicConsonant_:Z

    .line 446
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PhonemeDefinition;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/PhonemeDefinition;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/PhonemeDefinition$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/PhonemeDefinition$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/PhonemeDefinition;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 280
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/PhonemeDefinition;->name_:Ljava/lang/String;

    .line 322
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->rounded_:Z

    .line 329
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->voiced_:Z

    .line 336
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->schwa_:Z

    .line 343
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->vowel_:Z

    .line 350
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->closedSyllable_:Z

    .line 357
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->archi_:Z

    .line 378
    iput-boolean v1, p0, Lspeech/patts/PhonemeDefinition;->syllabicConsonant_:Z

    .line 446
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/PhonemeDefinition;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$Length;)Lspeech/patts/PhonemeDefinition$Length;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Lspeech/patts/PhonemeDefinition$Length;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonemeDefinition;->length_:Lspeech/patts/PhonemeDefinition$Length;

    return-object p1
.end method

.method static synthetic access$1102(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasPoa:Z

    return p1
.end method

.method static synthetic access$1202(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;)Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonemeDefinition;->poa_:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    return-object p1
.end method

.method static synthetic access$1302(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasManner:Z

    return p1
.end method

.method static synthetic access$1402(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$Manner;)Lspeech/patts/PhonemeDefinition$Manner;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Lspeech/patts/PhonemeDefinition$Manner;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonemeDefinition;->manner_:Lspeech/patts/PhonemeDefinition$Manner;

    return-object p1
.end method

.method static synthetic access$1502(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasRounded:Z

    return p1
.end method

.method static synthetic access$1602(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->rounded_:Z

    return p1
.end method

.method static synthetic access$1702(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasVoiced:Z

    return p1
.end method

.method static synthetic access$1802(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->voiced_:Z

    return p1
.end method

.method static synthetic access$1902(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasSchwa:Z

    return p1
.end method

.method static synthetic access$2002(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->schwa_:Z

    return p1
.end method

.method static synthetic access$2102(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasVowel:Z

    return p1
.end method

.method static synthetic access$2202(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->vowel_:Z

    return p1
.end method

.method static synthetic access$2302(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasClosedSyllable:Z

    return p1
.end method

.method static synthetic access$2402(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->closedSyllable_:Z

    return p1
.end method

.method static synthetic access$2502(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasArchi:Z

    return p1
.end method

.method static synthetic access$2602(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->archi_:Z

    return p1
.end method

.method static synthetic access$2702(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasSecondaryPoa:Z

    return p1
.end method

.method static synthetic access$2802(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;)Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonemeDefinition;->secondaryPoa_:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    return-object p1
.end method

.method static synthetic access$2902(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasSecondaryManner:Z

    return p1
.end method

.method static synthetic access$3002(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$Manner;)Lspeech/patts/PhonemeDefinition$Manner;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Lspeech/patts/PhonemeDefinition$Manner;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonemeDefinition;->secondaryManner_:Lspeech/patts/PhonemeDefinition$Manner;

    return-object p1
.end method

.method static synthetic access$302(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasName:Z

    return p1
.end method

.method static synthetic access$3102(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasSyllabicConsonant:Z

    return p1
.end method

.method static synthetic access$3202(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->syllabicConsonant_:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/PhonemeDefinition;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonemeDefinition;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasHeight:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$Height;)Lspeech/patts/PhonemeDefinition$Height;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Lspeech/patts/PhonemeDefinition$Height;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonemeDefinition;->height_:Lspeech/patts/PhonemeDefinition$Height;

    return-object p1
.end method

.method static synthetic access$702(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasPosition:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/PhonemeDefinition;Lspeech/patts/PhonemeDefinition$Position;)Lspeech/patts/PhonemeDefinition$Position;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Lspeech/patts/PhonemeDefinition$Position;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/PhonemeDefinition;->position_:Lspeech/patts/PhonemeDefinition$Position;

    return-object p1
.end method

.method static synthetic access$902(Lspeech/patts/PhonemeDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/PhonemeDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/PhonemeDefinition;->hasLength:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/PhonemeDefinition;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/PhonemeDefinition;->defaultInstance:Lspeech/patts/PhonemeDefinition;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 383
    sget-object v0, Lspeech/patts/PhonemeDefinition$Height;->UNDEFINED_HEIGHT:Lspeech/patts/PhonemeDefinition$Height;

    iput-object v0, p0, Lspeech/patts/PhonemeDefinition;->height_:Lspeech/patts/PhonemeDefinition$Height;

    .line 384
    sget-object v0, Lspeech/patts/PhonemeDefinition$Position;->UNDEFINED_POSITION:Lspeech/patts/PhonemeDefinition$Position;

    iput-object v0, p0, Lspeech/patts/PhonemeDefinition;->position_:Lspeech/patts/PhonemeDefinition$Position;

    .line 385
    sget-object v0, Lspeech/patts/PhonemeDefinition$Length;->UNDEFINED_LENGTH:Lspeech/patts/PhonemeDefinition$Length;

    iput-object v0, p0, Lspeech/patts/PhonemeDefinition;->length_:Lspeech/patts/PhonemeDefinition$Length;

    .line 386
    sget-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->UNDEFINED_POA:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    iput-object v0, p0, Lspeech/patts/PhonemeDefinition;->poa_:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 387
    sget-object v0, Lspeech/patts/PhonemeDefinition$Manner;->UNDEFINED_MANNER:Lspeech/patts/PhonemeDefinition$Manner;

    iput-object v0, p0, Lspeech/patts/PhonemeDefinition;->manner_:Lspeech/patts/PhonemeDefinition$Manner;

    .line 388
    sget-object v0, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->UNDEFINED_POA:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    iput-object v0, p0, Lspeech/patts/PhonemeDefinition;->secondaryPoa_:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    .line 389
    sget-object v0, Lspeech/patts/PhonemeDefinition$Manner;->UNDEFINED_MANNER:Lspeech/patts/PhonemeDefinition$Manner;

    iput-object v0, p0, Lspeech/patts/PhonemeDefinition;->secondaryManner_:Lspeech/patts/PhonemeDefinition$Manner;

    .line 390
    return-void
.end method

.method public static newBuilder()Lspeech/patts/PhonemeDefinition$Builder;
    .locals 1

    .prologue
    .line 583
    # invokes: Lspeech/patts/PhonemeDefinition$Builder;->create()Lspeech/patts/PhonemeDefinition$Builder;
    invoke-static {}, Lspeech/patts/PhonemeDefinition$Builder;->access$100()Lspeech/patts/PhonemeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/PhonemeDefinition;)Lspeech/patts/PhonemeDefinition$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/PhonemeDefinition;

    .prologue
    .line 586
    invoke-static {}, Lspeech/patts/PhonemeDefinition;->newBuilder()Lspeech/patts/PhonemeDefinition$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/PhonemeDefinition$Builder;->mergeFrom(Lspeech/patts/PhonemeDefinition;)Lspeech/patts/PhonemeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getArchi()Z
    .locals 1

    .prologue
    .line 359
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->archi_:Z

    return v0
.end method

.method public getClosedSyllable()Z
    .locals 1

    .prologue
    .line 352
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->closedSyllable_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getDefaultInstanceForType()Lspeech/patts/PhonemeDefinition;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/PhonemeDefinition;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/PhonemeDefinition;->defaultInstance:Lspeech/patts/PhonemeDefinition;

    return-object v0
.end method

.method public getHeight()Lspeech/patts/PhonemeDefinition$Height;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition;->height_:Lspeech/patts/PhonemeDefinition$Height;

    return-object v0
.end method

.method public getLength()Lspeech/patts/PhonemeDefinition$Length;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition;->length_:Lspeech/patts/PhonemeDefinition$Length;

    return-object v0
.end method

.method public getManner()Lspeech/patts/PhonemeDefinition$Manner;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition;->manner_:Lspeech/patts/PhonemeDefinition$Manner;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getPoa()Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition;->poa_:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    return-object v0
.end method

.method public getPosition()Lspeech/patts/PhonemeDefinition$Position;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition;->position_:Lspeech/patts/PhonemeDefinition$Position;

    return-object v0
.end method

.method public getRounded()Z
    .locals 1

    .prologue
    .line 324
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->rounded_:Z

    return v0
.end method

.method public getSchwa()Z
    .locals 1

    .prologue
    .line 338
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->schwa_:Z

    return v0
.end method

.method public getSecondaryManner()Lspeech/patts/PhonemeDefinition$Manner;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition;->secondaryManner_:Lspeech/patts/PhonemeDefinition$Manner;

    return-object v0
.end method

.method public getSecondaryPoa()Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lspeech/patts/PhonemeDefinition;->secondaryPoa_:Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 448
    iget v0, p0, Lspeech/patts/PhonemeDefinition;->memoizedSerializedSize:I

    .line 449
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 513
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 451
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 452
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 453
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 456
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasHeight()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 457
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getHeight()Lspeech/patts/PhonemeDefinition$Height;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/PhonemeDefinition$Height;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 460
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasPosition()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 461
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getPosition()Lspeech/patts/PhonemeDefinition$Position;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/PhonemeDefinition$Position;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 464
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasLength()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 465
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getLength()Lspeech/patts/PhonemeDefinition$Length;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/PhonemeDefinition$Length;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 468
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasPoa()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 469
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getPoa()Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 472
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasManner()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 473
    const/4 v2, 0x6

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getManner()Lspeech/patts/PhonemeDefinition$Manner;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/PhonemeDefinition$Manner;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 476
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasRounded()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 477
    const/4 v2, 0x7

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getRounded()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 480
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasVoiced()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 481
    const/16 v2, 0x8

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getVoiced()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 484
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasSchwa()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 485
    const/16 v2, 0x9

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getSchwa()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 488
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasVowel()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 489
    const/16 v2, 0xa

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getVowel()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 492
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasClosedSyllable()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 493
    const/16 v2, 0xb

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getClosedSyllable()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 496
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasArchi()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 497
    const/16 v2, 0xc

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getArchi()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 500
    :cond_c
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasSecondaryPoa()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 501
    const/16 v2, 0xd

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getSecondaryPoa()Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 504
    :cond_d
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasSecondaryManner()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 505
    const/16 v2, 0xe

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getSecondaryManner()Lspeech/patts/PhonemeDefinition$Manner;

    move-result-object v3

    invoke-virtual {v3}, Lspeech/patts/PhonemeDefinition$Manner;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 508
    :cond_e
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasSyllabicConsonant()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 509
    const/16 v2, 0xf

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getSyllabicConsonant()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 512
    :cond_f
    iput v0, p0, Lspeech/patts/PhonemeDefinition;->memoizedSerializedSize:I

    move v1, v0

    .line 513
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto/16 :goto_0
.end method

.method public getSyllabicConsonant()Z
    .locals 1

    .prologue
    .line 380
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->syllabicConsonant_:Z

    return v0
.end method

.method public getVoiced()Z
    .locals 1

    .prologue
    .line 331
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->voiced_:Z

    return v0
.end method

.method public getVowel()Z
    .locals 1

    .prologue
    .line 345
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->vowel_:Z

    return v0
.end method

.method public hasArchi()Z
    .locals 1

    .prologue
    .line 358
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasArchi:Z

    return v0
.end method

.method public hasClosedSyllable()Z
    .locals 1

    .prologue
    .line 351
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasClosedSyllable:Z

    return v0
.end method

.method public hasHeight()Z
    .locals 1

    .prologue
    .line 288
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasHeight:Z

    return v0
.end method

.method public hasLength()Z
    .locals 1

    .prologue
    .line 302
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasLength:Z

    return v0
.end method

.method public hasManner()Z
    .locals 1

    .prologue
    .line 316
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasManner:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 281
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasName:Z

    return v0
.end method

.method public hasPoa()Z
    .locals 1

    .prologue
    .line 309
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasPoa:Z

    return v0
.end method

.method public hasPosition()Z
    .locals 1

    .prologue
    .line 295
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasPosition:Z

    return v0
.end method

.method public hasRounded()Z
    .locals 1

    .prologue
    .line 323
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasRounded:Z

    return v0
.end method

.method public hasSchwa()Z
    .locals 1

    .prologue
    .line 337
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasSchwa:Z

    return v0
.end method

.method public hasSecondaryManner()Z
    .locals 1

    .prologue
    .line 372
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasSecondaryManner:Z

    return v0
.end method

.method public hasSecondaryPoa()Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasSecondaryPoa:Z

    return v0
.end method

.method public hasSyllabicConsonant()Z
    .locals 1

    .prologue
    .line 379
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasSyllabicConsonant:Z

    return v0
.end method

.method public hasVoiced()Z
    .locals 1

    .prologue
    .line 330
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasVoiced:Z

    return v0
.end method

.method public hasVowel()Z
    .locals 1

    .prologue
    .line 344
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasVowel:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 392
    iget-boolean v0, p0, Lspeech/patts/PhonemeDefinition;->hasName:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 393
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->toBuilder()Lspeech/patts/PhonemeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/PhonemeDefinition$Builder;
    .locals 1

    .prologue
    .line 588
    invoke-static {p0}, Lspeech/patts/PhonemeDefinition;->newBuilder(Lspeech/patts/PhonemeDefinition;)Lspeech/patts/PhonemeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 398
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getSerializedSize()I

    .line 399
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 402
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasHeight()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 403
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getHeight()Lspeech/patts/PhonemeDefinition$Height;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/PhonemeDefinition$Height;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 405
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasPosition()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 406
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getPosition()Lspeech/patts/PhonemeDefinition$Position;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/PhonemeDefinition$Position;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 408
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasLength()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 409
    const/4 v0, 0x4

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getLength()Lspeech/patts/PhonemeDefinition$Length;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/PhonemeDefinition$Length;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 411
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasPoa()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 412
    const/4 v0, 0x5

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getPoa()Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 414
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasManner()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 415
    const/4 v0, 0x6

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getManner()Lspeech/patts/PhonemeDefinition$Manner;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/PhonemeDefinition$Manner;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 417
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasRounded()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 418
    const/4 v0, 0x7

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getRounded()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 420
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasVoiced()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 421
    const/16 v0, 0x8

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getVoiced()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 423
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasSchwa()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 424
    const/16 v0, 0x9

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getSchwa()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 426
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasVowel()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 427
    const/16 v0, 0xa

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getVowel()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 429
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasClosedSyllable()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 430
    const/16 v0, 0xb

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getClosedSyllable()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 432
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasArchi()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 433
    const/16 v0, 0xc

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getArchi()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 435
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasSecondaryPoa()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 436
    const/16 v0, 0xd

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getSecondaryPoa()Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/PhonemeDefinition$PlaceOfArticulation;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 438
    :cond_c
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasSecondaryManner()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 439
    const/16 v0, 0xe

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getSecondaryManner()Lspeech/patts/PhonemeDefinition$Manner;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/PhonemeDefinition$Manner;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 441
    :cond_d
    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->hasSyllabicConsonant()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 442
    const/16 v0, 0xf

    invoke-virtual {p0}, Lspeech/patts/PhonemeDefinition;->getSyllabicConsonant()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 444
    :cond_e
    return-void
.end method
