.class public final Lspeech/patts/CostFunctions$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "CostFunctions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/CostFunctions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/CostFunctions;",
        "Lspeech/patts/CostFunctions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/CostFunctions;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 353
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/CostFunctions$Builder;
    .locals 1

    .prologue
    .line 347
    invoke-static {}, Lspeech/patts/CostFunctions$Builder;->create()Lspeech/patts/CostFunctions$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/CostFunctions$Builder;
    .locals 3

    .prologue
    .line 356
    new-instance v0, Lspeech/patts/CostFunctions$Builder;

    invoke-direct {v0}, Lspeech/patts/CostFunctions$Builder;-><init>()V

    .line 357
    .local v0, "builder":Lspeech/patts/CostFunctions$Builder;
    new-instance v1, Lspeech/patts/CostFunctions;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/CostFunctions;-><init>(Lspeech/patts/CostFunctions$1;)V

    iput-object v1, v0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    .line 358
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 347
    invoke-virtual {p0}, Lspeech/patts/CostFunctions$Builder;->build()Lspeech/patts/CostFunctions;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/CostFunctions;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/CostFunctions$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    invoke-static {v0}, Lspeech/patts/CostFunctions$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 389
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/CostFunctions$Builder;->buildPartial()Lspeech/patts/CostFunctions;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/CostFunctions;
    .locals 3

    .prologue
    .line 402
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    if-nez v1, :cond_0

    .line 403
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 406
    :cond_0
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/CostFunctions;->access$300(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 407
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    iget-object v2, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/CostFunctions;->access$300(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/CostFunctions;->access$302(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 410
    :cond_1
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/CostFunctions;->access$400(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 411
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    iget-object v2, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/CostFunctions;->access$400(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/CostFunctions;->access$402(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 414
    :cond_2
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/CostFunctions;->access$500(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 415
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    iget-object v2, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/CostFunctions;->access$500(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/CostFunctions;->access$502(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 418
    :cond_3
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/CostFunctions;->access$600(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 419
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    iget-object v2, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/CostFunctions;->access$600(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/CostFunctions;->access$602(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 422
    :cond_4
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/CostFunctions;->access$700(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_5

    .line 423
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    iget-object v2, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/CostFunctions;->access$700(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/CostFunctions;->access$702(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 426
    :cond_5
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/CostFunctions;->access$800(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_6

    .line 427
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    iget-object v2, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/CostFunctions;->access$800(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/CostFunctions;->access$802(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 430
    :cond_6
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->table_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/CostFunctions;->access$900(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_7

    .line 431
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    iget-object v2, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->table_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/CostFunctions;->access$900(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/CostFunctions;->table_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/CostFunctions;->access$902(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 434
    :cond_7
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/CostFunctions;->access$1000(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_8

    .line 435
    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    iget-object v2, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/CostFunctions;->access$1000(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/CostFunctions;->access$1002(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 438
    :cond_8
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    .line 439
    .local v0, "returnMe":Lspeech/patts/CostFunctions;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    .line 440
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 347
    invoke-virtual {p0}, Lspeech/patts/CostFunctions$Builder;->clone()Lspeech/patts/CostFunctions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 347
    invoke-virtual {p0}, Lspeech/patts/CostFunctions$Builder;->clone()Lspeech/patts/CostFunctions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 347
    invoke-virtual {p0}, Lspeech/patts/CostFunctions$Builder;->clone()Lspeech/patts/CostFunctions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/CostFunctions$Builder;
    .locals 2

    .prologue
    .line 375
    invoke-static {}, Lspeech/patts/CostFunctions$Builder;->create()Lspeech/patts/CostFunctions$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    invoke-virtual {v0, v1}, Lspeech/patts/CostFunctions$Builder;->mergeFrom(Lspeech/patts/CostFunctions;)Lspeech/patts/CostFunctions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    invoke-virtual {v0}, Lspeech/patts/CostFunctions;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 347
    check-cast p1, Lspeech/patts/CostFunctions;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/CostFunctions$Builder;->mergeFrom(Lspeech/patts/CostFunctions;)Lspeech/patts/CostFunctions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/CostFunctions;)Lspeech/patts/CostFunctions$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/CostFunctions;

    .prologue
    .line 444
    invoke-static {}, Lspeech/patts/CostFunctions;->getDefaultInstance()Lspeech/patts/CostFunctions;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 496
    :cond_0
    :goto_0
    return-object p0

    .line 445
    :cond_1
    # getter for: Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$300(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 446
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$300(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 447
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/CostFunctions;->access$302(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 449
    :cond_2
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$300(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/CostFunctions;->basetypeTarget_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$300(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 451
    :cond_3
    # getter for: Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$400(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 452
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$400(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 453
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/CostFunctions;->access$402(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 455
    :cond_4
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$400(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/CostFunctions;->basetypeJoin_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$400(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 457
    :cond_5
    # getter for: Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$500(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 458
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$500(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 459
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/CostFunctions;->access$502(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 461
    :cond_6
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$500(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/CostFunctions;->modelTarget_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$500(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 463
    :cond_7
    # getter for: Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$600(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 464
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$600(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 465
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/CostFunctions;->access$602(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 467
    :cond_8
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$600(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/CostFunctions;->modelJoin_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$600(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 469
    :cond_9
    # getter for: Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$700(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 470
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$700(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 471
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/CostFunctions;->access$702(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 473
    :cond_a
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$700(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/CostFunctions;->unitTarget_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$700(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 475
    :cond_b
    # getter for: Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$800(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 476
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$800(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 477
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/CostFunctions;->access$802(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 479
    :cond_c
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$800(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/CostFunctions;->unitJoin_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$800(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 481
    :cond_d
    # getter for: Lspeech/patts/CostFunctions;->table_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$900(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 482
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->table_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$900(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 483
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/CostFunctions;->table_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/CostFunctions;->access$902(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 485
    :cond_e
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->table_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$900(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/CostFunctions;->table_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$900(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 487
    :cond_f
    # getter for: Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$1000(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    .line 488
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$1000(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 489
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/CostFunctions;->access$1002(Lspeech/patts/CostFunctions;Ljava/util/List;)Ljava/util/List;

    .line 491
    :cond_10
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # getter for: Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/CostFunctions;->access$1000(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/CostFunctions;->sparseTable_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/CostFunctions;->access$1000(Lspeech/patts/CostFunctions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 493
    :cond_11
    invoke-virtual {p1}, Lspeech/patts/CostFunctions;->hasUnitType()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494
    invoke-virtual {p1}, Lspeech/patts/CostFunctions;->getUnitType()Lspeech/patts/CostFunctions$UnitType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/CostFunctions$Builder;->setUnitType(Lspeech/patts/CostFunctions$UnitType;)Lspeech/patts/CostFunctions$Builder;

    goto/16 :goto_0
.end method

.method public setUnitType(Lspeech/patts/CostFunctions$UnitType;)Lspeech/patts/CostFunctions$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/CostFunctions$UnitType;

    .prologue
    .line 991
    if-nez p1, :cond_0

    .line 992
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 994
    :cond_0
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/CostFunctions;->hasUnitType:Z
    invoke-static {v0, v1}, Lspeech/patts/CostFunctions;->access$1102(Lspeech/patts/CostFunctions;Z)Z

    .line 995
    iget-object v0, p0, Lspeech/patts/CostFunctions$Builder;->result:Lspeech/patts/CostFunctions;

    # setter for: Lspeech/patts/CostFunctions;->unitType_:Lspeech/patts/CostFunctions$UnitType;
    invoke-static {v0, p1}, Lspeech/patts/CostFunctions;->access$1202(Lspeech/patts/CostFunctions;Lspeech/patts/CostFunctions$UnitType;)Lspeech/patts/CostFunctions$UnitType;

    .line 996
    return-object p0
.end method
