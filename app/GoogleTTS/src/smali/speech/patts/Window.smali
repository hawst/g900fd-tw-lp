.class public final Lspeech/patts/Window;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Window.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/Window$1;,
        Lspeech/patts/Window$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/Window;


# instance fields
.field private coefficientsMemoizedSerializedSize:I

.field private coefficients_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 298
    new-instance v0, Lspeech/patts/Window;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/Window;-><init>(Z)V

    sput-object v0, Lspeech/patts/Window;->defaultInstance:Lspeech/patts/Window;

    .line 299
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 300
    sget-object v0, Lspeech/patts/Window;->defaultInstance:Lspeech/patts/Window;

    invoke-direct {v0}, Lspeech/patts/Window;->initFields()V

    .line 301
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Window;->coefficients_:Ljava/util/List;

    .line 33
    iput v1, p0, Lspeech/patts/Window;->coefficientsMemoizedSerializedSize:I

    .line 53
    iput v1, p0, Lspeech/patts/Window;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/Window;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/Window$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/Window$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/Window;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, -0x1

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Window;->coefficients_:Ljava/util/List;

    .line 33
    iput v1, p0, Lspeech/patts/Window;->coefficientsMemoizedSerializedSize:I

    .line 53
    iput v1, p0, Lspeech/patts/Window;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/Window;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Window;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Window;->coefficients_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/Window;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Window;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Window;->coefficients_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/Window;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/Window;->defaultInstance:Lspeech/patts/Window;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method public static newBuilder()Lspeech/patts/Window$Builder;
    .locals 1

    .prologue
    .line 141
    # invokes: Lspeech/patts/Window$Builder;->create()Lspeech/patts/Window$Builder;
    invoke-static {}, Lspeech/patts/Window$Builder;->access$100()Lspeech/patts/Window$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/Window;)Lspeech/patts/Window$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/Window;

    .prologue
    .line 144
    invoke-static {}, Lspeech/patts/Window;->newBuilder()Lspeech/patts/Window$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/Window$Builder;->mergeFrom(Lspeech/patts/Window;)Lspeech/patts/Window$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCoefficientsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/Window;->coefficients_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Window;->getDefaultInstanceForType()Lspeech/patts/Window;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/Window;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/Window;->defaultInstance:Lspeech/patts/Window;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 55
    iget v1, p0, Lspeech/patts/Window;->memoizedSerializedSize:I

    .line 56
    .local v1, "size":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    .line 71
    .end local v1    # "size":I
    .local v2, "size":I
    :goto_0
    return v2

    .line 58
    .end local v2    # "size":I
    .restart local v1    # "size":I
    :cond_0
    const/4 v1, 0x0

    .line 60
    const/4 v0, 0x0

    .line 61
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/Window;->getCoefficientsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x4

    .line 62
    add-int/2addr v1, v0

    .line 63
    invoke-virtual {p0}, Lspeech/patts/Window;->getCoefficientsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 64
    add-int/lit8 v1, v1, 0x1

    .line 65
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 68
    :cond_1
    iput v0, p0, Lspeech/patts/Window;->coefficientsMemoizedSerializedSize:I

    .line 70
    iput v1, p0, Lspeech/patts/Window;->memoizedSerializedSize:I

    move v2, v1

    .line 71
    .end local v1    # "size":I
    .restart local v2    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Window;->toBuilder()Lspeech/patts/Window$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/Window$Builder;
    .locals 1

    .prologue
    .line 146
    invoke-static {p0}, Lspeech/patts/Window;->newBuilder(Lspeech/patts/Window;)Lspeech/patts/Window$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lspeech/patts/Window;->getSerializedSize()I

    .line 44
    invoke-virtual {p0}, Lspeech/patts/Window;->getCoefficientsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 45
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 46
    iget v2, p0, Lspeech/patts/Window;->coefficientsMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 48
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Window;->getCoefficientsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 49
    .local v0, "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_0

    .line 51
    .end local v0    # "element":F
    :cond_1
    return-void
.end method
