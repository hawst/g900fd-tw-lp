.class public final Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "DummyNameThatNoOneEverSees.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;",
        "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 524
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1000()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    .locals 1

    .prologue
    .line 518
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    .locals 3

    .prologue
    .line 527
    new-instance v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    invoke-direct {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;-><init>()V

    .line 528
    .local v0, "builder":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    new-instance v1, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;-><init>(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$1;)V

    iput-object v1, v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    .line 529
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 518
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->build()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 558
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    invoke-static {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 560
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->buildPartial()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    .locals 3

    .prologue
    .line 573
    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    if-nez v1, :cond_0

    .line 574
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 577
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    .line 578
    .local v0, "returnMe":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    .line 579
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 518
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 518
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 518
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    .locals 2

    .prologue
    .line 546
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    invoke-virtual {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    invoke-virtual {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 518
    check-cast p1, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    .prologue
    .line 583
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getDefaultInstance()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 596
    :cond_0
    :goto_0
    return-object p0

    .line 584
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 585
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->setName(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    .line 587
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasRe()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 588
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getRe()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->setRe(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    .line 590
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasNumberOutput()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 591
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getNumberOutput()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->setNumberOutput(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    .line 593
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasSuffixOutput()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getSuffixOutput()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->setSuffixOutput(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 643
    if-nez p1, :cond_0

    .line 644
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 646
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasName:Z
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->access$1202(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Z)Z

    .line 647
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->access$1302(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Ljava/lang/String;)Ljava/lang/String;

    .line 648
    return-object p0
.end method

.method public setNumberOutput(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 685
    if-nez p1, :cond_0

    .line 686
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 688
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasNumberOutput:Z
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->access$1602(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Z)Z

    .line 689
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->numberOutput_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->access$1702(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Ljava/lang/String;)Ljava/lang/String;

    .line 690
    return-object p0
.end method

.method public setRe(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 664
    if-nez p1, :cond_0

    .line 665
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 667
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasRe:Z
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->access$1402(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Z)Z

    .line 668
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->re_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->access$1502(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Ljava/lang/String;)Ljava/lang/String;

    .line 669
    return-object p0
.end method

.method public setSuffixOutput(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 706
    if-nez p1, :cond_0

    .line 707
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 709
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasSuffixOutput:Z
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->access$1802(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Z)Z

    .line 710
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->suffixOutput_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->access$1902(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Ljava/lang/String;)Ljava/lang/String;

    .line 711
    return-object p0
.end method
