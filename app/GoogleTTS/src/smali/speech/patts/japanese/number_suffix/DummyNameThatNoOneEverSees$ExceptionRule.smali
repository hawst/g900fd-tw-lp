.class public final Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "DummyNameThatNoOneEverSees.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExceptionRule"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;


# instance fields
.field private hasNumberOutput:Z

.field private hasRe:Z

.field private hasSuffixOutput:Z

.field private memoizedSerializedSize:I

.field private numberOutput_:Ljava/lang/String;

.field private re_:Ljava/lang/String;

.field private suffixOutput_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1062
    new-instance v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;-><init>(Z)V

    sput-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    .line 1063
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 1064
    sget-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    invoke-direct {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->initFields()V

    .line 1065
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 734
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 751
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->re_:Ljava/lang/String;

    .line 758
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->numberOutput_:Ljava/lang/String;

    .line 765
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->suffixOutput_:Ljava/lang/String;

    .line 789
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->memoizedSerializedSize:I

    .line 735
    invoke-direct {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->initFields()V

    .line 736
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$1;

    .prologue
    .line 731
    invoke-direct {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 737
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 751
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->re_:Ljava/lang/String;

    .line 758
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->numberOutput_:Ljava/lang/String;

    .line 765
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->suffixOutput_:Ljava/lang/String;

    .line 789
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->memoizedSerializedSize:I

    .line 737
    return-void
.end method

.method static synthetic access$2302(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    .param p1, "x1"    # Z

    .prologue
    .line 731
    iput-boolean p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasRe:Z

    return p1
.end method

.method static synthetic access$2402(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 731
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->re_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2502(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    .param p1, "x1"    # Z

    .prologue
    .line 731
    iput-boolean p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasNumberOutput:Z

    return p1
.end method

.method static synthetic access$2602(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 731
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->numberOutput_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2702(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    .param p1, "x1"    # Z

    .prologue
    .line 731
    iput-boolean p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasSuffixOutput:Z

    return p1
.end method

.method static synthetic access$2802(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 731
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->suffixOutput_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    .locals 1

    .prologue
    .line 741
    sget-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 770
    return-void
.end method

.method public static newBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    .locals 1

    .prologue
    .line 878
    # invokes: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->access$2100()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    .prologue
    .line 881
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->newBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 731
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->getDefaultInstanceForType()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    .locals 1

    .prologue
    .line 745
    sget-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    return-object v0
.end method

.method public getNumberOutput()Ljava/lang/String;
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->numberOutput_:Ljava/lang/String;

    return-object v0
.end method

.method public getRe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 753
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->re_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 791
    iget v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->memoizedSerializedSize:I

    .line 792
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 808
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 794
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 795
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasRe()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 796
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->getRe()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 799
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasNumberOutput()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 800
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->getNumberOutput()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 803
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasSuffixOutput()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 804
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->getSuffixOutput()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 807
    :cond_3
    iput v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->memoizedSerializedSize:I

    move v1, v0

    .line 808
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getSuffixOutput()Ljava/lang/String;
    .locals 1

    .prologue
    .line 767
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->suffixOutput_:Ljava/lang/String;

    return-object v0
.end method

.method public hasNumberOutput()Z
    .locals 1

    .prologue
    .line 759
    iget-boolean v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasNumberOutput:Z

    return v0
.end method

.method public hasRe()Z
    .locals 1

    .prologue
    .line 752
    iget-boolean v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasRe:Z

    return v0
.end method

.method public hasSuffixOutput()Z
    .locals 1

    .prologue
    .line 766
    iget-boolean v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasSuffixOutput:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 772
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 731
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->toBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    .locals 1

    .prologue
    .line 883
    invoke-static {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->newBuilder(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 777
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->getSerializedSize()I

    .line 778
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasRe()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 779
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->getRe()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 781
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasNumberOutput()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 782
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->getNumberOutput()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 784
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasSuffixOutput()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 785
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->getSuffixOutput()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 787
    :cond_2
    return-void
.end method
