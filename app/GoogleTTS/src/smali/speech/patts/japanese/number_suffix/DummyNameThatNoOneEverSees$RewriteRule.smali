.class public final Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "DummyNameThatNoOneEverSees.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RewriteRule"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;


# instance fields
.field private hasName:Z

.field private hasNumberOutput:Z

.field private hasRe:Z

.field private hasSuffixOutput:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private numberOutput_:Ljava/lang/String;

.field private re_:Ljava/lang/String;

.field private suffixOutput_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 723
    new-instance v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;-><init>(Z)V

    sput-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    .line 724
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 725
    sget-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    invoke-direct {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->initFields()V

    .line 726
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 353
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 370
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->name_:Ljava/lang/String;

    .line 377
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->re_:Ljava/lang/String;

    .line 384
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->numberOutput_:Ljava/lang/String;

    .line 391
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->suffixOutput_:Ljava/lang/String;

    .line 418
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->memoizedSerializedSize:I

    .line 354
    invoke-direct {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->initFields()V

    .line 355
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$1;

    .prologue
    .line 350
    invoke-direct {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 370
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->name_:Ljava/lang/String;

    .line 377
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->re_:Ljava/lang/String;

    .line 384
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->numberOutput_:Ljava/lang/String;

    .line 391
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->suffixOutput_:Ljava/lang/String;

    .line 418
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->memoizedSerializedSize:I

    .line 356
    return-void
.end method

.method static synthetic access$1202(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    .param p1, "x1"    # Z

    .prologue
    .line 350
    iput-boolean p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasName:Z

    return p1
.end method

.method static synthetic access$1302(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 350
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1402(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    .param p1, "x1"    # Z

    .prologue
    .line 350
    iput-boolean p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasRe:Z

    return p1
.end method

.method static synthetic access$1502(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 350
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->re_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1602(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    .param p1, "x1"    # Z

    .prologue
    .line 350
    iput-boolean p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasNumberOutput:Z

    return p1
.end method

.method static synthetic access$1702(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 350
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->numberOutput_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1802(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    .param p1, "x1"    # Z

    .prologue
    .line 350
    iput-boolean p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasSuffixOutput:Z

    return p1
.end method

.method static synthetic access$1902(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 350
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->suffixOutput_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    .locals 1

    .prologue
    .line 360
    sget-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 396
    return-void
.end method

.method public static newBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    .locals 1

    .prologue
    .line 511
    # invokes: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->access$1000()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    .prologue
    .line 514
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->newBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;->mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 350
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getDefaultInstanceForType()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    .locals 1

    .prologue
    .line 364
    sget-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getNumberOutput()Ljava/lang/String;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->numberOutput_:Ljava/lang/String;

    return-object v0
.end method

.method public getRe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->re_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 420
    iget v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->memoizedSerializedSize:I

    .line 421
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 441
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 423
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 424
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 425
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 428
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasRe()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 429
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getRe()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 432
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasNumberOutput()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 433
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getNumberOutput()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 436
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasSuffixOutput()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 437
    const/4 v2, 0x4

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getSuffixOutput()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 440
    :cond_4
    iput v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->memoizedSerializedSize:I

    move v1, v0

    .line 441
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getSuffixOutput()Ljava/lang/String;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->suffixOutput_:Ljava/lang/String;

    return-object v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 371
    iget-boolean v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasName:Z

    return v0
.end method

.method public hasNumberOutput()Z
    .locals 1

    .prologue
    .line 385
    iget-boolean v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasNumberOutput:Z

    return v0
.end method

.method public hasRe()Z
    .locals 1

    .prologue
    .line 378
    iget-boolean v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasRe:Z

    return v0
.end method

.method public hasSuffixOutput()Z
    .locals 1

    .prologue
    .line 392
    iget-boolean v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasSuffixOutput:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 398
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 350
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->toBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;
    .locals 1

    .prologue
    .line 516
    invoke-static {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->newBuilder(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 403
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getSerializedSize()I

    .line 404
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 407
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasRe()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 408
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getRe()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 410
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasNumberOutput()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 411
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getNumberOutput()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 413
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->hasSuffixOutput()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 414
    const/4 v0, 0x4

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;->getSuffixOutput()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 416
    :cond_3
    return-void
.end method
