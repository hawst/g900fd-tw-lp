.class public final Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "DummyNameThatNoOneEverSees.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SuffixWordVerbalizerRules"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;


# instance fields
.field private category_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;",
            ">;"
        }
    .end annotation
.end field

.field private exceptionRule_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private rule_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1533
    new-instance v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;-><init>(Z)V

    sput-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    .line 1534
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 1535
    sget-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    invoke-direct {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->initFields()V

    .line 1536
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1073
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1089
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;

    .line 1101
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;

    .line 1113
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;

    .line 1143
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->memoizedSerializedSize:I

    .line 1074
    invoke-direct {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->initFields()V

    .line 1075
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$1;

    .prologue
    .line 1070
    invoke-direct {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 1076
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1089
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;

    .line 1101
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;

    .line 1113
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;

    .line 1143
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->memoizedSerializedSize:I

    .line 1076
    return-void
.end method

.method static synthetic access$3200(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    .prologue
    .line 1070
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3202(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 1070
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3300(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    .prologue
    .line 1070
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3302(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 1070
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3400(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    .prologue
    .line 1070
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3402(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 1070
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;
    .locals 1

    .prologue
    .line 1080
    sget-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 1124
    return-void
.end method

.method public static newBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;
    .locals 1

    .prologue
    .line 1232
    # invokes: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->access$3000()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    .prologue
    .line 1235
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->newBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCategoryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1092
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1070
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->getDefaultInstanceForType()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;
    .locals 1

    .prologue
    .line 1084
    sget-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    return-object v0
.end method

.method public getExceptionRuleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1116
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;

    return-object v0
.end method

.method public getRuleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1104
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 1145
    iget v2, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->memoizedSerializedSize:I

    .line 1146
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 1162
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 1148
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 1149
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->getCategoryList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    .line 1150
    .local v0, "element":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1152
    goto :goto_1

    .line 1153
    .end local v0    # "element":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->getRuleList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    .line 1154
    .local v0, "element":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1156
    goto :goto_2

    .line 1157
    .end local v0    # "element":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->getExceptionRuleList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    .line 1158
    .local v0, "element":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1160
    goto :goto_3

    .line 1161
    .end local v0    # "element":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    :cond_3
    iput v2, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->memoizedSerializedSize:I

    move v3, v2

    .line 1162
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 1126
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1070
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->toBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;
    .locals 1

    .prologue
    .line 1237
    invoke-static {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->newBuilder(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1131
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->getSerializedSize()I

    .line 1132
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->getCategoryList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    .line 1133
    .local v0, "element":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 1135
    .end local v0    # "element":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->getRuleList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;

    .line 1136
    .local v0, "element":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 1138
    .end local v0    # "element":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$RewriteRule;
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->getExceptionRuleList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    .line 1139
    .local v0, "element":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    .line 1141
    .end local v0    # "element":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    :cond_2
    return-void
.end method
