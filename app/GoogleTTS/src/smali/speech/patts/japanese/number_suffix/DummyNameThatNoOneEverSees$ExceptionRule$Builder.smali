.class public final Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "DummyNameThatNoOneEverSees.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;",
        "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 891
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$2100()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    .locals 1

    .prologue
    .line 885
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    .locals 3

    .prologue
    .line 894
    new-instance v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    invoke-direct {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;-><init>()V

    .line 895
    .local v0, "builder":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    new-instance v1, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;-><init>(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$1;)V

    iput-object v1, v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    .line 896
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 885
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->build()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    .locals 1

    .prologue
    .line 924
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 925
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    invoke-static {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 927
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->buildPartial()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    .locals 3

    .prologue
    .line 940
    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    if-nez v1, :cond_0

    .line 941
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 944
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    .line 945
    .local v0, "returnMe":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    .line 946
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 885
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 885
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 885
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    .locals 2

    .prologue
    .line 913
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    invoke-virtual {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 921
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    invoke-virtual {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 885
    check-cast p1, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    .prologue
    .line 950
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->getDefaultInstance()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 960
    :cond_0
    :goto_0
    return-object p0

    .line 951
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasRe()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 952
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->getRe()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->setRe(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    .line 954
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasNumberOutput()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 955
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->getNumberOutput()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->setNumberOutput(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    .line 957
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasSuffixOutput()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 958
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->getSuffixOutput()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->setSuffixOutput(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;

    goto :goto_0
.end method

.method public setNumberOutput(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1024
    if-nez p1, :cond_0

    .line 1025
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1027
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasNumberOutput:Z
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->access$2502(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;Z)Z

    .line 1028
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->numberOutput_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->access$2602(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;Ljava/lang/String;)Ljava/lang/String;

    .line 1029
    return-object p0
.end method

.method public setRe(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1003
    if-nez p1, :cond_0

    .line 1004
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1006
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasRe:Z
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->access$2302(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;Z)Z

    .line 1007
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->re_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->access$2402(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;Ljava/lang/String;)Ljava/lang/String;

    .line 1008
    return-object p0
.end method

.method public setSuffixOutput(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1045
    if-nez p1, :cond_0

    .line 1046
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1048
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->hasSuffixOutput:Z
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->access$2702(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;Z)Z

    .line 1049
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->suffixOutput_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;->access$2802(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$ExceptionRule;Ljava/lang/String;)Ljava/lang/String;

    .line 1050
    return-object p0
.end method
