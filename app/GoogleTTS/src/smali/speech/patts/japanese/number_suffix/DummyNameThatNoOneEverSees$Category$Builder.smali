.class public final Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "DummyNameThatNoOneEverSees.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;",
        "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    .locals 1

    .prologue
    .line 165
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    .locals 3

    .prologue
    .line 174
    new-instance v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    invoke-direct {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;-><init>()V

    .line 175
    .local v0, "builder":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    new-instance v1, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;-><init>(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$1;)V

    iput-object v1, v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    .line 176
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->build()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    invoke-static {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 207
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->buildPartial()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    .locals 3

    .prologue
    .line 220
    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    if-nez v1, :cond_0

    .line 221
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 224
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    .line 225
    .local v0, "returnMe":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    .line 226
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    .locals 2

    .prologue
    .line 193
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    invoke-virtual {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    invoke-virtual {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 165
    check-cast p1, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    .prologue
    .line 230
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->getDefaultInstance()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-object p0

    .line 231
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasRule()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->getRule()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->setRule(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    .line 234
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasRe()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 235
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->getRe()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->setRe(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    .line 237
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasReadingRe()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    invoke-virtual {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->getReadingRe()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->setReadingRe(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    goto :goto_0
.end method

.method public setRe(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 304
    if-nez p1, :cond_0

    .line 305
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 307
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasRe:Z
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->access$502(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;Z)Z

    .line 308
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->re_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->access$602(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;Ljava/lang/String;)Ljava/lang/String;

    .line 309
    return-object p0
.end method

.method public setReadingRe(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 325
    if-nez p1, :cond_0

    .line 326
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 328
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasReadingRe:Z
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->access$702(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;Z)Z

    .line 329
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->readingRe_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->access$802(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;Ljava/lang/String;)Ljava/lang/String;

    .line 330
    return-object p0
.end method

.method public setRule(Ljava/lang/String;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 283
    if-nez p1, :cond_0

    .line 284
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 286
    :cond_0
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasRule:Z
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->access$302(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;Z)Z

    .line 287
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->rule_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->access$402(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;Ljava/lang/String;)Ljava/lang/String;

    .line 288
    return-object p0
.end method
