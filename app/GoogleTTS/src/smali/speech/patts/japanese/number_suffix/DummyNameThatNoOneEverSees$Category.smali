.class public final Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "DummyNameThatNoOneEverSees.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Category"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;


# instance fields
.field private hasRe:Z

.field private hasReadingRe:Z

.field private hasRule:Z

.field private memoizedSerializedSize:I

.field private re_:Ljava/lang/String;

.field private readingRe_:Ljava/lang/String;

.field private rule_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 342
    new-instance v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;-><init>(Z)V

    sput-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    .line 343
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 344
    sget-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    invoke-direct {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->initFields()V

    .line 345
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->rule_:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->re_:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->readingRe_:Ljava/lang/String;

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->memoizedSerializedSize:I

    .line 15
    invoke-direct {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->initFields()V

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$1;

    .prologue
    .line 11
    invoke-direct {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->rule_:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->re_:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->readingRe_:Ljava/lang/String;

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->memoizedSerializedSize:I

    .line 17
    return-void
.end method

.method static synthetic access$302(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasRule:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->rule_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasRe:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->re_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasReadingRe:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->readingRe_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public static newBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    .locals 1

    .prologue
    .line 158
    # invokes: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->access$100()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    .prologue
    .line 161
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->newBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;->mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->getDefaultInstanceForType()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->defaultInstance:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;

    return-object v0
.end method

.method public getRe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->re_:Ljava/lang/String;

    return-object v0
.end method

.method public getReadingRe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->readingRe_:Ljava/lang/String;

    return-object v0
.end method

.method public getRule()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->rule_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 71
    iget v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->memoizedSerializedSize:I

    .line 72
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 88
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 74
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 75
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasRule()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 76
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->getRule()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 79
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasRe()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 80
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->getRe()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 83
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasReadingRe()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 84
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->getReadingRe()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 87
    :cond_3
    iput v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->memoizedSerializedSize:I

    move v1, v0

    .line 88
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasRe()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasRe:Z

    return v0
.end method

.method public hasReadingRe()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasReadingRe:Z

    return v0
.end method

.method public hasRule()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasRule:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->toBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;
    .locals 1

    .prologue
    .line 163
    invoke-static {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->newBuilder(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->getSerializedSize()I

    .line 58
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasRule()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->getRule()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 61
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasRe()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->getRe()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 64
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->hasReadingRe()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$Category;->getReadingRe()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 67
    :cond_2
    return-void
.end method
