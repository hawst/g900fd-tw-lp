.class public final Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "DummyNameThatNoOneEverSees.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;",
        "Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1245
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$3000()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;
    .locals 1

    .prologue
    .line 1239
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;
    .locals 3

    .prologue
    .line 1248
    new-instance v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    invoke-direct {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;-><init>()V

    .line 1249
    .local v0, "builder":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;
    new-instance v1, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;-><init>(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$1;)V

    iput-object v1, v0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    .line 1250
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1239
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->build()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;
    .locals 1

    .prologue
    .line 1278
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1279
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    invoke-static {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1281
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->buildPartial()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;
    .locals 3

    .prologue
    .line 1294
    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    if-nez v1, :cond_0

    .line 1295
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1298
    :cond_0
    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3200(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 1299
    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    iget-object v2, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3200(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3202(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;Ljava/util/List;)Ljava/util/List;

    .line 1302
    :cond_1
    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3300(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 1303
    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    iget-object v2, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3300(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3302(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;Ljava/util/List;)Ljava/util/List;

    .line 1306
    :cond_2
    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3400(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 1307
    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    iget-object v2, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3400(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3402(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;Ljava/util/List;)Ljava/util/List;

    .line 1310
    :cond_3
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    .line 1311
    .local v0, "returnMe":Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    .line 1312
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1239
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1239
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1239
    invoke-virtual {p0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;
    .locals 2

    .prologue
    .line 1267
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->create()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    invoke-virtual {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 1275
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    invoke-virtual {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 1239
    check-cast p1, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    .prologue
    .line 1316
    invoke-static {}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->getDefaultInstance()Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1335
    :cond_0
    :goto_0
    return-object p0

    .line 1317
    :cond_1
    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3200(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1318
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3200(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1319
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3202(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;Ljava/util/List;)Ljava/util/List;

    .line 1321
    :cond_2
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3200(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->category_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3200(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1323
    :cond_3
    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3300(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1324
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3300(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1325
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3302(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;Ljava/util/List;)Ljava/util/List;

    .line 1327
    :cond_4
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3300(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->rule_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3300(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1329
    :cond_5
    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3400(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1330
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3400(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1331
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3402(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;Ljava/util/List;)Ljava/util/List;

    .line 1333
    :cond_6
    iget-object v0, p0, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules$Builder;->result:Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3400(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->exceptionRule_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;->access$3400(Lspeech/patts/japanese/number_suffix/DummyNameThatNoOneEverSees$SuffixWordVerbalizerRules;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method
