.class public final Lspeech/patts/FeatureSet$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FeatureSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/FeatureSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/FeatureSet;",
        "Lspeech/patts/FeatureSet$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/FeatureSet;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/FeatureSet$Builder;
    .locals 1

    .prologue
    .line 204
    invoke-static {}, Lspeech/patts/FeatureSet$Builder;->create()Lspeech/patts/FeatureSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/FeatureSet$Builder;
    .locals 3

    .prologue
    .line 213
    new-instance v0, Lspeech/patts/FeatureSet$Builder;

    invoke-direct {v0}, Lspeech/patts/FeatureSet$Builder;-><init>()V

    .line 214
    .local v0, "builder":Lspeech/patts/FeatureSet$Builder;
    new-instance v1, Lspeech/patts/FeatureSet;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/FeatureSet;-><init>(Lspeech/patts/FeatureSet$1;)V

    iput-object v1, v0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    .line 215
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lspeech/patts/FeatureSet$Builder;->build()Lspeech/patts/FeatureSet;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/FeatureSet;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/FeatureSet$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 244
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    invoke-static {v0}, Lspeech/patts/FeatureSet$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 246
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FeatureSet$Builder;->buildPartial()Lspeech/patts/FeatureSet;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/FeatureSet;
    .locals 3

    .prologue
    .line 259
    iget-object v1, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    if-nez v1, :cond_0

    .line 260
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 263
    :cond_0
    iget-object v1, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FeatureSet;->access$300(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 264
    iget-object v1, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    iget-object v2, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FeatureSet;->access$300(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FeatureSet;->access$302(Lspeech/patts/FeatureSet;Ljava/util/List;)Ljava/util/List;

    .line 267
    :cond_1
    iget-object v1, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->question_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FeatureSet;->access$400(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 268
    iget-object v1, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    iget-object v2, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->question_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FeatureSet;->access$400(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FeatureSet;->question_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FeatureSet;->access$402(Lspeech/patts/FeatureSet;Ljava/util/List;)Ljava/util/List;

    .line 271
    :cond_2
    iget-object v1, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FeatureSet;->access$500(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 272
    iget-object v1, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    iget-object v2, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FeatureSet;->access$500(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FeatureSet;->access$502(Lspeech/patts/FeatureSet;Ljava/util/List;)Ljava/util/List;

    .line 275
    :cond_3
    iget-object v1, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FeatureSet;->access$600(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 276
    iget-object v1, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    iget-object v2, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FeatureSet;->access$600(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FeatureSet;->access$602(Lspeech/patts/FeatureSet;Ljava/util/List;)Ljava/util/List;

    .line 279
    :cond_4
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    .line 280
    .local v0, "returnMe":Lspeech/patts/FeatureSet;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    .line 281
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lspeech/patts/FeatureSet$Builder;->clone()Lspeech/patts/FeatureSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lspeech/patts/FeatureSet$Builder;->clone()Lspeech/patts/FeatureSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 204
    invoke-virtual {p0}, Lspeech/patts/FeatureSet$Builder;->clone()Lspeech/patts/FeatureSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/FeatureSet$Builder;
    .locals 2

    .prologue
    .line 232
    invoke-static {}, Lspeech/patts/FeatureSet$Builder;->create()Lspeech/patts/FeatureSet$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    invoke-virtual {v0, v1}, Lspeech/patts/FeatureSet$Builder;->mergeFrom(Lspeech/patts/FeatureSet;)Lspeech/patts/FeatureSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    invoke-virtual {v0}, Lspeech/patts/FeatureSet;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 204
    check-cast p1, Lspeech/patts/FeatureSet;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/FeatureSet$Builder;->mergeFrom(Lspeech/patts/FeatureSet;)Lspeech/patts/FeatureSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/FeatureSet;)Lspeech/patts/FeatureSet$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/FeatureSet;

    .prologue
    .line 285
    invoke-static {}, Lspeech/patts/FeatureSet;->getDefaultInstance()Lspeech/patts/FeatureSet;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 310
    :cond_0
    :goto_0
    return-object p0

    .line 286
    :cond_1
    # getter for: Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureSet;->access$300(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 287
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureSet;->access$300(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 288
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FeatureSet;->access$302(Lspeech/patts/FeatureSet;Ljava/util/List;)Ljava/util/List;

    .line 290
    :cond_2
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureSet;->access$300(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FeatureSet;->nf_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureSet;->access$300(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 292
    :cond_3
    # getter for: Lspeech/patts/FeatureSet;->question_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureSet;->access$400(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 293
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->question_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureSet;->access$400(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 294
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FeatureSet;->question_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FeatureSet;->access$402(Lspeech/patts/FeatureSet;Ljava/util/List;)Ljava/util/List;

    .line 296
    :cond_4
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->question_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureSet;->access$400(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FeatureSet;->question_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureSet;->access$400(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 298
    :cond_5
    # getter for: Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureSet;->access$500(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 299
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureSet;->access$500(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 300
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FeatureSet;->access$502(Lspeech/patts/FeatureSet;Ljava/util/List;)Ljava/util/List;

    .line 302
    :cond_6
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureSet;->access$500(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FeatureSet;->custom_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureSet;->access$500(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 304
    :cond_7
    # getter for: Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureSet;->access$600(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 305
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureSet;->access$600(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 306
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FeatureSet;->access$602(Lspeech/patts/FeatureSet;Ljava/util/List;)Ljava/util/List;

    .line 308
    :cond_8
    iget-object v0, p0, Lspeech/patts/FeatureSet$Builder;->result:Lspeech/patts/FeatureSet;

    # getter for: Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureSet;->access$600(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FeatureSet;->acoustic_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureSet;->access$600(Lspeech/patts/FeatureSet;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method
