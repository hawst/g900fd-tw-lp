.class public final enum Lspeech/patts/LmScore$Type;
.super Ljava/lang/Enum;
.source "LmScore.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/LmScore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/LmScore$Type;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/LmScore$Type;

.field public static final enum PRODLM:Lspeech/patts/LmScore$Type;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/LmScore$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lspeech/patts/LmScore$Type;

    const-string v1, "PRODLM"

    invoke-direct {v0, v1, v2, v2, v3}, Lspeech/patts/LmScore$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/LmScore$Type;->PRODLM:Lspeech/patts/LmScore$Type;

    .line 22
    new-array v0, v3, [Lspeech/patts/LmScore$Type;

    sget-object v1, Lspeech/patts/LmScore$Type;->PRODLM:Lspeech/patts/LmScore$Type;

    aput-object v1, v0, v2

    sput-object v0, Lspeech/patts/LmScore$Type;->$VALUES:[Lspeech/patts/LmScore$Type;

    .line 42
    new-instance v0, Lspeech/patts/LmScore$Type$1;

    invoke-direct {v0}, Lspeech/patts/LmScore$Type$1;-><init>()V

    sput-object v0, Lspeech/patts/LmScore$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput p3, p0, Lspeech/patts/LmScore$Type;->index:I

    .line 53
    iput p4, p0, Lspeech/patts/LmScore$Type;->value:I

    .line 54
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/LmScore$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lspeech/patts/LmScore$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/LmScore$Type;

    return-object v0
.end method

.method public static values()[Lspeech/patts/LmScore$Type;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lspeech/patts/LmScore$Type;->$VALUES:[Lspeech/patts/LmScore$Type;

    invoke-virtual {v0}, [Lspeech/patts/LmScore$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/LmScore$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lspeech/patts/LmScore$Type;->value:I

    return v0
.end method
