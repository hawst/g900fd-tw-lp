.class public final enum Lspeech/patts/PhonemeDefinition$Position;
.super Ljava/lang/Enum;
.source "PhonemeDefinition.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PhonemeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Position"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/PhonemeDefinition$Position;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/PhonemeDefinition$Position;

.field public static final enum BACK:Lspeech/patts/PhonemeDefinition$Position;

.field public static final enum CENTRAL:Lspeech/patts/PhonemeDefinition$Position;

.field public static final enum FRONT:Lspeech/patts/PhonemeDefinition$Position;

.field public static final enum NEARBACK:Lspeech/patts/PhonemeDefinition$Position;

.field public static final enum NEARFRONT:Lspeech/patts/PhonemeDefinition$Position;

.field public static final enum UNDEFINED_POSITION:Lspeech/patts/PhonemeDefinition$Position;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/PhonemeDefinition$Position;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 189
    new-instance v0, Lspeech/patts/PhonemeDefinition$Position;

    const-string v1, "UNDEFINED_POSITION"

    invoke-direct {v0, v1, v5, v5, v5}, Lspeech/patts/PhonemeDefinition$Position;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Position;->UNDEFINED_POSITION:Lspeech/patts/PhonemeDefinition$Position;

    .line 190
    new-instance v0, Lspeech/patts/PhonemeDefinition$Position;

    const-string v1, "FRONT"

    invoke-direct {v0, v1, v6, v6, v6}, Lspeech/patts/PhonemeDefinition$Position;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Position;->FRONT:Lspeech/patts/PhonemeDefinition$Position;

    .line 191
    new-instance v0, Lspeech/patts/PhonemeDefinition$Position;

    const-string v1, "NEARFRONT"

    invoke-direct {v0, v1, v7, v7, v7}, Lspeech/patts/PhonemeDefinition$Position;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Position;->NEARFRONT:Lspeech/patts/PhonemeDefinition$Position;

    .line 192
    new-instance v0, Lspeech/patts/PhonemeDefinition$Position;

    const-string v1, "CENTRAL"

    invoke-direct {v0, v1, v8, v8, v8}, Lspeech/patts/PhonemeDefinition$Position;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Position;->CENTRAL:Lspeech/patts/PhonemeDefinition$Position;

    .line 193
    new-instance v0, Lspeech/patts/PhonemeDefinition$Position;

    const-string v1, "NEARBACK"

    invoke-direct {v0, v1, v9, v9, v9}, Lspeech/patts/PhonemeDefinition$Position;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Position;->NEARBACK:Lspeech/patts/PhonemeDefinition$Position;

    .line 194
    new-instance v0, Lspeech/patts/PhonemeDefinition$Position;

    const-string v1, "BACK"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$Position;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Position;->BACK:Lspeech/patts/PhonemeDefinition$Position;

    .line 187
    const/4 v0, 0x6

    new-array v0, v0, [Lspeech/patts/PhonemeDefinition$Position;

    sget-object v1, Lspeech/patts/PhonemeDefinition$Position;->UNDEFINED_POSITION:Lspeech/patts/PhonemeDefinition$Position;

    aput-object v1, v0, v5

    sget-object v1, Lspeech/patts/PhonemeDefinition$Position;->FRONT:Lspeech/patts/PhonemeDefinition$Position;

    aput-object v1, v0, v6

    sget-object v1, Lspeech/patts/PhonemeDefinition$Position;->NEARFRONT:Lspeech/patts/PhonemeDefinition$Position;

    aput-object v1, v0, v7

    sget-object v1, Lspeech/patts/PhonemeDefinition$Position;->CENTRAL:Lspeech/patts/PhonemeDefinition$Position;

    aput-object v1, v0, v8

    sget-object v1, Lspeech/patts/PhonemeDefinition$Position;->NEARBACK:Lspeech/patts/PhonemeDefinition$Position;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lspeech/patts/PhonemeDefinition$Position;->BACK:Lspeech/patts/PhonemeDefinition$Position;

    aput-object v2, v0, v1

    sput-object v0, Lspeech/patts/PhonemeDefinition$Position;->$VALUES:[Lspeech/patts/PhonemeDefinition$Position;

    .line 217
    new-instance v0, Lspeech/patts/PhonemeDefinition$Position$1;

    invoke-direct {v0}, Lspeech/patts/PhonemeDefinition$Position$1;-><init>()V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Position;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 226
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 227
    iput p3, p0, Lspeech/patts/PhonemeDefinition$Position;->index:I

    .line 228
    iput p4, p0, Lspeech/patts/PhonemeDefinition$Position;->value:I

    .line 229
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/PhonemeDefinition$Position;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 187
    const-class v0, Lspeech/patts/PhonemeDefinition$Position;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonemeDefinition$Position;

    return-object v0
.end method

.method public static values()[Lspeech/patts/PhonemeDefinition$Position;
    .locals 1

    .prologue
    .line 187
    sget-object v0, Lspeech/patts/PhonemeDefinition$Position;->$VALUES:[Lspeech/patts/PhonemeDefinition$Position;

    invoke-virtual {v0}, [Lspeech/patts/PhonemeDefinition$Position;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/PhonemeDefinition$Position;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lspeech/patts/PhonemeDefinition$Position;->value:I

    return v0
.end method
