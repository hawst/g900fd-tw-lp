.class public final Lspeech/patts/FeatureData;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "FeatureData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/FeatureData$1;,
        Lspeech/patts/FeatureData$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/FeatureData;


# instance fields
.field private boolFeaturesMemoizedSerializedSize:I

.field private boolFeatures_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private floatFeaturesMemoizedSerializedSize:I

.field private floatFeatures_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private intFeaturesMemoizedSerializedSize:I

.field private intFeatures_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private stringFeatures_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 555
    new-instance v0, Lspeech/patts/FeatureData;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/FeatureData;-><init>(Z)V

    sput-object v0, Lspeech/patts/FeatureData;->defaultInstance:Lspeech/patts/FeatureData;

    .line 556
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 557
    sget-object v0, Lspeech/patts/FeatureData;->defaultInstance:Lspeech/patts/FeatureData;

    invoke-direct {v0}, Lspeech/patts/FeatureData;->initFields()V

    .line 558
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;

    .line 33
    iput v1, p0, Lspeech/patts/FeatureData;->boolFeaturesMemoizedSerializedSize:I

    .line 37
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;

    .line 46
    iput v1, p0, Lspeech/patts/FeatureData;->intFeaturesMemoizedSerializedSize:I

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;

    .line 59
    iput v1, p0, Lspeech/patts/FeatureData;->floatFeaturesMemoizedSerializedSize:I

    .line 63
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;

    .line 108
    iput v1, p0, Lspeech/patts/FeatureData;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/FeatureData;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/FeatureData$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/FeatureData$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/FeatureData;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, -0x1

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;

    .line 33
    iput v1, p0, Lspeech/patts/FeatureData;->boolFeaturesMemoizedSerializedSize:I

    .line 37
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;

    .line 46
    iput v1, p0, Lspeech/patts/FeatureData;->intFeaturesMemoizedSerializedSize:I

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;

    .line 59
    iput v1, p0, Lspeech/patts/FeatureData;->floatFeaturesMemoizedSerializedSize:I

    .line 63
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;

    .line 108
    iput v1, p0, Lspeech/patts/FeatureData;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/FeatureData;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FeatureData;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/FeatureData;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FeatureData;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lspeech/patts/FeatureData;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FeatureData;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lspeech/patts/FeatureData;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FeatureData;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lspeech/patts/FeatureData;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FeatureData;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lspeech/patts/FeatureData;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FeatureData;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lspeech/patts/FeatureData;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/FeatureData;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lspeech/patts/FeatureData;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/FeatureData;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/FeatureData;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/FeatureData;->defaultInstance:Lspeech/patts/FeatureData;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method public static newBuilder()Lspeech/patts/FeatureData$Builder;
    .locals 1

    .prologue
    .line 230
    # invokes: Lspeech/patts/FeatureData$Builder;->create()Lspeech/patts/FeatureData$Builder;
    invoke-static {}, Lspeech/patts/FeatureData$Builder;->access$100()Lspeech/patts/FeatureData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/FeatureData;

    .prologue
    .line 233
    invoke-static {}, Lspeech/patts/FeatureData;->newBuilder()Lspeech/patts/FeatureData$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/FeatureData$Builder;->mergeFrom(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBoolFeaturesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getDefaultInstanceForType()Lspeech/patts/FeatureData;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/FeatureData;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/FeatureData;->defaultInstance:Lspeech/patts/FeatureData;

    return-object v0
.end method

.method public getFloatFeaturesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;

    return-object v0
.end method

.method public getIntFeaturesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 110
    iget v3, p0, Lspeech/patts/FeatureData;->memoizedSerializedSize:I

    .line 111
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 160
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 113
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 115
    const/4 v0, 0x0

    .line 116
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getBoolFeaturesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v0, v5, 0x1

    .line 117
    add-int/2addr v3, v0

    .line 118
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getBoolFeaturesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 119
    add-int/lit8 v3, v3, 0x1

    .line 120
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v3, v5

    .line 123
    :cond_1
    iput v0, p0, Lspeech/patts/FeatureData;->boolFeaturesMemoizedSerializedSize:I

    .line 126
    const/4 v0, 0x0

    .line 127
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getIntFeaturesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 128
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v0, v5

    .line 130
    goto :goto_1

    .line 131
    .end local v1    # "element":I
    :cond_2
    add-int/2addr v3, v0

    .line 132
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getIntFeaturesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 133
    add-int/lit8 v3, v3, 0x1

    .line 134
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v3, v5

    .line 137
    :cond_3
    iput v0, p0, Lspeech/patts/FeatureData;->intFeaturesMemoizedSerializedSize:I

    .line 140
    const/4 v0, 0x0

    .line 141
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getFloatFeaturesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v0, v5, 0x4

    .line 142
    add-int/2addr v3, v0

    .line 143
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getFloatFeaturesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 144
    add-int/lit8 v3, v3, 0x1

    .line 145
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v3, v5

    .line 148
    :cond_4
    iput v0, p0, Lspeech/patts/FeatureData;->floatFeaturesMemoizedSerializedSize:I

    .line 151
    const/4 v0, 0x0

    .line 152
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getStringFeaturesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 153
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 155
    goto :goto_2

    .line 156
    .end local v1    # "element":Ljava/lang/String;
    :cond_5
    add-int/2addr v3, v0

    .line 157
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getStringFeaturesList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 159
    iput v3, p0, Lspeech/patts/FeatureData;->memoizedSerializedSize:I

    move v4, v3

    .line 160
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getStringFeaturesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->toBuilder()Lspeech/patts/FeatureData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/FeatureData$Builder;
    .locals 1

    .prologue
    .line 235
    invoke-static {p0}, Lspeech/patts/FeatureData;->newBuilder(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getSerializedSize()I

    .line 82
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getBoolFeaturesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 83
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 84
    iget v2, p0, Lspeech/patts/FeatureData;->boolFeaturesMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 86
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getBoolFeaturesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 87
    .local v0, "element":Z
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBoolNoTag(Z)V

    goto :goto_0

    .line 89
    .end local v0    # "element":Z
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getIntFeaturesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 90
    const/16 v2, 0x12

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 91
    iget v2, p0, Lspeech/patts/FeatureData;->intFeaturesMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 93
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getIntFeaturesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 94
    .local v0, "element":I
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32NoTag(I)V

    goto :goto_1

    .line 96
    .end local v0    # "element":I
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getFloatFeaturesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 97
    const/16 v2, 0x1a

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 98
    iget v2, p0, Lspeech/patts/FeatureData;->floatFeaturesMemoizedSerializedSize:I

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeRawVarint32(I)V

    .line 100
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getFloatFeaturesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 101
    .local v0, "element":F
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeFloatNoTag(F)V

    goto :goto_2

    .line 103
    .end local v0    # "element":F
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/FeatureData;->getStringFeaturesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 104
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_3

    .line 106
    .end local v0    # "element":Ljava/lang/String;
    :cond_6
    return-void
.end method
