.class public final Lspeech/patts/Model2;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Model2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/Model2$1;,
        Lspeech/patts/Model2$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/Model2;


# instance fields
.field private hasName:Z

.field private hasTarget:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private target_:Lspeech/patts/FeatureData;

.field private unitId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 399
    new-instance v0, Lspeech/patts/Model2;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/Model2;-><init>(Z)V

    sput-object v0, Lspeech/patts/Model2;->defaultInstance:Lspeech/patts/Model2;

    .line 400
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 401
    sget-object v0, Lspeech/patts/Model2;->defaultInstance:Lspeech/patts/Model2;

    invoke-direct {v0}, Lspeech/patts/Model2;->initFields()V

    .line 402
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Model2;->unitId_:Ljava/util/List;

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Model2;->name_:Ljava/lang/String;

    .line 71
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Model2;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/Model2;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/Model2$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/Model2$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/Model2;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Model2;->unitId_:Ljava/util/List;

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/Model2;->name_:Ljava/lang/String;

    .line 71
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/Model2;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$300(Lspeech/patts/Model2;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Model2;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Model2;->unitId_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/Model2;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Model2;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Model2;->unitId_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$402(Lspeech/patts/Model2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Model2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Model2;->hasTarget:Z

    return p1
.end method

.method static synthetic access$500(Lspeech/patts/Model2;)Lspeech/patts/FeatureData;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/Model2;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/Model2;->target_:Lspeech/patts/FeatureData;

    return-object v0
.end method

.method static synthetic access$502(Lspeech/patts/Model2;Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Model2;
    .param p1, "x1"    # Lspeech/patts/FeatureData;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Model2;->target_:Lspeech/patts/FeatureData;

    return-object p1
.end method

.method static synthetic access$602(Lspeech/patts/Model2;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Model2;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/Model2;->hasName:Z

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/Model2;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/Model2;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/Model2;->name_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/Model2;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/Model2;->defaultInstance:Lspeech/patts/Model2;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lspeech/patts/FeatureData;->getDefaultInstance()Lspeech/patts/FeatureData;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/Model2;->target_:Lspeech/patts/FeatureData;

    .line 50
    return-void
.end method

.method public static newBuilder()Lspeech/patts/Model2$Builder;
    .locals 1

    .prologue
    .line 165
    # invokes: Lspeech/patts/Model2$Builder;->create()Lspeech/patts/Model2$Builder;
    invoke-static {}, Lspeech/patts/Model2$Builder;->access$100()Lspeech/patts/Model2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/Model2;)Lspeech/patts/Model2$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/Model2;

    .prologue
    .line 168
    invoke-static {}, Lspeech/patts/Model2;->newBuilder()Lspeech/patts/Model2$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/Model2$Builder;->mergeFrom(Lspeech/patts/Model2;)Lspeech/patts/Model2$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Model2;->getDefaultInstanceForType()Lspeech/patts/Model2;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/Model2;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/Model2;->defaultInstance:Lspeech/patts/Model2;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lspeech/patts/Model2;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 73
    iget v3, p0, Lspeech/patts/Model2;->memoizedSerializedSize:I

    .line 74
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 95
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 76
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 77
    invoke-virtual {p0}, Lspeech/patts/Model2;->hasTarget()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 78
    const/4 v5, 0x1

    invoke-virtual {p0}, Lspeech/patts/Model2;->getTarget()Lspeech/patts/FeatureData;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 82
    :cond_1
    const/4 v0, 0x0

    .line 83
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/Model2;->getUnitIdList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 84
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v0, v5

    .line 86
    goto :goto_1

    .line 87
    .end local v1    # "element":I
    :cond_2
    add-int/2addr v3, v0

    .line 88
    invoke-virtual {p0}, Lspeech/patts/Model2;->getUnitIdList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 90
    invoke-virtual {p0}, Lspeech/patts/Model2;->hasName()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 91
    const/4 v5, 0x3

    invoke-virtual {p0}, Lspeech/patts/Model2;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 94
    :cond_3
    iput v3, p0, Lspeech/patts/Model2;->memoizedSerializedSize:I

    move v4, v3

    .line 95
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto :goto_0
.end method

.method public getTarget()Lspeech/patts/FeatureData;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/Model2;->target_:Lspeech/patts/FeatureData;

    return-object v0
.end method

.method public getUnitIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lspeech/patts/Model2;->unitId_:Ljava/util/List;

    return-object v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lspeech/patts/Model2;->hasName:Z

    return v0
.end method

.method public hasTarget()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/Model2;->hasTarget:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 52
    iget-boolean v1, p0, Lspeech/patts/Model2;->hasTarget:Z

    if-nez v1, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v0

    .line 53
    :cond_1
    iget-boolean v1, p0, Lspeech/patts/Model2;->hasName:Z

    if-eqz v1, :cond_0

    .line 54
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/Model2;->toBuilder()Lspeech/patts/Model2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/Model2$Builder;
    .locals 1

    .prologue
    .line 170
    invoke-static {p0}, Lspeech/patts/Model2;->newBuilder(Lspeech/patts/Model2;)Lspeech/patts/Model2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-virtual {p0}, Lspeech/patts/Model2;->getSerializedSize()I

    .line 60
    invoke-virtual {p0}, Lspeech/patts/Model2;->hasTarget()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/Model2;->getTarget()Lspeech/patts/FeatureData;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 63
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/Model2;->getUnitIdList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 64
    .local v0, "element":I
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    goto :goto_0

    .line 66
    .end local v0    # "element":I
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/Model2;->hasName()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 67
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/Model2;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 69
    :cond_2
    return-void
.end method
