.class public final Lspeech/patts/PhonologyDefinitions$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PhonologyDefinitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PhonologyDefinitions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/PhonologyDefinitions;",
        "Lspeech/patts/PhonologyDefinitions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/PhonologyDefinitions;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1312
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1700()Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 1

    .prologue
    .line 1306
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$Builder;->create()Lspeech/patts/PhonologyDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 3

    .prologue
    .line 1315
    new-instance v0, Lspeech/patts/PhonologyDefinitions$Builder;

    invoke-direct {v0}, Lspeech/patts/PhonologyDefinitions$Builder;-><init>()V

    .line 1316
    .local v0, "builder":Lspeech/patts/PhonologyDefinitions$Builder;
    new-instance v1, Lspeech/patts/PhonologyDefinitions;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/PhonologyDefinitions;-><init>(Lspeech/patts/PhonologyDefinitions$1;)V

    iput-object v1, v0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    .line 1317
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1306
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$Builder;->build()Lspeech/patts/PhonologyDefinitions;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/PhonologyDefinitions;
    .locals 1

    .prologue
    .line 1345
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1346
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1348
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$Builder;->buildPartial()Lspeech/patts/PhonologyDefinitions;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/PhonologyDefinitions;
    .locals 3

    .prologue
    .line 1361
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    if-nez v1, :cond_0

    .line 1362
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1365
    :cond_0
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$1900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 1366
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$1900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$1902(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1369
    :cond_1
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$2000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 1370
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$2000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$2002(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1373
    :cond_2
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$2100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 1374
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$2100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$2102(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1377
    :cond_3
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$2200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 1378
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$2200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$2202(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1381
    :cond_4
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$2300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_5

    .line 1382
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$2300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$2302(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1385
    :cond_5
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$2400(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_6

    .line 1386
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$2400(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$2402(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1389
    :cond_6
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$2500(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_7

    .line 1390
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$2500(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$2502(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1393
    :cond_7
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$2600(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_8

    .line 1394
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$2600(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$2602(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1397
    :cond_8
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$2700(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_9

    .line 1398
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$2700(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$2702(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1401
    :cond_9
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$2800(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_a

    .line 1402
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$2800(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$2802(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1405
    :cond_a
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$2900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_b

    .line 1406
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$2900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$2902(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1409
    :cond_b
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$3000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_c

    .line 1410
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$3000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$3002(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1413
    :cond_c
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$3100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_d

    .line 1414
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$3100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$3102(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1417
    :cond_d
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$3200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_e

    .line 1418
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$3200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$3202(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1421
    :cond_e
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/PhonologyDefinitions;->access$3300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_f

    .line 1422
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    iget-object v2, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/PhonologyDefinitions;->access$3300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/PhonologyDefinitions;->access$3302(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1425
    :cond_f
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    .line 1426
    .local v0, "returnMe":Lspeech/patts/PhonologyDefinitions;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    .line 1427
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1306
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$Builder;->clone()Lspeech/patts/PhonologyDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1306
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$Builder;->clone()Lspeech/patts/PhonologyDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1306
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$Builder;->clone()Lspeech/patts/PhonologyDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2

    .prologue
    .line 1334
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$Builder;->create()Lspeech/patts/PhonologyDefinitions$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    invoke-virtual {v0, v1}, Lspeech/patts/PhonologyDefinitions$Builder;->mergeFrom(Lspeech/patts/PhonologyDefinitions;)Lspeech/patts/PhonologyDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 1342
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    invoke-virtual {v0}, Lspeech/patts/PhonologyDefinitions;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 1306
    check-cast p1, Lspeech/patts/PhonologyDefinitions;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/PhonologyDefinitions$Builder;->mergeFrom(Lspeech/patts/PhonologyDefinitions;)Lspeech/patts/PhonologyDefinitions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/PhonologyDefinitions;)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/PhonologyDefinitions;

    .prologue
    .line 1431
    invoke-static {}, Lspeech/patts/PhonologyDefinitions;->getDefaultInstance()Lspeech/patts/PhonologyDefinitions;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1561
    :cond_0
    :goto_0
    return-object p0

    .line 1432
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasLanguage()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1433
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setLanguage(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1435
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasRegion()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1436
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getRegion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setRegion(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1438
    :cond_3
    # getter for: Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$1900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1439
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$1900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1440
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$1902(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1442
    :cond_4
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$1900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->phonemeDef_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$1900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1444
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasWordidDefinition()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1445
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getWordidDefinition()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setWordidDefinition(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1447
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasSpellingDefinition()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1448
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getSpellingDefinition()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setSpellingDefinition(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1450
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasLegalizingVowel()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1451
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getLegalizingVowel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setLegalizingVowel(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1453
    :cond_8
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasMaxOnsetOrder()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1454
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getMaxOnsetOrder()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setMaxOnsetOrder(I)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1456
    :cond_9
    # getter for: Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1457
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1458
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$2002(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1460
    :cond_a
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->onset_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1462
    :cond_b
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasMaxCodaOrder()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1463
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getMaxCodaOrder()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setMaxCodaOrder(I)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1465
    :cond_c
    # getter for: Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 1466
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1467
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$2102(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1469
    :cond_d
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->coda_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1471
    :cond_e
    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 1472
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1473
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$2202(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1475
    :cond_f
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignOnset_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1477
    :cond_10
    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 1478
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1479
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$2302(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1481
    :cond_11
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignCoda_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1483
    :cond_12
    # getter for: Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2400(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_14

    .line 1484
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2400(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1485
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$2402(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1487
    :cond_13
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2400(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->bannedRhyme_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2400(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1489
    :cond_14
    # getter for: Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2500(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_16

    .line 1490
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2500(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1491
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$2502(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1493
    :cond_15
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2500(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->permittedRhyme_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2500(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1495
    :cond_16
    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2600(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_18

    .line 1496
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2600(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1497
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$2602(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1499
    :cond_17
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2600(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->foreignPhonemeDef_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2600(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1501
    :cond_18
    # getter for: Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2700(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 1502
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2700(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1503
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$2702(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1505
    :cond_19
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2700(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->stressDef_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2700(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1507
    :cond_1a
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasOrthographicStressLetters()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1508
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getOrthographicStressLetters()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setOrthographicStressLetters(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1510
    :cond_1b
    # getter for: Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2800(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1d

    .line 1511
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2800(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1512
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$2802(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1514
    :cond_1c
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2800(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->primaryStressTriggers_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2800(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1516
    :cond_1d
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasSinglePrimaryStressRequired()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1517
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getSinglePrimaryStressRequired()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setSinglePrimaryStressRequired(Z)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1519
    :cond_1e
    # getter for: Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_20

    .line 1520
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1521
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$2902(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1523
    :cond_1f
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$2900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->primaryStress_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$2900(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1525
    :cond_20
    # getter for: Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$3000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_22

    .line 1526
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$3000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 1527
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$3002(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1529
    :cond_21
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$3000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->legalGeminate_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$3000(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1531
    :cond_22
    # getter for: Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$3100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_24

    .line 1532
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$3100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 1533
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$3102(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1535
    :cond_23
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$3100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->phonemeFallbackMap_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$3100(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1537
    :cond_24
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasCompoundStressPosition()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 1538
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getCompoundStressPosition()Lspeech/patts/PhonologyDefinitions$StressPosition;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setCompoundStressPosition(Lspeech/patts/PhonologyDefinitions$StressPosition;)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1540
    :cond_25
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasCompoundMergeSameConsonants()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 1541
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getCompoundMergeSameConsonants()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setCompoundMergeSameConsonants(Z)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1543
    :cond_26
    # getter for: Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$3200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_28

    .line 1544
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$3200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 1545
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$3202(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1547
    :cond_27
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$3200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->compoundMergeRules_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$3200(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1549
    :cond_28
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasSpellingStressMark()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 1550
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getSpellingStressMark()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setSpellingStressMark(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1552
    :cond_29
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->hasVowelLetters()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 1553
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions;->getVowelLetters()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$Builder;->setVowelLetters(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;

    .line 1555
    :cond_2a
    # getter for: Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$3300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1556
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$3300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 1557
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$3302(Lspeech/patts/PhonologyDefinitions;Ljava/util/List;)Ljava/util/List;

    .line 1559
    :cond_2b
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # getter for: Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions;->access$3300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/PhonologyDefinitions;->vowelPhonemeReduction_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/PhonologyDefinitions;->access$3300(Lspeech/patts/PhonologyDefinitions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setCompoundMergeSameConsonants(Z)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2579
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasCompoundMergeSameConsonants:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$5402(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 2580
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->compoundMergeSameConsonants_:Z
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$5502(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 2581
    return-object p0
.end method

.method public setCompoundStressPosition(Lspeech/patts/PhonologyDefinitions$StressPosition;)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/PhonologyDefinitions$StressPosition;

    .prologue
    .line 2558
    if-nez p1, :cond_0

    .line 2559
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2561
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasCompoundStressPosition:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$5202(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 2562
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->compoundStressPosition_:Lspeech/patts/PhonologyDefinitions$StressPosition;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$5302(Lspeech/patts/PhonologyDefinitions;Lspeech/patts/PhonologyDefinitions$StressPosition;)Lspeech/patts/PhonologyDefinitions$StressPosition;

    .line 2563
    return-object p0
.end method

.method public setLanguage(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1743
    if-nez p1, :cond_0

    .line 1744
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1746
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasLanguage:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$3402(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 1747
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->language_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$3502(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;

    .line 1748
    return-object p0
.end method

.method public setLegalizingVowel(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1878
    if-nez p1, :cond_0

    .line 1879
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1881
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasLegalizingVowel:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$4202(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 1882
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->legalizingVowel_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$4302(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;

    .line 1883
    return-object p0
.end method

.method public setMaxCodaOrder(I)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1968
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasMaxCodaOrder:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$4602(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 1969
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->maxCodaOrder_:I
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$4702(Lspeech/patts/PhonologyDefinitions;I)I

    .line 1970
    return-object p0
.end method

.method public setMaxOnsetOrder(I)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1899
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasMaxOnsetOrder:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$4402(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 1900
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->maxOnsetOrder_:I
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$4502(Lspeech/patts/PhonologyDefinitions;I)I

    .line 1901
    return-object p0
.end method

.method public setOrthographicStressLetters(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2343
    if-nez p1, :cond_0

    .line 2344
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2346
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasOrthographicStressLetters:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$4802(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 2347
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->orthographicStressLetters_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$4902(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;

    .line 2348
    return-object p0
.end method

.method public setRegion(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1764
    if-nez p1, :cond_0

    .line 1765
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1767
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasRegion:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$3602(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 1768
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->region_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$3702(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;

    .line 1769
    return-object p0
.end method

.method public setSinglePrimaryStressRequired(Z)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2415
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasSinglePrimaryStressRequired:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$5002(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 2416
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->singlePrimaryStressRequired_:Z
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$5102(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 2417
    return-object p0
.end method

.method public setSpellingDefinition(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1857
    if-nez p1, :cond_0

    .line 1858
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1860
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasSpellingDefinition:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$4002(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 1861
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->spellingDefinition_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$4102(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;

    .line 1862
    return-object p0
.end method

.method public setSpellingStressMark(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2648
    if-nez p1, :cond_0

    .line 2649
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2651
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasSpellingStressMark:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$5602(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 2652
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->spellingStressMark_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$5702(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;

    .line 2653
    return-object p0
.end method

.method public setVowelLetters(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2669
    if-nez p1, :cond_0

    .line 2670
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2672
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasVowelLetters:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$5802(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 2673
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->vowelLetters_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$5902(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;

    .line 2674
    return-object p0
.end method

.method public setWordidDefinition(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1836
    if-nez p1, :cond_0

    .line 1837
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1839
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions;->hasWordidDefinition:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions;->access$3802(Lspeech/patts/PhonologyDefinitions;Z)Z

    .line 1840
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$Builder;->result:Lspeech/patts/PhonologyDefinitions;

    # setter for: Lspeech/patts/PhonologyDefinitions;->wordidDefinition_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions;->access$3902(Lspeech/patts/PhonologyDefinitions;Ljava/lang/String;)Ljava/lang/String;

    .line 1841
    return-object p0
.end method
