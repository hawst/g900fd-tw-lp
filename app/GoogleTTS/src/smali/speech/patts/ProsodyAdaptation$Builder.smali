.class public final Lspeech/patts/ProsodyAdaptation$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ProsodyAdaptation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/ProsodyAdaptation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/ProsodyAdaptation;",
        "Lspeech/patts/ProsodyAdaptation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/ProsodyAdaptation;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/ProsodyAdaptation$Builder;
    .locals 1

    .prologue
    .line 269
    invoke-static {}, Lspeech/patts/ProsodyAdaptation$Builder;->create()Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/ProsodyAdaptation$Builder;
    .locals 3

    .prologue
    .line 278
    new-instance v0, Lspeech/patts/ProsodyAdaptation$Builder;

    invoke-direct {v0}, Lspeech/patts/ProsodyAdaptation$Builder;-><init>()V

    .line 279
    .local v0, "builder":Lspeech/patts/ProsodyAdaptation$Builder;
    new-instance v1, Lspeech/patts/ProsodyAdaptation;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/ProsodyAdaptation;-><init>(Lspeech/patts/ProsodyAdaptation$1;)V

    iput-object v1, v0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    .line 280
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 269
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation$Builder;->build()Lspeech/patts/ProsodyAdaptation;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/ProsodyAdaptation;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 309
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    invoke-static {v0}, Lspeech/patts/ProsodyAdaptation$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 311
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation$Builder;->buildPartial()Lspeech/patts/ProsodyAdaptation;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/ProsodyAdaptation;
    .locals 3

    .prologue
    .line 324
    iget-object v1, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    if-nez v1, :cond_0

    .line 325
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 328
    :cond_0
    iget-object v1, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/ProsodyAdaptation;->access$300(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 329
    iget-object v1, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    iget-object v2, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/ProsodyAdaptation;->access$300(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/ProsodyAdaptation;->access$302(Lspeech/patts/ProsodyAdaptation;Ljava/util/List;)Ljava/util/List;

    .line 332
    :cond_1
    iget-object v1, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/ProsodyAdaptation;->access$400(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 333
    iget-object v1, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    iget-object v2, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/ProsodyAdaptation;->access$400(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/ProsodyAdaptation;->access$402(Lspeech/patts/ProsodyAdaptation;Ljava/util/List;)Ljava/util/List;

    .line 336
    :cond_2
    iget-object v1, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/ProsodyAdaptation;->access$500(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 337
    iget-object v1, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    iget-object v2, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/ProsodyAdaptation;->access$500(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/ProsodyAdaptation;->access$502(Lspeech/patts/ProsodyAdaptation;Ljava/util/List;)Ljava/util/List;

    .line 340
    :cond_3
    iget-object v1, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/ProsodyAdaptation;->access$600(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 341
    iget-object v1, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    iget-object v2, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/ProsodyAdaptation;->access$600(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/ProsodyAdaptation;->access$602(Lspeech/patts/ProsodyAdaptation;Ljava/util/List;)Ljava/util/List;

    .line 344
    :cond_4
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    .line 345
    .local v0, "returnMe":Lspeech/patts/ProsodyAdaptation;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    .line 346
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 269
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation$Builder;->clone()Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 269
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation$Builder;->clone()Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 269
    invoke-virtual {p0}, Lspeech/patts/ProsodyAdaptation$Builder;->clone()Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/ProsodyAdaptation$Builder;
    .locals 2

    .prologue
    .line 297
    invoke-static {}, Lspeech/patts/ProsodyAdaptation$Builder;->create()Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    invoke-virtual {v0, v1}, Lspeech/patts/ProsodyAdaptation$Builder;->mergeFrom(Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    invoke-virtual {v0}, Lspeech/patts/ProsodyAdaptation;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 269
    check-cast p1, Lspeech/patts/ProsodyAdaptation;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/ProsodyAdaptation$Builder;->mergeFrom(Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/ProsodyAdaptation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/ProsodyAdaptation;)Lspeech/patts/ProsodyAdaptation$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/ProsodyAdaptation;

    .prologue
    .line 350
    invoke-static {}, Lspeech/patts/ProsodyAdaptation;->getDefaultInstance()Lspeech/patts/ProsodyAdaptation;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 381
    :cond_0
    :goto_0
    return-object p0

    .line 351
    :cond_1
    # getter for: Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProsodyAdaptation;->access$300(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 352
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProsodyAdaptation;->access$300(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 353
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/ProsodyAdaptation;->access$302(Lspeech/patts/ProsodyAdaptation;Ljava/util/List;)Ljava/util/List;

    .line 355
    :cond_2
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProsodyAdaptation;->access$300(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/ProsodyAdaptation;->targetF0Mean_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProsodyAdaptation;->access$300(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 357
    :cond_3
    # getter for: Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProsodyAdaptation;->access$400(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 358
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProsodyAdaptation;->access$400(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 359
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/ProsodyAdaptation;->access$402(Lspeech/patts/ProsodyAdaptation;Ljava/util/List;)Ljava/util/List;

    .line 361
    :cond_4
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProsodyAdaptation;->access$400(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/ProsodyAdaptation;->targetF0Std_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProsodyAdaptation;->access$400(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 363
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/ProsodyAdaptation;->hasSpeakingRateSource()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 364
    invoke-virtual {p1}, Lspeech/patts/ProsodyAdaptation;->getSpeakingRateSource()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lspeech/patts/ProsodyAdaptation$Builder;->setSpeakingRateSource(D)Lspeech/patts/ProsodyAdaptation$Builder;

    .line 366
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/ProsodyAdaptation;->hasSpeakingRateTarget()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 367
    invoke-virtual {p1}, Lspeech/patts/ProsodyAdaptation;->getSpeakingRateTarget()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lspeech/patts/ProsodyAdaptation$Builder;->setSpeakingRateTarget(D)Lspeech/patts/ProsodyAdaptation$Builder;

    .line 369
    :cond_7
    # getter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProsodyAdaptation;->access$500(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 370
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProsodyAdaptation;->access$500(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 371
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/ProsodyAdaptation;->access$502(Lspeech/patts/ProsodyAdaptation;Ljava/util/List;)Ljava/util/List;

    .line 373
    :cond_8
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProsodyAdaptation;->access$500(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Mean_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProsodyAdaptation;->access$500(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 375
    :cond_9
    # getter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProsodyAdaptation;->access$600(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 376
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProsodyAdaptation;->access$600(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 377
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/ProsodyAdaptation;->access$602(Lspeech/patts/ProsodyAdaptation;Ljava/util/List;)Ljava/util/List;

    .line 379
    :cond_a
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # getter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/ProsodyAdaptation;->access$600(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/ProsodyAdaptation;->sourceF0Std_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/ProsodyAdaptation;->access$600(Lspeech/patts/ProsodyAdaptation;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public setSpeakingRateSource(D)Lspeech/patts/ProsodyAdaptation$Builder;
    .locals 3
    .param p1, "value"    # D

    .prologue
    .line 540
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/ProsodyAdaptation;->hasSpeakingRateSource:Z
    invoke-static {v0, v1}, Lspeech/patts/ProsodyAdaptation;->access$702(Lspeech/patts/ProsodyAdaptation;Z)Z

    .line 541
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # setter for: Lspeech/patts/ProsodyAdaptation;->speakingRateSource_:D
    invoke-static {v0, p1, p2}, Lspeech/patts/ProsodyAdaptation;->access$802(Lspeech/patts/ProsodyAdaptation;D)D

    .line 542
    return-object p0
.end method

.method public setSpeakingRateTarget(D)Lspeech/patts/ProsodyAdaptation$Builder;
    .locals 3
    .param p1, "value"    # D

    .prologue
    .line 558
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/ProsodyAdaptation;->hasSpeakingRateTarget:Z
    invoke-static {v0, v1}, Lspeech/patts/ProsodyAdaptation;->access$902(Lspeech/patts/ProsodyAdaptation;Z)Z

    .line 559
    iget-object v0, p0, Lspeech/patts/ProsodyAdaptation$Builder;->result:Lspeech/patts/ProsodyAdaptation;

    # setter for: Lspeech/patts/ProsodyAdaptation;->speakingRateTarget_:D
    invoke-static {v0, p1, p2}, Lspeech/patts/ProsodyAdaptation;->access$1002(Lspeech/patts/ProsodyAdaptation;D)D

    .line 560
    return-object p0
.end method
