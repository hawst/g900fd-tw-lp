.class public final Lspeech/patts/HomographContext;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "HomographContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/HomographContext$1;,
        Lspeech/patts/HomographContext$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/HomographContext;


# instance fields
.field private fullMatchOnly_:Z

.field private hasFullMatchOnly:Z

.field private hasIndex:Z

.field private hasPattern:Z

.field private index_:I

.field private memoizedSerializedSize:I

.field private pattern_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 331
    new-instance v0, Lspeech/patts/HomographContext;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/HomographContext;-><init>(Z)V

    sput-object v0, Lspeech/patts/HomographContext;->defaultInstance:Lspeech/patts/HomographContext;

    .line 332
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 333
    sget-object v0, Lspeech/patts/HomographContext;->defaultInstance:Lspeech/patts/HomographContext;

    invoke-direct {v0}, Lspeech/patts/HomographContext;->initFields()V

    .line 334
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/HomographContext;->pattern_:Ljava/lang/String;

    .line 32
    iput v1, p0, Lspeech/patts/HomographContext;->index_:I

    .line 39
    iput-boolean v1, p0, Lspeech/patts/HomographContext;->fullMatchOnly_:Z

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/HomographContext;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/HomographContext;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/HomographContext$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/HomographContext$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/HomographContext;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/HomographContext;->pattern_:Ljava/lang/String;

    .line 32
    iput v1, p0, Lspeech/patts/HomographContext;->index_:I

    .line 39
    iput-boolean v1, p0, Lspeech/patts/HomographContext;->fullMatchOnly_:Z

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/HomographContext;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$302(Lspeech/patts/HomographContext;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographContext;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographContext;->hasPattern:Z

    return p1
.end method

.method static synthetic access$402(Lspeech/patts/HomographContext;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographContext;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographContext;->pattern_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lspeech/patts/HomographContext;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographContext;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographContext;->hasIndex:Z

    return p1
.end method

.method static synthetic access$602(Lspeech/patts/HomographContext;I)I
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographContext;
    .param p1, "x1"    # I

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/HomographContext;->index_:I

    return p1
.end method

.method static synthetic access$702(Lspeech/patts/HomographContext;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographContext;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographContext;->hasFullMatchOnly:Z

    return p1
.end method

.method static synthetic access$802(Lspeech/patts/HomographContext;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographContext;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographContext;->fullMatchOnly_:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/HomographContext;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/HomographContext;->defaultInstance:Lspeech/patts/HomographContext;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public static newBuilder()Lspeech/patts/HomographContext$Builder;
    .locals 1

    .prologue
    .line 153
    # invokes: Lspeech/patts/HomographContext$Builder;->create()Lspeech/patts/HomographContext$Builder;
    invoke-static {}, Lspeech/patts/HomographContext$Builder;->access$100()Lspeech/patts/HomographContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/HomographContext;)Lspeech/patts/HomographContext$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/HomographContext;

    .prologue
    .line 156
    invoke-static {}, Lspeech/patts/HomographContext;->newBuilder()Lspeech/patts/HomographContext$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/HomographContext$Builder;->mergeFrom(Lspeech/patts/HomographContext;)Lspeech/patts/HomographContext$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/HomographContext;->getDefaultInstanceForType()Lspeech/patts/HomographContext;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/HomographContext;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/HomographContext;->defaultInstance:Lspeech/patts/HomographContext;

    return-object v0
.end method

.method public getFullMatchOnly()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lspeech/patts/HomographContext;->fullMatchOnly_:Z

    return v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lspeech/patts/HomographContext;->index_:I

    return v0
.end method

.method public getPattern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/HomographContext;->pattern_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 66
    iget v0, p0, Lspeech/patts/HomographContext;->memoizedSerializedSize:I

    .line 67
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 83
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 69
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 70
    invoke-virtual {p0}, Lspeech/patts/HomographContext;->hasPattern()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/HomographContext;->getPattern()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 74
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/HomographContext;->hasIndex()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 75
    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/patts/HomographContext;->getIndex()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 78
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/HomographContext;->hasFullMatchOnly()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 79
    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/patts/HomographContext;->getFullMatchOnly()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 82
    :cond_3
    iput v0, p0, Lspeech/patts/HomographContext;->memoizedSerializedSize:I

    move v1, v0

    .line 83
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasFullMatchOnly()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lspeech/patts/HomographContext;->hasFullMatchOnly:Z

    return v0
.end method

.method public hasIndex()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/HomographContext;->hasIndex:Z

    return v0
.end method

.method public hasPattern()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/HomographContext;->hasPattern:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lspeech/patts/HomographContext;->hasPattern:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 47
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/HomographContext;->toBuilder()Lspeech/patts/HomographContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/HomographContext$Builder;
    .locals 1

    .prologue
    .line 158
    invoke-static {p0}, Lspeech/patts/HomographContext;->newBuilder(Lspeech/patts/HomographContext;)Lspeech/patts/HomographContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0}, Lspeech/patts/HomographContext;->getSerializedSize()I

    .line 53
    invoke-virtual {p0}, Lspeech/patts/HomographContext;->hasPattern()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const/4 v0, 0x1

    invoke-virtual {p0}, Lspeech/patts/HomographContext;->getPattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 56
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/HomographContext;->hasIndex()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    const/4 v0, 0x2

    invoke-virtual {p0}, Lspeech/patts/HomographContext;->getIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 59
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/HomographContext;->hasFullMatchOnly()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    const/4 v0, 0x3

    invoke-virtual {p0}, Lspeech/patts/HomographContext;->getFullMatchOnly()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 62
    :cond_2
    return-void
.end method
