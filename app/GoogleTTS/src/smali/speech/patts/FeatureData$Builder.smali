.class public final Lspeech/patts/FeatureData$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FeatureData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/FeatureData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/FeatureData;",
        "Lspeech/patts/FeatureData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/FeatureData;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/FeatureData$Builder;
    .locals 1

    .prologue
    .line 237
    invoke-static {}, Lspeech/patts/FeatureData$Builder;->create()Lspeech/patts/FeatureData$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/FeatureData$Builder;
    .locals 3

    .prologue
    .line 246
    new-instance v0, Lspeech/patts/FeatureData$Builder;

    invoke-direct {v0}, Lspeech/patts/FeatureData$Builder;-><init>()V

    .line 247
    .local v0, "builder":Lspeech/patts/FeatureData$Builder;
    new-instance v1, Lspeech/patts/FeatureData;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/FeatureData;-><init>(Lspeech/patts/FeatureData$1;)V

    iput-object v1, v0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    .line 248
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Lspeech/patts/FeatureData$Builder;->build()Lspeech/patts/FeatureData;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/FeatureData;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/FeatureData$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    invoke-static {v0}, Lspeech/patts/FeatureData$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 279
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FeatureData$Builder;->buildPartial()Lspeech/patts/FeatureData;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/FeatureData;
    .locals 3

    .prologue
    .line 292
    iget-object v1, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    if-nez v1, :cond_0

    .line 293
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 296
    :cond_0
    iget-object v1, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FeatureData;->access$300(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 297
    iget-object v1, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    iget-object v2, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FeatureData;->access$300(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FeatureData;->access$302(Lspeech/patts/FeatureData;Ljava/util/List;)Ljava/util/List;

    .line 300
    :cond_1
    iget-object v1, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FeatureData;->access$400(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 301
    iget-object v1, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    iget-object v2, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FeatureData;->access$400(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FeatureData;->access$402(Lspeech/patts/FeatureData;Ljava/util/List;)Ljava/util/List;

    .line 304
    :cond_2
    iget-object v1, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FeatureData;->access$500(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 305
    iget-object v1, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    iget-object v2, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FeatureData;->access$500(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FeatureData;->access$502(Lspeech/patts/FeatureData;Ljava/util/List;)Ljava/util/List;

    .line 308
    :cond_3
    iget-object v1, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/FeatureData;->access$600(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 309
    iget-object v1, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    iget-object v2, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/FeatureData;->access$600(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/FeatureData;->access$602(Lspeech/patts/FeatureData;Ljava/util/List;)Ljava/util/List;

    .line 312
    :cond_4
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    .line 313
    .local v0, "returnMe":Lspeech/patts/FeatureData;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    .line 314
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Lspeech/patts/FeatureData$Builder;->clone()Lspeech/patts/FeatureData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Lspeech/patts/FeatureData$Builder;->clone()Lspeech/patts/FeatureData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 237
    invoke-virtual {p0}, Lspeech/patts/FeatureData$Builder;->clone()Lspeech/patts/FeatureData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/FeatureData$Builder;
    .locals 2

    .prologue
    .line 265
    invoke-static {}, Lspeech/patts/FeatureData$Builder;->create()Lspeech/patts/FeatureData$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    invoke-virtual {v0, v1}, Lspeech/patts/FeatureData$Builder;->mergeFrom(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    invoke-virtual {v0}, Lspeech/patts/FeatureData;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 237
    check-cast p1, Lspeech/patts/FeatureData;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/FeatureData$Builder;->mergeFrom(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/FeatureData;)Lspeech/patts/FeatureData$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/FeatureData;

    .prologue
    .line 318
    invoke-static {}, Lspeech/patts/FeatureData;->getDefaultInstance()Lspeech/patts/FeatureData;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-object p0

    .line 319
    :cond_1
    # getter for: Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureData;->access$300(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 320
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureData;->access$300(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 321
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FeatureData;->access$302(Lspeech/patts/FeatureData;Ljava/util/List;)Ljava/util/List;

    .line 323
    :cond_2
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureData;->access$300(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FeatureData;->boolFeatures_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureData;->access$300(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 325
    :cond_3
    # getter for: Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureData;->access$400(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 326
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureData;->access$400(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 327
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FeatureData;->access$402(Lspeech/patts/FeatureData;Ljava/util/List;)Ljava/util/List;

    .line 329
    :cond_4
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureData;->access$400(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FeatureData;->intFeatures_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureData;->access$400(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 331
    :cond_5
    # getter for: Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureData;->access$500(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 332
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureData;->access$500(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 333
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FeatureData;->access$502(Lspeech/patts/FeatureData;Ljava/util/List;)Ljava/util/List;

    .line 335
    :cond_6
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureData;->access$500(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FeatureData;->floatFeatures_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureData;->access$500(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 337
    :cond_7
    # getter for: Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureData;->access$600(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureData;->access$600(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 339
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/FeatureData;->access$602(Lspeech/patts/FeatureData;Ljava/util/List;)Ljava/util/List;

    .line 341
    :cond_8
    iget-object v0, p0, Lspeech/patts/FeatureData$Builder;->result:Lspeech/patts/FeatureData;

    # getter for: Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/FeatureData;->access$600(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/FeatureData;->stringFeatures_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/FeatureData;->access$600(Lspeech/patts/FeatureData;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method
