.class public final Lspeech/patts/HmmDiagsProto$HmmModel;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "HmmDiagsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/HmmDiagsProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HmmModel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/HmmDiagsProto$HmmModel$Builder;,
        Lspeech/patts/HmmDiagsProto$HmmModel$State;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/HmmDiagsProto$HmmModel;


# instance fields
.field private hasName:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private state_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/HmmDiagsProto$HmmModel$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 646
    new-instance v0, Lspeech/patts/HmmDiagsProto$HmmModel;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/HmmDiagsProto$HmmModel;-><init>(Z)V

    sput-object v0, Lspeech/patts/HmmDiagsProto$HmmModel;->defaultInstance:Lspeech/patts/HmmDiagsProto$HmmModel;

    .line 647
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 648
    sget-object v0, Lspeech/patts/HmmDiagsProto$HmmModel;->defaultInstance:Lspeech/patts/HmmDiagsProto$HmmModel;

    invoke-direct {v0}, Lspeech/patts/HmmDiagsProto$HmmModel;->initFields()V

    .line 649
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 333
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->name_:Ljava/lang/String;

    .line 339
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;

    .line 366
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->memoizedSerializedSize:I

    .line 26
    invoke-direct {p0}, Lspeech/patts/HmmDiagsProto$HmmModel;->initFields()V

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/HmmDiagsProto$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/HmmDiagsProto$1;

    .prologue
    .line 22
    invoke-direct {p0}, Lspeech/patts/HmmDiagsProto$HmmModel;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 333
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->name_:Ljava/lang/String;

    .line 339
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;

    .line 366
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->memoizedSerializedSize:I

    .line 28
    return-void
.end method

.method static synthetic access$1000(Lspeech/patts/HmmDiagsProto$HmmModel;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HmmDiagsProto$HmmModel;

    .prologue
    .line 22
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1002(Lspeech/patts/HmmDiagsProto$HmmModel;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HmmDiagsProto$HmmModel;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 22
    iput-object p1, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1102(Lspeech/patts/HmmDiagsProto$HmmModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HmmDiagsProto$HmmModel;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->hasName:Z

    return p1
.end method

.method static synthetic access$1202(Lspeech/patts/HmmDiagsProto$HmmModel;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HmmDiagsProto$HmmModel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->name_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/HmmDiagsProto$HmmModel;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lspeech/patts/HmmDiagsProto$HmmModel;->defaultInstance:Lspeech/patts/HmmDiagsProto$HmmModel;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 350
    return-void
.end method

.method public static newBuilder()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;
    .locals 1

    .prologue
    .line 451
    # invokes: Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->create()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;
    invoke-static {}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->access$800()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/HmmDiagsProto$HmmModel;)Lspeech/patts/HmmDiagsProto$HmmModel$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/HmmDiagsProto$HmmModel;

    .prologue
    .line 454
    invoke-static {}, Lspeech/patts/HmmDiagsProto$HmmModel;->newBuilder()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/HmmDiagsProto$HmmModel$Builder;->mergeFrom(Lspeech/patts/HmmDiagsProto$HmmModel;)Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel;->getDefaultInstanceForType()Lspeech/patts/HmmDiagsProto$HmmModel;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/HmmDiagsProto$HmmModel;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lspeech/patts/HmmDiagsProto$HmmModel;->defaultInstance:Lspeech/patts/HmmDiagsProto$HmmModel;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 368
    iget v2, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->memoizedSerializedSize:I

    .line 369
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 381
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 371
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 372
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel;->hasName()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 373
    const/4 v4, 0x1

    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 376
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel;->getStateList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/HmmDiagsProto$HmmModel$State;

    .line 377
    .local v0, "element":Lspeech/patts/HmmDiagsProto$HmmModel$State;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 379
    goto :goto_1

    .line 380
    .end local v0    # "element":Lspeech/patts/HmmDiagsProto$HmmModel$State;
    :cond_2
    iput v2, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->memoizedSerializedSize:I

    move v3, v2

    .line 381
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getStateList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/HmmDiagsProto$HmmModel$State;",
            ">;"
        }
    .end annotation

    .prologue
    .line 342
    iget-object v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->state_:Ljava/util/List;

    return-object v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 334
    iget-boolean v0, p0, Lspeech/patts/HmmDiagsProto$HmmModel;->hasName:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel;->toBuilder()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/HmmDiagsProto$HmmModel$Builder;
    .locals 1

    .prologue
    .line 456
    invoke-static {p0}, Lspeech/patts/HmmDiagsProto$HmmModel;->newBuilder(Lspeech/patts/HmmDiagsProto$HmmModel;)Lspeech/patts/HmmDiagsProto$HmmModel$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 357
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel;->getSerializedSize()I

    .line 358
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel;->hasName()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 359
    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 361
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/HmmDiagsProto$HmmModel;->getStateList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/HmmDiagsProto$HmmModel$State;

    .line 362
    .local v0, "element":Lspeech/patts/HmmDiagsProto$HmmModel$State;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 364
    .end local v0    # "element":Lspeech/patts/HmmDiagsProto$HmmModel$State;
    :cond_1
    return-void
.end method
