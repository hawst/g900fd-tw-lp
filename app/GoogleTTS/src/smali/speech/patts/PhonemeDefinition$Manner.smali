.class public final enum Lspeech/patts/PhonemeDefinition$Manner;
.super Ljava/lang/Enum;
.source "PhonemeDefinition.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PhonemeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Manner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lspeech/patts/PhonemeDefinition$Manner;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lspeech/patts/PhonemeDefinition$Manner;

.field public static final enum AFFRICATE:Lspeech/patts/PhonemeDefinition$Manner;

.field public static final enum APPROXIMANT:Lspeech/patts/PhonemeDefinition$Manner;

.field public static final enum CLICK:Lspeech/patts/PhonemeDefinition$Manner;

.field public static final enum FRICATIVE:Lspeech/patts/PhonemeDefinition$Manner;

.field public static final enum LATERAL:Lspeech/patts/PhonemeDefinition$Manner;

.field public static final enum NASAL:Lspeech/patts/PhonemeDefinition$Manner;

.field public static final enum STOP:Lspeech/patts/PhonemeDefinition$Manner;

.field public static final enum TAP:Lspeech/patts/PhonemeDefinition$Manner;

.field public static final enum TRILL:Lspeech/patts/PhonemeDefinition$Manner;

.field public static final enum UNDEFINED_MANNER:Lspeech/patts/PhonemeDefinition$Manner;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lspeech/patts/PhonemeDefinition$Manner;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 83
    new-instance v0, Lspeech/patts/PhonemeDefinition$Manner;

    const-string v1, "UNDEFINED_MANNER"

    invoke-direct {v0, v1, v5, v5, v5}, Lspeech/patts/PhonemeDefinition$Manner;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Manner;->UNDEFINED_MANNER:Lspeech/patts/PhonemeDefinition$Manner;

    .line 84
    new-instance v0, Lspeech/patts/PhonemeDefinition$Manner;

    const-string v1, "APPROXIMANT"

    invoke-direct {v0, v1, v6, v6, v6}, Lspeech/patts/PhonemeDefinition$Manner;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Manner;->APPROXIMANT:Lspeech/patts/PhonemeDefinition$Manner;

    .line 85
    new-instance v0, Lspeech/patts/PhonemeDefinition$Manner;

    const-string v1, "LATERAL"

    invoke-direct {v0, v1, v7, v7, v7}, Lspeech/patts/PhonemeDefinition$Manner;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Manner;->LATERAL:Lspeech/patts/PhonemeDefinition$Manner;

    .line 86
    new-instance v0, Lspeech/patts/PhonemeDefinition$Manner;

    const-string v1, "NASAL"

    invoke-direct {v0, v1, v8, v8, v8}, Lspeech/patts/PhonemeDefinition$Manner;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Manner;->NASAL:Lspeech/patts/PhonemeDefinition$Manner;

    .line 87
    new-instance v0, Lspeech/patts/PhonemeDefinition$Manner;

    const-string v1, "AFFRICATE"

    invoke-direct {v0, v1, v9, v9, v9}, Lspeech/patts/PhonemeDefinition$Manner;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Manner;->AFFRICATE:Lspeech/patts/PhonemeDefinition$Manner;

    .line 88
    new-instance v0, Lspeech/patts/PhonemeDefinition$Manner;

    const-string v1, "STOP"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$Manner;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Manner;->STOP:Lspeech/patts/PhonemeDefinition$Manner;

    .line 89
    new-instance v0, Lspeech/patts/PhonemeDefinition$Manner;

    const-string v1, "FRICATIVE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$Manner;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Manner;->FRICATIVE:Lspeech/patts/PhonemeDefinition$Manner;

    .line 90
    new-instance v0, Lspeech/patts/PhonemeDefinition$Manner;

    const-string v1, "TRILL"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$Manner;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Manner;->TRILL:Lspeech/patts/PhonemeDefinition$Manner;

    .line 91
    new-instance v0, Lspeech/patts/PhonemeDefinition$Manner;

    const-string v1, "TAP"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$Manner;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Manner;->TAP:Lspeech/patts/PhonemeDefinition$Manner;

    .line 92
    new-instance v0, Lspeech/patts/PhonemeDefinition$Manner;

    const-string v1, "CLICK"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, Lspeech/patts/PhonemeDefinition$Manner;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Manner;->CLICK:Lspeech/patts/PhonemeDefinition$Manner;

    .line 81
    const/16 v0, 0xa

    new-array v0, v0, [Lspeech/patts/PhonemeDefinition$Manner;

    sget-object v1, Lspeech/patts/PhonemeDefinition$Manner;->UNDEFINED_MANNER:Lspeech/patts/PhonemeDefinition$Manner;

    aput-object v1, v0, v5

    sget-object v1, Lspeech/patts/PhonemeDefinition$Manner;->APPROXIMANT:Lspeech/patts/PhonemeDefinition$Manner;

    aput-object v1, v0, v6

    sget-object v1, Lspeech/patts/PhonemeDefinition$Manner;->LATERAL:Lspeech/patts/PhonemeDefinition$Manner;

    aput-object v1, v0, v7

    sget-object v1, Lspeech/patts/PhonemeDefinition$Manner;->NASAL:Lspeech/patts/PhonemeDefinition$Manner;

    aput-object v1, v0, v8

    sget-object v1, Lspeech/patts/PhonemeDefinition$Manner;->AFFRICATE:Lspeech/patts/PhonemeDefinition$Manner;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lspeech/patts/PhonemeDefinition$Manner;->STOP:Lspeech/patts/PhonemeDefinition$Manner;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lspeech/patts/PhonemeDefinition$Manner;->FRICATIVE:Lspeech/patts/PhonemeDefinition$Manner;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lspeech/patts/PhonemeDefinition$Manner;->TRILL:Lspeech/patts/PhonemeDefinition$Manner;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lspeech/patts/PhonemeDefinition$Manner;->TAP:Lspeech/patts/PhonemeDefinition$Manner;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lspeech/patts/PhonemeDefinition$Manner;->CLICK:Lspeech/patts/PhonemeDefinition$Manner;

    aput-object v2, v0, v1

    sput-object v0, Lspeech/patts/PhonemeDefinition$Manner;->$VALUES:[Lspeech/patts/PhonemeDefinition$Manner;

    .line 119
    new-instance v0, Lspeech/patts/PhonemeDefinition$Manner$1;

    invoke-direct {v0}, Lspeech/patts/PhonemeDefinition$Manner$1;-><init>()V

    sput-object v0, Lspeech/patts/PhonemeDefinition$Manner;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 129
    iput p3, p0, Lspeech/patts/PhonemeDefinition$Manner;->index:I

    .line 130
    iput p4, p0, Lspeech/patts/PhonemeDefinition$Manner;->value:I

    .line 131
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lspeech/patts/PhonemeDefinition$Manner;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 81
    const-class v0, Lspeech/patts/PhonemeDefinition$Manner;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lspeech/patts/PhonemeDefinition$Manner;

    return-object v0
.end method

.method public static values()[Lspeech/patts/PhonemeDefinition$Manner;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lspeech/patts/PhonemeDefinition$Manner;->$VALUES:[Lspeech/patts/PhonemeDefinition$Manner;

    invoke-virtual {v0}, [Lspeech/patts/PhonemeDefinition$Manner;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lspeech/patts/PhonemeDefinition$Manner;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lspeech/patts/PhonemeDefinition$Manner;->value:I

    return v0
.end method
