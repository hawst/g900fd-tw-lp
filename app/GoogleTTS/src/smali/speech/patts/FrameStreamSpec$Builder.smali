.class public final Lspeech/patts/FrameStreamSpec$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "FrameStreamSpec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/FrameStreamSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/FrameStreamSpec;",
        "Lspeech/patts/FrameStreamSpec$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/FrameStreamSpec;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 333
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/FrameStreamSpec$Builder;
    .locals 1

    .prologue
    .line 327
    invoke-static {}, Lspeech/patts/FrameStreamSpec$Builder;->create()Lspeech/patts/FrameStreamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/FrameStreamSpec$Builder;
    .locals 3

    .prologue
    .line 336
    new-instance v0, Lspeech/patts/FrameStreamSpec$Builder;

    invoke-direct {v0}, Lspeech/patts/FrameStreamSpec$Builder;-><init>()V

    .line 337
    .local v0, "builder":Lspeech/patts/FrameStreamSpec$Builder;
    new-instance v1, Lspeech/patts/FrameStreamSpec;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/FrameStreamSpec;-><init>(Lspeech/patts/FrameStreamSpec$1;)V

    iput-object v1, v0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    .line 338
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 327
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec$Builder;->build()Lspeech/patts/FrameStreamSpec;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/FrameStreamSpec;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    invoke-static {v0}, Lspeech/patts/FrameStreamSpec$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 369
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec$Builder;->buildPartial()Lspeech/patts/FrameStreamSpec;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/FrameStreamSpec;
    .locals 3

    .prologue
    .line 382
    iget-object v1, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    if-nez v1, :cond_0

    .line 383
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 386
    :cond_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    .line 387
    .local v0, "returnMe":Lspeech/patts/FrameStreamSpec;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    .line 388
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 327
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec$Builder;->clone()Lspeech/patts/FrameStreamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 327
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec$Builder;->clone()Lspeech/patts/FrameStreamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 327
    invoke-virtual {p0}, Lspeech/patts/FrameStreamSpec$Builder;->clone()Lspeech/patts/FrameStreamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2

    .prologue
    .line 355
    invoke-static {}, Lspeech/patts/FrameStreamSpec$Builder;->create()Lspeech/patts/FrameStreamSpec$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    invoke-virtual {v0, v1}, Lspeech/patts/FrameStreamSpec$Builder;->mergeFrom(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/FrameStreamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    invoke-virtual {v0}, Lspeech/patts/FrameStreamSpec;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeAmplitudeSpec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 674
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    invoke-virtual {v0}, Lspeech/patts/FrameStreamSpec;->hasAmplitudeSpec()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->amplitudeSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0}, Lspeech/patts/FrameStreamSpec;->access$1600(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 676
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    iget-object v1, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->amplitudeSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v1}, Lspeech/patts/FrameStreamSpec;->access$1600(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/SpeechParamSpec;->newBuilder(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/SpeechParamSpec$Builder;->mergeFrom(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec$Builder;->buildPartial()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    # setter for: Lspeech/patts/FrameStreamSpec;->amplitudeSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$1602(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    .line 681
    :goto_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasAmplitudeSpec:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$1502(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 682
    return-object p0

    .line 679
    :cond_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->amplitudeSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$1602(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    goto :goto_0
.end method

.method public mergeAperiodicitySpec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 711
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    invoke-virtual {v0}, Lspeech/patts/FrameStreamSpec;->hasAperiodicitySpec()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->aperiodicitySpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0}, Lspeech/patts/FrameStreamSpec;->access$1800(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 713
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    iget-object v1, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->aperiodicitySpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v1}, Lspeech/patts/FrameStreamSpec;->access$1800(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/SpeechParamSpec;->newBuilder(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/SpeechParamSpec$Builder;->mergeFrom(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec$Builder;->buildPartial()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    # setter for: Lspeech/patts/FrameStreamSpec;->aperiodicitySpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$1802(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    .line 718
    :goto_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasAperiodicitySpec:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$1702(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 719
    return-object p0

    .line 716
    :cond_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->aperiodicitySpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$1802(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    goto :goto_0
.end method

.method public mergeBandAperiodicitySpec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 896
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    invoke-virtual {v0}, Lspeech/patts/FrameStreamSpec;->hasBandAperiodicitySpec()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->bandAperiodicitySpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0}, Lspeech/patts/FrameStreamSpec;->access$2800(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 898
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    iget-object v1, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->bandAperiodicitySpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v1}, Lspeech/patts/FrameStreamSpec;->access$2800(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/SpeechParamSpec;->newBuilder(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/SpeechParamSpec$Builder;->mergeFrom(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec$Builder;->buildPartial()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    # setter for: Lspeech/patts/FrameStreamSpec;->bandAperiodicitySpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$2802(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    .line 903
    :goto_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasBandAperiodicitySpec:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$2702(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 904
    return-object p0

    .line 901
    :cond_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->bandAperiodicitySpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$2802(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 327
    check-cast p1, Lspeech/patts/FrameStreamSpec;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/FrameStreamSpec$Builder;->mergeFrom(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/FrameStreamSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/FrameStreamSpec;

    .prologue
    .line 392
    invoke-static {}, Lspeech/patts/FrameStreamSpec;->getDefaultInstance()Lspeech/patts/FrameStreamSpec;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 432
    :cond_0
    :goto_0
    return-object p0

    .line 393
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasFrameShift()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 394
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getFrameShift()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->setFrameShift(F)Lspeech/patts/FrameStreamSpec$Builder;

    .line 396
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasSampleRate()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 397
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getSampleRate()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->setSampleRate(I)Lspeech/patts/FrameStreamSpec$Builder;

    .line 399
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasFrequencyWarping()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 400
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getFrequencyWarping()F

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->setFrequencyWarping(F)Lspeech/patts/FrameStreamSpec$Builder;

    .line 402
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasUseLinearAp()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 403
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getUseLinearAp()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->setUseLinearAp(Z)Lspeech/patts/FrameStreamSpec$Builder;

    .line 405
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasMgcGammaStage()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 406
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getMgcGammaStage()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->setMgcGammaStage(I)Lspeech/patts/FrameStreamSpec$Builder;

    .line 408
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasDspProcType()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 409
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getDspProcType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->setDspProcType(Ljava/lang/String;)Lspeech/patts/FrameStreamSpec$Builder;

    .line 411
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasAmplitudeSpec()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 412
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getAmplitudeSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->mergeAmplitudeSpec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;

    .line 414
    :cond_8
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasAperiodicitySpec()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 415
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getAperiodicitySpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->mergeAperiodicitySpec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;

    .line 417
    :cond_9
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasLogF0Spec()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 418
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getLogF0Spec()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->mergeLogF0Spec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;

    .line 420
    :cond_a
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasVoicingSpec()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 421
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getVoicingSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->mergeVoicingSpec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;

    .line 423
    :cond_b
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasMelCepstrumSpec()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 424
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getMelCepstrumSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->mergeMelCepstrumSpec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;

    .line 426
    :cond_c
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasLineSpectralPairSpec()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 427
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getLineSpectralPairSpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->mergeLineSpectralPairSpec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;

    .line 429
    :cond_d
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->hasBandAperiodicitySpec()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    invoke-virtual {p1}, Lspeech/patts/FrameStreamSpec;->getBandAperiodicitySpec()Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/FrameStreamSpec$Builder;->mergeBandAperiodicitySpec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;

    goto/16 :goto_0
.end method

.method public mergeLineSpectralPairSpec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 859
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    invoke-virtual {v0}, Lspeech/patts/FrameStreamSpec;->hasLineSpectralPairSpec()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->lineSpectralPairSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0}, Lspeech/patts/FrameStreamSpec;->access$2600(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 861
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    iget-object v1, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->lineSpectralPairSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v1}, Lspeech/patts/FrameStreamSpec;->access$2600(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/SpeechParamSpec;->newBuilder(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/SpeechParamSpec$Builder;->mergeFrom(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec$Builder;->buildPartial()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    # setter for: Lspeech/patts/FrameStreamSpec;->lineSpectralPairSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$2602(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    .line 866
    :goto_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasLineSpectralPairSpec:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$2502(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 867
    return-object p0

    .line 864
    :cond_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->lineSpectralPairSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$2602(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    goto :goto_0
.end method

.method public mergeLogF0Spec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 748
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    invoke-virtual {v0}, Lspeech/patts/FrameStreamSpec;->hasLogF0Spec()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->logF0Spec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0}, Lspeech/patts/FrameStreamSpec;->access$2000(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 750
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    iget-object v1, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->logF0Spec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v1}, Lspeech/patts/FrameStreamSpec;->access$2000(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/SpeechParamSpec;->newBuilder(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/SpeechParamSpec$Builder;->mergeFrom(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec$Builder;->buildPartial()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    # setter for: Lspeech/patts/FrameStreamSpec;->logF0Spec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$2002(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    .line 755
    :goto_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasLogF0Spec:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$1902(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 756
    return-object p0

    .line 753
    :cond_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->logF0Spec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$2002(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    goto :goto_0
.end method

.method public mergeMelCepstrumSpec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 822
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    invoke-virtual {v0}, Lspeech/patts/FrameStreamSpec;->hasMelCepstrumSpec()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->melCepstrumSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0}, Lspeech/patts/FrameStreamSpec;->access$2400(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 824
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    iget-object v1, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->melCepstrumSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v1}, Lspeech/patts/FrameStreamSpec;->access$2400(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/SpeechParamSpec;->newBuilder(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/SpeechParamSpec$Builder;->mergeFrom(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec$Builder;->buildPartial()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    # setter for: Lspeech/patts/FrameStreamSpec;->melCepstrumSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$2402(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    .line 829
    :goto_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasMelCepstrumSpec:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$2302(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 830
    return-object p0

    .line 827
    :cond_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->melCepstrumSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$2402(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    goto :goto_0
.end method

.method public mergeVoicingSpec(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/SpeechParamSpec;

    .prologue
    .line 785
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    invoke-virtual {v0}, Lspeech/patts/FrameStreamSpec;->hasVoicingSpec()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->voicingSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0}, Lspeech/patts/FrameStreamSpec;->access$2200(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v0

    invoke-static {}, Lspeech/patts/SpeechParamSpec;->getDefaultInstance()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 787
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    iget-object v1, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # getter for: Lspeech/patts/FrameStreamSpec;->voicingSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v1}, Lspeech/patts/FrameStreamSpec;->access$2200(Lspeech/patts/FrameStreamSpec;)Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    invoke-static {v1}, Lspeech/patts/SpeechParamSpec;->newBuilder(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lspeech/patts/SpeechParamSpec$Builder;->mergeFrom(Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/patts/SpeechParamSpec$Builder;->buildPartial()Lspeech/patts/SpeechParamSpec;

    move-result-object v1

    # setter for: Lspeech/patts/FrameStreamSpec;->voicingSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$2202(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    .line 792
    :goto_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasVoicingSpec:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$2102(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 793
    return-object p0

    .line 790
    :cond_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->voicingSpec_:Lspeech/patts/SpeechParamSpec;
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$2202(Lspeech/patts/FrameStreamSpec;Lspeech/patts/SpeechParamSpec;)Lspeech/patts/SpeechParamSpec;

    goto :goto_0
.end method

.method public setDspProcType(Ljava/lang/String;)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 640
    if-nez p1, :cond_0

    .line 641
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 643
    :cond_0
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasDspProcType:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$1302(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 644
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->dspProcType_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$1402(Lspeech/patts/FrameStreamSpec;Ljava/lang/String;)Ljava/lang/String;

    .line 645
    return-object p0
.end method

.method public setFrameShift(F)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 550
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasFrameShift:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$302(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 551
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->frameShift_:F
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$402(Lspeech/patts/FrameStreamSpec;F)F

    .line 552
    return-object p0
.end method

.method public setFrequencyWarping(F)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 586
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasFrequencyWarping:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$702(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 587
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->frequencyWarping_:F
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$802(Lspeech/patts/FrameStreamSpec;F)F

    .line 588
    return-object p0
.end method

.method public setMgcGammaStage(I)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 622
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasMgcGammaStage:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$1102(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 623
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->mgcGammaStage_:I
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$1202(Lspeech/patts/FrameStreamSpec;I)I

    .line 624
    return-object p0
.end method

.method public setSampleRate(I)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 568
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasSampleRate:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$502(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 569
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->sampleRate_:I
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$602(Lspeech/patts/FrameStreamSpec;I)I

    .line 570
    return-object p0
.end method

.method public setUseLinearAp(Z)Lspeech/patts/FrameStreamSpec$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 604
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/FrameStreamSpec;->hasUseLinearAp:Z
    invoke-static {v0, v1}, Lspeech/patts/FrameStreamSpec;->access$902(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 605
    iget-object v0, p0, Lspeech/patts/FrameStreamSpec$Builder;->result:Lspeech/patts/FrameStreamSpec;

    # setter for: Lspeech/patts/FrameStreamSpec;->useLinearAp_:Z
    invoke-static {v0, p1}, Lspeech/patts/FrameStreamSpec;->access$1002(Lspeech/patts/FrameStreamSpec;Z)Z

    .line 606
    return-object p0
.end method
