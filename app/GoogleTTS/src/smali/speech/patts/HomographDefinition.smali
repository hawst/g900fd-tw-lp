.class public final Lspeech/patts/HomographDefinition;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "HomographDefinition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/HomographDefinition$1;,
        Lspeech/patts/HomographDefinition$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/HomographDefinition;


# instance fields
.field private beforeProper_:Z

.field private beforeSemclass_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private beforeWord_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private context_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/HomographContext;",
            ">;"
        }
    .end annotation
.end field

.field private featureId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llocation/unified/FeatureIdProto;",
            ">;"
        }
    .end annotation
.end field

.field private followProper_:Z

.field private followSemclass_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private followWord_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasBeforeProper:Z

.field private hasFollowProper:Z

.field private hasIsDefault:Z

.field private hasSentenceFinal:Z

.field private hasSentenceInitial:Z

.field private hasSpelling:Z

.field private hasText:Z

.field private hasTranslationRule:Z

.field private hasWordid:Z

.field private isDefault_:Z

.field private memoizedSerializedSize:I

.field private morphosyntacticFeatures_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private phrase_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private posCategory_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s2CellId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private sentenceFinal_:Z

.field private sentenceInitial_:Z

.field private spelling_:Ljava/lang/String;

.field private stopWord_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private text_:Ljava/lang/String;

.field private translationRule_:Ljava/lang/String;

.field private wordid_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1455
    new-instance v0, Lspeech/patts/HomographDefinition;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/HomographDefinition;-><init>(Z)V

    sput-object v0, Lspeech/patts/HomographDefinition;->defaultInstance:Lspeech/patts/HomographDefinition;

    .line 1456
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 1457
    sget-object v0, Lspeech/patts/HomographDefinition;->defaultInstance:Lspeech/patts/HomographDefinition;

    invoke-direct {v0}, Lspeech/patts/HomographDefinition;->initFields()V

    .line 1458
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->spelling_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->wordid_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->translationRule_:Ljava/lang/String;

    .line 46
    iput-boolean v1, p0, Lspeech/patts/HomographDefinition;->sentenceInitial_:Z

    .line 53
    iput-boolean v1, p0, Lspeech/patts/HomographDefinition;->sentenceFinal_:Z

    .line 59
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;

    .line 71
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;

    .line 83
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;

    .line 95
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;

    .line 107
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;

    .line 120
    iput-boolean v1, p0, Lspeech/patts/HomographDefinition;->beforeProper_:Z

    .line 127
    iput-boolean v1, p0, Lspeech/patts/HomographDefinition;->followProper_:Z

    .line 133
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;

    .line 145
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;

    .line 158
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->text_:Ljava/lang/String;

    .line 164
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;

    .line 176
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;

    .line 188
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;

    .line 200
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;

    .line 213
    iput-boolean v1, p0, Lspeech/patts/HomographDefinition;->isDefault_:Z

    .line 293
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/HomographDefinition;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/HomographDefinition;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/HomographDefinition$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/HomographDefinition$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/HomographDefinition;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->spelling_:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->wordid_:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->translationRule_:Ljava/lang/String;

    .line 46
    iput-boolean v1, p0, Lspeech/patts/HomographDefinition;->sentenceInitial_:Z

    .line 53
    iput-boolean v1, p0, Lspeech/patts/HomographDefinition;->sentenceFinal_:Z

    .line 59
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;

    .line 71
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;

    .line 83
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;

    .line 95
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;

    .line 107
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;

    .line 120
    iput-boolean v1, p0, Lspeech/patts/HomographDefinition;->beforeProper_:Z

    .line 127
    iput-boolean v1, p0, Lspeech/patts/HomographDefinition;->followProper_:Z

    .line 133
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;

    .line 145
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;

    .line 158
    const-string v0, ""

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->text_:Ljava/lang/String;

    .line 164
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;

    .line 176
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;

    .line 188
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;

    .line 200
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;

    .line 213
    iput-boolean v1, p0, Lspeech/patts/HomographDefinition;->isDefault_:Z

    .line 293
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/HomographDefinition;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1000(Lspeech/patts/HomographDefinition;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1002(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1100(Lspeech/patts/HomographDefinition;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1102(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1200(Lspeech/patts/HomographDefinition;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1202(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1300(Lspeech/patts/HomographDefinition;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1302(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1402(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->hasSpelling:Z

    return p1
.end method

.method static synthetic access$1502(Lspeech/patts/HomographDefinition;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->spelling_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1602(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->hasWordid:Z

    return p1
.end method

.method static synthetic access$1702(Lspeech/patts/HomographDefinition;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->wordid_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1802(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->hasTranslationRule:Z

    return p1
.end method

.method static synthetic access$1902(Lspeech/patts/HomographDefinition;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->translationRule_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2002(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->hasSentenceInitial:Z

    return p1
.end method

.method static synthetic access$2102(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->sentenceInitial_:Z

    return p1
.end method

.method static synthetic access$2202(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->hasSentenceFinal:Z

    return p1
.end method

.method static synthetic access$2302(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->sentenceFinal_:Z

    return p1
.end method

.method static synthetic access$2402(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->hasBeforeProper:Z

    return p1
.end method

.method static synthetic access$2502(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->beforeProper_:Z

    return p1
.end method

.method static synthetic access$2602(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->hasFollowProper:Z

    return p1
.end method

.method static synthetic access$2702(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->followProper_:Z

    return p1
.end method

.method static synthetic access$2802(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->hasText:Z

    return p1
.end method

.method static synthetic access$2902(Lspeech/patts/HomographDefinition;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->text_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lspeech/patts/HomographDefinition;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3002(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->hasIsDefault:Z

    return p1
.end method

.method static synthetic access$302(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3102(Lspeech/patts/HomographDefinition;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/HomographDefinition;->isDefault_:Z

    return p1
.end method

.method static synthetic access$400(Lspeech/patts/HomographDefinition;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lspeech/patts/HomographDefinition;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lspeech/patts/HomographDefinition;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lspeech/patts/HomographDefinition;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$800(Lspeech/patts/HomographDefinition;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$802(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$900(Lspeech/patts/HomographDefinition;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$902(Lspeech/patts/HomographDefinition;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/HomographDefinition;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lspeech/patts/HomographDefinition;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/HomographDefinition;->defaultInstance:Lspeech/patts/HomographDefinition;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 218
    return-void
.end method

.method public static newBuilder()Lspeech/patts/HomographDefinition$Builder;
    .locals 1

    .prologue
    .line 495
    # invokes: Lspeech/patts/HomographDefinition$Builder;->create()Lspeech/patts/HomographDefinition$Builder;
    invoke-static {}, Lspeech/patts/HomographDefinition$Builder;->access$100()Lspeech/patts/HomographDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/HomographDefinition;)Lspeech/patts/HomographDefinition$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/HomographDefinition;

    .prologue
    .line 498
    invoke-static {}, Lspeech/patts/HomographDefinition;->newBuilder()Lspeech/patts/HomographDefinition$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/HomographDefinition$Builder;->mergeFrom(Lspeech/patts/HomographDefinition;)Lspeech/patts/HomographDefinition$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBeforeProper()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->beforeProper_:Z

    return v0
.end method

.method public getBeforeSemclassList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->beforeSemclass_:Ljava/util/List;

    return-object v0
.end method

.method public getBeforeWordList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->beforeWord_:Ljava/util/List;

    return-object v0
.end method

.method public getContextList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/HomographContext;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->context_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getDefaultInstanceForType()Lspeech/patts/HomographDefinition;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/HomographDefinition;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/HomographDefinition;->defaultInstance:Lspeech/patts/HomographDefinition;

    return-object v0
.end method

.method public getFeatureIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Llocation/unified/FeatureIdProto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->featureId_:Ljava/util/List;

    return-object v0
.end method

.method public getFollowProper()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->followProper_:Z

    return v0
.end method

.method public getFollowSemclassList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->followSemclass_:Ljava/util/List;

    return-object v0
.end method

.method public getFollowWordList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->followWord_:Ljava/util/List;

    return-object v0
.end method

.method public getIsDefault()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->isDefault_:Z

    return v0
.end method

.method public getMorphosyntacticFeaturesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->morphosyntacticFeatures_:Ljava/util/List;

    return-object v0
.end method

.method public getPhraseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->phrase_:Ljava/util/List;

    return-object v0
.end method

.method public getPosCategoryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->posCategory_:Ljava/util/List;

    return-object v0
.end method

.method public getS2CellIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->s2CellId_:Ljava/util/List;

    return-object v0
.end method

.method public getSentenceFinal()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->sentenceFinal_:Z

    return v0
.end method

.method public getSentenceInitial()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->sentenceInitial_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    .line 295
    iget v4, p0, Lspeech/patts/HomographDefinition;->memoizedSerializedSize:I

    .line 296
    .local v4, "size":I
    const/4 v6, -0x1

    if-eq v4, v6, :cond_0

    move v5, v4

    .line 425
    .end local v4    # "size":I
    .local v5, "size":I
    :goto_0
    return v5

    .line 298
    .end local v5    # "size":I
    .restart local v4    # "size":I
    :cond_0
    const/4 v4, 0x0

    .line 299
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasSpelling()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 300
    const/4 v6, 0x1

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getSpelling()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v6

    add-int/2addr v4, v6

    .line 303
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasWordid()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 304
    const/4 v6, 0x2

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getWordid()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v6

    add-int/2addr v4, v6

    .line 307
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasSentenceInitial()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 308
    const/4 v6, 0x3

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getSentenceInitial()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v6

    add-int/2addr v4, v6

    .line 311
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasSentenceFinal()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 312
    const/4 v6, 0x4

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getSentenceFinal()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v6

    add-int/2addr v4, v6

    .line 316
    :cond_4
    const/4 v0, 0x0

    .line 317
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getBeforeSemclassList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 318
    .local v2, "element":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 320
    goto :goto_1

    .line 321
    .end local v2    # "element":Ljava/lang/String;
    :cond_5
    add-int/2addr v4, v0

    .line 322
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getBeforeSemclassList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    .line 325
    const/4 v0, 0x0

    .line 326
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getFollowSemclassList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 327
    .restart local v2    # "element":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 329
    goto :goto_2

    .line 330
    .end local v2    # "element":Ljava/lang/String;
    :cond_6
    add-int/2addr v4, v0

    .line 331
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getFollowSemclassList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    .line 334
    const/4 v0, 0x0

    .line 335
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getBeforeWordList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 336
    .restart local v2    # "element":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 338
    goto :goto_3

    .line 339
    .end local v2    # "element":Ljava/lang/String;
    :cond_7
    add-int/2addr v4, v0

    .line 340
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getBeforeWordList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    .line 343
    const/4 v0, 0x0

    .line 344
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getFollowWordList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 345
    .restart local v2    # "element":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 347
    goto :goto_4

    .line 348
    .end local v2    # "element":Ljava/lang/String;
    :cond_8
    add-int/2addr v4, v0

    .line 349
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getFollowWordList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    .line 351
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasBeforeProper()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 352
    const/16 v6, 0x9

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getBeforeProper()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v6

    add-int/2addr v4, v6

    .line 355
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasFollowProper()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 356
    const/16 v6, 0xa

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getFollowProper()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v6

    add-int/2addr v4, v6

    .line 359
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasIsDefault()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 360
    const/16 v6, 0xb

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getIsDefault()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v6

    add-int/2addr v4, v6

    .line 364
    :cond_b
    const/4 v0, 0x0

    .line 365
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getStopWordList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 366
    .restart local v2    # "element":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 368
    goto :goto_5

    .line 369
    .end local v2    # "element":Ljava/lang/String;
    :cond_c
    add-int/2addr v4, v0

    .line 370
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getStopWordList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    .line 373
    const/4 v0, 0x0

    .line 374
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getPhraseList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 375
    .restart local v2    # "element":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 377
    goto :goto_6

    .line 378
    .end local v2    # "element":Ljava/lang/String;
    :cond_d
    add-int/2addr v4, v0

    .line 379
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getPhraseList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    .line 381
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getContextList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lspeech/patts/HomographContext;

    .line 382
    .local v2, "element":Lspeech/patts/HomographContext;
    const/16 v6, 0xe

    invoke-static {v6, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v6

    add-int/2addr v4, v6

    .line 384
    goto :goto_7

    .line 385
    .end local v2    # "element":Lspeech/patts/HomographContext;
    :cond_e
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasText()Z

    move-result v6

    if-eqz v6, :cond_f

    .line 386
    const/16 v6, 0xf

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v6

    add-int/2addr v4, v6

    .line 389
    :cond_f
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasTranslationRule()Z

    move-result v6

    if-eqz v6, :cond_10

    .line 390
    const/16 v6, 0x11

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getTranslationRule()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v6

    add-int/2addr v4, v6

    .line 394
    :cond_10
    const/4 v0, 0x0

    .line 395
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getPosCategoryList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 396
    .local v2, "element":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 398
    goto :goto_8

    .line 399
    .end local v2    # "element":Ljava/lang/String;
    :cond_11
    add-int/2addr v4, v0

    .line 400
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getPosCategoryList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v4, v6

    .line 402
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getFeatureIdList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Llocation/unified/FeatureIdProto;

    .line 403
    .local v2, "element":Llocation/unified/FeatureIdProto;
    const/16 v6, 0x13

    invoke-static {v6, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v6

    add-int/2addr v4, v6

    .line 405
    goto :goto_9

    .line 407
    .end local v2    # "element":Llocation/unified/FeatureIdProto;
    :cond_12
    const/4 v0, 0x0

    .line 408
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getS2CellIdList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_13

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 409
    .local v2, "element":J
    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64SizeNoTag(J)I

    move-result v6

    add-int/2addr v0, v6

    .line 411
    goto :goto_a

    .line 412
    .end local v2    # "element":J
    :cond_13
    add-int/2addr v4, v0

    .line 413
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getS2CellIdList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v4, v6

    .line 416
    const/4 v0, 0x0

    .line 417
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getMorphosyntacticFeaturesList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 418
    .local v2, "element":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 420
    goto :goto_b

    .line 421
    .end local v2    # "element":Ljava/lang/String;
    :cond_14
    add-int/2addr v4, v0

    .line 422
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getMorphosyntacticFeaturesList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v4, v6

    .line 424
    iput v4, p0, Lspeech/patts/HomographDefinition;->memoizedSerializedSize:I

    move v5, v4

    .line 425
    .end local v4    # "size":I
    .restart local v5    # "size":I
    goto/16 :goto_0
.end method

.method public getSpelling()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->spelling_:Ljava/lang/String;

    return-object v0
.end method

.method public getStopWordList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->stopWord_:Ljava/util/List;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->text_:Ljava/lang/String;

    return-object v0
.end method

.method public getTranslationRule()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->translationRule_:Ljava/lang/String;

    return-object v0
.end method

.method public getWordid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lspeech/patts/HomographDefinition;->wordid_:Ljava/lang/String;

    return-object v0
.end method

.method public hasBeforeProper()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->hasBeforeProper:Z

    return v0
.end method

.method public hasFollowProper()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->hasFollowProper:Z

    return v0
.end method

.method public hasIsDefault()Z
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->hasIsDefault:Z

    return v0
.end method

.method public hasSentenceFinal()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->hasSentenceFinal:Z

    return v0
.end method

.method public hasSentenceInitial()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->hasSentenceInitial:Z

    return v0
.end method

.method public hasSpelling()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->hasSpelling:Z

    return v0
.end method

.method public hasText()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->hasText:Z

    return v0
.end method

.method public hasTranslationRule()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->hasTranslationRule:Z

    return v0
.end method

.method public hasWordid()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lspeech/patts/HomographDefinition;->hasWordid:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 220
    iget-boolean v3, p0, Lspeech/patts/HomographDefinition;->hasSpelling:Z

    if-nez v3, :cond_1

    .line 225
    :cond_0
    :goto_0
    return v2

    .line 221
    :cond_1
    iget-boolean v3, p0, Lspeech/patts/HomographDefinition;->hasWordid:Z

    if-eqz v3, :cond_0

    .line 222
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getContextList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/HomographContext;

    .line 223
    .local v0, "element":Lspeech/patts/HomographContext;
    invoke-virtual {v0}, Lspeech/patts/HomographContext;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 225
    .end local v0    # "element":Lspeech/patts/HomographContext;
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->toBuilder()Lspeech/patts/HomographDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/HomographDefinition$Builder;
    .locals 1

    .prologue
    .line 500
    invoke-static {p0}, Lspeech/patts/HomographDefinition;->newBuilder(Lspeech/patts/HomographDefinition;)Lspeech/patts/HomographDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 230
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getSerializedSize()I

    .line 231
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasSpelling()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 232
    const/4 v3, 0x1

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getSpelling()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 234
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasWordid()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 235
    const/4 v3, 0x2

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getWordid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 237
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasSentenceInitial()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 238
    const/4 v3, 0x3

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getSentenceInitial()Z

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 240
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasSentenceFinal()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 241
    const/4 v3, 0x4

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getSentenceFinal()Z

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 243
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getBeforeSemclassList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 244
    .local v0, "element":Ljava/lang/String;
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 246
    .end local v0    # "element":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getFollowSemclassList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 247
    .restart local v0    # "element":Ljava/lang/String;
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_1

    .line 249
    .end local v0    # "element":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getBeforeWordList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 250
    .restart local v0    # "element":Ljava/lang/String;
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_2

    .line 252
    .end local v0    # "element":Ljava/lang/String;
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getFollowWordList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 253
    .restart local v0    # "element":Ljava/lang/String;
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_3

    .line 255
    .end local v0    # "element":Ljava/lang/String;
    :cond_7
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasBeforeProper()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 256
    const/16 v3, 0x9

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getBeforeProper()Z

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 258
    :cond_8
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasFollowProper()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 259
    const/16 v3, 0xa

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getFollowProper()Z

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 261
    :cond_9
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasIsDefault()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 262
    const/16 v3, 0xb

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getIsDefault()Z

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 264
    :cond_a
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getStopWordList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 265
    .restart local v0    # "element":Ljava/lang/String;
    const/16 v3, 0xc

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_4

    .line 267
    .end local v0    # "element":Ljava/lang/String;
    :cond_b
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getPhraseList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 268
    .restart local v0    # "element":Ljava/lang/String;
    const/16 v3, 0xd

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_5

    .line 270
    .end local v0    # "element":Ljava/lang/String;
    :cond_c
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getContextList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/HomographContext;

    .line 271
    .local v0, "element":Lspeech/patts/HomographContext;
    const/16 v3, 0xe

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_6

    .line 273
    .end local v0    # "element":Lspeech/patts/HomographContext;
    :cond_d
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasText()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 274
    const/16 v3, 0xf

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 276
    :cond_e
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->hasTranslationRule()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 277
    const/16 v3, 0x11

    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getTranslationRule()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 279
    :cond_f
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getPosCategoryList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 280
    .local v0, "element":Ljava/lang/String;
    const/16 v3, 0x12

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_7

    .line 282
    .end local v0    # "element":Ljava/lang/String;
    :cond_10
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getFeatureIdList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llocation/unified/FeatureIdProto;

    .line 283
    .local v0, "element":Llocation/unified/FeatureIdProto;
    const/16 v3, 0x13

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_8

    .line 285
    .end local v0    # "element":Llocation/unified/FeatureIdProto;
    :cond_11
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getS2CellIdList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 286
    .local v0, "element":J
    const/16 v3, 0x14

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    goto :goto_9

    .line 288
    .end local v0    # "element":J
    :cond_12
    invoke-virtual {p0}, Lspeech/patts/HomographDefinition;->getMorphosyntacticFeaturesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 289
    .local v0, "element":Ljava/lang/String;
    const/16 v3, 0x15

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_a

    .line 291
    .end local v0    # "element":Ljava/lang/String;
    :cond_13
    return-void
.end method
