.class public final Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PhonologyDefinitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;",
        "Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 525
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$800()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    .locals 1

    .prologue
    .line 519
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->create()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    .locals 3

    .prologue
    .line 528
    new-instance v0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    invoke-direct {v0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;-><init>()V

    .line 529
    .local v0, "builder":Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    new-instance v1, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;-><init>(Lspeech/patts/PhonologyDefinitions$1;)V

    iput-object v1, v0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    .line 530
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 519
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->build()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 559
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    invoke-static {v0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 561
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->buildPartial()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    .locals 3

    .prologue
    .line 574
    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    if-nez v1, :cond_0

    .line 575
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 578
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    .line 579
    .local v0, "returnMe":Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    .line 580
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 519
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->clone()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 519
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->clone()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 519
    invoke-virtual {p0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->clone()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    .locals 2

    .prologue
    .line 547
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->create()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    invoke-virtual {v0, v1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->mergeFrom(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;)Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    invoke-virtual {v0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 519
    check-cast p1, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->mergeFrom(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;)Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;)Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    .locals 1
    .param p1, "other"    # Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    .prologue
    .line 584
    invoke-static {}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->getDefaultInstance()Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 594
    :cond_0
    :goto_0
    return-object p0

    .line 585
    :cond_1
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasVowelLetter()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 586
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->getVowelLetter()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->setVowelLetter(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    .line 588
    :cond_2
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasStressedPhoneme()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 589
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->getStressedPhoneme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->setStressedPhoneme(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    .line 591
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasUnstressedPhoneme()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    invoke-virtual {p1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->getUnstressedPhoneme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->setUnstressedPhoneme(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;

    goto :goto_0
.end method

.method public setStressedPhoneme(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 658
    if-nez p1, :cond_0

    .line 659
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 661
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasStressedPhoneme:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->access$1202(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;Z)Z

    .line 662
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    # setter for: Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->stressedPhoneme_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->access$1302(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;Ljava/lang/String;)Ljava/lang/String;

    .line 663
    return-object p0
.end method

.method public setUnstressedPhoneme(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 679
    if-nez p1, :cond_0

    .line 680
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 682
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasUnstressedPhoneme:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->access$1402(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;Z)Z

    .line 683
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    # setter for: Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->unstressedPhoneme_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->access$1502(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;Ljava/lang/String;)Ljava/lang/String;

    .line 684
    return-object p0
.end method

.method public setVowelLetter(Ljava/lang/String;)Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 637
    if-nez p1, :cond_0

    .line 638
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 640
    :cond_0
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->hasVowelLetter:Z
    invoke-static {v0, v1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->access$1002(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;Z)Z

    .line 641
    iget-object v0, p0, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction$Builder;->result:Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;

    # setter for: Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->vowelLetter_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;->access$1102(Lspeech/patts/PhonologyDefinitions$VowelPhonemeReduction;Ljava/lang/String;)Ljava/lang/String;

    .line 642
    return-object p0
.end method
