.class public final Lspeech/patts/RomanNumeralContextWord$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "RomanNumeralContextWord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/patts/RomanNumeralContextWord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lspeech/patts/RomanNumeralContextWord;",
        "Lspeech/patts/RomanNumeralContextWord$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lspeech/patts/RomanNumeralContextWord;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 296
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 1

    .prologue
    .line 290
    invoke-static {}, Lspeech/patts/RomanNumeralContextWord$Builder;->create()Lspeech/patts/RomanNumeralContextWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 3

    .prologue
    .line 299
    new-instance v0, Lspeech/patts/RomanNumeralContextWord$Builder;

    invoke-direct {v0}, Lspeech/patts/RomanNumeralContextWord$Builder;-><init>()V

    .line 300
    .local v0, "builder":Lspeech/patts/RomanNumeralContextWord$Builder;
    new-instance v1, Lspeech/patts/RomanNumeralContextWord;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lspeech/patts/RomanNumeralContextWord;-><init>(Lspeech/patts/RomanNumeralContextWord$1;)V

    iput-object v1, v0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    .line 301
    return-object v0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord$Builder;->build()Lspeech/patts/RomanNumeralContextWord;

    move-result-object v0

    return-object v0
.end method

.method public build()Lspeech/patts/RomanNumeralContextWord;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 330
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    invoke-static {v0}, Lspeech/patts/RomanNumeralContextWord$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 332
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord$Builder;->buildPartial()Lspeech/patts/RomanNumeralContextWord;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lspeech/patts/RomanNumeralContextWord;
    .locals 3

    .prologue
    .line 345
    iget-object v1, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    if-nez v1, :cond_0

    .line 346
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 349
    :cond_0
    iget-object v1, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    # getter for: Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;
    invoke-static {v1}, Lspeech/patts/RomanNumeralContextWord;->access$300(Lspeech/patts/RomanNumeralContextWord;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 350
    iget-object v1, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    iget-object v2, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    # getter for: Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;
    invoke-static {v2}, Lspeech/patts/RomanNumeralContextWord;->access$300(Lspeech/patts/RomanNumeralContextWord;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;
    invoke-static {v1, v2}, Lspeech/patts/RomanNumeralContextWord;->access$302(Lspeech/patts/RomanNumeralContextWord;Ljava/util/List;)Ljava/util/List;

    .line 353
    :cond_1
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    .line 354
    .local v0, "returnMe":Lspeech/patts/RomanNumeralContextWord;
    const/4 v1, 0x0

    iput-object v1, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    .line 355
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord$Builder;->clone()Lspeech/patts/RomanNumeralContextWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord$Builder;->clone()Lspeech/patts/RomanNumeralContextWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 290
    invoke-virtual {p0}, Lspeech/patts/RomanNumeralContextWord$Builder;->clone()Lspeech/patts/RomanNumeralContextWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 2

    .prologue
    .line 318
    invoke-static {}, Lspeech/patts/RomanNumeralContextWord$Builder;->create()Lspeech/patts/RomanNumeralContextWord$Builder;

    move-result-object v0

    iget-object v1, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    invoke-virtual {v0, v1}, Lspeech/patts/RomanNumeralContextWord$Builder;->mergeFrom(Lspeech/patts/RomanNumeralContextWord;)Lspeech/patts/RomanNumeralContextWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    invoke-virtual {v0}, Lspeech/patts/RomanNumeralContextWord;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/GeneratedMessageLite;

    .prologue
    .line 290
    check-cast p1, Lspeech/patts/RomanNumeralContextWord;

    .end local p1    # "x0":Lcom/google/protobuf/GeneratedMessageLite;
    invoke-virtual {p0, p1}, Lspeech/patts/RomanNumeralContextWord$Builder;->mergeFrom(Lspeech/patts/RomanNumeralContextWord;)Lspeech/patts/RomanNumeralContextWord$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lspeech/patts/RomanNumeralContextWord;)Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 2
    .param p1, "other"    # Lspeech/patts/RomanNumeralContextWord;

    .prologue
    .line 359
    invoke-static {}, Lspeech/patts/RomanNumeralContextWord;->getDefaultInstance()Lspeech/patts/RomanNumeralContextWord;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-object p0

    .line 360
    :cond_1
    # getter for: Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/RomanNumeralContextWord;->access$300(Lspeech/patts/RomanNumeralContextWord;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 361
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    # getter for: Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/RomanNumeralContextWord;->access$300(Lspeech/patts/RomanNumeralContextWord;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 362
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;
    invoke-static {v0, v1}, Lspeech/patts/RomanNumeralContextWord;->access$302(Lspeech/patts/RomanNumeralContextWord;Ljava/util/List;)Ljava/util/List;

    .line 364
    :cond_2
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    # getter for: Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;
    invoke-static {v0}, Lspeech/patts/RomanNumeralContextWord;->access$300(Lspeech/patts/RomanNumeralContextWord;)Ljava/util/List;

    move-result-object v0

    # getter for: Lspeech/patts/RomanNumeralContextWord;->spelling_:Ljava/util/List;
    invoke-static {p1}, Lspeech/patts/RomanNumeralContextWord;->access$300(Lspeech/patts/RomanNumeralContextWord;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 366
    :cond_3
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->hasSemclass()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 367
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->getSemclass()Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/RomanNumeralContextWord$Builder;->setSemclass(Lspeech/patts/RomanNumeralContextWord$SemioticClass;)Lspeech/patts/RomanNumeralContextWord$Builder;

    .line 369
    :cond_4
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->hasMorphosyntacticFeatures()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 370
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->getMorphosyntacticFeatures()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lspeech/patts/RomanNumeralContextWord$Builder;->setMorphosyntacticFeatures(Ljava/lang/String;)Lspeech/patts/RomanNumeralContextWord$Builder;

    .line 372
    :cond_5
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->hasMinDistanceBefore()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 373
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->getMinDistanceBefore()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/RomanNumeralContextWord$Builder;->setMinDistanceBefore(I)Lspeech/patts/RomanNumeralContextWord$Builder;

    .line 375
    :cond_6
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->hasMaxDistanceBefore()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 376
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->getMaxDistanceBefore()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/RomanNumeralContextWord$Builder;->setMaxDistanceBefore(I)Lspeech/patts/RomanNumeralContextWord$Builder;

    .line 378
    :cond_7
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->hasMinDistanceAfter()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 379
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->getMinDistanceAfter()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/RomanNumeralContextWord$Builder;->setMinDistanceAfter(I)Lspeech/patts/RomanNumeralContextWord$Builder;

    .line 381
    :cond_8
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->hasMaxDistanceAfter()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 382
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->getMaxDistanceAfter()I

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/RomanNumeralContextWord$Builder;->setMaxDistanceAfter(I)Lspeech/patts/RomanNumeralContextWord$Builder;

    .line 384
    :cond_9
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->hasAllProper()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    invoke-virtual {p1}, Lspeech/patts/RomanNumeralContextWord;->getAllProper()Z

    move-result v0

    invoke-virtual {p0, v0}, Lspeech/patts/RomanNumeralContextWord$Builder;->setAllProper(Z)Lspeech/patts/RomanNumeralContextWord$Builder;

    goto/16 :goto_0
.end method

.method public setAllProper(Z)Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 608
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/RomanNumeralContextWord;->hasAllProper:Z
    invoke-static {v0, v1}, Lspeech/patts/RomanNumeralContextWord;->access$1602(Lspeech/patts/RomanNumeralContextWord;Z)Z

    .line 609
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    # setter for: Lspeech/patts/RomanNumeralContextWord;->allProper_:Z
    invoke-static {v0, p1}, Lspeech/patts/RomanNumeralContextWord;->access$1702(Lspeech/patts/RomanNumeralContextWord;Z)Z

    .line 610
    return-object p0
.end method

.method public setMaxDistanceAfter(I)Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 590
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/RomanNumeralContextWord;->hasMaxDistanceAfter:Z
    invoke-static {v0, v1}, Lspeech/patts/RomanNumeralContextWord;->access$1402(Lspeech/patts/RomanNumeralContextWord;Z)Z

    .line 591
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    # setter for: Lspeech/patts/RomanNumeralContextWord;->maxDistanceAfter_:I
    invoke-static {v0, p1}, Lspeech/patts/RomanNumeralContextWord;->access$1502(Lspeech/patts/RomanNumeralContextWord;I)I

    .line 592
    return-object p0
.end method

.method public setMaxDistanceBefore(I)Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 554
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/RomanNumeralContextWord;->hasMaxDistanceBefore:Z
    invoke-static {v0, v1}, Lspeech/patts/RomanNumeralContextWord;->access$1002(Lspeech/patts/RomanNumeralContextWord;Z)Z

    .line 555
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    # setter for: Lspeech/patts/RomanNumeralContextWord;->maxDistanceBefore_:I
    invoke-static {v0, p1}, Lspeech/patts/RomanNumeralContextWord;->access$1102(Lspeech/patts/RomanNumeralContextWord;I)I

    .line 556
    return-object p0
.end method

.method public setMinDistanceAfter(I)Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 572
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/RomanNumeralContextWord;->hasMinDistanceAfter:Z
    invoke-static {v0, v1}, Lspeech/patts/RomanNumeralContextWord;->access$1202(Lspeech/patts/RomanNumeralContextWord;Z)Z

    .line 573
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    # setter for: Lspeech/patts/RomanNumeralContextWord;->minDistanceAfter_:I
    invoke-static {v0, p1}, Lspeech/patts/RomanNumeralContextWord;->access$1302(Lspeech/patts/RomanNumeralContextWord;I)I

    .line 574
    return-object p0
.end method

.method public setMinDistanceBefore(I)Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 536
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/RomanNumeralContextWord;->hasMinDistanceBefore:Z
    invoke-static {v0, v1}, Lspeech/patts/RomanNumeralContextWord;->access$802(Lspeech/patts/RomanNumeralContextWord;Z)Z

    .line 537
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    # setter for: Lspeech/patts/RomanNumeralContextWord;->minDistanceBefore_:I
    invoke-static {v0, p1}, Lspeech/patts/RomanNumeralContextWord;->access$902(Lspeech/patts/RomanNumeralContextWord;I)I

    .line 538
    return-object p0
.end method

.method public setMorphosyntacticFeatures(Ljava/lang/String;)Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 515
    if-nez p1, :cond_0

    .line 516
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 518
    :cond_0
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/RomanNumeralContextWord;->hasMorphosyntacticFeatures:Z
    invoke-static {v0, v1}, Lspeech/patts/RomanNumeralContextWord;->access$602(Lspeech/patts/RomanNumeralContextWord;Z)Z

    .line 519
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    # setter for: Lspeech/patts/RomanNumeralContextWord;->morphosyntacticFeatures_:Ljava/lang/String;
    invoke-static {v0, p1}, Lspeech/patts/RomanNumeralContextWord;->access$702(Lspeech/patts/RomanNumeralContextWord;Ljava/lang/String;)Ljava/lang/String;

    .line 520
    return-object p0
.end method

.method public setSemclass(Lspeech/patts/RomanNumeralContextWord$SemioticClass;)Lspeech/patts/RomanNumeralContextWord$Builder;
    .locals 2
    .param p1, "value"    # Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    .prologue
    .line 494
    if-nez p1, :cond_0

    .line 495
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 497
    :cond_0
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    const/4 v1, 0x1

    # setter for: Lspeech/patts/RomanNumeralContextWord;->hasSemclass:Z
    invoke-static {v0, v1}, Lspeech/patts/RomanNumeralContextWord;->access$402(Lspeech/patts/RomanNumeralContextWord;Z)Z

    .line 498
    iget-object v0, p0, Lspeech/patts/RomanNumeralContextWord$Builder;->result:Lspeech/patts/RomanNumeralContextWord;

    # setter for: Lspeech/patts/RomanNumeralContextWord;->semclass_:Lspeech/patts/RomanNumeralContextWord$SemioticClass;
    invoke-static {v0, p1}, Lspeech/patts/RomanNumeralContextWord;->access$502(Lspeech/patts/RomanNumeralContextWord;Lspeech/patts/RomanNumeralContextWord$SemioticClass;)Lspeech/patts/RomanNumeralContextWord$SemioticClass;

    .line 499
    return-object p0
.end method
