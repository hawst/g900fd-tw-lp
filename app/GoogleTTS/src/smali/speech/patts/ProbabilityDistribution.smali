.class public final Lspeech/patts/ProbabilityDistribution;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "ProbabilityDistribution.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lspeech/patts/ProbabilityDistribution$1;,
        Lspeech/patts/ProbabilityDistribution$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lspeech/patts/ProbabilityDistribution;


# instance fields
.field private amplitudeGaussians_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Gaussian;",
            ">;"
        }
    .end annotation
.end field

.field private aperiodicityGaussians_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Gaussian;",
            ">;"
        }
    .end annotation
.end field

.field private bandAperiodicityGaussians_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Gaussian;",
            ">;"
        }
    .end annotation
.end field

.field private hasVoicingPosterior:Z

.field private lineSpectralPairGaussians_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Gaussian;",
            ">;"
        }
    .end annotation
.end field

.field private logF0Gaussians_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Gaussian;",
            ">;"
        }
    .end annotation
.end field

.field private melCepstrumGaussians_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Gaussian;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private voicingPosterior_:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 765
    new-instance v0, Lspeech/patts/ProbabilityDistribution;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lspeech/patts/ProbabilityDistribution;-><init>(Z)V

    sput-object v0, Lspeech/patts/ProbabilityDistribution;->defaultInstance:Lspeech/patts/ProbabilityDistribution;

    .line 766
    invoke-static {}, Lspeech/patts/DummyNameThatNoOneEverSees;->internalForceInit()V

    .line 767
    sget-object v0, Lspeech/patts/ProbabilityDistribution;->defaultInstance:Lspeech/patts/ProbabilityDistribution;

    invoke-direct {v0}, Lspeech/patts/ProbabilityDistribution;->initFields()V

    .line 768
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/ProbabilityDistribution;->voicingPosterior_:F

    .line 67
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;

    .line 79
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;

    .line 91
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;

    .line 133
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/ProbabilityDistribution;->memoizedSerializedSize:I

    .line 9
    invoke-direct {p0}, Lspeech/patts/ProbabilityDistribution;->initFields()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lspeech/patts/ProbabilityDistribution$1;)V
    .locals 0
    .param p1, "x0"    # Lspeech/patts/ProbabilityDistribution$1;

    .prologue
    .line 5
    invoke-direct {p0}, Lspeech/patts/ProbabilityDistribution;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lspeech/patts/ProbabilityDistribution;->voicingPosterior_:F

    .line 67
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;

    .line 79
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;

    .line 91
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;

    .line 133
    const/4 v0, -0x1

    iput v0, p0, Lspeech/patts/ProbabilityDistribution;->memoizedSerializedSize:I

    .line 11
    return-void
.end method

.method static synthetic access$1002(Lspeech/patts/ProbabilityDistribution;F)F
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;
    .param p1, "x1"    # F

    .prologue
    .line 5
    iput p1, p0, Lspeech/patts/ProbabilityDistribution;->voicingPosterior_:F

    return p1
.end method

.method static synthetic access$300(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$800(Lspeech/patts/ProbabilityDistribution;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;

    .prologue
    .line 5
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$802(Lspeech/patts/ProbabilityDistribution;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5
    iput-object p1, p0, Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$902(Lspeech/patts/ProbabilityDistribution;Z)Z
    .locals 0
    .param p0, "x0"    # Lspeech/patts/ProbabilityDistribution;
    .param p1, "x1"    # Z

    .prologue
    .line 5
    iput-boolean p1, p0, Lspeech/patts/ProbabilityDistribution;->hasVoicingPosterior:Z

    return p1
.end method

.method public static getDefaultInstance()Lspeech/patts/ProbabilityDistribution;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lspeech/patts/ProbabilityDistribution;->defaultInstance:Lspeech/patts/ProbabilityDistribution;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public static newBuilder()Lspeech/patts/ProbabilityDistribution$Builder;
    .locals 1

    .prologue
    .line 238
    # invokes: Lspeech/patts/ProbabilityDistribution$Builder;->create()Lspeech/patts/ProbabilityDistribution$Builder;
    invoke-static {}, Lspeech/patts/ProbabilityDistribution$Builder;->access$100()Lspeech/patts/ProbabilityDistribution$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lspeech/patts/ProbabilityDistribution;)Lspeech/patts/ProbabilityDistribution$Builder;
    .locals 1
    .param p0, "prototype"    # Lspeech/patts/ProbabilityDistribution;

    .prologue
    .line 241
    invoke-static {}, Lspeech/patts/ProbabilityDistribution;->newBuilder()Lspeech/patts/ProbabilityDistribution$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lspeech/patts/ProbabilityDistribution$Builder;->mergeFrom(Lspeech/patts/ProbabilityDistribution;)Lspeech/patts/ProbabilityDistribution$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAmplitudeGaussiansList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Gaussian;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution;->amplitudeGaussians_:Ljava/util/List;

    return-object v0
.end method

.method public getAperiodicityGaussiansList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Gaussian;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution;->aperiodicityGaussians_:Ljava/util/List;

    return-object v0
.end method

.method public getBandAperiodicityGaussiansList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Gaussian;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution;->bandAperiodicityGaussians_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getDefaultInstanceForType()Lspeech/patts/ProbabilityDistribution;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lspeech/patts/ProbabilityDistribution;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lspeech/patts/ProbabilityDistribution;->defaultInstance:Lspeech/patts/ProbabilityDistribution;

    return-object v0
.end method

.method public getLineSpectralPairGaussiansList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Gaussian;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution;->lineSpectralPairGaussians_:Ljava/util/List;

    return-object v0
.end method

.method public getLogF0GaussiansList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Gaussian;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution;->logF0Gaussians_:Ljava/util/List;

    return-object v0
.end method

.method public getMelCepstrumGaussiansList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/patts/Gaussian;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lspeech/patts/ProbabilityDistribution;->melCepstrumGaussians_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 135
    iget v2, p0, Lspeech/patts/ProbabilityDistribution;->memoizedSerializedSize:I

    .line 136
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 168
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 138
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 139
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getMelCepstrumGaussiansList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Gaussian;

    .line 140
    .local v0, "element":Lspeech/patts/Gaussian;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 142
    goto :goto_1

    .line 143
    .end local v0    # "element":Lspeech/patts/Gaussian;
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getLineSpectralPairGaussiansList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Gaussian;

    .line 144
    .restart local v0    # "element":Lspeech/patts/Gaussian;
    const/4 v4, 0x2

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 146
    goto :goto_2

    .line 147
    .end local v0    # "element":Lspeech/patts/Gaussian;
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getBandAperiodicityGaussiansList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Gaussian;

    .line 148
    .restart local v0    # "element":Lspeech/patts/Gaussian;
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 150
    goto :goto_3

    .line 151
    .end local v0    # "element":Lspeech/patts/Gaussian;
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getLogF0GaussiansList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Gaussian;

    .line 152
    .restart local v0    # "element":Lspeech/patts/Gaussian;
    const/4 v4, 0x4

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 154
    goto :goto_4

    .line 155
    .end local v0    # "element":Lspeech/patts/Gaussian;
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->hasVoicingPosterior()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 156
    const/4 v4, 0x5

    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getVoicingPosterior()F

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v4

    add-int/2addr v2, v4

    .line 159
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getAmplitudeGaussiansList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Gaussian;

    .line 160
    .restart local v0    # "element":Lspeech/patts/Gaussian;
    const/4 v4, 0x6

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 162
    goto :goto_5

    .line 163
    .end local v0    # "element":Lspeech/patts/Gaussian;
    :cond_6
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getAperiodicityGaussiansList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Gaussian;

    .line 164
    .restart local v0    # "element":Lspeech/patts/Gaussian;
    const/4 v4, 0x7

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 166
    goto :goto_6

    .line 167
    .end local v0    # "element":Lspeech/patts/Gaussian;
    :cond_7
    iput v2, p0, Lspeech/patts/ProbabilityDistribution;->memoizedSerializedSize:I

    move v3, v2

    .line 168
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto/16 :goto_0
.end method

.method public getVoicingPosterior()F
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lspeech/patts/ProbabilityDistribution;->voicingPosterior_:F

    return v0
.end method

.method public hasVoicingPosterior()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lspeech/patts/ProbabilityDistribution;->hasVoicingPosterior:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->toBuilder()Lspeech/patts/ProbabilityDistribution$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lspeech/patts/ProbabilityDistribution$Builder;
    .locals 1

    .prologue
    .line 243
    invoke-static {p0}, Lspeech/patts/ProbabilityDistribution;->newBuilder(Lspeech/patts/ProbabilityDistribution;)Lspeech/patts/ProbabilityDistribution$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getSerializedSize()I

    .line 110
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getMelCepstrumGaussiansList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Gaussian;

    .line 111
    .local v0, "element":Lspeech/patts/Gaussian;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 113
    .end local v0    # "element":Lspeech/patts/Gaussian;
    :cond_0
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getLineSpectralPairGaussiansList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Gaussian;

    .line 114
    .restart local v0    # "element":Lspeech/patts/Gaussian;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 116
    .end local v0    # "element":Lspeech/patts/Gaussian;
    :cond_1
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getBandAperiodicityGaussiansList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Gaussian;

    .line 117
    .restart local v0    # "element":Lspeech/patts/Gaussian;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    .line 119
    .end local v0    # "element":Lspeech/patts/Gaussian;
    :cond_2
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getLogF0GaussiansList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Gaussian;

    .line 120
    .restart local v0    # "element":Lspeech/patts/Gaussian;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    .line 122
    .end local v0    # "element":Lspeech/patts/Gaussian;
    :cond_3
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->hasVoicingPosterior()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 123
    const/4 v2, 0x5

    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getVoicingPosterior()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 125
    :cond_4
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getAmplitudeGaussiansList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Gaussian;

    .line 126
    .restart local v0    # "element":Lspeech/patts/Gaussian;
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_4

    .line 128
    .end local v0    # "element":Lspeech/patts/Gaussian;
    :cond_5
    invoke-virtual {p0}, Lspeech/patts/ProbabilityDistribution;->getAperiodicityGaussiansList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/patts/Gaussian;

    .line 129
    .restart local v0    # "element":Lspeech/patts/Gaussian;
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_5

    .line 131
    .end local v0    # "element":Lspeech/patts/Gaussian;
    :cond_6
    return-void
.end method
