.class Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$1;
.super Ljava/lang/Object;
.source "WifiDisplayDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

.field final synthetic val$dlgNum:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;I)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$1;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    iput p2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$1;->val$dlgNum:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 167
    const/4 v2, 0x4

    if-ne p2, v2, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 169
    iget v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$1;->val$dlgNum:I

    packed-switch v2, :pswitch_data_0

    .line 200
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$1;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->dismiss()V

    .line 201
    const-string v1, "WifiDisplayDialogFragment"

    const-string v2, "Back Button is pressed... same process with cancel..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    :goto_1
    return v0

    .line 172
    :pswitch_1
    const-string v1, "WifiDisplayDialogFragment"

    const-string v2, "Cancel is clicked..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$1;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # invokes: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->sendIntentToNative()V
    invoke-static {v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$000(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    goto :goto_0

    .line 177
    :pswitch_2
    const-string v1, "WifiDisplayDialogFragment"

    const-string v2, "Cancel is hooked when restart case..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 181
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$1;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # invokes: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->enableWifiDisplay(Z)V
    invoke-static {v2, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$100(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)V

    goto :goto_0

    .line 189
    :pswitch_4
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$1;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # invokes: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->enableWifiDisplay(Z)V
    invoke-static {v2, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$100(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)V

    goto :goto_0

    .line 192
    :pswitch_5
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$1;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # invokes: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->enableWifiDisplay(Z)V
    invoke-static {v2, v0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$100(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)V

    .line 193
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$1;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # setter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsHome:Z
    invoke-static {v2, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$202(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)Z

    goto :goto_0

    .line 196
    :pswitch_6
    const-string v1, "wlan.wfd.autoconnect"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 205
    goto :goto_1

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_6
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
