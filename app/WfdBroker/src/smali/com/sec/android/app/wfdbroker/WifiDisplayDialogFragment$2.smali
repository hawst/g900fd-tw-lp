.class Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$2;
.super Ljava/lang/Object;
.source "WifiDisplayDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->createWelcomeDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

.field final synthetic val$not_show_again:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$2;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    iput-object p2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$2;->val$not_show_again:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$2;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$2;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # getter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$300(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "wfd_broker_do_not_show_again"

    const-string v3, "wfdbrokerkey"

    iget-object v4, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$2;->val$not_show_again:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->writePreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$2;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsContinue:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$402(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)Z

    .line 282
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 283
    return-void
.end method
