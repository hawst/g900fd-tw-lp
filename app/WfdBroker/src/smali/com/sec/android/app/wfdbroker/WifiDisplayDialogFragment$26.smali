.class Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$26;
.super Ljava/lang/Object;
.source "WifiDisplayDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->createWfdDialog(I)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V
    .locals 0

    .prologue
    .line 709
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$26;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 713
    const-string v0, "WifiDisplayDialogFragment"

    const-string v1, "OK is clicked.."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$26;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # invokes: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->isWfdConnected()Z
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$1200(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 715
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$26;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->enableWifiDisplay(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$100(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)V

    .line 717
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 718
    return-void
.end method
