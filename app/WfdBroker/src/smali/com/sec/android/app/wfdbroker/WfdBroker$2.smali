.class Lcom/sec/android/app/wfdbroker/WfdBroker$2;
.super Landroid/database/ContentObserver;
.source "WfdBroker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wfdbroker/WfdBroker;->showScanningPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wfdbroker/WfdBroker;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 424
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$2;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 427
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$2;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "wifi_display_on"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$2;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$200(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_0

    .line 429
    const-string v0, "WfdBroker"

    const-string v1, "onChange WIFI_DISPLAY_ON false : finish()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$2;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    .line 432
    :cond_0
    return-void
.end method
