.class Lcom/sec/android/app/wfdbroker/WfdBroker$4;
.super Ljava/lang/Object;
.source "WfdBroker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wfdbroker/WfdBroker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wfdbroker/WfdBroker;)V
    .locals 0

    .prologue
    .line 491
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1100(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 495
    const-string v0, "WfdBroker"

    const-string v1, "scanning timeout"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;
    invoke-static {v1}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$900(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/database/ContentObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;
    invoke-static {v0, v3}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$902(Lcom/sec/android/app/wfdbroker/WfdBroker;Landroid/database/ContentObserver;)Landroid/database/ContentObserver;

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanInProgress:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$502(Lcom/sec/android/app/wfdbroker/WfdBroker;Z)Z

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$400(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/hardware/display/DisplayManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->stopScanWifiDisplays()V

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # invokes: Lcom/sec/android/app/wfdbroker/WfdBroker;->enableWifiDisplay(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1200(Lcom/sec/android/app/wfdbroker/WfdBroker;Z)V

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mNfcReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$800(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/wfdbroker/WfdBroker;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mNfcReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v0, v3}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$802(Lcom/sec/android/app/wfdbroker/WfdBroker;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    .line 506
    :goto_0
    return-void

    .line 505
    :cond_0
    const-string v0, "WfdBroker"

    const-string v1, "scanning timeout but activity is not running"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
