.class public Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;
.super Landroid/app/DialogFragment;
.source "WifiDisplayDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$Listener;
    }
.end annotation


# static fields
.field private static final CHINA_WLAN:Z

.field private static final DIALOG_AUTO_CONNECT:I = 0xc

.field private static final DIALOG_DISCONNECT_NO_BUSY:I = 0x18

.field private static final DIALOG_DONGLE_UPDATE:I = 0x17

.field private static final DIALOG_ERROR_CONNECTION_FAILED:I = 0xf

.field private static final DIALOG_ERROR_CONNECT_DISCON:I = 0x10

.field private static final DIALOG_ERROR_CONNECT_FAILED:I = 0xd

.field private static final DIALOG_ERROR_NETWORK_FAILED:I = 0x25

.field private static final DIALOG_HDMI_BUSY:I = 0x13

.field private static final DIALOG_HDMI_CONNECTED:I = 0x22

.field private static final DIALOG_HOTSPOT_BUSY:I = 0x14

.field private static final DIALOG_INVALID_HDCP_KEY:I = 0x1c

.field private static final DIALOG_LIMITED_PLAYBACK:I = 0x1f

.field private static final DIALOG_LIMITED_RECORDING:I = 0x20

.field private static final DIALOG_OXYGEN_ENABLED:I = 0x26

.field private static final DIALOG_P2P_BUSY:I = 0x11

.field private static final DIALOG_POWER_SAVING_ENABLED:I = 0x1d

.field private static final DIALOG_POWER_SAVING_MODE_ENABLED:I = 0x21

.field private static final DIALOG_RESTART:I = 0x12

.field private static final DIALOG_SCANNING_BYNFC:I = 0x23

.field private static final DIALOG_SIDE_SYNC_RUNNING:I = 0x1e

.field private static final DIALOG_TAG_COMPLETE:I = 0x1b

.field private static final DIALOG_TAG_TOUCH:I = 0x1a

.field private static final DIALOG_TERMINATE:I = 0xe

.field private static final DIALOG_WELCOME:I = 0x24

.field private static final DO_NOT_SHOW_AGAIN:Ljava/lang/String; = "wfd_broker_do_not_show_again"

.field private static final KEY:Ljava/lang/String; = "wfdbrokerkey"

.field private static final TAG:Ljava/lang/String; = "WifiDisplayDialogFragment"


# instance fields
.field private mButtonSoundPool:Landroid/media/SoundPool;

.field private mContext:Landroid/content/Context;

.field private mCurrentSoundID:I

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mIsContinue:Z

.field private mIsHome:Z

.field private mListener:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$Listener;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWfdDialogId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "China"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->CHINA_WLAN:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 83
    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    .line 85
    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mButtonSoundPool:Landroid/media/SoundPool;

    .line 86
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mCurrentSoundID:I

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsContinue:Z

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsHome:Z

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->sendIntentToNative()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->enableWifiDisplay(Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->setOxygenOff()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->isSideSyncConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->isWfdConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;
    .param p1, "x1"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->setPowerSavingMode(I)V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsHome:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsContinue:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->setWakeLock()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Landroid/hardware/display/DisplayManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mCurrentSoundID:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mCurrentSoundID:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->removeGroupP2p()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->setHotspotOff()V

    return-void
.end method

.method private createAutoConnectDialog(I)Landroid/app/AlertDialog;
    .locals 7
    .param p1, "resId"    # I

    .prologue
    .line 302
    const-string v4, "WifiDisplayDialogFragment"

    const-string v5, "createAutoConnectDialog"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 305
    .local v0, "autoConnectDialog":Landroid/app/AlertDialog;
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 306
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030001

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 307
    .local v1, "bodyView":Landroid/view/View;
    const v4, 0x7f070002

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 309
    .local v3, "tv":Landroid/widget/TextView;
    if-eqz v3, :cond_0

    .line 310
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(I)V

    .line 313
    :cond_0
    sget-boolean v4, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->CHINA_WLAN:Z

    if-eqz v4, :cond_1

    .line 314
    const v4, 0x7f050027

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 317
    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 319
    new-instance v4, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$4;

    invoke-direct {v4, p0, v0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$4;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 337
    const/4 v4, -0x2

    const/high16 v5, 0x7f050000

    invoke-virtual {p0, v5}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$5;

    invoke-direct {v6, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$5;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v4, v5, v6}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 346
    return-object v0

    .line 316
    :cond_1
    const v4, 0x7f050026

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setTitle(I)V

    goto :goto_0
.end method

.method private createErrorHandlingDialog(I)Landroid/app/AlertDialog;
    .locals 4
    .param p1, "nErrorType"    # I

    .prologue
    .line 360
    const-string v1, "WifiDisplayDialogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createErrorHandlingDialog << "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 362
    .local v0, "errorHandlingDialog":Landroid/app/AlertDialog;
    sget-boolean v1, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->CHINA_WLAN:Z

    if-eqz v1, :cond_1

    .line 363
    const v1, 0x7f050027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 367
    :goto_0
    const/16 v1, 0xd

    if-ne p1, v1, :cond_2

    .line 368
    const v1, 0x7f050015

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 378
    :cond_0
    :goto_1
    const/4 v1, -0x1

    const v2, 0x7f050001

    invoke-virtual {p0, v2}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$6;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 388
    return-object v0

    .line 365
    :cond_1
    const v1, 0x7f050026

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    goto :goto_0

    .line 370
    :cond_2
    const/16 v1, 0x25

    if-ne p1, v1, :cond_3

    .line 371
    const v1, 0x7f05001b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 373
    :cond_3
    const/16 v1, 0x1c

    if-ne p1, v1, :cond_0

    .line 374
    const v1, 0x7f050029

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private createWelcomeDialog()Landroid/app/Dialog;
    .locals 7

    .prologue
    .line 264
    const-string v4, "WifiDisplayDialogFragment"

    const-string v5, "createWelcomeDialog"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 267
    .local v0, "WelcomeDialog":Landroid/app/AlertDialog;
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 268
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030003

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 269
    .local v1, "bodyView":Landroid/view/View;
    const v4, 0x7f070007

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 271
    .local v3, "not_show_again":Landroid/widget/CheckBox;
    sget-boolean v4, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->CHINA_WLAN:Z

    if-eqz v4, :cond_0

    .line 272
    const v4, 0x7f050027

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 275
    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 276
    const/4 v4, -0x3

    const v5, 0x7f050001

    invoke-virtual {p0, v5}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$2;

    invoke-direct {v6, p0, v3}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$2;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v4, v5, v6}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 285
    new-instance v4, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$3;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 298
    return-object v0

    .line 274
    :cond_0
    const v4, 0x7f050026

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setTitle(I)V

    goto :goto_0
.end method

.method private createWfdDialog(I)Landroid/app/AlertDialog;
    .locals 10
    .param p1, "dlgNum"    # I

    .prologue
    const/4 v4, 0x1

    const/high16 v9, 0x7f050000

    const/4 v8, -0x2

    const v7, 0x7f050001

    const/4 v6, -0x1

    .line 393
    const-string v1, "WifiDisplayDialogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createWfdDialog << "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 395
    .local v0, "wfdDialog":Landroid/app/AlertDialog;
    sget-boolean v1, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->CHINA_WLAN:Z

    if-eqz v1, :cond_0

    .line 396
    const v1, 0x7f050027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 399
    :goto_0
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 401
    packed-switch p1, :pswitch_data_0

    .line 767
    :goto_1
    :pswitch_0
    return-object v0

    .line 398
    :cond_0
    const v1, 0x7f050026

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 403
    :pswitch_1
    const v1, 0x7f050015

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 404
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$7;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_1

    .line 416
    :pswitch_2
    const v1, 0x7f050019

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 417
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$8;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$8;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_1

    .line 430
    :pswitch_3
    const v1, 0x7f050029

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 431
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$9;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_1

    .line 444
    :pswitch_4
    const v1, 0x7f050024

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 445
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 493
    invoke-virtual {p0, v9}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$11;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$11;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v8, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_1

    .line 507
    :pswitch_5
    sget-boolean v1, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->CHINA_WLAN:Z

    if-eqz v1, :cond_1

    .line 508
    const v1, 0x7f050012

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 511
    :goto_2
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$12;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$12;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 523
    invoke-virtual {p0, v9}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$13;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$13;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v8, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_1

    .line 510
    :cond_1
    const v1, 0x7f050011

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 536
    :pswitch_6
    sget-boolean v1, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->CHINA_WLAN:Z

    if-eqz v1, :cond_2

    .line 537
    const v1, 0x7f050021

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 540
    :goto_3
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$14;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_1

    .line 539
    :cond_2
    const v1, 0x7f050020

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 552
    :pswitch_7
    const v1, 0x7f050028

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 553
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$15;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$15;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_1

    .line 566
    :pswitch_8
    const v1, 0x7f05000e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 567
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$16;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$16;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 579
    invoke-virtual {p0, v9}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$17;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$17;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v8, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_1

    .line 592
    :pswitch_9
    const v1, 0x7f050010

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 593
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$18;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$18;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 605
    invoke-virtual {p0, v9}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$19;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$19;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v8, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_1

    .line 617
    :pswitch_a
    const v1, 0x7f05002a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 618
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$20;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$20;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 628
    invoke-virtual {p0, v9}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$21;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$21;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v8, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_1

    .line 641
    :pswitch_b
    const v1, 0x7f050025

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 642
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$22;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$22;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_1

    .line 654
    :pswitch_c
    const v1, 0x7f05001a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 655
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$23;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$23;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_1

    .line 667
    :pswitch_d
    const v1, 0x7f050022

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050023

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 668
    const v1, 0x7f050023

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 669
    invoke-virtual {p0, v9}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$24;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$24;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v8, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 680
    const v1, 0x7f05001d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$25;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$25;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_1

    .line 707
    :pswitch_e
    const v1, 0x7f050018

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050008

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 708
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$26;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$26;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_1

    .line 722
    :pswitch_f
    const v1, 0x7f05001e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050008

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 723
    invoke-virtual {p0, v7}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$27;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$27;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_1

    .line 737
    :pswitch_10
    const v1, 0x7f05002c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 738
    const v1, 0x7f05000a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 739
    const v1, 0x7f050002

    invoke-virtual {p0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$28;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$28;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v6, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 754
    invoke-virtual {p0, v9}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$29;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$29;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v0, v8, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_1

    .line 401
    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method private enableWifiDisplay(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 832
    const-string v0, "WifiDisplayDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enableWifiDisplay enable="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 833
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "wifi_display_on"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 835
    return-void

    .line 833
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSideSyncConnected()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 845
    const/4 v0, 0x0

    .line 846
    .local v0, "isSideSync":I
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sidesync_source_connect"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 847
    if-nez v0, :cond_0

    .line 850
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isWfdConnected()Z
    .locals 2

    .prologue
    .line 838
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 839
    const/4 v0, 0x1

    .line 841
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newDialog(I)Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;
    .locals 5
    .param p0, "dlgNum"    # I

    .prologue
    .line 100
    new-instance v1, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;-><init>()V

    .line 101
    .local v1, "wfdDialogFragment":Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 102
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "dlgnum"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 103
    const-string v2, "WifiDisplayDialogFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WifiDisplayDialogFragment dlgNum = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {v1, v0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 105
    return-object v1
.end method

.method private removeGroupP2p()V
    .locals 5

    .prologue
    .line 780
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    const-string v3, "wifip2p"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pManager;

    .line 782
    .local v1, "wifiP2pManager":Landroid/net/wifi/p2p/WifiP2pManager;
    if-eqz v1, :cond_0

    .line 783
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    .line 785
    .local v0, "p2pChannel":Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$30;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$30;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 795
    .end local v0    # "p2pChannel":Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    :cond_0
    return-void
.end method

.method private sendIntentToNative()V
    .locals 3

    .prologue
    .line 772
    const-string v1, "WifiDisplayDialogFragment"

    const-string v2, "sendIntentToNative"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.WIFI_DISPLAY_UPDATE_INPUT_FROM_APP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 775
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 776
    return-void
.end method

.method private setHotspotOff()V
    .locals 3

    .prologue
    .line 798
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 800
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    if-eqz v0, :cond_0

    .line 801
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    .line 803
    :cond_0
    return-void
.end method

.method private setOxygenOff()V
    .locals 3

    .prologue
    .line 806
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 808
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    if-eqz v0, :cond_0

    .line 809
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiIBSSEnabled(Z)Z

    .line 811
    :cond_0
    return-void
.end method

.method private setPowerSavingMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 814
    invoke-static {}, Lcom/sec/android/emergencymode/EmergencyManager;->supportUltraPowerSavingMode()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 815
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "powersaving_switch"

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v3

    invoke-static {v1, v2, p1, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 816
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.POWERSAVINGMODE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 817
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 819
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string v1, "android.settings.POWERSAVING_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 820
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "changed"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 821
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 829
    :goto_0
    return-void

    .line 824
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "psm_switch"

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v3

    invoke-static {v1, v2, p1, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 825
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.POWERSAVING_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 826
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "changed"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 827
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0
.end method

.method private setWakeLock()V
    .locals 4

    .prologue
    .line 350
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 351
    .local v0, "PowerMgr":Landroid/os/PowerManager;
    const/16 v1, 0xa

    const-string v2, "WifiDisplayDialogFragment"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 353
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/32 v2, 0x15f90

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 354
    const-string v1, "WifiDisplayDialogFragment"

    const-string v2, "WfdPicker WakeLock...this will be released automatically 90 secs after..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 215
    const-string v0, "WifiDisplayDialogFragment"

    const-string v1, "onCancel"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->dismiss()V

    .line 217
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 114
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsHome:Z

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 118
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "dlgnum"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 123
    .local v0, "dlgNum":I
    iput v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mWfdDialogId:I

    .line 124
    const-string v2, "WifiDisplayDialogFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreateDialog mWfdDialogId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mWfdDialogId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const/4 v1, 0x0

    .line 126
    .local v1, "retDialog":Landroid/app/Dialog;
    packed-switch v0, :pswitch_data_0

    .line 159
    :pswitch_0
    const-string v2, "WifiDisplayDialogFragment"

    const-string v3, "dialog type error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const/4 v2, 0x0

    .line 209
    :goto_0
    return-object v2

    .line 128
    :pswitch_1
    const v2, 0x7f050030

    invoke-direct {p0, v2}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->createAutoConnectDialog(I)Landroid/app/AlertDialog;

    move-result-object v1

    .line 163
    :goto_1
    new-instance v2, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$1;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;I)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    move-object v2, v1

    .line 209
    goto :goto_0

    .line 131
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->createErrorHandlingDialog(I)Landroid/app/AlertDialog;

    move-result-object v1

    .line 132
    goto :goto_1

    .line 134
    :pswitch_3
    invoke-direct {p0, v0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->createErrorHandlingDialog(I)Landroid/app/AlertDialog;

    move-result-object v1

    .line 135
    goto :goto_1

    .line 137
    :pswitch_4
    const v2, 0x7f050006

    invoke-direct {p0, v2}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->createAutoConnectDialog(I)Landroid/app/AlertDialog;

    move-result-object v1

    .line 138
    goto :goto_1

    .line 140
    :pswitch_5
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->createWelcomeDialog()Landroid/app/Dialog;

    move-result-object v2

    goto :goto_0

    .line 156
    :pswitch_6
    invoke-direct {p0, v0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->createWfdDialog(I)Landroid/app/AlertDialog;

    move-result-object v1

    .line 157
    goto :goto_1

    .line 126
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_1
        :pswitch_2
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public onDetach()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 242
    const-string v2, "WifiDisplayDialogFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDetach mWfdDialogId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mWfdDialogId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    iget v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mWfdDialogId:I

    const/16 v3, 0xe

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->isWfdConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 246
    const/4 v0, 0x0

    .line 247
    .local v0, "isScreenLocked":Z
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    .line 248
    .local v1, "keyguardManager":Landroid/app/KeyguardManager;
    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    .line 249
    if-nez v0, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsHome:Z

    if-ne v2, v5, :cond_1

    .line 250
    :cond_0
    invoke-direct {p0, v5}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->enableWifiDisplay(Z)V

    .line 252
    .end local v0    # "isScreenLocked":Z
    .end local v1    # "keyguardManager":Landroid/app/KeyguardManager;
    :cond_1
    invoke-super {p0}, Landroid/app/DialogFragment;->onDetach()V

    .line 253
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 228
    const-string v0, "WifiDisplayDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDismiss - mWfdDialogId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mWfdDialogId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget-boolean v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsContinue:Z

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mListener:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$Listener;

    invoke-interface {v0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$Listener;->ok()V

    .line 237
    :goto_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 238
    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mListener:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$Listener;

    invoke-interface {v0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$Listener;->cancel()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 221
    const-string v0, "WifiDisplayDialogFragment"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->dismissAllowingStateLoss()V

    .line 223
    invoke-super {p0}, Landroid/app/DialogFragment;->onStop()V

    .line 224
    return-void
.end method

.method setContinue(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 854
    iput-boolean p1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsContinue:Z

    .line 855
    return-void
.end method

.method public setListener(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$Listener;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mListener:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$Listener;

    .line 110
    return-void
.end method

.method public writePreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefName"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "value"    # Z

    .prologue
    .line 257
    const/4 v2, 0x0

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 258
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 259
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p3, p4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 260
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 261
    return-void
.end method
