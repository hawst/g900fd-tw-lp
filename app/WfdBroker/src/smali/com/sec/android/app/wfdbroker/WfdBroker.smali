.class public Lcom/sec/android/app/wfdbroker/WfdBroker;
.super Landroid/app/Activity;
.source "WfdBroker.java"


# static fields
.field private static final ACION_DP_HELP:Ljava/lang/String; = "com.samsung.wfd.DP_HELP"

.field private static final ACTION_NFC_CONNECT:Ljava/lang/String; = "com.samsung.wfd.NFC_CONNECT"

.field private static final ACTION_WRITE_NFC:Ljava/lang/String; = "com.samsung.wfd.WRITE_NFC"

.field private static final BASE_WIFI_P2P_MANAGER:I = 0x22000

.field private static final DEBUG:Z = true

.field private static final DIALOG_AUTO_CONNECT:I = 0xc

.field private static final DIALOG_DISCONNECT_NO_BUSY:I = 0x18

.field private static final DIALOG_DONGLE_UPDATE:I = 0x17

.field private static final DIALOG_ERROR_CONNECTION_FAILED:I = 0xf

.field private static final DIALOG_ERROR_CONNECT_DISCON:I = 0x10

.field private static final DIALOG_ERROR_CONNECT_FAILED:I = 0xd

.field private static final DIALOG_HDMI_BUSY:I = 0x13

.field private static final DIALOG_HOTSPOT_BUSY:I = 0x14

.field private static final DIALOG_INVALID_HDCP_KEY:I = 0x1c

.field private static final DIALOG_LIMITED_PLAYBACK:I = 0x1f

.field private static final DIALOG_LIMITED_RECORDING:I = 0x20

.field private static final DIALOG_NUM_BASE:I = 0x22000

.field private static final DIALOG_OXYGEN_ENABLED:I = 0x26

.field private static final DIALOG_P2P_BUSY:I = 0x11

.field private static final DIALOG_POWER_SAVING_ENABLED:I = 0x1d

.field private static final DIALOG_POWER_SAVING_MODE_ENABLED:I = 0x21

.field private static final DIALOG_RESTART:I = 0x12

.field private static final DIALOG_SCANNING_BYNFC:I = 0x23

.field private static final DIALOG_SIDE_SYNC_RUNNING:I = 0x1e

.field private static final DIALOG_TAG_COMPLETE:I = 0x1b

.field private static final DIALOG_TAG_TOUCH:I = 0x1a

.field private static final DIALOG_TERMINATE:I = 0xe

.field private static final DIALOG_WELCOME:I = 0x24

.field private static final DO_NOT_SHOW_AGAIN:Ljava/lang/String; = "wfd_broker_do_not_show_again"

.field private static final EXCEPTION_CASE_GROUP_PLAY:I = 0x7

.field private static final KEY:Ljava/lang/String; = "wfdbrokerkey"

.field private static final POPUP_CAUSE_OXYGEN_NETWORK_ENABLED:I = 0x22090

.field private static final SCANNING_TIMEOUT_SECONDS:I = 0x14

.field private static final TAG:Ljava/lang/String; = "WfdBroker"

.field private static mBrokerDetachRequested:Z

.field private static mConnectDifDev:Z

.field private static mLastConnectedDeviceAddr:Ljava/lang/String;

.field private static nfcMsgs:[Landroid/os/Parcelable;

.field private static staticConnectingDevAddrFromNfc:Ljava/lang/String;


# instance fields
.field private final ACTION_TAG_COMPLETE:Ljava/lang/String;

.field private final ACTION_TAG_TOUCH:Ljava/lang/String;

.field private final ALLSHARE_ACTION_STOP:Ljava/lang/String;

.field private final ALLSHARE_MIME_TYPE_NAME:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDebugEnable:Z

.field private mDeviceAddr:Ljava/lang/String;

.field private mDeviceName:Ljava/lang/String;

.field private mDialogCnt:I

.field private mDialogFlag:I

.field private mDialogNum:I

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mHandler:Landroid/os/Handler;

.field private mIsContinue:Z

.field private mIsFWUpgradeOn:Z

.field private mLaunchedFromNfc:Z

.field private mNfcReceiver:Landroid/content/BroadcastReceiver;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mScanInProgress:Z

.field private final mScanningTimeout:Ljava/lang/Runnable;

.field private mSettingsObserver:Landroid/database/ContentObserver;

.field private mUpdateURL:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mLastConnectedDeviceAddr:Ljava/lang/String;

    .line 118
    sput-boolean v1, Lcom/sec/android/app/wfdbroker/WfdBroker;->mConnectDifDev:Z

    .line 119
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/wfdbroker/WfdBroker;->nfcMsgs:[Landroid/os/Parcelable;

    .line 122
    sput-boolean v1, Lcom/sec/android/app/wfdbroker/WfdBroker;->mBrokerDetachRequested:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 68
    iput v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I

    .line 69
    iput v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogCnt:I

    .line 71
    iput-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mUpdateURL:Ljava/lang/String;

    .line 104
    iput v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogFlag:I

    .line 106
    const-string v0, "application/vnd.sec.wfd.nfc"

    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->ALLSHARE_MIME_TYPE_NAME:Ljava/lang/String;

    .line 109
    const-string v0, "com.sec.android.allshare.intent.action.CAST_STOP"

    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->ALLSHARE_ACTION_STOP:Ljava/lang/String;

    .line 111
    const-string v0, "com.sec.android.allshare.intent.action.TAG_TOUCH"

    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->ACTION_TAG_TOUCH:Ljava/lang/String;

    .line 112
    const-string v0, "android.nfc.action.NDEF_DISCOVERED"

    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->ACTION_TAG_COMPLETE:Ljava/lang/String;

    .line 121
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDeviceName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDeviceAddr:Ljava/lang/String;

    .line 125
    iput-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 127
    iput-boolean v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDebugEnable:Z

    .line 128
    iput-boolean v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mLaunchedFromNfc:Z

    .line 129
    iput-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mHandler:Landroid/os/Handler;

    .line 130
    iput-boolean v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanInProgress:Z

    .line 131
    iput-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 132
    iput-boolean v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mIsFWUpgradeOn:Z

    .line 135
    iput-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mContext:Landroid/content/Context;

    .line 136
    iput-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mNfcReceiver:Landroid/content/BroadcastReceiver;

    .line 137
    new-instance v0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wfdbroker/WfdBroker$1;-><init>(Lcom/sec/android/app/wfdbroker/WfdBroker;)V

    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 491
    new-instance v0, Lcom/sec/android/app/wfdbroker/WfdBroker$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/wfdbroker/WfdBroker$4;-><init>(Lcom/sec/android/app/wfdbroker/WfdBroker;)V

    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanningTimeout:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/wfdbroker/WfdBroker;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mIsFWUpgradeOn:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/wfdbroker/WfdBroker;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mIsFWUpgradeOn:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/wfdbroker/WfdBroker;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->removeAnyFragmentDialog()V

    return-void
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/android/app/wfdbroker/WfdBroker;->staticConnectingDevAddrFromNfc:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/wfdbroker/WfdBroker;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/android/app/wfdbroker/WfdBroker;->enableWifiDisplay(Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/wfdbroker/WfdBroker;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogCnt:I

    return v0
.end method

.method static synthetic access$1310(Lcom/sec/android/app/wfdbroker/WfdBroker;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogCnt:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogCnt:I

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/wfdbroker/WfdBroker;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mIsContinue:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/wfdbroker/WfdBroker;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogFlag:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/wfdbroker/WfdBroker;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->isWfdConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/wfdbroker/WfdBroker;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/wfdbroker/WfdBroker;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;
    .param p1, "x1"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I

    return p1
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 62
    sget-boolean v0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mBrokerDetachRequested:Z

    return v0
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 62
    sput-boolean p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mBrokerDetachRequested:Z

    return p0
.end method

.method static synthetic access$400(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/hardware/display/DisplayManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/wfdbroker/WfdBroker;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanInProgress:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/wfdbroker/WfdBroker;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanInProgress:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/wfdbroker/WfdBroker;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanningTimeout:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mNfcReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/wfdbroker/WfdBroker;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;
    .param p1, "x1"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mNfcReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/database/ContentObserver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/wfdbroker/WfdBroker;Landroid/database/ContentObserver;)Landroid/database/ContentObserver;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/wfdbroker/WfdBroker;
    .param p1, "x1"    # Landroid/database/ContentObserver;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;

    return-object p1
.end method

.method private checkDebugLogEnable()V
    .locals 2

    .prologue
    .line 207
    const-string v0, "secmm.wfds.wfds"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    const-string v0, "WfdBroker"

    const-string v1, "WifiDisplaySettings debug log enabled."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDebugEnable:Z

    .line 214
    :goto_0
    return-void

    .line 213
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDebugEnable:Z

    goto :goto_0
.end method

.method private enableWifiDisplay(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 685
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "wifi_display_on"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 687
    return-void

    .line 685
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWfdConnected()Z
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 511
    const/4 v0, 0x1

    .line 513
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeAnyFragmentDialog()V
    .locals 4

    .prologue
    .line 690
    const-string v2, "WfdBroker"

    const-string v3, "removeAnyFragmentDialog"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 692
    .local v0, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 693
    .local v1, "prev":Landroid/app/Fragment;
    if-eqz v1, :cond_0

    .line 694
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 696
    :cond_0
    return-void
.end method

.method private sendIntentToDongleUpdater()V
    .locals 4

    .prologue
    .line 663
    const-string v1, "WfdBroker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendIntentToDongleUpdater >> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mUpdateURL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.wfd.LAUNCH_WFD_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 666
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 667
    const-string v1, "url"

    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mUpdateURL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 668
    invoke-virtual {p0, v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->sendBroadcast(Landroid/content/Intent;)V

    .line 669
    return-void
.end method


# virtual methods
.method getNdefMessages(Landroid/content/Intent;)[Landroid/nfc/NdefMessage;
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 518
    const/4 v3, 0x0

    .line 520
    .local v3, "msgs":[Landroid/nfc/NdefMessage;
    const-string v6, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    .line 521
    .local v4, "rawMsgs":[Landroid/os/Parcelable;
    if-eqz v4, :cond_0

    .line 522
    array-length v6, v4

    new-array v3, v6, [Landroid/nfc/NdefMessage;

    .line 523
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v4

    if-ge v1, v6, :cond_1

    .line 524
    aget-object v6, v4, v1

    check-cast v6, Landroid/nfc/NdefMessage;

    aput-object v6, v3, v1

    .line 523
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 528
    .end local v1    # "i":I
    :cond_0
    new-array v0, v7, [B

    .line 529
    .local v0, "empty":[B
    new-instance v5, Landroid/nfc/NdefRecord;

    const/4 v6, 0x5

    invoke-direct {v5, v6, v0, v0, v0}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    .line 530
    .local v5, "record":Landroid/nfc/NdefRecord;
    new-instance v2, Landroid/nfc/NdefMessage;

    new-array v6, v8, [Landroid/nfc/NdefRecord;

    aput-object v5, v6, v7

    invoke-direct {v2, v6}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    .line 533
    .local v2, "msg":Landroid/nfc/NdefMessage;
    new-array v3, v8, [Landroid/nfc/NdefMessage;

    .end local v3    # "msgs":[Landroid/nfc/NdefMessage;
    aput-object v2, v3, v7

    .line 537
    .end local v0    # "empty":[B
    .end local v2    # "msg":Landroid/nfc/NdefMessage;
    .end local v5    # "record":Landroid/nfc/NdefRecord;
    .restart local v3    # "msgs":[Landroid/nfc/NdefMessage;
    :cond_1
    return-object v3
.end method

.method public getdataPreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefName"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 415
    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 416
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0, p3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 417
    .local v1, "value":Z
    return v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 218
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 219
    const-string v11, "WfdBroker"

    const-string v12, "onCreate"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    iput-object p0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mContext:Landroid/content/Context;

    .line 224
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->checkDebugLogEnable()V

    .line 226
    new-instance v11, Landroid/os/Handler;

    invoke-direct {v11}, Landroid/os/Handler;-><init>()V

    iput-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mHandler:Landroid/os/Handler;

    .line 227
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 228
    .local v3, "intentFilter":Landroid/content/IntentFilter;
    const-string v11, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v3, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 229
    const-string v11, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v3, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 230
    const-string v11, "android.intent.action.WIFI_DISPLAY_URL_FROM_NATIVE"

    invoke-virtual {v3, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 231
    const-string v11, "DisplayManager.POPUP_CAUSE_INVALID_HDCP_KEY"

    invoke-virtual {v3, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 232
    const-string v11, "android.intent.action.DETACH_WFD_BROKER"

    invoke-virtual {v3, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 233
    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v11, v3}, Lcom/sec/android/app/wfdbroker/WfdBroker;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 235
    const/4 v11, 0x0

    iput v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogFlag:I

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const/high16 v12, 0x7f060000

    invoke-virtual {v11, v12}, Landroid/content/Context;->setTheme(I)V

    .line 239
    const/high16 v11, 0x7f030000

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->setContentView(I)V

    .line 240
    const-string v11, "display"

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/hardware/display/DisplayManager;

    iput-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 241
    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-nez v11, :cond_0

    .line 242
    const-string v11, "WfdBroker"

    const-string v12, "mDisplayManager is null! the activity will be destroyed"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    .line 246
    :cond_0
    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v11}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v9

    .line 248
    .local v9, "status":Landroid/hardware/display/WifiDisplayStatus;
    if-eqz v9, :cond_1

    const/4 v11, 0x2

    invoke-virtual {v9}, Landroid/hardware/display/WifiDisplayStatus;->getFeatureState()I

    move-result v12

    if-ne v11, v12, :cond_1

    invoke-virtual {v9}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v11

    const/4 v12, 0x3

    if-ne v11, v12, :cond_1

    .line 250
    const-string v11, "WfdBroker"

    const-string v12, "feature state is off and active display is already disconnecting"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    .line 254
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->isWfdConnected()Z

    move-result v10

    .line 256
    .local v10, "wfdConnected":Z
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getIntent()Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 258
    .local v1, "action":Ljava/lang/String;
    const-string v11, "com.samsung.wfd.LAUNCH_WFD_POPUP"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string v12, "cause"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    iput v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I

    .line 261
    const-string v11, "WfdBroker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mDialogNum:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I

    const v14, 0x22000

    sub-int/2addr v13, v14

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    iget v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I

    packed-switch v11, :pswitch_data_0

    .line 411
    :cond_2
    :goto_0
    :pswitch_0
    return-void

    .line 266
    :pswitch_1
    const/16 v11, 0xe

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto :goto_0

    .line 269
    :pswitch_2
    const/16 v11, 0x10

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto :goto_0

    .line 272
    :pswitch_3
    const/16 v11, 0x11

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto :goto_0

    .line 275
    :pswitch_4
    const/16 v11, 0x26

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto :goto_0

    .line 278
    :pswitch_5
    const/16 v11, 0x12

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto :goto_0

    .line 281
    :pswitch_6
    const/16 v11, 0x13

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto :goto_0

    .line 284
    :pswitch_7
    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v11}, Landroid/hardware/display/DisplayManager;->checkExceptionalCase()I

    move-result v11

    const/4 v12, 0x7

    if-ne v11, v12, :cond_3

    .line 285
    const-string v11, "WfdBroker"

    const-string v12, "group play is running!"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    goto :goto_0

    .line 289
    :cond_3
    const/16 v11, 0x14

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto :goto_0

    .line 293
    :pswitch_8
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string v12, "url"

    invoke-virtual {v11, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mUpdateURL:Ljava/lang/String;

    .line 294
    const-string v11, "WfdBroker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "update URL:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mUpdateURL:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    const/16 v11, 0x17

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto :goto_0

    .line 298
    :pswitch_9
    const/16 v11, 0x1d

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto :goto_0

    .line 301
    :pswitch_a
    const/16 v11, 0x1e

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto :goto_0

    .line 304
    :pswitch_b
    const/16 v11, 0x1f

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto :goto_0

    .line 307
    :pswitch_c
    const/16 v11, 0x20

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto/16 :goto_0

    .line 310
    :pswitch_d
    const/16 v11, 0x21

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto/16 :goto_0

    .line 313
    :pswitch_e
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f050016

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v12

    const/4 v13, 0x1

    invoke-static {v11, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/Toast;->show()V

    .line 315
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    goto/16 :goto_0

    .line 319
    :pswitch_f
    const/4 v11, 0x0

    invoke-direct {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->enableWifiDisplay(Z)V

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    goto/16 :goto_0

    .line 323
    :pswitch_10
    const/16 v11, 0xc

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto/16 :goto_0

    .line 328
    :cond_4
    const-string v11, "com.sec.android.allshare.intent.action.CAST_STOP"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 329
    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v11}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v11

    invoke-virtual {v11}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v11

    const/4 v12, 0x2

    if-ne v11, v12, :cond_2

    .line 330
    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v11}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    goto/16 :goto_0

    .line 333
    :cond_5
    const-string v11, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 334
    const-string v11, "WfdBroker"

    const-string v12, "received NFC Tag that is contained application/vnd.sec.wfd.nfc"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    const-string v11, "WfdBroker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mLaunchedFromNfc = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mLaunchedFromNfc:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", getActiveDisplayState() = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v13}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v13

    invoke-virtual {v13}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", getScanState() = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v13}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v13

    invoke-virtual {v13}, Landroid/hardware/display/WifiDisplayStatus;->getScanState()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    iget-boolean v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mLaunchedFromNfc:Z

    if-nez v11, :cond_6

    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v11}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v11

    invoke-virtual {v11}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_7

    .line 342
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    goto/16 :goto_0

    .line 346
    :cond_7
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mLaunchedFromNfc:Z

    .line 347
    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    const-string v12, "android.hardware.nfc"

    invoke-virtual {v11, v12}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 350
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getIntent()Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getNdefMessages(Landroid/content/Intent;)[Landroid/nfc/NdefMessage;

    move-result-object v4

    .line 351
    .local v4, "msgs":[Landroid/nfc/NdefMessage;
    const/4 v11, 0x0

    aget-object v11, v4, v11

    invoke-virtual {v11}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v11, v11, v12

    invoke-virtual {v11}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v8

    .line 352
    .local v8, "payload":[B
    const-string v11, "WfdBroker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "string of payload"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v8}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    array-length v11, v8

    const/4 v12, 0x1

    if-ge v11, v12, :cond_9

    .line 354
    const/4 v5, 0x0

    .line 356
    .local v5, "newIntent":Landroid/content/Intent;
    if-eqz v10, :cond_8

    .line 357
    const-string v11, "WfdBroker"

    const-string v12, "isWfdConnected >> true"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    new-instance v5, Landroid/content/Intent;

    .end local v5    # "newIntent":Landroid/content/Intent;
    const-string v11, "com.samsung.wfd.WRITE_NFC"

    invoke-direct {v5, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 362
    .restart local v5    # "newIntent":Landroid/content/Intent;
    :goto_1
    const-string v11, "called_by_nfc"

    const/4 v12, 0x1

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 363
    const/high16 v11, 0x20000

    invoke-virtual {v5, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 364
    const-string v11, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getIntent()Landroid/content/Intent;

    move-result-object v12

    const-string v13, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {v12, v13}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 365
    invoke-virtual {p0, v5}, Lcom/sec/android/app/wfdbroker/WfdBroker;->startActivity(Landroid/content/Intent;)V

    .line 366
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    goto/16 :goto_0

    .line 360
    :cond_8
    new-instance v5, Landroid/content/Intent;

    .end local v5    # "newIntent":Landroid/content/Intent;
    const-string v11, "com.samsung.wfd.DP_HELP"

    invoke-direct {v5, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v5    # "newIntent":Landroid/content/Intent;
    goto :goto_1

    .line 368
    .end local v5    # "newIntent":Landroid/content/Intent;
    :cond_9
    if-nez v10, :cond_b

    .line 370
    new-instance v2, Landroid/content/Intent;

    const-string v11, "android.intent.action.DETACH_WFD_BROKER"

    invoke-direct {v2, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 371
    .local v2, "intent":Landroid/content/Intent;
    const-string v11, "dialogNum"

    const/16 v12, 0x24

    invoke-virtual {v2, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 372
    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mContext:Landroid/content/Context;

    invoke-virtual {v11, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 374
    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v8}, Ljava/lang/String;-><init>([B)V

    sput-object v11, Lcom/sec/android/app/wfdbroker/WfdBroker;->staticConnectingDevAddrFromNfc:Ljava/lang/String;

    .line 376
    const-string v11, "WfdBroker"

    const-string v12, "attempt to connect"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->removeAnyFragmentDialog()V

    .line 380
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "wfd_broker_do_not_show_again"

    const-string v13, "wfdbrokerkey"

    invoke-virtual {p0, v11, v12, v13}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getdataPreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    .line 381
    .local v7, "notShowAgain":Z
    if-eqz v7, :cond_a

    .line 382
    const-string v11, "WfdBroker"

    const-string v12, "do not show again welcome!"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showScanningPopup()V

    goto/16 :goto_0

    .line 386
    :cond_a
    const/16 v11, 0x24

    invoke-virtual {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    .line 387
    const-string v11, "WfdBroker"

    const-string v12, "showing welcoming dialog"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    const/16 v11, 0x24

    iput v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I

    goto/16 :goto_0

    .line 390
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v7    # "notShowAgain":Z
    :cond_b
    const-string v11, "WfdBroker"

    const-string v12, "finish connection"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v8}, Ljava/lang/String;-><init>([B)V

    .line 392
    .local v6, "nfcAddr":Ljava/lang/String;
    const-string v11, "WfdBroker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "nfc connecting device addr : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    const/4 v0, 0x0

    .line 394
    .local v0, "ConnectedAddr":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v11, :cond_c

    .line 395
    iget-object v11, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v11}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v11

    invoke-virtual {v11}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v11

    invoke-virtual {v11}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 396
    :cond_c
    if-eqz v0, :cond_d

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_d

    .line 398
    const-string v11, "WfdBroker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "disconnect and connect different device... nfcaddr="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " DEVICE_ADDRESS="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v13}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v13

    invoke-virtual {v13}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v13

    invoke-virtual {v13}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getIntent()Landroid/content/Intent;

    move-result-object v11

    const-string v12, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {v11, v12}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v11

    sput-object v11, Lcom/sec/android/app/wfdbroker/WfdBroker;->nfcMsgs:[Landroid/os/Parcelable;

    .line 402
    :cond_d
    const/4 v11, 0x0

    invoke-direct {p0, v11}, Lcom/sec/android/app/wfdbroker/WfdBroker;->enableWifiDisplay(Z)V

    .line 403
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    goto/16 :goto_0

    .line 407
    .end local v0    # "ConnectedAddr":Ljava/lang/String;
    .end local v4    # "msgs":[Landroid/nfc/NdefMessage;
    .end local v6    # "nfcAddr":Ljava/lang/String;
    .end local v8    # "payload":[B
    :cond_e
    const-string v11, "WfdBroker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "action:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    goto/16 :goto_0

    .line 263
    nop

    :pswitch_data_0
    .packed-switch 0x22070
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_10
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_e
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 557
    const-string v0, "WfdBroker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy mDialogFlag = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogFlag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    iget v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogFlag:I

    sparse-switch v0, :sswitch_data_0

    .line 573
    :goto_0
    iget v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I

    const/16 v1, 0x23

    if-ne v0, v1, :cond_2

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanningTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mNfcReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mNfcReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_1

    .line 580
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 581
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 583
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    if-eq v0, v4, :cond_2

    .line 584
    invoke-direct {p0, v3}, Lcom/sec/android/app/wfdbroker/WfdBroker;->enableWifiDisplay(Z)V

    .line 588
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mIsContinue:Z

    if-nez v0, :cond_3

    .line 589
    iget v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogFlag:I

    sparse-switch v0, :sswitch_data_1

    .line 603
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 605
    iput v3, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogFlag:I

    .line 606
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 607
    return-void

    .line 564
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->restartWifiDisplay()V

    goto :goto_0

    .line 567
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->sendIntentToDongleUpdater()V

    goto :goto_0

    .line 596
    :sswitch_2
    invoke-direct {p0, v3}, Lcom/sec/android/app/wfdbroker/WfdBroker;->enableWifiDisplay(Z)V

    goto :goto_1

    .line 600
    :cond_3
    invoke-direct {p0, v4}, Lcom/sec/android/app/wfdbroker/WfdBroker;->enableWifiDisplay(Z)V

    goto :goto_1

    .line 559
    :sswitch_data_0
    .sparse-switch
        0x12 -> :sswitch_0
        0x17 -> :sswitch_1
    .end sparse-switch

    .line 589
    :sswitch_data_1
    .sparse-switch
        0x11 -> :sswitch_2
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x1e -> :sswitch_2
        0x21 -> :sswitch_2
        0x26 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 549
    const-string v0, "WfdBroker"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 552
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 542
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 544
    const-string v0, "WfdBroker"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    return-void
.end method

.method public showFragmentDialog(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 611
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->removeAnyFragmentDialog()V

    .line 612
    iput p1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogFlag:I

    .line 613
    iget v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogCnt:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogCnt:I

    .line 614
    const-string v2, "WfdBroker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showFragmentDialog "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogCnt:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    invoke-static {p1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->newDialog(I)Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    move-result-object v0

    .line 616
    .local v0, "dialog":Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;
    new-instance v2, Lcom/sec/android/app/wfdbroker/WfdBroker$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/wfdbroker/WfdBroker$5;-><init>(Lcom/sec/android/app/wfdbroker/WfdBroker;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->setListener(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$Listener;)V

    .line 654
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 659
    :goto_0
    return-void

    .line 655
    :catch_0
    move-exception v1

    .line 656
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    .line 657
    const-string v2, "WfdBroker"

    const-string v3, "IllegalStateException when dialog show"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public showScanningPopup()V
    .locals 6

    .prologue
    const/16 v5, 0x23

    .line 420
    const-string v1, "WfdBroker"

    const-string v2, "show scanning popup"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->removeAnyFragmentDialog()V

    .line 423
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;

    if-nez v1, :cond_0

    .line 424
    new-instance v1, Lcom/sec/android/app/wfdbroker/WfdBroker$2;

    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/wfdbroker/WfdBroker$2;-><init>(Lcom/sec/android/app/wfdbroker/WfdBroker;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 435
    invoke-virtual {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "wifi_display_on"

    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 439
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 440
    .local v0, "filter":Landroid/content/IntentFilter;
    new-instance v1, Lcom/sec/android/app/wfdbroker/WfdBroker$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/wfdbroker/WfdBroker$3;-><init>(Lcom/sec/android/app/wfdbroker/WfdBroker;)V

    iput-object v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mNfcReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 486
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/wfdbroker/WfdBroker;->enableWifiDisplay(Z)V

    .line 487
    invoke-virtual {p0, v5}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    .line 488
    iput v5, p0, Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I

    .line 489
    return-void
.end method
