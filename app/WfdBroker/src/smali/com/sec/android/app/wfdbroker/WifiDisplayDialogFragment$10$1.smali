.class Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10$1;
.super Landroid/os/CountDownTimer;
.source "WifiDisplayDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;

.field final synthetic val$arg0:Landroid/content/DialogInterface;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;JJLandroid/content/DialogInterface;)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 461
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10$1;->this$1:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;

    iput-object p6, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10$1;->val$arg0:Landroid/content/DialogInterface;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method

.method private isAudioSilent()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 479
    iget-object v3, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10$1;->this$1:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;

    iget-object v3, v3, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # getter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$300(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/wfdbroker/WfdBroker;

    const-string v5, "audio"

    invoke-virtual {v3, v5}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 480
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v2

    .line 481
    .local v2, "ringerMode":I
    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 482
    .local v1, "masterStreamVolume":I
    if-eqz v1, :cond_0

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    :cond_0
    move v3, v4

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onFinish()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 467
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10$1;->isAudioSilent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 468
    const-string v1, "WifiDisplayDialogFragment"

    const-string v2, "Silent mode or volume is zero"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10$1;->val$arg0:Landroid/content/DialogInterface;

    invoke-interface {v1}, Landroid/content/DialogInterface;->dismiss()V

    .line 476
    return-void

    .line 470
    :cond_0
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x4

    const/4 v3, 0x7

    invoke-direct {v0, v1, v3, v5}, Landroid/media/SoundPool;-><init>(III)V

    .line 471
    .local v0, "mButtonSoundPool":Landroid/media/SoundPool;
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10$1;->this$1:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;

    iget-object v3, v1, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10$1;->this$1:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;

    iget-object v1, v1, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # getter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$300(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v1}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v6, 0x7f040000

    invoke-virtual {v0, v1, v6, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    # setter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mCurrentSoundID:I
    invoke-static {v3, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$702(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;I)I

    .line 473
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10$1;->this$1:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;

    iget-object v1, v1, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # getter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mCurrentSoundID:I
    invoke-static {v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$700(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)I

    move-result v1

    move v3, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto :goto_0
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 464
    return-void
.end method
