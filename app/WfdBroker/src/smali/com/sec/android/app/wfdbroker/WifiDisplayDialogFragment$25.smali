.class Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$25;
.super Ljava/lang/Object;
.source "WifiDisplayDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->createWfdDialog(I)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V
    .locals 0

    .prologue
    .line 681
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$25;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v4, 0x1

    .line 685
    const-string v2, "WifiDisplayDialogFragment"

    const-string v3, "Proceed is clicked..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$25;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # invokes: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->isSideSyncConnected()Z
    invoke-static {v2}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$1100(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 687
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.sidesync.source.action.STOP_SERVICE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 688
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "REASON"

    const-string v3, "CAST_START"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 689
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$25;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # getter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$300(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 691
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.sidesync.sink.action.STOP_SERVICE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 692
    .local v1, "intentSink":Landroid/content/Intent;
    const-string v2, "REASON"

    const-string v3, "CAST_START"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 693
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$25;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # getter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$300(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 696
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "intentSink":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.sidesync.action.FINISH_SIDESYNC_APP"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 697
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v2, "FINISH_SIDESYNC_APP_REASON"

    const-string v3, "BY_SETTINGS_TETHERING"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 698
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$25;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # getter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$300(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 700
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$25;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # setter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsContinue:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$402(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)Z

    .line 701
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$25;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # invokes: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->enableWifiDisplay(Z)V
    invoke-static {v2, v4}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$100(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)V

    .line 702
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 703
    return-void
.end method
