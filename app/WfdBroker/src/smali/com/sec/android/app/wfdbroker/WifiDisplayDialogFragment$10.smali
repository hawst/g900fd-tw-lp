.class Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;
.super Ljava/lang/Object;
.source "WifiDisplayDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->createWfdDialog(I)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V
    .locals 0

    .prologue
    .line 446
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const-wide/16 v2, 0x1f4

    const/4 v5, 0x2

    const/4 v8, 0x0

    .line 450
    const-string v1, "WifiDisplayDialogFragment"

    const-string v4, "terminate\'s OK is clicked..."

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # setter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsHome:Z
    invoke-static {v1, v8}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$202(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)Z

    .line 452
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # getter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mDisplayManager:Landroid/hardware/display/DisplayManager;
    invoke-static {v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$600(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Landroid/hardware/display/DisplayManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v7

    .line 454
    .local v7, "wifiDisplayStatus":Landroid/hardware/display/WifiDisplayStatus;
    invoke-virtual {v7}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v1

    if-ne v1, v5, :cond_0

    invoke-virtual {v7}, Landroid/hardware/display/WifiDisplayStatus;->getConnectedState()I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # getter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mDisplayManager:Landroid/hardware/display/DisplayManager;
    invoke-static {v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$600(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Landroid/hardware/display/DisplayManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/display/DisplayManager;->disableWifiDisplay()V

    .line 461
    :goto_0
    new-instance v0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10$1;

    move-object v1, p0

    move-wide v4, v2

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10$1;-><init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;JJLandroid/content/DialogInterface;)V

    .line 485
    .local v0, "timer":Landroid/os/CountDownTimer;
    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 487
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # invokes: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->enableWifiDisplay(Z)V
    invoke-static {v1, v8}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$100(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)V

    .line 489
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 490
    return-void

    .line 458
    .end local v0    # "timer":Landroid/os/CountDownTimer;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$10;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # getter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mDisplayManager:Landroid/hardware/display/DisplayManager;
    invoke-static {v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$600(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Landroid/hardware/display/DisplayManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    goto :goto_0
.end method
