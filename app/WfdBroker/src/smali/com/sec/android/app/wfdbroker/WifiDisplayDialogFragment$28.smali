.class Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$28;
.super Ljava/lang/Object;
.source "WifiDisplayDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->createWfdDialog(I)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)V
    .locals 0

    .prologue
    .line 740
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$28;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v3, 0x0

    .line 744
    const-string v0, "WifiDisplayDialogFragment"

    const-string v1, "OK is clicked..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$28;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # getter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$300(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$28;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 748
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$28;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->mIsContinue:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$402(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;Z)Z

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$28;->this$0:Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    # invokes: Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->setPowerSavingMode(I)V
    invoke-static {v0, v3}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->access$1300(Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;I)V

    .line 750
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 751
    return-void
.end method
