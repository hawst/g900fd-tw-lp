.class public final Lcom/sec/android/app/wfdbroker/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wfdbroker/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final dlg_cancel:I = 0x7f050000

.field public static final dlg_ok:I = 0x7f050001

.field public static final enable_text:I = 0x7f050002

.field public static final lockpattern_retry_button_text:I = 0x7f050003

.field public static final permdesc_wfdbroker:I = 0x7f050004

.field public static final permlab_wfdbroker:I = 0x7f050005

.field public static final progress_scanning:I = 0x7f050006

.field public static final stms_appgroup:I = 0x7f050007

.field public static final wfd_fhdstate_title:I = 0x7f050008

.field public static final wfd_never_show_again:I = 0x7f050009

.field public static final wfd_power_saving_confirmation:I = 0x7f05000a

.field public static final wfd_power_saving_mode_disabled:I = 0x7f05000b

.field public static final wfd_settings_cannot_proceed_cause_drm:I = 0x7f05000c

.field public static final wfd_settings_cannot_proceed_cause_hdmi_busy:I = 0x7f05000d

.field public static final wfd_settings_cannot_proceed_cause_hotspot_busy:I = 0x7f05000e

.field public static final wfd_settings_cannot_proceed_cause_hotspot_busy_chn:I = 0x7f050032

.field public static final wfd_settings_cannot_proceed_cause_max_res:I = 0x7f05000f

.field public static final wfd_settings_cannot_proceed_cause_oxygen_enabled:I = 0x7f050010

.field public static final wfd_settings_cannot_proceed_cause_p2p_busy:I = 0x7f050011

.field public static final wfd_settings_cannot_proceed_cause_p2p_busy_chn:I = 0x7f050012

.field public static final wfd_settings_cannot_proceed_cause_sbeam_busy:I = 0x7f050013

.field public static final wfd_settings_cannot_proceed_cause_split_window:I = 0x7f050014

.field public static final wfd_settings_connection_fail:I = 0x7f050015

.field public static final wfd_settings_disconnect_connection_bt_or_earphone_connected:I = 0x7f050016

.field public static final wfd_settings_disconnect_connection_hdmi_connected:I = 0x7f050017

.field public static final wfd_settings_enable_fhd:I = 0x7f050018

.field public static final wfd_settings_has_been_disabled:I = 0x7f050019

.field public static final wfd_settings_inform_power_saving_mode_enabled:I = 0x7f05001a

.field public static final wfd_settings_network_fail:I = 0x7f05001b

.field public static final wfd_settings_ongoing_restart:I = 0x7f05001c

.field public static final wfd_settings_proceed:I = 0x7f05001d

.field public static final wfd_settings_recording_fhd:I = 0x7f05001e

.field public static final wfd_settings_recording_is_not_available:I = 0x7f05001f

.field public static final wfd_settings_restart:I = 0x7f050020

.field public static final wfd_settings_restart_chn:I = 0x7f050021

.field public static final wfd_settings_sidesync_running:I = 0x7f050022

.field public static final wfd_settings_sidesync_title:I = 0x7f050023

.field public static final wfd_settings_terminates:I = 0x7f050024

.field public static final wfd_settings_terminates_no_busy:I = 0x7f050025

.field public static final wfd_settings_title:I = 0x7f050026

.field public static final wfd_settings_title_chn:I = 0x7f050027

.field public static final wfd_settings_unable_to_start_screen_mirroring_and_disconnect_hdmi_cable:I = 0x7f050028

.field public static final wfd_settings_unable_to_start_screen_mirroring_because_of_a_hardware_issue:I = 0x7f050029

.field public static final wfd_settings_update_dongle:I = 0x7f05002a

.field public static final wfd_settings_update_process:I = 0x7f05002b

.field public static final wfd_start_allshare:I = 0x7f05002c

.field public static final wfd_weak_network_toast_message:I = 0x7f05002d

.field public static final wfd_welcome_message:I = 0x7f05002e

.field public static final wifi_p2p_available_devices:I = 0x7f05002f

.field public static final wifi_p2p_connecting:I = 0x7f050030

.field public static final wifi_p2p_no_device_found:I = 0x7f050031


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
