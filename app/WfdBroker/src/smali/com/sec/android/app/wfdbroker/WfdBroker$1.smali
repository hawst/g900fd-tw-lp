.class Lcom/sec/android/app/wfdbroker/WfdBroker$1;
.super Landroid/content/BroadcastReceiver;
.source "WfdBroker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/wfdbroker/WfdBroker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wfdbroker/WfdBroker;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 140
    const-string v6, "WfdBroker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "received :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const-string v7, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 142
    const-string v6, "state"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 143
    .local v3, "state":Z
    if-eqz v3, :cond_0

    .line 144
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    .line 203
    .end local v3    # "state":Z
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const-string v7, "android.intent.action.WIFI_DISPLAY_URL_FROM_NATIVE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 148
    const-string v6, "URL"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 150
    .local v5, "updateURL":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 151
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    const/4 v7, 0x1

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mIsFWUpgradeOn:Z
    invoke-static {v6, v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$002(Lcom/sec/android/app/wfdbroker/WfdBroker;Z)Z

    .line 152
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # invokes: Lcom/sec/android/app/wfdbroker/WfdBroker;->removeAnyFragmentDialog()V
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$100(Lcom/sec/android/app/wfdbroker/WfdBroker;)V

    .line 153
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v6, v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$202(Lcom/sec/android/app/wfdbroker/WfdBroker;I)I

    goto :goto_0

    .line 155
    .end local v5    # "updateURL":Ljava/lang/String;
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const-string v7, "DisplayManager.POPUP_CAUSE_INVALID_HDCP_KEY"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 156
    const-string v6, "WfdBroker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "received INVALID_HDCP_KEY"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v8}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$200(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # invokes: Lcom/sec/android/app/wfdbroker/WfdBroker;->removeAnyFragmentDialog()V
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$100(Lcom/sec/android/app/wfdbroker/WfdBroker;)V

    .line 158
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v6, v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$202(Lcom/sec/android/app/wfdbroker/WfdBroker;I)I

    .line 159
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    const/16 v7, 0x1c

    invoke-virtual {v6, v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto :goto_0

    .line 160
    :cond_3
    const-string v6, "android.intent.action.DETACH_WFD_BROKER"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 161
    const-string v6, "dialogNum"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 162
    .local v0, "dialogNum":I
    const-string v6, "WfdBroker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "received : WIFIDISPLAY_DETACH_WFD_BROKER mDialogNum = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v8}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$200(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", IntentExtra : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    const/16 v6, 0x24

    if-ne v0, v6, :cond_4

    const/16 v6, 0x24

    iget-object v7, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$200(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v7

    if-eq v6, v7, :cond_0

    const/16 v6, 0x23

    iget-object v7, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$200(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v7

    if-eq v6, v7, :cond_0

    .line 166
    :cond_4
    const/4 v6, 0x1

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mBrokerDetachRequested:Z
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$302(Z)Z

    .line 167
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # invokes: Lcom/sec/android/app/wfdbroker/WfdBroker;->removeAnyFragmentDialog()V
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$100(Lcom/sec/android/app/wfdbroker/WfdBroker;)V

    .line 168
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    goto/16 :goto_0

    .line 170
    .end local v0    # "dialogNum":I
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const-string v7, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 171
    const-string v6, "android.hardware.display.extra.WIFI_DISPLAY_STATUS"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/hardware/display/WifiDisplayStatus;

    .line 173
    .local v4, "status":Landroid/hardware/display/WifiDisplayStatus;
    const-string v6, "WfdBroker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ACTION_WIFI_DISPLAY_STATUS_CHANGED = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", RunningMode = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v8}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$200(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v8

    const v9, 0x22000

    sub-int/2addr v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    const/4 v6, 0x4

    invoke-virtual {v4}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v7

    if-ne v6, v7, :cond_6

    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$200(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v6

    const v7, 0x2208a

    if-ne v6, v7, :cond_6

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mBrokerDetachRequested:Z
    invoke-static {}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$300()Z

    move-result v6

    if-nez v6, :cond_6

    .line 176
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v6, v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$202(Lcom/sec/android/app/wfdbroker/WfdBroker;I)I

    .line 177
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # invokes: Lcom/sec/android/app/wfdbroker/WfdBroker;->removeAnyFragmentDialog()V
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$100(Lcom/sec/android/app/wfdbroker/WfdBroker;)V

    .line 178
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    const/16 v7, 0xd

    invoke-virtual {v6, v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V

    goto/16 :goto_0

    .line 179
    :cond_6
    const/4 v6, 0x2

    invoke-virtual {v4}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v7

    if-eq v6, v7, :cond_7

    const/4 v6, 0x1

    invoke-virtual {v4}, Landroid/hardware/display/WifiDisplayStatus;->getFeatureState()I

    move-result v7

    if-ne v6, v7, :cond_9

    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$200(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v6

    const v7, 0x2208a

    if-ne v6, v7, :cond_9

    .line 181
    const/4 v6, 0x2

    invoke-virtual {v4}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v7

    if-ne v6, v7, :cond_8

    .line 182
    const-string v6, "wlan.wfd.autoconnect"

    const-string v7, "true"

    invoke-static {v6, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v6, v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$202(Lcom/sec/android/app/wfdbroker/WfdBroker;I)I

    .line 185
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # invokes: Lcom/sec/android/app/wfdbroker/WfdBroker;->removeAnyFragmentDialog()V
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$100(Lcom/sec/android/app/wfdbroker/WfdBroker;)V

    .line 186
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    goto/16 :goto_0

    .line 187
    :cond_9
    const/4 v6, 0x1

    invoke-virtual {v4}, Landroid/hardware/display/WifiDisplayStatus;->getFeatureState()I

    move-result v7

    if-ne v6, v7, :cond_a

    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$200(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v6

    const v7, 0x22072

    if-ne v6, v7, :cond_a

    .line 188
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v6, v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$202(Lcom/sec/android/app/wfdbroker/WfdBroker;I)I

    .line 189
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # invokes: Lcom/sec/android/app/wfdbroker/WfdBroker;->removeAnyFragmentDialog()V
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$100(Lcom/sec/android/app/wfdbroker/WfdBroker;)V

    .line 190
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    goto/16 :goto_0

    .line 191
    :cond_a
    invoke-virtual {v4}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$200(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v6

    const v7, 0x22075

    if-ne v6, v7, :cond_0

    .line 192
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "dialog"

    invoke-virtual {v6, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 193
    .local v2, "prev":Landroid/app/Fragment;
    if-eqz v2, :cond_b

    move-object v1, v2

    .line 194
    check-cast v1, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;

    .line 195
    .local v1, "fragment":Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;
    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;->setContinue(Z)V

    .line 197
    .end local v1    # "fragment":Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment;
    :cond_b
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v6, v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$202(Lcom/sec/android/app/wfdbroker/WfdBroker;I)I

    .line 198
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # invokes: Lcom/sec/android/app/wfdbroker/WfdBroker;->removeAnyFragmentDialog()V
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$100(Lcom/sec/android/app/wfdbroker/WfdBroker;)V

    .line 199
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$1;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    .line 200
    const-string v6, "WfdBroker"

    const-string v7, "Remove the Hotpot dialog when wifi disconnect"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
