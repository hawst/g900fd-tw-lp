.class Lcom/sec/android/app/wfdbroker/WfdBroker$5;
.super Ljava/lang/Object;
.source "WfdBroker.java"

# interfaces
.implements Lcom/sec/android/app/wfdbroker/WifiDisplayDialogFragment$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wfdbroker/WfdBroker;->showFragmentDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wfdbroker/WfdBroker;)V
    .locals 0

    .prologue
    .line 616
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 633
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # operator-- for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogCnt:I
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1310(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    .line 634
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mIsContinue:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1402(Lcom/sec/android/app/wfdbroker/WfdBroker;Z)Z

    .line 635
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogFlag:I
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1500(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_2

    .line 636
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanInProgress:Z
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$500(Lcom/sec/android/app/wfdbroker/WfdBroker;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanInProgress:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$502(Lcom/sec/android/app/wfdbroker/WfdBroker;Z)Z

    .line 638
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$400(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/hardware/display/DisplayManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->stopScanWifiDisplays()V

    .line 639
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$700(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanningTimeout:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$600(Lcom/sec/android/app/wfdbroker/WfdBroker;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 647
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogCnt:I
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1300(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v0

    if-nez v0, :cond_1

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    .line 650
    :cond_1
    return-void

    .line 641
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogFlag:I
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1500(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # invokes: Lcom/sec/android/app/wfdbroker/WfdBroker;->isWfdConnected()Z
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1600(Lcom/sec/android/app/wfdbroker/WfdBroker;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mIsFWUpgradeOn:Z
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$000(Lcom/sec/android/app/wfdbroker/WfdBroker;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 643
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$400(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/hardware/display/DisplayManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getLastConnectedDisplay(Z)Landroid/hardware/display/WifiDisplay;

    .line 644
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$400(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/hardware/display/DisplayManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    goto :goto_0
.end method

.method public ok()V
    .locals 2

    .prologue
    .line 619
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # operator-- for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogCnt:I
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1310(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mIsContinue:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1402(Lcom/sec/android/app/wfdbroker/WfdBroker;Z)Z

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogFlag:I
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1500(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v0

    const/16 v1, 0x24

    if-ne v0, v1, :cond_1

    .line 622
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->showScanningPopup()V

    .line 629
    :cond_0
    :goto_0
    return-void

    .line 626
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogCnt:I
    invoke-static {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1300(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v0

    if-nez v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$5;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v0}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    goto :goto_0
.end method
