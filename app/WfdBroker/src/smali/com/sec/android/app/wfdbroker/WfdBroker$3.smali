.class Lcom/sec/android/app/wfdbroker/WfdBroker$3;
.super Landroid/content/BroadcastReceiver;
.source "WfdBroker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/wfdbroker/WfdBroker;->showScanningPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;


# direct methods
.method constructor <init>(Lcom/sec/android/app/wfdbroker/WfdBroker;)V
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private findSavedDevice()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 469
    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->staticConnectingDevAddrFromNfc:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1000()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    .line 482
    :cond_0
    :goto_0
    return-object v5

    .line 473
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;
    invoke-static {v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$400(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/hardware/display/DisplayManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v4

    .line 474
    .local v4, "status":Landroid/hardware/display/WifiDisplayStatus;
    invoke-virtual {v4}, Landroid/hardware/display/WifiDisplayStatus;->getDisplays()[Landroid/hardware/display/WifiDisplay;

    move-result-object v0

    .local v0, "arr$":[Landroid/hardware/display/WifiDisplay;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 475
    .local v1, "display":Landroid/hardware/display/WifiDisplay;
    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplay;->isAvailable()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v6

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->staticConnectingDevAddrFromNfc:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$1000()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 477
    const-string v5, "WfdBroker"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "saved device is found! : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 474
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 443
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 444
    const-string v2, "WfdBroker"

    const-string v3, "status changed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;
    invoke-static {v2}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$400(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/hardware/display/DisplayManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v1

    .line 446
    .local v1, "status":Landroid/hardware/display/WifiDisplayStatus;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getScanState()I

    move-result v2

    if-nez v2, :cond_0

    .line 449
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanInProgress:Z
    invoke-static {v2, v6}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$502(Lcom/sec/android/app/wfdbroker/WfdBroker;Z)Z

    .line 450
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$700(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanningTimeout:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$600(Lcom/sec/android/app/wfdbroker/WfdBroker;)Ljava/lang/Runnable;

    move-result-object v3

    const-wide/16 v4, 0x4e20

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 451
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;
    invoke-static {v2}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$400(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/hardware/display/DisplayManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/display/DisplayManager;->scanWifiDisplays()V

    .line 454
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->findSavedDevice()Ljava/lang/String;

    move-result-object v0

    .line 456
    .local v0, "dev":Ljava/lang/String;
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDialogNum:I
    invoke-static {v2}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$200(Lcom/sec/android/app/wfdbroker/WfdBroker;)I

    move-result v2

    const/16 v3, 0x23

    if-ne v2, v3, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getScanState()I

    move-result v2

    if-ne v2, v6, :cond_1

    .line 457
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mDisplayManager:Landroid/hardware/display/DisplayManager;
    invoke-static {v2}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$400(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/hardware/display/DisplayManager;

    move-result-object v2

    const/16 v3, 0x9

    invoke-virtual {v2, v3, v0}, Landroid/hardware/display/DisplayManager;->connectWifiDisplayWithMode(ILjava/lang/String;)V

    .line 458
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$700(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mScanningTimeout:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$600(Lcom/sec/android/app/wfdbroker/WfdBroker;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 459
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    iget-object v3, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mNfcReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v3}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$800(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/content/BroadcastReceiver;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/wfdbroker/WfdBroker;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 460
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v2}, Lcom/sec/android/app/wfdbroker/WfdBroker;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # getter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;
    invoke-static {v3}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$900(Lcom/sec/android/app/wfdbroker/WfdBroker;)Landroid/database/ContentObserver;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 461
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mSettingsObserver:Landroid/database/ContentObserver;
    invoke-static {v2, v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$902(Lcom/sec/android/app/wfdbroker/WfdBroker;Landroid/database/ContentObserver;)Landroid/database/ContentObserver;

    .line 462
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    # setter for: Lcom/sec/android/app/wfdbroker/WfdBroker;->mNfcReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v2, v7}, Lcom/sec/android/app/wfdbroker/WfdBroker;->access$802(Lcom/sec/android/app/wfdbroker/WfdBroker;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    .line 463
    iget-object v2, p0, Lcom/sec/android/app/wfdbroker/WfdBroker$3;->this$0:Lcom/sec/android/app/wfdbroker/WfdBroker;

    invoke-virtual {v2}, Lcom/sec/android/app/wfdbroker/WfdBroker;->finish()V

    .line 466
    .end local v0    # "dev":Ljava/lang/String;
    .end local v1    # "status":Landroid/hardware/display/WifiDisplayStatus;
    :cond_1
    return-void
.end method
