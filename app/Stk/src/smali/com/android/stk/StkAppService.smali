.class public Lcom/android/stk/StkAppService;
.super Landroid/app/Service;
.source "StkAppService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/stk/StkAppService$2;,
        Lcom/android/stk/StkAppService$ServiceHandler;,
        Lcom/android/stk/StkAppService$MessageHandler;,
        Lcom/android/stk/StkAppService$DelayedCmd;,
        Lcom/android/stk/StkAppService$InitiatedByUserAction;
    }
.end annotation


# static fields
.field static sInstance:Lcom/android/stk/StkAppService;


# instance fields
.field private final DELAY_TIME_FOR_RESET:I

.field private lastSelectedItem:Ljava/lang/String;

.field private launchBrowser:Z

.field public final lock:Ljava/util/concurrent/locks/ReentrantLock;

.field mBackupProxy:Landroid/net/ProxyInfo;

.field private mBrowserSettings:Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

.field private mCmdInProgress:Z

.field private mCmdsQ:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/stk/StkAppService$DelayedCmd;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

.field private mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

.field public mIsMainMenu:Z

.field mIsProxyChanged:Z

.field private mIsStartedByUser:Z

.field mIsSystemShutdown:Z

.field private mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

.field private mMenuIsVisibile:Z

.field public mMenuItemBlock:Z

.field private mMessageHandler:Landroid/os/Handler;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private volatile mServiceHandler:Lcom/android/stk/StkAppService$ServiceHandler;

.field private volatile mServiceLooper:Landroid/os/Looper;

.field private mSetEventList:[Z

.field private mStkService:Lcom/android/internal/telephony/cat/AppInterface;

.field mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private responseNeeded:Z

.field private salesCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    sput-object v0, Lcom/android/stk/StkAppService;->sInstance:Lcom/android/stk/StkAppService;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 103
    iput-object v1, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    .line 104
    iput-object v1, p0, Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    .line 105
    iput-object v1, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    .line 106
    iput-object v1, p0, Lcom/android/stk/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 107
    iput-object v1, p0, Lcom/android/stk/StkAppService;->lastSelectedItem:Ljava/lang/String;

    .line 108
    iput-boolean v2, p0, Lcom/android/stk/StkAppService;->mMenuIsVisibile:Z

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/stk/StkAppService;->responseNeeded:Z

    .line 110
    iput-boolean v2, p0, Lcom/android/stk/StkAppService;->mCmdInProgress:Z

    .line 111
    iput-object v1, p0, Lcom/android/stk/StkAppService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 112
    iput-object v1, p0, Lcom/android/stk/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    .line 113
    iput-boolean v2, p0, Lcom/android/stk/StkAppService;->launchBrowser:Z

    .line 114
    iput-object v1, p0, Lcom/android/stk/StkAppService;->mBrowserSettings:Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    .line 116
    iput-object v1, p0, Lcom/android/stk/StkAppService;->mSetEventList:[Z

    .line 122
    iput-object v1, p0, Lcom/android/stk/StkAppService;->mMessageHandler:Landroid/os/Handler;

    .line 127
    iput-boolean v2, p0, Lcom/android/stk/StkAppService;->mMenuItemBlock:Z

    .line 128
    iput-boolean v2, p0, Lcom/android/stk/StkAppService;->mIsMainMenu:Z

    .line 132
    iput-boolean v2, p0, Lcom/android/stk/StkAppService;->mIsStartedByUser:Z

    .line 135
    iput-boolean v2, p0, Lcom/android/stk/StkAppService;->mIsProxyChanged:Z

    .line 137
    iput-boolean v2, p0, Lcom/android/stk/StkAppService;->mIsSystemShutdown:Z

    .line 188
    const/16 v0, 0x1388

    iput v0, p0, Lcom/android/stk/StkAppService;->DELAY_TIME_FOR_RESET:I

    .line 201
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/android/stk/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 399
    return-void
.end method

.method static synthetic access$1000(Lcom/android/stk/StkAppService;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk/StkAppService;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/android/stk/StkAppService;->handleCmdResponse(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/stk/StkAppService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk/StkAppService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->callDelayedMsg()V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/stk/StkAppService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk/StkAppService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->handleSessionEnd()V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/stk/StkAppService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/stk/StkAppService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/stk/StkAppService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk/StkAppService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->handleDelayedCmd()V

    return-void
.end method

.method static synthetic access$1500(Lcom/android/stk/StkAppService;Lcom/android/internal/telephony/cat/CatEventDownload;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk/StkAppService;
    .param p1, "x1"    # Lcom/android/internal/telephony/cat/CatEventDownload;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/android/stk/StkAppService;->handleEvent(Lcom/android/internal/telephony/cat/CatEventDownload;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/stk/StkAppService;)Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;
    .locals 1
    .param p0, "x0"    # Lcom/android/stk/StkAppService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mBrowserSettings:Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/stk/StkAppService;Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk/StkAppService;
    .param p1, "x1"    # Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/android/stk/StkAppService;->launchBrowser(Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/android/stk/StkAppService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/android/stk/StkAppService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mMessageHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/stk/StkAppService;)Lcom/android/internal/telephony/cat/CatCmdMessage;
    .locals 1
    .param p0, "x0"    # Lcom/android/stk/StkAppService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/stk/StkAppService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/stk/StkAppService;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/android/stk/StkAppService;->mIsStartedByUser:Z

    return p1
.end method

.method static synthetic access$400(Lcom/android/stk/StkAppService;Lcom/android/internal/telephony/cat/Menu;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk/StkAppService;
    .param p1, "x1"    # Lcom/android/internal/telephony/cat/Menu;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/android/stk/StkAppService;->launchMenuActivity(Lcom/android/internal/telephony/cat/Menu;)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/stk/StkAppService;Lcom/android/internal/telephony/cat/CatCmdMessage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/stk/StkAppService;
    .param p1, "x1"    # Lcom/android/internal/telephony/cat/CatCmdMessage;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/android/stk/StkAppService;->isCmdInteractive(Lcom/android/internal/telephony/cat/CatCmdMessage;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/stk/StkAppService;Lcom/android/internal/telephony/cat/CatCmdMessage;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk/StkAppService;
    .param p1, "x1"    # Lcom/android/internal/telephony/cat/CatCmdMessage;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/android/stk/StkAppService;->handleCmd(Lcom/android/internal/telephony/cat/CatCmdMessage;)V

    return-void
.end method

.method static synthetic access$700(Lcom/android/stk/StkAppService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/stk/StkAppService;

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/android/stk/StkAppService;->mCmdInProgress:Z

    return v0
.end method

.method static synthetic access$702(Lcom/android/stk/StkAppService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/stk/StkAppService;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/android/stk/StkAppService;->mCmdInProgress:Z

    return p1
.end method

.method static synthetic access$800(Lcom/android/stk/StkAppService;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/android/stk/StkAppService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/stk/StkAppService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/stk/StkAppService;

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/android/stk/StkAppService;->responseNeeded:Z

    return v0
.end method

.method static synthetic access$902(Lcom/android/stk/StkAppService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/stk/StkAppService;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/android/stk/StkAppService;->responseNeeded:Z

    return p1
.end method

.method private callDelayedMsg()V
    .locals 2

    .prologue
    .line 680
    iget-object v1, p0, Lcom/android/stk/StkAppService;->mServiceHandler:Lcom/android/stk/StkAppService$ServiceHandler;

    invoke-virtual {v1}, Lcom/android/stk/StkAppService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 681
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 682
    iget-object v1, p0, Lcom/android/stk/StkAppService;->mServiceHandler:Lcom/android/stk/StkAppService$ServiceHandler;

    invoke-virtual {v1, v0}, Lcom/android/stk/StkAppService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 683
    return-void
.end method

.method private canLaunchBrowser()Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1004
    const-string v8, "canLaunchBrowser"

    invoke-static {p0, v8}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1013
    invoke-virtual {p0}, Lcom/android/stk/StkAppService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    .line 1014
    .local v1, "iContext":Landroid/content/Context;
    const/4 v3, 0x0

    .line 1015
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 1019
    .local v0, "am":Landroid/app/ActivityManager;
    const-string v8, "activity"

    invoke-virtual {v1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "am":Landroid/app/ActivityManager;
    check-cast v0, Landroid/app/ActivityManager;

    .line 1020
    .restart local v0    # "am":Landroid/app/ActivityManager;
    if-nez v0, :cond_1

    .line 1021
    const-string v7, "Activity Manager is NULL"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1045
    :cond_0
    :goto_0
    return v6

    .line 1025
    :cond_1
    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v5

    .line 1026
    .local v5, "runningTaskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v5, :cond_2

    .line 1028
    const-string v7, "runningTaskInfo == null"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1032
    :cond_2
    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1033
    .local v4, "runInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    const-string v8, "Getting first Running task info"

    invoke-static {p0, v8}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1036
    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 1037
    .local v2, "packName":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Value of package name"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1038
    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "com.android.browser"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "com.sec.android.app.sbrowser"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1040
    :cond_3
    const-string v6, "Package Name matches"

    invoke-static {p0, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move v6, v7

    .line 1041
    goto :goto_0
.end method

.method private canLaunchDisptextDialog()Z
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 948
    const-string v10, "canLaunchDisptextDialog"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 949
    invoke-virtual {p0}, Lcom/android/stk/StkAppService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    .line 950
    .local v2, "iContext":Landroid/content/Context;
    const/4 v5, 0x0

    .line 951
    .local v5, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 956
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 957
    if-nez v5, :cond_0

    .line 958
    const-string v9, "Package Manager is NULL"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 998
    :goto_0
    return v8

    .line 963
    :cond_0
    const-string v10, "activity"

    invoke-virtual {v2, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "am":Landroid/app/ActivityManager;
    check-cast v0, Landroid/app/ActivityManager;

    .line 964
    .restart local v0    # "am":Landroid/app/ActivityManager;
    if-nez v0, :cond_1

    .line 965
    const-string v9, "Activity Manager is NULL"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 969
    :cond_1
    new-instance v10, Landroid/content/Intent;

    const-string v11, "android.intent.action.MAIN"

    invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v11, "android.intent.category.HOME"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v5, v10, v9}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 971
    .local v1, "homeInfo":Landroid/content/pm/ResolveInfo;
    if-nez v1, :cond_2

    .line 972
    const-string v9, "homdInfo is NULL"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 975
    :cond_2
    iget-object v10, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 976
    .local v3, "origPackName":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Value of original Packgage name"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 978
    invoke-virtual {v0, v8}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v7

    .line 979
    .local v7, "runningTaskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v7, :cond_3

    .line 981
    const-string v9, "runningTaskInfo == null"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 985
    :cond_3
    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 986
    .local v6, "runInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    const-string v10, "Getting first Running task info"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 989
    iget-object v10, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 990
    .local v4, "packName":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Value of package name"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 991
    iget-object v10, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    iget-object v10, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "com.android.stk"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 993
    :cond_4
    const-string v9, "Package Name matches"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v8, v9

    .line 998
    goto/16 :goto_0
.end method

.method private distroyMenuActivity()V
    .locals 4

    .prologue
    .line 1279
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1280
    .local v1, "newIntent":Landroid/content/Intent;
    const-string v2, "com.android.stk"

    const-string v3, "com.android.stk.StkMenuActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1281
    const/high16 v0, 0x14000000

    .line 1284
    .local v0, "intentFlags":I
    sget-object v2, Lcom/android/stk/StkAppService$InitiatedByUserAction;->unknown:Lcom/android/stk/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v2}, Lcom/android/stk/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk/StkAppService$InitiatedByUserAction;)I

    move-result v2

    or-int/2addr v0, v2

    .line 1286
    const-string v2, "STATE"

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1287
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1288
    iget-object v2, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1289
    return-void
.end method

.method private getFlagActivityNoUserAction(Lcom/android/stk/StkAppService$InitiatedByUserAction;)I
    .locals 3
    .param p1, "userAction"    # Lcom/android/stk/StkAppService$InitiatedByUserAction;

    .prologue
    const/4 v1, 0x0

    .line 1254
    sget-object v0, Lcom/android/stk/StkAppService$InitiatedByUserAction;->yes:Lcom/android/stk/StkAppService$InitiatedByUserAction;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-boolean v2, p0, Lcom/android/stk/StkAppService;->mMenuIsVisibile:Z

    or-int/2addr v0, v2

    if-eqz v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/high16 v1, 0x40000

    goto :goto_1
.end method

.method static getInstance()Lcom/android/stk/StkAppService;
    .locals 1

    .prologue
    .line 367
    sget-object v0, Lcom/android/stk/StkAppService;->sInstance:Lcom/android/stk/StkAppService;

    return-object v0
.end method

.method private getItemName(I)Ljava/lang/String;
    .locals 5
    .param p1, "itemId"    # I

    .prologue
    const/4 v3, 0x0

    .line 1671
    iget-object v4, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v4}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v2

    .line 1672
    .local v2, "menu":Lcom/android/internal/telephony/cat/Menu;
    if-nez v2, :cond_1

    .line 1680
    :cond_0
    :goto_0
    return-object v3

    .line 1675
    :cond_1
    iget-object v4, v2, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/telephony/cat/Item;

    .line 1676
    .local v1, "item":Lcom/android/internal/telephony/cat/Item;
    iget v4, v1, Lcom/android/internal/telephony/cat/Item;->id:I

    if-ne v4, p1, :cond_2

    .line 1677
    iget-object v3, v1, Lcom/android/internal/telephony/cat/Item;->text:Ljava/lang/String;

    goto :goto_0
.end method

.method private handleCmd(Lcom/android/internal/telephony/cat/CatCmdMessage;)V
    .locals 14
    .param p1, "cmdMsg"    # Lcom/android/internal/telephony/cat/CatCmdMessage;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    const/16 v11, 0x18

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 731
    if-nez p1, :cond_1

    .line 937
    :cond_0
    :goto_0
    return-void

    .line 735
    :cond_1
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-nez v7, :cond_3

    .line 736
    const-string v7, "DCG"

    const-string v8, "GG"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "DCGG"

    const-string v8, "GG"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "DCGS"

    const-string v8, "GG"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "DCGGS"

    const-string v8, "GG"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "CG"

    const-string v8, "GG"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 742
    :cond_2
    invoke-static {v9}, Lcom/android/internal/telephony/cat/CatService;->getInstance(I)Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v7

    iput-object v7, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    .line 746
    :goto_1
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-nez v7, :cond_3

    .line 747
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 748
    .local v0, "args":Landroid/os/Bundle;
    const-string v7, "op"

    invoke-virtual {v0, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 749
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->TERMINAL_CRNTLY_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

    .line 750
    .local v5, "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    const-string v7, "response id"

    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 751
    const-string v7, "error code"

    invoke-virtual {v5}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 752
    const-string v7, "additional info"

    invoke-virtual {v0, v7, v12}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 753
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/android/stk/StkAppService;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v7, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/stk/StkAppService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 758
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v5    # "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    :cond_3
    iput-object p1, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    .line 759
    const/4 v6, 0x1

    .line 762
    .local v6, "waitForUsersResponse":Z
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 763
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v7

    sget-object v8, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_MENU:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    if-eq v7, v8, :cond_4

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v7

    sget-object v8, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SELECT_ITEM:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    if-ne v7, v8, :cond_7

    .line 765
    :cond_4
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->unlockMenuActivityBlock()V

    .line 770
    :goto_2
    sget-object v7, Lcom/android/stk/StkAppService$2;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 930
    :cond_5
    :goto_3
    if-nez v6, :cond_0

    .line 931
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-eqz v7, :cond_14

    .line 932
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->callDelayedMsg()V

    goto/16 :goto_0

    .line 744
    .end local v6    # "waitForUsersResponse":Z
    :cond_6
    invoke-static {}, Lcom/android/internal/telephony/cat/CatService;->getInstance()Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v7

    iput-object v7, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    goto :goto_1

    .line 767
    .restart local v6    # "waitForUsersResponse":Z
    :cond_7
    invoke-virtual {p0}, Lcom/android/stk/StkAppService;->lockMenuActivityBlock()V

    goto :goto_2

    .line 772
    :pswitch_0
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v4

    .line 773
    .local v4, "msg":Lcom/android/internal/telephony/cat/TextMessage;
    iget-boolean v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->responseNeeded:Z

    iput-boolean v7, p0, Lcom/android/stk/StkAppService;->responseNeeded:Z

    .line 774
    iget-object v7, p0, Lcom/android/stk/StkAppService;->lastSelectedItem:Ljava/lang/String;

    if-eqz v7, :cond_8

    .line 775
    iget-object v7, p0, Lcom/android/stk/StkAppService;->lastSelectedItem:Ljava/lang/String;

    iput-object v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    .line 784
    :goto_4
    iget-boolean v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->isHighPriority:Z

    if-eqz v7, :cond_a

    .line 785
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->launchTextDialog()V

    goto :goto_3

    .line 776
    :cond_8
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    if-eqz v7, :cond_9

    .line 777
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v7

    iget-object v7, v7, Lcom/android/internal/telephony/cat/Menu;->title:Ljava/lang/String;

    iput-object v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    goto :goto_4

    .line 780
    :cond_9
    const-string v7, ""

    iput-object v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    goto :goto_4

    .line 786
    :cond_a
    iget-boolean v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->isHighPriority:Z

    if-nez v7, :cond_5

    .line 787
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->canLaunchDisptextDialog()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 788
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->launchTextDialog()V

    goto :goto_3

    .line 791
    :cond_b
    const-string v7, "Can not display Normal Priority text"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 792
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 793
    .restart local v0    # "args":Landroid/os/Bundle;
    const-string v7, "op"

    invoke-virtual {v0, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 794
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->TERMINAL_CRNTLY_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

    .line 795
    .restart local v5    # "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    const-string v7, "response id"

    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 796
    const-string v7, "error code"

    invoke-virtual {v5}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 797
    const-string v7, "additional info"

    invoke-virtual {v0, v7, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 798
    const-string v7, "additional info data"

    invoke-virtual {v0, v7, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 799
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/android/stk/StkAppService;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v7, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/stk/StkAppService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_3

    .line 805
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v4    # "msg":Lcom/android/internal/telephony/cat/TextMessage;
    .end local v5    # "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    :pswitch_1
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v7

    iput-object v7, p0, Lcom/android/stk/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 806
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/stk/StkAppService;->launchMenuActivity(Lcom/android/internal/telephony/cat/Menu;)V

    goto/16 :goto_3

    .line 809
    :pswitch_2
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    iput-object v7, p0, Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    .line 810
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v7

    iput-object v7, p0, Lcom/android/stk/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 812
    const-string v7, "DCG"

    const-string v8, "GG"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_c

    const-string v7, "DCGG"

    const-string v8, "GG"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_c

    const-string v7, "DCGS"

    const-string v8, "GG"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_c

    const-string v7, "DCGGS"

    const-string v8, "GG"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_c

    const-string v7, "CG"

    const-string v8, "GG"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 818
    :cond_c
    invoke-static {v9}, Lcom/android/internal/telephony/cat/CatService;->getInstance(I)Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v7

    iput-object v7, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    .line 823
    :goto_5
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->removeMenu()Z

    move-result v7

    if-eqz v7, :cond_10

    .line 824
    const-string v7, "Uninstall App"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 825
    iput-object v13, p0, Lcom/android/stk/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 826
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/android/stk/StkAppInstaller;->unInstall(Landroid/content/Context;)V

    .line 841
    :cond_d
    :goto_6
    iget-boolean v7, p0, Lcom/android/stk/StkAppService;->mMenuIsVisibile:Z

    if-eqz v7, :cond_e

    .line 842
    invoke-direct {p0, v13}, Lcom/android/stk/StkAppService;->launchMenuActivity(Lcom/android/internal/telephony/cat/Menu;)V

    .line 845
    :cond_e
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 846
    .restart local v0    # "args":Landroid/os/Bundle;
    const-string v7, "op"

    invoke-virtual {v0, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 847
    const-string v7, "response id"

    const/16 v8, 0xe

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 848
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/android/stk/StkAppService;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v7, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/stk/StkAppService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 849
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-eqz v7, :cond_5

    iget-boolean v7, p0, Lcom/android/stk/StkAppService;->mIsSystemShutdown:Z

    if-nez v7, :cond_5

    .line 850
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v7, v9}, Lcom/android/internal/telephony/cat/AppInterface;->sentTerminalResponseForSetupMenu(Z)V

    goto/16 :goto_3

    .line 820
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_f
    invoke-static {}, Lcom/android/internal/telephony/cat/CatService;->getInstance()Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v7

    iput-object v7, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    goto :goto_5

    .line 828
    :cond_10
    const-string v7, "Install App"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 830
    const-string v7, "CHU"

    iget-object v8, p0, Lcom/android/stk/StkAppService;->salesCode:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 831
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "phone1_on"

    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 832
    .local v2, "mSimActive":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[STK]DB in Settings, mSimActive="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 833
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    if-eqz v7, :cond_d

    if-ne v9, v2, :cond_d

    .line 834
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/android/stk/StkAppInstaller;->install(Landroid/content/Context;)V

    goto :goto_6

    .line 838
    .end local v2    # "mSimActive":I
    :cond_11
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/android/stk/StkAppInstaller;->install(Landroid/content/Context;)V

    goto :goto_6

    .line 854
    :pswitch_3
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->launchInputActivity()V

    goto/16 :goto_3

    .line 857
    :pswitch_4
    const/4 v6, 0x0

    .line 859
    goto/16 :goto_3

    .line 864
    :pswitch_5
    const/4 v6, 0x0

    .line 865
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->launchEventMessage()V

    goto/16 :goto_3

    .line 868
    :pswitch_6
    const/4 v6, 0x0

    .line 869
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v7

    const-string v8, "CscFeature_RIL_RemoveToastDuringStkRefresh"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 870
    const-string v7, "Do not display a toast for SIM Refresh"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 873
    :pswitch_7
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getBrowserSettings()Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    move-result-object v7

    iget-object v7, v7, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->mode:Lcom/android/internal/telephony/cat/LaunchBrowserMode;

    sget-object v8, Lcom/android/internal/telephony/cat/LaunchBrowserMode;->LAUNCH_IF_NOT_ALREADY_LAUNCHED:Lcom/android/internal/telephony/cat/LaunchBrowserMode;

    if-ne v7, v8, :cond_12

    invoke-direct {p0}, Lcom/android/stk/StkAppService;->canLaunchBrowser()Z

    move-result v7

    if-nez v7, :cond_12

    .line 874
    const-string v7, "Launch Browser mode is LAUNCH_IF_NOT_ALREADY_LAUNCHED and browser is already launched"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 875
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 876
    .local v1, "argsBrowser":Landroid/os/Bundle;
    const-string v7, "op"

    invoke-virtual {v1, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 877
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->LAUNCH_BROWSER_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    .line 878
    .restart local v5    # "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    const-string v7, "response id"

    invoke-virtual {v1, v7, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 879
    const-string v7, "error code"

    invoke-virtual {v5}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    move-result v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 880
    const-string v7, "additional info"

    invoke-virtual {v1, v7, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 881
    const-string v7, "additional info data"

    invoke-virtual {v1, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 882
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/android/stk/StkAppService;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v7, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/stk/StkAppService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_3

    .line 885
    .end local v1    # "argsBrowser":Landroid/os/Bundle;
    .end local v5    # "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    :cond_12
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v7

    iput-boolean v9, v7, Lcom/android/internal/telephony/cat/TextMessage;->userClear:Z

    .line 886
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/stk/StkAppService;->launchConfirmationDialog(Lcom/android/internal/telephony/cat/TextMessage;)V

    goto/16 :goto_3

    .line 890
    :pswitch_8
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCallSettings()Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;

    move-result-object v7

    iget-object v7, v7, Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;->confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v7, v7, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    if-nez v7, :cond_13

    .line 892
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    const v8, 0x7f06000e

    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 893
    .local v3, "message":Ljava/lang/CharSequence;
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCallSettings()Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;

    move-result-object v7

    iget-object v7, v7, Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;->confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    .line 896
    .end local v3    # "message":Ljava/lang/CharSequence;
    :cond_13
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCallSettings()Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;

    move-result-object v7

    iget-object v7, v7, Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;->confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;

    invoke-direct {p0, v7}, Lcom/android/stk/StkAppService;->launchConfirmationDialog(Lcom/android/internal/telephony/cat/TextMessage;)V

    goto/16 :goto_3

    .line 899
    :pswitch_9
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->launchToneDialog()V

    goto/16 :goto_3

    .line 902
    :pswitch_a
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getNumberOfEventList()I

    move-result v7

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getEventList()[I

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/android/stk/StkAppService;->processSetEventList(I[I)V

    goto/16 :goto_3

    .line 905
    :pswitch_b
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getinitLanguage()Z

    move-result v8

    invoke-direct {p0, v7, v8}, Lcom/android/stk/StkAppService;->processLanguageNotification(Ljava/lang/String;Z)V

    goto/16 :goto_3

    .line 908
    :pswitch_c
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/stk/StkAppService;->launchConfirmationDialog(Lcom/android/internal/telephony/cat/TextMessage;)V

    .line 909
    const-string v7, "OPEN CHANNEL"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 913
    :pswitch_d
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/stk/StkAppService;->launchConfirmationDialog(Lcom/android/internal/telephony/cat/TextMessage;)V

    .line 914
    const-string v7, "CLOSE CHANNEL"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 921
    :pswitch_e
    const/4 v6, 0x0

    .line 922
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->launchEventMessage()V

    .line 923
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 934
    :cond_14
    iput-boolean v12, p0, Lcom/android/stk/StkAppService;->mCmdInProgress:Z

    goto/16 :goto_0

    .line 770
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_e
    .end packed-switch
.end method

.method private handleCmdResponse(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "args"    # Landroid/os/Bundle;

    .prologue
    const/4 v12, 0x1

    .line 1088
    iget-object v10, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    if-nez v10, :cond_1

    .line 1240
    :cond_0
    :goto_0
    return-void

    .line 1091
    :cond_1
    new-instance v8, Lcom/android/internal/telephony/cat/CatResponseMessage;

    iget-object v10, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-direct {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;-><init>(Lcom/android/internal/telephony/cat/CatCmdMessage;)V

    .line 1094
    .local v8, "resMsg":Lcom/android/internal/telephony/cat/CatResponseMessage;
    const-string v10, "help"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 1096
    .local v5, "helpRequired":Z
    const-string v10, "response id"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 1221
    :pswitch_0
    const-string v10, "Unknown result id"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1098
    :pswitch_1
    const-string v10, "RES_ID_MENU_SELECTION"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1099
    const-string v10, "menu selection"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 1100
    .local v7, "menuSelection":I
    sget-object v10, Lcom/android/stk/StkAppService$2;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    iget-object v11, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v11}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_1

    .line 1225
    .end local v7    # "menuSelection":I
    :cond_2
    :goto_1
    :pswitch_2
    iget-object v10, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-nez v10, :cond_4

    .line 1226
    const-string v10, "DCG"

    const-string v11, "GG"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "DCGG"

    const-string v11, "GG"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "DCGS"

    const-string v11, "GG"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "DCGGS"

    const-string v11, "GG"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "CG"

    const-string v11, "GG"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 1232
    :cond_3
    invoke-static {v12}, Lcom/android/internal/telephony/cat/CatService;->getInstance(I)Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v10

    iput-object v10, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    .line 1237
    :cond_4
    :goto_2
    iget-object v10, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-eqz v10, :cond_0

    .line 1238
    iget-object v10, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v10, v8}, Lcom/android/internal/telephony/cat/AppInterface;->onCmdResponse(Lcom/android/internal/telephony/cat/CatResponseMessage;)V

    goto :goto_0

    .line 1103
    .restart local v7    # "menuSelection":I
    :pswitch_3
    invoke-direct {p0, v7}, Lcom/android/stk/StkAppService;->getItemName(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/android/stk/StkAppService;->lastSelectedItem:Ljava/lang/String;

    .line 1104
    if-eqz v5, :cond_5

    .line 1105
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->HELP_INFO_REQUIRED:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1109
    :goto_3
    invoke-virtual {v8, v7}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setMenuSelection(I)V

    goto :goto_1

    .line 1107
    :cond_5
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto :goto_3

    .line 1116
    .end local v7    # "menuSelection":I
    :pswitch_4
    const-string v10, "RES_ID_INPUT"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1117
    const-string v10, "input"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1118
    .local v6, "input":Ljava/lang/String;
    iget-object v10, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v10}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geInput()Lcom/android/internal/telephony/cat/Input;

    move-result-object v2

    .line 1119
    .local v2, "cmdInput":Lcom/android/internal/telephony/cat/Input;
    if-eqz v2, :cond_6

    iget-boolean v10, v2, Lcom/android/internal/telephony/cat/Input;->yesNo:Z

    if-eqz v10, :cond_6

    .line 1120
    const-string v10, "YES"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 1122
    .local v9, "yesNoSelection":Z
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setYesNo(Z)V

    goto :goto_1

    .line 1124
    .end local v9    # "yesNoSelection":Z
    :cond_6
    if-eqz v5, :cond_7

    .line 1125
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->HELP_INFO_REQUIRED:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    .line 1127
    :cond_7
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1128
    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setInput(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1133
    .end local v2    # "cmdInput":Lcom/android/internal/telephony/cat/Input;
    .end local v6    # "input":Ljava/lang/String;
    :pswitch_5
    const-string v10, "RES_ID_CONFIRM"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1134
    const-string v10, "confirm"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 1135
    .local v3, "confirmed":Z
    sget-object v10, Lcom/android/stk/StkAppService$2;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    iget-object v11, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v11}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_2

    :pswitch_6
    goto/16 :goto_1

    .line 1137
    :pswitch_7
    if-eqz v3, :cond_8

    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    :goto_4
    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    :cond_8
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->UICC_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    goto :goto_4

    .line 1141
    :pswitch_8
    if-eqz v3, :cond_9

    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    :goto_5
    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1143
    if-eqz v3, :cond_2

    .line 1144
    iput-boolean v12, p0, Lcom/android/stk/StkAppService;->launchBrowser:Z

    .line 1145
    iget-object v10, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v10}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getBrowserSettings()Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    move-result-object v10

    iput-object v10, p0, Lcom/android/stk/StkAppService;->mBrowserSettings:Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    goto/16 :goto_1

    .line 1141
    :cond_9
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->UICC_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    goto :goto_5

    .line 1149
    :pswitch_9
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1150
    invoke-virtual {v8, v3}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setConfirmation(Z)V

    .line 1151
    if-eqz v3, :cond_2

    .line 1152
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->launchCallMsg()V

    goto/16 :goto_1

    .line 1156
    :pswitch_a
    const-string v10, "Open Channel Command response"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1157
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1158
    invoke-virtual {v8, v3}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setConfirmation(Z)V

    .line 1159
    if-eqz v3, :cond_a

    .line 1160
    const-string v10, "User Confirmed"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1161
    const-string v10, "OPEN CHANNEL"

    invoke-direct {p0, v10}, Lcom/android/stk/StkAppService;->launchBIPChannel(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1163
    :cond_a
    const-string v10, "User did not Confirm!"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1167
    :pswitch_b
    const-string v10, "Close Channel Command response"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1168
    if-eqz v3, :cond_b

    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    :goto_6
    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1170
    invoke-virtual {v8, v3}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setConfirmation(Z)V

    .line 1171
    if-eqz v3, :cond_c

    .line 1172
    const-string v10, "User Confirmed"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1173
    const-string v10, "CLOSE CHANNEL"

    invoke-direct {p0, v10}, Lcom/android/stk/StkAppService;->launchBIPChannel(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1168
    :cond_b
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->UICC_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    goto :goto_6

    .line 1175
    :cond_c
    const-string v10, "User did not Confirm!"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1183
    .end local v3    # "confirmed":Z
    :pswitch_c
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    .line 1186
    :pswitch_d
    const-string v10, "RES_ID_BACKWARD"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1187
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->BACKWARD_MOVE_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    .line 1190
    :pswitch_e
    const-string v10, "RES_ID_END_SESSION"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1191
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->UICC_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    .line 1194
    :pswitch_f
    const-string v10, "RES_ID_TIMEOUT"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1199
    iget-object v10, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v10}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->value()I

    move-result v10

    sget-object v11, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->DISPLAY_TEXT:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    invoke-virtual {v11}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->value()I

    move-result v11

    if-ne v10, v11, :cond_d

    iget-object v10, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v10}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v10

    iget-boolean v10, v10, Lcom/android/internal/telephony/cat/TextMessage;->userClear:Z

    if-nez v10, :cond_d

    .line 1202
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    .line 1204
    :cond_d
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->NO_RESPONSE_FROM_USER:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    .line 1208
    :pswitch_10
    const-string v10, "RES_ID_GENERAL_ERROR"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1209
    const-string v10, "error code"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 1210
    .local v4, "errorCode":I
    const-string v10, "additional info"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1213
    .local v0, "additionalInfo":Z
    invoke-static {v4}, Lcom/android/internal/telephony/cat/ResultCode;->fromInt(I)Lcom/android/internal/telephony/cat/ResultCode;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1214
    invoke-virtual {v8, v0}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setAdditionalInfo(Z)V

    .line 1215
    if-ne v0, v12, :cond_2

    .line 1216
    const-string v10, "additional info data"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1217
    .local v1, "additionalInfoData":I
    invoke-virtual {v8, v1}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setAdditionalInfoData(I)V

    goto/16 :goto_1

    .line 1234
    .end local v0    # "additionalInfo":Z
    .end local v1    # "additionalInfoData":I
    .end local v4    # "errorCode":I
    :cond_e
    invoke-static {}, Lcom/android/internal/telephony/cat/CatService;->getInstance()Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v10

    iput-object v10, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    goto/16 :goto_2

    .line 1096
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_10
    .end packed-switch

    .line 1100
    :pswitch_data_1
    .packed-switch 0x6
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 1135
    :pswitch_data_2
    .packed-switch 0x8
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private handleDelayedCmd()V
    .locals 2

    .prologue
    .line 606
    iget-object v1, p0, Lcom/android/stk/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 607
    iget-object v1, p0, Lcom/android/stk/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/stk/StkAppService$DelayedCmd;

    .line 608
    .local v0, "cmd":Lcom/android/stk/StkAppService$DelayedCmd;
    iget v1, v0, Lcom/android/stk/StkAppService$DelayedCmd;->id:I

    packed-switch v1, :pswitch_data_0

    .line 619
    .end local v0    # "cmd":Lcom/android/stk/StkAppService$DelayedCmd;
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 610
    .restart local v0    # "cmd":Lcom/android/stk/StkAppService$DelayedCmd;
    :pswitch_1
    iget-object v1, v0, Lcom/android/stk/StkAppService$DelayedCmd;->msg:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-direct {p0, v1}, Lcom/android/stk/StkAppService;->handleCmd(Lcom/android/internal/telephony/cat/CatCmdMessage;)V

    goto :goto_0

    .line 613
    :pswitch_2
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->handleSessionEnd()V

    goto :goto_0

    .line 608
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private handleEvent(Lcom/android/internal/telephony/cat/CatEventDownload;)V
    .locals 10
    .param p1, "event"    # Lcom/android/internal/telephony/cat/CatEventDownload;

    .prologue
    const/16 v9, 0x82

    const/16 v8, 0x81

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 623
    const/4 v0, 0x0

    .line 625
    .local v0, "additionalInfo":[B
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getEvent()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_0

    iget-boolean v3, p0, Lcom/android/stk/StkAppService;->mIsProxyChanged:Z

    if-ne v3, v6, :cond_0

    .line 627
    const-string v3, "received browser termination event and proxy was chaned, restore proxy"

    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 628
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 629
    .local v1, "cm":Landroid/net/ConnectivityManager;
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mBackupProxy:Landroid/net/ProxyInfo;

    invoke-virtual {v1, v3}, Landroid/net/ConnectivityManager;->setGlobalProxy(Landroid/net/ProxyInfo;)V

    .line 630
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/stk/StkAppService;->mBackupProxy:Landroid/net/ProxyInfo;

    .line 631
    iput-boolean v5, p0, Lcom/android/stk/StkAppService;->mIsProxyChanged:Z

    .line 634
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    :cond_0
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mSetEventList:[Z

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getEvent()I

    move-result v4

    aget-boolean v3, v3, v4

    if-ne v3, v6, :cond_1

    .line 635
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getEvent()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 677
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 637
    :pswitch_1
    const-string v3, "send user activity event to RIL"

    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 638
    new-instance v2, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-direct {v2, v3, v9, v8, v4}, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;-><init>(III[B)V

    .line 640
    .local v2, "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v3, v2}, Lcom/android/internal/telephony/cat/AppInterface;->onEventDownload(Lcom/android/internal/telephony/cat/CatEnvelopeMessage;)V

    .line 641
    const-string v3, "gsm.sim.userEvent"

    const-string v4, "0"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mSetEventList:[Z

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getEvent()I

    move-result v4

    aput-boolean v5, v3, v4

    goto :goto_0

    .line 645
    .end local v2    # "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    :pswitch_2
    const-string v3, "send Idle screen event to RIL"

    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 646
    new-instance v2, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;

    const/4 v3, 0x5

    const/4 v4, 0x0

    invoke-direct {v2, v3, v7, v8, v4}, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;-><init>(III[B)V

    .line 648
    .restart local v2    # "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v3, v2}, Lcom/android/internal/telephony/cat/AppInterface;->onEventDownload(Lcom/android/internal/telephony/cat/CatEnvelopeMessage;)V

    .line 649
    const-string v3, "gsm.sim.screenEvent"

    const-string v4, "0"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mSetEventList:[Z

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getEvent()I

    move-result v4

    aput-boolean v5, v3, v4

    goto :goto_0

    .line 653
    .end local v2    # "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    :pswitch_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "send setting language event to RIL, language = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 654
    const/4 v3, 0x4

    new-array v0, v3, [B

    .line 655
    const/16 v3, 0x2d

    aput-byte v3, v0, v5

    .line 656
    aput-byte v7, v0, v6

    .line 657
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v7

    .line 658
    const/4 v3, 0x3

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 659
    new-instance v2, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;

    const/4 v3, 0x7

    invoke-direct {v2, v3, v9, v8, v0}, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;-><init>(III[B)V

    .line 661
    .restart local v2    # "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v3, v2}, Lcom/android/internal/telephony/cat/AppInterface;->onEventDownload(Lcom/android/internal/telephony/cat/CatEnvelopeMessage;)V

    goto/16 :goto_0

    .line 664
    .end local v2    # "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    :pswitch_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "browser termination event to RIL, cause = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getBrowserTerminationCause()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 665
    const/4 v3, 0x3

    new-array v0, v3, [B

    .line 666
    const/16 v3, -0x4c

    aput-byte v3, v0, v5

    .line 667
    aput-byte v6, v0, v6

    .line 668
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getBrowserTerminationCause()I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v7

    .line 669
    new-instance v2, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;

    const/16 v3, 0x8

    invoke-direct {v2, v3, v9, v8, v0}, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;-><init>(III[B)V

    .line 671
    .restart local v2    # "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v3, v2}, Lcom/android/internal/telephony/cat/AppInterface;->onEventDownload(Lcom/android/internal/telephony/cat/CatEnvelopeMessage;)V

    goto/16 :goto_0

    .line 635
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private handleSessionEnd()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 686
    const-string v0, "handleSessionEnd()"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 687
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    iput-object v0, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    .line 688
    iput-object v3, p0, Lcom/android/stk/StkAppService;->lastSelectedItem:Ljava/lang/String;

    .line 689
    const-string v0, "unlockMenuActivityBlock()"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 690
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->unlockMenuActivityBlock()V

    .line 694
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v0}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v0

    iput-object v0, p0, Lcom/android/stk/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 696
    iput-boolean v2, p0, Lcom/android/stk/StkAppService;->mIsMainMenu:Z

    .line 699
    :cond_0
    iget-boolean v0, p0, Lcom/android/stk/StkAppService;->mIsStartedByUser:Z

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/android/stk/StkAppService;->isRunningStk()Z

    move-result v0

    if-ne v0, v2, :cond_4

    .line 700
    const-string v0, "distroyMenuActivity"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 701
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->distroyMenuActivity()V

    .line 706
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-eqz v0, :cond_5

    .line 707
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->callDelayedMsg()V

    .line 712
    :goto_1
    iget-boolean v0, p0, Lcom/android/stk/StkAppService;->launchBrowser:Z

    if-eqz v0, :cond_2

    .line 713
    iput-boolean v1, p0, Lcom/android/stk/StkAppService;->launchBrowser:Z

    .line 714
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mBrowserSettings:Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    invoke-direct {p0, v0}, Lcom/android/stk/StkAppService;->launchBrowser(Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;)V

    .line 717
    :cond_2
    iget-object v0, p0, Lcom/android/stk/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 719
    :try_start_0
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 720
    const-string v0, "before release wakeup"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 721
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 724
    :cond_3
    iget-object v0, p0, Lcom/android/stk/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 727
    invoke-virtual {p0}, Lcom/android/stk/StkAppService;->enableKeyguard()V

    .line 728
    return-void

    .line 702
    :cond_4
    iget-boolean v0, p0, Lcom/android/stk/StkAppService;->mMenuIsVisibile:Z

    if-eqz v0, :cond_1

    .line 703
    invoke-direct {p0, v3}, Lcom/android/stk/StkAppService;->launchMenuActivity(Lcom/android/internal/telephony/cat/Menu;)V

    goto :goto_0

    .line 709
    :cond_5
    iput-boolean v1, p0, Lcom/android/stk/StkAppService;->mCmdInProgress:Z

    goto :goto_1

    .line 724
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/stk/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private isCmdInteractive(Lcom/android/internal/telephony/cat/CatCmdMessage;)Z
    .locals 2
    .param p1, "cmd"    # Lcom/android/internal/telephony/cat/CatCmdMessage;

    .prologue
    .line 590
    sget-object v0, Lcom/android/stk/StkAppService$2;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 601
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 598
    :pswitch_0
    const-string v0, "Command is Informative!"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 599
    const/4 v0, 0x0

    goto :goto_0

    .line 590
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private isRunningStk()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1051
    const-string v7, "isRunningStk"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1052
    invoke-virtual {p0}, Lcom/android/stk/StkAppService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    .line 1053
    .local v2, "iContext":Landroid/content/Context;
    const/4 v0, 0x0

    .line 1058
    .local v0, "am":Landroid/app/ActivityManager;
    const-string v7, "activity"

    invoke-virtual {v2, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "am":Landroid/app/ActivityManager;
    check-cast v0, Landroid/app/ActivityManager;

    .line 1059
    .restart local v0    # "am":Landroid/app/ActivityManager;
    if-nez v0, :cond_1

    .line 1060
    const-string v6, "Activity Manager is NULL"

    invoke-static {p0, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1083
    :cond_0
    :goto_0
    return v5

    .line 1064
    :cond_1
    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v4

    .line 1065
    .local v4, "runningTaskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v4, :cond_2

    .line 1067
    const-string v6, "runningTaskInfo == null"

    invoke-static {p0, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1071
    :cond_2
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1072
    .local v3, "runInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    const-string v7, "Getting first Running task info"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1074
    iget-object v7, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 1075
    .local v1, "className":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Value of class name : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1076
    iget-object v7, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.android.stk.StkMenuActivity"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1078
    const-string v5, "Class Name matches"

    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move v5, v6

    .line 1079
    goto :goto_0
.end method

.method private launchBIPChannel(Ljava/lang/String;)V
    .locals 5
    .param p1, "Str"    # Ljava/lang/String;

    .prologue
    .line 1735
    iget-object v2, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v0

    .line 1736
    .local v0, "msg":Lcom/android/internal/telephony/cat/TextMessage;
    iget-object v2, v0, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 1737
    :cond_0
    const-string v2, "Sending..."

    iput-object v2, v0, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    .line 1740
    :cond_1
    iput-object p1, v0, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    .line 1741
    const-string v2, "Launch BIP Channel"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1742
    iget-object v2, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v0, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 1744
    .local v1, "toast":Landroid/widget/Toast;
    const/16 v2, 0x50

    const/4 v3, 0x0

    const/16 v4, 0x32

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 1745
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1746
    return-void
.end method

.method private launchBrowser(Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;)V
    .locals 12
    .param p1, "settings"    # Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    .prologue
    const/high16 v11, 0x4000000

    .line 1383
    if-nez p1, :cond_0

    .line 1449
    :goto_0
    return-void

    .line 1387
    :cond_0
    const/4 v4, 0x0

    .line 1389
    .local v4, "intent":Landroid/content/Intent;
    iget-object v9, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    if-nez v9, :cond_1

    .line 1390
    const-string v9, "url is null, so try to get default url from browser"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1391
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v9, "android.intent.action.STK_BROWSER_GET_HOMEPAGE"

    invoke-direct {v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1392
    .restart local v4    # "intent":Landroid/content/Intent;
    iget-object v9, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 1396
    :cond_1
    iget-object v3, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->gatewayProxy:Ljava/lang/String;

    .line 1397
    .local v3, "gatewayProxy":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 1398
    const-string v9, "gateway/proxy informaion is in launch browser cmd, change proxy"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1399
    iget-object v9, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    const-string v10, "connectivity"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1400
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getGlobalProxy()Landroid/net/ProxyInfo;

    move-result-object v9

    iput-object v9, p0, Lcom/android/stk/StkAppService;->mBackupProxy:Landroid/net/ProxyInfo;

    .line 1401
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getProxy()Landroid/net/ProxyInfo;

    move-result-object v8

    .line 1402
    .local v8, "tempProxy":Landroid/net/ProxyInfo;
    const/16 v6, 0x50

    .line 1403
    .local v6, "port":I
    const-string v2, ""

    .line 1405
    .local v2, "exclusionList":Ljava/lang/String;
    if-eqz v8, :cond_2

    .line 1406
    invoke-virtual {v8}, Landroid/net/ProxyInfo;->getPort()I

    move-result v6

    .line 1407
    invoke-virtual {v8}, Landroid/net/ProxyInfo;->getExclusionListAsString()Ljava/lang/String;

    move-result-object v2

    .line 1409
    :cond_2
    new-instance v7, Landroid/net/ProxyInfo;

    invoke-direct {v7, v3, v6, v2}, Landroid/net/ProxyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1410
    .local v7, "proxy":Landroid/net/ProxyInfo;
    invoke-virtual {v0, v7}, Landroid/net/ConnectivityManager;->setGlobalProxy(Landroid/net/ProxyInfo;)V

    .line 1411
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/android/stk/StkAppService;->mIsProxyChanged:Z

    .line 1412
    const-string v9, "gsm.sim.browserEvent"

    const-string v10, "1"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1415
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    .end local v2    # "exclusionList":Ljava/lang/String;
    .end local v6    # "port":I
    .end local v7    # "proxy":Landroid/net/ProxyInfo;
    .end local v8    # "tempProxy":Landroid/net/ProxyInfo;
    :cond_3
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v9, "android.intent.action.VIEW"

    invoke-direct {v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1417
    .restart local v4    # "intent":Landroid/content/Intent;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "settings.url = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1418
    iget-object v9, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    const-string v10, "http://"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    iget-object v9, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    const-string v10, "https://"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    iget-object v9, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    const-string v10, "dss://"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1419
    :cond_4
    iget-object v9, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1425
    .local v1, "data":Landroid/net/Uri;
    :goto_1
    invoke-virtual {v4, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1427
    const/high16 v9, 0x10000000

    invoke-virtual {v4, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1428
    sget-object v9, Lcom/android/stk/StkAppService$2;->$SwitchMap$com$android$internal$telephony$cat$LaunchBrowserMode:[I

    iget-object v10, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->mode:Lcom/android/internal/telephony/cat/LaunchBrowserMode;

    invoke-virtual {v10}, Lcom/android/internal/telephony/cat/LaunchBrowserMode;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 1442
    :goto_2
    invoke-virtual {p0, v4}, Lcom/android/stk/StkAppService;->startActivity(Landroid/content/Intent;)V

    .line 1447
    const-wide/16 v10, 0x2710

    :try_start_0
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1448
    :catch_0
    move-exception v9

    goto/16 :goto_0

    .line 1421
    .end local v1    # "data":Landroid/net/Uri;
    :cond_5
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "http://"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1422
    .local v5, "modifiedUrl":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "modifiedUrl = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1423
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1    # "data":Landroid/net/Uri;
    goto :goto_1

    .line 1430
    .end local v5    # "modifiedUrl":Ljava/lang/String;
    :pswitch_0
    invoke-virtual {v4, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_2

    .line 1433
    :pswitch_1
    const/high16 v9, 0x8000000

    invoke-virtual {v4, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_2

    .line 1436
    :pswitch_2
    invoke-virtual {v4, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_2

    .line 1428
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private launchCallMsg()V
    .locals 6

    .prologue
    .line 1452
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v3}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCallSettings()Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;

    move-result-object v3

    iget-object v1, v3, Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;->callMsg:Lcom/android/internal/telephony/cat/TextMessage;

    .line 1455
    .local v1, "msg":Lcom/android/internal/telephony/cat/TextMessage;
    iget-object v3, v1, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 1456
    :cond_0
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    const v4, 0x7f06000e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1457
    .local v0, "message":Ljava/lang/CharSequence;
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    .line 1459
    .end local v0    # "message":Ljava/lang/CharSequence;
    :cond_1
    iget-object v3, p0, Lcom/android/stk/StkAppService;->lastSelectedItem:Ljava/lang/String;

    iput-object v3, v1, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    .line 1461
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v1, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 1463
    .local v2, "toast":Landroid/widget/Toast;
    const/16 v3, 0x50

    const/4 v4, 0x0

    const/16 v5, 0x32

    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/Toast;->setGravity(III)V

    .line 1464
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1465
    return-void
.end method

.method private launchConfirmationDialog(Lcom/android/internal/telephony/cat/TextMessage;)V
    .locals 3
    .param p1, "msg"    # Lcom/android/internal/telephony/cat/TextMessage;

    .prologue
    .line 1361
    sget-object v1, Lcom/android/stk/StkAppService$2;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    iget-object v2, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1369
    iget-object v1, p0, Lcom/android/stk/StkAppService;->lastSelectedItem:Ljava/lang/String;

    iput-object v1, p1, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    .line 1372
    :goto_0
    const-string v1, "Launch Dialog"

    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1373
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/stk/StkDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1374
    .local v0, "newIntent":Landroid/content/Intent;
    const/high16 v1, 0x50800000

    sget-object v2, Lcom/android/stk/StkAppService$InitiatedByUserAction;->unknown:Lcom/android/stk/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v2}, Lcom/android/stk/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk/StkAppService$InitiatedByUserAction;)I

    move-result v2

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1378
    const-string v1, "TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1379
    invoke-virtual {p0, v0}, Lcom/android/stk/StkAppService;->startActivity(Landroid/content/Intent;)V

    .line 1380
    return-void

    .line 1363
    .end local v0    # "newIntent":Landroid/content/Intent;
    :pswitch_0
    const-string v1, "OPEN CHANNEL"

    iput-object v1, p1, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    goto :goto_0

    .line 1366
    :pswitch_1
    const-string v1, "CLOSE CHANNEL"

    iput-object v1, p1, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    goto :goto_0

    .line 1361
    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private launchEventMessage()V
    .locals 11

    .prologue
    const/16 v10, 0x8

    .line 1321
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v2

    .line 1322
    .local v2, "msg":Lcom/android/internal/telephony/cat/TextMessage;
    if-eqz v2, :cond_0

    iget-object v7, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    if-nez v7, :cond_1

    .line 1358
    :cond_0
    :goto_0
    return-void

    .line 1325
    :cond_1
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v7

    if-nez v7, :cond_2

    .line 1326
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1327
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mServiceHandler:Lcom/android/stk/StkAppService$ServiceHandler;

    invoke-virtual {v7}, Lcom/android/stk/StkAppService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v6

    .line 1328
    .local v6, "wakelockMsg":Landroid/os/Message;
    iput v10, v6, Landroid/os/Message;->arg1:I

    .line 1329
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mServiceHandler:Lcom/android/stk/StkAppService$ServiceHandler;

    const-wide/16 v8, 0x2710

    invoke-virtual {v7, v6, v8, v9}, Lcom/android/stk/StkAppService$ServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1331
    .end local v6    # "wakelockMsg":Landroid/os/Message;
    :cond_2
    invoke-virtual {p0}, Lcom/android/stk/StkAppService;->disableKeyguard()V

    .line 1333
    new-instance v3, Landroid/widget/Toast;

    iget-object v7, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v3, v7}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    .line 1334
    .local v3, "toast":Landroid/widget/Toast;
    iget-object v7, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1336
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const/high16 v7, 0x7f030000

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 1337
    .local v5, "v":Landroid/view/View;
    const v7, 0x102000b

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1339
    .local v4, "tv":Landroid/widget/TextView;
    const v7, 0x1020006

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1341
    .local v1, "iv":Landroid/widget/ImageView;
    iget-object v7, v2, Lcom/android/internal/telephony/cat/TextMessage;->icon:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_3

    .line 1342
    iget-object v7, v2, Lcom/android/internal/telephony/cat/TextMessage;->icon:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1352
    :goto_1
    iget-object v7, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354
    invoke-virtual {v3, v5}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 1355
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Landroid/widget/Toast;->setDuration(I)V

    .line 1356
    const/16 v7, 0x50

    const/4 v8, 0x0

    const/16 v9, 0x32

    invoke-virtual {v3, v7, v8, v9}, Landroid/widget/Toast;->setGravity(III)V

    .line 1357
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1344
    :cond_3
    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private launchInputActivity()V
    .locals 3

    .prologue
    .line 1292
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1293
    .local v0, "newIntent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    sget-object v2, Lcom/android/stk/StkAppService$InitiatedByUserAction;->unknown:Lcom/android/stk/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v2}, Lcom/android/stk/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk/StkAppService$InitiatedByUserAction;)I

    move-result v2

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1295
    const-string v1, "com.android.stk"

    const-string v2, "com.android.stk.StkInputActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1296
    const-string v1, "INPUT"

    iget-object v2, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geInput()Lcom/android/internal/telephony/cat/Input;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1297
    iget-object v1, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1298
    return-void
.end method

.method private launchMenuActivity(Lcom/android/internal/telephony/cat/Menu;)V
    .locals 4
    .param p1, "menu"    # Lcom/android/internal/telephony/cat/Menu;

    .prologue
    .line 1259
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1260
    .local v1, "newIntent":Landroid/content/Intent;
    const-string v2, "com.android.stk"

    const-string v3, "com.android.stk.StkMenuActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1261
    const/high16 v0, 0x14000000

    .line 1263
    .local v0, "intentFlags":I
    if-nez p1, :cond_0

    .line 1265
    sget-object v2, Lcom/android/stk/StkAppService$InitiatedByUserAction;->yes:Lcom/android/stk/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v2}, Lcom/android/stk/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk/StkAppService$InitiatedByUserAction;)I

    move-result v2

    or-int/2addr v0, v2

    .line 1267
    const-string v2, "STATE"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1274
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1275
    iget-object v2, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1276
    return-void

    .line 1270
    :cond_0
    sget-object v2, Lcom/android/stk/StkAppService$InitiatedByUserAction;->unknown:Lcom/android/stk/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v2}, Lcom/android/stk/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk/StkAppService$InitiatedByUserAction;)I

    move-result v2

    or-int/2addr v0, v2

    .line 1272
    const-string v2, "STATE"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private launchTextDialog()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1301
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/android/stk/StkDialogActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1302
    .local v0, "newIntent":Landroid/content/Intent;
    const/high16 v2, 0x58800000    # 1.12589991E15f

    sget-object v3, Lcom/android/stk/StkAppService$InitiatedByUserAction;->unknown:Lcom/android/stk/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v3}, Lcom/android/stk/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk/StkAppService$InitiatedByUserAction;)I

    move-result v3

    or-int/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1307
    const-string v2, "TEXT"

    iget-object v3, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v3}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1309
    new-instance v1, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-direct {v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>()V

    .line 1310
    .local v1, "style":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    invoke-virtual {v1, v4}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 1311
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 1313
    invoke-virtual {p0, v0}, Lcom/android/stk/StkAppService;->startActivity(Landroid/content/Intent;)V

    .line 1314
    iget-object v2, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v2

    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->duration:Lcom/android/internal/telephony/cat/Duration;

    invoke-static {v2}, Lcom/android/stk/StkApp;->calculateDurationInMilis(Lcom/android/internal/telephony/cat/Duration;)I

    move-result v2

    const v3, 0x5b8d80

    if-ne v2, v3, :cond_0

    .line 1315
    const-string v2, "sustained text. It\'ll be delete with new display text"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1316
    iput-boolean v4, p0, Lcom/android/stk/StkAppService;->mCmdInProgress:Z

    .line 1318
    :cond_0
    return-void
.end method

.method private launchToneDialog()V
    .locals 3

    .prologue
    .line 1660
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/stk/ToneDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1661
    .local v0, "newIntent":Landroid/content/Intent;
    const/high16 v1, 0x50800000

    sget-object v2, Lcom/android/stk/StkAppService$InitiatedByUserAction;->unknown:Lcom/android/stk/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v2}, Lcom/android/stk/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk/StkAppService$InitiatedByUserAction;)I

    move-result v2

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1665
    const-string v1, "TEXT"

    iget-object v2, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1666
    const-string v1, "TONE"

    iget-object v2, p0, Lcom/android/stk/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getToneSettings()Lcom/android/internal/telephony/cat/ToneSettings;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1667
    invoke-virtual {p0, v0}, Lcom/android/stk/StkAppService;->startActivity(Landroid/content/Intent;)V

    .line 1668
    return-void
.end method

.method private processLanguageNotification(Ljava/lang/String;Z)V
    .locals 11
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "initLanguage"    # Z

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1592
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "processLanguageNotification language = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", init = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1593
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 1594
    .local v4, "handled":Ljava/lang/Boolean;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1595
    .local v1, "args":Landroid/os/Bundle;
    const-string v7, "op"

    const/4 v8, 0x2

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1596
    if-ne p2, v10, :cond_0

    .line 1598
    const-string v7, "Language info is null, and init language"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1599
    const-string p1, "en"

    .line 1601
    :cond_0
    if-nez p2, :cond_1

    if-nez p1, :cond_1

    .line 1603
    const-string v7, "Language info is null, and not init language"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1634
    :goto_0
    return-void

    .line 1607
    :cond_1
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 1608
    .local v0, "am":Landroid/app/IActivityManager;
    if-nez v0, :cond_2

    .line 1609
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1625
    .end local v0    # "am":Landroid/app/IActivityManager;
    :goto_1
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1626
    const-string v7, "response id"

    const/16 v8, 0xe

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1633
    :goto_2
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/android/stk/StkAppService;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v7, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/stk/StkAppService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 1611
    .restart local v0    # "am":Landroid/app/IActivityManager;
    :cond_2
    :try_start_1
    invoke-interface {v0}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 1612
    .local v2, "config":Landroid/content/res/Configuration;
    new-instance v5, Ljava/util/Locale;

    invoke-direct {v5, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 1613
    .local v5, "locale":Ljava/util/Locale;
    if-eqz v2, :cond_3

    if-nez v5, :cond_4

    .line 1614
    :cond_3
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_1

    .line 1616
    :cond_4
    iput-object v5, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1617
    const/4 v7, 0x1

    iput-boolean v7, v2, Landroid/content/res/Configuration;->userSetLocale:Z

    .line 1618
    invoke-interface {v0, v2}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1621
    .end local v0    # "am":Landroid/app/IActivityManager;
    .end local v2    # "config":Landroid/content/res/Configuration;
    .end local v5    # "locale":Ljava/util/Locale;
    :catch_0
    move-exception v3

    .line 1622
    .local v3, "e":Landroid/os/RemoteException;
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_1

    .line 1628
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_5
    sget-object v6, Lcom/android/internal/telephony/cat/ResultCode;->TERMINAL_CRNTLY_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

    .line 1629
    .local v6, "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    const-string v7, "response id"

    const/16 v8, 0x18

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1630
    const-string v7, "error code"

    invoke-virtual {v6}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    move-result v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1631
    const-string v7, "additional info"

    invoke-virtual {v1, v7, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2
.end method

.method private processSetEventList(I[I)V
    .locals 12
    .param p1, "numberOfEvents"    # I
    .param p2, "events"    # [I

    .prologue
    const/4 v11, 0x5

    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1514
    const-string v5, "processSetEventList"

    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1515
    const/4 v0, 0x1

    .line 1516
    .local v0, "allHandledEvent":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_2

    .line 1517
    aget v5, p2, v2

    sparse-switch v5, :sswitch_data_0

    .line 1516
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1519
    :sswitch_0
    const-string v5, "gsm.sim.userEvent"

    const-string v6, "1"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1520
    iget-object v5, p0, Lcom/android/stk/StkAppService;->mSetEventList:[Z

    aput-boolean v7, v5, v10

    goto :goto_1

    .line 1523
    :sswitch_1
    const-string v5, "gsm.sim.screenEvent"

    const-string v6, "1"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1524
    iget-object v5, p0, Lcom/android/stk/StkAppService;->mSetEventList:[Z

    aput-boolean v7, v5, v11

    goto :goto_1

    .line 1527
    :sswitch_2
    const-string v5, "gsm.sim.lenguageEvent"

    const-string v6, "1"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1528
    iget-object v5, p0, Lcom/android/stk/StkAppService;->mSetEventList:[Z

    const/4 v6, 0x7

    aput-boolean v7, v5, v6

    goto :goto_1

    .line 1531
    :sswitch_3
    const-string v5, "gsm.sim.browserEvent"

    const-string v6, "1"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1532
    iget-object v5, p0, Lcom/android/stk/StkAppService;->mSetEventList:[Z

    const/16 v6, 0x8

    aput-boolean v7, v5, v6

    goto :goto_1

    .line 1535
    :sswitch_4
    const-string v5, "processSetEventList data"

    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1536
    iget-object v5, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-eqz v5, :cond_0

    .line 1537
    iget-object v5, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v5, v7}, Lcom/android/internal/telephony/cat/AppInterface;->setEventListDataAvailable(Z)V

    goto :goto_1

    .line 1541
    :sswitch_5
    const-string v5, "processSetEventList channel"

    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1542
    iget-object v5, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-eqz v5, :cond_0

    .line 1543
    iget-object v5, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v5, v7}, Lcom/android/internal/telephony/cat/AppInterface;->setEventListChannelStatus(Z)V

    goto :goto_1

    .line 1547
    :sswitch_6
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    const/16 v5, 0x14

    if-ge v3, v5, :cond_1

    .line 1548
    iget-object v5, p0, Lcom/android/stk/StkAppService;->mSetEventList:[Z

    aput-boolean v8, v5, v3

    .line 1547
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1549
    :cond_1
    const-string v5, "gsm.sim.userEvent"

    const-string v6, "0"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1550
    const-string v5, "gsm.sim.screenEvent"

    const-string v6, "0"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1551
    const-string v5, "gsm.sim.lenguageEvent"

    const-string v6, "0"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1552
    const-string v5, "gsm.sim.browserEvent"

    const-string v6, "0"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1559
    .end local v3    # "j":I
    :cond_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1560
    .local v1, "args":Landroid/os/Bundle;
    const-string v5, "op"

    invoke-virtual {v1, v5, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1561
    const/4 v2, 0x0

    :goto_3
    if-ge v2, p1, :cond_3

    .line 1562
    aget v5, p2, v2

    if-eqz v5, :cond_4

    aget v5, p2, v2

    if-eq v5, v7, :cond_4

    aget v5, p2, v2

    if-eq v5, v9, :cond_4

    aget v5, p2, v2

    const/4 v6, 0x3

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    if-eq v5, v10, :cond_4

    aget v5, p2, v2

    if-eq v5, v11, :cond_4

    aget v5, p2, v2

    const/4 v6, 0x7

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    const/16 v6, 0x8

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    const/16 v6, 0xb

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    const/16 v6, 0x9

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    const/16 v6, 0xa

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    const/16 v6, 0xff

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    const/16 v6, 0x12

    if-eq v5, v6, :cond_4

    .line 1575
    const/4 v0, 0x0

    .line 1580
    :cond_3
    if-eqz v0, :cond_5

    .line 1581
    const-string v5, "response id"

    const/16 v6, 0xe

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1588
    :goto_4
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/android/stk/StkAppService;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/android/stk/StkAppService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1589
    return-void

    .line 1561
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1583
    :cond_5
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

    .line 1584
    .local v4, "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    const-string v5, "response id"

    const/16 v6, 0x18

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1585
    const-string v5, "error code"

    invoke-virtual {v4}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1586
    const-string v5, "additional info"

    invoke-virtual {v1, v5, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_4

    .line 1517
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x5 -> :sswitch_1
        0x7 -> :sswitch_2
        0x8 -> :sswitch_3
        0x9 -> :sswitch_4
        0xa -> :sswitch_5
        0xff -> :sswitch_6
    .end sparse-switch
.end method

.method private removeMenu()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1685
    :try_start_0
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-object v3, v3, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v1, :cond_0

    iget-object v3, p0, Lcom/android/stk/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-object v3, v3, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    if-nez v3, :cond_0

    .line 1693
    :goto_0
    return v1

    .line 1689
    :catch_0
    move-exception v0

    .line 1690
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v2, "Unable to get Menu\'s items size"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_0
    move v1, v2

    .line 1693
    goto :goto_0
.end method

.method private unlockMenuActivityBlock()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1711
    iget-boolean v0, p0, Lcom/android/stk/StkAppService;->mMenuItemBlock:Z

    if-nez v0, :cond_0

    .line 1720
    :goto_0
    return-void

    .line 1715
    :cond_0
    const-string v0, "unlockMenuActivityBlock"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1717
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/stk/StkAppService;->mMenuItemBlock:Z

    .line 1718
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1719
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "has message! : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/stk/StkAppService;->mMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private waitForLooper()V
    .locals 2

    .prologue
    .line 371
    :goto_0
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mServiceHandler:Lcom/android/stk/StkAppService$ServiceHandler;

    if-nez v0, :cond_0

    .line 372
    monitor-enter p0

    .line 374
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377
    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 375
    :catch_0
    move-exception v0

    goto :goto_1

    .line 379
    :cond_0
    return-void
.end method


# virtual methods
.method public clearmIsStartedByUser()V
    .locals 1

    .prologue
    .line 1749
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/stk/StkAppService;->mIsStartedByUser:Z

    .line 1750
    return-void
.end method

.method declared-synchronized disableKeyguard()V
    .locals 2

    .prologue
    .line 1704
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    if-nez v0, :cond_0

    .line 1705
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mKeyguardManager:Landroid/app/KeyguardManager;

    const-string v1, "STK"

    invoke-virtual {v0, v1}, Landroid/app/KeyguardManager;->newKeyguardLock(Ljava/lang/String;)Landroid/app/KeyguardManager$KeyguardLock;

    move-result-object v0

    iput-object v0, p0, Lcom/android/stk/StkAppService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    .line 1706
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    invoke-virtual {v0}, Landroid/app/KeyguardManager$KeyguardLock;->disableKeyguard()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1708
    :cond_0
    monitor-exit p0

    return-void

    .line 1704
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized enableKeyguard()V
    .locals 1

    .prologue
    .line 1697
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    if-eqz v0, :cond_0

    .line 1698
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    invoke-virtual {v0}, Landroid/app/KeyguardManager$KeyguardLock;->reenableKeyguard()V

    .line 1699
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/stk/StkAppService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1701
    :cond_0
    monitor-exit p0

    return-void

    .line 1697
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method getMainMenu()Lcom/android/internal/telephony/cat/Menu;
    .locals 2

    .prologue
    .line 355
    const/4 v0, 0x0

    .line 356
    .local v0, "mainMenu":Lcom/android/internal/telephony/cat/Menu;
    iget-object v1, p0, Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    if-eqz v1, :cond_0

    .line 357
    iget-object v1, p0, Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v0

    .line 359
    :cond_0
    return-object v0
.end method

.method getMenu()Lcom/android/internal/telephony/cat/Menu;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    return-object v0
.end method

.method indicateMenuVisibility(Z)V
    .locals 0
    .param p1, "visibility"    # Z

    .prologue
    .line 344
    iput-boolean p1, p0, Lcom/android/stk/StkAppService;->mMenuIsVisibile:Z

    .line 345
    return-void
.end method

.method lockMenuActivityBlock()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1723
    iget-boolean v0, p0, Lcom/android/stk/StkAppService;->mMenuItemBlock:Z

    if-ne v0, v2, :cond_0

    .line 1732
    :goto_0
    return-void

    .line 1727
    :cond_0
    const-string v0, "lockMenuActivityBlock"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1729
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1730
    iput-boolean v2, p0, Lcom/android/stk/StkAppService;->mMenuItemBlock:Z

    .line 1731
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mMessageHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/stk/StkAppService;->mMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 328
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x14

    .line 220
    const-string v3, "DCG"

    const-string v4, "GG"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "DCGG"

    const-string v4, "GG"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "DCGS"

    const-string v4, "GG"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "DCGGS"

    const-string v4, "GG"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "CG"

    const-string v4, "GG"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 226
    :cond_0
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/android/internal/telephony/cat/CatService;->getInstance(I)Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v3

    iput-object v3, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    .line 231
    :goto_0
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, p0, Lcom/android/stk/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    .line 232
    new-instance v2, Ljava/lang/Thread;

    const-string v3, "Stk App Service"

    invoke-direct {v2, v6, p0, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 234
    .local v2, "serviceThread":Ljava/lang/Thread;
    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/stk/StkAppService;->salesCode:Ljava/lang/String;

    .line 236
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 237
    invoke-virtual {p0}, Lcom/android/stk/StkAppService;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    .line 238
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;

    const-string v4, "notification"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    iput-object v3, p0, Lcom/android/stk/StkAppService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 240
    sput-object p0, Lcom/android/stk/StkAppService;->sInstance:Lcom/android/stk/StkAppService;

    .line 242
    const-string v3, "power"

    invoke-virtual {p0, v3}, Lcom/android/stk/StkAppService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 243
    .local v1, "pm":Landroid/os/PowerManager;
    const v3, 0x3000001a

    const-string v4, "STK"

    invoke-virtual {v1, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    iput-object v3, p0, Lcom/android/stk/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 247
    const-string v3, "keyguard"

    invoke-virtual {p0, v3}, Lcom/android/stk/StkAppService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/KeyguardManager;

    iput-object v3, p0, Lcom/android/stk/StkAppService;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 249
    new-array v3, v5, [Z

    iput-object v3, p0, Lcom/android/stk/StkAppService;->mSetEventList:[Z

    .line 250
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v5, :cond_2

    .line 251
    iget-object v3, p0, Lcom/android/stk/StkAppService;->mSetEventList:[Z

    const/4 v4, 0x0

    aput-boolean v4, v3, v0

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 228
    .end local v0    # "i":I
    .end local v1    # "pm":Landroid/os/PowerManager;
    .end local v2    # "serviceThread":Ljava/lang/Thread;
    :cond_1
    invoke-static {}, Lcom/android/internal/telephony/cat/CatService;->getInstance()Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v3

    iput-object v3, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    goto :goto_0

    .line 252
    .restart local v0    # "i":I
    .restart local v1    # "pm":Landroid/os/PowerManager;
    .restart local v2    # "serviceThread":Ljava/lang/Thread;
    :cond_2
    new-instance v3, Lcom/android/stk/StkAppService$MessageHandler;

    invoke-direct {v3, p0, v6}, Lcom/android/stk/StkAppService$MessageHandler;-><init>(Lcom/android/stk/StkAppService;Lcom/android/stk/StkAppService$1;)V

    iput-object v3, p0, Lcom/android/stk/StkAppService;->mMessageHandler:Landroid/os/Handler;

    .line 253
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 322
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->waitForLooper()V

    .line 323
    iget-object v0, p0, Lcom/android/stk/StkAppService;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 324
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 258
    const-string v2, "onStart)"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    invoke-direct {p0}, Lcom/android/stk/StkAppService;->waitForLooper()V

    .line 261
    const-string v2, "DCG"

    const-string v3, "GG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "DCGG"

    const-string v3, "GG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "DCGS"

    const-string v3, "GG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "DCGGS"

    const-string v3, "GG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "CG"

    const-string v3, "GG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 267
    :cond_0
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/android/internal/telephony/cat/CatService;->getInstance(I)Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v2

    iput-object v2, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    .line 274
    :goto_0
    if-nez p1, :cond_3

    .line 318
    :cond_1
    :goto_1
    return-void

    .line 269
    :cond_2
    invoke-static {}, Lcom/android/internal/telephony/cat/CatService;->getInstance()Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v2

    iput-object v2, p0, Lcom/android/stk/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    goto :goto_0

    .line 278
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 280
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 284
    iget-object v2, p0, Lcom/android/stk/StkAppService;->mServiceHandler:Lcom/android/stk/StkAppService$ServiceHandler;

    invoke-virtual {v2}, Lcom/android/stk/StkAppService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 285
    .local v1, "msg":Landroid/os/Message;
    const-string v2, "op"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 286
    iget v2, v1, Landroid/os/Message;->arg1:I

    sparse-switch v2, :sswitch_data_0

    goto :goto_1

    .line 288
    :sswitch_0
    const-string v2, "cmd message"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 313
    :goto_2
    :sswitch_1
    const-string v2, "Before SendMessage to ServiceHandler)"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 314
    iget-object v2, p0, Lcom/android/stk/StkAppService;->mServiceHandler:Lcom/android/stk/StkAppService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/android/stk/StkAppService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 317
    const-string v2, "After SendMessage to ServiceHandler)"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 293
    :sswitch_2
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_2

    .line 301
    :sswitch_3
    const-string v2, "event"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_2

    .line 305
    :sswitch_4
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_2

    .line 286
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x5 -> :sswitch_1
        0x7 -> :sswitch_3
        0x9 -> :sswitch_2
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x64 -> :sswitch_4
    .end sparse-switch
.end method

.method public run()V
    .locals 2

    .prologue
    .line 332
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 334
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/stk/StkAppService;->mServiceLooper:Landroid/os/Looper;

    .line 335
    new-instance v0, Lcom/android/stk/StkAppService$ServiceHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/stk/StkAppService$ServiceHandler;-><init>(Lcom/android/stk/StkAppService;Lcom/android/stk/StkAppService$1;)V

    iput-object v0, p0, Lcom/android/stk/StkAppService;->mServiceHandler:Lcom/android/stk/StkAppService$ServiceHandler;

    .line 337
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 338
    return-void
.end method
