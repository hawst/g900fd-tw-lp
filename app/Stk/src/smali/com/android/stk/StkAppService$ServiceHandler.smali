.class final Lcom/android/stk/StkAppService$ServiceHandler;
.super Landroid/os/Handler;
.source "StkAppService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/stk/StkAppService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/stk/StkAppService;


# direct methods
.method private constructor <init>(Lcom/android/stk/StkAppService;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/stk/StkAppService;Lcom/android/stk/StkAppService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/stk/StkAppService;
    .param p2, "x1"    # Lcom/android/stk/StkAppService$1;

    .prologue
    .line 399
    invoke-direct {p0, p1}, Lcom/android/stk/StkAppService$ServiceHandler;-><init>(Lcom/android/stk/StkAppService;)V

    return-void
.end method

.method private handleCardStatusChangeAndIccRefresh(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "args"    # Landroid/os/Bundle;

    .prologue
    .line 566
    const-string v1, "CTC"

    const-string v2, "ro.csc.sales_code"

    const-string v3, ""

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 567
    new-instance v0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;

    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/IccRefreshResponse;-><init>()V

    .line 568
    .local v0, "state":Lcom/android/internal/telephony/uicc/IccRefreshResponse;
    const-string v1, "refresh_result"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->refreshResult:I

    .line 569
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Icc Refresh Result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->refreshResult:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 570
    iget v1, v0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->refreshResult:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 571
    iget-object v1, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mMessageHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/android/stk/StkAppService;->access$1800(Lcom/android/stk/StkAppService;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/stk/StkAppService$ServiceHandler$1;

    invoke-direct {v2, p0}, Lcom/android/stk/StkAppService$ServiceHandler$1;-><init>(Lcom/android/stk/StkAppService$ServiceHandler;)V

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 586
    .end local v0    # "state":Lcom/android/internal/telephony/uicc/IccRefreshResponse;
    :cond_0
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x4

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 402
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 404
    .local v3, "opcode":I
    sparse-switch v3, :sswitch_data_0

    .line 532
    :cond_0
    :goto_0
    return-void

    .line 406
    :sswitch_0
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$200(Lcom/android/stk/StkAppService;)Lcom/android/internal/telephony/cat/CatCmdMessage;

    move-result-object v4

    if-nez v4, :cond_1

    .line 407
    const-string v4, "Invalid mMainCmd"

    invoke-static {p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 411
    :cond_1
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # setter for: Lcom/android/stk/StkAppService;->mIsStartedByUser:Z
    invoke-static {v4, v8}, Lcom/android/stk/StkAppService;->access$302(Lcom/android/stk/StkAppService;Z)Z

    .line 412
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # invokes: Lcom/android/stk/StkAppService;->launchMenuActivity(Lcom/android/internal/telephony/cat/Menu;)V
    invoke-static {v4, v7}, Lcom/android/stk/StkAppService;->access$400(Lcom/android/stk/StkAppService;Lcom/android/internal/telephony/cat/Menu;)V

    goto :goto_0

    .line 416
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/internal/telephony/cat/CatCmdMessage;

    .line 425
    .local v0, "cmdMsg":Lcom/android/internal/telephony/cat/CatCmdMessage;
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # invokes: Lcom/android/stk/StkAppService;->isCmdInteractive(Lcom/android/internal/telephony/cat/CatCmdMessage;)Z
    invoke-static {v4, v0}, Lcom/android/stk/StkAppService;->access$500(Lcom/android/stk/StkAppService;Lcom/android/internal/telephony/cat/CatCmdMessage;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 426
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # invokes: Lcom/android/stk/StkAppService;->handleCmd(Lcom/android/internal/telephony/cat/CatCmdMessage;)V
    invoke-static {v4, v0}, Lcom/android/stk/StkAppService;->access$600(Lcom/android/stk/StkAppService;Lcom/android/internal/telephony/cat/CatCmdMessage;)V

    goto :goto_0

    .line 428
    :cond_2
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mCmdInProgress:Z
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$700(Lcom/android/stk/StkAppService;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 429
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # setter for: Lcom/android/stk/StkAppService;->mCmdInProgress:Z
    invoke-static {v4, v8}, Lcom/android/stk/StkAppService;->access$702(Lcom/android/stk/StkAppService;Z)Z

    .line 430
    iget-object v5, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/android/internal/telephony/cat/CatCmdMessage;

    # invokes: Lcom/android/stk/StkAppService;->handleCmd(Lcom/android/internal/telephony/cat/CatCmdMessage;)V
    invoke-static {v5, v4}, Lcom/android/stk/StkAppService;->access$600(Lcom/android/stk/StkAppService;Lcom/android/internal/telephony/cat/CatCmdMessage;)V

    goto :goto_0

    .line 432
    :cond_3
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mCmdsQ:Ljava/util/LinkedList;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$800(Lcom/android/stk/StkAppService;)Ljava/util/LinkedList;

    move-result-object v5

    new-instance v6, Lcom/android/stk/StkAppService$DelayedCmd;

    iget-object v7, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-direct {v6, v7, v8, v4}, Lcom/android/stk/StkAppService$DelayedCmd;-><init>(Lcom/android/stk/StkAppService;ILcom/android/internal/telephony/cat/CatCmdMessage;)V

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_0

    .line 439
    .end local v0    # "cmdMsg":Lcom/android/internal/telephony/cat/CatCmdMessage;
    :sswitch_2
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->responseNeeded:Z
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$900(Lcom/android/stk/StkAppService;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 440
    iget-object v5, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/os/Bundle;

    # invokes: Lcom/android/stk/StkAppService;->handleCmdResponse(Landroid/os/Bundle;)V
    invoke-static {v5, v4}, Lcom/android/stk/StkAppService;->access$1000(Lcom/android/stk/StkAppService;Landroid/os/Bundle;)V

    .line 443
    :cond_4
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mCmdsQ:Ljava/util/LinkedList;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$800(Lcom/android/stk/StkAppService;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-eqz v4, :cond_5

    .line 444
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # invokes: Lcom/android/stk/StkAppService;->callDelayedMsg()V
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$1100(Lcom/android/stk/StkAppService;)V

    .line 449
    :goto_1
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # setter for: Lcom/android/stk/StkAppService;->responseNeeded:Z
    invoke-static {v4, v8}, Lcom/android/stk/StkAppService;->access$902(Lcom/android/stk/StkAppService;Z)Z

    goto :goto_0

    .line 446
    :cond_5
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    const/4 v5, 0x0

    # setter for: Lcom/android/stk/StkAppService;->mCmdInProgress:Z
    invoke-static {v4, v5}, Lcom/android/stk/StkAppService;->access$702(Lcom/android/stk/StkAppService;Z)Z

    goto :goto_1

    .line 453
    :sswitch_3
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mCmdInProgress:Z
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$700(Lcom/android/stk/StkAppService;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 454
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # setter for: Lcom/android/stk/StkAppService;->mCmdInProgress:Z
    invoke-static {v4, v8}, Lcom/android/stk/StkAppService;->access$702(Lcom/android/stk/StkAppService;Z)Z

    .line 455
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # invokes: Lcom/android/stk/StkAppService;->handleSessionEnd()V
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$1200(Lcom/android/stk/StkAppService;)V

    goto/16 :goto_0

    .line 457
    :cond_6
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mCmdsQ:Ljava/util/LinkedList;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$800(Lcom/android/stk/StkAppService;)Ljava/util/LinkedList;

    move-result-object v4

    new-instance v5, Lcom/android/stk/StkAppService$DelayedCmd;

    iget-object v6, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    invoke-direct {v5, v6, v9, v7}, Lcom/android/stk/StkAppService$DelayedCmd;-><init>(Lcom/android/stk/StkAppService;ILcom/android/internal/telephony/cat/CatCmdMessage;)V

    invoke-virtual {v4, v5}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 462
    :sswitch_4
    const-string v4, "OP_BOOT_COMPLETED"

    invoke-static {p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 463
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$200(Lcom/android/stk/StkAppService;)Lcom/android/internal/telephony/cat/CatCmdMessage;

    move-result-object v4

    if-nez v4, :cond_0

    .line 464
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$1300(Lcom/android/stk/StkAppService;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/stk/StkAppInstaller;->unInstall(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 470
    :sswitch_5
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/os/Bundle;

    const-string v5, "simcard_sim_activate"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v8, :cond_7

    .line 472
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$1300(Lcom/android/stk/StkAppService;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "phone1_on"

    invoke-static {v4, v5, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 473
    .local v2, "mSimActive":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Install App by user activation. mSimActive="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mMainCmd="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;
    invoke-static {v5}, Lcom/android/stk/StkAppService;->access$200(Lcom/android/stk/StkAppService;)Lcom/android/internal/telephony/cat/CatCmdMessage;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 474
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$200(Lcom/android/stk/StkAppService;)Lcom/android/internal/telephony/cat/CatCmdMessage;

    move-result-object v4

    if-eqz v4, :cond_0

    if-ne v8, v2, :cond_0

    .line 475
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$1300(Lcom/android/stk/StkAppService;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/stk/StkAppInstaller;->install(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 479
    .end local v2    # "mSimActive":I
    :cond_7
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mCmdInProgress:Z
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$700(Lcom/android/stk/StkAppService;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 480
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # setter for: Lcom/android/stk/StkAppService;->mCmdInProgress:Z
    invoke-static {v4, v8}, Lcom/android/stk/StkAppService;->access$702(Lcom/android/stk/StkAppService;Z)Z

    .line 481
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # invokes: Lcom/android/stk/StkAppService;->handleSessionEnd()V
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$1200(Lcom/android/stk/StkAppService;)V

    .line 485
    :goto_2
    const-string v4, "Uninstall App by user deactivation"

    invoke-static {p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 486
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$1300(Lcom/android/stk/StkAppService;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/stk/StkAppInstaller;->unInstall(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 483
    :cond_8
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mCmdsQ:Ljava/util/LinkedList;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$800(Lcom/android/stk/StkAppService;)Ljava/util/LinkedList;

    move-result-object v4

    new-instance v5, Lcom/android/stk/StkAppService$DelayedCmd;

    iget-object v6, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    invoke-direct {v5, v6, v9, v7}, Lcom/android/stk/StkAppService$DelayedCmd;-><init>(Lcom/android/stk/StkAppService;ILcom/android/internal/telephony/cat/CatCmdMessage;)V

    invoke-virtual {v4, v5}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_2

    .line 492
    :sswitch_6
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # invokes: Lcom/android/stk/StkAppService;->handleDelayedCmd()V
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$1400(Lcom/android/stk/StkAppService;)V

    goto/16 :goto_0

    .line 496
    :sswitch_7
    const-string v4, "Card/Icc Status change received"

    invoke-static {p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 497
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/os/Bundle;

    invoke-direct {p0, v4}, Lcom/android/stk/StkAppService$ServiceHandler;->handleCardStatusChangeAndIccRefresh(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 501
    :sswitch_8
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/internal/telephony/cat/CatEventDownload;

    .line 502
    .local v1, "event":Lcom/android/internal/telephony/cat/CatEventDownload;
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # invokes: Lcom/android/stk/StkAppService;->handleEvent(Lcom/android/internal/telephony/cat/CatEventDownload;)V
    invoke-static {v4, v1}, Lcom/android/stk/StkAppService;->access$1500(Lcom/android/stk/StkAppService;Lcom/android/internal/telephony/cat/CatEventDownload;)V

    goto/16 :goto_0

    .line 506
    .end local v1    # "event":Lcom/android/internal/telephony/cat/CatEventDownload;
    :sswitch_9
    const-string v4, "before ckecking timed release wakeup"

    invoke-static {p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 508
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    iget-object v4, v4, Lcom/android/stk/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 510
    :try_start_0
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    iget-object v4, v4, Lcom/android/stk/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 511
    const-string v4, "before timed release wakeup"

    invoke-static {p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 512
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    iget-object v4, v4, Lcom/android/stk/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 515
    :cond_9
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    iget-object v4, v4, Lcom/android/stk/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    iget-object v5, v5, Lcom/android/stk/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v4

    .line 520
    :sswitch_a
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mBrowserSettings:Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$1600(Lcom/android/stk/StkAppService;)Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    move-result-object v5

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/os/Bundle;

    const-string v6, "homepage"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    .line 521
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    iget-object v5, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mBrowserSettings:Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;
    invoke-static {v5}, Lcom/android/stk/StkAppService;->access$1600(Lcom/android/stk/StkAppService;)Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    move-result-object v5

    # invokes: Lcom/android/stk/StkAppService;->launchBrowser(Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;)V
    invoke-static {v4, v5}, Lcom/android/stk/StkAppService;->access$1700(Lcom/android/stk/StkAppService;Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;)V

    goto/16 :goto_0

    .line 525
    :sswitch_b
    const-string v4, "Uninstall App for ACTION_SHUTDOWN"

    invoke-static {p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 526
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    iput-boolean v8, v4, Lcom/android/stk/StkAppService;->mIsSystemShutdown:Z

    .line 527
    iget-object v4, p0, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$1300(Lcom/android/stk/StkAppService;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/stk/StkAppInstaller;->unInstall(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 404
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_0
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_6
        0x7 -> :sswitch_8
        0x8 -> :sswitch_9
        0x9 -> :sswitch_a
        0xa -> :sswitch_b
        0x11 -> :sswitch_7
        0x64 -> :sswitch_5
    .end sparse-switch
.end method
