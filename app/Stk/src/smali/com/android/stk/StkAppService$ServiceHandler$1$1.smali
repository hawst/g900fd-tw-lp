.class Lcom/android/stk/StkAppService$ServiceHandler$1$1;
.super Ljava/lang/Object;
.source "StkAppService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/stk/StkAppService$ServiceHandler$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/stk/StkAppService$ServiceHandler$1;


# direct methods
.method constructor <init>(Lcom/android/stk/StkAppService$ServiceHandler$1;)V
    .locals 0

    .prologue
    .line 573
    iput-object p1, p0, Lcom/android/stk/StkAppService$ServiceHandler$1$1;->this$2:Lcom/android/stk/StkAppService$ServiceHandler$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 575
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 576
    .local v0, "rebootIntent":Landroid/content/Intent;
    const-string v1, "android.intent.action.REBOOT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 577
    const-string v1, "android.intent.extra.KEY_CONFIRM"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 578
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 579
    iget-object v1, p0, Lcom/android/stk/StkAppService$ServiceHandler$1$1;->this$2:Lcom/android/stk/StkAppService$ServiceHandler$1;

    iget-object v1, v1, Lcom/android/stk/StkAppService$ServiceHandler$1;->this$1:Lcom/android/stk/StkAppService$ServiceHandler;

    iget-object v1, v1, Lcom/android/stk/StkAppService$ServiceHandler;->this$0:Lcom/android/stk/StkAppService;

    # getter for: Lcom/android/stk/StkAppService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/stk/StkAppService;->access$1300(Lcom/android/stk/StkAppService;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 580
    return-void
.end method
