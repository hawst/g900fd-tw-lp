.class public Lcom/samsung/android/app/filterinstaller/FilterUninstaller;
.super Ljava/lang/Object;
.source "FilterUninstaller.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FilterInstaller"


# instance fields
.field final mPackageName:Ljava/lang/String;

.field final mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/filterinstaller/FilterPackageService;Ljava/lang/String;)V
    .locals 0
    .param p1, "serviceContext"    # Lcom/samsung/android/app/filterinstaller/FilterPackageService;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p2, p0, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->mPackageName:Ljava/lang/String;

    .line 37
    iput-object p1, p0, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    .line 38
    return-void
.end method

.method private removeFiles(Landroid/net/Uri;Ljava/lang/Long;)V
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "packageId"    # Ljava/lang/Long;

    .prologue
    .line 56
    const/4 v6, 0x0

    .line 58
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v0}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_ID"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string v3, "filename"

    aput-object v3, v2, v1

    const-string v3, "package_id = ?"

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 64
    if-nez v6, :cond_1

    .line 88
    if-eqz v6, :cond_0

    .line 89
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    :goto_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 69
    new-instance v8, Ljava/io/File;

    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 71
    .local v8, "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    sget-boolean v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v0, :cond_1

    .line 73
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    .line 74
    .local v9, "name":Ljava/lang/String;
    const-string v0, "FilterInstaller"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x9

    invoke-virtual {v9, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is deleted."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 85
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "name":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 86
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 88
    if-eqz v6, :cond_0

    .line 89
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 78
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v8    # "file":Ljava/io/File;
    :cond_2
    :try_start_3
    sget-boolean v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v0, :cond_1

    .line 79
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    .line 80
    .restart local v9    # "name":Ljava/lang/String;
    const-string v0, "FilterInstaller"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x9

    invoke-virtual {v9, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isn\'t deleted,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 88
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 89
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 88
    :cond_4
    if-eqz v6, :cond_0

    .line 89
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private removeFromDB()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 48
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 49
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->packagesUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->mPackageName:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 51
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 52
    invoke-virtual {v0, v1, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 53
    :cond_0
    return-void
.end method

.method private removeFromFS()V
    .locals 14

    .prologue
    .line 94
    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 96
    .local v12, "packageToBeDeleted":Ljava/lang/Long;
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 97
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->packagesUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->mPackageName:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 99
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "filters"

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 101
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_ID"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "filename"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "package_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 105
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 158
    :goto_0
    return-void

    .line 109
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 110
    const/4 v2, 0x2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 112
    new-instance v9, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->FILTER_DIR:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 115
    .local v9, "filterFile":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 116
    sget-boolean v2, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v2, :cond_1

    .line 117
    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    .line 118
    .local v11, "name":Ljava/lang/String;
    const-string v2, "FilterInstaller"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x9

    invoke-virtual {v11, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is deleted."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    .end local v11    # "name":Ljava/lang/String;
    :cond_1
    :goto_2
    const/4 v8, 0x0

    .line 130
    .local v8, "file":Ljava/io/File;
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ".so"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    .line 131
    .local v10, "index":I
    const/4 v13, 0x0

    .line 132
    .local v13, "sigName":Ljava/lang/String;
    const/4 v2, -0x1

    if-eq v10, v2, :cond_0

    .line 133
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x1

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".sig"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 134
    new-instance v8, Ljava/io/File;

    .end local v8    # "file":Ljava/io/File;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->FILTER_DIR:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v8, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 136
    .restart local v8    # "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 137
    sget-boolean v2, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v2, :cond_0

    .line 138
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    .line 139
    .restart local v11    # "name":Ljava/lang/String;
    const-string v2, "FilterInstaller"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x9

    invoke-virtual {v11, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is deleted."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 149
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "filterFile":Ljava/io/File;
    .end local v10    # "index":I
    .end local v11    # "name":Ljava/lang/String;
    .end local v13    # "sigName":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 150
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    if-eqz v6, :cond_2

    .line 153
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 156
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_3
    sget-object v2, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->iconUri:Landroid/net/Uri;

    invoke-direct {p0, v2, v12}, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->removeFiles(Landroid/net/Uri;Ljava/lang/Long;)V

    .line 157
    sget-object v2, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->textureUri:Landroid/net/Uri;

    invoke-direct {p0, v2, v12}, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->removeFiles(Landroid/net/Uri;Ljava/lang/Long;)V

    goto/16 :goto_0

    .line 122
    .restart local v9    # "filterFile":Ljava/io/File;
    :cond_3
    :try_start_2
    sget-boolean v2, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v2, :cond_1

    .line 123
    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    .line 124
    .restart local v11    # "name":Ljava/lang/String;
    const-string v2, "FilterInstaller"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x9

    invoke-virtual {v11, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isn\'t deleted."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 152
    .end local v9    # "filterFile":Ljava/io/File;
    .end local v11    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v6, :cond_4

    .line 153
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2

    .line 142
    .restart local v8    # "file":Ljava/io/File;
    .restart local v9    # "filterFile":Ljava/io/File;
    .restart local v10    # "index":I
    .restart local v13    # "sigName":Ljava/lang/String;
    :cond_5
    :try_start_3
    sget-boolean v2, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v2, :cond_0

    .line 143
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    .line 144
    .restart local v11    # "name":Ljava/lang/String;
    const-string v2, "FilterInstaller"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x9

    invoke-virtual {v11, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isn\'t deleted,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 152
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "filterFile":Ljava/io/File;
    .end local v10    # "index":I
    .end local v11    # "name":Ljava/lang/String;
    .end local v13    # "sigName":Ljava/lang/String;
    :cond_6
    if-eqz v6, :cond_2

    .line 153
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_3
.end method


# virtual methods
.method public uninstallFilters()V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->mPackageName:Ljava/lang/String;

    const-string v1, "com.sec.android.filter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    invoke-direct {p0}, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->removeFromFS()V

    .line 43
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->removeFromDB()V

    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.action.DOWNFILTER_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->sendBroadcast(Landroid/content/Intent;)V

    .line 45
    return-void
.end method
