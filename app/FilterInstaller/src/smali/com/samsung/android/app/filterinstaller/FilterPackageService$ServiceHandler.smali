.class final Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;
.super Landroid/os/Handler;
.source "FilterPackageService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/filterinstaller/FilterPackageService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/filterinstaller/FilterPackageService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    .line 106
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 107
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 111
    sget-boolean v1, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v1, :cond_0

    .line 112
    const-string v1, "FilterInstaller"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/filterinstaller/FilterPackageService;->removePackageColon(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->access$000(Lcom/samsung/android/app/filterinstaller/FilterPackageService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "packageName":Ljava/lang/String;
    iget v1, p1, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    .line 152
    iget-object v1, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    # getter for: Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mStartId:I
    invoke-static {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->access$100(Lcom/samsung/android/app/filterinstaller/FilterPackageService;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->stopSelfResult(I)Z

    .line 155
    :goto_0
    return-void

    .line 119
    :pswitch_0
    new-instance v1, Lcom/samsung/android/app/filterinstaller/FilterInstaller;

    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;-><init>(Lcom/samsung/android/app/filterinstaller/FilterPackageService;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->installFilters()V

    goto :goto_0

    .line 128
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    # getter for: Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mStartId:I
    invoke-static {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->access$100(Lcom/samsung/android/app/filterinstaller/FilterPackageService;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->stopSelfResult(I)Z

    goto :goto_0

    .line 132
    :pswitch_2
    new-instance v1, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;

    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;-><init>(Lcom/samsung/android/app/filterinstaller/FilterPackageService;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->uninstallFilters()V

    .line 133
    iget-object v1, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->stopFilterInstaller()V

    goto :goto_0

    .line 137
    :pswitch_3
    new-instance v1, Lcom/samsung/android/app/filterinstaller/FilterLocaleChanger;

    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-direct {v1, v2}, Lcom/samsung/android/app/filterinstaller/FilterLocaleChanger;-><init>(Lcom/samsung/android/app/filterinstaller/FilterPackageService;)V

    invoke-virtual {v1}, Lcom/samsung/android/app/filterinstaller/FilterLocaleChanger;->changeLocale()V

    goto :goto_0

    .line 141
    :pswitch_4
    new-instance v1, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;

    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;-><init>(Lcom/samsung/android/app/filterinstaller/FilterPackageService;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->installFilters()V

    .line 142
    const-string v1, "FilterInstaller"

    const-string v2, "install done"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 146
    :pswitch_5
    new-instance v1, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;

    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;-><init>(Lcom/samsung/android/app/filterinstaller/FilterPackageService;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/android/app/filterinstaller/FilterUninstaller;->uninstallFilters()V

    .line 147
    new-instance v1, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;

    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->this$0:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;-><init>(Lcom/samsung/android/app/filterinstaller/FilterPackageService;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->installFilters()V

    .line 148
    const-string v1, "FilterInstaller"

    const-string v2, "reset done"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
