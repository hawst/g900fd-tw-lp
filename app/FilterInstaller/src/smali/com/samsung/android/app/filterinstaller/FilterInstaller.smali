.class public Lcom/samsung/android/app/filterinstaller/FilterInstaller;
.super Ljava/lang/Object;
.source "FilterInstaller.java"


# static fields
.field private static final ICON_SIZE:I = 0x100

.field private static final LIB_EXT:Ljava/lang/String; = ".so"

.field private static final SIG_EXT:Ljava/lang/String; = ".sig"

.field private static final TAG:Ljava/lang/String; = "FilterInstaller"

.field private static final TEX_EXT:Ljava/lang/String; = ".png"

.field private static final TEX_PREFIX:Ljava/lang/String; = "tex_"


# instance fields
.field mContext:Landroid/content/Context;

.field final mPackageManager:Landroid/content/pm/PackageManager;

.field final mPackageName:Ljava/lang/String;

.field final mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/filterinstaller/FilterPackageService;Ljava/lang/String;)V
    .locals 4
    .param p1, "serviceContext"    # Lcom/samsung/android/app/filterinstaller/FilterPackageService;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p2, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    .line 71
    iput-object p1, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    .line 72
    iget-object v1, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 75
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mContext:Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    return-void

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v1, "FilterInstaller"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can not create context of package ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkSoSignature(Ljava/lang/String;)Z
    .locals 11
    .param p1, "filterSo"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/spec/InvalidKeySpecException;,
            Ljava/security/InvalidKeyException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 532
    const/4 v8, 0x0

    .line 534
    .local v8, "result":Z
    const/4 v2, 0x0

    .line 535
    .local v2, "in":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 538
    .local v5, "insig":Ljava/io/FileInputStream;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v7

    .line 539
    .local v7, "key":Ljava/security/PublicKey;
    const/4 v1, 0x0

    .line 540
    .local v1, "filterPath":Ljava/lang/String;
    const-string v9, ".so"

    invoke-virtual {p1, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 541
    .local v4, "index":I
    const/4 v9, -0x1

    if-eq v4, v9, :cond_0

    .line 542
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->FILTER_DIR:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {p1, v10, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 544
    :cond_0
    new-instance v3, Ljava/io/FileInputStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".so"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v3, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 545
    .end local v2    # "in":Ljava/io/FileInputStream;
    .local v3, "in":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v6, Ljava/io/FileInputStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".sig"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 547
    .end local v5    # "insig":Ljava/io/FileInputStream;
    .local v6, "insig":Ljava/io/FileInputStream;
    :try_start_2
    invoke-direct {p0, v3}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->inputStreamToBytes(Ljava/io/InputStream;)[B

    move-result-object v9

    invoke-direct {p0, v6}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->inputStreamToBytes(Ljava/io/InputStream;)[B

    move-result-object v10

    invoke-direct {p0, v9, v10, v7}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->verifyData([B[BLjava/security/PublicKey;)Z

    move-result v8

    .line 548
    if-nez v8, :cond_1

    .line 549
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".so"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->deleteUnMatchedFilter(Ljava/lang/String;)Z

    .line 550
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".sig"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->deleteUnMatchedFilter(Ljava/lang/String;)Z

    .line 552
    :cond_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 553
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 558
    if-eqz v3, :cond_2

    .line 559
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 564
    :cond_2
    :goto_0
    if-eqz v6, :cond_3

    .line 565
    :try_start_4
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    move-object v5, v6

    .end local v6    # "insig":Ljava/io/FileInputStream;
    .restart local v5    # "insig":Ljava/io/FileInputStream;
    move-object v2, v3

    .line 570
    .end local v1    # "filterPath":Ljava/lang/String;
    .end local v3    # "in":Ljava/io/FileInputStream;
    .end local v4    # "index":I
    .end local v7    # "key":Ljava/security/PublicKey;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :cond_4
    :goto_1
    return v8

    .line 560
    .end local v2    # "in":Ljava/io/FileInputStream;
    .end local v5    # "insig":Ljava/io/FileInputStream;
    .restart local v1    # "filterPath":Ljava/lang/String;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    .restart local v4    # "index":I
    .restart local v6    # "insig":Ljava/io/FileInputStream;
    .restart local v7    # "key":Ljava/security/PublicKey;
    :catch_0
    move-exception v0

    .line 561
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 566
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 567
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .end local v6    # "insig":Ljava/io/FileInputStream;
    .restart local v5    # "insig":Ljava/io/FileInputStream;
    move-object v2, v3

    .line 569
    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    goto :goto_1

    .line 554
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "filterPath":Ljava/lang/String;
    .end local v4    # "index":I
    .end local v7    # "key":Ljava/security/PublicKey;
    :catch_2
    move-exception v0

    .line 555
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 558
    if-eqz v2, :cond_5

    .line 559
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 564
    :cond_5
    :goto_3
    if-eqz v5, :cond_4

    .line 565
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 566
    :catch_3
    move-exception v0

    .line 567
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 560
    :catch_4
    move-exception v0

    .line 561
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 557
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    .line 558
    :goto_4
    if-eqz v2, :cond_6

    .line 559
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 564
    :cond_6
    :goto_5
    if-eqz v5, :cond_7

    .line 565
    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 568
    :cond_7
    :goto_6
    throw v9

    .line 560
    :catch_5
    move-exception v0

    .line 561
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 566
    .end local v0    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 567
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 557
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "filterPath":Ljava/lang/String;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    .restart local v4    # "index":I
    .restart local v7    # "key":Ljava/security/PublicKey;
    :catchall_1
    move-exception v9

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v2    # "in":Ljava/io/FileInputStream;
    .end local v5    # "insig":Ljava/io/FileInputStream;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    .restart local v6    # "insig":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v9

    move-object v5, v6

    .end local v6    # "insig":Ljava/io/FileInputStream;
    .restart local v5    # "insig":Ljava/io/FileInputStream;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    goto :goto_4

    .line 554
    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    :catch_7
    move-exception v0

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v2    # "in":Ljava/io/FileInputStream;
    .end local v5    # "insig":Ljava/io/FileInputStream;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    .restart local v6    # "insig":Ljava/io/FileInputStream;
    :catch_8
    move-exception v0

    move-object v5, v6

    .end local v6    # "insig":Ljava/io/FileInputStream;
    .restart local v5    # "insig":Ljava/io/FileInputStream;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method private copyFileFromAssets(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 9
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "targetDir"    # Ljava/lang/String;
    .param p3, "newfilterName"    # Ljava/lang/String;
    .param p4, "executable"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 435
    const/4 v2, 0x0

    .line 436
    .local v2, "filter":Ljava/io/File;
    const/4 v4, 0x0

    .line 438
    .local v4, "out":Ljava/io/BufferedOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    .end local v2    # "filter":Ljava/io/File;
    .local v3, "filter":Ljava/io/File;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 441
    new-instance v5, Ljava/io/BufferedOutputStream;

    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 442
    .end local v4    # "out":Ljava/io/BufferedOutputStream;
    .local v5, "out":Ljava/io/BufferedOutputStream;
    const/16 v7, 0x400

    :try_start_2
    new-array v0, v7, [B

    .line 444
    .local v0, "b":[B
    const/4 v6, 0x0

    .line 445
    .local v6, "read":I
    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v6

    if-lez v6, :cond_1

    .line 446
    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7, v6}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 456
    .end local v0    # "b":[B
    .end local v6    # "read":I
    :catch_0
    move-exception v1

    move-object v4, v5

    .end local v5    # "out":Ljava/io/BufferedOutputStream;
    .restart local v4    # "out":Ljava/io/BufferedOutputStream;
    move-object v2, v3

    .line 457
    .end local v3    # "filter":Ljava/io/File;
    .local v1, "e":Ljava/lang/Exception;
    .restart local v2    # "filter":Ljava/io/File;
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 458
    const/4 v7, 0x0

    .line 460
    if-eqz v4, :cond_0

    .line 461
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_2
    return-object v7

    .line 449
    .end local v2    # "filter":Ljava/io/File;
    .end local v4    # "out":Ljava/io/BufferedOutputStream;
    .restart local v0    # "b":[B
    .restart local v3    # "filter":Ljava/io/File;
    .restart local v5    # "out":Ljava/io/BufferedOutputStream;
    .restart local v6    # "read":I
    :cond_1
    :try_start_4
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->flush()V

    .line 451
    const/4 v7, 0x0

    invoke-virtual {v3, p4, v7}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 453
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Ljava/io/File;->setReadable(ZZ)Z

    .line 455
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v7

    .line 460
    if-eqz v5, :cond_2

    .line 461
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V

    :cond_2
    move-object v4, v5

    .end local v5    # "out":Ljava/io/BufferedOutputStream;
    .restart local v4    # "out":Ljava/io/BufferedOutputStream;
    move-object v2, v3

    .end local v3    # "filter":Ljava/io/File;
    .restart local v2    # "filter":Ljava/io/File;
    goto :goto_2

    .line 460
    .end local v0    # "b":[B
    .end local v6    # "read":I
    :catchall_0
    move-exception v7

    :goto_3
    if-eqz v4, :cond_3

    .line 461
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V

    :cond_3
    throw v7

    .line 460
    .end local v2    # "filter":Ljava/io/File;
    .restart local v3    # "filter":Ljava/io/File;
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "filter":Ljava/io/File;
    .restart local v2    # "filter":Ljava/io/File;
    goto :goto_3

    .end local v2    # "filter":Ljava/io/File;
    .end local v4    # "out":Ljava/io/BufferedOutputStream;
    .restart local v3    # "filter":Ljava/io/File;
    .restart local v5    # "out":Ljava/io/BufferedOutputStream;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "out":Ljava/io/BufferedOutputStream;
    .restart local v4    # "out":Ljava/io/BufferedOutputStream;
    move-object v2, v3

    .end local v3    # "filter":Ljava/io/File;
    .restart local v2    # "filter":Ljava/io/File;
    goto :goto_3

    .line 456
    :catch_1
    move-exception v1

    goto :goto_1

    .end local v2    # "filter":Ljava/io/File;
    .restart local v3    # "filter":Ljava/io/File;
    :catch_2
    move-exception v1

    move-object v2, v3

    .end local v3    # "filter":Ljava/io/File;
    .restart local v2    # "filter":Ljava/io/File;
    goto :goto_1
.end method

.method private createDownloadFilterDirectory()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 409
    const/4 v7, 0x3

    new-array v3, v7, [Ljava/lang/String;

    sget-object v7, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->FILTER_DIR:Ljava/lang/String;

    aput-object v7, v3, v10

    sget-object v7, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->DRAWABLE_DIR:Ljava/lang/String;

    aput-object v7, v3, v11

    const/4 v7, 0x2

    sget-object v8, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->TEXTURE_DIR:Ljava/lang/String;

    aput-object v8, v3, v7

    .line 411
    .local v3, "dirs":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 412
    .local v1, "dest":Ljava/io/File;
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v2, v0, v4

    .line 413
    .local v2, "dir":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    .end local v1    # "dest":Ljava/io/File;
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 414
    .restart local v1    # "dest":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 415
    sget-boolean v7, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v7, :cond_0

    .line 416
    const-string v7, "FilterInstaller"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "create new directory:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    :cond_0
    invoke-virtual {v1, v11, v10}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 421
    invoke-virtual {v1, v11, v10}, Ljava/io/File;->setReadable(ZZ)Z

    .line 412
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 424
    .end local v2    # "dir":Ljava/lang/String;
    :cond_1
    if-eqz v1, :cond_2

    .line 426
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v6

    .line 427
    .local v6, "parent":Ljava/io/File;
    if-eqz v6, :cond_2

    .line 428
    invoke-virtual {v6, v11, v10}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 429
    invoke-virtual {v6, v11, v10}, Ljava/io/File;->setReadable(ZZ)Z

    .line 432
    .end local v6    # "parent":Ljava/io/File;
    :cond_2
    return-void
.end method

.method private deleteUnMatchedFilter(Ljava/lang/String;)Z
    .locals 4
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 467
    const/4 v0, 0x0

    .line 469
    .local v0, "file":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    .end local v0    # "file":Ljava/io/File;
    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 471
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 472
    const-string v1, "FilterInstaller"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] is not deleted"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    const/4 v1, 0x0

    .line 476
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private getBestDimension(II)Landroid/graphics/Point;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    const/16 v0, 0x100

    .line 381
    if-le p1, p2, :cond_1

    .line 382
    if-le p1, v0, :cond_0

    .line 383
    mul-int/lit16 v0, p2, 0x100

    div-int p2, v0, p1

    .line 384
    const/16 p1, 0x100

    .line 393
    :cond_0
    :goto_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0

    .line 387
    :cond_1
    if-le p2, v0, :cond_0

    .line 388
    mul-int/lit16 v0, p1, 0x100

    div-int p1, v0, p2

    .line 389
    const/16 p2, 0x100

    goto :goto_0
.end method

.method private getFilterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "filterName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 405
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getPublicKey()Ljava/security/PublicKey;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/spec/InvalidKeySpecException;
        }
    .end annotation

    .prologue
    .line 574
    iget-object v4, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v4}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 575
    .local v0, "am":Landroid/content/res/AssetManager;
    const-string v4, "public_key.der"

    invoke-virtual {v0, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 578
    .local v2, "publicKey":Ljava/io/InputStream;
    :try_start_0
    new-instance v3, Ljava/security/spec/X509EncodedKeySpec;

    invoke-direct {p0, v2}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->inputStreamToBytes(Ljava/io/InputStream;)[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    .line 579
    .local v3, "spec":Ljava/security/spec/X509EncodedKeySpec;
    const-string v4, "RSA"

    invoke-static {v4}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v1

    .line 581
    .local v1, "kf":Ljava/security/KeyFactory;
    invoke-virtual {v1, v3}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 583
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    return-object v4

    .end local v1    # "kf":Ljava/security/KeyFactory;
    .end local v3    # "spec":Ljava/security/spec/X509EncodedKeySpec;
    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v4
.end method

.method private inputStreamToBytes(Ljava/io/InputStream;)[B
    .locals 5
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 588
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 591
    .local v0, "buffer":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x800

    new-array v1, v3, [B

    .line 593
    .local v1, "data":[B
    :goto_0
    array-length v3, v1

    invoke-virtual {p1, v1, v4, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .local v2, "nRead":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 594
    invoke-virtual {v0, v1, v4, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 596
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 598
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3
.end method

.method private insertPackageToDB(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 363
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 364
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 366
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "name"

    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    sget-object v2, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->packagesUri:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 369
    return-void
.end method

.method private putInfoForDual(Ljava/lang/String;Landroid/content/res/Resources;Landroid/content/ContentValues;)Z
    .locals 5
    .param p1, "fileTitle"    # Ljava/lang/String;
    .param p2, "res"    # Landroid/content/res/Resources;
    .param p3, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v4, 0x1

    .line 480
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_posx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 482
    .local v1, "rn":Ljava/lang/String;
    const-string v2, "dimen"

    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {p2, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 483
    .local v0, "resId":I
    if-nez v0, :cond_0

    .line 484
    const-string v2, "posx"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 489
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_posy"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 490
    const-string v2, "dimen"

    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {p2, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 491
    if-nez v0, :cond_1

    .line 492
    const-string v2, "posy"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 497
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_width"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 498
    const-string v2, "dimen"

    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {p2, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 499
    if-nez v0, :cond_2

    .line 500
    const-string v2, "width"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 505
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_height"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 506
    const-string v2, "dimen"

    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {p2, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 507
    if-nez v0, :cond_3

    .line 508
    const-string v2, "height"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 513
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_filtertype"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 514
    const-string v2, "string"

    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {p2, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 515
    if-nez v0, :cond_4

    .line 516
    const-string v2, "filter_type"

    const-string v3, "SINGLE"

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_dualhandler"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 522
    const-string v2, "string"

    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {p2, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 523
    if-nez v0, :cond_5

    .line 524
    const-string v2, "handler"

    const-string v3, "TRUE"

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    :goto_5
    return v4

    .line 486
    :cond_0
    const-string v2, "posx"

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 494
    :cond_1
    const-string v2, "posy"

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 502
    :cond_2
    const-string v2, "width"

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_2

    .line 510
    :cond_3
    const-string v2, "height"

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_3

    .line 518
    :cond_4
    const-string v2, "filter_type"

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 526
    :cond_5
    const-string v2, "handler"

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method private removePackage(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 401
    invoke-direct {p0, p1}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->removePackageFromDB(Ljava/lang/String;)V

    .line 402
    return-void
.end method

.method private removePackageFromDB(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 372
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 374
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->packagesUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 375
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 376
    invoke-virtual {v0, v1, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 378
    :cond_0
    return-void
.end method

.method private storeFilters(Landroid/content/res/AssetManager;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)J
    .locals 20
    .param p1, "am"    # Landroid/content/res/AssetManager;
    .param p2, "res"    # Landroid/content/res/Resources;
    .param p3, "filename"    # Ljava/lang/String;
    .param p4, "fileTitle"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    const/4 v6, 0x0

    .line 212
    .local v6, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "so/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v6

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->getFilterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 214
    .local v7, "newFilename":Ljava/lang/String;
    sget-object v17, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->FILTER_DIR:Ljava/lang/String;

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v6, v1, v7, v2}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->copyFileFromAssets(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    .line 216
    .local v8, "pathFilter":Ljava/lang/String;
    const-string v17, "FilterInstaller"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "newFilename = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", pathFilter = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    const-string v17, ".so"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v17

    if-eqz v17, :cond_5

    .line 220
    :try_start_1
    const-string v17, "FilterInstaller"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "checkSignature fileName = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->checkSoSignature(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_1

    .line 222
    const-string v17, "FilterInstaller"

    const-string v18, "check false"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/security/SignatureException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 223
    const-wide/16 v18, -0x1

    .line 275
    if-eqz v6, :cond_0

    .line 276
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 277
    :cond_0
    const/4 v6, 0x0

    :goto_0
    return-wide v18

    .line 225
    :catch_0
    move-exception v5

    .line 226
    .local v5, "e":Ljava/security/InvalidKeyException;
    :try_start_2
    invoke-virtual {v5}, Ljava/security/InvalidKeyException;->printStackTrace()V

    .line 238
    .end local v5    # "e":Ljava/security/InvalidKeyException;
    :cond_1
    :goto_1
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "_title"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 239
    .local v12, "rn_title":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "_vendor"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 240
    .local v13, "rn_vendor":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "_version"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 241
    .local v14, "rn_version":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 242
    .local v4, "cr":Landroid/content/ContentResolver;
    new-instance v16, Landroid/content/ContentValues;

    const/16 v17, 0x5

    invoke-direct/range {v16 .. v17}, Landroid/content/ContentValues;-><init>(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 245
    .local v16, "values":Landroid/content/ContentValues;
    :try_start_3
    const-string v17, "string"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v12, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 246
    .local v9, "resId_title":I
    const-string v17, "string"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v13, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 247
    .local v10, "resId_vendor":I
    const-string v17, "string"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v14, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    .line 249
    .local v11, "resId_version":I
    const-string v17, "FilterInstaller"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "title = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", vendor = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", version = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    const-string v17, "name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v16 .. v18}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-string v17, "filename"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    const-string v17, "package_name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const-string v17, "vendor"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v16 .. v18}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v17, "version"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v16 .. v18}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 263
    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    move-object/from16 v3, v16

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->putInfoForDual(Ljava/lang/String;Landroid/content/res/Resources;Landroid/content/ContentValues;)Z

    .line 264
    sget-object v17, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->filtersUri:Landroid/net/Uri;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v15

    .line 266
    .local v15, "uri":Landroid/net/Uri;
    if-nez v15, :cond_8

    .line 267
    sget-boolean v17, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v17, :cond_2

    .line 268
    const-string v17, "FilterInstaller"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Installation failure: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 270
    :cond_2
    const-wide/16 v18, -0x1

    .line 275
    if-eqz v6, :cond_3

    .line 276
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 277
    :cond_3
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 227
    .end local v4    # "cr":Landroid/content/ContentResolver;
    .end local v9    # "resId_title":I
    .end local v10    # "resId_vendor":I
    .end local v11    # "resId_version":I
    .end local v12    # "rn_title":Ljava/lang/String;
    .end local v13    # "rn_vendor":Ljava/lang/String;
    .end local v14    # "rn_version":Ljava/lang/String;
    .end local v15    # "uri":Landroid/net/Uri;
    .end local v16    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v5

    .line 228
    .local v5, "e":Ljava/security/NoSuchAlgorithmException;
    :try_start_5
    invoke-virtual {v5}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 275
    .end local v5    # "e":Ljava/security/NoSuchAlgorithmException;
    .end local v7    # "newFilename":Ljava/lang/String;
    .end local v8    # "pathFilter":Ljava/lang/String;
    :catchall_0
    move-exception v17

    if-eqz v6, :cond_4

    .line 276
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 277
    :cond_4
    const/4 v6, 0x0

    throw v17

    .line 229
    .restart local v7    # "newFilename":Ljava/lang/String;
    .restart local v8    # "pathFilter":Ljava/lang/String;
    :catch_2
    move-exception v5

    .line 230
    .local v5, "e":Ljava/security/spec/InvalidKeySpecException;
    :try_start_6
    invoke-virtual {v5}, Ljava/security/spec/InvalidKeySpecException;->printStackTrace()V

    goto/16 :goto_1

    .line 231
    .end local v5    # "e":Ljava/security/spec/InvalidKeySpecException;
    :catch_3
    move-exception v5

    .line 232
    .local v5, "e":Ljava/security/SignatureException;
    invoke-virtual {v5}, Ljava/security/SignatureException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 236
    .end local v5    # "e":Ljava/security/SignatureException;
    :cond_5
    const-wide/16 v18, -0x1

    .line 275
    if-eqz v6, :cond_6

    .line 276
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 277
    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 256
    .restart local v4    # "cr":Landroid/content/ContentResolver;
    .restart local v12    # "rn_title":Ljava/lang/String;
    .restart local v13    # "rn_vendor":Ljava/lang/String;
    .restart local v14    # "rn_version":Ljava/lang/String;
    .restart local v16    # "values":Landroid/content/ContentValues;
    :catch_4
    move-exception v5

    .line 257
    .local v5, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_7
    const-string v17, "FilterInstaller"

    const-string v18, "Resource is not defined properly!"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-virtual {v5}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 260
    const-wide/16 v18, -0x1

    .line 275
    if-eqz v6, :cond_7

    .line 276
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 277
    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 272
    .end local v5    # "e":Landroid/content/res/Resources$NotFoundException;
    .restart local v9    # "resId_title":I
    .restart local v10    # "resId_vendor":I
    .restart local v11    # "resId_version":I
    .restart local v15    # "uri":Landroid/net/Uri;
    :cond_8
    :try_start_8
    invoke-virtual {v15}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-wide v18

    .line 275
    if-eqz v6, :cond_9

    .line 276
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 277
    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_0
.end method

.method private storeIconToFS(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "filterIcon"    # Landroid/graphics/drawable/Drawable;
    .param p2, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 316
    if-nez p1, :cond_0

    .line 317
    const/4 v4, 0x0

    .line 332
    .end local p1    # "filterIcon":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object v4

    .line 320
    .restart local p1    # "filterIcon":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-direct {p0, v5, v6}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->getBestDimension(II)Landroid/graphics/Point;

    move-result-object v2

    .line 321
    .local v2, "p":Landroid/graphics/Point;
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    .end local p1    # "filterIcon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 322
    .local v3, "packageBitmap":Landroid/graphics/Bitmap;
    iget v5, v2, Landroid/graphics/Point;->x:I

    iget v6, v2, Landroid/graphics/Point;->y:I

    invoke-static {v3, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 324
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->DRAWABLE_DIR:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 325
    .local v4, "pngFilepath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 326
    .local v0, "icon":Ljava/io/File;
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 327
    .local v1, "out":Ljava/io/BufferedOutputStream;
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    invoke-virtual {v3, v5, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 328
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V

    .line 330
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v7}, Ljava/io/File;->setReadable(ZZ)Z

    goto :goto_0
.end method

.method private storeIcons(Landroid/content/res/Resources;JLjava/lang/String;)Z
    .locals 10
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "filterId"    # J
    .param p4, "resourceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 282
    const-string v7, "drawable"

    iget-object v8, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {p1, p4, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 283
    .local v4, "resID":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 286
    .local v2, "name":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v7}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 287
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 290
    .local v6, "values":Landroid/content/ContentValues;
    :try_start_0
    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-direct {p0, v7, v2}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->storeIconToFS(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 306
    .local v3, "pngFilepath":Ljava/lang/String;
    const-string v7, "package_name"

    iget-object v8, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const-string v7, "filename"

    invoke-virtual {v6, v7, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const-string v7, "filter_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 310
    sget-object v7, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->iconUri:Landroid/net/Uri;

    invoke-virtual {v0, v7, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 312
    const/4 v7, 0x1

    .end local v3    # "pngFilepath":Ljava/lang/String;
    :goto_0
    return v7

    .line 291
    :catch_0
    move-exception v1

    .line 297
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    sget-object v7, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->filtersUri_id:Landroid/net/Uri;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 298
    .local v5, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    if-eqz v5, :cond_0

    .line 299
    invoke-virtual {v0, v5, v9, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 302
    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private storePackage(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 397
    invoke-direct {p0, p1}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->insertPackageToDB(Ljava/lang/String;)V

    .line 398
    return-void
.end method

.method private storeTextures(Landroid/content/res/AssetManager;JLjava/lang/String;[Ljava/lang/String;)V
    .locals 16
    .param p1, "am"    # Landroid/content/res/AssetManager;
    .param p2, "filterId"    # J
    .param p4, "filename"    # Ljava/lang/String;
    .param p5, "textures"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "tex_"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p4

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 174
    .local v9, "prefix":Ljava/lang/String;
    move-object/from16 v1, p5

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v10, v1, v4

    .line 175
    .local v10, "tex":Ljava/lang/String;
    sget-boolean v12, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v12, :cond_0

    .line 176
    const-string v12, "FilterInstaller"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "tex:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", prefix:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :cond_0
    invoke-virtual {v10, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 174
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 183
    :cond_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "tex/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 185
    .local v5, "in":Ljava/io/InputStream;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v12}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->getFilterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 186
    .local v7, "newTextureName":Ljava/lang/String;
    const/4 v8, 0x0

    .line 188
    .local v8, "pathTexture":Ljava/lang/String;
    :try_start_0
    sget-object v12, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->TEXTURE_DIR:Ljava/lang/String;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v12, v7, v13}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->copyFileFromAssets(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 192
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 196
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v12}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 197
    .local v2, "cr":Landroid/content/ContentResolver;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 199
    .local v11, "values":Landroid/content/ContentValues;
    const-string v12, "package_name"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v12, "filename"

    invoke-virtual {v11, v12, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v12, "filter_id"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 203
    sget-object v12, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->textureUri:Landroid/net/Uri;

    invoke-virtual {v2, v12, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_1

    .line 189
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v11    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v3

    .line 190
    .local v3, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 192
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto :goto_2

    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v12

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    throw v12

    .line 205
    .end local v5    # "in":Ljava/io/InputStream;
    .end local v7    # "newTextureName":Ljava/lang/String;
    .end local v8    # "pathTexture":Ljava/lang/String;
    .end local v10    # "tex":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private storeTitles(Landroid/content/res/Resources;JLjava/lang/String;)Z
    .locals 8
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "filterId"    # J
    .param p4, "fileTitle"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 336
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_title"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 338
    .local v2, "rn":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v5}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 339
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v4, Landroid/content/ContentValues;

    const/4 v5, 0x5

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 341
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "string"

    iget-object v6, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {p1, v2, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 342
    .local v1, "resId":I
    if-nez v1, :cond_1

    .line 343
    sget-object v5, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->filtersUri_id:Landroid/net/Uri;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 344
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 345
    invoke-virtual {v0, v3, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 347
    :cond_0
    const/4 v5, 0x0

    .line 359
    .end local v3    # "uri":Landroid/net/Uri;
    :goto_0
    return v5

    .line 350
    :cond_1
    const-string v5, "package_name"

    iget-object v6, p0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const-string v5, "country"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const-string v5, "filter_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 354
    const-string v5, "title"

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v5, "resource_name"

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    sget-object v5, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->titleUri:Landroid/net/Uri;

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 359
    const/4 v5, 0x1

    goto :goto_0
.end method

.method private verifyData([B[BLjava/security/PublicKey;)Z
    .locals 2
    .param p1, "data"    # [B
    .param p2, "sigBytes"    # [B
    .param p3, "publicKey"    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/InvalidKeyException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 603
    const-string v1, "SHA256withRSA"

    invoke-static {v1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    .line 604
    .local v0, "signature":Ljava/security/Signature;
    invoke-virtual {v0, p3}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 605
    invoke-virtual {v0, p1}, Ljava/security/Signature;->update([B)V

    .line 606
    invoke-virtual {v0, p2}, Ljava/security/Signature;->verify([B)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public installFilters()V
    .locals 20

    .prologue
    .line 85
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageManager:Landroid/content/pm/PackageManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x1000

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v15

    .line 89
    .local v15, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v2, v15, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 90
    iget-object v2, v15, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v16

    .line 91
    .local v16, "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "com.sec.android.camera.permission.USE_EFFECT_FILTER"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 92
    const-string v2, "FilterInstaller"

    const-string v18, "There is no requred permission"

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->stopFilterInstaller()V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 162
    .end local v15    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v16    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v10

    .line 163
    .local v10, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "FilterInstaller"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Can not get package info of package ["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "]"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    invoke-virtual {v10}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 98
    .end local v10    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v15    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_2
    :try_start_1
    const-string v2, "FilterInstaller"

    const-string v18, "There is no permission not at all"

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->stopFilterInstaller()V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 165
    .end local v15    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_1
    move-exception v10

    .line 166
    .local v10, "e":Ljava/lang/SecurityException;
    invoke-virtual {v10}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    .line 104
    .end local v10    # "e":Ljava/lang/SecurityException;
    .restart local v15    # "packageInfo":Landroid/content/pm/PackageInfo;
    .restart local v16    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 105
    .local v17, "res":Landroid/content/res/Resources;
    invoke-virtual/range {v17 .. v17}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    .line 107
    .local v3, "am":Landroid/content/res/AssetManager;
    const-string v2, "so"

    invoke-virtual {v3, v2}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 108
    .local v14, "list":[Ljava/lang/String;
    array-length v2, v14

    if-nez v2, :cond_4

    .line 110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->stopFilterInstaller()V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 167
    .end local v3    # "am":Landroid/content/res/AssetManager;
    .end local v14    # "list":[Ljava/lang/String;
    .end local v15    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v16    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v17    # "res":Landroid/content/res/Resources;
    :catch_2
    move-exception v10

    .line 168
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 113
    .end local v10    # "e":Ljava/io/IOException;
    .restart local v3    # "am":Landroid/content/res/AssetManager;
    .restart local v14    # "list":[Ljava/lang/String;
    .restart local v15    # "packageInfo":Landroid/content/pm/PackageInfo;
    .restart local v16    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v17    # "res":Landroid/content/res/Resources;
    :cond_4
    :try_start_3
    const-string v2, "tex"

    invoke-virtual {v3, v2}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 115
    .local v7, "textures":[Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->createDownloadFilterDirectory()V

    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->storePackage(Ljava/lang/String;)V

    .line 118
    const/4 v9, 0x0

    .line 119
    .local v9, "count":I
    move-object v8, v14

    .local v8, "arr$":[Ljava/lang/String;
    array-length v13, v8

    .local v13, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_1
    if-ge v12, v13, :cond_b

    aget-object v11, v8, v12

    .line 122
    .local v11, "filterFile":Ljava/lang/String;
    const-string v2, ".so"

    invoke-virtual {v11, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 123
    const/4 v2, 0x0

    const-string v18, ".so"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v11, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 128
    .local v6, "fileTitle":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v3, v1, v11, v6}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->storeFilters(Landroid/content/res/AssetManager;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    .line 130
    .local v4, "id":J
    const-wide/16 v18, -0x1

    cmp-long v2, v4, v18

    if-nez v2, :cond_7

    .line 131
    const-string v2, ".so"

    invoke-virtual {v11, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 132
    const-string v2, "FilterInstaller"

    const-string v18, "storeFilters fail"

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_5
    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 125
    .end local v4    # "id":J
    .end local v6    # "fileTitle":Ljava/lang/String;
    :cond_6
    move-object v6, v11

    .restart local v6    # "fileTitle":Ljava/lang/String;
    goto :goto_2

    .line 137
    .restart local v4    # "id":J
    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v4, v5, v6}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->storeIcons(Landroid/content/res/Resources;JLjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 138
    const-string v2, "FilterInstaller"

    const-string v18, "storeIcons fail"

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 143
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v4, v5, v6}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->storeTitles(Landroid/content/res/Resources;JLjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 144
    const-string v2, "FilterInstaller"

    const-string v18, "storeTitles fail"

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 149
    :cond_9
    array-length v2, v7

    if-lez v2, :cond_a

    move-object/from16 v2, p0

    .line 150
    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->storeTextures(Landroid/content/res/AssetManager;JLjava/lang/String;[Ljava/lang/String;)V

    .line 152
    :cond_a
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 155
    .end local v4    # "id":J
    .end local v6    # "fileTitle":Ljava/lang/String;
    .end local v11    # "filterFile":Ljava/lang/String;
    :cond_b
    if-gtz v9, :cond_c

    .line 156
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mPackageName:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->removePackage(Ljava/lang/String;)V

    .line 159
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    new-instance v18, Landroid/content/Intent;

    const-string v19, "com.samsung.android.action.DOWNFILTER_CHANGED"

    invoke-direct/range {v18 .. v19}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->sendBroadcast(Landroid/content/Intent;)V

    .line 160
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/filterinstaller/FilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->stopFilterInstaller()V
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0
.end method
