.class public Lcom/samsung/android/app/filterinstaller/FilterLocaleChanger;
.super Ljava/lang/Object;
.source "FilterLocaleChanger.java"


# static fields
.field private static final INC_DEL:Ljava/lang/String; = "include_deleted"

.field private static final PKG_ID:Ljava/lang/String; = "package_id"

.field private static final PKG_NAME:Ljava/lang/String; = "package_name"

.field private static final RES_NAME:Ljava/lang/String; = "resource_name"

.field private static final RES_TYPE:Ljava/lang/String; = "string"

.field private static final TITLE:Ljava/lang/String; = "title"

.field private static final TITLE_ID:Ljava/lang/String; = "title_id"


# instance fields
.field final mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/filterinstaller/FilterPackageService;)V
    .locals 0
    .param p1, "serviceContext"    # Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/samsung/android/app/filterinstaller/FilterLocaleChanger;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    .line 30
    return-void
.end method


# virtual methods
.method public changeLocale()V
    .locals 23

    .prologue
    .line 37
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/filterinstaller/FilterLocaleChanger;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v4}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v16

    .line 39
    .local v16, "pm":Landroid/content/pm/PackageManager;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/filterinstaller/FilterLocaleChanger;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v4}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 40
    .local v2, "cr":Landroid/content/ContentResolver;
    sget-object v4, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->filtersUri:Landroid/net/Uri;

    const-string v5, "include_deleted"

    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 42
    .local v3, "uri":Landroid/net/Uri;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "package_id"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 44
    .local v9, "c":Landroid/database/Cursor;
    if-nez v9, :cond_1

    .line 45
    const-string v4, "FilterLocaleChanger"

    const-string v5, "cursor null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    if-nez v16, :cond_2

    .line 48
    const-string v4, "FilterLocaleChanger"

    const-string v5, "pm is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 53
    :cond_2
    const-wide/16 v12, -0x1

    .line 54
    .local v12, "oldPackageId":J
    const/4 v11, 0x0

    .line 56
    .local v11, "packageName":Ljava/lang/String;
    const/16 v17, 0x0

    .line 57
    .local v17, "res":Landroid/content/res/Resources;
    new-instance v22, Landroid/content/ContentValues;

    const/4 v4, 0x1

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 60
    .local v22, "values":Landroid/content/ContentValues;
    :cond_3
    :goto_1
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 61
    const-string v4, "package_id"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 62
    .local v14, "packageId":J
    cmp-long v4, v12, v14

    if-eqz v4, :cond_4

    .line 63
    const-string v4, "package_name"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 64
    const/16 v4, 0x80

    move-object/from16 v0, v16

    invoke-virtual {v0, v11, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    .line 66
    .local v8, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v4, v8, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object v4, v8, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 68
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v17

    .line 69
    move-wide v12, v14

    .line 74
    .end local v8    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_4
    const-string v4, "resource_name"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 75
    .local v19, "resourceName":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 81
    if-eqz v17, :cond_3

    .line 82
    const-string v4, "string"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v18

    .line 84
    .local v18, "resId":I
    if-eqz v18, :cond_3

    .line 88
    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 89
    .local v20, "title":Ljava/lang/String;
    const-string v4, "title_id"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 91
    .local v21, "titleId":Ljava/lang/String;
    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentValues;->clear()V

    .line 92
    const-string v4, "title"

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    sget-object v4, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->titleUri:Landroid/net/Uri;

    move-object/from16 v0, v21

    invoke-static {v4, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 96
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 100
    .end local v14    # "packageId":J
    .end local v18    # "resId":I
    .end local v19    # "resourceName":Ljava/lang/String;
    .end local v20    # "title":Ljava/lang/String;
    .end local v21    # "titleId":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 102
    .local v10, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    invoke-virtual {v10}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    if-eqz v9, :cond_0

    .line 105
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 98
    .end local v10    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_5
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/filterinstaller/FilterLocaleChanger;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v4}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->stopFilterInstaller()V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104
    if-eqz v9, :cond_0

    .line 105
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 104
    :catchall_0
    move-exception v4

    if-eqz v9, :cond_6

    .line 105
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v4
.end method
