.class public Lcom/samsung/android/app/filterinstaller/FilterPackageService;
.super Landroid/app/Service;
.source "FilterPackageService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;
    }
.end annotation


# static fields
.field private static final ACTION_ADDED:I = 0x1

.field private static final ACTION_CHANGED:I = 0x2

.field static final ACTION_FILTER_CHANGED:Ljava/lang/String; = "com.samsung.android.action.DOWNFILTER_CHANGED"

.field private static final ACTION_INSTALL_PRELOAD_FILTER:I = 0x6

.field private static final ACTION_LOCALE_CHANGED:I = 0x5

.field private static final ACTION_REMOVED:I = 0x4

.field private static final ACTION_REPLACED:I = 0x3

.field private static final ACTION_RESET_DB:I = 0x7

.field static final APK_ASSET_LIB_DIR:Ljava/lang/String; = "so"

.field static final APK_ASSET_TEXTURE_DIR:Ljava/lang/String; = "tex"

.field public static final DELETED:Ljava/lang/String; = "deleted"

.field static final DRAWABLE_DIR:Ljava/lang/String;

.field public static final EFFECT_NAME:Ljava/lang/String; = "name"

.field public static final FILE_NAME:Ljava/lang/String; = "filename"

.field static final FILTER_DIR:Ljava/lang/String;

.field public static final FILTER_PROJECTION:[Ljava/lang/String;

.field public static final INDEX_EFFECT_NAME:I = 0x1

.field public static final INDEX_FILE_NAME:I = 0x2

.field public static final INDEX_ID:I = 0x0

.field public static final INDEX_MVENDOR:I = 0x4

.field public static final INDEX_PACKAGE_NAME:I = 0x6

.field public static final INDEX_TITLE:I = 0x5

.field public static final INDEX_VERSION:I = 0x3

.field public static final INSTALL_PRELOAD_FILTER:Ljava/lang/String; = "com.sec.android.filter.install"

.field public static final ORDER:Ljava/lang/String; = "filter_order"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field public static final PRELOAD_FILTER_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.filter"

.field public static final RESET_DB:Ljava/lang/String; = "com.sec.android.filter.reset"

.field public static final TAG:Ljava/lang/String; = "FilterInstaller"

.field static final TEXTURE_DIR:Ljava/lang/String;

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final VENDOR:Ljava/lang/String; = "vendor"

.field public static final VERBOSE_LOGGING:Z

.field public static final VERSION:Ljava/lang/String; = "version"

.field public static final _ID:Ljava/lang/String; = "_ID"

.field static final filtersUri:Landroid/net/Uri;

.field static final filtersUri_id:Landroid/net/Uri;

.field static final filtersUri_include_deleted:Landroid/net/Uri;

.field static final iconUri:Landroid/net/Uri;

.field static final packagesUri:Landroid/net/Uri;

.field static final textureUri:Landroid/net/Uri;

.field static final titleUri:Landroid/net/Uri;


# instance fields
.field private mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

.field private mStartId:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 35
    const-string v0, "FilterInstaller"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/DownFilters/Lib"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->FILTER_DIR:Ljava/lang/String;

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/DownFilters/Res"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->DRAWABLE_DIR:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/DownFilters/Tex"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->TEXTURE_DIR:Ljava/lang/String;

    .line 58
    const-string v0, "content://com.samsung.android.provider.filterprovider/filters"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->filtersUri:Landroid/net/Uri;

    .line 59
    const-string v0, "content://com.samsung.android.provider.filterprovider/filters/include_deleted"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->filtersUri_include_deleted:Landroid/net/Uri;

    .line 60
    const-string v0, "content://com.samsung.android.provider.filterprovider/filters/#"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->filtersUri_id:Landroid/net/Uri;

    .line 61
    const-string v0, "content://com.samsung.android.provider.filterprovider/packages"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->packagesUri:Landroid/net/Uri;

    .line 62
    const-string v0, "content://com.samsung.android.provider.filterprovider/icons"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->iconUri:Landroid/net/Uri;

    .line 63
    const-string v0, "content://com.samsung.android.provider.filterprovider/titles"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->titleUri:Landroid/net/Uri;

    .line 64
    const-string v0, "content://com.samsung.android.provider.filterprovider/textures"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->textureUri:Landroid/net/Uri;

    .line 85
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "name"

    aput-object v2, v0, v1

    const-string v1, "filename"

    aput-object v1, v0, v3

    const/4 v1, 0x3

    const-string v2, "version"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "vendor"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "package_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "deleted"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "filter_order"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->FILTER_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 104
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/filterinstaller/FilterPackageService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/filterinstaller/FilterPackageService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->removePackageColon(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/filterinstaller/FilterPackageService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    .prologue
    .line 32
    iget v0, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mStartId:I

    return v0
.end method

.method private removePackageColon(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "packageUri"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x0

    .line 100
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "package:"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 93
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 161
    sget-boolean v1, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v1, :cond_0

    .line 162
    const-string v1, "FilterInstaller"

    const-string v2, "FilterPackageService.onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "FilterPackageServiceHandler"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 167
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 169
    new-instance v1, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;-><init>(Lcom/samsung/android/app/filterinstaller/FilterPackageService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    .line 170
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 240
    sget-boolean v0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v0, :cond_0

    .line 241
    const-string v0, "FilterInstaller"

    const-string v1, "FilterPackageService.onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 174
    if-nez p1, :cond_0

    .line 175
    const-string v3, "FilterInstaller"

    const-string v4, "onStartCommand(null)"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :goto_0
    return v2

    .line 179
    :cond_0
    sget-boolean v4, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v4, :cond_1

    .line 180
    const-string v4, "FilterInstaller"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onStartCommand("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_1
    iput p3, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mStartId:I

    .line 185
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 187
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 188
    .local v1, "msg":Landroid/os/Message;
    iput v3, v1, Landroid/os/Message;->arg1:I

    .line 189
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 191
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .end local v1    # "msg":Landroid/os/Message;
    :goto_1
    move v2, v3

    .line 231
    goto :goto_0

    .line 192
    :cond_2
    const-string v4, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 193
    iget-object v4, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v4}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 194
    .restart local v1    # "msg":Landroid/os/Message;
    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 195
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 197
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 198
    .end local v1    # "msg":Landroid/os/Message;
    :cond_3
    const-string v2, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 199
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 200
    .restart local v1    # "msg":Landroid/os/Message;
    const/4 v2, 0x3

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 201
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 203
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 204
    .end local v1    # "msg":Landroid/os/Message;
    :cond_4
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 205
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 206
    .restart local v1    # "msg":Landroid/os/Message;
    const/4 v2, 0x4

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 207
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 209
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 210
    .end local v1    # "msg":Landroid/os/Message;
    :cond_5
    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 211
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 212
    .restart local v1    # "msg":Landroid/os/Message;
    const/4 v2, 0x5

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 214
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 215
    .end local v1    # "msg":Landroid/os/Message;
    :cond_6
    const-string v2, "com.sec.android.filter.install"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 216
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 217
    .restart local v1    # "msg":Landroid/os/Message;
    const/4 v2, 0x6

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 218
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 220
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 221
    .end local v1    # "msg":Landroid/os/Message;
    :cond_7
    const-string v2, "com.sec.android.filter.reset"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 222
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 223
    .restart local v1    # "msg":Landroid/os/Message;
    const/4 v2, 0x7

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 224
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 226
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mServiceHandler:Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 228
    .end local v1    # "msg":Landroid/os/Message;
    :cond_8
    iget v2, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mStartId:I

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->stopSelfResult(I)Z

    goto/16 :goto_1
.end method

.method public stopFilterInstaller()V
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->mStartId:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->stopSelfResult(I)Z

    .line 236
    return-void
.end method
