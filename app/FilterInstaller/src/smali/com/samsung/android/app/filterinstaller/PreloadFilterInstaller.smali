.class public Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;
.super Ljava/lang/Object;
.source "PreloadFilterInstaller.java"


# static fields
.field private static final ICON_SIZE:I = 0x100

.field private static final TAG:Ljava/lang/String; = "PreloadFilterInstaller"

.field private static final num_preload_filter:I

.field private static final num_preload_filter_texture:I = 0x2


# instance fields
.field private mLastFilterId:I

.field final mPackageName:Ljava/lang/String;

.field final mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    :goto_0
    sput v0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->num_preload_filter:I

    return-void

    :cond_0
    const/16 v0, 0xc

    goto :goto_0
.end method

.method public constructor <init>(Lcom/samsung/android/app/filterinstaller/FilterPackageService;Ljava/lang/String;)V
    .locals 1
    .param p1, "serviceContext"    # Lcom/samsung/android/app/filterinstaller/FilterPackageService;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    .line 61
    iput-object p2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mPackageName:Ljava/lang/String;

    .line 62
    iput-object p1, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    .line 63
    return-void
.end method

.method private copyFileFromAssets(Landroid/content/res/AssetManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 15
    .param p1, "am"    # Landroid/content/res/AssetManager;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "inputPath"    # Ljava/lang/String;
    .param p4, "outPath"    # Ljava/lang/String;
    .param p5, "executable"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 171
    const/4 v10, 0x0

    .line 172
    .local v10, "filter":Ljava/io/File;
    const/4 v12, 0x0

    .line 173
    .local v12, "is":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 174
    .local v2, "bis":Ljava/io/BufferedInputStream;
    const/4 v4, 0x0

    .line 176
    .local v4, "bos":Ljava/io/BufferedOutputStream;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, "assetsPath":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 180
    .local v8, "dstFile":Ljava/lang/String;
    :try_start_0
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    .end local v10    # "filter":Ljava/io/File;
    .local v11, "filter":Ljava/io/File;
    :try_start_1
    invoke-virtual {v11}, Ljava/io/File;->createNewFile()Z

    .line 183
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v12

    .line 184
    new-instance v5, Ljava/io/BufferedOutputStream;

    new-instance v13, Ljava/io/FileOutputStream;

    invoke-direct {v13, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v13}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 185
    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .local v5, "bos":Ljava/io/BufferedOutputStream;
    :try_start_2
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v12}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 187
    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .local v3, "bis":Ljava/io/BufferedInputStream;
    const/high16 v13, 0x100000

    :try_start_3
    new-array v6, v13, [B

    .line 189
    .local v6, "buffer":[B
    :goto_0
    invoke-virtual {v3, v6}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v7

    .local v7, "count":I
    const/4 v13, -0x1

    if-eq v7, v13, :cond_0

    .line 190
    const/4 v13, 0x0

    invoke-virtual {v5, v6, v13, v7}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_0

    .line 197
    .end local v6    # "buffer":[B
    .end local v7    # "count":I
    :catch_0
    move-exception v9

    move-object v4, v5

    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    move-object v2, v3

    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    move-object v10, v11

    .line 198
    .end local v11    # "filter":Ljava/io/File;
    .local v9, "e":Ljava/lang/Exception;
    .restart local v10    # "filter":Ljava/io/File;
    :goto_1
    :try_start_4
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 200
    invoke-virtual {p0, v12}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->closeSilently(Ljava/io/Closeable;)V

    .line 201
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->closeSilently(Ljava/io/Closeable;)V

    .line 202
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->closeSilently(Ljava/io/Closeable;)V

    .line 204
    .end local v9    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    .line 192
    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "filter":Ljava/io/File;
    .restart local v3    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v6    # "buffer":[B
    .restart local v7    # "count":I
    .restart local v11    # "filter":Ljava/io/File;
    :cond_0
    :try_start_5
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->flush()V

    .line 194
    const/4 v13, 0x0

    move/from16 v0, p5

    invoke-virtual {v11, v0, v13}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 195
    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-virtual {v11, v13, v14}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 200
    invoke-virtual {p0, v12}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->closeSilently(Ljava/io/Closeable;)V

    .line 201
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->closeSilently(Ljava/io/Closeable;)V

    .line 202
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->closeSilently(Ljava/io/Closeable;)V

    move-object v4, v5

    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    move-object v2, v3

    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    move-object v10, v11

    .line 203
    .end local v11    # "filter":Ljava/io/File;
    .restart local v10    # "filter":Ljava/io/File;
    goto :goto_2

    .line 200
    .end local v6    # "buffer":[B
    .end local v7    # "count":I
    :catchall_0
    move-exception v13

    :goto_3
    invoke-virtual {p0, v12}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->closeSilently(Ljava/io/Closeable;)V

    .line 201
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->closeSilently(Ljava/io/Closeable;)V

    .line 202
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->closeSilently(Ljava/io/Closeable;)V

    throw v13

    .line 200
    .end local v10    # "filter":Ljava/io/File;
    .restart local v11    # "filter":Ljava/io/File;
    :catchall_1
    move-exception v13

    move-object v10, v11

    .end local v11    # "filter":Ljava/io/File;
    .restart local v10    # "filter":Ljava/io/File;
    goto :goto_3

    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "filter":Ljava/io/File;
    .restart local v5    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "filter":Ljava/io/File;
    :catchall_2
    move-exception v13

    move-object v4, v5

    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "filter":Ljava/io/File;
    .restart local v10    # "filter":Ljava/io/File;
    goto :goto_3

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "filter":Ljava/io/File;
    .restart local v3    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "filter":Ljava/io/File;
    :catchall_3
    move-exception v13

    move-object v4, v5

    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    move-object v2, v3

    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    move-object v10, v11

    .end local v11    # "filter":Ljava/io/File;
    .restart local v10    # "filter":Ljava/io/File;
    goto :goto_3

    .line 197
    :catch_1
    move-exception v9

    goto :goto_1

    .end local v10    # "filter":Ljava/io/File;
    .restart local v11    # "filter":Ljava/io/File;
    :catch_2
    move-exception v9

    move-object v10, v11

    .end local v11    # "filter":Ljava/io/File;
    .restart local v10    # "filter":Ljava/io/File;
    goto :goto_1

    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "filter":Ljava/io/File;
    .restart local v5    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "filter":Ljava/io/File;
    :catch_3
    move-exception v9

    move-object v4, v5

    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "filter":Ljava/io/File;
    .restart local v10    # "filter":Ljava/io/File;
    goto :goto_1
.end method

.method private copyFileFromRes(Landroid/content/res/Resources;Ljava/lang/String;)V
    .locals 10
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "iconName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 207
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->DRAWABLE_DIR:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".png"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 209
    .local v5, "pngFilepath":Ljava/lang/String;
    const-string v7, "drawable"

    iget-object v8, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mPackageName:Ljava/lang/String;

    invoke-virtual {p1, p2, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 210
    .local v6, "resID":I
    invoke-virtual {p1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 212
    .local v0, "filterIcon":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_0

    .line 225
    .end local v0    # "filterIcon":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-void

    .line 215
    .restart local v0    # "filterIcon":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-direct {p0, v7, v8}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getBestDimension(II)Landroid/graphics/Point;

    move-result-object v3

    .line 216
    .local v3, "p":Landroid/graphics/Point;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "filterIcon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 217
    .local v4, "packageBitmap":Landroid/graphics/Bitmap;
    iget v7, v3, Landroid/graphics/Point;->x:I

    iget v8, v3, Landroid/graphics/Point;->y:I

    invoke-static {v4, v7, v8, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 219
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 220
    .local v1, "icon":Ljava/io/File;
    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 221
    .local v2, "out":Ljava/io/BufferedOutputStream;
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x50

    invoke-virtual {v4, v7, v8, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 222
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->closeSilently(Ljava/io/Closeable;)V

    .line 224
    const/4 v7, 0x1

    invoke-virtual {v1, v7, v9}, Ljava/io/File;->setReadable(ZZ)Z

    goto :goto_0
.end method

.method private createDownloadFilterDirectory()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 145
    const/4 v7, 0x3

    new-array v3, v7, [Ljava/lang/String;

    sget-object v7, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->FILTER_DIR:Ljava/lang/String;

    aput-object v7, v3, v10

    sget-object v7, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->DRAWABLE_DIR:Ljava/lang/String;

    aput-object v7, v3, v11

    const/4 v7, 0x2

    sget-object v8, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->TEXTURE_DIR:Ljava/lang/String;

    aput-object v8, v3, v7

    .line 147
    .local v3, "dirs":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 148
    .local v1, "dest":Ljava/io/File;
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v2, v0, v4

    .line 149
    .local v2, "dir":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    .end local v1    # "dest":Ljava/io/File;
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 150
    .restart local v1    # "dest":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 151
    sget-boolean v7, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->VERBOSE_LOGGING:Z

    if-eqz v7, :cond_0

    .line 152
    const-string v7, "PreloadFilterInstaller"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "create new directory:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_0
    invoke-virtual {v1, v11, v10}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 157
    invoke-virtual {v1, v11, v10}, Ljava/io/File;->setReadable(ZZ)Z

    .line 148
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 160
    .end local v2    # "dir":Ljava/lang/String;
    :cond_1
    if-eqz v1, :cond_2

    .line 162
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v6

    .line 163
    .local v6, "parent":Ljava/io/File;
    if-eqz v6, :cond_2

    .line 164
    invoke-virtual {v6, v11, v10}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 165
    invoke-virtual {v6, v11, v10}, Ljava/io/File;->setReadable(ZZ)Z

    .line 168
    .end local v6    # "parent":Ljava/io/File;
    :cond_2
    return-void
.end method

.method private getBestDimension(II)Landroid/graphics/Point;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    const/16 v0, 0x100

    .line 129
    if-le p1, p2, :cond_1

    .line 130
    if-le p1, v0, :cond_0

    .line 131
    mul-int/lit16 v0, p2, 0x100

    div-int p2, v0, p1

    .line 132
    const/16 p1, 0x100

    .line 141
    :cond_0
    :goto_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0

    .line 135
    :cond_1
    if-le p2, v0, :cond_0

    .line 136
    mul-int/lit16 v0, p1, 0x100

    div-int p1, v0, p2

    .line 137
    const/16 p2, 0x100

    goto :goto_0
.end method

.method private getFilterIndex(Ljava/lang/String;)I
    .locals 3
    .param p1, "filterName"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 487
    const/4 v0, 0x0

    .line 489
    .local v0, "ret":I
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 492
    const-string v2, "VIGNETTE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 493
    const/4 v0, 0x0

    :goto_0
    move v1, v0

    .line 541
    :cond_0
    return v1

    .line 494
    :cond_1
    const-string v2, "VINTAGE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    .line 495
    const/4 v0, 0x1

    goto :goto_0

    .line 496
    :cond_2
    const-string v2, "FADEDCOLOUR"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    .line 497
    const/4 v0, 0x2

    goto :goto_0

    .line 498
    :cond_3
    const-string v2, "GREYSCALE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_4

    .line 499
    const/4 v0, 0x3

    goto :goto_0

    .line 500
    :cond_4
    const-string v2, "SEPIA"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_5

    .line 501
    const/4 v0, 0x4

    goto :goto_0

    .line 502
    :cond_5
    const-string v2, "TINT"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_6

    .line 503
    const/4 v0, 0x5

    goto :goto_0

    .line 504
    :cond_6
    const-string v2, "TURQUOISE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_7

    .line 505
    const/4 v0, 0x6

    goto :goto_0

    .line 506
    :cond_7
    const-string v2, "FISHEYE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_8

    .line 507
    const/4 v0, 0x7

    goto :goto_0

    .line 508
    :cond_8
    const-string v2, "RUGGED"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 509
    const/16 v0, 0x8

    goto :goto_0

    .line 513
    :cond_9
    const-string v2, "VIGNETTE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_a

    .line 514
    const/4 v0, 0x0

    goto :goto_0

    .line 515
    :cond_a
    const-string v2, "VINTAGE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_b

    .line 516
    const/4 v0, 0x1

    goto :goto_0

    .line 517
    :cond_b
    const-string v2, "FADEDCOLOUR"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_c

    .line 518
    const/4 v0, 0x2

    goto :goto_0

    .line 519
    :cond_c
    const-string v2, "GREYSCALE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_d

    .line 520
    const/4 v0, 0x3

    goto :goto_0

    .line 521
    :cond_d
    const-string v2, "SEPIA"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_e

    .line 522
    const/4 v0, 0x4

    goto/16 :goto_0

    .line 523
    :cond_e
    const-string v2, "TINT"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_f

    .line 524
    const/4 v0, 0x5

    goto/16 :goto_0

    .line 525
    :cond_f
    const-string v2, "TURQUOISE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_10

    .line 526
    const/4 v0, 0x6

    goto/16 :goto_0

    .line 527
    :cond_10
    const-string v2, "CARTOON"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_11

    .line 528
    const/4 v0, 0x7

    goto/16 :goto_0

    .line 529
    :cond_11
    const-string v2, "FISHEYE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_12

    .line 530
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 531
    :cond_12
    const-string v2, "MOODY"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_13

    .line 532
    const/16 v0, 0x9

    goto/16 :goto_0

    .line 533
    :cond_13
    const-string v2, "OILPASTEL"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_14

    .line 534
    const/16 v0, 0xa

    goto/16 :goto_0

    .line 535
    :cond_14
    const-string v2, "RUGGED"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 536
    const/16 v0, 0xb

    goto/16 :goto_0
.end method

.method private initFilterValues([Landroid/content/ContentValues;)V
    .locals 2
    .param p1, "filterValues"    # [Landroid/content/ContentValues;

    .prologue
    .line 369
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    const-string v0, "FADEDCOLOUR"

    const-string v1, "com.sec.android.filter.libfadedcolours.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const-string v0, "FISHEYE"

    const-string v1, "com.sec.android.filter.libfisheye.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-string v0, "GREYSCALE"

    const-string v1, "com.sec.android.filter.libgreyscale.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    const-string v0, "RUGGED"

    const-string v1, "com.sec.android.filter.librugged.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    const-string v0, "SEPIA"

    const-string v1, "com.sec.android.filter.libsepia.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const-string v0, "TINT"

    const-string v1, "com.sec.android.filter.libtint.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const-string v0, "TURQUOISE"

    const-string v1, "com.sec.android.filter.libturquoise.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    const-string v0, "VIGNETTE"

    const-string v1, "com.sec.android.filter.libvignette.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const-string v0, "VINTAGE"

    const-string v1, "com.sec.android.filter.libvintage.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :goto_0
    return-void

    .line 382
    :cond_0
    const-string v0, "CARTOON"

    const-string v1, "com.sec.android.filter.libcartoon.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    const-string v0, "FADEDCOLOUR"

    const-string v1, "com.sec.android.filter.libfadedcolours.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    const-string v0, "FISHEYE"

    const-string v1, "com.sec.android.filter.libfisheye.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    const-string v0, "GREYSCALE"

    const-string v1, "com.sec.android.filter.libgreyscale.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    const-string v0, "MOODY"

    const-string v1, "com.sec.android.filter.libmoody.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    const-string v0, "OILPASTEL"

    const-string v1, "com.sec.android.filter.liboilpastel.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    const-string v0, "RUGGED"

    const-string v1, "com.sec.android.filter.librugged.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    const-string v0, "SEPIA"

    const-string v1, "com.sec.android.filter.libsepia.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    const-string v0, "TINT"

    const-string v1, "com.sec.android.filter.libtint.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    const-string v0, "TURQUOISE"

    const-string v1, "com.sec.android.filter.libturquoise.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const-string v0, "VIGNETTE"

    const-string v1, "com.sec.android.filter.libvignette.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    const-string v0, "VINTAGE"

    const-string v1, "com.sec.android.filter.libvintage.so"

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initIconValues([Landroid/content/ContentValues;)V
    .locals 3
    .param p1, "iconValues"    # [Landroid/content/ContentValues;

    .prologue
    .line 404
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    const-string v0, "FADEDCOLOUR"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libfadedcolours.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 407
    const-string v0, "FISHEYE"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libfisheye.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 408
    const-string v0, "GREYSCALE"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libgreyscale.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x3

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 409
    const-string v0, "RUGGED"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.librugged.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x4

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 410
    const-string v0, "SEPIA"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libsepia.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x5

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 411
    const-string v0, "TINT"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libtint.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x6

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 412
    const-string v0, "TURQUOISE"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libturquoise.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x7

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 413
    const-string v0, "VIGNETTE"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libvignette.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x8

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 414
    const-string v0, "VINTAGE"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libvintage.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x9

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 430
    :goto_0
    return-void

    .line 417
    :cond_0
    const-string v0, "FADEDCOLOUR"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libfadedcolours.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 418
    const-string v0, "FISHEYE"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libfisheye.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x3

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 419
    const-string v0, "GREYSCALE"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libgreyscale.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x4

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 420
    const-string v0, "RUGGED"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.librugged.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x7

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 421
    const-string v0, "SEPIA"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libsepia.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x8

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 422
    const-string v0, "TINT"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libtint.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x9

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 423
    const-string v0, "TURQUOISE"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libturquoise.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0xa

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 424
    const-string v0, "VIGNETTE"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libvignette.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0xb

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 425
    const-string v0, "VINTAGE"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libvintage.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0xc

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 426
    const-string v0, "OILPASTEL"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.liboilpastel.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x6

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 427
    const-string v0, "MOODY"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libmoody.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x5

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 428
    const-string v0, "CARTOON"

    const-string v1, "/system/cameradata/preloadfilters/Res/com.sec.android.filter.libcartoon.png"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method

.method private initTextureValues([Landroid/content/ContentValues;)V
    .locals 5
    .param p1, "textureValues"    # [Landroid/content/ContentValues;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 476
    aget-object v0, p1, v3

    const-string v1, "package_name"

    const-string v2, "com.sec.android.filter"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    aget-object v0, p1, v3

    const-string v1, "filename"

    const-string v2, "/system/cameradata/preloadfilters/Tex/com.sec.android.filter.tex_liboilpastel.png"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    aget-object v0, p1, v3

    const-string v1, "filter_id"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 481
    aget-object v0, p1, v4

    const-string v1, "package_name"

    const-string v2, "com.sec.android.filter"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    aget-object v0, p1, v4

    const-string v1, "filename"

    const-string v2, "/system/cameradata/preloadfilters/Tex/com.sec.android.filter.tex_librugged.png"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    aget-object v0, p1, v4

    const-string v1, "filter_id"

    iget v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    add-int/lit8 v2, v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 484
    return-void
.end method

.method private initTitleValues([Landroid/content/ContentValues;Landroid/content/res/Resources;)V
    .locals 2
    .param p1, "titleValues"    # [Landroid/content/ContentValues;
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 445
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447
    const-string v0, "FADEDCOLOUR"

    const-string v1, "libfadedcolours_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 448
    const-string v0, "FISHEYE"

    const-string v1, "libfisheye_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 449
    const-string v0, "GREYSCALE"

    const-string v1, "libgreyscale_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 450
    const-string v0, "RUGGED"

    const-string v1, "librugged_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 451
    const-string v0, "SEPIA"

    const-string v1, "libsepia_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 452
    const-string v0, "TINT"

    const-string v1, "libtint_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 453
    const-string v0, "TURQUOISE"

    const-string v1, "libturquoise_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 454
    const-string v0, "VIGNETTE"

    const-string v1, "libvignette_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 455
    const-string v0, "VINTAGE"

    const-string v1, "libvintage_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 472
    :goto_0
    return-void

    .line 458
    :cond_0
    const-string v0, "FADEDCOLOUR"

    const-string v1, "libfadedcolours_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 459
    const-string v0, "FISHEYE"

    const-string v1, "libfisheye_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 460
    const-string v0, "GREYSCALE"

    const-string v1, "libgreyscale_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 461
    const-string v0, "RUGGED"

    const-string v1, "librugged_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 462
    const-string v0, "SEPIA"

    const-string v1, "libsepia_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 463
    const-string v0, "TINT"

    const-string v1, "libtint_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 464
    const-string v0, "TURQUOISE"

    const-string v1, "libturquoise_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 465
    const-string v0, "VIGNETTE"

    const-string v1, "libvignette_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 466
    const-string v0, "VINTAGE"

    const-string v1, "libvintage_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 467
    const-string v0, "OILPASTEL"

    const-string v1, "liboilpastel_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 468
    const-string v0, "MOODY"

    const-string v1, "libmoody_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 469
    const-string v0, "CARTOON"

    const-string v1, "libcartoon_title"

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    goto :goto_0
.end method

.method private putFilterValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "filterValues"    # [Landroid/content/ContentValues;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 350
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "name"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "filename"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "package_name"

    const-string v2, "com.sec.android.filter"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "vendor"

    const-string v2, "SAMSUNG_MOBILE"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "version"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 357
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "posx"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 358
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "posy"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 359
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "width"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 360
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "height"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 361
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "filter_type"

    const-string v2, "SINGLE"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "handler"

    const-string v2, "TRUE"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    return-void
.end method

.method private putIconValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "iconValues"    # [Landroid/content/ContentValues;
    .param p2, "filterName"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "filterId"    # I

    .prologue
    .line 398
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "package_name"

    const-string v2, "com.sec.android.filter"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "filename"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v0

    aget-object v0, p1, v0

    const-string v1, "filter_id"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 401
    return-void
.end method

.method private putTitleValues([Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V
    .locals 4
    .param p1, "titleValues"    # [Landroid/content/ContentValues;
    .param p2, "filterName"    # Ljava/lang/String;
    .param p3, "titleName"    # Ljava/lang/String;
    .param p4, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 433
    const/4 v0, 0x0

    .line 435
    .local v0, "resId":I
    const-string v1, "string"

    const-string v2, "com.sec.android.filter"

    invoke-virtual {p4, p3, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 436
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v1

    aget-object v1, p1, v1

    const-string v2, "package_name"

    const-string v3, "com.sec.android.filter"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v1

    aget-object v1, p1, v1

    const-string v2, "country"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v1

    aget-object v1, p1, v1

    const-string v2, "filter_id"

    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 439
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v1

    aget-object v1, p1, v1

    const-string v2, "title"

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    invoke-direct {p0, p2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->getFilterIndex(Ljava/lang/String;)I

    move-result v1

    aget-object v1, p1, v1

    const-string v2, "resource_name"

    invoke-virtual {v1, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    return-void
.end method

.method private removeAllRecord()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 229
    iget-object v2, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v2}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 230
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->packagesUri:Landroid/net/Uri;

    const-string v3, "com.sec.android.filter"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 231
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 232
    invoke-virtual {v0, v1, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 234
    :cond_0
    return-void
.end method


# virtual methods
.method public InitializePreloadFilterDB(Landroid/content/res/Resources;)V
    .locals 10
    .param p1, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 288
    iget-object v5, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v5}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 289
    .local v0, "cr":Landroid/content/ContentResolver;
    sget v5, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->num_preload_filter:I

    new-array v4, v5, [Landroid/content/ContentValues;

    .line 290
    .local v4, "values":[Landroid/content/ContentValues;
    new-array v3, v9, [Landroid/content/ContentValues;

    .line 291
    .local v3, "tex_values":[Landroid/content/ContentValues;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget v5, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->num_preload_filter:I

    if-ge v1, v5, :cond_0

    .line 292
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    aput-object v5, v4, v1

    .line 291
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 294
    :cond_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v9, :cond_1

    .line 295
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    aput-object v5, v3, v1

    .line 294
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 298
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->removeAllRecord()V

    .line 300
    aget-object v5, v4, v8

    const-string v6, "name"

    const-string v7, "com.sec.android.filter"

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    sget-object v5, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->packagesUri:Landroid/net/Uri;

    aget-object v6, v4, v8

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 302
    aget-object v5, v4, v8

    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 304
    invoke-direct {p0, v4}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->initFilterValues([Landroid/content/ContentValues;)V

    .line 305
    sget-object v5, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->filtersUri:Landroid/net/Uri;

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v2

    .line 306
    .local v2, "ret":I
    sget v5, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->num_preload_filter:I

    if-eq v5, v2, :cond_3

    .line 307
    const-string v5, "PreloadFilterInstaller"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "filter table : inserted record is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    :cond_2
    :goto_2
    return-void

    .line 311
    :cond_3
    const/4 v1, 0x0

    :goto_3
    sget v5, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->num_preload_filter:I

    if-ge v1, v5, :cond_4

    .line 312
    aget-object v5, v4, v1

    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 311
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 314
    :cond_4
    invoke-direct {p0, v4}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->initIconValues([Landroid/content/ContentValues;)V

    .line 315
    sget-object v5, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->iconUri:Landroid/net/Uri;

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v2

    .line 316
    sget v5, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->num_preload_filter:I

    if-eq v5, v2, :cond_5

    .line 317
    const-string v5, "PreloadFilterInstaller"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "icon table : inserted record is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 321
    :cond_5
    const/4 v1, 0x0

    :goto_4
    sget v5, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->num_preload_filter:I

    if-ge v1, v5, :cond_6

    .line 322
    aget-object v5, v4, v1

    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 321
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 324
    :cond_6
    invoke-direct {p0, v4, p1}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->initTitleValues([Landroid/content/ContentValues;Landroid/content/res/Resources;)V

    .line 325
    sget-object v5, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->titleUri:Landroid/net/Uri;

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v2

    .line 326
    sget v5, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->num_preload_filter:I

    if-eq v5, v2, :cond_7

    .line 327
    const-string v5, "PreloadFilterInstaller"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "title table : inserted record is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 331
    :cond_7
    invoke-direct {p0, v3}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->initTextureValues([Landroid/content/ContentValues;)V

    .line 332
    sget-object v5, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->textureUri:Landroid/net/Uri;

    invoke-virtual {v0, v5, v3}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v2

    .line 333
    if-eq v9, v2, :cond_2

    .line 334
    const-string v5, "PreloadFilterInstaller"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "texture table : inserted record is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public closeSilently(Ljava/io/Closeable;)V
    .locals 1
    .param p1, "c"    # Ljava/io/Closeable;

    .prologue
    .line 340
    if-nez p1, :cond_0

    .line 347
    :goto_0
    return-void

    .line 343
    :cond_0
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 344
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public installFilters()V
    .locals 6

    .prologue
    .line 67
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->loadExternalFilters()Z

    move-result v3

    if-nez v3, :cond_0

    .line 68
    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    iget-object v4, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mPackageName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 69
    .local v0, "con":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 70
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->InitializePreloadFilterDB(Landroid/content/res/Resources;)V

    .line 118
    .end local v0    # "con":Landroid/content/Context;
    .end local v2    # "res":Landroid/content/res/Resources;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v3}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->stopFilterInstaller()V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 126
    :goto_0
    return-void

    .line 121
    :catch_0
    move-exception v1

    .line 122
    .local v1, "e":Ljava/lang/SecurityException;
    invoke-virtual {v1}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    .line 123
    .end local v1    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v1

    .line 124
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 120
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v3

    goto :goto_0
.end method

.method public loadExternalFilters()Z
    .locals 17

    .prologue
    .line 237
    const/4 v7, 0x0

    .line 239
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mServiceContext:Lcom/samsung/android/app/filterinstaller/FilterPackageService;

    invoke-virtual {v1}, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->filtersUri_include_deleted:Landroid/net/Uri;

    sget-object v3, Lcom/samsung/android/app/filterinstaller/FilterPackageService;->FILTER_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 240
    const/4 v10, 0x0

    .line 241
    .local v10, "filterCount":I
    :cond_0
    :goto_0
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 242
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 243
    .local v12, "id":I
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->mLastFilterId:I

    .line 245
    const/4 v1, 0x1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 246
    .local v9, "effectName":Ljava/lang/String;
    if-eqz v9, :cond_0

    const-string v1, "Unnamed filter"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 249
    const/4 v1, 0x2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 250
    .local v11, "filterName":Ljava/lang/String;
    if-eqz v11, :cond_0

    const-string v1, "Unnamed filter"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 253
    const/4 v1, 0x3

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 255
    .local v16, "version":I
    const/4 v1, 0x4

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 256
    .local v15, "vendor":Ljava/lang/String;
    if-eqz v15, :cond_0

    const-string v1, "Unknown vendor"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 259
    const/4 v1, 0x5

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 260
    .local v14, "title":Ljava/lang/String;
    if-eqz v14, :cond_0

    const-string v1, "Unknown title"

    invoke-virtual {v14, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 263
    const/4 v1, 0x6

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 264
    .local v13, "packageName":Ljava/lang/String;
    if-eqz v13, :cond_0

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 267
    add-int/lit8 v10, v10, 0x1

    .line 268
    goto :goto_0

    .line 269
    .end local v9    # "effectName":Ljava/lang/String;
    .end local v11    # "filterName":Ljava/lang/String;
    .end local v12    # "id":I
    .end local v13    # "packageName":Ljava/lang/String;
    .end local v14    # "title":Ljava/lang/String;
    .end local v15    # "vendor":Ljava/lang/String;
    .end local v16    # "version":I
    :cond_1
    sget v1, Lcom/samsung/android/app/filterinstaller/PreloadFilterInstaller;->num_preload_filter:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v10, v1, :cond_3

    .line 270
    const/4 v1, 0x0

    .line 277
    if-eqz v7, :cond_2

    .line 278
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 282
    .end local v10    # "filterCount":I
    :cond_2
    :goto_1
    return v1

    .line 272
    .restart local v10    # "filterCount":I
    :cond_3
    const/4 v1, 0x1

    .line 277
    if-eqz v7, :cond_2

    .line 278
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 274
    .end local v10    # "filterCount":I
    :catch_0
    move-exception v8

    .line 275
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 277
    if-eqz v7, :cond_4

    .line 278
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 282
    :cond_4
    const/4 v1, 0x1

    goto :goto_1

    .line 277
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_5

    .line 278
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1
.end method
