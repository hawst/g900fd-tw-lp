.class public Lcom/sec/android/service/sm/job/SecurityJob;
.super Ljava/lang/Object;
.source "SecurityJob.java"


# static fields
.field private static final CHECK_ICD_SUCCESS:Ljava/lang/String; = "1"

.field private static final ENFORCE_SECUREBOOT_OFF:I = 0x0

.field private static final ENFORCE_SECUREBOOT_ON:I = 0x1

.field private static final KNOX_INSTALLED_FILE:Ljava/lang/String; = "/data/system/edk_p_container_1"

.field private static final KNOX_MDPP_FILE:Ljava/lang/String; = "/data/system/edk_mdpp"

.field public static final MDPP_DISABLED:I = 0x8

.field public static final MDPP_EMPTY:I = 0x10

.field public static final MDPP_ENABLED:I = 0x1

.field public static final MDPP_ENFORCING:I = 0x2

.field public static final MDPP_READY:I = 0x4

.field private static final OLD_KEY_FILE:Ljava/lang/String; = "/data/system/password.key"

.field private static final PROPERTY_MDPP:Ljava/lang/String; = "security.mdpp"

.field private static final PROPERTY_MDPP_DISABLED:Ljava/lang/String; = "Disabled"

.field private static final PROPERTY_MDPP_EMPTY:Ljava/lang/String; = "None"

.field private static final PROPERTY_MDPP_ENABLED:Ljava/lang/String; = "Enabled"

.field private static final PROPERTY_MDPP_ENFORCING:Ljava/lang/String; = "Enforcing"

.field private static final PROPERTY_MDPP_READY:Ljava/lang/String; = "Ready"

.field private static final PROPERTY_MDPP_RELEASE:Ljava/lang/String; = "ro.security.mdpp.release"

.field public static final PROPERTY_MDPP_RESULT:Ljava/lang/String; = "security.mdpp.result"

.field private static final PROPERTY_MDPP_UX:Ljava/lang/String; = "ro.security.mdpp.ux"

.field private static final PROPERTY_MDPP_VER:Ljava/lang/String; = "ro.security.mdpp.ver"

.field public static final SEC_MANAGER_ERR_CCMODE_ALREADY_ENABLED:I = -0xe

.field public static final SEC_MANAGER_ERR_CCMODE_ALREADY_READY:I = -0xf

.field public static final SEC_MANAGER_ERR_CCMODE_DISABLED:I = -0xb

.field public static final SEC_MANAGER_ERR_CCMODE_ELSE:I = -0xd

.field public static final SEC_MANAGER_ERR_CCMODE_EMPTY:I = -0xc

.field public static final SEC_MANAGER_ERR_CCMODE_NOT_SUPPORT_CCMODE:I = -0x10

.field public static final SEC_MANAGER_ERR_ENFORCE_SB_FLAG_FAIL:I = -0x18

.field public static final SEC_MANAGER_ERR_GET_CCMODE_FLAG_FAIL:I = -0x1c

.field public static final SEC_MANAGER_ERR_GET_FIPS_STATUS_FAIL:I = -0x23

.field public static final SEC_MANAGER_ERR_ICD_FILE_NOT_EXIST:I = -0x16

.field public static final SEC_MANAGER_ERR_ICD_RESULT_INVALID:I = -0x17

.field public static final SEC_MANAGER_ERR_INTEGRITY_CHECK_FAIL:I = -0x15

.field public static final SEC_MANAGER_ERR_INVALID_INPUT:I = -0x1

.field public static final SEC_MANAGER_ERR_LOCK_FINGER_PRINT_SET:I = -0x27

.field public static final SEC_MANAGER_ERR_ODE_ENCRYPED_EXTERNAL:I = -0x21

.field public static final SEC_MANAGER_ERR_ODE_ENCRYPED_INTERNAL:I = -0x20

.field public static final SEC_MANAGER_ERR_ODE_ENCRYPED_NONE:I = -0x1f

.field public static final SEC_MANAGER_ERR_ODE_GETTING_STATUS_FAIL:I = -0x1e

.field public static final SEC_MANAGER_ERR_ODE_NOT_SET:I = -0x22

.field public static final SEC_MANAGER_ERR_OUT_OF_RANGE_OF_MAX_PW_COUNT:I = -0x19

.field public static final SEC_MANAGER_ERR_PASSWORD_HISTORY_LENGTH_SET:I = -0x25

.field public static final SEC_MANAGER_ERR_RECOVERY_PASSWORD_POLICY_SET:I = -0x24

.field public static final SEC_MANAGER_ERR_REVOCATION_CHECK_POLICY_SET:I = -0x26

.field public static final SEC_MANAGER_ERR_SCREENLOCK_NOT_SET:I = -0x1a

.field public static final SEC_MANAGER_ERR_SET_CCMODE_FLAG_FAIL:I = -0x1b

.field public static final SEC_MANAGER_ERR_SKMM_SELFTEST_FAIL:I = -0x14

.field public static final SEC_MANAGER_OK:I

.field private static mSecurityManagerService:Lcom/sec/android/service/sm/service/SecurityManagerService;

.field private static mSecurityNativeJob:Lcom/sec/android/service/sm/job/SecurityNativeJob;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Lcom/sec/android/service/sm/job/SecurityNativeJob;

    invoke-direct {v0}, Lcom/sec/android/service/sm/job/SecurityNativeJob;-><init>()V

    sput-object v0, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityNativeJob:Lcom/sec/android/service/sm/job/SecurityNativeJob;

    .line 79
    new-instance v0, Lcom/sec/android/service/sm/service/SecurityManagerService;

    invoke-direct {v0}, Lcom/sec/android/service/sm/service/SecurityManagerService;-><init>()V

    sput-object v0, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityManagerService:Lcom/sec/android/service/sm/service/SecurityManagerService;

    .line 80
    return-void
.end method

.method private static checkCCModeOnDevice()I
    .locals 6

    .prologue
    .line 291
    const/4 v4, 0x0

    .line 292
    .local v4, "result":I
    new-instance v3, Ljava/io/File;

    const-string v5, "/data/system/password.key"

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 293
    .local v3, "oldKeyFile":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v5, "/data/system/edk_p_container_1"

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 294
    .local v1, "knoxInstalledFile":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v5, "/data/system/edk_mdpp"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 295
    .local v2, "knoxMDPPFile":Ljava/io/File;
    sget-object v5, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityNativeJob:Lcom/sec/android/service/sm/job/SecurityNativeJob;

    invoke-virtual {v5}, Lcom/sec/android/service/sm/job/SecurityNativeJob;->getCCModeFlag()I

    move-result v0

    .line 297
    .local v0, "CCModeFlag":I
    const-string v5, "check the current status #1 ..."

    invoke-static {v5}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 298
    const/4 v5, 0x1

    if-ne v0, v5, :cond_0

    .line 299
    const/4 v4, 0x1

    .line 326
    :goto_0
    return v4

    .line 300
    :cond_0
    const/4 v5, 0x2

    if-ne v0, v5, :cond_1

    .line 301
    const/4 v4, 0x2

    .line 302
    goto :goto_0

    :cond_1
    const/4 v5, 0x4

    if-ne v0, v5, :cond_2

    .line 303
    const/4 v4, 0x4

    .line 304
    goto :goto_0

    :cond_2
    const/16 v5, 0x8

    if-ne v0, v5, :cond_3

    .line 305
    const/16 v4, 0x8

    .line 306
    goto :goto_0

    .line 308
    :cond_3
    const-string v5, "check the current status #2 ..."

    invoke-static {v5}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 309
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_6

    .line 310
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 311
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 312
    const/4 v4, 0x4

    .line 313
    goto :goto_0

    .line 315
    :cond_4
    const/16 v4, 0x10

    .line 317
    goto :goto_0

    .line 318
    :cond_5
    const/4 v4, 0x4

    .line 320
    goto :goto_0

    .line 322
    :cond_6
    const/16 v4, 0x10

    goto :goto_0
.end method

.method private static checkDevicePolicy()I
    .locals 11

    .prologue
    .line 330
    const/4 v8, 0x0

    .line 333
    .local v8, "res":I
    sget-object v9, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityManagerService:Lcom/sec/android/service/sm/service/SecurityManagerService;

    .line 334
    invoke-virtual {v9}, Lcom/sec/android/service/sm/service/SecurityManagerService;->getMaximumFailedPasswordsForWipe()I

    move-result v6

    .line 336
    .local v6, "maximumFailedPasswordsForWipe":I
    if-lez v6, :cond_0

    .line 337
    const/16 v9, 0xa

    if-le v6, v9, :cond_1

    .line 338
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failed. maximumFailedPasswordsForWipe = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 339
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 338
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 340
    const/16 v8, -0x19

    .line 413
    .end local v8    # "res":I
    :goto_0
    return v8

    .line 342
    .restart local v8    # "res":I
    :cond_1
    const-string v9, "MaxPassword check SUCCESS"

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 345
    sget-object v9, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityManagerService:Lcom/sec/android/service/sm/service/SecurityManagerService;

    invoke-virtual {v9}, Lcom/sec/android/service/sm/service/SecurityManagerService;->isKeyguardSecure()Z

    move-result v5

    .line 346
    .local v5, "isSecure":Z
    if-nez v5, :cond_2

    .line 347
    const-string v9, "Failed. the screen is not locked."

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 348
    const/16 v8, -0x1a

    goto :goto_0

    .line 350
    :cond_2
    const-string v9, "ScreenLock check SUCCESS"

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 353
    sget-object v9, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityManagerService:Lcom/sec/android/service/sm/service/SecurityManagerService;

    invoke-virtual {v9}, Lcom/sec/android/service/sm/service/SecurityManagerService;->getPasswordRecoverable()Z

    move-result v3

    .line 354
    .local v3, "isPasswordRecoverable":Z
    if-eqz v3, :cond_3

    .line 355
    const-string v9, "Failed. Recovery password policy has been set."

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 356
    const/16 v8, -0x24

    goto :goto_0

    .line 358
    :cond_3
    const-string v9, "RecoveryPassword check SUCCESS"

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 361
    sget-object v9, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityManagerService:Lcom/sec/android/service/sm/service/SecurityManagerService;

    invoke-virtual {v9}, Lcom/sec/android/service/sm/service/SecurityManagerService;->getPasswordHistoryLength()I

    move-result v7

    .line 362
    .local v7, "passwordHistoryLength":I
    if-eqz v7, :cond_4

    .line 363
    const-string v9, "Failed. PasswordHistoryLength has been set."

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 364
    const/16 v8, -0x25

    goto :goto_0

    .line 366
    :cond_4
    const-string v9, "PasswordHistoryLength check SUCCESS"

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 369
    sget-object v9, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityManagerService:Lcom/sec/android/service/sm/service/SecurityManagerService;

    invoke-virtual {v9}, Lcom/sec/android/service/sm/service/SecurityManagerService;->isRevocationCheckEnabled()Z

    move-result v4

    .line 370
    .local v4, "isRevocationCheckEnabled":Z
    if-nez v4, :cond_5

    .line 371
    const-string v9, "Failed. RevocationCheckPolicy has not been set yet."

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 372
    const/16 v8, -0x26

    goto :goto_0

    .line 374
    :cond_5
    const-string v9, "RevocationCheckPolicy check SUCCESS"

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 377
    sget-object v9, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityManagerService:Lcom/sec/android/service/sm/service/SecurityManagerService;

    invoke-virtual {v9}, Lcom/sec/android/service/sm/service/SecurityManagerService;->isLockFingerprintEnabled()Z

    move-result v2

    .line 378
    .local v2, "isLockFingerprintEnabled":Z
    if-eqz v2, :cond_6

    .line 379
    const-string v9, "Failed. FingerPrint has been set."

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 380
    const/16 v8, -0x27

    goto :goto_0

    .line 382
    :cond_6
    const-string v9, "LockFingerPrint check SUCCESS"

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 385
    sget-object v9, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityManagerService:Lcom/sec/android/service/sm/service/SecurityManagerService;

    .line 386
    invoke-virtual {v9}, Lcom/sec/android/service/sm/service/SecurityManagerService;->getSamsungEncryptionStatusForCC()I

    move-result v0

    .line 387
    .local v0, "isEncrypted":I
    sget-object v9, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityManagerService:Lcom/sec/android/service/sm/service/SecurityManagerService;

    invoke-virtual {v9}, Lcom/sec/android/service/sm/service/SecurityManagerService;->isExternalSDRemovable()Z

    move-result v1

    .line 388
    .local v1, "isExternalSDRemovable":Z
    packed-switch v0, :pswitch_data_0

    .line 409
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failed. ODE check = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 410
    const/16 v8, -0x22

    goto/16 :goto_0

    .line 390
    :pswitch_0
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failed. ODE check = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 391
    const/16 v8, -0x1e

    goto/16 :goto_0

    .line 393
    :pswitch_1
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failed. ODE check = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 394
    const/16 v8, -0x1f

    goto/16 :goto_0

    .line 396
    :pswitch_2
    const-string v9, "ODE check SUCCESS"

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 399
    :pswitch_3
    if-eqz v1, :cond_7

    .line 400
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failed. ODE check = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 401
    const/16 v8, -0x20

    goto/16 :goto_0

    .line 403
    :cond_7
    const-string v9, "ODE check SUCCESS"

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 406
    :pswitch_4
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failed. ODE check = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 407
    const/16 v8, -0x21

    goto/16 :goto_0

    .line 388
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static checkICD()I
    .locals 6

    .prologue
    const/16 v4, -0x17

    const/16 v3, -0x16

    .line 444
    const/4 v0, 0x0

    .line 446
    .local v0, "res":I
    new-instance v2, Ljava/io/File;

    const-string v5, "/system/bin/icd"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 447
    .local v2, "target":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 448
    const-string v4, "#1 icd doesn\'t exist."

    invoke-static {v4}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    move v0, v3

    .line 476
    .end local v0    # "res":I
    :cond_0
    :goto_0
    return v0

    .line 452
    .restart local v0    # "res":I
    :cond_1
    new-instance v2, Ljava/io/File;

    .end local v2    # "target":Ljava/io/File;
    const-string v5, "/dev/icd"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 453
    .restart local v2    # "target":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 454
    invoke-static {v2}, Lcom/sec/android/service/sm/util/FileIO;->getLineInFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 455
    .local v1, "result":Ljava/lang/String;
    const-string v5, "1"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 456
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Failed. #2 icd : "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    move v0, v4

    .line 457
    goto :goto_0

    .line 460
    .end local v1    # "result":Ljava/lang/String;
    :cond_2
    const-string v4, "#2 icd doesn\'t exist."

    invoke-static {v4}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    move v0, v3

    .line 461
    goto :goto_0

    .line 464
    .restart local v1    # "result":Ljava/lang/String;
    :cond_3
    new-instance v2, Ljava/io/File;

    .end local v2    # "target":Ljava/io/File;
    const-string v5, "/dev/icdr"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 465
    .restart local v2    # "target":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 466
    invoke-static {v2}, Lcom/sec/android/service/sm/util/FileIO;->getLineInFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 467
    const-string v3, "1"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 468
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Failed. #3 icd : "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    move v0, v4

    .line 469
    goto :goto_0

    .line 472
    :cond_4
    const-string v4, "#3 icd doesn\'t exist."

    invoke-static {v4}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    move v0, v3

    .line 473
    goto :goto_0
.end method

.method private static enforceSB(Z)I
    .locals 5
    .param p0, "enabled"    # Z

    .prologue
    const/16 v2, -0x18

    .line 480
    const/4 v0, 0x0

    .line 481
    .local v0, "res":I
    const/4 v1, -0x1

    .line 483
    .local v1, "secureBootFlag":I
    if-eqz p0, :cond_1

    .line 484
    sget-object v3, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityNativeJob:Lcom/sec/android/service/sm/job/SecurityNativeJob;

    invoke-virtual {v3}, Lcom/sec/android/service/sm/job/SecurityNativeJob;->setSBFlagOn()I

    move-result v0

    .line 485
    if-eqz v0, :cond_0

    .line 486
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. setSBFlagOn() res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 509
    :goto_0
    return v2

    .line 489
    :cond_0
    sget-object v3, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityNativeJob:Lcom/sec/android/service/sm/job/SecurityNativeJob;

    invoke-virtual {v3}, Lcom/sec/android/service/sm/job/SecurityNativeJob;->getSBFlag()I

    move-result v1

    .line 490
    const/4 v3, 0x1

    if-eq v1, v3, :cond_3

    .line 491
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. SBFlag has yet to set. current flag = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 492
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 491
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    goto :goto_0

    .line 496
    :cond_1
    sget-object v3, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityNativeJob:Lcom/sec/android/service/sm/job/SecurityNativeJob;

    invoke-virtual {v3}, Lcom/sec/android/service/sm/job/SecurityNativeJob;->setSBFlagOff()I

    move-result v0

    .line 497
    if-eqz v0, :cond_2

    .line 498
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. setSBFlagOff() res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    goto :goto_0

    .line 501
    :cond_2
    sget-object v3, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityNativeJob:Lcom/sec/android/service/sm/job/SecurityNativeJob;

    invoke-virtual {v3}, Lcom/sec/android/service/sm/job/SecurityNativeJob;->getSBFlag()I

    move-result v1

    .line 502
    if-eqz v1, :cond_3

    .line 503
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. SBFlag has yet to set. current flag = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 504
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 503
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    move v2, v0

    .line 509
    goto :goto_0
.end method

.method private static performCCModePreProcess()I
    .locals 3

    .prologue
    .line 417
    const/4 v0, 0x0

    .line 420
    .local v0, "res":I
    invoke-static {}, Lcom/sec/android/service/sm/job/SecurityJob;->checkICD()I

    move-result v0

    .line 421
    if-eqz v0, :cond_0

    .line 422
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed. checkICD = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 423
    const/16 v0, -0x15

    .line 441
    :goto_0
    return v0

    .line 427
    :cond_0
    invoke-static {}, Lcom/sec/android/service/sm/util/FipsStatus;->getFipsStatus()I

    move-result v0

    .line 428
    if-eqz v0, :cond_1

    .line 429
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed. FipsStatus = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 430
    const/16 v0, -0x23

    goto :goto_0

    .line 434
    :cond_1
    sget-object v1, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityNativeJob:Lcom/sec/android/service/sm/job/SecurityNativeJob;

    invoke-virtual {v1}, Lcom/sec/android/service/sm/job/SecurityNativeJob;->SKMM_SelfTest()I

    move-result v0

    .line 435
    if-eqz v0, :cond_2

    .line 436
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed. SKMM and FIPSOpenssl self test = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 437
    const/16 v0, -0x14

    goto :goto_0

    .line 440
    :cond_2
    const-string v1, "Pre Process OK."

    invoke-static {v1}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setCCMode(I)I
    .locals 5
    .param p0, "status"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 553
    const/4 v0, 0x0

    .line 555
    .local v0, "res":I
    sparse-switch p0, :sswitch_data_0

    .line 587
    invoke-static {v3, v3}, Lcom/sec/android/service/sm/job/SecurityJob;->setFlags(ZI)I

    move-result v0

    if-eqz v0, :cond_5

    move v1, v0

    .line 594
    .end local v0    # "res":I
    .local v1, "res":I
    :goto_0
    return v1

    .line 557
    .end local v1    # "res":I
    .restart local v0    # "res":I
    :sswitch_0
    invoke-static {v4, v4}, Lcom/sec/android/service/sm/job/SecurityJob;->setFlags(ZI)I

    move-result v0

    if-eqz v0, :cond_0

    move v1, v0

    .line 558
    .end local v0    # "res":I
    .restart local v1    # "res":I
    goto :goto_0

    .line 559
    .end local v1    # "res":I
    .restart local v0    # "res":I
    :cond_0
    const-string v2, "security.mdpp"

    const-string v3, "Enabled"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "security.mdpp : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "security.mdpp"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    move v1, v0

    .line 594
    .end local v0    # "res":I
    .restart local v1    # "res":I
    goto :goto_0

    .line 563
    .end local v1    # "res":I
    .restart local v0    # "res":I
    :sswitch_1
    const/4 v2, 0x2

    invoke-static {v4, v2}, Lcom/sec/android/service/sm/job/SecurityJob;->setFlags(ZI)I

    move-result v0

    if-eqz v0, :cond_1

    move v1, v0

    .line 564
    .end local v0    # "res":I
    .restart local v1    # "res":I
    goto :goto_0

    .line 565
    .end local v1    # "res":I
    .restart local v0    # "res":I
    :cond_1
    const-string v2, "security.mdpp"

    const-string v3, "Enforcing"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 569
    :sswitch_2
    const/4 v2, 0x4

    invoke-static {v3, v2}, Lcom/sec/android/service/sm/job/SecurityJob;->setFlags(ZI)I

    move-result v0

    if-eqz v0, :cond_2

    move v1, v0

    .line 570
    .end local v0    # "res":I
    .restart local v1    # "res":I
    goto :goto_0

    .line 571
    .end local v1    # "res":I
    .restart local v0    # "res":I
    :cond_2
    const-string v2, "security.mdpp"

    const-string v3, "Ready"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 575
    :sswitch_3
    const/16 v2, 0x8

    invoke-static {v4, v2}, Lcom/sec/android/service/sm/job/SecurityJob;->setFlags(ZI)I

    move-result v0

    if-eqz v0, :cond_3

    move v1, v0

    .line 576
    .end local v0    # "res":I
    .restart local v1    # "res":I
    goto :goto_0

    .line 577
    .end local v1    # "res":I
    .restart local v0    # "res":I
    :cond_3
    const-string v2, "security.mdpp"

    const-string v3, "Disabled"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 581
    :sswitch_4
    const/16 v2, 0x10

    invoke-static {v3, v2}, Lcom/sec/android/service/sm/job/SecurityJob;->setFlags(ZI)I

    move-result v0

    if-eqz v0, :cond_4

    move v1, v0

    .line 582
    .end local v0    # "res":I
    .restart local v1    # "res":I
    goto :goto_0

    .line 583
    .end local v1    # "res":I
    .restart local v0    # "res":I
    :cond_4
    const-string v2, "security.mdpp"

    const-string v3, "None"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 589
    :cond_5
    const-string v2, "security.mdpp"

    const-string v3, "None"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setCCMode default... status = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    goto :goto_1

    .line 555
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
    .end sparse-switch
.end method

.method public static setCCModeFlag(I)I
    .locals 6
    .param p0, "status"    # I

    .prologue
    const/16 v3, -0x1b

    .line 513
    const/4 v2, 0x0

    .line 514
    .local v2, "res":I
    move v0, p0

    .line 516
    .local v0, "_status":I
    sget-object v4, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityNativeJob:Lcom/sec/android/service/sm/job/SecurityNativeJob;

    invoke-virtual {v4, v0}, Lcom/sec/android/service/sm/job/SecurityNativeJob;->setCCModeFlag(I)I

    move-result v2

    .line 517
    if-eqz v2, :cond_1

    .line 518
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed. setCCModeFlag() res = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    move v2, v3

    .line 528
    .end local v2    # "res":I
    :cond_0
    :goto_0
    return v2

    .line 521
    .restart local v2    # "res":I
    :cond_1
    sget-object v4, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityNativeJob:Lcom/sec/android/service/sm/job/SecurityNativeJob;

    invoke-virtual {v4}, Lcom/sec/android/service/sm/job/SecurityNativeJob;->getCCModeFlag()I

    move-result v1

    .line 522
    .local v1, "ccFlag":I
    if-eq v1, v0, :cond_0

    .line 523
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed. CCMode Flag has yet to set. current flag = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 524
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 523
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    move v2, v3

    .line 525
    goto :goto_0
.end method

.method private static setFlags(ZI)I
    .locals 4
    .param p0, "SBenabled"    # Z
    .param p1, "CCModeStatus"    # I

    .prologue
    .line 532
    const/4 v0, 0x0

    .line 535
    .local v0, "res":I
    invoke-static {p0}, Lcom/sec/android/service/sm/job/SecurityJob;->enforceSB(Z)I

    move-result v0

    .line 536
    if-eqz v0, :cond_0

    .line 537
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed. enforceSB : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 538
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 537
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    move v1, v0

    .line 549
    .end local v0    # "res":I
    .local v1, "res":I
    :goto_0
    return v1

    .line 542
    .end local v1    # "res":I
    .restart local v0    # "res":I
    :cond_0
    invoke-static {p1}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCModeFlag(I)I

    move-result v0

    .line 543
    if-eqz v0, :cond_1

    .line 544
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed. setCCModeFlag : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 545
    const-string v3, ", result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 544
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    move v1, v0

    .line 546
    .end local v0    # "res":I
    .restart local v1    # "res":I
    goto :goto_0

    .end local v1    # "res":I
    .restart local v0    # "res":I
    :cond_1
    move v1, v0

    .line 549
    .end local v0    # "res":I
    .restart local v1    # "res":I
    goto :goto_0
.end method


# virtual methods
.method public enableMDFPPMode(Z)I
    .locals 10
    .param p1, "enable"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/16 v6, 0x8

    const/16 v5, 0x10

    .line 148
    const/4 v1, 0x0

    .line 150
    .local v1, "res":I
    const-string v3, "security.mdpp.result"

    const-string v4, "None"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v3, "Enabled"

    const-string v4, "ro.security.mdpp.ux"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 154
    const-string v3, "This model does not support CC mode."

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 155
    const/16 v1, -0x10

    .line 156
    const-string v3, "security.mdpp.result"

    .line 157
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 156
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    .line 268
    .end local v1    # "res":I
    .local v2, "res":I
    :goto_0
    return v2

    .line 162
    .end local v2    # "res":I
    .restart local v1    # "res":I
    :cond_0
    invoke-static {}, Lcom/sec/android/service/sm/job/SecurityJob;->checkCCModeOnDevice()I

    move-result v0

    .line 163
    .local v0, "ccModeOnDevice":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "the current mode : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 165
    if-eqz p1, :cond_c

    .line 166
    if-ne v0, v9, :cond_2

    .line 167
    const-string v3, "Failed. CCMode is already enabled."

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 168
    const/16 v1, -0xe

    .line 169
    const-string v3, "security.mdpp.result"

    .line 170
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 169
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    move v2, v1

    .line 268
    .end local v1    # "res":I
    .restart local v2    # "res":I
    goto :goto_0

    .line 172
    .end local v2    # "res":I
    .restart local v1    # "res":I
    :cond_2
    if-eq v0, v8, :cond_3

    .line 173
    if-ne v0, v7, :cond_6

    .line 174
    :cond_3
    invoke-static {v7}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 175
    if-eqz v1, :cond_4

    .line 176
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. setCCMode. res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 177
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 176
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    move v2, v1

    .line 178
    .end local v1    # "res":I
    .restart local v2    # "res":I
    goto :goto_0

    .line 180
    .end local v2    # "res":I
    .restart local v1    # "res":I
    :cond_4
    invoke-static {}, Lcom/sec/android/service/sm/job/SecurityJob;->checkDevicePolicy()I

    move-result v1

    .line 181
    const-string v3, "security.mdpp.result"

    .line 182
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 181
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    if-eqz v1, :cond_5

    .line 184
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Prerequisite policies have yet to set. res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 185
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 184
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    goto :goto_1

    .line 187
    :cond_5
    invoke-static {}, Lcom/sec/android/service/sm/job/SecurityJob;->performCCModePreProcess()I

    move-result v1

    .line 188
    const-string v3, "security.mdpp.result"

    .line 189
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 188
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    if-eqz v1, :cond_1

    .line 191
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. CCMode Pre-Process. res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 192
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 191
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 193
    invoke-static {v6}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 194
    if-eqz v1, :cond_1

    .line 195
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. setCCMode. res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 196
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 195
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    goto/16 :goto_1

    .line 201
    :cond_6
    if-ne v0, v5, :cond_8

    .line 202
    invoke-static {v5}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 203
    if-eqz v1, :cond_7

    .line 204
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. setCCMode. res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 206
    :cond_7
    const/16 v1, -0xc

    .line 207
    const-string v3, "security.mdpp.result"

    .line 208
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 207
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 209
    :cond_8
    if-ne v0, v6, :cond_a

    .line 210
    invoke-static {v6}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 211
    if-eqz v1, :cond_9

    .line 212
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. setCCMode. res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 214
    :cond_9
    const/16 v1, -0xb

    .line 215
    const-string v3, "security.mdpp.result"

    .line 216
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 215
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 218
    :cond_a
    invoke-static {v5}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 219
    if-eqz v1, :cond_b

    .line 220
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. setCCMode. res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 222
    :cond_b
    const/16 v1, -0xd

    .line 223
    const-string v3, "security.mdpp.result"

    .line 224
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 223
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 228
    :cond_c
    if-eq v0, v9, :cond_d

    .line 229
    if-ne v0, v7, :cond_f

    .line 230
    :cond_d
    invoke-static {v8}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 231
    if-eqz v1, :cond_e

    .line 232
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. setCCMode. res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 234
    :cond_e
    const-string v3, "security.mdpp.result"

    .line 235
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 234
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 236
    :cond_f
    if-ne v0, v8, :cond_10

    .line 237
    const-string v3, "Failed. CCMode is already ready."

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 238
    const/16 v1, -0xf

    .line 239
    const-string v3, "security.mdpp.result"

    .line 240
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 239
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 241
    :cond_10
    if-ne v0, v6, :cond_12

    .line 242
    invoke-static {v6}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 243
    if-eqz v1, :cond_11

    .line 244
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. setCCMode. res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 246
    :cond_11
    const/16 v1, -0xb

    .line 247
    const-string v3, "security.mdpp.result"

    .line 248
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 247
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 249
    :cond_12
    if-ne v0, v5, :cond_14

    .line 250
    invoke-static {v5}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 251
    if-eqz v1, :cond_13

    .line 252
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. setCCMode. res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 254
    :cond_13
    const/16 v1, -0xc

    .line 255
    const-string v3, "security.mdpp.result"

    .line 256
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 255
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 258
    :cond_14
    invoke-static {v5}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 259
    if-eqz v1, :cond_15

    .line 260
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed. setCCMode. res = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 262
    :cond_15
    const/16 v1, -0xd

    .line 263
    const-string v3, "security.mdpp.result"

    .line 264
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 263
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public getCCModeStatus()Ljava/lang/String;
    .locals 2

    .prologue
    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "v"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "ro.security.mdpp.ver"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Release "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 286
    const-string v1, "ro.security.mdpp.release"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 287
    const-string v1, "security.mdpp"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 285
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStringForCC(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v2, 0x7f040008

    .line 602
    const-string v1, "security.mdpp.result"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 603
    .local v0, "res":I
    packed-switch v0, :pswitch_data_0

    .line 646
    :pswitch_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    .line 606
    :pswitch_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 609
    :pswitch_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 612
    :pswitch_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 614
    :pswitch_4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f04000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 616
    :pswitch_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f04000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 625
    :pswitch_6
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f04000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 627
    :pswitch_7
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f04000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 630
    :pswitch_8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 634
    :pswitch_9
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f04000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 636
    :pswitch_a
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f04000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 638
    :pswitch_b
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 640
    :pswitch_c
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 642
    :pswitch_d
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 644
    :pswitch_e
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 603
    :pswitch_data_0
    .packed-switch -0x27
        :pswitch_8
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_6
        :pswitch_9
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_9
        :pswitch_0
        :pswitch_6
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public initCCMode()I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v5, 0x10

    const/16 v4, 0x8

    .line 83
    const/4 v1, 0x0

    .line 84
    .local v1, "res":I
    invoke-static {}, Lcom/sec/android/service/sm/job/SecurityJob;->checkCCModeOnDevice()I

    move-result v0

    .line 85
    .local v0, "ccModeOnDevice":I
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "the current mode : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 87
    if-eq v0, v6, :cond_0

    if-ne v0, v7, :cond_4

    .line 88
    :cond_0
    invoke-static {}, Lcom/sec/android/service/sm/job/SecurityJob;->checkDevicePolicy()I

    move-result v1

    .line 89
    if-eqz v1, :cond_2

    .line 90
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Prerequisite policies have yet to set. res = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 91
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 90
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 93
    invoke-static {v7}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 94
    if-eqz v1, :cond_1

    .line 95
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed. setCCMode. res = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 144
    :cond_1
    :goto_0
    return v1

    .line 98
    :cond_2
    invoke-static {}, Lcom/sec/android/service/sm/job/SecurityJob;->performCCModePreProcess()I

    move-result v1

    .line 99
    if-eqz v1, :cond_3

    .line 100
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed. CCMode Pre-Process. res = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 101
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 100
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 102
    invoke-static {v4}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 103
    if-eqz v1, :cond_1

    .line 104
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed. setCCMode. res = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 105
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 104
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    goto :goto_0

    .line 108
    :cond_3
    invoke-static {v6}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 109
    if-eqz v1, :cond_1

    .line 110
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed. setCCMode. res = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 111
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 110
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    goto :goto_0

    .line 119
    :cond_4
    if-ne v0, v8, :cond_5

    .line 120
    invoke-static {v8}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 121
    if-eqz v1, :cond_1

    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed. setCCMode. res = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    goto :goto_0

    .line 124
    :cond_5
    if-ne v0, v4, :cond_7

    .line 125
    invoke-static {v4}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 126
    if-eqz v1, :cond_6

    .line 127
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed. setCCMode. res = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 129
    :cond_6
    const/16 v1, -0xb

    .line 130
    goto/16 :goto_0

    :cond_7
    if-ne v0, v5, :cond_9

    .line 131
    invoke-static {v5}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 132
    if-eqz v1, :cond_8

    .line 133
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed. setCCMode. res = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 135
    :cond_8
    const/16 v1, -0xc

    .line 136
    goto/16 :goto_0

    .line 137
    :cond_9
    invoke-static {v5}, Lcom/sec/android/service/sm/job/SecurityJob;->setCCMode(I)I

    move-result v1

    .line 138
    if-eqz v1, :cond_a

    .line 139
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed. setCCMode. res = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/sm/util/Log;->e(Ljava/lang/String;)I

    .line 141
    :cond_a
    const/16 v1, -0xd

    goto/16 :goto_0
.end method

.method public isCCMode()Z
    .locals 2

    .prologue
    .line 272
    const-string v1, "security.mdpp"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 274
    .local v0, "propertyMDPP":Ljava/lang/String;
    const-string v1, "Enabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 275
    const/4 v1, 0x1

    .line 277
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isFIPSMode()Z
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x0

    return v0
.end method

.method public verifyVPN()I
    .locals 1

    .prologue
    .line 598
    sget-object v0, Lcom/sec/android/service/sm/job/SecurityJob;->mSecurityNativeJob:Lcom/sec/android/service/sm/job/SecurityNativeJob;

    invoke-virtual {v0}, Lcom/sec/android/service/sm/job/SecurityNativeJob;->verifyVPN()I

    move-result v0

    return v0
.end method
