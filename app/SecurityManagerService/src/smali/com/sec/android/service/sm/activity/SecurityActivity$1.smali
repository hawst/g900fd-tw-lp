.class Lcom/sec/android/service/sm/activity/SecurityActivity$1;
.super Ljava/lang/Object;
.source "SecurityActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/sm/activity/SecurityActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/sm/activity/SecurityActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/service/sm/activity/SecurityActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/service/sm/activity/SecurityActivity$1;->this$0:Lcom/sec/android/service/sm/activity/SecurityActivity;

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 48
    iget-object v1, p0, Lcom/sec/android/service/sm/activity/SecurityActivity$1;->this$0:Lcom/sec/android/service/sm/activity/SecurityActivity;

    invoke-virtual {v1}, Lcom/sec/android/service/sm/activity/SecurityActivity;->finish()V

    .line 51
    :goto_0
    return-void

    .line 38
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.REBOOT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "startIntent":Landroid/content/Intent;
    const-string v1, "android.intent.action.REBOOT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 40
    const-string v1, "android.intent.extra.KEY_EVENT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 41
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 42
    iget-object v1, p0, Lcom/sec/android/service/sm/activity/SecurityActivity$1;->this$0:Lcom/sec/android/service/sm/activity/SecurityActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/service/sm/activity/SecurityActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 45
    .end local v0    # "startIntent":Landroid/content/Intent;
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/service/sm/activity/SecurityActivity$1;->this$0:Lcom/sec/android/service/sm/activity/SecurityActivity;

    invoke-virtual {v1}, Lcom/sec/android/service/sm/activity/SecurityActivity;->finish()V

    goto :goto_0

    .line 36
    :pswitch_data_0
    .packed-switch 0x7f050002
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
