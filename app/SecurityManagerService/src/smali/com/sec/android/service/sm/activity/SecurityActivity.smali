.class public Lcom/sec/android/service/sm/activity/SecurityActivity;
.super Landroid/app/Activity;
.source "SecurityActivity.java"


# instance fields
.field private btn_cancel:Landroid/widget/Button;

.field private btn_ok:Landroid/widget/Button;

.field onclick:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 32
    new-instance v0, Lcom/sec/android/service/sm/activity/SecurityActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/sm/activity/SecurityActivity$1;-><init>(Lcom/sec/android/service/sm/activity/SecurityActivity;)V

    iput-object v0, p0, Lcom/sec/android/service/sm/activity/SecurityActivity;->onclick:Landroid/view/View$OnClickListener;

    .line 12
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/service/sm/activity/SecurityActivity;->finish()V

    .line 30
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 17
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/android/service/sm/activity/SecurityActivity;->setContentView(I)V

    .line 19
    const v0, 0x7f050003

    invoke-virtual {p0, v0}, Lcom/sec/android/service/sm/activity/SecurityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/service/sm/activity/SecurityActivity;->btn_ok:Landroid/widget/Button;

    .line 20
    const v0, 0x7f050002

    invoke-virtual {p0, v0}, Lcom/sec/android/service/sm/activity/SecurityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/service/sm/activity/SecurityActivity;->btn_cancel:Landroid/widget/Button;

    .line 22
    iget-object v0, p0, Lcom/sec/android/service/sm/activity/SecurityActivity;->btn_ok:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/service/sm/activity/SecurityActivity;->onclick:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    iget-object v0, p0, Lcom/sec/android/service/sm/activity/SecurityActivity;->btn_cancel:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/service/sm/activity/SecurityActivity;->onclick:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 25
    return-void
.end method
