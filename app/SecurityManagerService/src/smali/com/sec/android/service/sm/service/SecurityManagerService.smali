.class public Lcom/sec/android/service/sm/service/SecurityManagerService;
.super Landroid/app/Service;
.source "SecurityManagerService.java"


# static fields
.field private static mDEM:Landroid/dirEncryption/DirEncryptionManager;

.field private static mDPM:Landroid/app/admin/DevicePolicyManager;

.field private static mKM:Landroid/app/KeyguardManager;

.field private static mLPU:Lcom/android/internal/widget/LockPatternUtils;

.field private static mSecurityJob:Lcom/sec/android/service/sm/job/SecurityJob;


# instance fields
.field private final mBinder:Lcom/sec/android/service/sm/aidl/ISecurityManager$Stub;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 83
    new-instance v0, Lcom/sec/android/service/sm/service/SecurityManagerService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/sm/service/SecurityManagerService$1;-><init>(Lcom/sec/android/service/sm/service/SecurityManagerService;)V

    iput-object v0, p0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mBinder:Lcom/sec/android/service/sm/aidl/ISecurityManager$Stub;

    .line 29
    return-void
.end method

.method static synthetic access$0()Lcom/sec/android/service/sm/job/SecurityJob;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mSecurityJob:Lcom/sec/android/service/sm/job/SecurityJob;

    return-object v0
.end method

.method private printVersion()V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/service/sm/util/Log;->setLogState(Z)V

    .line 153
    const v0, 0x7f040002

    invoke-virtual {p0, v0}, Lcom/sec/android/service/sm/service/SecurityManagerService;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/sm/util/Log;->i(Ljava/lang/String;)I

    .line 154
    return-void
.end method


# virtual methods
.method public getMaximumFailedPasswordsForWipe()I
    .locals 2

    .prologue
    .line 118
    sget-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mDPM:Landroid/app/admin/DevicePolicyManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;)I

    move-result v0

    return v0
.end method

.method public getPasswordHistoryLength()I
    .locals 2

    .prologue
    .line 130
    sget-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mDPM:Landroid/app/admin/DevicePolicyManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordHistoryLength(Landroid/content/ComponentName;)I

    move-result v0

    return v0
.end method

.method public getPasswordRecoverable()Z
    .locals 2

    .prologue
    .line 126
    sget-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mDPM:Landroid/app/admin/DevicePolicyManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordRecoverable(Landroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method

.method public getSamsungEncryptionStatusForCC()I
    .locals 2

    .prologue
    .line 122
    sget-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mDPM:Landroid/app/admin/DevicePolicyManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getSamsungEncryptionStatusForCC(Landroid/content/ComponentName;)I

    move-result v0

    return v0
.end method

.method public isExternalSDRemovable()Z
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mDEM:Landroid/dirEncryption/DirEncryptionManager;

    if-nez v0, :cond_0

    .line 148
    const/4 v0, 0x0

    .line 149
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mDEM:Landroid/dirEncryption/DirEncryptionManager;

    invoke-virtual {v0}, Landroid/dirEncryption/DirEncryptionManager;->isExternalSDRemovable()Z

    move-result v0

    goto :goto_0
.end method

.method public isKeyguardSecure()Z
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mKM:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    return v0
.end method

.method public isLockFingerprintEnabled()Z
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mLPU:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isLockFingerprintEnabled()Z

    move-result v0

    return v0
.end method

.method public isRevocationCheckEnabled()Z
    .locals 2

    .prologue
    .line 138
    invoke-static {}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getInstance()Landroid/sec/enterprise/EnterpriseDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getCertificatePolicy()Landroid/sec/enterprise/certificate/CertificatePolicy;

    move-result-object v0

    .line 139
    .local v0, "certPolicy":Landroid/sec/enterprise/certificate/CertificatePolicy;
    invoke-virtual {v0}, Landroid/sec/enterprise/certificate/CertificatePolicy;->isRevocationCheckEnabled()Z

    move-result v1

    return v1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 58
    const-string v0, "onBind"

    invoke-static {v0}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 60
    invoke-direct {p0}, Lcom/sec/android/service/sm/service/SecurityManagerService;->printVersion()V

    .line 62
    iget-object v0, p0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mBinder:Lcom/sec/android/service/sm/aidl/ISecurityManager$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 33
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 34
    const-string v0, "onCreate"

    invoke-static {v0}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 36
    new-instance v0, Lcom/sec/android/service/sm/job/SecurityJob;

    invoke-direct {v0}, Lcom/sec/android/service/sm/job/SecurityJob;-><init>()V

    sput-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mSecurityJob:Lcom/sec/android/service/sm/job/SecurityJob;

    .line 37
    const-string v0, "device_policy"

    invoke-virtual {p0, v0}, Lcom/sec/android/service/sm/service/SecurityManagerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    sput-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mDPM:Landroid/app/admin/DevicePolicyManager;

    .line 38
    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/sec/android/service/sm/service/SecurityManagerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    sput-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mKM:Landroid/app/KeyguardManager;

    .line 39
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    invoke-direct {v0, p0}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mLPU:Lcom/android/internal/widget/LockPatternUtils;

    .line 40
    new-instance v0, Landroid/dirEncryption/DirEncryptionManager;

    invoke-virtual {p0}, Lcom/sec/android/service/sm/service/SecurityManagerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/dirEncryption/DirEncryptionManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/service/sm/service/SecurityManagerService;->mDEM:Landroid/dirEncryption/DirEncryptionManager;

    .line 41
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 79
    const-string v0, "onDestory"

    invoke-static {v0}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 80
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 81
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 68
    const-string v0, "onRebind"

    invoke-static {v0}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 69
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 45
    const-string v1, "onStartCommand"

    invoke-static {v1}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 47
    invoke-direct {p0}, Lcom/sec/android/service/sm/service/SecurityManagerService;->printVersion()V

    .line 49
    const/4 v0, 0x0

    .line 50
    .local v0, "res":I
    sget-object v1, Lcom/sec/android/service/sm/service/SecurityManagerService;->mSecurityJob:Lcom/sec/android/service/sm/job/SecurityJob;

    invoke-virtual {v1}, Lcom/sec/android/service/sm/job/SecurityJob;->initCCMode()I

    move-result v0

    .line 51
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "init CCMode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/sm/util/Log;->i(Ljava/lang/String;)I

    .line 53
    const/4 v1, 0x1

    return v1
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 73
    const-string v0, "onUnbind"

    invoke-static {v0}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 74
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
