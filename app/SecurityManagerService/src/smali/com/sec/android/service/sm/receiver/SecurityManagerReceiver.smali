.class public Lcom/sec/android/service/sm/receiver/SecurityManagerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SecurityManagerReceiver.java"


# static fields
.field private static mSecurityJob:Lcom/sec/android/service/sm/job/SecurityJob;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 22
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    new-instance v1, Lcom/sec/android/service/sm/job/SecurityJob;

    invoke-direct {v1}, Lcom/sec/android/service/sm/job/SecurityJob;-><init>()V

    sput-object v1, Lcom/sec/android/service/sm/receiver/SecurityManagerReceiver;->mSecurityJob:Lcom/sec/android/service/sm/job/SecurityJob;

    .line 27
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 29
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30
    const-string v1, "starts SecurityManagerService with Boot Complete"

    invoke-static {v1}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 31
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/service/sm/service/SecurityManagerService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 34
    :cond_0
    const-string v1, "android.intent.action.sms.ENABLE_MDFPP_MODE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 35
    const-string v1, "Enable MDFPP Mode"

    invoke-static {v1}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 36
    sget-object v1, Lcom/sec/android/service/sm/receiver/SecurityManagerReceiver;->mSecurityJob:Lcom/sec/android/service/sm/job/SecurityJob;

    invoke-virtual {v1, v4}, Lcom/sec/android/service/sm/job/SecurityJob;->enableMDFPPMode(Z)I

    .line 37
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "security.mdpp.result"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/android/service/sm/receiver/SecurityManagerReceiver;->setNotification(Landroid/content/Context;)V

    .line 39
    const-string v1, "Enabled"

    invoke-static {v1}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 41
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "security.mdpp.result"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 42
    sget-object v1, Lcom/sec/android/service/sm/receiver/SecurityManagerReceiver;->mSecurityJob:Lcom/sec/android/service/sm/job/SecurityJob;

    invoke-virtual {v1, p1}, Lcom/sec/android/service/sm/job/SecurityJob;->getStringForCC(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 45
    :cond_2
    const-string v1, "android.intent.action.sms.DISABLE_MDFPP_MODE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 46
    const-string v1, "Disable MDFPP Mode"

    invoke-static {v1}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 47
    sget-object v1, Lcom/sec/android/service/sm/receiver/SecurityManagerReceiver;->mSecurityJob:Lcom/sec/android/service/sm/job/SecurityJob;

    invoke-virtual {v1, v3}, Lcom/sec/android/service/sm/job/SecurityJob;->enableMDFPPMode(Z)I

    .line 50
    :cond_3
    return-void
.end method

.method public setNotification(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const-string v3, "Notice for applying security policy"

    invoke-static {v3}, Lcom/sec/android/service/sm/util/Log;->d(Ljava/lang/String;)I

    .line 54
    const/4 v3, 0x0

    .line 55
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/service/sm/activity/SecurityActivity;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v5, 0x400000

    .line 54
    invoke-static {p1, v3, v4, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 56
    .local v2, "pi":Landroid/app/PendingIntent;
    new-instance v3, Landroid/app/Notification$Builder;

    invoke-direct {v3, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f040005

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f040006

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 59
    const v4, 0x108009b

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 60
    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 61
    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 62
    .local v1, "noti":Landroid/app/Notification;
    const/16 v3, 0x28

    iput v3, v1, Landroid/app/Notification;->flags:I

    .line 63
    const-string v3, "notification"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 64
    .local v0, "mNotiMgr":Landroid/app/NotificationManager;
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v0, v3, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 65
    return-void
.end method
