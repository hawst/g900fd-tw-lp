.class public final Lcom/sec/android/service/sm/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/sm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f040000

.field public static final app_version:I = 0x7f040002

.field public static final cancel:I = 0x7f040004

.field public static final cannot_enable_cc_mode:I = 0x7f040009

.field public static final cc_mode_disabled:I = 0x7f04000a

.field public static final cc_mode_enabled:I = 0x7f040007

.field public static final cc_mode_not_supported:I = 0x7f04000b

.field public static final change_screen_lock_type_to_password:I = 0x7f040013

.field public static final check_password_policy:I = 0x7f04000d

.field public static final content:I = 0x7f040006

.field public static final disable_password_history_length_policy:I = 0x7f040012

.field public static final disable_recovery_password:I = 0x7f040011

.field public static final enable_encrypt_device:I = 0x7f040010

.field public static final enable_encrypt_device_and_sd_card:I = 0x7f04000e

.field public static final enable_encrypt_sd_card:I = 0x7f04000f

.field public static final enable_revocation_check_policy:I = 0x7f040014

.field public static final error_occurred_while_enabling_cc_mode:I = 0x7f04000c

.field public static final failed_to_enable_cc_mode:I = 0x7f040008

.field public static final hello_world:I = 0x7f040001

.field public static final ok:I = 0x7f040003

.field public static final title:I = 0x7f040005


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
