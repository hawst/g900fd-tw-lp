.class public final Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;
.super Ljava/lang/Object;


# instance fields
.field _hashCode:I

.field _type:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    invoke-virtual {p0, p1}, Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;->reset(Ljava/lang/Class;)V

    .line 221
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 231
    if-ne p1, p0, :cond_1

    .line 234
    :cond_0
    :goto_0
    return v0

    .line 232
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 233
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    .line 234
    :cond_3
    check-cast p1, Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;

    iget-object v2, p1, Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;->_type:Ljava/lang/Class;

    iget-object v3, p0, Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;->_type:Ljava/lang/Class;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 237
    iget v0, p0, Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;->_hashCode:I

    return v0
.end method

.method public reset(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 224
    iput-object p1, p0, Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;->_type:Ljava/lang/Class;

    .line 225
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;->_hashCode:I

    .line 226
    return-void
.end method
