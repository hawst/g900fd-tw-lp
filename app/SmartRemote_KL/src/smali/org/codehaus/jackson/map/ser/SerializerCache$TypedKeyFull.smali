.class public final Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;
.super Ljava/lang/Object;


# instance fields
.field _type:Lorg/codehaus/jackson/type/JavaType;


# direct methods
.method public constructor <init>(Lorg/codehaus/jackson/type/JavaType;)V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    iput-object p1, p0, Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;->_type:Lorg/codehaus/jackson/type/JavaType;

    .line 195
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 204
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 207
    :cond_0
    :goto_0
    return v0

    .line 205
    :cond_1
    if-eqz p1, :cond_0

    .line 206
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 207
    check-cast p1, Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;

    iget-object v0, p1, Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;->_type:Lorg/codehaus/jackson/type/JavaType;

    iget-object v1, p0, Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;->_type:Lorg/codehaus/jackson/type/JavaType;

    invoke-virtual {v0, v1}, Lorg/codehaus/jackson/type/JavaType;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;->_type:Lorg/codehaus/jackson/type/JavaType;

    invoke-virtual {v0}, Lorg/codehaus/jackson/type/JavaType;->hashCode()I

    move-result v0

    return v0
.end method

.method public reset(Lorg/codehaus/jackson/type/JavaType;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;->_type:Lorg/codehaus/jackson/type/JavaType;

    .line 199
    return-void
.end method
