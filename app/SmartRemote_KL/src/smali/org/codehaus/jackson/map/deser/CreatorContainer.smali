.class public Lorg/codehaus/jackson/map/deser/CreatorContainer;
.super Ljava/lang/Object;


# instance fields
.field final _beanClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field final _canFixAccess:Z

.field _delegatingConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

.field _delegatingFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

.field _intConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

.field _intFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

.field _longConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

.field _longFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

.field _propertyBasedConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

.field _propertyBasedConstructorProperties:[Lorg/codehaus/jackson/map/deser/SettableBeanProperty;

.field _propertyBasedFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

.field _propertyBasedFactoryProperties:[Lorg/codehaus/jackson/map/deser/SettableBeanProperty;

.field _strConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

.field _strFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;


# direct methods
.method public constructor <init>(Ljava/lang/Class;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedFactoryProperties:[Lorg/codehaus/jackson/map/deser/SettableBeanProperty;

    .line 24
    iput-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedConstructorProperties:[Lorg/codehaus/jackson/map/deser/SettableBeanProperty;

    .line 27
    iput-boolean p2, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_canFixAccess:Z

    .line 28
    iput-object p1, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_beanClass:Ljava/lang/Class;

    .line 29
    return-void
.end method


# virtual methods
.method public addDelegatingConstructor(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_delegatingConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    const-string/jumbo v1, "long"

    invoke-virtual {p0, p1, v0, v1}, Lorg/codehaus/jackson/map/deser/CreatorContainer;->verifyNonDup(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Ljava/lang/String;)Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_delegatingConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    .line 49
    return-void
.end method

.method public addDelegatingFactory(Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_delegatingFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    const-string/jumbo v1, "long"

    invoke-virtual {p0, p1, v0, v1}, Lorg/codehaus/jackson/map/deser/CreatorContainer;->verifyNonDup(Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Ljava/lang/String;)Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_delegatingFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    .line 69
    return-void
.end method

.method public addIntConstructor(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_intConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    const-string/jumbo v1, "int"

    invoke-virtual {p0, p1, v0, v1}, Lorg/codehaus/jackson/map/deser/CreatorContainer;->verifyNonDup(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Ljava/lang/String;)Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_intConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    .line 42
    return-void
.end method

.method public addIntFactory(Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_intFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    const-string/jumbo v1, "int"

    invoke-virtual {p0, p1, v0, v1}, Lorg/codehaus/jackson/map/deser/CreatorContainer;->verifyNonDup(Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Ljava/lang/String;)Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_intFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    .line 62
    return-void
.end method

.method public addLongConstructor(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_longConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    const-string/jumbo v1, "long"

    invoke-virtual {p0, p1, v0, v1}, Lorg/codehaus/jackson/map/deser/CreatorContainer;->verifyNonDup(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Ljava/lang/String;)Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_longConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    .line 45
    return-void
.end method

.method public addLongFactory(Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_longFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    const-string/jumbo v1, "long"

    invoke-virtual {p0, p1, v0, v1}, Lorg/codehaus/jackson/map/deser/CreatorContainer;->verifyNonDup(Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Ljava/lang/String;)Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_longFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    .line 65
    return-void
.end method

.method public addPropertyConstructor(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;[Lorg/codehaus/jackson/map/deser/SettableBeanProperty;)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    const-string/jumbo v1, "property-based"

    invoke-virtual {p0, p1, v0, v1}, Lorg/codehaus/jackson/map/deser/CreatorContainer;->verifyNonDup(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Ljava/lang/String;)Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    .line 54
    iput-object p2, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedConstructorProperties:[Lorg/codehaus/jackson/map/deser/SettableBeanProperty;

    .line 55
    return-void
.end method

.method public addPropertyFactory(Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;[Lorg/codehaus/jackson/map/deser/SettableBeanProperty;)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    const-string/jumbo v1, "property-based"

    invoke-virtual {p0, p1, v0, v1}, Lorg/codehaus/jackson/map/deser/CreatorContainer;->verifyNonDup(Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Ljava/lang/String;)Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    .line 74
    iput-object p2, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedFactoryProperties:[Lorg/codehaus/jackson/map/deser/SettableBeanProperty;

    .line 75
    return-void
.end method

.method public addStringConstructor(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_strConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    const-string/jumbo v1, "String"

    invoke-virtual {p0, p1, v0, v1}, Lorg/codehaus/jackson/map/deser/CreatorContainer;->verifyNonDup(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Ljava/lang/String;)Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_strConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    .line 39
    return-void
.end method

.method public addStringFactory(Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;)V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_strFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    const-string/jumbo v1, "String"

    invoke-virtual {p0, p1, v0, v1}, Lorg/codehaus/jackson/map/deser/CreatorContainer;->verifyNonDup(Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Ljava/lang/String;)Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_strFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    .line 59
    return-void
.end method

.method public delegatingCreator()Lorg/codehaus/jackson/map/deser/Creator$Delegating;
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_delegatingConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_delegatingFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    if-nez v0, :cond_0

    .line 104
    const/4 v0, 0x0

    .line 106
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/codehaus/jackson/map/deser/Creator$Delegating;

    iget-object v1, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_delegatingConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    iget-object v2, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_delegatingFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    invoke-direct {v0, v1, v2}, Lorg/codehaus/jackson/map/deser/Creator$Delegating;-><init>(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;)V

    goto :goto_0
.end method

.method public numberCreator()Lorg/codehaus/jackson/map/deser/Creator$NumberBased;
    .locals 6

    .prologue
    .line 93
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_intConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_intFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_longConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_longFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    if-nez v0, :cond_0

    .line 95
    const/4 v0, 0x0

    .line 97
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/codehaus/jackson/map/deser/Creator$NumberBased;

    iget-object v1, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_beanClass:Ljava/lang/Class;

    iget-object v2, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_intConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    iget-object v3, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_intFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    iget-object v4, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_longConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    iget-object v5, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_longFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    invoke-direct/range {v0 .. v5}, Lorg/codehaus/jackson/map/deser/Creator$NumberBased;-><init>(Ljava/lang/Class;Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;)V

    goto :goto_0
.end method

.method public propertyBasedCreator()Lorg/codehaus/jackson/map/deser/Creator$PropertyBased;
    .locals 5

    .prologue
    .line 111
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    if-nez v0, :cond_0

    .line 112
    const/4 v0, 0x0

    .line 114
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/codehaus/jackson/map/deser/Creator$PropertyBased;

    iget-object v1, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    iget-object v2, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedConstructorProperties:[Lorg/codehaus/jackson/map/deser/SettableBeanProperty;

    iget-object v3, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    iget-object v4, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_propertyBasedFactoryProperties:[Lorg/codehaus/jackson/map/deser/SettableBeanProperty;

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/codehaus/jackson/map/deser/Creator$PropertyBased;-><init>(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;[Lorg/codehaus/jackson/map/deser/SettableBeanProperty;Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;[Lorg/codehaus/jackson/map/deser/SettableBeanProperty;)V

    goto :goto_0
.end method

.method public stringCreator()Lorg/codehaus/jackson/map/deser/Creator$StringBased;
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_strConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_strFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x0

    .line 88
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/codehaus/jackson/map/deser/Creator$StringBased;

    iget-object v1, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_beanClass:Ljava/lang/Class;

    iget-object v2, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_strConstructor:Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;

    iget-object v3, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_strFactory:Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;

    invoke-direct {v0, v1, v2, v3}, Lorg/codehaus/jackson/map/deser/Creator$StringBased;-><init>(Ljava/lang/Class;Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;)V

    goto :goto_0
.end method

.method protected verifyNonDup(Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;Ljava/lang/String;)Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;
    .locals 3

    .prologue
    .line 127
    if-eqz p2, :cond_0

    .line 128
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Conflicting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " constructors: already had "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", encountered "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_0
    iget-boolean v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_canFixAccess:Z

    if-eqz v0, :cond_1

    .line 131
    invoke-virtual {p1}, Lorg/codehaus/jackson/map/introspect/AnnotatedConstructor;->getAnnotated()Ljava/lang/reflect/Constructor;

    move-result-object v0

    invoke-static {v0}, Lorg/codehaus/jackson/map/util/ClassUtil;->checkAndFixAccess(Ljava/lang/reflect/Member;)V

    .line 133
    :cond_1
    return-object p1
.end method

.method protected verifyNonDup(Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;Ljava/lang/String;)Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;
    .locals 3

    .prologue
    .line 139
    if-eqz p2, :cond_0

    .line 140
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Conflicting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " factory methods: already had "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", encountered "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    iget-boolean v0, p0, Lorg/codehaus/jackson/map/deser/CreatorContainer;->_canFixAccess:Z

    if-eqz v0, :cond_1

    .line 143
    invoke-virtual {p1}, Lorg/codehaus/jackson/map/introspect/AnnotatedMethod;->getAnnotated()Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lorg/codehaus/jackson/map/util/ClassUtil;->checkAndFixAccess(Ljava/lang/reflect/Member;)V

    .line 145
    :cond_1
    return-object p1
.end method
