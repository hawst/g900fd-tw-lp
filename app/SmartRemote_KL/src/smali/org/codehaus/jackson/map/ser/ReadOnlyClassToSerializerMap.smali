.class public final Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;
.super Ljava/lang/Object;


# instance fields
.field final _map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Lorg/codehaus/jackson/map/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field final _typedKeyFull:Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;

.field final _typedKeyRaw:Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyRaw;

.field final _untypedKeyRaw:Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;


# direct methods
.method private constructor <init>(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Lorg/codehaus/jackson/map/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyRaw;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyRaw;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_typedKeyRaw:Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyRaw;

    .line 29
    new-instance v0, Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;-><init>(Lorg/codehaus/jackson/type/JavaType;)V

    iput-object v0, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_typedKeyFull:Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;

    .line 31
    new-instance v0, Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_untypedKeyRaw:Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;

    .line 35
    iput-object p1, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_map:Ljava/util/HashMap;

    .line 36
    return-void
.end method

.method public static from(Ljava/util/HashMap;)Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Lorg/codehaus/jackson/map/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;>;)",
            "Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;"
        }
    .end annotation

    .prologue
    .line 51
    new-instance v1, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;

    invoke-virtual {p0}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;-><init>(Ljava/util/HashMap;)V

    return-object v1
.end method


# virtual methods
.method public instance()Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;

    iget-object v1, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_map:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;-><init>(Ljava/util/HashMap;)V

    return-object v0
.end method

.method public typedValueSerializer(Ljava/lang/Class;)Lorg/codehaus/jackson/map/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lorg/codehaus/jackson/map/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_typedKeyRaw:Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyRaw;

    invoke-virtual {v0, p1}, Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyRaw;->reset(Ljava/lang/Class;)V

    .line 63
    iget-object v0, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_map:Ljava/util/HashMap;

    iget-object v1, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_typedKeyRaw:Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyRaw;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/codehaus/jackson/map/JsonSerializer;

    return-object v0
.end method

.method public typedValueSerializer(Lorg/codehaus/jackson/type/JavaType;)Lorg/codehaus/jackson/map/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/codehaus/jackson/type/JavaType;",
            ")",
            "Lorg/codehaus/jackson/map/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_typedKeyFull:Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;

    invoke-virtual {v0, p1}, Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;->reset(Lorg/codehaus/jackson/type/JavaType;)V

    .line 57
    iget-object v0, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_map:Ljava/util/HashMap;

    iget-object v1, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_typedKeyFull:Lorg/codehaus/jackson/map/ser/SerializerCache$TypedKeyFull;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/codehaus/jackson/map/JsonSerializer;

    return-object v0
.end method

.method public untypedValueSerializer(Ljava/lang/Class;)Lorg/codehaus/jackson/map/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lorg/codehaus/jackson/map/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_untypedKeyRaw:Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;

    invoke-virtual {v0, p1}, Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;->reset(Ljava/lang/Class;)V

    .line 69
    iget-object v0, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_map:Ljava/util/HashMap;

    iget-object v1, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_untypedKeyRaw:Lorg/codehaus/jackson/map/ser/SerializerCache$UntypedKeyRaw;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/codehaus/jackson/map/JsonSerializer;

    return-object v0
.end method

.method public untypedValueSerializer(Lorg/codehaus/jackson/type/JavaType;)Lorg/codehaus/jackson/map/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/codehaus/jackson/type/JavaType;",
            ")",
            "Lorg/codehaus/jackson/map/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lorg/codehaus/jackson/map/ser/ReadOnlyClassToSerializerMap;->_map:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/codehaus/jackson/map/JsonSerializer;

    return-object v0
.end method
