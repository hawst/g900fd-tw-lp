.class public abstract Lorg/codehaus/jackson/impl/Utf8NumericParser;
.super Lorg/codehaus/jackson/impl/StreamBasedParserBase;


# direct methods
.method public constructor <init>(Lorg/codehaus/jackson/io/IOContext;ILjava/io/InputStream;[BIIZ)V
    .locals 0

    .prologue
    .line 31
    invoke-direct/range {p0 .. p7}, Lorg/codehaus/jackson/impl/StreamBasedParserBase;-><init>(Lorg/codehaus/jackson/io/IOContext;ILjava/io/InputStream;[BIIZ)V

    .line 32
    return-void
.end method


# virtual methods
.method protected final parseNumberText(I)Lorg/codehaus/jackson/JsonToken;
    .locals 14

    .prologue
    const/16 v13, 0x39

    const/16 v11, 0x2d

    const/16 v12, 0x30

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    iget-object v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v0}, Lorg/codehaus/jackson/util/TextBuffer;->emptyAndGetCurrentSegment()[C

    move-result-object v3

    .line 60
    if-ne p1, v11, :cond_b

    move v10, v1

    .line 63
    :goto_0
    if-eqz v10, :cond_1b

    .line 64
    aput-char v11, v3, v2

    .line 66
    iget v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    iget v4, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputEnd:I

    if-lt v0, v4, :cond_0

    .line 67
    invoke-virtual {p0}, Lorg/codehaus/jackson/impl/Utf8NumericParser;->loadMoreGuaranteed()V

    .line 69
    :cond_0
    iget-object v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputBuffer:[B

    iget v4, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    aget-byte v0, v0, v4

    and-int/lit16 p1, v0, 0xff

    move v0, v1

    :goto_1
    move v4, v2

    move v6, p1

    .line 78
    :goto_2
    if-lt v6, v12, :cond_1a

    if-le v6, v13, :cond_c

    move v7, v2

    move v9, v4

    move v5, v0

    move v4, v6

    .line 102
    :goto_3
    if-nez v9, :cond_1

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Missing integer part (next char "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_getCharDesc(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v6, ")"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/jackson/impl/Utf8NumericParser;->reportInvalidNumber(Ljava/lang/String;)V

    .line 108
    :cond_1
    const/16 v0, 0x2e

    if-ne v4, v0, :cond_19

    .line 109
    add-int/lit8 v0, v5, 0x1

    int-to-char v6, v4

    aput-char v6, v3, v5

    move v5, v4

    move-object v4, v3

    move v3, v0

    move v0, v2

    .line 113
    :goto_4
    iget v6, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    iget v8, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputEnd:I

    if-lt v6, v8, :cond_10

    invoke-virtual {p0}, Lorg/codehaus/jackson/impl/Utf8NumericParser;->loadMore()Z

    move-result v6

    if-nez v6, :cond_10

    move v6, v5

    move v5, v1

    .line 129
    :goto_5
    if-nez v0, :cond_2

    .line 130
    const-string/jumbo v7, "Decimal point not followed by a digit"

    invoke-virtual {p0, v6, v7}, Lorg/codehaus/jackson/impl/Utf8NumericParser;->reportUnexpectedNumberChar(ILjava/lang/String;)V

    :cond_2
    move v8, v0

    move v0, v3

    move-object v3, v4

    .line 135
    :goto_6
    const/16 v4, 0x65

    if-eq v6, v4, :cond_3

    const/16 v4, 0x45

    if-ne v6, v4, :cond_16

    .line 136
    :cond_3
    array-length v4, v3

    if-lt v0, v4, :cond_4

    .line 137
    iget-object v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v0}, Lorg/codehaus/jackson/util/TextBuffer;->finishCurrentSegment()[C

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 140
    :cond_4
    add-int/lit8 v4, v0, 0x1

    int-to-char v6, v6

    aput-char v6, v3, v0

    .line 142
    iget v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    iget v6, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputEnd:I

    if-lt v0, v6, :cond_5

    .line 143
    invoke-virtual {p0}, Lorg/codehaus/jackson/impl/Utf8NumericParser;->loadMoreGuaranteed()V

    .line 145
    :cond_5
    iget-object v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputBuffer:[B

    iget v6, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    aget-byte v0, v0, v6

    and-int/lit16 v6, v0, 0xff

    .line 147
    if-eq v6, v11, :cond_6

    const/16 v0, 0x2b

    if-ne v6, v0, :cond_15

    .line 148
    :cond_6
    array-length v0, v3

    if-lt v4, v0, :cond_14

    .line 149
    iget-object v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v0}, Lorg/codehaus/jackson/util/TextBuffer;->finishCurrentSegment()[C

    move-result-object v3

    move v0, v2

    .line 152
    :goto_7
    add-int/lit8 v4, v0, 0x1

    int-to-char v6, v6

    aput-char v6, v3, v0

    .line 154
    iget v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    iget v6, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputEnd:I

    if-lt v0, v6, :cond_7

    .line 155
    invoke-virtual {p0}, Lorg/codehaus/jackson/impl/Utf8NumericParser;->loadMoreGuaranteed()V

    .line 157
    :cond_7
    iget-object v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputBuffer:[B

    iget v6, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    aget-byte v0, v0, v6

    and-int/lit16 v0, v0, 0xff

    move v7, v0

    move v0, v4

    move v4, v2

    .line 161
    :goto_8
    if-gt v7, v13, :cond_13

    if-lt v7, v12, :cond_13

    .line 162
    add-int/lit8 v4, v4, 0x1

    .line 163
    array-length v6, v3

    if-lt v0, v6, :cond_8

    .line 164
    iget-object v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v0}, Lorg/codehaus/jackson/util/TextBuffer;->finishCurrentSegment()[C

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 167
    :cond_8
    add-int/lit8 v6, v0, 0x1

    int-to-char v11, v7

    aput-char v11, v3, v0

    .line 168
    iget v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    iget v11, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputEnd:I

    if-lt v0, v11, :cond_12

    invoke-virtual {p0}, Lorg/codehaus/jackson/impl/Utf8NumericParser;->loadMore()Z

    move-result v0

    if-nez v0, :cond_12

    move v2, v4

    move v0, v1

    move v1, v6

    .line 175
    :goto_9
    if-nez v2, :cond_9

    .line 176
    const-string/jumbo v3, "Exponent indicator not followed by a digit"

    invoke-virtual {p0, v7, v3}, Lorg/codehaus/jackson/impl/Utf8NumericParser;->reportUnexpectedNumberChar(ILjava/lang/String;)V

    .line 181
    :cond_9
    :goto_a
    if-nez v0, :cond_a

    .line 182
    iget v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    .line 184
    :cond_a
    iget-object v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v0, v1}, Lorg/codehaus/jackson/util/TextBuffer;->setCurrentLength(I)V

    .line 187
    invoke-virtual {p0, v10, v9, v8, v2}, Lorg/codehaus/jackson/impl/Utf8NumericParser;->reset(ZIII)Lorg/codehaus/jackson/JsonToken;

    move-result-object v0

    return-object v0

    :cond_b
    move v10, v2

    .line 60
    goto/16 :goto_0

    .line 81
    :cond_c
    add-int/lit8 v4, v4, 0x1

    .line 83
    const/4 v5, 0x2

    if-ne v4, v5, :cond_d

    .line 84
    add-int/lit8 v5, v0, -0x1

    aget-char v5, v3, v5

    if-ne v5, v12, :cond_d

    .line 85
    const-string/jumbo v5, "Leading zeroes not allowed"

    invoke-virtual {p0, v5}, Lorg/codehaus/jackson/impl/Utf8NumericParser;->reportInvalidNumber(Ljava/lang/String;)V

    .line 88
    :cond_d
    array-length v5, v3

    if-lt v0, v5, :cond_e

    .line 89
    iget-object v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v0}, Lorg/codehaus/jackson/util/TextBuffer;->finishCurrentSegment()[C

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 92
    :cond_e
    add-int/lit8 v5, v0, 0x1

    int-to-char v6, v6

    aput-char v6, v3, v0

    .line 93
    iget v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    iget v6, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputEnd:I

    if-lt v0, v6, :cond_f

    invoke-virtual {p0}, Lorg/codehaus/jackson/impl/Utf8NumericParser;->loadMore()Z

    move-result v0

    if-nez v0, :cond_f

    move v7, v1

    move v9, v4

    move v4, v2

    .line 97
    goto/16 :goto_3

    .line 99
    :cond_f
    iget-object v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputBuffer:[B

    iget v6, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    aget-byte v0, v0, v6

    and-int/lit16 v6, v0, 0xff

    move v0, v5

    goto/16 :goto_2

    .line 117
    :cond_10
    iget-object v5, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputBuffer:[B

    iget v6, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    add-int/lit8 v8, v6, 0x1

    iput v8, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    .line 118
    if-lt v5, v12, :cond_18

    if-le v5, v13, :cond_11

    move v6, v5

    move v5, v7

    .line 119
    goto/16 :goto_5

    .line 121
    :cond_11
    add-int/lit8 v0, v0, 0x1

    .line 122
    array-length v6, v4

    if-lt v3, v6, :cond_17

    .line 123
    iget-object v3, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v3}, Lorg/codehaus/jackson/util/TextBuffer;->finishCurrentSegment()[C

    move-result-object v4

    move v6, v2

    .line 126
    :goto_b
    add-int/lit8 v3, v6, 0x1

    int-to-char v8, v5

    aput-char v8, v4, v6

    goto/16 :goto_4

    .line 172
    :cond_12
    iget-object v0, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputBuffer:[B

    iget v7, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    add-int/lit8 v11, v7, 0x1

    iput v11, p0, Lorg/codehaus/jackson/impl/Utf8NumericParser;->_inputPtr:I

    aget-byte v0, v0, v7

    and-int/lit16 v0, v0, 0xff

    move v7, v0

    move v0, v6

    goto/16 :goto_8

    :cond_13
    move v2, v4

    move v1, v0

    move v0, v5

    goto/16 :goto_9

    :cond_14
    move v0, v4

    goto/16 :goto_7

    :cond_15
    move v0, v4

    move v7, v6

    move v4, v2

    goto/16 :goto_8

    :cond_16
    move v1, v0

    move v0, v5

    goto/16 :goto_a

    :cond_17
    move v6, v3

    goto :goto_b

    :cond_18
    move v6, v5

    move v5, v7

    goto/16 :goto_5

    :cond_19
    move v8, v2

    move v0, v5

    move v6, v4

    move v5, v7

    goto/16 :goto_6

    :cond_1a
    move v7, v2

    move v9, v4

    move v5, v0

    move v4, v6

    goto/16 :goto_3

    :cond_1b
    move v0, v2

    goto/16 :goto_1
.end method
