.class final Lorg/codehaus/jackson/impl/ObjectWContext;
.super Lorg/codehaus/jackson/impl/JsonWriteContext;


# instance fields
.field protected _currentName:Ljava/lang/String;

.field protected _expectValue:Z


# direct methods
.method public constructor <init>(Lorg/codehaus/jackson/impl/JsonWriteContext;)V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lorg/codehaus/jackson/impl/JsonWriteContext;-><init>(ILorg/codehaus/jackson/impl/JsonWriteContext;)V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/codehaus/jackson/impl/ObjectWContext;->_currentName:Ljava/lang/String;

    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/codehaus/jackson/impl/ObjectWContext;->_expectValue:Z

    .line 201
    return-void
.end method


# virtual methods
.method protected appendDesc(Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    const/16 v1, 0x22

    .line 230
    const/16 v0, 0x7b

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 231
    iget-object v0, p0, Lorg/codehaus/jackson/impl/ObjectWContext;->_currentName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 234
    iget-object v0, p0, Lorg/codehaus/jackson/impl/ObjectWContext;->_currentName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 239
    :goto_0
    const/16 v0, 0x5d

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 240
    return-void

    .line 237
    :cond_0
    const/16 v0, 0x3f

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public getCurrentName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lorg/codehaus/jackson/impl/ObjectWContext;->_currentName:Ljava/lang/String;

    return-object v0
.end method

.method public writeFieldName(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lorg/codehaus/jackson/impl/ObjectWContext;->_currentName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 210
    const/4 v0, 0x4

    .line 213
    :goto_0
    return v0

    .line 212
    :cond_0
    iput-object p1, p0, Lorg/codehaus/jackson/impl/ObjectWContext;->_currentName:Ljava/lang/String;

    .line 213
    iget v0, p0, Lorg/codehaus/jackson/impl/ObjectWContext;->_index:I

    if-gez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeValue()I
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lorg/codehaus/jackson/impl/ObjectWContext;->_currentName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 220
    const/4 v0, 0x5

    .line 224
    :goto_0
    return v0

    .line 222
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/codehaus/jackson/impl/ObjectWContext;->_currentName:Ljava/lang/String;

    .line 223
    iget v0, p0, Lorg/codehaus/jackson/impl/ObjectWContext;->_index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/codehaus/jackson/impl/ObjectWContext;->_index:I

    .line 224
    const/4 v0, 0x2

    goto :goto_0
.end method
