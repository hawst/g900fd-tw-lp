.class public abstract Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;
.super Lorg/codehaus/jackson/impl/ReaderBasedParserBase;


# direct methods
.method public constructor <init>(Lorg/codehaus/jackson/io/IOContext;ILjava/io/Reader;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lorg/codehaus/jackson/impl/ReaderBasedParserBase;-><init>(Lorg/codehaus/jackson/io/IOContext;ILjava/io/Reader;)V

    .line 29
    return-void
.end method

.method private final parseNumberText2(Z)Lorg/codehaus/jackson/JsonToken;
    .locals 14

    .prologue
    const/16 v10, 0x2d

    const/16 v12, 0x39

    const/16 v11, 0x30

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 178
    iget-object v0, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v0}, Lorg/codehaus/jackson/util/TextBuffer;->emptyAndGetCurrentSegment()[C

    move-result-object v3

    .line 182
    if-eqz p1, :cond_19

    .line 183
    aput-char v10, v3, v2

    move v0, v1

    :goto_0
    move-object v4, v3

    move v3, v0

    move v0, v2

    .line 193
    :goto_1
    iget v5, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    iget v6, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputEnd:I

    if-lt v5, v6, :cond_8

    invoke-virtual {p0}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->loadMore()Z

    move-result v5

    if-nez v5, :cond_8

    move v8, v1

    move v5, v2

    .line 217
    :goto_2
    if-nez v0, :cond_0

    .line 218
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Missing integer part (next char "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v5}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_getCharDesc(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->reportInvalidNumber(Ljava/lang/String;)V

    .line 223
    :cond_0
    const/16 v6, 0x2e

    if-ne v5, v6, :cond_16

    .line 224
    add-int/lit8 v6, v3, 0x1

    aput-char v5, v4, v3

    move v3, v2

    move v13, v5

    move v5, v6

    move-object v6, v4

    move v4, v13

    .line 228
    :goto_3
    iget v7, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    iget v9, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputEnd:I

    if-lt v7, v9, :cond_b

    invoke-virtual {p0}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->loadMore()Z

    move-result v7

    if-nez v7, :cond_b

    move v7, v4

    move v4, v1

    .line 244
    :goto_4
    if-nez v3, :cond_1

    .line 245
    const-string/jumbo v8, "Decimal point not followed by a digit"

    invoke-virtual {p0, v7, v8}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->reportUnexpectedNumberChar(ILjava/lang/String;)V

    :cond_1
    move v9, v3

    move v3, v5

    move v13, v4

    move-object v4, v6

    move v6, v13

    .line 250
    :goto_5
    const/16 v5, 0x65

    if-eq v7, v5, :cond_2

    const/16 v5, 0x45

    if-ne v7, v5, :cond_13

    .line 251
    :cond_2
    array-length v5, v4

    if-lt v3, v5, :cond_3

    .line 252
    iget-object v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v3}, Lorg/codehaus/jackson/util/TextBuffer;->finishCurrentSegment()[C

    move-result-object v3

    move-object v4, v3

    move v3, v2

    .line 255
    :cond_3
    add-int/lit8 v5, v3, 0x1

    aput-char v7, v4, v3

    .line 257
    iget v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    iget v7, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputEnd:I

    if-ge v3, v7, :cond_d

    iget-object v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    iget v7, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    aget-char v7, v3, v7

    .line 260
    :goto_6
    if-eq v7, v10, :cond_4

    const/16 v3, 0x2b

    if-ne v7, v3, :cond_12

    .line 261
    :cond_4
    array-length v3, v4

    if-lt v5, v3, :cond_11

    .line 262
    iget-object v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v3}, Lorg/codehaus/jackson/util/TextBuffer;->finishCurrentSegment()[C

    move-result-object v4

    move v3, v2

    .line 265
    :goto_7
    add-int/lit8 v5, v3, 0x1

    aput-char v7, v4, v3

    .line 267
    iget v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    iget v7, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputEnd:I

    if-ge v3, v7, :cond_e

    iget-object v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    iget v7, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    aget-char v3, v3, v7

    :goto_8
    move v8, v3

    move v3, v5

    move v5, v2

    .line 272
    :goto_9
    if-gt v8, v12, :cond_10

    if-lt v8, v11, :cond_10

    .line 273
    add-int/lit8 v5, v5, 0x1

    .line 274
    array-length v7, v4

    if-lt v3, v7, :cond_5

    .line 275
    iget-object v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v3}, Lorg/codehaus/jackson/util/TextBuffer;->finishCurrentSegment()[C

    move-result-object v3

    move-object v4, v3

    move v3, v2

    .line 278
    :cond_5
    add-int/lit8 v7, v3, 0x1

    aput-char v8, v4, v3

    .line 279
    iget v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    iget v10, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputEnd:I

    if-lt v3, v10, :cond_f

    invoke-virtual {p0}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->loadMore()Z

    move-result v3

    if-nez v3, :cond_f

    move v2, v5

    move v3, v7

    .line 286
    :goto_a
    if-nez v2, :cond_6

    .line 287
    const-string/jumbo v4, "Exponent indicator not followed by a digit"

    invoke-virtual {p0, v8, v4}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->reportUnexpectedNumberChar(ILjava/lang/String;)V

    .line 292
    :cond_6
    :goto_b
    if-nez v1, :cond_7

    .line 293
    iget v1, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    .line 295
    :cond_7
    iget-object v1, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v1, v3}, Lorg/codehaus/jackson/util/TextBuffer;->setCurrentLength(I)V

    .line 298
    invoke-virtual {p0, p1, v0, v9, v2}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->reset(ZIII)Lorg/codehaus/jackson/JsonToken;

    move-result-object v0

    return-object v0

    .line 199
    :cond_8
    iget-object v5, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    iget v6, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    aget-char v6, v5, v6

    .line 200
    if-lt v6, v11, :cond_18

    if-le v6, v12, :cond_9

    move v8, v2

    move v5, v6

    .line 201
    goto/16 :goto_2

    .line 203
    :cond_9
    add-int/lit8 v0, v0, 0x1

    .line 205
    const/4 v5, 0x2

    if-ne v0, v5, :cond_a

    .line 206
    add-int/lit8 v5, v3, -0x1

    aget-char v5, v4, v5

    if-ne v5, v11, :cond_a

    .line 207
    const-string/jumbo v5, "Leading zeroes not allowed"

    invoke-virtual {p0, v5}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->reportInvalidNumber(Ljava/lang/String;)V

    .line 210
    :cond_a
    array-length v5, v4

    if-lt v3, v5, :cond_17

    .line 211
    iget-object v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v3}, Lorg/codehaus/jackson/util/TextBuffer;->finishCurrentSegment()[C

    move-result-object v4

    move v5, v2

    .line 214
    :goto_c
    add-int/lit8 v3, v5, 0x1

    aput-char v6, v4, v5

    goto/16 :goto_1

    .line 232
    :cond_b
    iget-object v4, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    iget v7, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    add-int/lit8 v9, v7, 0x1

    iput v9, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    aget-char v4, v4, v7

    .line 233
    if-lt v4, v11, :cond_15

    if-le v4, v12, :cond_c

    move v7, v4

    move v4, v8

    .line 234
    goto/16 :goto_4

    .line 236
    :cond_c
    add-int/lit8 v3, v3, 0x1

    .line 237
    array-length v7, v6

    if-lt v5, v7, :cond_14

    .line 238
    iget-object v5, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    invoke-virtual {v5}, Lorg/codehaus/jackson/util/TextBuffer;->finishCurrentSegment()[C

    move-result-object v6

    move v7, v2

    .line 241
    :goto_d
    add-int/lit8 v5, v7, 0x1

    aput-char v4, v6, v7

    goto/16 :goto_3

    .line 257
    :cond_d
    const-string/jumbo v3, "expected a digit for number exponent"

    invoke-virtual {p0, v3}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->getNextChar(Ljava/lang/String;)C

    move-result v7

    goto/16 :goto_6

    .line 267
    :cond_e
    const-string/jumbo v3, "expected a digit for number exponent"

    invoke-virtual {p0, v3}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->getNextChar(Ljava/lang/String;)C

    move-result v3

    goto/16 :goto_8

    .line 283
    :cond_f
    iget-object v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    iget v8, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    add-int/lit8 v10, v8, 0x1

    iput v10, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    aget-char v3, v3, v8

    move v8, v3

    move v3, v7

    goto/16 :goto_9

    :cond_10
    move v2, v5

    move v1, v6

    goto/16 :goto_a

    :cond_11
    move v3, v5

    goto/16 :goto_7

    :cond_12
    move v8, v7

    move v3, v5

    move v5, v2

    goto/16 :goto_9

    :cond_13
    move v1, v6

    goto/16 :goto_b

    :cond_14
    move v7, v5

    goto :goto_d

    :cond_15
    move v7, v4

    move v4, v8

    goto/16 :goto_4

    :cond_16
    move v9, v2

    move v6, v8

    move v7, v5

    goto/16 :goto_5

    :cond_17
    move v5, v3

    goto :goto_c

    :cond_18
    move v8, v2

    move v5, v6

    goto/16 :goto_2

    :cond_19
    move v0, v2

    goto/16 :goto_0
.end method


# virtual methods
.method protected final parseNumberText(I)Lorg/codehaus/jackson/JsonToken;
    .locals 13

    .prologue
    const/16 v11, 0x2d

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v10, 0x39

    const/16 v9, 0x30

    .line 60
    if-ne p1, v11, :cond_1

    move v0, v1

    .line 61
    :goto_0
    iget v4, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    .line 62
    add-int/lit8 v5, v4, -0x1

    .line 63
    iget v7, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputEnd:I

    .line 67
    if-eqz v0, :cond_11

    .line 68
    iget v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputEnd:I

    if-lt v4, v3, :cond_2

    .line 164
    :cond_0
    if-eqz v0, :cond_e

    add-int/lit8 v1, v5, 0x1

    :goto_1
    iput v1, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    .line 165
    invoke-direct {p0, v0}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->parseNumberText2(Z)Lorg/codehaus/jackson/JsonToken;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_1
    move v0, v2

    .line 60
    goto :goto_0

    .line 71
    :cond_2
    iget-object v6, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    add-int/lit8 v3, v4, 0x1

    aget-char v4, v6, v4

    .line 73
    if-gt v4, v10, :cond_3

    if-ge v4, v9, :cond_4

    .line 74
    :cond_3
    const-string/jumbo v6, "expected digit (0-9) to follow minus sign, for valid numeric value"

    invoke-virtual {p0, v4, v6}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->reportUnexpectedNumberChar(ILjava/lang/String;)V

    .line 94
    :cond_4
    :goto_3
    iget v4, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputEnd:I

    if-ge v3, v4, :cond_0

    .line 97
    iget-object v6, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    add-int/lit8 v4, v3, 0x1

    aget-char v3, v6, v3

    .line 98
    if-lt v3, v9, :cond_5

    if-le v3, v10, :cond_a

    .line 112
    :cond_5
    const/16 v6, 0x2e

    if-ne v3, v6, :cond_10

    move v3, v2

    move v6, v4

    .line 115
    :goto_4
    if-ge v6, v7, :cond_0

    .line 118
    iget-object v8, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    add-int/lit8 v4, v6, 0x1

    aget-char v6, v8, v6

    .line 119
    if-lt v6, v9, :cond_6

    if-le v6, v10, :cond_b

    .line 125
    :cond_6
    if-nez v3, :cond_7

    .line 126
    const-string/jumbo v8, "Decimal point not followed by a digit"

    invoke-virtual {p0, v6, v8}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->reportUnexpectedNumberChar(ILjava/lang/String;)V

    :cond_7
    move v12, v3

    move v3, v4

    move v4, v6

    move v6, v12

    .line 131
    :goto_5
    const/16 v8, 0x65

    if-eq v4, v8, :cond_8

    const/16 v8, 0x45

    if-ne v4, v8, :cond_d

    .line 132
    :cond_8
    if-ge v3, v7, :cond_0

    .line 136
    iget-object v8, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    add-int/lit8 v4, v3, 0x1

    aget-char v3, v8, v3

    .line 137
    if-eq v3, v11, :cond_9

    const/16 v8, 0x2b

    if-ne v3, v8, :cond_f

    .line 138
    :cond_9
    if-ge v4, v7, :cond_0

    .line 141
    iget-object v8, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    add-int/lit8 v3, v4, 0x1

    aget-char v4, v8, v4

    .line 143
    :goto_6
    if-gt v4, v10, :cond_c

    if-lt v4, v9, :cond_c

    .line 144
    add-int/lit8 v2, v2, 0x1

    .line 145
    if-ge v3, v7, :cond_0

    .line 148
    iget-object v8, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    add-int/lit8 v4, v3, 0x1

    aget-char v3, v8, v3

    move v12, v4

    move v4, v3

    move v3, v12

    goto :goto_6

    .line 102
    :cond_a
    add-int/lit8 v1, v1, 0x1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_11

    .line 103
    iget-object v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    add-int/lit8 v6, v4, -0x2

    aget-char v3, v3, v6

    if-ne v3, v9, :cond_11

    .line 104
    const-string/jumbo v3, "Leading zeroes not allowed"

    invoke-virtual {p0, v3}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->reportInvalidNumber(Ljava/lang/String;)V

    move v3, v4

    goto :goto_3

    .line 122
    :cond_b
    add-int/lit8 v3, v3, 0x1

    move v6, v4

    goto :goto_4

    .line 151
    :cond_c
    if-nez v2, :cond_d

    .line 152
    const-string/jumbo v7, "Exponent indicator not followed by a digit"

    invoke-virtual {p0, v4, v7}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->reportUnexpectedNumberChar(ILjava/lang/String;)V

    .line 157
    :cond_d
    add-int/lit8 v3, v3, -0x1

    .line 158
    iput v3, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputPtr:I

    .line 159
    sub-int/2addr v3, v5

    .line 160
    iget-object v4, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_textBuffer:Lorg/codehaus/jackson/util/TextBuffer;

    iget-object v7, p0, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->_inputBuffer:[C

    invoke-virtual {v4, v7, v5, v3}, Lorg/codehaus/jackson/util/TextBuffer;->resetWithShared([CII)V

    .line 161
    invoke-virtual {p0, v0, v1, v6, v2}, Lorg/codehaus/jackson/impl/ReaderBasedNumericParser;->reset(ZIII)Lorg/codehaus/jackson/JsonToken;

    move-result-object v0

    goto/16 :goto_2

    :cond_e
    move v1, v5

    .line 164
    goto/16 :goto_1

    :cond_f
    move v12, v4

    move v4, v3

    move v3, v12

    goto :goto_6

    :cond_10
    move v6, v2

    move v12, v4

    move v4, v3

    move v3, v12

    goto :goto_5

    :cond_11
    move v3, v4

    goto/16 :goto_3
.end method
