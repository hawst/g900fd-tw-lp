.class public Lcom/htc/circontrol/CIRControl;
.super Ljava/lang/Object;


# static fields
.field static final CIRSERVICE_CLASS:Ljava/lang/String; = "com.htc.cirmodule.CIRControlService"

.field static final CIRSERVICE_PATH:Ljava/lang/String; = "com.htc.cirmodule"

.field public static final ERR_CANCEL:I = 0x18

.field public static final ERR_CANCEL_FAIL:I = 0x15

.field public static final ERR_CHECKSUM_ERROR:I = 0x16

.field public static final ERR_CMD_DROPPED:I = 0x5

.field public static final ERR_CMD_FAILED:I = 0x2

.field public static final ERR_HW_BUSY:I = 0x3

.field public static final ERR_INVALID_VALUE:I = 0x13

.field public static final ERR_IO_ERROR:I = 0x4

.field public static final ERR_LEARNING_TIMEOUT:I = 0x14

.field public static final ERR_NONE:I = 0x0

.field public static final ERR_OBJ_ERROR:I = 0x20

.field public static final ERR_OUT_OF_FREQ:I = 0x17

.field public static final ERR_PIPE_ERROR:I = 0x1a

.field public static final ERR_PULSE_ERROR:I = 0x19

.field public static final ERR_TTY_READ_ERROR:I = 0x12

.field public static final ERR_TTY_SETUP_ERROR:I = 0x10

.field public static final ERR_TTY_WRITE_ERROR:I = 0x11

.field public static final ERR_UNKNOWN:I = 0x1

.field public static final KEY_CMD_RESULT:Ljava/lang/String; = "Result"

.field public static final KEY_COMMAND:Ljava/lang/String; = "Command"

.field public static final KEY_DISCARD_ID:Ljava/lang/String; = "DCID"

.field public static final KEY_DROPPABLE:Ljava/lang/String; = "Drop"

.field public static final KEY_RESULT_ID:Ljava/lang/String; = "RID"

.field public static final KEY_TIMEOUT:Ljava/lang/String; = "Timeout"

.field public static final MSG_ARG_DROPPABLE:I = 0x1

.field public static final MSG_ARG_NORMAL:I = 0x0

.field public static final MSG_CMD_CANCEL:I = 0x8

.field public static final MSG_CMD_DISCARD:I = 0x9

.field public static final MSG_CMD_GET_FW_VER:I = 0x6

.field public static final MSG_CMD_LEARN_IR:I = 0x3

.field public static final MSG_CMD_REGISTER_CLIENT:I = 0x1

.field public static final MSG_CMD_RESET:I = 0x7

.field public static final MSG_CMD_TRANSMIT_IR:I = 0x4

.field public static final MSG_CMD_UNREGISTER_CLIENT:I = 0x2

.field public static final MSG_RET_CANCEL:I = 0x6

.field public static final MSG_RET_DISCARD:I = 0x7

.field public static final MSG_RET_GET_FW_VER:I = 0x4

.field public static final MSG_RET_LEARN_IR:I = 0x1

.field public static final MSG_RET_RESET:I = 0x5

.field public static final MSG_RET_STARTED:I = 0x8

.field public static final MSG_RET_TRANSMIT_IR:I = 0x2


# instance fields
.field private TAG_DEBUG:Ljava/lang/String;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIsBound:Z

.field private final mMessenger:Landroid/os/Messenger;

.field private mService:Landroid/os/Messenger;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v1, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    .line 26
    iput-object v1, p0, Lcom/htc/circontrol/CIRControl;->mService:Landroid/os/Messenger;

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    .line 28
    iput-object v1, p0, Lcom/htc/circontrol/CIRControl;->mHandler:Landroid/os/Handler;

    .line 243
    const-string/jumbo v0, "CIRControl"

    iput-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    .line 245
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/htc/circontrol/CIRControl$IncomingHandler;

    invoke-direct {v1, p0}, Lcom/htc/circontrol/CIRControl$IncomingHandler;-><init>(Lcom/htc/circontrol/CIRControl;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/htc/circontrol/CIRControl;->mMessenger:Landroid/os/Messenger;

    .line 290
    new-instance v0, Lcom/htc/circontrol/CIRControl$1;

    invoke-direct {v0, p0}, Lcom/htc/circontrol/CIRControl$1;-><init>(Lcom/htc/circontrol/CIRControl;)V

    iput-object v0, p0, Lcom/htc/circontrol/CIRControl;->mConnection:Landroid/content/ServiceConnection;

    .line 284
    if-eqz p1, :cond_0

    .line 285
    iput-object p1, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    .line 286
    iput-object p2, p0, Lcom/htc/circontrol/CIRControl;->mHandler:Landroid/os/Handler;

    .line 288
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/htc/circontrol/CIRControl;Landroid/os/Messenger;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/htc/circontrol/CIRControl;->mService:Landroid/os/Messenger;

    return-void
.end method

.method static synthetic access$1(Lcom/htc/circontrol/CIRControl;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/htc/circontrol/CIRControl;Z)V
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    return-void
.end method

.method static synthetic access$3(Lcom/htc/circontrol/CIRControl;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 499
    invoke-direct {p0, p1, p2}, Lcom/htc/circontrol/CIRControl;->sendMessageToService(ILandroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$4(Lcom/htc/circontrol/CIRControl;ILandroid/os/Bundle;II)V
    .locals 0

    .prologue
    .line 526
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/htc/circontrol/CIRControl;->sendMessageToUI(ILandroid/os/Bundle;II)V

    return-void
.end method

.method private sendCommand(I)Ljava/util/UUID;
    .locals 5

    .prologue
    .line 407
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 408
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    .line 409
    const-string/jumbo v2, "RID"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 410
    iget-object v2, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Control(L&S): UUID="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    iget-object v2, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "sendCommand: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    invoke-direct {p0, p1, v0}, Lcom/htc/circontrol/CIRControl;->sendMessageToService(ILandroid/os/Bundle;)V

    .line 415
    return-object v1
.end method

.method private sendMessageToService(ILandroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 502
    iget-boolean v0, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    if-eqz v0, :cond_2

    .line 503
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mService:Landroid/os/Messenger;

    if-eqz v0, :cond_1

    .line 505
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 506
    if-eqz p2, :cond_0

    .line 508
    invoke-virtual {v0, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 510
    :cond_0
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->mMessenger:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 511
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "sendMessageToService: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->mService:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 524
    :goto_0
    return-void

    .line 513
    :catch_0
    move-exception v0

    .line 514
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 518
    :cond_1
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "sendMessageToService: mService null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 522
    :cond_2
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "sendMessageToService: mIsBound false"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private sendMessageToUI(ILandroid/os/Bundle;II)V
    .locals 4

    .prologue
    .line 529
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 530
    const/4 v0, 0x0

    invoke-static {v0, p1, p3, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    .line 531
    if-eqz p2, :cond_0

    .line 532
    invoke-virtual {v0, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 533
    :cond_0
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "sendMessageToUI: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 539
    :goto_0
    return-void

    .line 537
    :cond_1
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "sendMessageToUI: mHandler null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private startCIRService()V
    .locals 3

    .prologue
    .line 485
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 486
    const-string/jumbo v1, "com.htc.cirmodule"

    const-string/jumbo v2, "com.htc.cirmodule.CIRControlService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 487
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v2, "StartCIRService"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 489
    return-void
.end method

.method private stopCIRService()V
    .locals 3

    .prologue
    .line 493
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 494
    const-string/jumbo v1, "com.htc.cirmodule"

    const-string/jumbo v2, "com.htc.cirmodule.CIRControlService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 495
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v2, "StopCIRService"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 497
    return-void
.end method


# virtual methods
.method public cancelCommand()Ljava/util/UUID;
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    if-nez v0, :cond_1

    .line 366
    :cond_0
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "Cannot cancelCommand because null context or not bound yet!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    const/4 v0, 0x0

    .line 373
    :goto_0
    return-object v0

    .line 369
    :cond_1
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "cancelCommand"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    monitor-enter p0

    .line 371
    const/16 v0, 0x8

    :try_start_0
    invoke-direct {p0, v0}, Lcom/htc/circontrol/CIRControl;->sendCommand(I)Ljava/util/UUID;

    move-result-object v0

    .line 370
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public discardCommand(Ljava/util/UUID;)Ljava/util/UUID;
    .locals 2

    .prologue
    .line 382
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    if-nez v0, :cond_1

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "Cannot discardCommand because null context or not bound yet!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    const/4 v0, 0x0

    .line 387
    :goto_0
    return-object v0

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "discardCommand"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/htc/circontrol/CIRControl;->sendCommand(I)Ljava/util/UUID;

    move-result-object v0

    goto :goto_0
.end method

.method doBindService()V
    .locals 4

    .prologue
    .line 542
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 547
    :goto_0
    return-void

    .line 543
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 544
    const-string/jumbo v1, "com.htc.cirmodule"

    const-string/jumbo v2, "com.htc.cirmodule.CIRControlService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 545
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v2, "doBindService"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/htc/circontrol/CIRControl;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method

.method doUnbindService()V
    .locals 3

    .prologue
    .line 550
    iget-boolean v0, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    if-eqz v0, :cond_1

    .line 552
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mService:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    .line 554
    const/4 v0, 0x0

    const/4 v1, 0x2

    :try_start_0
    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 555
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->mMessenger:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 556
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v2, "doUnbindService: send unregister"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->mService:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 567
    :goto_0
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/htc/circontrol/CIRControl;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 568
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    .line 573
    :goto_1
    return-void

    .line 558
    :catch_0
    move-exception v0

    .line 560
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 564
    :cond_0
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "doUnbindService: mService null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 571
    :cond_1
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "doUnbindService: mIsBound false"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getFirmwareVersion()Ljava/util/UUID;
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    if-nez v0, :cond_1

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "Cannot getFirmwareVersion because null context or not bound yet!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    const/4 v0, 0x0

    .line 356
    :goto_0
    return-object v0

    .line 355
    :cond_1
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "getFirmwareVersion"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/htc/circontrol/CIRControl;->sendCommand(I)Ljava/util/UUID;

    move-result-object v0

    goto :goto_0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    return v0
.end method

.method public learnIRCmd(I)Ljava/util/UUID;
    .locals 5

    .prologue
    .line 463
    const/16 v0, 0x78

    if-le p1, v0, :cond_0

    .line 464
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "timeout value is not less than 120 seconds"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    if-nez v0, :cond_2

    .line 468
    :cond_1
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "Cannot learnIRCmd because null context or not bound yet!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    const/4 v0, 0x0

    .line 481
    :goto_0
    return-object v0

    .line 472
    :cond_2
    monitor-enter p0

    .line 473
    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 474
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 475
    const-string/jumbo v2, "RID"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 476
    const-string/jumbo v2, "Timeout"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 477
    iget-object v2, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Control(R): UUID="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " timeout: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    const/4 v2, 0x3

    invoke-direct {p0, v2, v1}, Lcom/htc/circontrol/CIRControl;->sendMessageToService(ILandroid/os/Bundle;)V

    .line 472
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public reset()Ljava/util/UUID;
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    if-nez v0, :cond_1

    .line 397
    :cond_0
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "Cannot reset because null context or not bound yet!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    const/4 v0, 0x0

    .line 401
    :goto_0
    return-object v0

    .line 400
    :cond_1
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "reset"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/htc/circontrol/CIRControl;->sendCommand(I)Ljava/util/UUID;

    move-result-object v0

    goto :goto_0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    if-eqz v0, :cond_1

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "Cannot start because null context or is bound already!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    :goto_0
    return-void

    .line 329
    :cond_1
    invoke-direct {p0}, Lcom/htc/circontrol/CIRControl;->startCIRService()V

    .line 330
    invoke-virtual {p0}, Lcom/htc/circontrol/CIRControl;->doBindService()V

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "Cannot stop because null context!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    :goto_0
    return-void

    .line 341
    :cond_0
    invoke-virtual {p0}, Lcom/htc/circontrol/CIRControl;->doUnbindService()V

    .line 342
    invoke-direct {p0}, Lcom/htc/circontrol/CIRControl;->stopCIRService()V

    goto :goto_0
.end method

.method public transmitIRCmd(Lcom/htc/htcircontrol/HtcIrData;Z)Ljava/util/UUID;
    .locals 5

    .prologue
    .line 430
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/htc/circontrol/CIRControl;->mIsBound:Z

    if-nez v0, :cond_1

    .line 431
    :cond_0
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    const-string/jumbo v1, "Cannot transmitIRCmd because null context or not bound yet!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    const/4 v0, 0x0

    .line 446
    :goto_0
    return-object v0

    .line 435
    :cond_1
    monitor-enter p0

    .line 436
    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 437
    const-string/jumbo v0, "Command"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 438
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 439
    const-string/jumbo v2, "RID"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 440
    iget-object v2, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Control(L&S): UUID="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    iget-object v2, p0, Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "transmitIRCmd: drop="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " {"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/htc/htcircontrol/HtcIrData;->getRepeatCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/htc/htcircontrol/HtcIrData;->getFrequency()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/htc/htcircontrol/HtcIrData;->getFrame()[I

    move-result-object v4

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    const-string/jumbo v2, "Drop"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 444
    const/4 v2, 0x4

    invoke-direct {p0, v2, v1}, Lcom/htc/circontrol/CIRControl;->sendMessageToService(ILandroid/os/Bundle;)V

    .line 435
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
