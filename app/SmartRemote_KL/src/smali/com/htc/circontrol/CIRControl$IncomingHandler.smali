.class Lcom/htc/circontrol/CIRControl$IncomingHandler;
.super Landroid/os/Handler;


# instance fields
.field final synthetic this$0:Lcom/htc/circontrol/CIRControl;


# direct methods
.method constructor <init>(Lcom/htc/circontrol/CIRControl;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/htc/circontrol/CIRControl$IncomingHandler;->this$0:Lcom/htc/circontrol/CIRControl;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 249
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 271
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 273
    :goto_1
    return-void

    .line 251
    :pswitch_1
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$IncomingHandler;->this$0:Lcom/htc/circontrol/CIRControl;

    # getter for: Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;
    invoke-static {v0}, Lcom/htc/circontrol/CIRControl;->access$1(Lcom/htc/circontrol/CIRControl;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Got <MSG_RET_LEARN_IR>"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$IncomingHandler;->this$0:Lcom/htc/circontrol/CIRControl;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    # invokes: Lcom/htc/circontrol/CIRControl;->sendMessageToUI(ILandroid/os/Bundle;II)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/htc/circontrol/CIRControl;->access$4(Lcom/htc/circontrol/CIRControl;ILandroid/os/Bundle;II)V

    goto :goto_1

    .line 255
    :pswitch_2
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$IncomingHandler;->this$0:Lcom/htc/circontrol/CIRControl;

    # getter for: Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;
    invoke-static {v0}, Lcom/htc/circontrol/CIRControl;->access$1(Lcom/htc/circontrol/CIRControl;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Got <MSG_RET_TRANSMIT_IR>"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$IncomingHandler;->this$0:Lcom/htc/circontrol/CIRControl;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    # invokes: Lcom/htc/circontrol/CIRControl;->sendMessageToUI(ILandroid/os/Bundle;II)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/htc/circontrol/CIRControl;->access$4(Lcom/htc/circontrol/CIRControl;ILandroid/os/Bundle;II)V

    goto :goto_1

    .line 259
    :pswitch_3
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$IncomingHandler;->this$0:Lcom/htc/circontrol/CIRControl;

    # getter for: Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;
    invoke-static {v0}, Lcom/htc/circontrol/CIRControl;->access$1(Lcom/htc/circontrol/CIRControl;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Got <MSG_RET_CANCEL>"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$IncomingHandler;->this$0:Lcom/htc/circontrol/CIRControl;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    # invokes: Lcom/htc/circontrol/CIRControl;->sendMessageToUI(ILandroid/os/Bundle;II)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/htc/circontrol/CIRControl;->access$4(Lcom/htc/circontrol/CIRControl;ILandroid/os/Bundle;II)V

    goto :goto_1

    .line 263
    :pswitch_4
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$IncomingHandler;->this$0:Lcom/htc/circontrol/CIRControl;

    # getter for: Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;
    invoke-static {v0}, Lcom/htc/circontrol/CIRControl;->access$1(Lcom/htc/circontrol/CIRControl;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Got <MSG_RET_DISCARD>"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$IncomingHandler;->this$0:Lcom/htc/circontrol/CIRControl;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    # invokes: Lcom/htc/circontrol/CIRControl;->sendMessageToUI(ILandroid/os/Bundle;II)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/htc/circontrol/CIRControl;->access$4(Lcom/htc/circontrol/CIRControl;ILandroid/os/Bundle;II)V

    goto :goto_0

    .line 249
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
