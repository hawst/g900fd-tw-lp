.class Lcom/htc/circontrol/CIRControl$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic this$0:Lcom/htc/circontrol/CIRControl;


# direct methods
.method constructor <init>(Lcom/htc/circontrol/CIRControl;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/htc/circontrol/CIRControl$1;->this$0:Lcom/htc/circontrol/CIRControl;

    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 293
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$1;->this$0:Lcom/htc/circontrol/CIRControl;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    invoke-static {v0, v1}, Lcom/htc/circontrol/CIRControl;->access$0(Lcom/htc/circontrol/CIRControl;Landroid/os/Messenger;)V

    .line 295
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$1;->this$0:Lcom/htc/circontrol/CIRControl;

    # getter for: Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;
    invoke-static {v0}, Lcom/htc/circontrol/CIRControl;->access$1(Lcom/htc/circontrol/CIRControl;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onServiceConnected register client"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$1;->this$0:Lcom/htc/circontrol/CIRControl;

    invoke-static {v0, v4}, Lcom/htc/circontrol/CIRControl;->access$2(Lcom/htc/circontrol/CIRControl;Z)V

    .line 297
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 298
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    .line 299
    const-string/jumbo v2, "RID"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 300
    iget-object v1, p0, Lcom/htc/circontrol/CIRControl$1;->this$0:Lcom/htc/circontrol/CIRControl;

    # invokes: Lcom/htc/circontrol/CIRControl;->sendMessageToService(ILandroid/os/Bundle;)V
    invoke-static {v1, v4, v0}, Lcom/htc/circontrol/CIRControl;->access$3(Lcom/htc/circontrol/CIRControl;ILandroid/os/Bundle;)V

    .line 301
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$1;->this$0:Lcom/htc/circontrol/CIRControl;

    const/16 v1, 0x8

    const/4 v2, 0x0

    # invokes: Lcom/htc/circontrol/CIRControl;->sendMessageToUI(ILandroid/os/Bundle;II)V
    invoke-static {v0, v1, v2, v3, v3}, Lcom/htc/circontrol/CIRControl;->access$4(Lcom/htc/circontrol/CIRControl;ILandroid/os/Bundle;II)V

    .line 302
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$1;->this$0:Lcom/htc/circontrol/CIRControl;

    # getter for: Lcom/htc/circontrol/CIRControl;->TAG_DEBUG:Ljava/lang/String;
    invoke-static {v0}, Lcom/htc/circontrol/CIRControl;->access$1(Lcom/htc/circontrol/CIRControl;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$1;->this$0:Lcom/htc/circontrol/CIRControl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/htc/circontrol/CIRControl;->access$0(Lcom/htc/circontrol/CIRControl;Landroid/os/Messenger;)V

    .line 308
    iget-object v0, p0, Lcom/htc/circontrol/CIRControl$1;->this$0:Lcom/htc/circontrol/CIRControl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/htc/circontrol/CIRControl;->access$2(Lcom/htc/circontrol/CIRControl;Z)V

    .line 309
    return-void
.end method
