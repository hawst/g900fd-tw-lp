.class public Lcom/htc/htcircontrol/HtcIrInterface;
.super Ljava/lang/Object;


# static fields
.field public static final CIR_CANCEL:I = 0x18

.field public static final CIR_CANCEL_ERROR:I = 0x15

.field public static final CIR_CHECKSUM_ERROR:I = 0x16

.field public static final CIR_COMPLETION:I = 0x0

.field public static final CIR_INVALID_VALUE:I = 0x13

.field public static final CIR_LEARNING_TIMEOUT:I = 0x14

.field public static final CIR_OBJ_ERROR:I = 0x20

.field public static final CIR_OUT_OF_FREQ:I = 0x17

.field public static final CIR_PIPE_ERROR:I = 0x1a

.field public static final CIR_PULSE_ERROR:I = 0x19

.field public static final CIR_TTY_READ_ERROR:I = 0x12

.field public static final CIR_TTY_SETUP_ERROR:I = 0x10

.field public static final CIR_TTY_WRITE_ERROR:I = 0x11

.field public static final CIR_UNKNOWN_ERROR:I = 0x30


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native cancelIR()I
.end method

.method public static native doReset()I
.end method

.method public static native enableIR(I)I
.end method

.method public static native getFirmwareVersion()I
.end method

.method public static native learnIR(I)Lcom/htc/htcircontrol/HtcIrData;
.end method

.method public static native loadAndSendIR(III[I)[I
.end method

.method public static native receiveIR(I)[I
.end method

.method public static native sendIR([I)[I
.end method

.method public static native transmitIR(Lcom/htc/htcircontrol/HtcIrData;)I
.end method
