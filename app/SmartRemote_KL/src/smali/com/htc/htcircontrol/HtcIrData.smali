.class public Lcom/htc/htcircontrol/HtcIrData;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final TAG:Ljava/lang/String; = "HtcIrData"

.field private static overhead:I


# instance fields
.field private frame:[I

.field private frequency:I

.field private period:I

.field private periodTolerance:I

.field private repeatCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/16 v0, 0x8

    sput v0, Lcom/htc/htcircontrol/HtcIrData;->overhead:I

    .line 10
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/16 v0, 0x96

    iput v0, p0, Lcom/htc/htcircontrol/HtcIrData;->period:I

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/htc/htcircontrol/HtcIrData;->periodTolerance:I

    .line 45
    return-void
.end method

.method public constructor <init>(II[I)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/16 v0, 0x96

    iput v0, p0, Lcom/htc/htcircontrol/HtcIrData;->period:I

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/htc/htcircontrol/HtcIrData;->periodTolerance:I

    .line 63
    const/16 v0, 0xff

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    if-ge p1, v0, :cond_1

    .line 64
    :cond_0
    const-string/jumbo v0, "HtcIrData"

    const-string/jumbo v1, "argument rc is invalid"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "rc must be in range of 1 - 255"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_1
    iput p1, p0, Lcom/htc/htcircontrol/HtcIrData;->repeatCount:I

    .line 69
    const v0, 0xea60

    if-gt p2, v0, :cond_2

    const/16 v0, 0x5dc0

    if-ge p2, v0, :cond_3

    .line 70
    :cond_2
    const-string/jumbo v0, "HtcIrData"

    const-string/jumbo v1, "argument freq is invalid"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "freq must be in range of 24000 - 60000"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_3
    iput p2, p0, Lcom/htc/htcircontrol/HtcIrData;->frequency:I

    .line 75
    if-nez p3, :cond_4

    .line 76
    const-string/jumbo v0, "HtcIrData"

    const-string/jumbo v1, "argument array is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "array must be not null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_4
    array-length v0, p3

    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/htc/htcircontrol/HtcIrData;->frame:[I

    .line 81
    invoke-direct {p0}, Lcom/htc/htcircontrol/HtcIrData;->validateFrame()Z

    move-result v0

    if-nez v0, :cond_5

    .line 82
    const-string/jumbo v0, "HtcIrData"

    const-string/jumbo v1, "argument array is too big or size is not even"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "array length is too big or not even"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_5
    invoke-direct {p0}, Lcom/htc/htcircontrol/HtcIrData;->calculateFramePeriod()V

    .line 88
    const-string/jumbo v0, "HtcIrData"

    const-string/jumbo v1, "Constructor"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    return-void
.end method

.method private calculateFramePeriod()V
    .locals 6

    .prologue
    .line 211
    const-wide/16 v2, 0x0

    .line 213
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/htc/htcircontrol/HtcIrData;->frame:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 218
    iget v0, p0, Lcom/htc/htcircontrol/HtcIrData;->frequency:I

    int-to-double v0, v0

    div-double v0, v2, v0

    .line 219
    iget v2, p0, Lcom/htc/htcircontrol/HtcIrData;->repeatCount:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    .line 220
    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    .line 221
    const-string/jumbo v2, "HtcIrData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "frame period = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    double-to-int v0, v0

    iput v0, p0, Lcom/htc/htcircontrol/HtcIrData;->period:I

    .line 223
    return-void

    .line 214
    :cond_0
    iget-object v1, p0, Lcom/htc/htcircontrol/HtcIrData;->frame:[I

    aget v1, v1, v0

    int-to-double v4, v1

    .line 215
    add-double/2addr v2, v4

    .line 213
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private validateFrame()Z
    .locals 7

    .prologue
    const/16 v6, 0x400

    const/4 v1, 0x0

    .line 226
    const/4 v3, 0x1

    .line 231
    iget-object v0, p0, Lcom/htc/htcircontrol/HtcIrData;->frame:[I

    array-length v0, v0

    sget v2, Lcom/htc/htcircontrol/HtcIrData;->overhead:I

    add-int/2addr v0, v2

    if-le v0, v6, :cond_1

    .line 258
    :cond_0
    :goto_0
    return v1

    .line 234
    :cond_1
    iget-object v0, p0, Lcom/htc/htcircontrol/HtcIrData;->frame:[I

    array-length v0, v0

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    move v0, v1

    move v2, v1

    .line 240
    :goto_1
    iget-object v4, p0, Lcom/htc/htcircontrol/HtcIrData;->frame:[I

    array-length v4, v4

    if-lt v2, v4, :cond_2

    .line 251
    sget v2, Lcom/htc/htcircontrol/HtcIrData;->overhead:I

    add-int/2addr v0, v2

    if-gt v0, v6, :cond_0

    move v1, v3

    goto :goto_0

    .line 241
    :cond_2
    iget-object v4, p0, Lcom/htc/htcircontrol/HtcIrData;->frame:[I

    aget v4, v4, v2

    const/16 v5, 0x80

    if-ge v4, v5, :cond_3

    .line 242
    add-int/lit8 v0, v0, 0x1

    .line 240
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 245
    :cond_3
    add-int/lit8 v0, v0, 0x2

    goto :goto_2
.end method


# virtual methods
.method public getFrame()[I
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/htc/htcircontrol/HtcIrData;->frame:[I

    iget-object v1, p0, Lcom/htc/htcircontrol/HtcIrData;->frame:[I

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    return-object v0
.end method

.method public getFrequency()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/htc/htcircontrol/HtcIrData;->frequency:I

    return v0
.end method

.method public getPeriod()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/htc/htcircontrol/HtcIrData;->period:I

    return v0
.end method

.method public getPeriodTolerance()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/htc/htcircontrol/HtcIrData;->periodTolerance:I

    return v0
.end method

.method public getRepeatCount()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/htc/htcircontrol/HtcIrData;->repeatCount:I

    return v0
.end method

.method public setFrame([I)V
    .locals 2

    .prologue
    .line 166
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/htc/htcircontrol/HtcIrData;->frame:[I

    .line 167
    invoke-direct {p0}, Lcom/htc/htcircontrol/HtcIrData;->validateFrame()Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    const-string/jumbo v0, "HtcIrData"

    const-string/jumbo v1, "argument array is too big or size is not even"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "array is too big or its size is not even"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_0
    return-void
.end method

.method public setFrequency(I)V
    .locals 2

    .prologue
    .line 130
    iput p1, p0, Lcom/htc/htcircontrol/HtcIrData;->frequency:I

    .line 131
    iget v0, p0, Lcom/htc/htcircontrol/HtcIrData;->frequency:I

    const v1, 0xea60

    if-gt v0, v1, :cond_0

    iget v0, p0, Lcom/htc/htcircontrol/HtcIrData;->frequency:I

    const/16 v1, 0x5dc0

    if-ge v0, v1, :cond_1

    .line 132
    :cond_0
    const-string/jumbo v0, "HtcIrData"

    const-string/jumbo v1, "argument freq is invalid"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "freq must be in range of 24000 - 60000"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_1
    return-void
.end method

.method public setPeriod(I)V
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/htc/htcircontrol/HtcIrData;->period:I

    .line 186
    return-void
.end method

.method public setPeriodTolerance(I)V
    .locals 2

    .prologue
    .line 202
    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-gt p1, v0, :cond_0

    .line 203
    iput p1, p0, Lcom/htc/htcircontrol/HtcIrData;->periodTolerance:I

    .line 208
    return-void

    .line 206
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "period tolerance should be in range 0 ~ 100"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setRepeatCount(I)V
    .locals 2

    .prologue
    .line 111
    const/16 v0, 0xff

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    if-ge p1, v0, :cond_1

    .line 112
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "repeat count must be in range of 1 - 255"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_1
    iput p1, p0, Lcom/htc/htcircontrol/HtcIrData;->repeatCount:I

    .line 115
    return-void
.end method
