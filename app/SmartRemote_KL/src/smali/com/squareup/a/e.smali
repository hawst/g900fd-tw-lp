.class Lcom/squareup/a/e;
.super Ljava/io/FilterOutputStream;


# instance fields
.field final synthetic a:Lcom/squareup/a/b;

.field final synthetic b:Lcom/squareup/a/a/h;

.field final synthetic c:Lcom/squareup/a/d;


# direct methods
.method constructor <init>(Lcom/squareup/a/d;Ljava/io/OutputStream;Lcom/squareup/a/b;Lcom/squareup/a/a/h;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/squareup/a/e;->c:Lcom/squareup/a/d;

    iput-object p3, p0, Lcom/squareup/a/e;->a:Lcom/squareup/a/b;

    iput-object p4, p0, Lcom/squareup/a/e;->b:Lcom/squareup/a/a/h;

    invoke-direct {p0, p2}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 320
    iget-object v0, p0, Lcom/squareup/a/e;->c:Lcom/squareup/a/d;

    iget-object v1, v0, Lcom/squareup/a/d;->a:Lcom/squareup/a/b;

    monitor-enter v1

    .line 321
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/e;->c:Lcom/squareup/a/d;

    invoke-static {v0}, Lcom/squareup/a/d;->a(Lcom/squareup/a/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    monitor-exit v1

    .line 329
    :goto_0
    return-void

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/e;->c:Lcom/squareup/a/d;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/squareup/a/d;->a(Lcom/squareup/a/d;Z)Z

    .line 325
    iget-object v0, p0, Lcom/squareup/a/e;->c:Lcom/squareup/a/d;

    iget-object v0, v0, Lcom/squareup/a/d;->a:Lcom/squareup/a/b;

    invoke-static {v0}, Lcom/squareup/a/b;->a(Lcom/squareup/a/b;)I

    .line 326
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    invoke-super {p0}, Ljava/io/FilterOutputStream;->close()V

    .line 328
    iget-object v0, p0, Lcom/squareup/a/e;->b:Lcom/squareup/a/a/h;

    invoke-virtual {v0}, Lcom/squareup/a/a/h;->a()V

    goto :goto_0

    .line 326
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public write([BII)V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/squareup/a/e;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 335
    return-void
.end method
