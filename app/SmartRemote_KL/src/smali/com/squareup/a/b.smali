.class public Lcom/squareup/a/b;
.super Ljava/net/ResponseCache;

# interfaces
.implements Lcom/squareup/a/t;


# instance fields
.field private final a:Lcom/squareup/a/a/e;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Ljava/io/File;J)V
    .locals 2

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/net/ResponseCache;-><init>()V

    .line 128
    const v0, 0x31191

    const/4 v1, 0x2

    invoke-static {p1, v0, v1, p2, p3}, Lcom/squareup/a/a/e;->a(Ljava/io/File;IIJ)Lcom/squareup/a/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/b;->a:Lcom/squareup/a/a/e;

    .line 129
    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/b/c;)I
    .locals 1

    .prologue
    .line 111
    invoke-static {p0}, Lcom/squareup/a/b;->b(Lcom/squareup/a/a/b/c;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/squareup/a/b;)I
    .locals 2

    .prologue
    .line 111
    iget v0, p0, Lcom/squareup/a/b;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/squareup/a/b;->b:I

    return v0
.end method

.method private a(Lcom/squareup/a/a/h;)V
    .locals 1

    .prologue
    .line 231
    if-eqz p1, :cond_0

    .line 232
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/a/a/h;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 234
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static b(Lcom/squareup/a/a/b/c;)I
    .locals 4

    .prologue
    .line 543
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 545
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 546
    :catch_0
    move-exception v1

    .line 547
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Expected an integer but was \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static synthetic b(Lcom/squareup/a/b;)I
    .locals 2

    .prologue
    .line 111
    iget v0, p0, Lcom/squareup/a/b;->c:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/squareup/a/b;->c:I

    return v0
.end method

.method private static c(Lcom/squareup/a/a/a/ac;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/squareup/a/a/a/ac;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/t;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/ai;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 145
    invoke-static {p1}, Lcom/squareup/a/b;->c(Lcom/squareup/a/a/a/ac;)Ljava/lang/String;

    move-result-object v1

    .line 149
    :try_start_0
    iget-object v2, p0, Lcom/squareup/a/b;->a:Lcom/squareup/a/a/e;

    invoke-virtual {v2, v1}, Lcom/squareup/a/a/e;->a(Ljava/lang/String;)Lcom/squareup/a/a/k;

    move-result-object v1

    .line 150
    if-nez v1, :cond_0

    .line 166
    :goto_0
    return-object v0

    .line 153
    :cond_0
    new-instance v2, Lcom/squareup/a/h;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/squareup/a/a/k;->a(I)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/a/h;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    invoke-virtual {v2, p1, v1}, Lcom/squareup/a/h;->a(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/k;)Lcom/squareup/a/a/a/ai;

    move-result-object v1

    .line 161
    invoke-virtual {v2, p1, v1}, Lcom/squareup/a/h;->a(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 162
    invoke-virtual {v1}, Lcom/squareup/a/a/a/ai;->h()Lcom/squareup/a/a/a/ak;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/a/a/t;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 166
    goto :goto_0

    .line 154
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public a(Lcom/squareup/a/a/a/ai;)Ljava/net/CacheRequest;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 170
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/a/a/a/ac;->d()Ljava/lang/String;

    move-result-object v1

    .line 172
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/squareup/a/b;->b(Lcom/squareup/a/a/a/ac;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-object v0

    .line 175
    :cond_1
    const-string/jumbo v2, "GET"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 182
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 186
    new-instance v1, Lcom/squareup/a/h;

    invoke-direct {v1, p1}, Lcom/squareup/a/h;-><init>(Lcom/squareup/a/a/a/ai;)V

    .line 189
    :try_start_0
    iget-object v2, p0, Lcom/squareup/a/b;->a:Lcom/squareup/a/a/e;

    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/a/b;->c(Lcom/squareup/a/a/a/ac;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/a/a/e;->b(Ljava/lang/String;)Lcom/squareup/a/a/h;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 190
    if-eqz v2, :cond_0

    .line 193
    :try_start_1
    invoke-virtual {v1, v2}, Lcom/squareup/a/h;->a(Lcom/squareup/a/a/h;)V

    .line 194
    new-instance v1, Lcom/squareup/a/d;

    invoke-direct {v1, p0, v2}, Lcom/squareup/a/d;-><init>(Lcom/squareup/a/b;Lcom/squareup/a/a/h;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    goto :goto_0

    .line 195
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 196
    :goto_1
    invoke-direct {p0, v1}, Lcom/squareup/a/b;->a(Lcom/squareup/a/a/h;)V

    goto :goto_0

    .line 195
    :catch_1
    move-exception v1

    move-object v1, v2

    goto :goto_1
.end method

.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 294
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/squareup/a/b;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/a/b;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    monitor-exit p0

    return-void

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/squareup/a/a/a/ai;Lcom/squareup/a/a/a/ai;)V
    .locals 3

    .prologue
    .line 214
    new-instance v1, Lcom/squareup/a/h;

    invoke-direct {v1, p2}, Lcom/squareup/a/h;-><init>(Lcom/squareup/a/a/a/ai;)V

    .line 215
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->h()Lcom/squareup/a/a/a/ak;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/f;

    invoke-static {v0}, Lcom/squareup/a/f;->a(Lcom/squareup/a/f;)Lcom/squareup/a/a/k;

    move-result-object v2

    .line 216
    const/4 v0, 0x0

    .line 218
    :try_start_0
    invoke-virtual {v2}, Lcom/squareup/a/a/k;->a()Lcom/squareup/a/a/h;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {v1, v0}, Lcom/squareup/a/h;->a(Lcom/squareup/a/a/h;)V

    .line 221
    invoke-virtual {v0}, Lcom/squareup/a/a/h;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 223
    :catch_0
    move-exception v1

    .line 224
    invoke-direct {p0, v0}, Lcom/squareup/a/b;->a(Lcom/squareup/a/a/h;)V

    goto :goto_0
.end method

.method public declared-synchronized a(Lcom/squareup/a/w;)V
    .locals 2

    .prologue
    .line 280
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/squareup/a/b;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/a/b;->f:I

    .line 282
    sget-object v0, Lcom/squareup/a/c;->a:[I

    invoke-virtual {p1}, Lcom/squareup/a/w;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 291
    :goto_0
    monitor-exit p0

    return-void

    .line 284
    :pswitch_0
    :try_start_1
    iget v0, p0, Lcom/squareup/a/b;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/a/b;->e:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 280
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 288
    :pswitch_1
    :try_start_2
    iget v0, p0, Lcom/squareup/a/b;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/a/b;->d:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 282
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public b(Lcom/squareup/a/a/a/ac;)Z
    .locals 2

    .prologue
    .line 202
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ac;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/a/v;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/b;->a:Lcom/squareup/a/a/e;

    invoke-static {p1}, Lcom/squareup/a/b;->c(Lcom/squareup/a/a/a/ac;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/e;->c(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :goto_0
    const/4 v0, 0x1

    .line 210
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 205
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public get(Ljava/net/URI;Ljava/lang/String;Ljava/util/Map;)Ljava/net/CacheResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/net/CacheResponse;"
        }
    .end annotation

    .prologue
    .line 133
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This is not a general purpose response cache."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public put(Ljava/net/URI;Ljava/net/URLConnection;)Ljava/net/CacheRequest;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This is not a general purpose response cache."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
