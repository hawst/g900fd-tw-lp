.class public final Lcom/squareup/a/j;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Lcom/squareup/a/k;

.field private final b:Lcom/squareup/a/x;

.field private c:Ljava/net/Socket;

.field private d:Ljava/io/InputStream;

.field private e:Ljava/io/OutputStream;

.field private f:Lcom/squareup/a/a/b/c;

.field private g:Lcom/squareup/a/a/b/b;

.field private h:Z

.field private i:Lcom/squareup/a/a/a/k;

.field private j:Lcom/squareup/a/a/c/ac;

.field private k:I

.field private l:J

.field private m:Lcom/squareup/a/m;

.field private n:I

.field private o:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/squareup/a/k;Lcom/squareup/a/x;)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/squareup/a/j;->h:Z

    .line 83
    const/4 v0, 0x1

    iput v0, p0, Lcom/squareup/a/j;->k:I

    .line 96
    iput-object p1, p0, Lcom/squareup/a/j;->a:Lcom/squareup/a/k;

    .line 97
    iput-object p2, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    .line 98
    return-void
.end method

.method private a(Lcom/squareup/a/z;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 174
    invoke-static {}, Lcom/squareup/a/a/o;->a()Lcom/squareup/a/a/o;

    move-result-object v5

    .line 177
    invoke-virtual {p0}, Lcom/squareup/a/j;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    invoke-direct {p0, p1}, Lcom/squareup/a/j;->b(Lcom/squareup/a/z;)V

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v0, v0, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v0, v0, Lcom/squareup/a/a;->d:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v1, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    iget-object v2, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v2, v2, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v2, v2, Lcom/squareup/a/a;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v6, v6, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget v6, v6, Lcom/squareup/a/a;->c:I

    invoke-virtual {v0, v1, v2, v6, v3}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    .line 184
    iget-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    .line 185
    iget-object v1, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-boolean v1, v1, Lcom/squareup/a/x;->d:Z

    if-eqz v1, :cond_3

    .line 186
    iget-object v1, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v1, v1, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v1, v1, Lcom/squareup/a/a;->b:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Lcom/squareup/a/a/o;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V

    .line 191
    :goto_0
    iget-object v1, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-boolean v1, v1, Lcom/squareup/a/x;->d:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v1, v1, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v1, v1, Lcom/squareup/a/a;->g:Ljava/util/List;

    sget-object v2, Lcom/squareup/a/v;->a:Lcom/squareup/a/v;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v1, v1, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v1, v1, Lcom/squareup/a/a;->g:Ljava/util/List;

    sget-object v2, Lcom/squareup/a/v;->b:Lcom/squareup/a/v;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    move v2, v3

    .line 196
    :goto_1
    if-eqz v2, :cond_2

    .line 197
    iget-object v1, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v1, v1, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v1, v1, Lcom/squareup/a/a;->g:Ljava/util/List;

    sget-object v6, Lcom/squareup/a/v;->a:Lcom/squareup/a/v;

    invoke-interface {v1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v1, v1, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v1, v1, Lcom/squareup/a/a;->g:Ljava/util/List;

    sget-object v6, Lcom/squareup/a/v;->b:Lcom/squareup/a/v;

    invoke-interface {v1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 199
    sget-object v1, Lcom/squareup/a/a/t;->f:Ljava/util/List;

    invoke-virtual {v5, v0, v1}, Lcom/squareup/a/a/o;->a(Ljavax/net/ssl/SSLSocket;Ljava/util/List;)V

    .line 208
    :cond_2
    :goto_2
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    .line 211
    iget-object v1, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v1, v1, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v1, v1, Lcom/squareup/a/a;->e:Ljavax/net/ssl/HostnameVerifier;

    iget-object v6, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v6, v6, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v6, v6, Lcom/squareup/a/a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v7

    invoke-interface {v1, v6, v7}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 212
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Hostname \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v2, v2, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v2, v2, Lcom/squareup/a/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' was not verified"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_3
    invoke-virtual {v5, v0}, Lcom/squareup/a/a/o;->a(Ljavax/net/ssl/SSLSocket;)V

    goto/16 :goto_0

    :cond_4
    move v2, v4

    .line 191
    goto :goto_1

    .line 200
    :cond_5
    iget-object v1, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v1, v1, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v1, v1, Lcom/squareup/a/a;->g:Ljava/util/List;

    sget-object v6, Lcom/squareup/a/v;->a:Lcom/squareup/a/v;

    invoke-interface {v1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 201
    sget-object v1, Lcom/squareup/a/a/t;->h:Ljava/util/List;

    invoke-virtual {v5, v0, v1}, Lcom/squareup/a/a/o;->a(Ljavax/net/ssl/SSLSocket;Ljava/util/List;)V

    goto :goto_2

    .line 203
    :cond_6
    sget-object v1, Lcom/squareup/a/a/t;->g:Ljava/util/List;

    invoke-virtual {v5, v0, v1}, Lcom/squareup/a/a/o;->a(Ljavax/net/ssl/SSLSocket;Ljava/util/List;)V

    goto :goto_2

    .line 215
    :cond_7
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/j;->e:Ljava/io/OutputStream;

    .line 216
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/j;->d:Ljava/io/InputStream;

    .line 217
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/a/m;->a(Ljavax/net/ssl/SSLSession;)Lcom/squareup/a/m;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/j;->m:Lcom/squareup/a/m;

    .line 218
    invoke-direct {p0}, Lcom/squareup/a/j;->q()V

    .line 221
    sget-object v1, Lcom/squareup/a/v;->c:Lcom/squareup/a/v;

    .line 222
    if-eqz v2, :cond_8

    invoke-virtual {v5, v0}, Lcom/squareup/a/a/o;->b(Ljavax/net/ssl/SSLSocket;)Lcom/squareup/a/a/b/d;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 223
    invoke-static {v2}, Lcom/squareup/a/a/t;->a(Lcom/squareup/a/a/b/d;)Lcom/squareup/a/v;

    move-result-object v1

    .line 226
    :cond_8
    iget-boolean v2, v1, Lcom/squareup/a/v;->e:Z

    if-eqz v2, :cond_9

    .line 227
    invoke-virtual {v0, v4}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 228
    new-instance v0, Lcom/squareup/a/a/c/ak;

    iget-object v2, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v2, v2, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    invoke-virtual {v2}, Lcom/squareup/a/a;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/squareup/a/j;->f:Lcom/squareup/a/a/b/c;

    iget-object v5, p0, Lcom/squareup/a/j;->g:Lcom/squareup/a/a/b/b;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/squareup/a/a/c/ak;-><init>(Ljava/lang/String;ZLcom/squareup/a/a/b/c;Lcom/squareup/a/a/b/b;)V

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/c/ak;->a(Lcom/squareup/a/v;)Lcom/squareup/a/a/c/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/c/ak;->a()Lcom/squareup/a/a/c/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/j;->j:Lcom/squareup/a/a/c/ac;

    .line 230
    iget-object v0, p0, Lcom/squareup/a/j;->j:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/c/ac;->e()V

    .line 234
    :goto_3
    return-void

    .line 232
    :cond_9
    new-instance v0, Lcom/squareup/a/a/a/k;

    iget-object v1, p0, Lcom/squareup/a/j;->a:Lcom/squareup/a/k;

    iget-object v2, p0, Lcom/squareup/a/j;->f:Lcom/squareup/a/a/b/c;

    iget-object v3, p0, Lcom/squareup/a/j;->g:Lcom/squareup/a/a/b/b;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/squareup/a/a/a/k;-><init>(Lcom/squareup/a/k;Lcom/squareup/a/j;Lcom/squareup/a/a/b/c;Lcom/squareup/a/a/b/b;)V

    iput-object v0, p0, Lcom/squareup/a/j;->i:Lcom/squareup/a/a/a/k;

    goto :goto_3
.end method

.method private b(Lcom/squareup/a/z;)V
    .locals 6

    .prologue
    .line 383
    iget-object v0, p0, Lcom/squareup/a/j;->d:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/squareup/a/a/b/m;->a(Ljava/io/InputStream;)Lcom/squareup/a/a/b/w;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/w;)Lcom/squareup/a/a/b/c;

    move-result-object v1

    .line 384
    iget-object v0, p0, Lcom/squareup/a/j;->e:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/squareup/a/a/b/m;->a(Ljava/io/OutputStream;)Lcom/squareup/a/a/b/v;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/v;)Lcom/squareup/a/a/b/b;

    move-result-object v0

    .line 385
    new-instance v2, Lcom/squareup/a/a/a/k;

    iget-object v3, p0, Lcom/squareup/a/j;->a:Lcom/squareup/a/k;

    invoke-direct {v2, v3, p0, v1, v0}, Lcom/squareup/a/a/a/k;-><init>(Lcom/squareup/a/k;Lcom/squareup/a/j;Lcom/squareup/a/a/b/c;Lcom/squareup/a/a/b/b;)V

    .line 386
    invoke-virtual {p1}, Lcom/squareup/a/z;->b()Lcom/squareup/a/a/a/ac;

    move-result-object v0

    .line 387
    invoke-virtual {p1}, Lcom/squareup/a/z;->a()Ljava/lang/String;

    move-result-object v3

    .line 389
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->e()Lcom/squareup/a/a/a/f;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/f;Ljava/lang/String;)V

    .line 390
    invoke-virtual {v2}, Lcom/squareup/a/a/a/k;->d()V

    .line 391
    invoke-virtual {v2}, Lcom/squareup/a/a/a/k;->e()Lcom/squareup/a/a/a/al;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/al;->a()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    .line 392
    invoke-virtual {v2}, Lcom/squareup/a/a/a/k;->g()V

    .line 394
    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->c()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 410
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unexpected response code for CONNECT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->c()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 398
    :sswitch_0
    invoke-interface {v1}, Lcom/squareup/a/a/b/c;->b()Lcom/squareup/a/a/b/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 399
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "TLS tunnel buffered too many bytes!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 404
    :sswitch_1
    iget-object v4, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v4, v4, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v4, v4, Lcom/squareup/a/a;->f:Lcom/squareup/a/o;

    iget-object v5, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v5, v5, Lcom/squareup/a/x;->b:Ljava/net/Proxy;

    invoke-static {v4, v0, v5}, Lcom/squareup/a/a/a/i;->a(Lcom/squareup/a/o;Lcom/squareup/a/a/a/ai;Ljava/net/Proxy;)Lcom/squareup/a/a/a/ac;

    move-result-object v0

    .line 406
    if-nez v0, :cond_0

    .line 407
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "Failed to authenticate with proxy"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 401
    :cond_1
    return-void

    .line 394
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x197 -> :sswitch_1
    .end sparse-switch
.end method

.method private q()V
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/squareup/a/j;->d:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/squareup/a/a/b/m;->a(Ljava/io/InputStream;)Lcom/squareup/a/a/b/w;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/w;)Lcom/squareup/a/a/b/c;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/j;->f:Lcom/squareup/a/a/b/c;

    .line 418
    iget-object v0, p0, Lcom/squareup/a/j;->e:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/squareup/a/a/b/m;->a(Ljava/io/OutputStream;)Lcom/squareup/a/a/b/v;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/v;)Lcom/squareup/a/a/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/j;->g:Lcom/squareup/a/a/b/b;

    .line 419
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 101
    iget-object v1, p0, Lcom/squareup/a/j;->a:Lcom/squareup/a/k;

    monitor-enter v1

    .line 102
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/j;->o:Ljava/lang/Object;

    monitor-exit v1

    return-object v0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/squareup/a/a/a/u;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/squareup/a/j;->j:Lcom/squareup/a/a/c/ac;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/a/a/a/at;

    iget-object v1, p0, Lcom/squareup/a/j;->j:Lcom/squareup/a/a/c/ac;

    invoke-direct {v0, p1, v1}, Lcom/squareup/a/a/a/at;-><init>(Lcom/squareup/a/a/a/u;Lcom/squareup/a/a/c/ac;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/a/a/a/w;

    iget-object v1, p0, Lcom/squareup/a/j;->i:Lcom/squareup/a/a/a/k;

    invoke-direct {v0, p1, v1}, Lcom/squareup/a/a/a/w;-><init>(Lcom/squareup/a/a/a/u;Lcom/squareup/a/a/a/k;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 348
    iput p1, p0, Lcom/squareup/a/j;->k:I

    .line 349
    return-void
.end method

.method public a(IILcom/squareup/a/z;)V
    .locals 4

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/squareup/a/j;->h:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v0, v0, Lcom/squareup/a/x;->b:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/net/Socket;

    iget-object v1, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v1, v1, Lcom/squareup/a/x;->b:Ljava/net/Proxy;

    invoke-direct {v0, v1}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    :goto_0
    iput-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    .line 155
    invoke-static {}, Lcom/squareup/a/a/o;->a()Lcom/squareup/a/a/o;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    iget-object v2, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v2, v2, Lcom/squareup/a/x;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/a/a/o;->a(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V

    .line 156
    iget-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v0, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 157
    iget-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/j;->d:Ljava/io/InputStream;

    .line 158
    iget-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/j;->e:Ljava/io/OutputStream;

    .line 160
    iget-object v0, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v0, v0, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v0, v0, Lcom/squareup/a/a;->d:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_2

    .line 161
    invoke-direct {p0, p3}, Lcom/squareup/a/j;->a(Lcom/squareup/a/z;)V

    .line 166
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/a/j;->h:Z

    .line 167
    return-void

    .line 154
    :cond_1
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    goto :goto_0

    .line 163
    :cond_2
    invoke-direct {p0}, Lcom/squareup/a/j;->q()V

    .line 164
    new-instance v0, Lcom/squareup/a/a/a/k;

    iget-object v1, p0, Lcom/squareup/a/j;->a:Lcom/squareup/a/k;

    iget-object v2, p0, Lcom/squareup/a/j;->f:Lcom/squareup/a/a/b/c;

    iget-object v3, p0, Lcom/squareup/a/j;->g:Lcom/squareup/a/a/b/b;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/squareup/a/a/a/k;-><init>(Lcom/squareup/a/k;Lcom/squareup/a/j;Lcom/squareup/a/a/b/c;Lcom/squareup/a/a/b/b;)V

    iput-object v0, p0, Lcom/squareup/a/j;->i:Lcom/squareup/a/a/a/k;

    goto :goto_1
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/squareup/a/j;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/squareup/a/j;->a:Lcom/squareup/a/k;

    monitor-enter v1

    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/j;->o:Ljava/lang/Object;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Connection already has an owner!"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 110
    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/squareup/a/j;->o:Ljava/lang/Object;

    .line 111
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public a(J)Z
    .locals 5

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/squareup/a/j;->j()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 361
    iget-boolean v0, p0, Lcom/squareup/a/j;->h:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "updateReadTimeout - not connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 363
    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/squareup/a/j;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/squareup/a/j;->a:Lcom/squareup/a/k;

    monitor-enter v1

    .line 139
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/j;->o:Ljava/lang/Object;

    if-eq v0, p1, :cond_1

    .line 140
    monitor-exit v1

    .line 148
    :goto_0
    return-void

    .line 143
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/squareup/a/j;->o:Ljava/lang/Object;

    .line 144
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    iget-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    goto :goto_0

    .line 144
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 121
    iget-object v1, p0, Lcom/squareup/a/j;->a:Lcom/squareup/a/k;

    monitor-enter v1

    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/j;->o:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 124
    const/4 v0, 0x0

    monitor-exit v1

    .line 128
    :goto_0
    return v0

    .line 127
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/squareup/a/j;->o:Ljava/lang/Object;

    .line 128
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/squareup/a/j;->h:Z

    return v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    .line 243
    return-void
.end method

.method public d()Lcom/squareup/a/x;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    return-object v0
.end method

.method public e()Ljava/net/Socket;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 269
    iget-object v2, p0, Lcom/squareup/a/j;->f:Lcom/squareup/a/a/b/c;

    if-nez v2, :cond_1

    .line 289
    :cond_0
    :goto_0
    return v0

    .line 272
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/a/j;->l()Z

    move-result v2

    if-nez v2, :cond_0

    .line 276
    :try_start_0
    iget-object v2, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getSoTimeout()I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    .line 278
    :try_start_1
    iget-object v2, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 279
    iget-object v2, p0, Lcom/squareup/a/j;->f:Lcom/squareup/a/a/b/c;

    invoke-interface {v2}, Lcom/squareup/a/a/b/c;->e()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 284
    :try_start_2
    iget-object v2, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    goto :goto_0

    .line 286
    :catch_0
    move-exception v1

    goto :goto_0

    .line 284
    :catchall_0
    move-exception v2

    iget-object v4, p0, Lcom/squareup/a/j;->c:Ljava/net/Socket;

    invoke-virtual {v4, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v2
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 288
    :catch_1
    move-exception v0

    move v0, v1

    .line 289
    goto :goto_0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/squareup/a/j;->j:Lcom/squareup/a/a/c/ac;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "spdyConnection != null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 295
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/a/j;->l:J

    .line 296
    return-void
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/squareup/a/j;->j:Lcom/squareup/a/a/c/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/j;->j:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/c/ac;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()J
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/squareup/a/j;->j:Lcom/squareup/a/a/c/ac;

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/a/j;->l:J

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/a/j;->j:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/c/ac;->c()J

    move-result-wide v0

    goto :goto_0
.end method

.method public k()Lcom/squareup/a/m;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/squareup/a/j;->m:Lcom/squareup/a/m;

    return-object v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/squareup/a/j;->j:Lcom/squareup/a/a/c/ac;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 344
    iget v0, p0, Lcom/squareup/a/j;->k:I

    return v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v0, v0, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v0, v0, Lcom/squareup/a/a;->d:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/j;->b:Lcom/squareup/a/x;

    iget-object v0, v0, Lcom/squareup/a/x;->b:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()V
    .locals 1

    .prologue
    .line 366
    iget v0, p0, Lcom/squareup/a/j;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/a/j;->n:I

    .line 367
    return-void
.end method

.method public p()I
    .locals 1

    .prologue
    .line 374
    iget v0, p0, Lcom/squareup/a/j;->n:I

    return v0
.end method
