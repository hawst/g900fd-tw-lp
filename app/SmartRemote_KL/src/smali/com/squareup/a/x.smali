.class public Lcom/squareup/a/x;
.super Ljava/lang/Object;


# instance fields
.field final a:Lcom/squareup/a/a;

.field final b:Ljava/net/Proxy;

.field final c:Ljava/net/InetSocketAddress;

.field final d:Z


# direct methods
.method public constructor <init>(Lcom/squareup/a/a;Ljava/net/Proxy;Ljava/net/InetSocketAddress;Z)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "address == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "proxy == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "inetSocketAddress == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_2
    iput-object p1, p0, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    .line 48
    iput-object p2, p0, Lcom/squareup/a/x;->b:Ljava/net/Proxy;

    .line 49
    iput-object p3, p0, Lcom/squareup/a/x;->c:Ljava/net/InetSocketAddress;

    .line 50
    iput-boolean p4, p0, Lcom/squareup/a/x;->d:Z

    .line 51
    return-void
.end method


# virtual methods
.method public a()Lcom/squareup/a/a;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    return-object v0
.end method

.method public b()Ljava/net/Proxy;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/squareup/a/x;->b:Ljava/net/Proxy;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 88
    instance-of v1, p1, Lcom/squareup/a/x;

    if-eqz v1, :cond_0

    .line 89
    check-cast p1, Lcom/squareup/a/x;

    .line 90
    iget-object v1, p0, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    iget-object v2, p1, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    invoke-virtual {v1, v2}, Lcom/squareup/a/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/a/x;->b:Ljava/net/Proxy;

    iget-object v2, p1, Lcom/squareup/a/x;->b:Ljava/net/Proxy;

    invoke-virtual {v1, v2}, Ljava/net/Proxy;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/a/x;->c:Ljava/net/InetSocketAddress;

    iget-object v2, p1, Lcom/squareup/a/x;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v1, v2}, Ljava/net/InetSocketAddress;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/squareup/a/x;->d:Z

    iget-boolean v2, p1, Lcom/squareup/a/x;->d:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 95
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 99
    .line 100
    iget-object v0, p0, Lcom/squareup/a/x;->a:Lcom/squareup/a/a;

    invoke-virtual {v0}, Lcom/squareup/a/a;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 101
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/a/x;->b:Ljava/net/Proxy;

    invoke-virtual {v1}, Ljava/net/Proxy;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/a/x;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v1}, Ljava/net/InetSocketAddress;->hashCode()I

    move-result v1

    add-int/2addr v1, v0

    .line 103
    iget-boolean v0, p0, Lcom/squareup/a/x;->d:Z

    if-eqz v0, :cond_0

    mul-int/lit8 v0, v1, 0x1f

    :goto_0
    add-int/2addr v0, v1

    .line 104
    return v0

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
