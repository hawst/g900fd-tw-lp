.class final Lcom/squareup/a/d;
.super Ljava/net/CacheRequest;


# instance fields
.field final synthetic a:Lcom/squareup/a/b;

.field private final b:Lcom/squareup/a/a/h;

.field private c:Ljava/io/OutputStream;

.field private d:Z

.field private e:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Lcom/squareup/a/b;Lcom/squareup/a/a/h;)V
    .locals 2

    .prologue
    .line 315
    iput-object p1, p0, Lcom/squareup/a/d;->a:Lcom/squareup/a/b;

    invoke-direct {p0}, Ljava/net/CacheRequest;-><init>()V

    .line 316
    iput-object p2, p0, Lcom/squareup/a/d;->b:Lcom/squareup/a/a/h;

    .line 317
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/squareup/a/a/h;->a(I)Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/d;->c:Ljava/io/OutputStream;

    .line 318
    new-instance v0, Lcom/squareup/a/e;

    iget-object v1, p0, Lcom/squareup/a/d;->c:Ljava/io/OutputStream;

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/squareup/a/e;-><init>(Lcom/squareup/a/d;Ljava/io/OutputStream;Lcom/squareup/a/b;Lcom/squareup/a/a/h;)V

    iput-object v0, p0, Lcom/squareup/a/d;->e:Ljava/io/OutputStream;

    .line 337
    return-void
.end method

.method static synthetic a(Lcom/squareup/a/d;)Z
    .locals 1

    .prologue
    .line 309
    iget-boolean v0, p0, Lcom/squareup/a/d;->d:Z

    return v0
.end method

.method static synthetic a(Lcom/squareup/a/d;Z)Z
    .locals 0

    .prologue
    .line 309
    iput-boolean p1, p0, Lcom/squareup/a/d;->d:Z

    return p1
.end method


# virtual methods
.method public abort()V
    .locals 2

    .prologue
    .line 340
    iget-object v1, p0, Lcom/squareup/a/d;->a:Lcom/squareup/a/b;

    monitor-enter v1

    .line 341
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/d;->d:Z

    if-eqz v0, :cond_0

    .line 342
    monitor-exit v1

    .line 352
    :goto_0
    return-void

    .line 344
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/a/d;->d:Z

    .line 345
    iget-object v0, p0, Lcom/squareup/a/d;->a:Lcom/squareup/a/b;

    invoke-static {v0}, Lcom/squareup/a/b;->b(Lcom/squareup/a/b;)I

    .line 346
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    iget-object v0, p0, Lcom/squareup/a/d;->c:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/squareup/a/a/t;->a(Ljava/io/Closeable;)V

    .line 349
    :try_start_1
    iget-object v0, p0, Lcom/squareup/a/d;->b:Lcom/squareup/a/a/h;

    invoke-virtual {v0}, Lcom/squareup/a/a/h;->b()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 350
    :catch_0
    move-exception v0

    goto :goto_0

    .line 346
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public getBody()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/squareup/a/d;->e:Ljava/io/OutputStream;

    return-object v0
.end method
