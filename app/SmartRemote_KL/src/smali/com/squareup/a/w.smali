.class public final enum Lcom/squareup/a/w;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/squareup/a/w;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final enum a:Lcom/squareup/a/w;

.field public static final enum b:Lcom/squareup/a/w;

.field public static final enum c:Lcom/squareup/a/w;

.field public static final enum d:Lcom/squareup/a/w;

.field private static final synthetic e:[Lcom/squareup/a/w;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lcom/squareup/a/w;

    const-string/jumbo v1, "CACHE"

    invoke-direct {v0, v1, v2}, Lcom/squareup/a/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/a/w;->a:Lcom/squareup/a/w;

    .line 36
    new-instance v0, Lcom/squareup/a/w;

    const-string/jumbo v1, "CONDITIONAL_CACHE"

    invoke-direct {v0, v1, v3}, Lcom/squareup/a/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/a/w;->b:Lcom/squareup/a/w;

    .line 39
    new-instance v0, Lcom/squareup/a/w;

    const-string/jumbo v1, "NETWORK"

    invoke-direct {v0, v1, v4}, Lcom/squareup/a/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/a/w;->c:Lcom/squareup/a/w;

    .line 46
    new-instance v0, Lcom/squareup/a/w;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v5}, Lcom/squareup/a/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/a/w;->d:Lcom/squareup/a/w;

    .line 25
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/a/w;

    sget-object v1, Lcom/squareup/a/w;->a:Lcom/squareup/a/w;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/a/w;->b:Lcom/squareup/a/w;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/a/w;->c:Lcom/squareup/a/w;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/a/w;->d:Lcom/squareup/a/w;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/a/w;->e:[Lcom/squareup/a/w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/a/w;
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/squareup/a/w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/w;

    return-object v0
.end method

.method public static values()[Lcom/squareup/a/w;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/squareup/a/w;->e:[Lcom/squareup/a/w;

    invoke-virtual {v0}, [Lcom/squareup/a/w;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/a/w;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/squareup/a/w;->b:Lcom/squareup/a/w;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/squareup/a/w;->c:Lcom/squareup/a/w;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/squareup/a/w;->a:Lcom/squareup/a/w;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/squareup/a/w;->b:Lcom/squareup/a/w;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
