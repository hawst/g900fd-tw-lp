.class public final Lcom/squareup/a/z;
.super Ljava/lang/Object;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final b:I

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "host == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "userAgent == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_1
    iput-object p1, p0, Lcom/squareup/a/z;->a:Ljava/lang/String;

    .line 52
    iput p2, p0, Lcom/squareup/a/z;->b:I

    .line 53
    iput-object p3, p0, Lcom/squareup/a/z;->c:Ljava/lang/String;

    .line 54
    iput-object p4, p0, Lcom/squareup/a/z;->d:Ljava/lang/String;

    .line 55
    return-void
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "CONNECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/squareup/a/z;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " HTTP/1.1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method b()Lcom/squareup/a/a/a/ac;
    .locals 6

    .prologue
    .line 67
    new-instance v0, Lcom/squareup/a/a/a/af;

    invoke-direct {v0}, Lcom/squareup/a/a/a/af;-><init>()V

    new-instance v1, Ljava/net/URL;

    const-string/jumbo v2, "https"

    iget-object v3, p0, Lcom/squareup/a/z;->a:Ljava/lang/String;

    iget v4, p0, Lcom/squareup/a/z;->b:I

    const-string/jumbo v5, "/"

    invoke-direct {v1, v2, v3, v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/af;->a(Ljava/net/URL;)Lcom/squareup/a/a/a/af;

    move-result-object v1

    .line 71
    const-string/jumbo v2, "Host"

    iget v0, p0, Lcom/squareup/a/z;->b:I

    const-string/jumbo v3, "https"

    invoke-static {v3}, Lcom/squareup/a/a/t;->a(Ljava/lang/String;)I

    move-result v3

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/squareup/a/z;->a:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    .line 72
    const-string/jumbo v0, "User-Agent"

    iget-object v2, p0, Lcom/squareup/a/z;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    .line 75
    iget-object v0, p0, Lcom/squareup/a/z;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 76
    const-string/jumbo v0, "Proxy-Authorization"

    iget-object v2, p0, Lcom/squareup/a/z;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    .line 81
    :cond_0
    const-string/jumbo v0, "Proxy-Connection"

    const-string/jumbo v2, "Keep-Alive"

    invoke-virtual {v1, v0, v2}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    .line 82
    invoke-virtual {v1}, Lcom/squareup/a/a/a/af;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v0

    return-object v0

    .line 71
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/a/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/squareup/a/z;->b:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
