.class final Lcom/squareup/a/h;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/squareup/a/a/a/f;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/squareup/a/a/a/f;

.field private final f:Lcom/squareup/a/m;


# direct methods
.method public constructor <init>(Lcom/squareup/a/a/a/ai;)V
    .locals 2

    .prologue
    .line 453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/h;->a:Ljava/lang/String;

    .line 455
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->e()Lcom/squareup/a/a/a/f;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->j()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/f;->a(Ljava/util/Set;)Lcom/squareup/a/a/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/h;->b:Lcom/squareup/a/a/a/f;

    .line 456
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/h;->c:Ljava/lang/String;

    .line 457
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/h;->d:Ljava/lang/String;

    .line 458
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/h;->e:Lcom/squareup/a/a/a/f;

    .line 459
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->f()Lcom/squareup/a/m;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/h;->f:Lcom/squareup/a/m;

    .line 460
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418
    :try_start_0
    invoke-static {p1}, Lcom/squareup/a/a/b/m;->a(Ljava/io/InputStream;)Lcom/squareup/a/a/b/w;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/w;)Lcom/squareup/a/a/b/c;

    move-result-object v2

    .line 419
    const/4 v1, 0x1

    invoke-interface {v2, v1}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/h;->a:Ljava/lang/String;

    .line 420
    const/4 v1, 0x1

    invoke-interface {v2, v1}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/h;->c:Ljava/lang/String;

    .line 421
    new-instance v3, Lcom/squareup/a/a/a/h;

    invoke-direct {v3}, Lcom/squareup/a/a/a/h;-><init>()V

    .line 422
    invoke-static {v2}, Lcom/squareup/a/b;->a(Lcom/squareup/a/a/b/c;)I

    move-result v4

    move v1, v0

    .line 423
    :goto_0
    if-ge v1, v4, :cond_0

    .line 424
    const/4 v5, 0x1

    invoke-interface {v2, v5}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/squareup/a/a/a/h;->a(Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    .line 423
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 426
    :cond_0
    invoke-virtual {v3}, Lcom/squareup/a/a/a/h;->a()Lcom/squareup/a/a/a/f;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/h;->b:Lcom/squareup/a/a/a/f;

    .line 428
    const/4 v1, 0x1

    invoke-interface {v2, v1}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/h;->d:Ljava/lang/String;

    .line 429
    new-instance v1, Lcom/squareup/a/a/a/h;

    invoke-direct {v1}, Lcom/squareup/a/a/a/h;-><init>()V

    .line 430
    invoke-static {v2}, Lcom/squareup/a/b;->a(Lcom/squareup/a/a/b/c;)I

    move-result v3

    .line 431
    :goto_1
    if-ge v0, v3, :cond_1

    .line 432
    const/4 v4, 0x1

    invoke-interface {v2, v4}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/squareup/a/a/a/h;->a(Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    .line 431
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 434
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/a/a/a/h;->a()Lcom/squareup/a/a/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/h;->e:Lcom/squareup/a/a/a/f;

    .line 436
    invoke-direct {p0}, Lcom/squareup/a/h;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 437
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 438
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 439
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "expected \"\" but was \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 449
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw v0

    .line 441
    :cond_2
    const/4 v0, 0x1

    :try_start_1
    invoke-interface {v2, v0}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 442
    invoke-direct {p0, v2}, Lcom/squareup/a/h;->a(Lcom/squareup/a/a/b/c;)Ljava/util/List;

    move-result-object v1

    .line 443
    invoke-direct {p0, v2}, Lcom/squareup/a/h;->a(Lcom/squareup/a/a/b/c;)Ljava/util/List;

    move-result-object v2

    .line 444
    invoke-static {v0, v1, v2}, Lcom/squareup/a/m;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/a/m;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/h;->f:Lcom/squareup/a/m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 449
    :goto_2
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 451
    return-void

    .line 446
    :cond_3
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/squareup/a/h;->f:Lcom/squareup/a/m;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private a(Lcom/squareup/a/a/b/c;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/a/a/b/c;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/Certificate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 493
    invoke-static {p1}, Lcom/squareup/a/b;->a(Lcom/squareup/a/a/b/c;)I

    move-result v2

    .line 494
    const/4 v0, -0x1

    if-ne v2, v0, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 504
    :cond_0
    return-object v0

    .line 497
    :cond_1
    :try_start_0
    const-string/jumbo v0, "X.509"

    invoke-static {v0}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v3

    .line 498
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 499
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 500
    const/4 v4, 0x1

    invoke-interface {p1, v4}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    move-result-object v4

    .line 501
    invoke-static {v4}, Lcom/squareup/a/a/b/d;->b(Ljava/lang/String;)Lcom/squareup/a/a/b/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/a/a/b/d;->f()[B

    move-result-object v4

    .line 502
    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-direct {v5, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v3, v5}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 499
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 505
    :catch_0
    move-exception v0

    .line 506
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/security/cert/CertificateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private a(Ljava/io/Writer;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/Writer;",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/Certificate;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 512
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 513
    const/4 v0, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 514
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/Certificate;

    invoke-virtual {v0}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object v0

    .line 515
    invoke-static {v0}, Lcom/squareup/a/a/b/d;->a([B)Lcom/squareup/a/a/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/b/d;->b()Ljava/lang/String;

    move-result-object v0

    .line 516
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 513
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 518
    :catch_0
    move-exception v0

    .line 519
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/security/cert/CertificateEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 521
    :cond_0
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 489
    iget-object v0, p0, Lcom/squareup/a/h;->a:Ljava/lang/String;

    const-string/jumbo v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/k;)Lcom/squareup/a/a/a/ai;
    .locals 4

    .prologue
    .line 530
    iget-object v0, p0, Lcom/squareup/a/h;->e:Lcom/squareup/a/a/a/f;

    const-string/jumbo v1, "Content-Type"

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 531
    iget-object v1, p0, Lcom/squareup/a/h;->e:Lcom/squareup/a/a/a/f;

    const-string/jumbo v2, "Content-Length"

    invoke-virtual {v1, v2}, Lcom/squareup/a/a/a/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 532
    new-instance v2, Lcom/squareup/a/a/a/al;

    invoke-direct {v2}, Lcom/squareup/a/a/a/al;-><init>()V

    invoke-virtual {v2, p1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/al;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/a/h;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/a/a/a/al;->a(Ljava/lang/String;)Lcom/squareup/a/a/a/al;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/a/h;->e:Lcom/squareup/a/a/a/f;

    invoke-virtual {v2, v3}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/f;)Lcom/squareup/a/a/a/al;

    move-result-object v2

    new-instance v3, Lcom/squareup/a/f;

    invoke-direct {v3, p2, v0, v1}, Lcom/squareup/a/f;-><init>(Lcom/squareup/a/a/k;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/ak;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/h;->f:Lcom/squareup/a/m;

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/m;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/al;->a()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/squareup/a/a/h;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0xa

    .line 463
    invoke-virtual {p1, v1}, Lcom/squareup/a/a/h;->a(I)Ljava/io/OutputStream;

    move-result-object v0

    .line 464
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/OutputStreamWriter;

    sget-object v4, Lcom/squareup/a/a/t;->e:Ljava/nio/charset/Charset;

    invoke-direct {v3, v0, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 466
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/a/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 467
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/a/h;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 468
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/a/h;->b:Lcom/squareup/a/a/a/f;

    invoke-virtual {v3}, Lcom/squareup/a/a/a/f;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    move v0, v1

    .line 469
    :goto_0
    iget-object v3, p0, Lcom/squareup/a/h;->b:Lcom/squareup/a/a/a/f;

    invoke-virtual {v3}, Lcom/squareup/a/a/a/f;->a()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 470
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/squareup/a/h;->b:Lcom/squareup/a/a/a/f;

    invoke-virtual {v4, v0}, Lcom/squareup/a/a/a/f;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/a/h;->b:Lcom/squareup/a/a/a/f;

    invoke-virtual {v4, v0}, Lcom/squareup/a/a/a/f;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 469
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 473
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/a/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 474
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/a/h;->e:Lcom/squareup/a/a/a/f;

    invoke-virtual {v3}, Lcom/squareup/a/a/a/f;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 475
    :goto_1
    iget-object v0, p0, Lcom/squareup/a/h;->e:Lcom/squareup/a/a/a/f;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/f;->a()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 476
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/a/h;->e:Lcom/squareup/a/a/a/f;

    invoke-virtual {v3, v1}, Lcom/squareup/a/a/a/f;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, ": "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/a/h;->e:Lcom/squareup/a/a/a/f;

    invoke-virtual {v3, v1}, Lcom/squareup/a/a/a/f;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 475
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 479
    :cond_1
    invoke-direct {p0}, Lcom/squareup/a/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 480
    invoke-virtual {v2, v5}, Ljava/io/Writer;->write(I)V

    .line 481
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/a/h;->f:Lcom/squareup/a/m;

    invoke-virtual {v1}, Lcom/squareup/a/m;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 482
    iget-object v0, p0, Lcom/squareup/a/h;->f:Lcom/squareup/a/m;

    invoke-virtual {v0}, Lcom/squareup/a/m;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lcom/squareup/a/h;->a(Ljava/io/Writer;Ljava/util/List;)V

    .line 483
    iget-object v0, p0, Lcom/squareup/a/h;->f:Lcom/squareup/a/m;

    invoke-virtual {v0}, Lcom/squareup/a/m;->d()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lcom/squareup/a/h;->a(Ljava/io/Writer;Ljava/util/List;)V

    .line 485
    :cond_2
    invoke-virtual {v2}, Ljava/io/Writer;->close()V

    .line 486
    return-void
.end method

.method public a(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;)Z
    .locals 2

    .prologue
    .line 524
    iget-object v0, p0, Lcom/squareup/a/h;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/squareup/a/a/a/ac;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/h;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/squareup/a/a/a/ac;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/h;->b:Lcom/squareup/a/a/a/f;

    invoke-virtual {p2, v0, p1}, Lcom/squareup/a/a/a/ai;->a(Lcom/squareup/a/a/a/f;Lcom/squareup/a/a/a/ac;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
