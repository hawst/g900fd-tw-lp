.class public final enum Lcom/squareup/a/v;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/squareup/a/v;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/squareup/a/v;

.field public static final enum b:Lcom/squareup/a/v;

.field public static final enum c:Lcom/squareup/a/v;

.field private static final synthetic f:[Lcom/squareup/a/v;


# instance fields
.field public final d:Lcom/squareup/a/a/b/d;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final e:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 33
    new-instance v0, Lcom/squareup/a/v;

    const-string/jumbo v1, "HTTP_2"

    const-string/jumbo v2, "HTTP-draft-09/2.0"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/squareup/a/v;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/squareup/a/v;->a:Lcom/squareup/a/v;

    .line 34
    new-instance v0, Lcom/squareup/a/v;

    const-string/jumbo v1, "SPDY_3"

    const-string/jumbo v2, "spdy/3.1"

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/squareup/a/v;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/squareup/a/v;->b:Lcom/squareup/a/v;

    .line 35
    new-instance v0, Lcom/squareup/a/v;

    const-string/jumbo v1, "HTTP_11"

    const-string/jumbo v2, "http/1.1"

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/squareup/a/v;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/squareup/a/v;->c:Lcom/squareup/a/v;

    .line 32
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/a/v;

    sget-object v1, Lcom/squareup/a/v;->a:Lcom/squareup/a/v;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/a/v;->b:Lcom/squareup/a/v;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/a/v;->c:Lcom/squareup/a/v;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/a/v;->f:[Lcom/squareup/a/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    invoke-static {p3}, Lcom/squareup/a/a/b/d;->a(Ljava/lang/String;)Lcom/squareup/a/a/b/d;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/v;->d:Lcom/squareup/a/a/b/d;

    .line 56
    iput-boolean p4, p0, Lcom/squareup/a/v;->e:Z

    .line 57
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/a/v;
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/squareup/a/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/v;

    return-object v0
.end method

.method public static values()[Lcom/squareup/a/v;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/squareup/a/v;->f:[Lcom/squareup/a/v;

    invoke-virtual {v0}, [Lcom/squareup/a/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/a/v;

    return-object v0
.end method
