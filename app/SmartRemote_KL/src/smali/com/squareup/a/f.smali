.class Lcom/squareup/a/f;
.super Lcom/squareup/a/a/a/ak;


# instance fields
.field private final a:Lcom/squareup/a/a/k;

.field private final b:Ljava/io/InputStream;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/a/a/k;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 558
    invoke-direct {p0}, Lcom/squareup/a/a/a/ak;-><init>()V

    .line 559
    iput-object p1, p0, Lcom/squareup/a/f;->a:Lcom/squareup/a/a/k;

    .line 560
    iput-object p2, p0, Lcom/squareup/a/f;->c:Ljava/lang/String;

    .line 561
    iput-object p3, p0, Lcom/squareup/a/f;->d:Ljava/lang/String;

    .line 564
    new-instance v0, Lcom/squareup/a/g;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/squareup/a/a/k;->a(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/a/g;-><init>(Lcom/squareup/a/f;Ljava/io/InputStream;Lcom/squareup/a/a/k;)V

    iput-object v0, p0, Lcom/squareup/a/f;->b:Ljava/io/InputStream;

    .line 570
    return-void
.end method

.method static synthetic a(Lcom/squareup/a/f;)Lcom/squareup/a/a/k;
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Lcom/squareup/a/f;->a:Lcom/squareup/a/a/k;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/squareup/a/f;->b:Ljava/io/InputStream;

    return-object v0
.end method
