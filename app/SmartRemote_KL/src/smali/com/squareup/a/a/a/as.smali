.class public final Lcom/squareup/a/a/a/as;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/squareup/a/a;

.field private final b:Ljava/net/URI;

.field private final c:Ljava/net/ProxySelector;

.field private final d:Lcom/squareup/a/k;

.field private final e:Lcom/squareup/a/a/l;

.field private final f:Lcom/squareup/a/y;

.field private g:Ljava/net/Proxy;

.field private h:Ljava/net/InetSocketAddress;

.field private i:Z

.field private j:Ljava/net/Proxy;

.field private k:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/net/Proxy;",
            ">;"
        }
    .end annotation
.end field

.field private l:[Ljava/net/InetAddress;

.field private m:I

.field private n:I

.field private o:I

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/x;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/a/a;Ljava/net/URI;Ljava/net/ProxySelector;Lcom/squareup/a/k;Lcom/squareup/a/a/l;Lcom/squareup/a/y;)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcom/squareup/a/a/a/as;->o:I

    .line 83
    iput-object p1, p0, Lcom/squareup/a/a/a/as;->a:Lcom/squareup/a/a;

    .line 84
    iput-object p2, p0, Lcom/squareup/a/a/a/as;->b:Ljava/net/URI;

    .line 85
    iput-object p3, p0, Lcom/squareup/a/a/a/as;->c:Ljava/net/ProxySelector;

    .line 86
    iput-object p4, p0, Lcom/squareup/a/a/a/as;->d:Lcom/squareup/a/k;

    .line 87
    iput-object p5, p0, Lcom/squareup/a/a/a/as;->e:Lcom/squareup/a/a/l;

    .line 88
    iput-object p6, p0, Lcom/squareup/a/a/a/as;->f:Lcom/squareup/a/y;

    .line 89
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/a/as;->p:Ljava/util/List;

    .line 91
    invoke-virtual {p1}, Lcom/squareup/a/a;->c()Ljava/net/Proxy;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/squareup/a/a/a/as;->a(Ljava/net/URI;Ljava/net/Proxy;)V

    .line 92
    return-void
.end method

.method private a(Ljava/net/Proxy;)V
    .locals 4

    .prologue
    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/squareup/a/a/a/as;->l:[Ljava/net/InetAddress;

    .line 217
    invoke-virtual {p1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_0

    .line 218
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->b:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 219
    iget-object v1, p0, Lcom/squareup/a/a/a/as;->b:Ljava/net/URI;

    invoke-static {v1}, Lcom/squareup/a/a/t;->a(Ljava/net/URI;)I

    move-result v1

    iput v1, p0, Lcom/squareup/a/a/a/as;->n:I

    .line 232
    :goto_0
    iget-object v1, p0, Lcom/squareup/a/a/a/as;->e:Lcom/squareup/a/a/l;

    invoke-interface {v1, v0}, Lcom/squareup/a/a/l;->a(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/as;->l:[Ljava/net/InetAddress;

    .line 233
    const/4 v0, 0x0

    iput v0, p0, Lcom/squareup/a/a/a/as;->m:I

    .line 234
    return-void

    .line 221
    :cond_0
    invoke-virtual {p1}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v0

    .line 222
    instance-of v1, v0, Ljava/net/InetSocketAddress;

    if-nez v1, :cond_1

    .line 223
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Proxy.address() is not an InetSocketAddress: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 226
    :cond_1
    check-cast v0, Ljava/net/InetSocketAddress;

    .line 227
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    .line 228
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    iput v0, p0, Lcom/squareup/a/a/a/as;->n:I

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/net/URI;Ljava/net/Proxy;)V
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/a/a/a/as;->i:Z

    .line 173
    if-eqz p2, :cond_1

    .line 174
    iput-object p2, p0, Lcom/squareup/a/a/a/as;->j:Ljava/net/Proxy;

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->c:Ljava/net/ProxySelector;

    invoke-virtual {v0, p1}, Ljava/net/ProxySelector;->select(Ljava/net/URI;)Ljava/util/List;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_0

    .line 178
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/as;->k:Ljava/util/Iterator;

    goto :goto_0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/squareup/a/a/a/as;->i:Z

    return v0
.end method

.method private c()Ljava/net/Proxy;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 191
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->j:Ljava/net/Proxy;

    if-eqz v0, :cond_0

    .line 192
    iput-boolean v3, p0, Lcom/squareup/a/a/a/as;->i:Z

    .line 193
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->j:Ljava/net/Proxy;

    .line 209
    :goto_0
    return-object v0

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->k:Ljava/util/Iterator;

    if-eqz v0, :cond_2

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->k:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->k:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/Proxy;

    .line 201
    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v2, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v1, v2, :cond_1

    goto :goto_0

    .line 208
    :cond_2
    iput-boolean v3, p0, Lcom/squareup/a/a/a/as;->i:Z

    .line 209
    sget-object v0, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    goto :goto_0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->l:[Ljava/net/InetAddress;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Ljava/net/InetSocketAddress;
    .locals 4

    .prologue
    .line 243
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Lcom/squareup/a/a/a/as;->l:[Ljava/net/InetAddress;

    iget v2, p0, Lcom/squareup/a/a/a/as;->m:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/squareup/a/a/a/as;->m:I

    aget-object v1, v1, v2

    iget v2, p0, Lcom/squareup/a/a/a/as;->n:I

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 245
    iget v1, p0, Lcom/squareup/a/a/a/as;->m:I

    iget-object v2, p0, Lcom/squareup/a/a/a/as;->l:[Ljava/net/InetAddress;

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 246
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/squareup/a/a/a/as;->l:[Ljava/net/InetAddress;

    .line 247
    const/4 v1, 0x0

    iput v1, p0, Lcom/squareup/a/a/a/as;->m:I

    .line 250
    :cond_0
    return-object v0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->a:Lcom/squareup/a/a;

    invoke-virtual {v0}, Lcom/squareup/a/a;->b()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/squareup/a/a/a/as;->o:I

    .line 256
    return-void

    .line 255
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 260
    iget v0, p0, Lcom/squareup/a/a/a/as;->o:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 265
    iget v2, p0, Lcom/squareup/a/a/a/as;->o:I

    if-ne v2, v0, :cond_0

    .line 266
    iput v1, p0, Lcom/squareup/a/a/a/as;->o:I

    .line 270
    :goto_0
    return v0

    .line 268
    :cond_0
    iget v0, p0, Lcom/squareup/a/a/a/as;->o:I

    if-nez v0, :cond_1

    .line 269
    const/4 v0, -0x1

    iput v0, p0, Lcom/squareup/a/a/a/as;->o:I

    move v0, v1

    .line 270
    goto :goto_0

    .line 272
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/squareup/a/x;
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->p:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/x;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/squareup/a/j;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 109
    :goto_0
    iget-object v1, p0, Lcom/squareup/a/a/a/as;->d:Lcom/squareup/a/k;

    iget-object v2, p0, Lcom/squareup/a/a/a/as;->a:Lcom/squareup/a/a;

    invoke-virtual {v1, v2}, Lcom/squareup/a/k;->a(Lcom/squareup/a/a;)Lcom/squareup/a/j;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 110
    const-string/jumbo v2, "GET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/a/j;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 139
    :goto_1
    return-object v0

    .line 111
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/a/j;->close()V

    goto :goto_0

    .line 115
    :cond_2
    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->g()Z

    move-result v1

    if-nez v1, :cond_6

    .line 116
    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->d()Z

    move-result v1

    if-nez v1, :cond_5

    .line 117
    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->b()Z

    move-result v1

    if-nez v1, :cond_4

    .line 118
    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->i()Z

    move-result v0

    if-nez v0, :cond_3

    .line 119
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 121
    :cond_3
    new-instance v0, Lcom/squareup/a/j;

    iget-object v1, p0, Lcom/squareup/a/a/a/as;->d:Lcom/squareup/a/k;

    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->j()Lcom/squareup/a/x;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/a/j;-><init>(Lcom/squareup/a/k;Lcom/squareup/a/x;)V

    goto :goto_1

    .line 123
    :cond_4
    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->c()Ljava/net/Proxy;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/a/a/as;->g:Ljava/net/Proxy;

    .line 124
    iget-object v1, p0, Lcom/squareup/a/a/a/as;->g:Ljava/net/Proxy;

    invoke-direct {p0, v1}, Lcom/squareup/a/a/a/as;->a(Ljava/net/Proxy;)V

    .line 126
    :cond_5
    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->e()Ljava/net/InetSocketAddress;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/a/a/as;->h:Ljava/net/InetSocketAddress;

    .line 127
    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->f()V

    .line 130
    :cond_6
    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->h()I

    move-result v1

    if-ne v1, v0, :cond_7

    .line 131
    :goto_2
    new-instance v1, Lcom/squareup/a/x;

    iget-object v2, p0, Lcom/squareup/a/a/a/as;->a:Lcom/squareup/a/a;

    iget-object v3, p0, Lcom/squareup/a/a/a/as;->g:Ljava/net/Proxy;

    iget-object v4, p0, Lcom/squareup/a/a/a/as;->h:Ljava/net/InetSocketAddress;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/squareup/a/x;-><init>(Lcom/squareup/a/a;Ljava/net/Proxy;Ljava/net/InetSocketAddress;Z)V

    .line 132
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->f:Lcom/squareup/a/y;

    invoke-virtual {v0, v1}, Lcom/squareup/a/y;->c(Lcom/squareup/a/x;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 133
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-virtual {p0, p1}, Lcom/squareup/a/a/a/as;->a(Ljava/lang/String;)Lcom/squareup/a/j;

    move-result-object v0

    goto :goto_1

    .line 130
    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    .line 139
    :cond_8
    new-instance v0, Lcom/squareup/a/j;

    iget-object v2, p0, Lcom/squareup/a/a/a/as;->d:Lcom/squareup/a/k;

    invoke-direct {v0, v2, v1}, Lcom/squareup/a/j;-><init>(Lcom/squareup/a/k;Lcom/squareup/a/x;)V

    goto :goto_1
.end method

.method public a(Lcom/squareup/a/j;Ljava/io/IOException;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 148
    invoke-virtual {p1}, Lcom/squareup/a/j;->p()I

    move-result v1

    if-lez v1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/a/j;->d()Lcom/squareup/a/x;

    move-result-object v1

    .line 151
    invoke-virtual {v1}, Lcom/squareup/a/x;->b()Ljava/net/Proxy;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v2

    sget-object v3, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/squareup/a/a/a/as;->c:Ljava/net/ProxySelector;

    if-eqz v2, :cond_2

    .line 153
    iget-object v2, p0, Lcom/squareup/a/a/a/as;->c:Ljava/net/ProxySelector;

    iget-object v3, p0, Lcom/squareup/a/a/a/as;->b:Ljava/net/URI;

    invoke-virtual {v1}, Lcom/squareup/a/x;->b()Ljava/net/Proxy;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v4

    invoke-virtual {v2, v3, v4, p2}, Ljava/net/ProxySelector;->connectFailed(Ljava/net/URI;Ljava/net/SocketAddress;Ljava/io/IOException;)V

    .line 156
    :cond_2
    iget-object v2, p0, Lcom/squareup/a/a/a/as;->f:Lcom/squareup/a/y;

    invoke-virtual {v2, v1}, Lcom/squareup/a/y;->a(Lcom/squareup/a/x;)V

    .line 161
    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, p2, Ljavax/net/ssl/SSLHandshakeException;

    if-nez v1, :cond_0

    instance-of v1, p2, Ljavax/net/ssl/SSLProtocolException;

    if-nez v1, :cond_0

    .line 164
    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->h()I

    move-result v1

    if-ne v1, v0, :cond_3

    .line 165
    :goto_1
    new-instance v1, Lcom/squareup/a/x;

    iget-object v2, p0, Lcom/squareup/a/a/a/as;->a:Lcom/squareup/a/a;

    iget-object v3, p0, Lcom/squareup/a/a/a/as;->g:Ljava/net/Proxy;

    iget-object v4, p0, Lcom/squareup/a/a/a/as;->h:Ljava/net/InetSocketAddress;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/squareup/a/x;-><init>(Lcom/squareup/a/a;Ljava/net/Proxy;Ljava/net/InetSocketAddress;Z)V

    .line 166
    iget-object v0, p0, Lcom/squareup/a/a/a/as;->f:Lcom/squareup/a/y;

    invoke-virtual {v0, v1}, Lcom/squareup/a/y;->a(Lcom/squareup/a/x;)V

    goto :goto_0

    .line 164
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/a/a/a/as;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
