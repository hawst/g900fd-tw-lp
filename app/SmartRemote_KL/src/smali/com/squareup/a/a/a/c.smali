.class public Lcom/squareup/a/a/a/c;
.super Ljava/lang/Object;


# instance fields
.field final a:J

.field final b:Lcom/squareup/a/a/a/ac;

.field final c:Lcom/squareup/a/a/a/ai;

.field private d:Ljava/util/Date;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/Date;

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Date;

.field private i:J

.field private j:J

.field private k:Ljava/lang/String;

.field private l:I


# direct methods
.method public constructor <init>(JLcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;)V
    .locals 5

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    const/4 v0, -0x1

    iput v0, p0, Lcom/squareup/a/a/a/c;->l:I

    .line 129
    iput-wide p1, p0, Lcom/squareup/a/a/a/c;->a:J

    .line 130
    iput-object p3, p0, Lcom/squareup/a/a/a/c;->b:Lcom/squareup/a/a/a/ac;

    .line 131
    iput-object p4, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    .line 133
    if-eqz p4, :cond_7

    .line 134
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p4}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/a/a/a/f;->a()I

    move-result v1

    if-ge v0, v1, :cond_7

    .line 135
    invoke-virtual {p4}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/a/a/a/f;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 136
    invoke-virtual {p4}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/squareup/a/a/a/f;->b(I)Ljava/lang/String;

    move-result-object v2

    .line 137
    const-string/jumbo v3, "Date"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 138
    invoke-static {v2}, Lcom/squareup/a/a/a/s;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/a/a/c;->d:Ljava/util/Date;

    .line 139
    iput-object v2, p0, Lcom/squareup/a/a/a/c;->e:Ljava/lang/String;

    .line 134
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_1
    const-string/jumbo v3, "Expires"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 141
    invoke-static {v2}, Lcom/squareup/a/a/a/s;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/a/a/c;->h:Ljava/util/Date;

    goto :goto_1

    .line 142
    :cond_2
    const-string/jumbo v3, "Last-Modified"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 143
    invoke-static {v2}, Lcom/squareup/a/a/a/s;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/a/a/c;->f:Ljava/util/Date;

    .line 144
    iput-object v2, p0, Lcom/squareup/a/a/a/c;->g:Ljava/lang/String;

    goto :goto_1

    .line 145
    :cond_3
    const-string/jumbo v3, "ETag"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 146
    iput-object v2, p0, Lcom/squareup/a/a/a/c;->k:Ljava/lang/String;

    goto :goto_1

    .line 147
    :cond_4
    const-string/jumbo v3, "Age"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 148
    invoke-static {v2}, Lcom/squareup/a/a/a/e;->a(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/squareup/a/a/a/c;->l:I

    goto :goto_1

    .line 149
    :cond_5
    sget-object v3, Lcom/squareup/a/a/a/aa;->b:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 150
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/squareup/a/a/a/c;->i:J

    goto :goto_1

    .line 151
    :cond_6
    sget-object v3, Lcom/squareup/a/a/a/aa;->c:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/squareup/a/a/a/c;->j:J

    goto :goto_1

    .line 156
    :cond_7
    return-void
.end method

.method private static a(Lcom/squareup/a/a/a/ac;)Z
    .locals 1

    .prologue
    .line 313
    const-string/jumbo v0, "If-Modified-Since"

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/a/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "If-None-Match"

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/a/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Lcom/squareup/a/a/a/a;
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v13, -0x1

    const/4 v12, 0x0

    .line 182
    iget-object v0, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    if-nez v0, :cond_0

    .line 183
    new-instance v0, Lcom/squareup/a/a/a/a;

    iget-object v1, p0, Lcom/squareup/a/a/a/c;->b:Lcom/squareup/a/a/a/ac;

    iget-object v2, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    sget-object v3, Lcom/squareup/a/w;->c:Lcom/squareup/a/w;

    invoke-direct {v0, v1, v2, v3, v12}, Lcom/squareup/a/a/a/a;-><init>(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;Lcom/squareup/a/w;Lcom/squareup/a/a/a/b;)V

    .line 250
    :goto_0
    return-object v0

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/c;->b:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->f()Lcom/squareup/a/m;

    move-result-object v0

    if-nez v0, :cond_1

    .line 188
    new-instance v0, Lcom/squareup/a/a/a/a;

    iget-object v1, p0, Lcom/squareup/a/a/a/c;->b:Lcom/squareup/a/a/a/ac;

    iget-object v2, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    sget-object v3, Lcom/squareup/a/w;->c:Lcom/squareup/a/w;

    invoke-direct {v0, v1, v2, v3, v12}, Lcom/squareup/a/a/a/a;-><init>(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;Lcom/squareup/a/w;Lcom/squareup/a/a/a/b;)V

    goto :goto_0

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    iget-object v1, p0, Lcom/squareup/a/a/a/c;->b:Lcom/squareup/a/a/a/ac;

    invoke-static {v0, v1}, Lcom/squareup/a/a/a/a;->a(Lcom/squareup/a/a/a/ai;Lcom/squareup/a/a/a/ac;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 195
    new-instance v0, Lcom/squareup/a/a/a/a;

    iget-object v1, p0, Lcom/squareup/a/a/a/c;->b:Lcom/squareup/a/a/a/ac;

    iget-object v2, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    sget-object v3, Lcom/squareup/a/w;->c:Lcom/squareup/a/w;

    invoke-direct {v0, v1, v2, v3, v12}, Lcom/squareup/a/a/a/a;-><init>(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;Lcom/squareup/a/w;Lcom/squareup/a/a/a/b;)V

    goto :goto_0

    .line 198
    :cond_2
    iget-object v0, p0, Lcom/squareup/a/a/a/c;->b:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->k()Lcom/squareup/a/i;

    move-result-object v6

    .line 199
    invoke-virtual {v6}, Lcom/squareup/a/i;->a()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/a/a/a/c;->b:Lcom/squareup/a/a/a/ac;

    invoke-static {v0}, Lcom/squareup/a/a/a/c;->a(Lcom/squareup/a/a/a/ac;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 200
    :cond_3
    new-instance v0, Lcom/squareup/a/a/a/a;

    iget-object v1, p0, Lcom/squareup/a/a/a/c;->b:Lcom/squareup/a/a/a/ac;

    iget-object v2, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    sget-object v3, Lcom/squareup/a/w;->c:Lcom/squareup/a/w;

    invoke-direct {v0, v1, v2, v3, v12}, Lcom/squareup/a/a/a/a;-><init>(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;Lcom/squareup/a/w;Lcom/squareup/a/a/a/b;)V

    goto :goto_0

    .line 203
    :cond_4
    invoke-direct {p0}, Lcom/squareup/a/a/a/c;->d()J

    move-result-wide v8

    .line 204
    invoke-direct {p0}, Lcom/squareup/a/a/a/c;->c()J

    move-result-wide v0

    .line 206
    invoke-virtual {v6}, Lcom/squareup/a/i;->c()I

    move-result v2

    if-eq v2, v13, :cond_5

    .line 207
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6}, Lcom/squareup/a/i;->c()I

    move-result v3

    int-to-long v10, v3

    invoke-virtual {v2, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 211
    :cond_5
    invoke-virtual {v6}, Lcom/squareup/a/i;->h()I

    move-result v2

    if-eq v2, v13, :cond_e

    .line 212
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6}, Lcom/squareup/a/i;->h()I

    move-result v3

    int-to-long v10, v3

    invoke-virtual {v2, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 216
    :goto_1
    iget-object v7, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v7}, Lcom/squareup/a/a/a/ai;->l()Lcom/squareup/a/i;

    move-result-object v7

    .line 217
    invoke-virtual {v7}, Lcom/squareup/a/i;->f()Z

    move-result v10

    if-nez v10, :cond_6

    invoke-virtual {v6}, Lcom/squareup/a/i;->g()I

    move-result v10

    if-eq v10, v13, :cond_6

    .line 218
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6}, Lcom/squareup/a/i;->g()I

    move-result v5

    int-to-long v10, v5

    invoke-virtual {v4, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 221
    :cond_6
    invoke-virtual {v7}, Lcom/squareup/a/i;->a()Z

    move-result v6

    if-nez v6, :cond_9

    add-long v6, v8, v2

    add-long/2addr v4, v0

    cmp-long v4, v6, v4

    if-gez v4, :cond_9

    .line 222
    iget-object v4, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v4}, Lcom/squareup/a/a/a/ai;->i()Lcom/squareup/a/a/a/al;

    move-result-object v4

    sget-object v5, Lcom/squareup/a/w;->a:Lcom/squareup/a/w;

    invoke-virtual {v4, v5}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/w;)Lcom/squareup/a/a/a/al;

    move-result-object v4

    .line 224
    add-long/2addr v2, v8

    cmp-long v0, v2, v0

    if-ltz v0, :cond_7

    .line 225
    const-string/jumbo v0, "Warning"

    const-string/jumbo v1, "110 HttpURLConnection \"Response is stale\""

    invoke-virtual {v4, v0, v1}, Lcom/squareup/a/a/a/al;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/al;

    .line 227
    :cond_7
    const-wide/32 v0, 0x5265c00

    .line 228
    cmp-long v0, v8, v0

    if-lez v0, :cond_8

    invoke-direct {p0}, Lcom/squareup/a/a/a/c;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 229
    const-string/jumbo v0, "Warning"

    const-string/jumbo v1, "113 HttpURLConnection \"Heuristic expiration\""

    invoke-virtual {v4, v0, v1}, Lcom/squareup/a/a/a/al;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/al;

    .line 231
    :cond_8
    new-instance v0, Lcom/squareup/a/a/a/a;

    iget-object v1, p0, Lcom/squareup/a/a/a/c;->b:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v4}, Lcom/squareup/a/a/a/al;->a()Lcom/squareup/a/a/a/ai;

    move-result-object v2

    sget-object v3, Lcom/squareup/a/w;->a:Lcom/squareup/a/w;

    invoke-direct {v0, v1, v2, v3, v12}, Lcom/squareup/a/a/a/a;-><init>(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;Lcom/squareup/a/w;Lcom/squareup/a/a/a/b;)V

    goto/16 :goto_0

    .line 234
    :cond_9
    iget-object v0, p0, Lcom/squareup/a/a/a/c;->b:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->g()Lcom/squareup/a/a/a/af;

    move-result-object v0

    .line 236
    iget-object v1, p0, Lcom/squareup/a/a/a/c;->f:Ljava/util/Date;

    if-eqz v1, :cond_c

    .line 237
    const-string/jumbo v1, "If-Modified-Since"

    iget-object v2, p0, Lcom/squareup/a/a/a/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    .line 242
    :cond_a
    :goto_2
    iget-object v1, p0, Lcom/squareup/a/a/a/c;->k:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 243
    const-string/jumbo v1, "If-None-Match"

    iget-object v2, p0, Lcom/squareup/a/a/a/c;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    .line 246
    :cond_b
    invoke-virtual {v0}, Lcom/squareup/a/a/a/af;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v2

    .line 247
    invoke-static {v2}, Lcom/squareup/a/a/a/c;->a(Lcom/squareup/a/a/a/ac;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/a/w;->b:Lcom/squareup/a/w;

    .line 250
    :goto_3
    new-instance v1, Lcom/squareup/a/a/a/a;

    iget-object v3, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    invoke-direct {v1, v2, v3, v0, v12}, Lcom/squareup/a/a/a/a;-><init>(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;Lcom/squareup/a/w;Lcom/squareup/a/a/a/b;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 238
    :cond_c
    iget-object v1, p0, Lcom/squareup/a/a/a/c;->d:Ljava/util/Date;

    if-eqz v1, :cond_a

    .line 239
    const-string/jumbo v1, "If-Modified-Since"

    iget-object v2, p0, Lcom/squareup/a/a/a/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    goto :goto_2

    .line 247
    :cond_d
    sget-object v0, Lcom/squareup/a/w;->c:Lcom/squareup/a/w;

    goto :goto_3

    :cond_e
    move-wide v2, v4

    goto/16 :goto_1
.end method

.method private c()J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 258
    iget-object v0, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->l()Lcom/squareup/a/i;

    move-result-object v0

    .line 259
    invoke-virtual {v0}, Lcom/squareup/a/i;->c()I

    move-result v1

    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    .line 260
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Lcom/squareup/a/i;->c()I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 279
    :cond_0
    :goto_0
    return-wide v2

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/c;->h:Ljava/util/Date;

    if-eqz v0, :cond_4

    .line 262
    iget-object v0, p0, Lcom/squareup/a/a/a/c;->d:Ljava/util/Date;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/a/a/a/c;->d:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 265
    :goto_1
    iget-object v4, p0, Lcom/squareup/a/a/a/c;->h:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long v0, v4, v0

    .line 266
    cmp-long v4, v0, v2

    if-lez v4, :cond_3

    :goto_2
    move-wide v2, v0

    goto :goto_0

    .line 262
    :cond_2
    iget-wide v0, p0, Lcom/squareup/a/a/a/c;->j:J

    goto :goto_1

    :cond_3
    move-wide v0, v2

    .line 266
    goto :goto_2

    .line 267
    :cond_4
    iget-object v0, p0, Lcom/squareup/a/a/a/c;->f:Ljava/util/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->a()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/squareup/a/a/a/c;->d:Ljava/util/Date;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/squareup/a/a/a/c;->d:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 276
    :goto_3
    iget-object v4, p0, Lcom/squareup/a/a/a/c;->f:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v0, v4

    .line 277
    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const-wide/16 v2, 0xa

    div-long v2, v0, v2

    goto :goto_0

    .line 273
    :cond_5
    iget-wide v0, p0, Lcom/squareup/a/a/a/c;->i:J

    goto :goto_3
.end method

.method private d()J
    .locals 8

    .prologue
    const-wide/16 v0, 0x0

    .line 287
    iget-object v2, p0, Lcom/squareup/a/a/a/c;->d:Ljava/util/Date;

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/squareup/a/a/a/c;->j:J

    iget-object v4, p0, Lcom/squareup/a/a/a/c;->d:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 290
    :cond_0
    iget v2, p0, Lcom/squareup/a/a/a/c;->l:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v3, p0, Lcom/squareup/a/a/a/c;->l:I

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 293
    :cond_1
    iget-wide v2, p0, Lcom/squareup/a/a/a/c;->j:J

    iget-wide v4, p0, Lcom/squareup/a/a/a/c;->i:J

    sub-long/2addr v2, v4

    .line 294
    iget-wide v4, p0, Lcom/squareup/a/a/a/c;->a:J

    iget-wide v6, p0, Lcom/squareup/a/a/a/c;->j:J

    sub-long/2addr v4, v6

    .line 295
    add-long/2addr v0, v2

    add-long/2addr v0, v4

    return-wide v0
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/squareup/a/a/a/c;->c:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->l()Lcom/squareup/a/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/i;->c()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/a/c;->h:Ljava/util/Date;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/squareup/a/a/a/a;
    .locals 5

    .prologue
    .line 163
    invoke-direct {p0}, Lcom/squareup/a/a/a/c;->b()Lcom/squareup/a/a/a/a;

    move-result-object v1

    .line 165
    iget-object v0, v1, Lcom/squareup/a/a/a/a;->c:Lcom/squareup/a/w;

    sget-object v2, Lcom/squareup/a/w;->a:Lcom/squareup/a/w;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/a/c;->b:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->k()Lcom/squareup/a/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/i;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    new-instance v0, Lcom/squareup/a/a/a/al;

    invoke-direct {v0}, Lcom/squareup/a/a/a/al;-><init>()V

    iget-object v2, v1, Lcom/squareup/a/a/a/a;->a:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0, v2}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    invoke-static {}, Lcom/squareup/a/a/a/a;->b()Lcom/squareup/a/a/a/av;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/av;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    sget-object v2, Lcom/squareup/a/w;->d:Lcom/squareup/a/w;

    invoke-virtual {v0, v2}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/w;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    invoke-static {}, Lcom/squareup/a/a/a/a;->a()Lcom/squareup/a/a/a/ak;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/ak;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/al;->a()Lcom/squareup/a/a/a/ai;

    move-result-object v2

    .line 173
    new-instance v0, Lcom/squareup/a/a/a/a;

    iget-object v1, v1, Lcom/squareup/a/a/a/a;->a:Lcom/squareup/a/a/a/ac;

    sget-object v3, Lcom/squareup/a/w;->d:Lcom/squareup/a/w;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/a/a/a/a;-><init>(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;Lcom/squareup/a/w;Lcom/squareup/a/a/a/b;)V

    .line 176
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
