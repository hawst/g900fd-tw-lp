.class public final Lcom/squareup/a/a/c/ao;
.super Ljava/lang/Object;


# static fields
.field static final synthetic d:Z


# instance fields
.field a:J

.field b:J

.field final c:Lcom/squareup/a/a/c/aq;

.field private final e:I

.field private final f:Lcom/squareup/a/a/c/ac;

.field private final g:I

.field private h:J

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/squareup/a/a/c/ar;

.field private l:Lcom/squareup/a/a/c/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/squareup/a/a/c/ao;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/squareup/a/a/c/ao;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(ILcom/squareup/a/a/c/ac;ZZILjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/a/a/c/ac;",
            "ZZI",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v0, 0x0

    const/4 v4, 0x0

    const/high16 v2, 0x10000

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-wide v0, p0, Lcom/squareup/a/a/c/ao;->a:J

    .line 57
    iput-wide v0, p0, Lcom/squareup/a/a/c/ao;->h:J

    .line 73
    iput-object v4, p0, Lcom/squareup/a/a/c/ao;->l:Lcom/squareup/a/a/c/a;

    .line 77
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "connection == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    if-nez p6, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "requestHeaders == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_1
    iput p1, p0, Lcom/squareup/a/a/c/ao;->e:I

    .line 80
    iput-object p2, p0, Lcom/squareup/a/a/c/ao;->f:Lcom/squareup/a/a/c/ac;

    .line 81
    iget-object v0, p2, Lcom/squareup/a/a/c/ac;->f:Lcom/squareup/a/a/c/y;

    invoke-virtual {v0, v2}, Lcom/squareup/a/a/c/y;->d(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/a/a/c/ao;->b:J

    .line 83
    new-instance v0, Lcom/squareup/a/a/c/ar;

    iget-object v1, p2, Lcom/squareup/a/a/c/ac;->e:Lcom/squareup/a/a/c/y;

    invoke-virtual {v1, v2}, Lcom/squareup/a/a/c/y;->d(I)I

    move-result v1

    int-to-long v2, v1

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/squareup/a/a/c/ar;-><init>(Lcom/squareup/a/a/c/ao;JLcom/squareup/a/a/c/ap;)V

    iput-object v0, p0, Lcom/squareup/a/a/c/ao;->k:Lcom/squareup/a/a/c/ar;

    .line 85
    new-instance v0, Lcom/squareup/a/a/c/aq;

    invoke-direct {v0, p0}, Lcom/squareup/a/a/c/aq;-><init>(Lcom/squareup/a/a/c/ao;)V

    iput-object v0, p0, Lcom/squareup/a/a/c/ao;->c:Lcom/squareup/a/a/c/aq;

    .line 86
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->k:Lcom/squareup/a/a/c/ar;

    invoke-static {v0, p4}, Lcom/squareup/a/a/c/ar;->a(Lcom/squareup/a/a/c/ar;Z)Z

    .line 87
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->c:Lcom/squareup/a/a/c/aq;

    invoke-static {v0, p3}, Lcom/squareup/a/a/c/aq;->a(Lcom/squareup/a/a/c/aq;Z)Z

    .line 88
    iput p5, p0, Lcom/squareup/a/a/c/ao;->g:I

    .line 89
    iput-object p6, p0, Lcom/squareup/a/a/c/ao;->i:Ljava/util/List;

    .line 90
    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->f:Lcom/squareup/a/a/c/ac;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/a/a/c/ao;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/squareup/a/a/c/ao;->e:I

    return v0
.end method

.method static synthetic c(Lcom/squareup/a/a/c/ao;)J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/squareup/a/a/c/ao;->h:J

    return-wide v0
.end method

.method static synthetic d(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/a;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->l:Lcom/squareup/a/a/c/a;

    return-object v0
.end method

.method private d(Lcom/squareup/a/a/c/a;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 259
    sget-boolean v1, Lcom/squareup/a/a/c/ao;->d:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 260
    :cond_0
    monitor-enter p0

    .line 261
    :try_start_0
    iget-object v1, p0, Lcom/squareup/a/a/c/ao;->l:Lcom/squareup/a/a/c/a;

    if-eqz v1, :cond_1

    .line 262
    monitor-exit p0

    .line 271
    :goto_0
    return v0

    .line 264
    :cond_1
    iget-object v1, p0, Lcom/squareup/a/a/c/ao;->k:Lcom/squareup/a/a/c/ar;

    invoke-static {v1}, Lcom/squareup/a/a/c/ar;->a(Lcom/squareup/a/a/c/ar;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/a/a/c/ao;->c:Lcom/squareup/a/a/c/aq;

    invoke-static {v1}, Lcom/squareup/a/a/c/aq;->a(Lcom/squareup/a/a/c/aq;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 265
    monitor-exit p0

    goto :goto_0

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 267
    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/squareup/a/a/c/ao;->l:Lcom/squareup/a/a/c/a;

    .line 268
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 269
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->f:Lcom/squareup/a/a/c/ac;

    iget v1, p0, Lcom/squareup/a/a/c/ao;->e:I

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/c/ac;->b(I)Lcom/squareup/a/a/c/ao;

    .line 271
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic e(Lcom/squareup/a/a/c/ao;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/squareup/a/a/c/ao;->h()V

    return-void
.end method

.method static synthetic f(Lcom/squareup/a/a/c/ao;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/squareup/a/a/c/ao;->i()V

    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 490
    sget-boolean v0, Lcom/squareup/a/a/c/ao;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 493
    :cond_0
    monitor-enter p0

    .line 494
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->k:Lcom/squareup/a/a/c/ar;

    invoke-static {v0}, Lcom/squareup/a/a/c/ar;->a(Lcom/squareup/a/a/c/ar;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->k:Lcom/squareup/a/a/c/ar;

    invoke-static {v0}, Lcom/squareup/a/a/c/ar;->b(Lcom/squareup/a/a/c/ar;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->c:Lcom/squareup/a/a/c/aq;

    invoke-static {v0}, Lcom/squareup/a/a/c/aq;->a(Lcom/squareup/a/a/c/aq;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->c:Lcom/squareup/a/a/c/aq;

    invoke-static {v0}, Lcom/squareup/a/a/c/aq;->b(Lcom/squareup/a/a/c/aq;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 495
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/a/a/c/ao;->a()Z

    move-result v1

    .line 496
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 497
    if-eqz v0, :cond_4

    .line 502
    sget-object v0, Lcom/squareup/a/a/c/a;->l:Lcom/squareup/a/a/c/a;

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/a;)V

    .line 506
    :cond_2
    :goto_1
    return-void

    .line 494
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 496
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 503
    :cond_4
    if-nez v1, :cond_2

    .line 504
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->f:Lcom/squareup/a/a/c/ac;

    iget v1, p0, Lcom/squareup/a/a/c/ao;->e:I

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/c/ac;->b(I)Lcom/squareup/a/a/c/ao;

    goto :goto_1
.end method

.method private i()V
    .locals 3

    .prologue
    .line 583
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->c:Lcom/squareup/a/a/c/aq;

    invoke-static {v0}, Lcom/squareup/a/a/c/aq;->b(Lcom/squareup/a/a/c/aq;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 585
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->c:Lcom/squareup/a/a/c/aq;

    invoke-static {v0}, Lcom/squareup/a/a/c/aq;->a(Lcom/squareup/a/a/c/aq;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 586
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "stream finished"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 587
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->l:Lcom/squareup/a/a/c/a;

    if-eqz v0, :cond_2

    .line 588
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stream was reset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/a/a/c/ao;->l:Lcom/squareup/a/a/c/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 590
    :cond_2
    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 1

    .prologue
    .line 208
    iput-wide p1, p0, Lcom/squareup/a/a/c/ao;->h:J

    .line 209
    return-void
.end method

.method a(Lcom/squareup/a/a/b/c;I)V
    .locals 4

    .prologue
    .line 306
    sget-boolean v0, Lcom/squareup/a/a/c/ao;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->k:Lcom/squareup/a/a/c/ar;

    int-to-long v2, p2

    invoke-virtual {v0, p1, v2, v3}, Lcom/squareup/a/a/c/ar;->a(Lcom/squareup/a/a/b/c;J)V

    .line 308
    return-void
.end method

.method public a(Lcom/squareup/a/a/c/a;)V
    .locals 2

    .prologue
    .line 240
    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/ao;->d(Lcom/squareup/a/a/c/a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 244
    :goto_0
    return-void

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->f:Lcom/squareup/a/a/c/ac;

    iget v1, p0, Lcom/squareup/a/a/c/ao;->e:I

    invoke-virtual {v0, v1, p1}, Lcom/squareup/a/a/c/ac;->b(ILcom/squareup/a/a/c/a;)V

    goto :goto_0
.end method

.method a(Ljava/util/List;Lcom/squareup/a/a/c/f;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;",
            "Lcom/squareup/a/a/c/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 275
    sget-boolean v0, Lcom/squareup/a/a/c/ao;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 276
    :cond_0
    const/4 v1, 0x0

    .line 277
    const/4 v0, 0x1

    .line 278
    monitor-enter p0

    .line 279
    :try_start_0
    iget-object v2, p0, Lcom/squareup/a/a/c/ao;->j:Ljava/util/List;

    if-nez v2, :cond_3

    .line 280
    invoke-virtual {p2}, Lcom/squareup/a/a/c/f;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 281
    sget-object v1, Lcom/squareup/a/a/c/a;->b:Lcom/squareup/a/a/c/a;

    .line 297
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    if-eqz v1, :cond_5

    .line 299
    invoke-virtual {p0, v1}, Lcom/squareup/a/a/c/ao;->b(Lcom/squareup/a/a/c/a;)V

    .line 303
    :cond_1
    :goto_1
    return-void

    .line 283
    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/squareup/a/a/c/ao;->j:Ljava/util/List;

    .line 284
    invoke-virtual {p0}, Lcom/squareup/a/a/c/ao;->a()Z

    move-result v0

    .line 285
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    goto :goto_0

    .line 297
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 288
    :cond_3
    :try_start_2
    invoke-virtual {p2}, Lcom/squareup/a/a/c/f;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 289
    sget-object v1, Lcom/squareup/a/a/c/a;->e:Lcom/squareup/a/a/c/a;

    goto :goto_0

    .line 291
    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 292
    iget-object v3, p0, Lcom/squareup/a/a/c/ao;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 293
    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 294
    iput-object v2, p0, Lcom/squareup/a/a/c/ao;->j:Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 300
    :cond_5
    if-nez v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->f:Lcom/squareup/a/a/c/ac;

    iget v1, p0, Lcom/squareup/a/a/c/ao;->e:I

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/c/ac;->b(I)Lcom/squareup/a/a/c/ao;

    goto :goto_1
.end method

.method public declared-synchronized a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 107
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/squareup/a/a/c/ao;->l:Lcom/squareup/a/a/c/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 115
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 110
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/squareup/a/a/c/ao;->k:Lcom/squareup/a/a/c/ar;

    invoke-static {v1}, Lcom/squareup/a/a/c/ar;->a(Lcom/squareup/a/a/c/ar;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/squareup/a/a/c/ao;->k:Lcom/squareup/a/a/c/ar;

    invoke-static {v1}, Lcom/squareup/a/a/c/ar;->b(Lcom/squareup/a/a/c/ar;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/squareup/a/a/c/ao;->c:Lcom/squareup/a/a/c/aq;

    invoke-static {v1}, Lcom/squareup/a/a/c/aq;->a(Lcom/squareup/a/a/c/aq;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/squareup/a/a/c/ao;->c:Lcom/squareup/a/a/c/aq;

    invoke-static {v1}, Lcom/squareup/a/a/c/aq;->b(Lcom/squareup/a/a/c/aq;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/squareup/a/a/c/ao;->j:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    .line 115
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method b(J)V
    .locals 3

    .prologue
    .line 578
    iget-wide v0, p0, Lcom/squareup/a/a/c/ao;->b:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/squareup/a/a/c/ao;->b:J

    .line 579
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 580
    :cond_0
    return-void
.end method

.method public b(Lcom/squareup/a/a/c/a;)V
    .locals 2

    .prologue
    .line 251
    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/ao;->d(Lcom/squareup/a/a/c/a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->f:Lcom/squareup/a/a/c/ac;

    iget v1, p0, Lcom/squareup/a/a/c/ao;->e:I

    invoke-virtual {v0, v1, p1}, Lcom/squareup/a/a/c/ac;->a(ILcom/squareup/a/a/c/a;)V

    goto :goto_0
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 120
    iget v0, p0, Lcom/squareup/a/a/c/ao;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 121
    :goto_0
    iget-object v3, p0, Lcom/squareup/a/a/c/ao;->f:Lcom/squareup/a/a/c/ac;

    iget-boolean v3, v3, Lcom/squareup/a/a/c/ac;->b:Z

    if-ne v3, v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 120
    goto :goto_0

    :cond_1
    move v1, v2

    .line 121
    goto :goto_1
.end method

.method public declared-synchronized c()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 137
    monitor-enter p0

    .line 139
    :try_start_0
    iget-wide v0, p0, Lcom/squareup/a/a/c/ao;->h:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    .line 140
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/32 v4, 0xf4240

    div-long/2addr v0, v4

    .line 141
    iget-wide v4, p0, Lcom/squareup/a/a/c/ao;->h:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :goto_0
    :try_start_1
    iget-object v6, p0, Lcom/squareup/a/a/c/ao;->j:Ljava/util/List;

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/squareup/a/a/c/ao;->l:Lcom/squareup/a/a/c/a;

    if-nez v6, :cond_2

    .line 145
    iget-wide v6, p0, Lcom/squareup/a/a/c/ao;->h:J

    cmp-long v6, v6, v2

    if-nez v6, :cond_0

    .line 146
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 159
    :catch_0
    move-exception v0

    .line 160
    :try_start_2
    new-instance v1, Ljava/io/InterruptedIOException;

    invoke-direct {v1}, Ljava/io/InterruptedIOException;-><init>()V

    .line 161
    invoke-virtual {v1, v0}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 162
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 147
    :cond_0
    cmp-long v6, v4, v2

    if-lez v6, :cond_1

    .line 148
    :try_start_3
    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 149
    iget-wide v4, p0, Lcom/squareup/a/a/c/ao;->h:J

    add-long/2addr v4, v0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    const-wide/32 v8, 0xf4240

    div-long/2addr v6, v8

    sub-long/2addr v4, v6

    goto :goto_0

    .line 151
    :cond_1
    new-instance v0, Ljava/net/SocketTimeoutException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Read response header timeout. readTimeoutMillis: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/squareup/a/a/c/ao;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_2
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->j:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 156
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->j:Ljava/util/List;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0

    .line 158
    :cond_3
    :try_start_4
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stream was reset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/a/a/c/ao;->l:Lcom/squareup/a/a/c/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_4
    move-wide v0, v2

    move-wide v4, v2

    goto :goto_0
.end method

.method declared-synchronized c(Lcom/squareup/a/a/c/a;)V
    .locals 1

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->l:Lcom/squareup/a/a/c/a;

    if-nez v0, :cond_0

    .line 325
    iput-object p1, p0, Lcom/squareup/a/a/c/ao;->l:Lcom/squareup/a/a/c/a;

    .line 326
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    :cond_0
    monitor-exit p0

    return-void

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 212
    iget-wide v0, p0, Lcom/squareup/a/a/c/ao;->h:J

    return-wide v0
.end method

.method public e()Lcom/squareup/a/a/b/w;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->k:Lcom/squareup/a/a/c/ar;

    return-object v0
.end method

.method public f()Lcom/squareup/a/a/b/v;
    .locals 2

    .prologue
    .line 227
    monitor-enter p0

    .line 228
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->j:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/a/a/c/ao;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "reply before requesting the sink"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->c:Lcom/squareup/a/a/c/aq;

    return-object v0
.end method

.method g()V
    .locals 2

    .prologue
    .line 311
    sget-boolean v0, Lcom/squareup/a/a/c/ao;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 313
    :cond_0
    monitor-enter p0

    .line 314
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->k:Lcom/squareup/a/a/c/ar;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/ar;->a(Lcom/squareup/a/a/c/ar;Z)Z

    .line 315
    invoke-virtual {p0}, Lcom/squareup/a/a/c/ao;->a()Z

    move-result v0

    .line 316
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 317
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318
    if-nez v0, :cond_1

    .line 319
    iget-object v0, p0, Lcom/squareup/a/a/c/ao;->f:Lcom/squareup/a/a/c/ac;

    iget v1, p0, Lcom/squareup/a/a/c/ao;->e:I

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/c/ac;->b(I)Lcom/squareup/a/a/c/ao;

    .line 321
    :cond_1
    return-void

    .line 317
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
