.class final Lcom/squareup/a/a/a/ap;
.super Ljava/net/HttpURLConnection;


# instance fields
.field private final a:Lcom/squareup/a/a/a/ac;

.field private final b:Lcom/squareup/a/a/a/ai;


# direct methods
.method public constructor <init>(Lcom/squareup/a/a/a/ai;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 248
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/a/a/a/ac;->a()Ljava/net/URL;

    move-result-object v1

    invoke-direct {p0, v1}, Ljava/net/HttpURLConnection;-><init>(Ljava/net/URL;)V

    .line 249
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/a/a/ap;->a:Lcom/squareup/a/a/a/ac;

    .line 250
    iput-object p1, p0, Lcom/squareup/a/a/a/ap;->b:Lcom/squareup/a/a/a/ai;

    .line 253
    iput-boolean v0, p0, Lcom/squareup/a/a/a/ap;->connected:Z

    .line 254
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->h()Lcom/squareup/a/a/a/ak;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/a/a/a/ap;->doOutput:Z

    .line 257
    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->a:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ap;->method:Ljava/lang/String;

    .line 258
    return-void

    .line 254
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/squareup/a/a/a/ap;)Lcom/squareup/a/a/a/ai;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->b:Lcom/squareup/a/a/a/ai;

    return-object v0
.end method


# virtual methods
.method public addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 281
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public connect()V
    .locals 1

    .prologue
    .line 264
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public disconnect()V
    .locals 1

    .prologue
    .line 269
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getAllowUserInteraction()Z
    .locals 1

    .prologue
    .line 463
    const/4 v0, 0x0

    return v0
.end method

.method public getConnectTimeout()I
    .locals 1

    .prologue
    .line 402
    const/4 v0, 0x0

    return v0
.end method

.method public getContent()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 418
    invoke-static {}, Lcom/squareup/a/a/a/an;->d()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getContent([Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 423
    invoke-static {}, Lcom/squareup/a/a/a/an;->d()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getDefaultUseCaches()Z
    .locals 1

    .prologue
    .line 488
    invoke-super {p0}, Ljava/net/HttpURLConnection;->getDefaultUseCaches()Z

    move-result v0

    return v0
.end method

.method public getDoInput()Z
    .locals 1

    .prologue
    .line 443
    const/4 v0, 0x1

    return v0
.end method

.method public getDoOutput()Z
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->a:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->f()Lcom/squareup/a/a/a/ae;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getErrorStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 380
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHeaderField(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 349
    if-gez p1, :cond_0

    .line 350
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid header index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :cond_0
    if-nez p1, :cond_1

    .line 353
    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->b:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->b()Ljava/lang/String;

    move-result-object v0

    .line 355
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->b:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/f;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 360
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->b:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->b:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeaderFieldKey(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 337
    if-gez p1, :cond_0

    .line 338
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid header index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340
    :cond_0
    if-nez p1, :cond_1

    .line 341
    const/4 v0, 0x0

    .line 343
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->b:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/f;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeaderFields()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 365
    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->b:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/a/ap;->b:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v1}, Lcom/squareup/a/a/a/ai;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/a/a/a/aa;->a(Lcom/squareup/a/a/a/f;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getIfModifiedSince()J
    .locals 2

    .prologue
    .line 483
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 428
    invoke-static {}, Lcom/squareup/a/a/a/an;->d()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getInstanceFollowRedirects()Z
    .locals 1

    .prologue
    .line 319
    invoke-super {p0}, Ljava/net/HttpURLConnection;->getInstanceFollowRedirects()Z

    move-result v0

    return v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 433
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getReadTimeout()I
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x0

    return v0
.end method

.method public getRequestMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->a:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRequestProperties()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 293
    invoke-static {}, Lcom/squareup/a/a/a/an;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getRequestProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->a:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->b:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->c()I

    move-result v0

    return v0
.end method

.method public getResponseMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/squareup/a/a/a/ap;->b:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUseCaches()Z
    .locals 1

    .prologue
    .line 473
    invoke-super {p0}, Ljava/net/HttpURLConnection;->getUseCaches()Z

    move-result v0

    return v0
.end method

.method public setAllowUserInteraction(Z)V
    .locals 1

    .prologue
    .line 458
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setChunkedStreamingMode(I)V
    .locals 1

    .prologue
    .line 308
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setConnectTimeout(I)V
    .locals 1

    .prologue
    .line 396
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setDefaultUseCaches(Z)V
    .locals 0

    .prologue
    .line 493
    invoke-super {p0, p1}, Ljava/net/HttpURLConnection;->setDefaultUseCaches(Z)V

    .line 494
    return-void
.end method

.method public setDoInput(Z)V
    .locals 1

    .prologue
    .line 438
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setDoOutput(Z)V
    .locals 1

    .prologue
    .line 448
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setFixedLengthStreamingMode(I)V
    .locals 1

    .prologue
    .line 298
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setFixedLengthStreamingMode(J)V
    .locals 1

    .prologue
    .line 303
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setIfModifiedSince(J)V
    .locals 1

    .prologue
    .line 478
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setInstanceFollowRedirects(Z)V
    .locals 1

    .prologue
    .line 313
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setReadTimeout(I)V
    .locals 1

    .prologue
    .line 407
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setRequestMethod(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 324
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 276
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setUseCaches(Z)V
    .locals 1

    .prologue
    .line 468
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public usingProxy()Z
    .locals 1

    .prologue
    .line 389
    const/4 v0, 0x0

    return v0
.end method
