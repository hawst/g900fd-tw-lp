.class final Lcom/squareup/a/a/c/h;
.super Ljava/lang/Object;


# instance fields
.field a:[Lcom/squareup/a/a/c/e;

.field b:I

.field c:I

.field d:Lcom/squareup/a/a/a;

.field e:Lcom/squareup/a/a/a;

.field f:I

.field private final g:Lcom/squareup/a/a/c/o;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/squareup/a/a/b/c;

.field private j:I


# direct methods
.method constructor <init>(ZILcom/squareup/a/a/b/w;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/c/h;->h:Ljava/util/List;

    .line 105
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/a/a/c/e;

    iput-object v0, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    .line 107
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/a/a/c/h;->b:I

    .line 108
    iput v1, p0, Lcom/squareup/a/a/c/h;->c:I

    .line 114
    new-instance v0, Lcom/squareup/a/a/c;

    invoke-direct {v0}, Lcom/squareup/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/c/h;->d:Lcom/squareup/a/a/a;

    .line 119
    new-instance v0, Lcom/squareup/a/a/c;

    invoke-direct {v0}, Lcom/squareup/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/c/h;->e:Lcom/squareup/a/a/a;

    .line 120
    iput v1, p0, Lcom/squareup/a/a/c/h;->f:I

    .line 123
    if-eqz p1, :cond_0

    sget-object v0, Lcom/squareup/a/a/c/o;->b:Lcom/squareup/a/a/c/o;

    :goto_0
    iput-object v0, p0, Lcom/squareup/a/a/c/h;->g:Lcom/squareup/a/a/c/o;

    .line 124
    iput p2, p0, Lcom/squareup/a/a/c/h;->j:I

    .line 125
    invoke-static {p3}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/w;)Lcom/squareup/a/a/b/c;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/c/h;->i:Lcom/squareup/a/a/b/c;

    .line 126
    return-void

    .line 123
    :cond_0
    sget-object v0, Lcom/squareup/a/a/c/o;->a:Lcom/squareup/a/a/c/o;

    goto :goto_0
.end method

.method private a(ILcom/squareup/a/a/c/e;)V
    .locals 6

    .prologue
    const/4 v3, -0x1

    .line 293
    iget v0, p2, Lcom/squareup/a/a/c/e;->j:I

    .line 294
    if-eq p1, v3, :cond_4

    .line 295
    iget-object v1, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/h;->d(I)I

    move-result v2

    aget-object v1, v1, v2

    iget v1, v1, Lcom/squareup/a/a/c/e;->j:I

    sub-int/2addr v0, v1

    move v1, v0

    .line 299
    :goto_0
    iget v0, p0, Lcom/squareup/a/a/c/h;->j:I

    if-le v1, v0, :cond_0

    .line 300
    invoke-direct {p0}, Lcom/squareup/a/a/c/h;->d()V

    .line 302
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->h:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    :goto_1
    return-void

    .line 307
    :cond_0
    iget v0, p0, Lcom/squareup/a/a/c/h;->f:I

    add-int/2addr v0, v1

    iget v2, p0, Lcom/squareup/a/a/c/h;->j:I

    sub-int/2addr v0, v2

    .line 308
    invoke-direct {p0, v0}, Lcom/squareup/a/a/c/h;->b(I)I

    move-result v0

    .line 310
    if-ne p1, v3, :cond_3

    .line 311
    iget v0, p0, Lcom/squareup/a/a/c/h;->c:I

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    array-length v2, v2

    if-le v0, v2, :cond_2

    .line 312
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v2, v0, [Lcom/squareup/a/a/c/e;

    .line 313
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    array-length v4, v4

    iget-object v5, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    array-length v5, v5

    invoke-static {v0, v3, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 314
    array-length v0, v2

    const/16 v3, 0x40

    if-ne v0, v3, :cond_1

    .line 315
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->d:Lcom/squareup/a/a/a;

    check-cast v0, Lcom/squareup/a/a/c;

    invoke-virtual {v0}, Lcom/squareup/a/a/c;->b()Lcom/squareup/a/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/c/h;->d:Lcom/squareup/a/a/a;

    .line 316
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->e:Lcom/squareup/a/a/a;

    check-cast v0, Lcom/squareup/a/a/c;

    invoke-virtual {v0}, Lcom/squareup/a/a/c;->b()Lcom/squareup/a/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/c/h;->e:Lcom/squareup/a/a/a;

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->d:Lcom/squareup/a/a/a;

    iget-object v3, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    array-length v3, v3

    invoke-interface {v0, v3}, Lcom/squareup/a/a/a;->d(I)V

    .line 320
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->e:Lcom/squareup/a/a/a;

    iget-object v3, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    array-length v3, v3

    invoke-interface {v0, v3}, Lcom/squareup/a/a/a;->d(I)V

    .line 321
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/a/a/c/h;->b:I

    .line 322
    iput-object v2, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    .line 324
    :cond_2
    iget v0, p0, Lcom/squareup/a/a/c/h;->b:I

    add-int/lit8 v2, v0, -0x1

    iput v2, p0, Lcom/squareup/a/a/c/h;->b:I

    .line 325
    iget-object v2, p0, Lcom/squareup/a/a/c/h;->d:Lcom/squareup/a/a/a;

    invoke-interface {v2, v0}, Lcom/squareup/a/a/a;->a(I)V

    .line 326
    iget-object v2, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    aput-object p2, v2, v0

    .line 327
    iget v0, p0, Lcom/squareup/a/a/c/h;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/a/a/c/h;->c:I

    .line 333
    :goto_2
    iget v0, p0, Lcom/squareup/a/a/c/h;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/a/a/c/h;->f:I

    goto :goto_1

    .line 329
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/h;->d(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, p1

    .line 330
    iget-object v2, p0, Lcom/squareup/a/a/c/h;->d:Lcom/squareup/a/a/a;

    invoke-interface {v2, v0}, Lcom/squareup/a/a/a;->a(I)V

    .line 331
    iget-object v2, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    aput-object p2, v2, v0

    goto :goto_2

    :cond_4
    move v1, v0

    goto/16 :goto_0
.end method

.method private b(I)I
    .locals 6

    .prologue
    .line 158
    const/4 v1, 0x0

    .line 159
    if-lez p1, :cond_1

    .line 161
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iget v2, p0, Lcom/squareup/a/a/c/h;->b:I

    if-lt v0, v2, :cond_0

    if-lez p1, :cond_0

    .line 162
    iget-object v2, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/squareup/a/a/c/e;->j:I

    sub-int/2addr p1, v2

    .line 163
    iget v2, p0, Lcom/squareup/a/a/c/h;->f:I

    iget-object v3, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/squareup/a/a/c/e;->j:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/squareup/a/a/c/h;->f:I

    .line 164
    iget v2, p0, Lcom/squareup/a/a/c/h;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/squareup/a/a/c/h;->c:I

    .line 165
    add-int/lit8 v1, v1, 0x1

    .line 161
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->d:Lcom/squareup/a/a/a;

    invoke-interface {v0, v1}, Lcom/squareup/a/a/a;->d(I)V

    .line 168
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->e:Lcom/squareup/a/a/a;

    invoke-interface {v0, v1}, Lcom/squareup/a/a/a;->d(I)V

    .line 169
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    iget v2, p0, Lcom/squareup/a/a/c/h;->b:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    iget v4, p0, Lcom/squareup/a/a/c/h;->b:I

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v4, v1

    iget v5, p0, Lcom/squareup/a/a/c/h;->c:I

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 171
    iget v0, p0, Lcom/squareup/a/a/c/h;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/a/a/c/h;->b:I

    .line 173
    :cond_1
    return v1
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 232
    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/h;->h(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233
    invoke-static {}, Lcom/squareup/a/a/c/g;->a()[Lcom/squareup/a/a/c/e;

    move-result-object v0

    iget v1, p0, Lcom/squareup/a/a/c/h;->c:I

    sub-int v1, p1, v1

    aget-object v0, v0, v1

    .line 234
    iget v1, p0, Lcom/squareup/a/a/c/h;->j:I

    if-nez v1, :cond_0

    .line 235
    iget-object v1, p0, Lcom/squareup/a/a/c/h;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    :goto_0
    return-void

    .line 237
    :cond_0
    const/4 v1, -0x1

    invoke-direct {p0, v1, v0}, Lcom/squareup/a/a/c/h;->a(ILcom/squareup/a/a/c/e;)V

    goto :goto_0

    .line 240
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/h;->d(I)I

    move-result v0

    .line 241
    iget-object v1, p0, Lcom/squareup/a/a/c/h;->d:Lcom/squareup/a/a/a;

    invoke-interface {v1, v0}, Lcom/squareup/a/a/a;->c(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 242
    iget-object v1, p0, Lcom/squareup/a/a/c/h;->h:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    iget-object v1, p0, Lcom/squareup/a/a/c/h;->e:Lcom/squareup/a/a/a;

    invoke-interface {v1, v0}, Lcom/squareup/a/a/a;->a(I)V

    .line 245
    :cond_2
    iget-object v1, p0, Lcom/squareup/a/a/c/h;->d:Lcom/squareup/a/a/a;

    invoke-interface {v1, v0}, Lcom/squareup/a/a/a;->b(I)V

    goto :goto_0
.end method

.method private d(I)I
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Lcom/squareup/a/a/c/h;->b:I

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, p1

    return v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 149
    invoke-direct {p0}, Lcom/squareup/a/a/c/h;->e()V

    .line 150
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 151
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/a/a/c/h;->b:I

    .line 152
    iput v2, p0, Lcom/squareup/a/a/c/h;->c:I

    .line 153
    iput v2, p0, Lcom/squareup/a/a/c/h;->f:I

    .line 154
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->d:Lcom/squareup/a/a/a;

    invoke-interface {v0}, Lcom/squareup/a/a/a;->a()V

    .line 209
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->e:Lcom/squareup/a/a/a;

    invoke-interface {v0}, Lcom/squareup/a/a/a;->a()V

    .line 210
    return-void
.end method

.method private e(I)V
    .locals 4

    .prologue
    .line 255
    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/h;->g(I)Lcom/squareup/a/a/b/d;

    move-result-object v0

    .line 256
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/a/a/c/h;->a(Z)Lcom/squareup/a/a/b/d;

    move-result-object v1

    .line 257
    iget-object v2, p0, Lcom/squareup/a/a/c/h;->h:Ljava/util/List;

    new-instance v3, Lcom/squareup/a/a/c/e;

    invoke-direct {v3, v0, v1}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Lcom/squareup/a/a/b/d;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 261
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/c/h;->a(Z)Lcom/squareup/a/a/b/d;

    move-result-object v0

    .line 262
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/a/a/c/h;->a(Z)Lcom/squareup/a/a/b/d;

    move-result-object v1

    .line 263
    iget-object v2, p0, Lcom/squareup/a/a/c/h;->h:Ljava/util/List;

    new-instance v3, Lcom/squareup/a/a/c/e;

    invoke-direct {v3, v0, v1}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Lcom/squareup/a/a/b/d;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    return-void
.end method

.method private f(I)V
    .locals 4

    .prologue
    .line 268
    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/h;->g(I)Lcom/squareup/a/a/b/d;

    move-result-object v0

    .line 269
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/a/a/c/h;->a(Z)Lcom/squareup/a/a/b/d;

    move-result-object v1

    .line 270
    const/4 v2, -0x1

    new-instance v3, Lcom/squareup/a/a/c/e;

    invoke-direct {v3, v0, v1}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Lcom/squareup/a/a/b/d;)V

    invoke-direct {p0, v2, v3}, Lcom/squareup/a/a/c/h;->a(ILcom/squareup/a/a/c/e;)V

    .line 271
    return-void
.end method

.method private g(I)Lcom/squareup/a/a/b/d;
    .locals 2

    .prologue
    .line 280
    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/h;->h(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    invoke-static {}, Lcom/squareup/a/a/c/g;->a()[Lcom/squareup/a/a/c/e;

    move-result-object v0

    iget v1, p0, Lcom/squareup/a/a/c/h;->c:I

    sub-int v1, p1, v1

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/squareup/a/a/c/e;->h:Lcom/squareup/a/a/b/d;

    .line 283
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/h;->d(I)I

    move-result v1

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/squareup/a/a/c/e;->h:Lcom/squareup/a/a/b/d;

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 274
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/c/h;->a(Z)Lcom/squareup/a/a/b/d;

    move-result-object v0

    .line 275
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/a/a/c/h;->a(Z)Lcom/squareup/a/a/b/d;

    move-result-object v1

    .line 276
    const/4 v2, -0x1

    new-instance v3, Lcom/squareup/a/a/c/e;

    invoke-direct {v3, v0, v1}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Lcom/squareup/a/a/b/d;)V

    invoke-direct {p0, v2, v3}, Lcom/squareup/a/a/c/h;->a(ILcom/squareup/a/a/c/e;)V

    .line 277
    return-void
.end method

.method private h()I
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->i:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private h(I)Z
    .locals 1

    .prologue
    .line 288
    iget v0, p0, Lcom/squareup/a/a/c/h;->c:I

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(II)I
    .locals 3

    .prologue
    .line 341
    and-int v0, p1, p2

    .line 342
    if-ge v0, p2, :cond_0

    .line 359
    :goto_0
    return v0

    .line 348
    :cond_0
    const/4 v0, 0x0

    .line 350
    :goto_1
    invoke-direct {p0}, Lcom/squareup/a/a/c/h;->h()I

    move-result v1

    .line 351
    and-int/lit16 v2, v1, 0x80

    if-eqz v2, :cond_1

    .line 352
    and-int/lit8 v1, v1, 0x7f

    shl-int/2addr v1, v0

    add-int/2addr p2, v1

    .line 353
    add-int/lit8 v0, v0, 0x7

    goto :goto_1

    .line 355
    :cond_1
    shl-int v0, v1, v0

    add-int/2addr v0, p2

    .line 356
    goto :goto_0
.end method

.method a(Z)Lcom/squareup/a/a/b/d;
    .locals 6

    .prologue
    .line 367
    invoke-direct {p0}, Lcom/squareup/a/a/c/h;->h()I

    move-result v1

    .line 368
    and-int/lit16 v0, v1, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    .line 369
    :goto_0
    const/16 v2, 0x7f

    invoke-virtual {p0, v1, v2}, Lcom/squareup/a/a/c/h;->a(II)I

    move-result v1

    .line 371
    iget-object v2, p0, Lcom/squareup/a/a/c/h;->i:Lcom/squareup/a/a/b/c;

    int-to-long v4, v1

    invoke-interface {v2, v4, v5}, Lcom/squareup/a/a/b/c;->c(J)Lcom/squareup/a/a/b/d;

    move-result-object v1

    .line 373
    if-eqz v0, :cond_2

    .line 374
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->g:Lcom/squareup/a/a/c/o;

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/c/o;->a(Lcom/squareup/a/a/b/d;)Lcom/squareup/a/a/b/d;

    move-result-object v0

    .line 377
    :goto_1
    if-eqz p1, :cond_0

    .line 378
    invoke-virtual {v0}, Lcom/squareup/a/a/b/d;->d()Lcom/squareup/a/a/b/d;

    move-result-object v0

    .line 381
    :cond_0
    return-object v0

    .line 368
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method a()V
    .locals 5

    .prologue
    const/16 v4, 0x80

    const/16 v3, 0x40

    const/16 v2, 0x3f

    .line 181
    :goto_0
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->i:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->e()Z

    move-result v0

    if-nez v0, :cond_6

    .line 182
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->i:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 183
    if-ne v0, v4, :cond_0

    .line 184
    invoke-direct {p0}, Lcom/squareup/a/a/c/h;->e()V

    goto :goto_0

    .line 185
    :cond_0
    and-int/lit16 v1, v0, 0x80

    if-ne v1, v4, :cond_1

    .line 186
    const/16 v1, 0x7f

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/c/h;->a(II)I

    move-result v0

    .line 187
    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/squareup/a/a/c/h;->c(I)V

    goto :goto_0

    .line 189
    :cond_1
    if-ne v0, v3, :cond_2

    .line 190
    invoke-direct {p0}, Lcom/squareup/a/a/c/h;->f()V

    goto :goto_0

    .line 191
    :cond_2
    and-int/lit8 v1, v0, 0x40

    if-ne v1, v3, :cond_3

    .line 192
    invoke-virtual {p0, v0, v2}, Lcom/squareup/a/a/c/h;->a(II)I

    move-result v0

    .line 193
    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/squareup/a/a/c/h;->e(I)V

    goto :goto_0

    .line 194
    :cond_3
    if-nez v0, :cond_4

    .line 195
    invoke-direct {p0}, Lcom/squareup/a/a/c/h;->g()V

    goto :goto_0

    .line 196
    :cond_4
    and-int/lit16 v1, v0, 0xc0

    if-nez v1, :cond_5

    .line 197
    invoke-virtual {p0, v0, v2}, Lcom/squareup/a/a/c/h;->a(II)I

    move-result v0

    .line 198
    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/squareup/a/a/c/h;->f(I)V

    goto :goto_0

    .line 201
    :cond_5
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unhandled byte: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 205
    :cond_6
    return-void
.end method

.method a(I)V
    .locals 2

    .prologue
    .line 138
    iput p1, p0, Lcom/squareup/a/a/c/h;->j:I

    .line 139
    iget v0, p0, Lcom/squareup/a/a/c/h;->j:I

    iget v1, p0, Lcom/squareup/a/a/c/h;->f:I

    if-ge v0, v1, :cond_0

    .line 140
    iget v0, p0, Lcom/squareup/a/a/c/h;->j:I

    if-nez v0, :cond_1

    .line 141
    invoke-direct {p0}, Lcom/squareup/a/a/c/h;->d()V

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget v0, p0, Lcom/squareup/a/a/c/h;->f:I

    iget v1, p0, Lcom/squareup/a/a/c/h;->j:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/squareup/a/a/c/h;->b(I)I

    goto :goto_0
.end method

.method b()V
    .locals 3

    .prologue
    .line 213
    iget-object v0, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iget v1, p0, Lcom/squareup/a/a/c/h;->b:I

    if-eq v0, v1, :cond_1

    .line 214
    iget-object v1, p0, Lcom/squareup/a/a/c/h;->d:Lcom/squareup/a/a/a;

    invoke-interface {v1, v0}, Lcom/squareup/a/a/a;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/a/a/c/h;->e:Lcom/squareup/a/a/a;

    invoke-interface {v1, v0}, Lcom/squareup/a/a/a;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/squareup/a/a/c/h;->h:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/a/a/c/h;->a:[Lcom/squareup/a/a/c/e;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 218
    :cond_1
    return-void
.end method

.method c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/a/a/c/h;->h:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 226
    iget-object v1, p0, Lcom/squareup/a/a/c/h;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 227
    iget-object v1, p0, Lcom/squareup/a/a/c/h;->e:Lcom/squareup/a/a/a;

    invoke-interface {v1}, Lcom/squareup/a/a/a;->a()V

    .line 228
    return-object v0
.end method
