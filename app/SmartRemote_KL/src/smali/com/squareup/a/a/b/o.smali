.class final Lcom/squareup/a/a/b/o;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/w;


# instance fields
.field final synthetic a:Ljava/io/InputStream;

.field private b:Lcom/squareup/a/a/b/e;


# direct methods
.method constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 105
    iput-object p1, p0, Lcom/squareup/a/a/b/o;->a:Ljava/io/InputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    sget-object v0, Lcom/squareup/a/a/b/e;->a:Lcom/squareup/a/a/b/e;

    iput-object v0, p0, Lcom/squareup/a/a/b/o;->b:Lcom/squareup/a/a/b/e;

    return-void
.end method


# virtual methods
.method public b(Lcom/squareup/a/a/b/j;J)J
    .locals 6

    .prologue
    .line 109
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/b/o;->b:Lcom/squareup/a/a/b/e;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/e;->b()V

    .line 111
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/a/a/b/j;->f(I)Lcom/squareup/a/a/b/t;

    move-result-object v0

    .line 112
    iget v1, v0, Lcom/squareup/a/a/b/t;->c:I

    rsub-int v1, v1, 0x800

    int-to-long v2, v1

    invoke-static {p2, p3, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    .line 113
    iget-object v2, p0, Lcom/squareup/a/a/b/o;->a:Ljava/io/InputStream;

    iget-object v3, v0, Lcom/squareup/a/a/b/t;->a:[B

    iget v4, v0, Lcom/squareup/a/a/b/t;->c:I

    invoke-virtual {v2, v3, v4, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 114
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    const-wide/16 v0, -0x1

    .line 117
    :goto_0
    return-wide v0

    .line 115
    :cond_1
    iget v2, v0, Lcom/squareup/a/a/b/t;->c:I

    add-int/2addr v2, v1

    iput v2, v0, Lcom/squareup/a/a/b/t;->c:I

    .line 116
    iget-wide v2, p1, Lcom/squareup/a/a/b/j;->b:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p1, Lcom/squareup/a/a/b/j;->b:J

    .line 117
    int-to-long v0, v1

    goto :goto_0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/squareup/a/a/b/o;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 122
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "source("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/b/o;->a:Ljava/io/InputStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
