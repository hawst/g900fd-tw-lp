.class final Lcom/squareup/a/a/a/p;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/v;


# instance fields
.field final synthetic a:Lcom/squareup/a/a/a/k;

.field private b:Z

.field private c:J


# direct methods
.method private constructor <init>(Lcom/squareup/a/a/a/k;J)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/squareup/a/a/a/p;->a:Lcom/squareup/a/a/a/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    iput-wide p2, p0, Lcom/squareup/a/a/a/p;->c:J

    .line 249
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/a/a/a/k;JLcom/squareup/a/a/a/l;)V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/a/a/a/p;-><init>(Lcom/squareup/a/a/a/k;J)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/squareup/a/a/a/p;->b:Z

    if-eqz v0, :cond_0

    .line 269
    :goto_0
    return-void

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/p;->a:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V

    goto :goto_0
.end method

.method public a(Lcom/squareup/a/a/b/j;J)V
    .locals 6

    .prologue
    .line 256
    iget-boolean v0, p0, Lcom/squareup/a/a/a/p;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/a/a/t;->a(JJJ)V

    .line 258
    iget-wide v0, p0, Lcom/squareup/a/a/a/p;->c:J

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    .line 259
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/squareup/a/a/a/p;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bytes but received "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/p;->a:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/b;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/a/a/b/b;->a(Lcom/squareup/a/a/b/j;J)V

    .line 263
    iget-wide v0, p0, Lcom/squareup/a/a/a/p;->c:J

    sub-long/2addr v0, p2

    iput-wide v0, p0, Lcom/squareup/a/a/a/p;->c:J

    .line 264
    return-void
.end method

.method public close()V
    .locals 4

    .prologue
    .line 272
    iget-boolean v0, p0, Lcom/squareup/a/a/a/p;->b:Z

    if-eqz v0, :cond_0

    .line 276
    :goto_0
    return-void

    .line 273
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/a/a/a/p;->b:Z

    .line 274
    iget-wide v0, p0, Lcom/squareup/a/a/a/p;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/p;->a:Lcom/squareup/a/a/a/k;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/k;I)I

    goto :goto_0
.end method
