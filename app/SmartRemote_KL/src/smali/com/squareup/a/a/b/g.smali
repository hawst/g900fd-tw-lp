.class public final Lcom/squareup/a/a/b/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/v;


# instance fields
.field private final a:Lcom/squareup/a/a/b/b;

.field private final b:Ljava/util/zip/Deflater;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/squareup/a/a/b/v;Ljava/util/zip/Deflater;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p1}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/v;)Lcom/squareup/a/a/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/b/g;->a:Lcom/squareup/a/a/b/b;

    .line 45
    iput-object p2, p0, Lcom/squareup/a/a/b/g;->b:Ljava/util/zip/Deflater;

    .line 46
    return-void
.end method

.method private a(Z)V
    .locals 7
    .annotation build Lorg/codehaus/mojo/animal_sniffer/IgnoreJRERequirement;
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/squareup/a/a/b/g;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->b()Lcom/squareup/a/a/b/j;

    move-result-object v1

    .line 76
    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/squareup/a/a/b/j;->f(I)Lcom/squareup/a/a/b/t;

    move-result-object v2

    .line 82
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/squareup/a/a/b/g;->b:Ljava/util/zip/Deflater;

    iget-object v3, v2, Lcom/squareup/a/a/b/t;->a:[B

    iget v4, v2, Lcom/squareup/a/a/b/t;->c:I

    iget v5, v2, Lcom/squareup/a/a/b/t;->c:I

    rsub-int v5, v5, 0x800

    const/4 v6, 0x2

    invoke-virtual {v0, v3, v4, v5, v6}, Ljava/util/zip/Deflater;->deflate([BIII)I

    move-result v0

    .line 86
    :goto_1
    if-lez v0, :cond_2

    .line 87
    iget v3, v2, Lcom/squareup/a/a/b/t;->c:I

    add-int/2addr v3, v0

    iput v3, v2, Lcom/squareup/a/a/b/t;->c:I

    .line 88
    iget-wide v2, v1, Lcom/squareup/a/a/b/j;->b:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/squareup/a/a/b/j;->b:J

    .line 89
    iget-object v0, p0, Lcom/squareup/a/a/b/g;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->c()Lcom/squareup/a/a/b/b;

    goto :goto_0

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/b/g;->b:Ljava/util/zip/Deflater;

    iget-object v3, v2, Lcom/squareup/a/a/b/t;->a:[B

    iget v4, v2, Lcom/squareup/a/a/b/t;->c:I

    iget v5, v2, Lcom/squareup/a/a/b/t;->c:I

    rsub-int v5, v5, 0x800

    invoke-virtual {v0, v3, v4, v5}, Ljava/util/zip/Deflater;->deflate([BII)I

    move-result v0

    goto :goto_1

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/squareup/a/a/b/g;->b:Ljava/util/zip/Deflater;

    invoke-virtual {v0}, Ljava/util/zip/Deflater;->needsInput()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/squareup/a/a/b/g;->a(Z)V

    .line 98
    iget-object v0, p0, Lcom/squareup/a/a/b/g;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V

    .line 99
    return-void
.end method

.method public a(Lcom/squareup/a/a/b/j;J)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 50
    iget-wide v0, p1, Lcom/squareup/a/a/b/j;->b:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/a/a/b/x;->a(JJJ)V

    .line 51
    :goto_0
    cmp-long v0, p2, v2

    if-lez v0, :cond_1

    .line 53
    iget-object v0, p1, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 54
    iget v1, v0, Lcom/squareup/a/a/b/t;->c:I

    iget v4, v0, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v1, v4

    .line 55
    iget-object v4, p0, Lcom/squareup/a/a/b/g;->b:Ljava/util/zip/Deflater;

    iget-object v5, v0, Lcom/squareup/a/a/b/t;->a:[B

    iget v6, v0, Lcom/squareup/a/a/b/t;->b:I

    invoke-virtual {v4, v5, v6, v1}, Ljava/util/zip/Deflater;->setInput([BII)V

    .line 58
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/squareup/a/a/b/g;->a(Z)V

    .line 61
    iget-wide v4, p1, Lcom/squareup/a/a/b/j;->b:J

    int-to-long v6, v1

    sub-long/2addr v4, v6

    iput-wide v4, p1, Lcom/squareup/a/a/b/j;->b:J

    .line 62
    iget v4, v0, Lcom/squareup/a/a/b/t;->b:I

    add-int/2addr v4, v1

    iput v4, v0, Lcom/squareup/a/a/b/t;->b:I

    .line 63
    iget v4, v0, Lcom/squareup/a/a/b/t;->b:I

    iget v5, v0, Lcom/squareup/a/a/b/t;->c:I

    if-ne v4, v5, :cond_0

    .line 64
    invoke-virtual {v0}, Lcom/squareup/a/a/b/t;->a()Lcom/squareup/a/a/b/t;

    move-result-object v4

    iput-object v4, p1, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 65
    sget-object v4, Lcom/squareup/a/a/b/u;->a:Lcom/squareup/a/a/b/u;

    invoke-virtual {v4, v0}, Lcom/squareup/a/a/b/u;->a(Lcom/squareup/a/a/b/t;)V

    .line 68
    :cond_0
    int-to-long v0, v1

    sub-long/2addr p2, v0

    .line 69
    goto :goto_0

    .line 70
    :cond_1
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/squareup/a/a/b/g;->c:Z

    if-eqz v0, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    const/4 v1, 0x0

    .line 108
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/b/g;->b:Ljava/util/zip/Deflater;

    invoke-virtual {v0}, Ljava/util/zip/Deflater;->finish()V

    .line 109
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/squareup/a/a/b/g;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/squareup/a/a/b/g;->b:Ljava/util/zip/Deflater;

    invoke-virtual {v0}, Ljava/util/zip/Deflater;->end()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 121
    :cond_2
    :goto_2
    :try_start_2
    iget-object v1, p0, Lcom/squareup/a/a/b/g;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v1}, Lcom/squareup/a/a/b/b;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 125
    :cond_3
    :goto_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/squareup/a/a/b/g;->c:Z

    .line 127
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/a/a/b/x;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 111
    goto :goto_1

    .line 116
    :catch_1
    move-exception v0

    .line 117
    if-eqz v1, :cond_2

    move-object v0, v1

    goto :goto_2

    .line 122
    :catch_2
    move-exception v1

    .line 123
    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "DeflaterSink("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/b/g;->a:Lcom/squareup/a/a/b/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
