.class Lcom/squareup/a/a/b/s;
.super Ljava/io/InputStream;


# instance fields
.field final synthetic a:Lcom/squareup/a/a/b/r;


# direct methods
.method constructor <init>(Lcom/squareup/a/a/b/r;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    invoke-static {v0}, Lcom/squareup/a/a/b/r;->a(Lcom/squareup/a/a/b/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_0
    return-void
.end method


# virtual methods
.method public available()I
    .locals 4

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/squareup/a/a/b/s;->a()V

    .line 177
    iget-object v0, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    iget-object v0, v0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    iget-wide v0, v0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/32 v2, 0x7fffffff

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/r;->close()V

    .line 182
    return-void
.end method

.method public read()I
    .locals 4

    .prologue
    .line 155
    invoke-direct {p0}, Lcom/squareup/a/a/b/s;->a()V

    .line 156
    iget-object v0, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    iget-object v0, v0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    iget-wide v0, v0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    iget-object v0, v0, Lcom/squareup/a/a/b/r;->b:Lcom/squareup/a/a/b/w;

    iget-object v1, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    iget-object v1, v1, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/a/a/b/w;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v0

    .line 158
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 160
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    iget-object v0, v0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public read([BII)I
    .locals 6

    .prologue
    .line 164
    invoke-direct {p0}, Lcom/squareup/a/a/b/s;->a()V

    .line 165
    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/a/a/b/x;->a(JJJ)V

    .line 167
    iget-object v0, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    iget-object v0, v0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    iget-wide v0, v0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    iget-object v0, v0, Lcom/squareup/a/a/b/r;->b:Lcom/squareup/a/a/b/w;

    iget-object v1, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    iget-object v1, v1, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/a/a/b/w;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v0

    .line 169
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 172
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    iget-object v0, v0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/a/a/b/j;->b([BII)I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/a/a/b/s;->a:Lcom/squareup/a/a/b/r;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".inputStream()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
