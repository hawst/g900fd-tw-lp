.class public final Lcom/squareup/a/a/c/ac;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final synthetic k:Z

.field private static final l:Ljava/util/concurrent/ExecutorService;


# instance fields
.field final a:Lcom/squareup/a/v;

.field final b:Z

.field c:J

.field d:J

.field final e:Lcom/squareup/a/a/c/y;

.field final f:Lcom/squareup/a/a/c/y;

.field final g:Lcom/squareup/a/a/c/b;

.field final h:Lcom/squareup/a/a/c/d;

.field final i:J

.field final j:Lcom/squareup/a/a/c/al;

.field private final m:Lcom/squareup/a/a/c/q;

.field private final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/a/a/c/ao;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ljava/lang/String;

.field private p:I

.field private q:I

.field private r:Z

.field private s:J

.field private t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/a/a/c/v;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Lcom/squareup/a/a/c/w;

.field private v:I

.field private w:Z

.field private final x:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 52
    const-class v0, Lcom/squareup/a/a/c/ac;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v8

    :goto_0
    sput-boolean v0, Lcom/squareup/a/a/c/ac;->k:Z

    .line 66
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const v3, 0x7fffffff

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    const-string/jumbo v0, "OkHttp SpdyConnection"

    invoke-static {v0, v8}, Lcom/squareup/a/a/t;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v1, Lcom/squareup/a/a/c/ac;->l:Ljava/util/concurrent/ExecutorService;

    return-void

    :cond_0
    move v0, v2

    .line 52
    goto :goto_0
.end method

.method private constructor <init>(Lcom/squareup/a/a/c/ak;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/c/ac;->n:Ljava/util/Map;

    .line 86
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/squareup/a/a/c/ac;->s:J

    .line 99
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/squareup/a/a/c/ac;->c:J

    .line 110
    new-instance v0, Lcom/squareup/a/a/c/y;

    invoke-direct {v0}, Lcom/squareup/a/a/c/y;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/c/ac;->e:Lcom/squareup/a/a/c/y;

    .line 115
    new-instance v0, Lcom/squareup/a/a/c/y;

    invoke-direct {v0}, Lcom/squareup/a/a/c/y;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/c/ac;->f:Lcom/squareup/a/a/c/y;

    .line 117
    iput-boolean v3, p0, Lcom/squareup/a/a/c/ac;->w:Z

    .line 762
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/c/ac;->x:Ljava/util/Set;

    .line 126
    invoke-static {p1}, Lcom/squareup/a/a/c/ak;->a(Lcom/squareup/a/a/c/ak;)Lcom/squareup/a/v;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/c/ac;->a:Lcom/squareup/a/v;

    .line 127
    invoke-static {p1}, Lcom/squareup/a/a/c/ak;->b(Lcom/squareup/a/a/c/ak;)Lcom/squareup/a/a/c/w;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/c/ac;->u:Lcom/squareup/a/a/c/w;

    .line 128
    invoke-static {p1}, Lcom/squareup/a/a/c/ak;->c(Lcom/squareup/a/a/c/ak;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/a/a/c/ac;->b:Z

    .line 129
    invoke-static {p1}, Lcom/squareup/a/a/c/ak;->d(Lcom/squareup/a/a/c/ak;)Lcom/squareup/a/a/c/q;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/c/ac;->m:Lcom/squareup/a/a/c/q;

    .line 130
    invoke-static {p1}, Lcom/squareup/a/a/c/ak;->c(Lcom/squareup/a/a/c/ak;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/squareup/a/a/c/ac;->q:I

    .line 131
    invoke-static {p1}, Lcom/squareup/a/a/c/ak;->c(Lcom/squareup/a/a/c/ak;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    iput v1, p0, Lcom/squareup/a/a/c/ac;->v:I

    .line 137
    invoke-static {p1}, Lcom/squareup/a/a/c/ak;->c(Lcom/squareup/a/a/c/ak;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->e:Lcom/squareup/a/a/c/y;

    const/4 v1, 0x7

    const/high16 v2, 0x1000000

    invoke-virtual {v0, v1, v3, v2}, Lcom/squareup/a/a/c/y;->a(III)Lcom/squareup/a/a/c/y;

    .line 141
    :cond_0
    invoke-static {p1}, Lcom/squareup/a/a/c/ak;->e(Lcom/squareup/a/a/c/ak;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/c/ac;->o:Ljava/lang/String;

    .line 144
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->a:Lcom/squareup/a/v;

    sget-object v1, Lcom/squareup/a/v;->a:Lcom/squareup/a/v;

    if-ne v0, v1, :cond_3

    .line 145
    new-instance v0, Lcom/squareup/a/a/c/j;

    invoke-direct {v0}, Lcom/squareup/a/a/c/j;-><init>()V

    .line 151
    :goto_2
    iget-object v1, p0, Lcom/squareup/a/a/c/ac;->f:Lcom/squareup/a/a/c/y;

    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Lcom/squareup/a/a/c/y;->d(I)I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/squareup/a/a/c/ac;->d:J

    .line 152
    invoke-static {p1}, Lcom/squareup/a/a/c/ak;->f(Lcom/squareup/a/a/c/ak;)Lcom/squareup/a/a/b/c;

    move-result-object v1

    iget-boolean v2, p0, Lcom/squareup/a/a/c/ac;->b:Z

    invoke-interface {v0, v1, v2}, Lcom/squareup/a/a/c/as;->a(Lcom/squareup/a/a/b/c;Z)Lcom/squareup/a/a/c/b;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/a/c/ac;->g:Lcom/squareup/a/a/c/b;

    .line 153
    invoke-static {p1}, Lcom/squareup/a/a/c/ak;->g(Lcom/squareup/a/a/c/ak;)Lcom/squareup/a/a/b/b;

    move-result-object v1

    iget-boolean v2, p0, Lcom/squareup/a/a/c/ac;->b:Z

    invoke-interface {v0, v1, v2}, Lcom/squareup/a/a/c/as;->a(Lcom/squareup/a/a/b/b;Z)Lcom/squareup/a/a/c/d;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    .line 154
    invoke-interface {v0}, Lcom/squareup/a/a/c/as;->a()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/a/a/c/ac;->i:J

    .line 156
    new-instance v0, Lcom/squareup/a/a/c/al;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/a/a/c/al;-><init>(Lcom/squareup/a/a/c/ac;Lcom/squareup/a/a/c/ad;)V

    iput-object v0, p0, Lcom/squareup/a/a/c/ac;->j:Lcom/squareup/a/a/c/al;

    .line 157
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/squareup/a/a/c/ac;->j:Lcom/squareup/a/a/c/al;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 158
    return-void

    :cond_1
    move v0, v2

    .line 130
    goto :goto_0

    :cond_2
    move v1, v2

    .line 131
    goto :goto_1

    .line 146
    :cond_3
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->a:Lcom/squareup/a/v;

    sget-object v1, Lcom/squareup/a/v;->b:Lcom/squareup/a/v;

    if-ne v0, v1, :cond_4

    .line 147
    new-instance v0, Lcom/squareup/a/a/c/z;

    invoke-direct {v0}, Lcom/squareup/a/a/c/z;-><init>()V

    goto :goto_2

    .line 149
    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lcom/squareup/a/a/c/ac;->a:Lcom/squareup/a/v;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method synthetic constructor <init>(Lcom/squareup/a/a/c/ak;Lcom/squareup/a/a/c/ad;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/ac;-><init>(Lcom/squareup/a/a/c/ak;)V

    return-void
.end method

.method private a(ILjava/util/List;ZZ)Lcom/squareup/a/a/c/ao;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;ZZ)",
            "Lcom/squareup/a/a/c/ao;"
        }
    .end annotation

    .prologue
    .line 232
    if-nez p3, :cond_0

    const/4 v5, 0x1

    .line 233
    :goto_0
    if-nez p4, :cond_1

    const/4 v6, 0x1

    .line 234
    :goto_1
    const/4 v7, -0x1

    .line 235
    const/4 v14, 0x0

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    move-object/from16 v16, v0

    monitor-enter v16

    .line 240
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 241
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/squareup/a/a/c/ac;->r:Z

    if-eqz v2, :cond_2

    .line 242
    new-instance v2, Ljava/io/IOException;

    const-string/jumbo v3, "shutdown"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 251
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2

    .line 260
    :catchall_1
    move-exception v2

    monitor-exit v16
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    .line 232
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 233
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 244
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/squareup/a/a/c/ac;->q:I

    .line 245
    move-object/from16 v0, p0

    iget v2, v0, Lcom/squareup/a/a/c/ac;->q:I

    add-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/squareup/a/a/c/ac;->q:I

    .line 246
    new-instance v2, Lcom/squareup/a/a/c/ao;

    move-object/from16 v4, p0

    move-object/from16 v8, p2

    invoke-direct/range {v2 .. v8}, Lcom/squareup/a/a/c/ao;-><init>(ILcom/squareup/a/a/c/ac;ZZILjava/util/List;)V

    .line 247
    invoke-virtual {v2}, Lcom/squareup/a/a/c/ao;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 248
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/squareup/a/a/c/ac;->n:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v4, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/squareup/a/a/c/ac;->a(Z)V

    .line 251
    :cond_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 252
    if-nez p1, :cond_5

    .line 253
    :try_start_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    move v9, v5

    move v10, v6

    move v11, v3

    move/from16 v12, p1

    move v13, v7

    move-object/from16 v15, p2

    invoke-interface/range {v8 .. v15}, Lcom/squareup/a/a/c/d;->a(ZZIIIILjava/util/List;)V

    .line 260
    :goto_2
    monitor-exit v16
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 262
    if-nez p3, :cond_4

    .line 263
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    invoke-interface {v3}, Lcom/squareup/a/a/c/d;->c()V

    .line 266
    :cond_4
    return-object v2

    .line 255
    :cond_5
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/squareup/a/a/c/ac;->b:Z

    if-eqz v4, :cond_6

    .line 256
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "client streams shouldn\'t have associated stream IDs"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 258
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    move/from16 v0, p1

    move-object/from16 v1, p2

    invoke-interface {v4, v0, v3, v1}, Lcom/squareup/a/a/c/d;->a(IILjava/util/List;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ac;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->o:Ljava/lang/String;

    return-object v0
.end method

.method private a(ILcom/squareup/a/a/b/c;IZ)V
    .locals 9

    .prologue
    .line 812
    new-instance v5, Lcom/squareup/a/a/b/j;

    invoke-direct {v5}, Lcom/squareup/a/a/b/j;-><init>()V

    .line 813
    int-to-long v0, p3

    invoke-interface {p2, v0, v1}, Lcom/squareup/a/a/b/c;->a(J)V

    .line 814
    int-to-long v0, p3

    invoke-interface {p2, v5, v0, v1}, Lcom/squareup/a/a/b/c;->b(Lcom/squareup/a/a/b/j;J)J

    .line 815
    invoke-virtual {v5}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v0

    int-to-long v2, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " != "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 816
    :cond_0
    sget-object v8, Lcom/squareup/a/a/c/ac;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/a/a/c/ai;

    const-string/jumbo v2, "OkHttp %s Push Data[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/a/a/c/ac;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/a/a/c/ai;-><init>(Lcom/squareup/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ILcom/squareup/a/a/b/j;IZ)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 830
    return-void
.end method

.method private a(ILjava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 765
    monitor-enter p0

    .line 766
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->x:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 767
    sget-object v0, Lcom/squareup/a/a/c/a;->b:Lcom/squareup/a/a/c/a;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/a/a/c/ac;->a(ILcom/squareup/a/a/c/a;)V

    .line 768
    monitor-exit p0

    .line 786
    :goto_0
    return-void

    .line 770
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->x:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 771
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 772
    sget-object v6, Lcom/squareup/a/a/c/ac;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/a/a/c/ag;

    const-string/jumbo v2, "OkHttp %s Push Request[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/a/a/c/ac;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/a/a/c/ag;-><init>(Lcom/squareup/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 771
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(ILjava/util/List;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 790
    sget-object v7, Lcom/squareup/a/a/c/ac;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/a/a/c/ah;

    const-string/jumbo v2, "OkHttp %s Push Headers[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/a/a/c/ac;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/a/a/c/ah;-><init>(Lcom/squareup/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;Z)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 804
    return-void
.end method

.method private a(Lcom/squareup/a/a/c/a;Lcom/squareup/a/a/c/a;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 431
    sget-boolean v0, Lcom/squareup/a/a/c/ac;->k:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 434
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/a;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    .line 441
    :goto_0
    monitor-enter p0

    .line 442
    :try_start_1
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 443
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v4, p0, Lcom/squareup/a/a/c/ac;->n:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    new-array v4, v4, [Lcom/squareup/a/a/c/ao;

    invoke-interface {v0, v4}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/a/a/c/ao;

    .line 444
    iget-object v4, p0, Lcom/squareup/a/a/c/ac;->n:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 445
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/squareup/a/a/c/ac;->a(Z)V

    move-object v5, v0

    .line 447
    :goto_1
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->t:Ljava/util/Map;

    if-eqz v0, :cond_7

    .line 448
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/a/a/c/ac;->t:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    new-array v2, v2, [Lcom/squareup/a/a/c/v;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/a/a/c/v;

    .line 449
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/squareup/a/a/c/ac;->t:Ljava/util/Map;

    move-object v4, v0

    .line 451
    :goto_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453
    if-eqz v5, :cond_3

    .line 454
    array-length v6, v5

    move v2, v3

    move-object v0, v1

    :goto_3
    if-ge v2, v6, :cond_2

    aget-object v1, v5, v2

    .line 456
    :try_start_2
    invoke-virtual {v1, p2}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/a;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 454
    :cond_1
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 435
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 436
    goto :goto_0

    .line 451
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 457
    :catch_1
    move-exception v1

    .line 458
    if-eqz v0, :cond_1

    move-object v0, v1

    goto :goto_4

    :cond_2
    move-object v1, v0

    .line 463
    :cond_3
    if-eqz v4, :cond_4

    .line 464
    array-length v2, v4

    move v0, v3

    :goto_5
    if-ge v0, v2, :cond_4

    aget-object v3, v4, v0

    .line 465
    invoke-virtual {v3}, Lcom/squareup/a/a/c/v;->c()V

    .line 464
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 470
    :cond_4
    :try_start_4
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->g:Lcom/squareup/a/a/c/b;

    invoke-interface {v0}, Lcom/squareup/a/a/c/b;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 475
    :goto_6
    :try_start_5
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    invoke-interface {v0}, Lcom/squareup/a/a/c/d;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    move-object v0, v1

    .line 480
    :cond_5
    :goto_7
    if-eqz v0, :cond_6

    throw v0

    .line 471
    :catch_2
    move-exception v0

    move-object v1, v0

    .line 472
    goto :goto_6

    .line 476
    :catch_3
    move-exception v0

    .line 477
    if-eqz v1, :cond_5

    move-object v0, v1

    goto :goto_7

    .line 481
    :cond_6
    return-void

    :cond_7
    move-object v4, v2

    goto :goto_2

    :cond_8
    move-object v5, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ac;ILcom/squareup/a/a/b/c;IZ)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/a/a/c/ac;->a(ILcom/squareup/a/a/b/c;IZ)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ac;ILcom/squareup/a/a/c/a;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/squareup/a/a/c/ac;->c(ILcom/squareup/a/a/c/a;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ac;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/squareup/a/a/c/ac;->a(ILjava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ac;ILjava/util/List;Z)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/a/a/c/ac;->a(ILjava/util/List;Z)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ac;Lcom/squareup/a/a/c/a;Lcom/squareup/a/a/c/a;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/a;Lcom/squareup/a/a/c/a;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ac;ZIILcom/squareup/a/a/c/v;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/a/a/c/ac;->b(ZIILcom/squareup/a/a/c/v;)V

    return-void
.end method

.method private declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 186
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/squareup/a/a/c/ac;->s:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    monitor-exit p0

    return-void

    .line 186
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(ZIILcom/squareup/a/a/c/v;)V
    .locals 9

    .prologue
    .line 373
    sget-object v8, Lcom/squareup/a/a/c/ac;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/a/a/c/af;

    const-string/jumbo v2, "OkHttp %s ping %08x%08x"

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/a/a/c/ac;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/a/a/c/af;-><init>(Lcom/squareup/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ZIILcom/squareup/a/a/c/v;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 382
    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ac;I)Z
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/ac;->d(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ac;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/squareup/a/a/c/ac;->w:Z

    return p1
.end method

.method static synthetic b(Lcom/squareup/a/a/c/ac;I)I
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Lcom/squareup/a/a/c/ac;->p:I

    return p1
.end method

.method static synthetic b(Lcom/squareup/a/a/c/ac;ZIILcom/squareup/a/a/c/v;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/a/a/c/ac;->a(ZIILcom/squareup/a/a/c/v;)V

    return-void
.end method

.method private b(ZIILcom/squareup/a/a/c/v;)V
    .locals 2

    .prologue
    .line 385
    iget-object v1, p0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    monitor-enter v1

    .line 387
    if-eqz p4, :cond_0

    :try_start_0
    invoke-virtual {p4}, Lcom/squareup/a/a/c/v;->a()V

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/a/a/c/d;->a(ZII)V

    .line 389
    monitor-exit v1

    .line 390
    return-void

    .line 389
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/squareup/a/a/c/ac;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ac;->r:Z

    return v0
.end method

.method static synthetic b(Lcom/squareup/a/a/c/ac;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/squareup/a/a/c/ac;->r:Z

    return p1
.end method

.method static synthetic c(Lcom/squareup/a/a/c/ac;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/squareup/a/a/c/ac;->p:I

    return v0
.end method

.method private declared-synchronized c(I)Lcom/squareup/a/a/c/v;
    .locals 2

    .prologue
    .line 393
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->t:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->t:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/squareup/a/a/c/ac;I)Lcom/squareup/a/a/c/v;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/ac;->c(I)Lcom/squareup/a/a/c/v;

    move-result-object v0

    return-object v0
.end method

.method private c(ILcom/squareup/a/a/c/a;)V
    .locals 7

    .prologue
    .line 833
    sget-object v6, Lcom/squareup/a/a/c/ac;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/a/a/c/aj;

    const-string/jumbo v2, "OkHttp %s Push Reset[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/a/a/c/ac;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/a/a/c/aj;-><init>(Lcom/squareup/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ILcom/squareup/a/a/c/a;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 841
    return-void
.end method

.method static synthetic d(Lcom/squareup/a/a/c/ac;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/squareup/a/a/c/ac;->q:I

    return v0
.end method

.method private d(I)Z
    .locals 2

    .prologue
    .line 758
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->a:Lcom/squareup/a/v;

    sget-object v1, Lcom/squareup/a/v;->a:Lcom/squareup/a/v;

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/squareup/a/a/c/ac;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->n:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/a/a/c/ac;)Lcom/squareup/a/a/c/q;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->m:Lcom/squareup/a/a/c/q;

    return-object v0
.end method

.method static synthetic f()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/squareup/a/a/c/ac;->l:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic g(Lcom/squareup/a/a/c/ac;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ac;->w:Z

    return v0
.end method

.method static synthetic h(Lcom/squareup/a/a/c/ac;)Lcom/squareup/a/a/c/w;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->u:Lcom/squareup/a/a/c/w;

    return-object v0
.end method

.method static synthetic i(Lcom/squareup/a/a/c/ac;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->x:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method declared-synchronized a(I)Lcom/squareup/a/a/c/ao;
    .locals 2

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->n:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/ao;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/List;ZZ)Lcom/squareup/a/a/c/ao;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;ZZ)",
            "Lcom/squareup/a/a/c/ao;"
        }
    .end annotation

    .prologue
    .line 227
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/squareup/a/a/c/ac;->a(ILjava/util/List;ZZ)Lcom/squareup/a/a/c/ao;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/squareup/a/v;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->a:Lcom/squareup/a/v;

    return-object v0
.end method

.method a(IJ)V
    .locals 8

    .prologue
    .line 341
    sget-object v0, Lcom/squareup/a/a/c/ac;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/a/a/c/ae;

    const-string/jumbo v3, "OkHttp Window Update %s stream %d"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/squareup/a/a/c/ac;->o:Ljava/lang/String;

    aput-object v5, v4, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    move-object v2, p0

    move v5, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Lcom/squareup/a/a/c/ae;-><init>(Lcom/squareup/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;IJ)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 349
    return-void
.end method

.method a(ILcom/squareup/a/a/c/a;)V
    .locals 7

    .prologue
    .line 326
    sget-object v6, Lcom/squareup/a/a/c/ac;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/a/a/c/ad;

    const-string/jumbo v2, "OkHttp %s stream %d"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/a/a/c/ac;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/a/a/c/ad;-><init>(Lcom/squareup/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ILcom/squareup/a/a/c/a;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 334
    return-void
.end method

.method public a(IZLcom/squareup/a/a/b/j;J)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const-wide/16 v8, 0x0

    .line 291
    cmp-long v0, p4, v8

    if-nez v0, :cond_2

    .line 292
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    invoke-interface {v0, p2, p1, p3, v1}, Lcom/squareup/a/a/c/d;->a(ZILcom/squareup/a/a/b/j;I)V

    .line 314
    :cond_0
    return-void

    .line 307
    :cond_1
    :try_start_0
    iget-wide v2, p0, Lcom/squareup/a/a/c/ac;->d:J

    invoke-static {p4, p5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iget-wide v4, p0, Lcom/squareup/a/a/c/ac;->i:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    .line 308
    iget-wide v4, p0, Lcom/squareup/a/a/c/ac;->d:J

    int-to-long v6, v2

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/squareup/a/a/c/ac;->d:J

    .line 309
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    int-to-long v4, v2

    sub-long/2addr p4, v4

    .line 312
    iget-object v3, p0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    if-eqz p2, :cond_3

    cmp-long v0, p4, v8

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v3, v0, p1, p3, v2}, Lcom/squareup/a/a/c/d;->a(ZILcom/squareup/a/a/b/j;I)V

    .line 296
    :cond_2
    cmp-long v0, p4, v8

    if-lez v0, :cond_0

    .line 298
    monitor-enter p0

    .line 300
    :goto_1
    :try_start_1
    iget-wide v2, p0, Lcom/squareup/a/a/c/ac;->d:J

    cmp-long v0, v2, v8

    if-gtz v0, :cond_1

    .line 301
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 303
    :catch_0
    move-exception v0

    .line 304
    :try_start_2
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 309
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_3
    move v0, v1

    .line 312
    goto :goto_0
.end method

.method a(J)V
    .locals 3

    .prologue
    .line 321
    iget-wide v0, p0, Lcom/squareup/a/a/c/ac;->d:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/squareup/a/a/c/ac;->d:J

    .line 322
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 323
    :cond_0
    return-void
.end method

.method public a(Lcom/squareup/a/a/c/a;)V
    .locals 4

    .prologue
    .line 407
    iget-object v1, p0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    monitor-enter v1

    .line 409
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410
    :try_start_1
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ac;->r:Z

    if-eqz v0, :cond_0

    .line 411
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 419
    :goto_0
    return-void

    .line 413
    :cond_0
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lcom/squareup/a/a/c/ac;->r:Z

    .line 414
    iget v0, p0, Lcom/squareup/a/a/c/ac;->p:I

    .line 415
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 417
    :try_start_4
    iget-object v2, p0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    sget-object v3, Lcom/squareup/a/a/t;->a:[B

    invoke-interface {v2, v0, p1, v3}, Lcom/squareup/a/a/c/d;->a(ILcom/squareup/a/a/c/a;[B)V

    .line 418
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 415
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method declared-synchronized b(I)Lcom/squareup/a/a/c/ao;
    .locals 2

    .prologue
    .line 178
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->n:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/ao;

    .line 179
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/a/a/c/ac;->n:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 180
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/squareup/a/a/c/ac;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    :cond_0
    monitor-exit p0

    return-object v0

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method b(ILcom/squareup/a/a/c/a;)V
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    invoke-interface {v0, p1, p2}, Lcom/squareup/a/a/c/d;->a(ILcom/squareup/a/a/c/a;)V

    .line 338
    return-void
.end method

.method public declared-synchronized b()Z
    .locals 4

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/squareup/a/a/c/ac;->s:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()J
    .locals 2

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/squareup/a/a/c/ac;->s:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 427
    sget-object v0, Lcom/squareup/a/a/c/a;->a:Lcom/squareup/a/a/c/a;

    sget-object v1, Lcom/squareup/a/a/c/a;->l:Lcom/squareup/a/a/c/a;

    invoke-direct {p0, v0, v1}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/a;Lcom/squareup/a/a/c/a;)V

    .line 428
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    invoke-interface {v0}, Lcom/squareup/a/a/c/d;->c()V

    .line 398
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 488
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    invoke-interface {v0}, Lcom/squareup/a/a/c/d;->a()V

    .line 489
    iget-object v0, p0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    iget-object v1, p0, Lcom/squareup/a/a/c/ac;->e:Lcom/squareup/a/a/c/y;

    invoke-interface {v0, v1}, Lcom/squareup/a/a/c/d;->a(Lcom/squareup/a/a/c/y;)V

    .line 490
    return-void
.end method
