.class final Lcom/squareup/a/a/c/aa;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/c/b;


# instance fields
.field private final a:Lcom/squareup/a/a/b/c;

.field private final b:Z

.field private final c:Lcom/squareup/a/a/c/s;


# direct methods
.method constructor <init>(Lcom/squareup/a/a/b/c;Z)V
    .locals 2

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput-object p1, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    .line 118
    new-instance v0, Lcom/squareup/a/a/c/s;

    iget-object v1, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-direct {v0, v1}, Lcom/squareup/a/a/c/s;-><init>(Lcom/squareup/a/a/b/c;)V

    iput-object v0, p0, Lcom/squareup/a/a/c/aa;->c:Lcom/squareup/a/a/c/s;

    .line 119
    iput-boolean p2, p0, Lcom/squareup/a/a/c/aa;->b:Z

    .line 120
    return-void
.end method

.method private static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;
    .locals 2

    .prologue
    .line 284
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Lcom/squareup/a/a/c/c;II)V
    .locals 8

    .prologue
    const v6, 0x7fffffff

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 197
    iget-object v2, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v2}, Lcom/squareup/a/a/b/c;->i()I

    move-result v2

    .line 198
    iget-object v3, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v3}, Lcom/squareup/a/a/b/c;->i()I

    move-result v4

    .line 199
    iget-object v3, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v3}, Lcom/squareup/a/a/b/c;->g()S

    move-result v5

    .line 200
    and-int v3, v2, v6

    .line 201
    and-int/2addr v4, v6

    .line 202
    const v2, 0xe000

    and-int/2addr v2, v5

    ushr-int/lit8 v5, v2, 0xd

    .line 204
    iget-object v2, p0, Lcom/squareup/a/a/c/aa;->c:Lcom/squareup/a/a/c/s;

    add-int/lit8 v6, p3, -0xa

    invoke-virtual {v2, v6}, Lcom/squareup/a/a/c/s;->a(I)Ljava/util/List;

    move-result-object v6

    .line 206
    and-int/lit8 v2, p2, 0x1

    if-eqz v2, :cond_0

    move v2, v1

    .line 207
    :goto_0
    and-int/lit8 v7, p2, 0x2

    if-eqz v7, :cond_1

    .line 208
    :goto_1
    sget-object v7, Lcom/squareup/a/a/c/f;->a:Lcom/squareup/a/a/c/f;

    move-object v0, p1

    invoke-interface/range {v0 .. v7}, Lcom/squareup/a/a/c/c;->a(ZZIIILjava/util/List;Lcom/squareup/a/a/c/f;)V

    .line 210
    return-void

    :cond_0
    move v2, v0

    .line 206
    goto :goto_0

    :cond_1
    move v1, v0

    .line 207
    goto :goto_1
.end method

.method private b(Lcom/squareup/a/a/c/c;II)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 213
    iget-object v0, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    .line 214
    const v2, 0x7fffffff

    and-int v3, v0, v2

    .line 215
    iget-object v0, p0, Lcom/squareup/a/a/c/aa;->c:Lcom/squareup/a/a/c/s;

    add-int/lit8 v2, p3, -0x4

    invoke-virtual {v0, v2}, Lcom/squareup/a/a/c/s;->a(I)Ljava/util/List;

    move-result-object v6

    .line 216
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    .line 217
    :goto_0
    sget-object v7, Lcom/squareup/a/a/c/f;->b:Lcom/squareup/a/a/c/f;

    move-object v0, p1

    move v5, v4

    invoke-interface/range {v0 .. v7}, Lcom/squareup/a/a/c/c;->a(ZZIIILjava/util/List;Lcom/squareup/a/a/c/f;)V

    .line 218
    return-void

    :cond_0
    move v2, v1

    .line 216
    goto :goto_0
.end method

.method private c(Lcom/squareup/a/a/c/c;II)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 221
    const/16 v0, 0x8

    if-eq p3, v0, :cond_0

    const-string/jumbo v0, "TYPE_RST_STREAM length: %d != 8"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    .line 223
    iget-object v1, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v1}, Lcom/squareup/a/a/b/c;->i()I

    move-result v1

    .line 224
    invoke-static {v1}, Lcom/squareup/a/a/c/a;->a(I)Lcom/squareup/a/a/c/a;

    move-result-object v2

    .line 225
    if-nez v2, :cond_1

    .line 226
    const-string/jumbo v0, "TYPE_RST_STREAM unexpected error code: %d"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lcom/squareup/a/a/c/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 228
    :cond_1
    invoke-interface {p1, v0, v2}, Lcom/squareup/a/a/c/c;->a(ILcom/squareup/a/a/c/a;)V

    .line 229
    return-void
.end method

.method private d(Lcom/squareup/a/a/c/c;II)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 232
    iget-object v0, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    .line 233
    const v2, 0x7fffffff

    and-int v3, v0, v2

    .line 234
    iget-object v0, p0, Lcom/squareup/a/a/c/aa;->c:Lcom/squareup/a/a/c/s;

    add-int/lit8 v2, p3, -0x4

    invoke-virtual {v0, v2}, Lcom/squareup/a/a/c/s;->a(I)Ljava/util/List;

    move-result-object v6

    .line 235
    sget-object v7, Lcom/squareup/a/a/c/f;->c:Lcom/squareup/a/a/c/f;

    move-object v0, p1

    move v2, v1

    move v5, v4

    invoke-interface/range {v0 .. v7}, Lcom/squareup/a/a/c/c;->a(ZZIIILjava/util/List;Lcom/squareup/a/a/c/f;)V

    .line 236
    return-void
.end method

.method private e(Lcom/squareup/a/a/c/c;II)V
    .locals 8

    .prologue
    const v2, 0x7fffffff

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 239
    const/16 v0, 0x8

    if-eq p3, v0, :cond_0

    const-string/jumbo v0, "TYPE_WINDOW_UPDATE length: %d != 8"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    .line 241
    iget-object v1, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v1}, Lcom/squareup/a/a/b/c;->i()I

    move-result v1

    .line 242
    and-int/2addr v0, v2

    .line 243
    and-int/2addr v1, v2

    int-to-long v2, v1

    .line 244
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    const-string/jumbo v0, "windowSizeIncrement was 0"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 245
    :cond_1
    invoke-interface {p1, v0, v2, v3}, Lcom/squareup/a/a/c/c;->a(IJ)V

    .line 246
    return-void
.end method

.method private f(Lcom/squareup/a/a/c/c;II)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 249
    const/4 v2, 0x4

    if-eq p3, v2, :cond_0

    const-string/jumbo v2, "TYPE_PING length: %d != 4"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v2, v0}, Lcom/squareup/a/a/c/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 250
    :cond_0
    iget-object v2, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v2}, Lcom/squareup/a/a/b/c;->i()I

    move-result v3

    .line 251
    iget-boolean v4, p0, Lcom/squareup/a/a/c/aa;->b:Z

    and-int/lit8 v2, v3, 0x1

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_0
    if-ne v4, v2, :cond_2

    .line 252
    :goto_1
    invoke-interface {p1, v0, v3, v1}, Lcom/squareup/a/a/c/c;->a(ZII)V

    .line 253
    return-void

    :cond_1
    move v2, v1

    .line 251
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private g(Lcom/squareup/a/a/c/c;II)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 256
    const/16 v0, 0x8

    if-eq p3, v0, :cond_0

    const-string/jumbo v0, "TYPE_GOAWAY length: %d != 8"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    .line 258
    iget-object v1, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v1}, Lcom/squareup/a/a/b/c;->i()I

    move-result v1

    .line 259
    invoke-static {v1}, Lcom/squareup/a/a/c/a;->c(I)Lcom/squareup/a/a/c/a;

    move-result-object v2

    .line 260
    if-nez v2, :cond_1

    .line 261
    const-string/jumbo v0, "TYPE_GOAWAY unexpected error code: %d"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lcom/squareup/a/a/c/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 263
    :cond_1
    sget-object v1, Lcom/squareup/a/a/b/d;->a:Lcom/squareup/a/a/b/d;

    invoke-interface {p1, v0, v2, v1}, Lcom/squareup/a/a/c/c;->a(ILcom/squareup/a/a/c/a;Lcom/squareup/a/a/b/d;)V

    .line 264
    return-void
.end method

.method private h(Lcom/squareup/a/a/c/c;II)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 267
    iget-object v2, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v2}, Lcom/squareup/a/a/b/c;->i()I

    move-result v3

    .line 268
    mul-int/lit8 v2, v3, 0x8

    add-int/lit8 v2, v2, 0x4

    if-eq p3, v2, :cond_0

    .line 269
    const-string/jumbo v2, "TYPE_SETTINGS length: %d != 4 + 8 * %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v2, v4}, Lcom/squareup/a/a/c/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 271
    :cond_0
    new-instance v4, Lcom/squareup/a/a/c/y;

    invoke-direct {v4}, Lcom/squareup/a/a/c/y;-><init>()V

    move v2, v1

    .line 272
    :goto_0
    if-ge v2, v3, :cond_1

    .line 273
    iget-object v5, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v5}, Lcom/squareup/a/a/b/c;->i()I

    move-result v5

    .line 274
    iget-object v6, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v6}, Lcom/squareup/a/a/b/c;->i()I

    move-result v6

    .line 275
    const/high16 v7, -0x1000000

    and-int/2addr v7, v5

    ushr-int/lit8 v7, v7, 0x18

    .line 276
    const v8, 0xffffff

    and-int/2addr v5, v8

    .line 277
    invoke-virtual {v4, v5, v7, v6}, Lcom/squareup/a/a/c/y;->a(III)Lcom/squareup/a/a/c/y;

    .line 272
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 279
    :cond_1
    and-int/lit8 v2, p2, 0x1

    if-eqz v2, :cond_2

    .line 280
    :goto_1
    invoke-interface {p1, v0, v4}, Lcom/squareup/a/a/c/c;->a(ZLcom/squareup/a/a/c/y;)V

    .line 281
    return-void

    :cond_2
    move v0, v1

    .line 279
    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method public a(Lcom/squareup/a/a/c/c;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 133
    :try_start_0
    iget-object v2, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v2}, Lcom/squareup/a/a/b/c;->i()I

    move-result v3

    .line 134
    iget-object v2, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v2}, Lcom/squareup/a/a/b/c;->i()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 139
    const/high16 v2, -0x80000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    move v2, v1

    .line 140
    :goto_0
    const/high16 v5, -0x1000000

    and-int/2addr v5, v4

    ushr-int/lit8 v5, v5, 0x18

    .line 141
    const v6, 0xffffff

    and-int/2addr v4, v6

    .line 143
    if-eqz v2, :cond_2

    .line 144
    const/high16 v0, 0x7fff0000

    and-int/2addr v0, v3

    ushr-int/lit8 v0, v0, 0x10

    .line 145
    const v2, 0xffff

    and-int/2addr v2, v3

    .line 147
    const/4 v3, 0x3

    if-eq v0, v3, :cond_1

    .line 148
    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "version != 3: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135
    :catch_0
    move-exception v1

    move v1, v0

    .line 192
    :goto_1
    return v1

    :cond_0
    move v2, v0

    .line 139
    goto :goto_0

    .line 151
    :cond_1
    packed-switch v2, :pswitch_data_0

    .line 185
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    int-to-long v2, v4

    invoke-interface {v0, v2, v3}, Lcom/squareup/a/a/b/c;->b(J)V

    goto :goto_1

    .line 153
    :pswitch_1
    invoke-direct {p0, p1, v5, v4}, Lcom/squareup/a/a/c/aa;->a(Lcom/squareup/a/a/c/c;II)V

    goto :goto_1

    .line 157
    :pswitch_2
    invoke-direct {p0, p1, v5, v4}, Lcom/squareup/a/a/c/aa;->b(Lcom/squareup/a/a/c/c;II)V

    goto :goto_1

    .line 161
    :pswitch_3
    invoke-direct {p0, p1, v5, v4}, Lcom/squareup/a/a/c/aa;->c(Lcom/squareup/a/a/c/c;II)V

    goto :goto_1

    .line 165
    :pswitch_4
    invoke-direct {p0, p1, v5, v4}, Lcom/squareup/a/a/c/aa;->h(Lcom/squareup/a/a/c/c;II)V

    goto :goto_1

    .line 169
    :pswitch_5
    invoke-direct {p0, p1, v5, v4}, Lcom/squareup/a/a/c/aa;->f(Lcom/squareup/a/a/c/c;II)V

    goto :goto_1

    .line 173
    :pswitch_6
    invoke-direct {p0, p1, v5, v4}, Lcom/squareup/a/a/c/aa;->g(Lcom/squareup/a/a/c/c;II)V

    goto :goto_1

    .line 177
    :pswitch_7
    invoke-direct {p0, p1, v5, v4}, Lcom/squareup/a/a/c/aa;->d(Lcom/squareup/a/a/c/c;II)V

    goto :goto_1

    .line 181
    :pswitch_8
    invoke-direct {p0, p1, v5, v4}, Lcom/squareup/a/a/c/aa;->e(Lcom/squareup/a/a/c/c;II)V

    goto :goto_1

    .line 189
    :cond_2
    const v2, 0x7fffffff

    and-int/2addr v2, v3

    .line 190
    and-int/lit8 v3, v5, 0x1

    if-eqz v3, :cond_3

    move v0, v1

    .line 191
    :cond_3
    iget-object v3, p0, Lcom/squareup/a/a/c/aa;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {p1, v0, v2, v3, v4}, Lcom/squareup/a/a/c/c;->a(ZILcom/squareup/a/a/b/c;I)V

    goto :goto_1

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public close()V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/squareup/a/a/c/aa;->c:Lcom/squareup/a/a/c/s;

    invoke-virtual {v0}, Lcom/squareup/a/a/c/s;->a()V

    .line 289
    return-void
.end method
