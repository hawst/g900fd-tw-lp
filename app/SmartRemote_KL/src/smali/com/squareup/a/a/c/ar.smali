.class final Lcom/squareup/a/a/c/ar;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/w;


# static fields
.field static final synthetic a:Z


# instance fields
.field final synthetic b:Lcom/squareup/a/a/c/ao;

.field private final c:Lcom/squareup/a/a/b/j;

.field private final d:Lcom/squareup/a/a/b/j;

.field private final e:J

.field private f:Z

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 339
    const-class v0, Lcom/squareup/a/a/c/ao;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/squareup/a/a/c/ar;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/squareup/a/a/c/ao;J)V
    .locals 2

    .prologue
    .line 358
    iput-object p1, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341
    new-instance v0, Lcom/squareup/a/a/b/j;

    invoke-direct {v0}, Lcom/squareup/a/a/b/j;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/c/ar;->c:Lcom/squareup/a/a/b/j;

    .line 344
    new-instance v0, Lcom/squareup/a/a/b/j;

    invoke-direct {v0}, Lcom/squareup/a/a/b/j;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/c/ar;->d:Lcom/squareup/a/a/b/j;

    .line 359
    iput-wide p2, p0, Lcom/squareup/a/a/c/ar;->e:J

    .line 360
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/a/a/c/ao;JLcom/squareup/a/a/c/ap;)V
    .locals 0

    .prologue
    .line 339
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/a/a/c/ar;-><init>(Lcom/squareup/a/a/c/ao;J)V

    return-void
.end method

.method private a()V
    .locals 10

    .prologue
    const-wide/32 v4, 0xf4240

    const-wide/16 v2, 0x0

    .line 403
    .line 405
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Lcom/squareup/a/a/c/ao;->c(Lcom/squareup/a/a/c/ao;)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 406
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    div-long v4, v0, v4

    .line 407
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Lcom/squareup/a/a/c/ao;->c(Lcom/squareup/a/a/c/ao;)J

    move-result-wide v0

    .line 410
    :goto_0
    :try_start_0
    iget-object v6, p0, Lcom/squareup/a/a/c/ar;->d:Lcom/squareup/a/a/b/j;

    invoke-virtual {v6}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/squareup/a/a/c/ar;->g:Z

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/squareup/a/a/c/ar;->f:Z

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v6}, Lcom/squareup/a/a/c/ao;->d(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/a;

    move-result-object v6

    if-nez v6, :cond_2

    .line 411
    iget-object v6, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v6}, Lcom/squareup/a/a/c/ao;->c(Lcom/squareup/a/a/c/ao;)J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-nez v6, :cond_0

    .line 412
    iget-object v6, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-virtual {v6}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 420
    :catch_0
    move-exception v0

    .line 421
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 413
    :cond_0
    cmp-long v6, v0, v2

    if-lez v6, :cond_1

    .line 414
    :try_start_1
    iget-object v6, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-virtual {v6, v0, v1}, Ljava/lang/Object;->wait(J)V

    .line 415
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Lcom/squareup/a/a/c/ao;->c(Lcom/squareup/a/a/c/ao;)J

    move-result-wide v0

    add-long/2addr v0, v4

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    const-wide/32 v8, 0xf4240

    div-long/2addr v6, v8

    sub-long/2addr v0, v6

    goto :goto_0

    .line 417
    :cond_1
    new-instance v0, Ljava/net/SocketTimeoutException;

    const-string/jumbo v1, "Read timed out"

    invoke-direct {v0, v1}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 423
    :cond_2
    return-void

    :cond_3
    move-wide v0, v2

    move-wide v4, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ar;)Z
    .locals 1

    .prologue
    .line 339
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ar;->g:Z

    return v0
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ar;Z)Z
    .locals 0

    .prologue
    .line 339
    iput-boolean p1, p0, Lcom/squareup/a/a/c/ar;->g:Z

    return p1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 480
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ar;->f:Z

    if-eqz v0, :cond_0

    .line 481
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Lcom/squareup/a/a/c/ao;->d(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 484
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stream was reset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v2}, Lcom/squareup/a/a/c/ao;->d(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 486
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/squareup/a/a/c/ar;)Z
    .locals 1

    .prologue
    .line 339
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ar;->f:Z

    return v0
.end method


# virtual methods
.method a(Lcom/squareup/a/a/b/c;J)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 426
    sget-boolean v0, Lcom/squareup/a/a/c/ar;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 452
    :cond_0
    sub-long/2addr p2, v4

    .line 455
    iget-object v3, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    monitor-enter v3

    .line 456
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->d:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v4

    cmp-long v0, v4, v10

    if-nez v0, :cond_7

    move v0, v1

    .line 457
    :goto_0
    iget-object v4, p0, Lcom/squareup/a/a/c/ar;->d:Lcom/squareup/a/a/b/j;

    iget-object v5, p0, Lcom/squareup/a/a/c/ar;->c:Lcom/squareup/a/a/b/j;

    iget-object v6, p0, Lcom/squareup/a/a/c/ar;->c:Lcom/squareup/a/a/b/j;

    invoke-virtual {v6}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Lcom/squareup/a/a/b/j;->a(Lcom/squareup/a/a/b/j;J)V

    .line 458
    if-eqz v0, :cond_1

    .line 459
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 461
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 428
    :cond_2
    cmp-long v0, p2, v10

    if-lez v0, :cond_3

    .line 431
    iget-object v3, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    monitor-enter v3

    .line 432
    :try_start_1
    iget-boolean v4, p0, Lcom/squareup/a/a/c/ar;->g:Z

    .line 433
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->d:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v6

    add-long/2addr v6, p2

    iget-wide v8, p0, Lcom/squareup/a/a/c/ar;->e:J

    cmp-long v0, v6, v8

    if-lez v0, :cond_4

    move v0, v1

    .line 434
    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 437
    if-eqz v0, :cond_5

    .line 438
    invoke-interface {p1, p2, p3}, Lcom/squareup/a/a/b/c;->b(J)V

    .line 439
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    sget-object v1, Lcom/squareup/a/a/c/a;->h:Lcom/squareup/a/a/c/a;

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/c/ao;->b(Lcom/squareup/a/a/c/a;)V

    .line 463
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v2

    .line 433
    goto :goto_1

    .line 434
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 444
    :cond_5
    if-eqz v4, :cond_6

    .line 445
    invoke-interface {p1, p2, p3}, Lcom/squareup/a/a/b/c;->b(J)V

    goto :goto_2

    .line 450
    :cond_6
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->c:Lcom/squareup/a/a/b/j;

    invoke-interface {p1, v0, p2, p3}, Lcom/squareup/a/a/b/c;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v4

    .line 451
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_7
    move v0, v2

    .line 456
    goto :goto_0

    .line 461
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public b(Lcom/squareup/a/a/b/j;J)J
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 364
    cmp-long v0, p2, v4

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_0
    iget-object v2, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    monitor-enter v2

    .line 368
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/a/a/c/ar;->a()V

    .line 369
    invoke-direct {p0}, Lcom/squareup/a/a/c/ar;->b()V

    .line 370
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->d:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    const-wide/16 v0, -0x1

    monitor-exit v2

    .line 394
    :goto_0
    return-wide v0

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->d:Lcom/squareup/a/a/b/j;

    iget-object v1, p0, Lcom/squareup/a/a/c/ar;->d:Lcom/squareup/a/a/b/j;

    invoke-virtual {v1}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v4

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-virtual {v0, p1, v4, v5}, Lcom/squareup/a/a/b/j;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v0

    .line 376
    iget-object v3, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    iget-wide v4, v3, Lcom/squareup/a/a/c/ao;->a:J

    add-long/2addr v4, v0

    iput-wide v4, v3, Lcom/squareup/a/a/c/ao;->a:J

    .line 377
    iget-object v3, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    iget-wide v4, v3, Lcom/squareup/a/a/c/ao;->a:J

    iget-object v3, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v3}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/a/a/c/ac;->f:Lcom/squareup/a/a/c/y;

    const/high16 v6, 0x10000

    invoke-virtual {v3, v6}, Lcom/squareup/a/a/c/y;->d(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-ltz v3, :cond_2

    .line 379
    iget-object v3, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v3}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v4}, Lcom/squareup/a/a/c/ao;->b(Lcom/squareup/a/a/c/ao;)I

    move-result v4

    iget-object v5, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    iget-wide v6, v5, Lcom/squareup/a/a/c/ao;->a:J

    invoke-virtual {v3, v4, v6, v7}, Lcom/squareup/a/a/c/ac;->a(IJ)V

    .line 380
    iget-object v3, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    const-wide/16 v4, 0x0

    iput-wide v4, v3, Lcom/squareup/a/a/c/ao;->a:J

    .line 382
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 385
    iget-object v2, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v2}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v2

    monitor-enter v2

    .line 386
    :try_start_1
    iget-object v3, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v3}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v3

    iget-wide v4, v3, Lcom/squareup/a/a/c/ac;->c:J

    add-long/2addr v4, v0

    iput-wide v4, v3, Lcom/squareup/a/a/c/ac;->c:J

    .line 387
    iget-object v3, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v3}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v3

    iget-wide v4, v3, Lcom/squareup/a/a/c/ac;->c:J

    iget-object v3, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v3}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/a/a/c/ac;->f:Lcom/squareup/a/a/c/y;

    const/high16 v6, 0x10000

    invoke-virtual {v3, v6}, Lcom/squareup/a/a/c/y;->d(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-ltz v3, :cond_3

    .line 389
    iget-object v3, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v3}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v5}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v5

    iget-wide v6, v5, Lcom/squareup/a/a/c/ac;->c:J

    invoke-virtual {v3, v4, v6, v7}, Lcom/squareup/a/a/c/ac;->a(IJ)V

    .line 390
    iget-object v3, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v3}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v3

    const-wide/16 v4, 0x0

    iput-wide v4, v3, Lcom/squareup/a/a/c/ac;->c:J

    .line 392
    :cond_3
    monitor-exit v2

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 382
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 471
    iget-object v1, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    monitor-enter v1

    .line 472
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/squareup/a/a/c/ar;->f:Z

    .line 473
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->d:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->o()V

    .line 474
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 475
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 476
    iget-object v0, p0, Lcom/squareup/a/a/c/ar;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Lcom/squareup/a/a/c/ao;->e(Lcom/squareup/a/a/c/ao;)V

    .line 477
    return-void

    .line 475
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
