.class final Lcom/squareup/a/a/b/r;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/c;


# instance fields
.field public final a:Lcom/squareup/a/a/b/j;

.field public final b:Lcom/squareup/a/a/b/w;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/squareup/a/a/b/w;)V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/squareup/a/a/b/j;

    invoke-direct {v0}, Lcom/squareup/a/a/b/j;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/squareup/a/a/b/r;-><init>(Lcom/squareup/a/a/b/w;Lcom/squareup/a/a/b/j;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/squareup/a/a/b/w;Lcom/squareup/a/a/b/j;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_0
    iput-object p2, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    .line 32
    iput-object p1, p0, Lcom/squareup/a/a/b/r;->b:Lcom/squareup/a/a/b/w;

    .line 33
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/squareup/a/a/b/r;->c:Z

    if-eqz v0, :cond_0

    .line 216
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/b/r;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/squareup/a/a/b/r;->c:Z

    return v0
.end method


# virtual methods
.method public a(B)J
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 142
    invoke-direct {p0}, Lcom/squareup/a/a/b/r;->a()V

    .line 143
    const-wide/16 v0, 0x0

    .line 145
    :cond_0
    iget-object v2, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v2, p1, v0, v1}, Lcom/squareup/a/a/b/j;->a(BJ)J

    move-result-wide v0

    cmp-long v2, v0, v6

    if-nez v2, :cond_1

    .line 146
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    iget-wide v0, v0, Lcom/squareup/a/a/b/j;->b:J

    .line 147
    iget-object v2, p0, Lcom/squareup/a/a/b/r;->b:Lcom/squareup/a/a/b/w;

    iget-object v3, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    const-wide/16 v4, 0x800

    invoke-interface {v2, v3, v4, v5}, Lcom/squareup/a/a/b/w;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 149
    :cond_1
    return-wide v0
.end method

.method public a(Z)Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const-wide/16 v8, 0x1

    const-wide/16 v2, 0x0

    .line 84
    invoke-direct {p0}, Lcom/squareup/a/a/b/r;->a()V

    move-wide v0, v2

    .line 87
    :cond_0
    iget-object v4, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    const/16 v5, 0xa

    invoke-virtual {v4, v5, v0, v1}, Lcom/squareup/a/a/b/j;->a(BJ)J

    move-result-wide v0

    cmp-long v4, v0, v10

    if-nez v4, :cond_3

    .line 88
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    iget-wide v0, v0, Lcom/squareup/a/a/b/j;->b:J

    .line 89
    iget-object v4, p0, Lcom/squareup/a/a/b/r;->b:Lcom/squareup/a/a/b/w;

    iget-object v5, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    const-wide/16 v6, 0x800

    invoke-interface {v4, v5, v6, v7}, Lcom/squareup/a/a/b/w;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v4

    cmp-long v4, v4, v10

    if-nez v4, :cond_0

    .line 90
    if-eqz p1, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    iget-wide v0, v0, Lcom/squareup/a/a/b/j;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    iget-wide v0, v0, Lcom/squareup/a/a/b/j;->b:J

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/b/r;->d(J)Ljava/lang/String;

    move-result-object v0

    .line 105
    :goto_0
    return-object v0

    .line 91
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 95
    :cond_3
    cmp-long v2, v0, v2

    if-lez v2, :cond_4

    iget-object v2, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    sub-long v4, v0, v8

    invoke-virtual {v2, v4, v5}, Lcom/squareup/a/a/b/j;->d(J)B

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_4

    .line 97
    sub-long/2addr v0, v8

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/b/r;->d(J)Ljava/lang/String;

    move-result-object v0

    .line 98
    const-wide/16 v2, 0x2

    invoke-virtual {p0, v2, v3}, Lcom/squareup/a/a/b/r;->b(J)V

    goto :goto_0

    .line 103
    :cond_4
    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/b/r;->d(J)Ljava/lang/String;

    move-result-object v0

    .line 104
    invoke-virtual {p0, v8, v9}, Lcom/squareup/a/a/b/r;->b(J)V

    goto :goto_0
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/squareup/a/a/b/r;->a()V

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    iget-wide v0, v0, Lcom/squareup/a/a/b/j;->b:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->b:Lcom/squareup/a/a/b/w;

    iget-object v1, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/a/a/b/w;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 66
    :cond_1
    return-void
.end method

.method public b(Lcom/squareup/a/a/b/j;J)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v0, -0x1

    .line 44
    cmp-long v2, p2, v4

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    invoke-direct {p0}, Lcom/squareup/a/a/b/r;->a()V

    .line 47
    iget-object v2, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    iget-wide v2, v2, Lcom/squareup/a/a/b/j;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 48
    iget-object v2, p0, Lcom/squareup/a/a/b/r;->b:Lcom/squareup/a/a/b/w;

    iget-object v3, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    const-wide/16 v4, 0x800

    invoke-interface {v2, v3, v4, v5}, Lcom/squareup/a/a/b/w;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v2

    .line 49
    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    .line 53
    :goto_0
    return-wide v0

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    iget-wide v0, v0, Lcom/squareup/a/a/b/j;->b:J

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 53
    iget-object v2, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v2, p1, v0, v1}, Lcom/squareup/a/a/b/j;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public b()Lcom/squareup/a/a/b/j;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    return-object v0
.end method

.method public b(J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 130
    invoke-direct {p0}, Lcom/squareup/a/a/b/r;->a()V

    .line 131
    :goto_0
    cmp-long v0, p1, v4

    if-lez v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    iget-wide v0, v0, Lcom/squareup/a/a/b/j;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/b/r;->b:Lcom/squareup/a/a/b/w;

    iget-object v1, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/a/a/b/w;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 136
    iget-object v2, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v2, v0, v1}, Lcom/squareup/a/a/b/j;->b(J)V

    .line 137
    sub-long/2addr p1, v0

    .line 138
    goto :goto_0

    .line 139
    :cond_1
    return-void
.end method

.method public c(J)Lcom/squareup/a/a/b/d;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0, p1, p2}, Lcom/squareup/a/a/b/r;->a(J)V

    .line 75
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/a/a/b/j;->c(J)Lcom/squareup/a/a/b/d;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/squareup/a/a/b/r;->c:Z

    if-eqz v0, :cond_0

    .line 208
    :goto_0
    return-void

    .line 205
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/a/a/b/r;->c:Z

    .line 206
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->b:Lcom/squareup/a/a/b/w;

    invoke-interface {v0}, Lcom/squareup/a/a/b/w;->close()V

    .line 207
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->o()V

    goto :goto_0
.end method

.method public d(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0, p1, p2}, Lcom/squareup/a/a/b/r;->a(J)V

    .line 80
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/a/a/b/j;->e(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 4

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/squareup/a/a/b/r;->a()V

    .line 58
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/b/r;->b:Lcom/squareup/a/a/b/w;

    iget-object v1, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/a/a/b/w;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()B
    .locals 2

    .prologue
    .line 69
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/b/r;->a(J)V

    .line 70
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->f()B

    move-result v0

    return v0
.end method

.method public g()S
    .locals 2

    .prologue
    .line 110
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/b/r;->a(J)V

    .line 111
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->g()S

    move-result v0

    return v0
.end method

.method public h()I
    .locals 2

    .prologue
    .line 115
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/b/r;->a(J)V

    .line 116
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->h()I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 120
    const-wide/16 v0, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/b/r;->a(J)V

    .line 121
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->i()I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 125
    const-wide/16 v0, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/b/r;->a(J)V

    .line 126
    iget-object v0, p0, Lcom/squareup/a/a/b/r;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->j()I

    move-result v0

    return v0
.end method

.method public k()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 153
    new-instance v0, Lcom/squareup/a/a/b/s;

    invoke-direct {v0, p0}, Lcom/squareup/a/a/b/s;-><init>(Lcom/squareup/a/a/b/r;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "buffer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/b/r;->b:Lcom/squareup/a/a/b/w;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
