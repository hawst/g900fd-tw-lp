.class public final Lcom/squareup/a/a/a/at;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/a/aw;


# instance fields
.field private final a:Lcom/squareup/a/a/a/u;

.field private final b:Lcom/squareup/a/a/c/ac;

.field private c:Lcom/squareup/a/a/c/ao;


# direct methods
.method public constructor <init>(Lcom/squareup/a/a/a/u;Lcom/squareup/a/a/c/ac;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/a/a/a/at;->a:Lcom/squareup/a/a/a/u;

    .line 56
    iput-object p2, p0, Lcom/squareup/a/a/a/at;->b:Lcom/squareup/a/a/c/ac;

    .line 57
    return-void
.end method

.method public static a(Ljava/util/List;Lcom/squareup/a/v;)Lcom/squareup/a/a/a/al;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;",
            "Lcom/squareup/a/v;",
            ")",
            "Lcom/squareup/a/a/a/al;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 157
    const/4 v2, 0x0

    .line 158
    const-string/jumbo v1, "HTTP/1.1"

    .line 160
    new-instance v6, Lcom/squareup/a/a/a/h;

    invoke-direct {v6}, Lcom/squareup/a/a/a/h;-><init>()V

    .line 161
    sget-object v0, Lcom/squareup/a/a/a/aa;->e:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/a/v;->d:Lcom/squareup/a/a/b/d;

    invoke-virtual {v4}, Lcom/squareup/a/a/b/d;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v0, v4}, Lcom/squareup/a/a/a/h;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    move v5, v3

    .line 162
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_5

    .line 163
    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/e;

    iget-object v7, v0, Lcom/squareup/a/a/c/e;->h:Lcom/squareup/a/a/b/d;

    .line 164
    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/e;

    iget-object v0, v0, Lcom/squareup/a/a/c/e;->i:Lcom/squareup/a/a/b/d;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/d;->a()Ljava/lang/String;

    move-result-object v8

    move-object v0, v1

    move v1, v3

    .line 165
    :goto_1
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 166
    invoke-virtual {v8, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 167
    const/4 v9, -0x1

    if-ne v4, v9, :cond_0

    .line 168
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v4

    .line 170
    :cond_0
    invoke-virtual {v8, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 171
    sget-object v9, Lcom/squareup/a/a/c/e;->a:Lcom/squareup/a/a/b/d;

    invoke-virtual {v7, v9}, Lcom/squareup/a/a/b/d;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 178
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v10, v2

    move-object v2, v1

    move v1, v10

    .line 179
    goto :goto_1

    .line 173
    :cond_1
    sget-object v9, Lcom/squareup/a/a/c/e;->g:Lcom/squareup/a/a/b/d;

    invoke-virtual {v7, v9}, Lcom/squareup/a/a/b/d;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move-object v0, v1

    move-object v1, v2

    .line 174
    goto :goto_2

    .line 175
    :cond_2
    invoke-static {p1, v7}, Lcom/squareup/a/a/a/at;->a(Lcom/squareup/a/v;Lcom/squareup/a/a/b/d;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 176
    invoke-virtual {v7}, Lcom/squareup/a/a/b/d;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9, v1}, Lcom/squareup/a/a/a/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    :cond_3
    move-object v1, v2

    goto :goto_2

    .line 162
    :cond_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move-object v1, v0

    goto :goto_0

    .line 181
    :cond_5
    if-nez v2, :cond_6

    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "Expected \':status\' header not present"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    :cond_6
    if-nez v1, :cond_7

    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "Expected \':version\' header not present"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_7
    new-instance v0, Lcom/squareup/a/a/a/al;

    invoke-direct {v0}, Lcom/squareup/a/a/a/al;-><init>()V

    new-instance v3, Lcom/squareup/a/a/a/av;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/squareup/a/a/a/av;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/av;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    invoke-virtual {v6}, Lcom/squareup/a/a/a/h;->a()Lcom/squareup/a/a/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/f;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/v;Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/a/a/a/ac;",
            "Lcom/squareup/a/v;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 97
    invoke-virtual {p0}, Lcom/squareup/a/a/a/ac;->e()Lcom/squareup/a/a/a/f;

    move-result-object v4

    .line 99
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v4}, Lcom/squareup/a/a/a/f;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0xa

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 100
    new-instance v0, Lcom/squareup/a/a/c/e;

    sget-object v1, Lcom/squareup/a/a/c/e;->b:Lcom/squareup/a/a/b/d;

    invoke-virtual {p0}, Lcom/squareup/a/a/a/ac;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    new-instance v0, Lcom/squareup/a/a/c/e;

    sget-object v1, Lcom/squareup/a/a/c/e;->c:Lcom/squareup/a/a/b/d;

    invoke-virtual {p0}, Lcom/squareup/a/a/a/ac;->a()Ljava/net/URL;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/a/a/a/ah;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    invoke-virtual {p0}, Lcom/squareup/a/a/a/ac;->a()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/a/u;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v0

    .line 103
    sget-object v1, Lcom/squareup/a/v;->b:Lcom/squareup/a/v;

    if-ne v1, p1, :cond_1

    .line 104
    new-instance v1, Lcom/squareup/a/a/c/e;

    sget-object v3, Lcom/squareup/a/a/c/e;->g:Lcom/squareup/a/a/b/d;

    invoke-direct {v1, v3, p2}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    new-instance v1, Lcom/squareup/a/a/c/e;

    sget-object v3, Lcom/squareup/a/a/c/e;->f:Lcom/squareup/a/a/b/d;

    invoke-direct {v1, v3, v0}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    :goto_0
    new-instance v0, Lcom/squareup/a/a/c/e;

    sget-object v1, Lcom/squareup/a/a/c/e;->d:Lcom/squareup/a/a/b/d;

    invoke-virtual {p0}, Lcom/squareup/a/a/a/ac;->a()Ljava/net/URL;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    new-instance v6, Ljava/util/LinkedHashSet;

    invoke-direct {v6}, Ljava/util/LinkedHashSet;-><init>()V

    move v1, v2

    .line 114
    :goto_1
    invoke-virtual {v4}, Lcom/squareup/a/a/a/f;->a()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 116
    invoke-virtual {v4, v1}, Lcom/squareup/a/a/a/f;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/b/d;->a(Ljava/lang/String;)Lcom/squareup/a/a/b/d;

    move-result-object v7

    .line 117
    invoke-virtual {v4, v1}, Lcom/squareup/a/a/a/f;->b(I)Ljava/lang/String;

    move-result-object v8

    .line 120
    invoke-static {p1, v7}, Lcom/squareup/a/a/a/at;->a(Lcom/squareup/a/v;Lcom/squareup/a/a/b/d;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 114
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 106
    :cond_1
    sget-object v1, Lcom/squareup/a/v;->a:Lcom/squareup/a/v;

    if-ne v1, p1, :cond_2

    .line 107
    new-instance v1, Lcom/squareup/a/a/c/e;

    sget-object v3, Lcom/squareup/a/a/c/e;->e:Lcom/squareup/a/a/b/d;

    invoke-direct {v1, v3, v0}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 123
    :cond_3
    sget-object v0, Lcom/squareup/a/a/c/e;->b:Lcom/squareup/a/a/b/d;

    invoke-virtual {v7, v0}, Lcom/squareup/a/a/b/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/a/a/c/e;->c:Lcom/squareup/a/a/b/d;

    invoke-virtual {v7, v0}, Lcom/squareup/a/a/b/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/a/a/c/e;->d:Lcom/squareup/a/a/b/d;

    invoke-virtual {v7, v0}, Lcom/squareup/a/a/b/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/a/a/c/e;->e:Lcom/squareup/a/a/b/d;

    invoke-virtual {v7, v0}, Lcom/squareup/a/a/b/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/a/a/c/e;->f:Lcom/squareup/a/a/b/d;

    invoke-virtual {v7, v0}, Lcom/squareup/a/a/b/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/a/a/c/e;->g:Lcom/squareup/a/a/b/d;

    invoke-virtual {v7, v0}, Lcom/squareup/a/a/b/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 134
    new-instance v0, Lcom/squareup/a/a/c/e;

    invoke-direct {v0, v7, v8}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    move v3, v2

    .line 139
    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 140
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/e;

    iget-object v0, v0, Lcom/squareup/a/a/c/e;->h:Lcom/squareup/a/a/b/d;

    invoke-virtual {v0, v7}, Lcom/squareup/a/a/b/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 141
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/e;

    iget-object v0, v0, Lcom/squareup/a/a/c/e;->i:Lcom/squareup/a/a/b/d;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v8}, Lcom/squareup/a/a/a/at;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 142
    new-instance v8, Lcom/squareup/a/a/c/e;

    invoke-direct {v8, v7, v0}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Ljava/lang/String;)V

    invoke-interface {v5, v3, v8}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 139
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 147
    :cond_6
    return-object v5
.end method

.method private static a(Lcom/squareup/a/v;Lcom/squareup/a/a/b/d;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 210
    const/4 v0, 0x0

    .line 211
    sget-object v2, Lcom/squareup/a/v;->b:Lcom/squareup/a/v;

    if-ne p0, v2, :cond_2

    .line 213
    const-string/jumbo v2, "connection"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "host"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "keep-alive"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "proxy-connection"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "transfer-encoding"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 235
    :cond_1
    :goto_0
    return v0

    .line 220
    :cond_2
    sget-object v2, Lcom/squareup/a/v;->a:Lcom/squareup/a/v;

    if-ne p0, v2, :cond_4

    .line 222
    const-string/jumbo v2, "connection"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "host"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "keep-alive"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "proxy-connection"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "te"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "transfer-encoding"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "encoding"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "upgrade"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/b/d;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_3
    move v0, v1

    .line 230
    goto :goto_0

    .line 233
    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method


# virtual methods
.method public a(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/b/v;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/squareup/a/a/a/at;->b(Lcom/squareup/a/a/a/ac;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/a/a/a/at;->c:Lcom/squareup/a/a/c/ao;

    invoke-virtual {v0}, Lcom/squareup/a/a/c/ao;->f()Lcom/squareup/a/a/b/v;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/net/CacheRequest;)Lcom/squareup/a/a/b/w;
    .locals 2

    .prologue
    .line 194
    new-instance v0, Lcom/squareup/a/a/a/au;

    iget-object v1, p0, Lcom/squareup/a/a/a/at;->c:Lcom/squareup/a/a/c/ao;

    invoke-direct {v0, v1, p1}, Lcom/squareup/a/a/a/au;-><init>(Lcom/squareup/a/a/c/ao;Ljava/net/CacheRequest;)V

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/squareup/a/a/a/at;->c:Lcom/squareup/a/a/c/ao;

    invoke-virtual {v0}, Lcom/squareup/a/a/c/ao;->f()Lcom/squareup/a/a/b/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/a/a/b/v;->close()V

    .line 84
    return-void
.end method

.method public a(Lcom/squareup/a/a/a/ar;)V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lcom/squareup/a/a/a/u;)V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/squareup/a/a/a/at;->c:Lcom/squareup/a/a/c/ao;

    sget-object v1, Lcom/squareup/a/a/c/a;->l:Lcom/squareup/a/a/c/a;

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/a;)V

    .line 202
    return-void
.end method

.method public b()Lcom/squareup/a/a/a/al;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/squareup/a/a/a/at;->c:Lcom/squareup/a/a/c/ao;

    invoke-virtual {v0}, Lcom/squareup/a/a/c/ao;->c()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/a/at;->b:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v1}, Lcom/squareup/a/a/c/ac;->a()Lcom/squareup/a/v;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/a/a/a/at;->a(Ljava/util/List;Lcom/squareup/a/v;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/squareup/a/a/a/ac;)V
    .locals 5

    .prologue
    .line 66
    iget-object v0, p0, Lcom/squareup/a/a/a/at;->c:Lcom/squareup/a/a/c/ao;

    if-eqz v0, :cond_0

    .line 76
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/at;->a:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->b()V

    .line 69
    iget-object v0, p0, Lcom/squareup/a/a/a/at;->a:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->c()Z

    move-result v0

    .line 70
    const/4 v1, 0x1

    .line 71
    iget-object v2, p0, Lcom/squareup/a/a/a/at;->a:Lcom/squareup/a/a/a/u;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/u;->k()Lcom/squareup/a/j;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/a/j;->m()I

    move-result v2

    invoke-static {v2}, Lcom/squareup/a/a/a/ah;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 72
    iget-object v3, p0, Lcom/squareup/a/a/a/at;->b:Lcom/squareup/a/a/c/ac;

    iget-object v4, p0, Lcom/squareup/a/a/a/at;->b:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v4}, Lcom/squareup/a/a/c/ac;->a()Lcom/squareup/a/v;

    move-result-object v4

    invoke-static {p1, v4, v2}, Lcom/squareup/a/a/a/at;->a(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/v;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v3, v2, v0, v1}, Lcom/squareup/a/a/c/ac;->a(Ljava/util/List;ZZ)Lcom/squareup/a/a/c/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/at;->c:Lcom/squareup/a/a/c/ao;

    .line 75
    iget-object v0, p0, Lcom/squareup/a/a/a/at;->c:Lcom/squareup/a/a/c/ao;

    iget-object v1, p0, Lcom/squareup/a/a/a/at;->a:Lcom/squareup/a/a/a/u;

    iget-object v1, v1, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v1}, Lcom/squareup/a/r;->b()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/squareup/a/a/c/ao;->a(J)V

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x1

    return v0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 191
    return-void
.end method
