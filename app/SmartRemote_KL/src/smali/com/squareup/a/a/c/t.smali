.class Lcom/squareup/a/a/c/t;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/w;


# instance fields
.field final synthetic a:Lcom/squareup/a/a/b/c;

.field final synthetic b:Lcom/squareup/a/a/c/s;


# direct methods
.method constructor <init>(Lcom/squareup/a/a/c/s;Lcom/squareup/a/a/b/c;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/squareup/a/a/c/t;->b:Lcom/squareup/a/a/c/s;

    iput-object p2, p0, Lcom/squareup/a/a/c/t;->a:Lcom/squareup/a/a/b/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Lcom/squareup/a/a/b/j;J)J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 42
    iget-object v2, p0, Lcom/squareup/a/a/c/t;->b:Lcom/squareup/a/a/c/s;

    invoke-static {v2}, Lcom/squareup/a/a/c/s;->a(Lcom/squareup/a/a/c/s;)I

    move-result v2

    if-nez v2, :cond_1

    .line 46
    :cond_0
    :goto_0
    return-wide v0

    .line 43
    :cond_1
    iget-object v2, p0, Lcom/squareup/a/a/c/t;->a:Lcom/squareup/a/a/b/c;

    iget-object v3, p0, Lcom/squareup/a/a/c/t;->b:Lcom/squareup/a/a/c/s;

    invoke-static {v3}, Lcom/squareup/a/a/c/s;->a(Lcom/squareup/a/a/c/s;)I

    move-result v3

    int-to-long v4, v3

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface {v2, p1, v4, v5}, Lcom/squareup/a/a/b/c;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v2

    .line 44
    cmp-long v4, v2, v0

    if-eqz v4, :cond_0

    .line 45
    iget-object v0, p0, Lcom/squareup/a/a/c/t;->b:Lcom/squareup/a/a/c/s;

    invoke-static {v0, v2, v3}, Lcom/squareup/a/a/c/s;->a(Lcom/squareup/a/a/c/s;J)I

    move-wide v0, v2

    .line 46
    goto :goto_0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/squareup/a/a/c/t;->a:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->close()V

    .line 51
    return-void
.end method
