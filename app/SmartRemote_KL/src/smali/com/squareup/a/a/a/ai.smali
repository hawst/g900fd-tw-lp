.class public final Lcom/squareup/a/a/a/ai;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/squareup/a/a/a/ac;

.field private final b:Lcom/squareup/a/a/a/av;

.field private final c:Lcom/squareup/a/m;

.field private final d:Lcom/squareup/a/a/a/f;

.field private final e:Lcom/squareup/a/a/a/ak;

.field private final f:Lcom/squareup/a/a/a/ai;

.field private volatile g:Lcom/squareup/a/a/a/am;

.field private volatile h:Lcom/squareup/a/i;


# direct methods
.method private constructor <init>(Lcom/squareup/a/a/a/al;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-static {p1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/al;)Lcom/squareup/a/a/a/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ai;->a:Lcom/squareup/a/a/a/ac;

    .line 60
    invoke-static {p1}, Lcom/squareup/a/a/a/al;->b(Lcom/squareup/a/a/a/al;)Lcom/squareup/a/a/a/av;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ai;->b:Lcom/squareup/a/a/a/av;

    .line 61
    invoke-static {p1}, Lcom/squareup/a/a/a/al;->c(Lcom/squareup/a/a/a/al;)Lcom/squareup/a/m;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ai;->c:Lcom/squareup/a/m;

    .line 62
    invoke-static {p1}, Lcom/squareup/a/a/a/al;->d(Lcom/squareup/a/a/a/al;)Lcom/squareup/a/a/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/h;->a()Lcom/squareup/a/a/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ai;->d:Lcom/squareup/a/a/a/f;

    .line 63
    invoke-static {p1}, Lcom/squareup/a/a/a/al;->e(Lcom/squareup/a/a/a/al;)Lcom/squareup/a/a/a/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ai;->e:Lcom/squareup/a/a/a/ak;

    .line 64
    invoke-static {p1}, Lcom/squareup/a/a/a/al;->f(Lcom/squareup/a/a/a/al;)Lcom/squareup/a/a/a/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ai;->f:Lcom/squareup/a/a/a/ai;

    .line 65
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/a/a/a/al;Lcom/squareup/a/a/a/aj;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/a/a/a/ai;-><init>(Lcom/squareup/a/a/a/al;)V

    return-void
.end method

.method static synthetic b(Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/a/a/ac;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->a:Lcom/squareup/a/a/a/ac;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/a/a/av;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->b:Lcom/squareup/a/a/a/av;

    return-object v0
.end method

.method static synthetic d(Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/m;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->c:Lcom/squareup/a/m;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/a/a/f;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->d:Lcom/squareup/a/a/a/f;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/a/a/ak;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->e:Lcom/squareup/a/a/a/ak;

    return-object v0
.end method

.method static synthetic g(Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/a/a/ai;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->f:Lcom/squareup/a/a/a/ai;

    return-object v0
.end method

.method private m()Lcom/squareup/a/a/a/am;
    .locals 3

    .prologue
    .line 278
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->g:Lcom/squareup/a/a/a/am;

    .line 279
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/a/a/a/am;

    iget-object v1, p0, Lcom/squareup/a/a/a/ai;->d:Lcom/squareup/a/a/a/f;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/a/a/a/am;-><init>(Lcom/squareup/a/a/a/f;Lcom/squareup/a/a/a/aj;)V

    iput-object v0, p0, Lcom/squareup/a/a/a/ai;->g:Lcom/squareup/a/a/a/am;

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/squareup/a/a/a/ac;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->a:Lcom/squareup/a/a/a/ac;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/a/a/a/ai;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->d:Lcom/squareup/a/a/a/f;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    move-object p2, v0

    :cond_0
    return-object p2
.end method

.method public a(Lcom/squareup/a/a/a/ai;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 174
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->c()I

    move-result v1

    const/16 v2, 0x130

    if-ne v1, v2, :cond_1

    .line 188
    :cond_0
    :goto_0
    return v0

    .line 181
    :cond_1
    invoke-direct {p1}, Lcom/squareup/a/a/a/ai;->m()Lcom/squareup/a/a/a/am;

    move-result-object v1

    .line 182
    invoke-direct {p0}, Lcom/squareup/a/a/a/ai;->m()Lcom/squareup/a/a/a/am;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/a/a/a/am;->a:Ljava/util/Date;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/squareup/a/a/a/am;->a:Ljava/util/Date;

    if-eqz v2, :cond_2

    iget-object v1, v1, Lcom/squareup/a/a/a/am;->a:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/squareup/a/a/a/ai;->m()Lcom/squareup/a/a/a/am;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/a/a/a/am;->a:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 188
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/squareup/a/a/a/f;Lcom/squareup/a/a/a/ac;)Z
    .locals 3

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/squareup/a/a/a/ai;->m()Lcom/squareup/a/a/a/am;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/a/am;->a(Lcom/squareup/a/a/a/am;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 163
    invoke-virtual {p1, v0}, Lcom/squareup/a/a/a/f;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p2, v0}, Lcom/squareup/a/a/a/ac;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/squareup/a/a/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 165
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->b:Lcom/squareup/a/a/a/av;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/av;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->b:Lcom/squareup/a/a/a/av;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/av;->c()I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->b:Lcom/squareup/a/a/a/av;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/av;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->b:Lcom/squareup/a/a/a/av;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/av;->b()I

    move-result v0

    return v0
.end method

.method public f()Lcom/squareup/a/m;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->c:Lcom/squareup/a/m;

    return-object v0
.end method

.method public g()Lcom/squareup/a/a/a/f;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->d:Lcom/squareup/a/a/a/f;

    return-object v0
.end method

.method public h()Lcom/squareup/a/a/a/ak;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->e:Lcom/squareup/a/a/a/ak;

    return-object v0
.end method

.method public i()Lcom/squareup/a/a/a/al;
    .locals 2

    .prologue
    .line 129
    new-instance v0, Lcom/squareup/a/a/a/al;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/a/a/a/al;-><init>(Lcom/squareup/a/a/a/ai;Lcom/squareup/a/a/a/aj;)V

    return-object v0
.end method

.method public j()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/squareup/a/a/a/ai;->m()Lcom/squareup/a/a/a/am;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/a/am;->a(Lcom/squareup/a/a/a/am;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/squareup/a/a/a/ai;->m()Lcom/squareup/a/a/a/am;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/a/am;->a(Lcom/squareup/a/a/a/am;)Ljava/util/Set;

    move-result-object v0

    const-string/jumbo v1, "*"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public l()Lcom/squareup/a/i;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->h:Lcom/squareup/a/i;

    .line 288
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/ai;->d:Lcom/squareup/a/a/a/f;

    invoke-static {v0}, Lcom/squareup/a/i;->a(Lcom/squareup/a/a/a/f;)Lcom/squareup/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ai;->h:Lcom/squareup/a/i;

    goto :goto_0
.end method
