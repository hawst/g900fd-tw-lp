.class public final Lcom/squareup/a/a/a/a;
.super Ljava/lang/Object;


# static fields
.field private static final d:Lcom/squareup/a/a/a/ak;

.field private static final e:Lcom/squareup/a/a/a/av;


# instance fields
.field public final a:Lcom/squareup/a/a/a/ac;

.field public final b:Lcom/squareup/a/a/a/ai;

.field public final c:Lcom/squareup/a/w;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/squareup/a/a/a/b;

    invoke-direct {v0}, Lcom/squareup/a/a/a/b;-><init>()V

    sput-object v0, Lcom/squareup/a/a/a/a;->d:Lcom/squareup/a/a/a/ak;

    .line 41
    :try_start_0
    new-instance v0, Lcom/squareup/a/a/a/av;

    const-string/jumbo v1, "HTTP/1.1 504 Gateway Timeout"

    invoke-direct {v0, v1}, Lcom/squareup/a/a/a/av;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/a/a/a/a;->e:Lcom/squareup/a/a/a/av;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    return-void

    .line 42
    :catch_0
    move-exception v0

    .line 43
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private constructor <init>(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;Lcom/squareup/a/w;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/a/a/a/a;->a:Lcom/squareup/a/a/a/ac;

    .line 54
    iput-object p2, p0, Lcom/squareup/a/a/a/a;->b:Lcom/squareup/a/a/a/ai;

    .line 55
    iput-object p3, p0, Lcom/squareup/a/a/a/a;->c:Lcom/squareup/a/w;

    .line 56
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;Lcom/squareup/a/w;Lcom/squareup/a/a/a/b;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/a/a/a/a;-><init>(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;Lcom/squareup/a/w;)V

    return-void
.end method

.method static synthetic a()Lcom/squareup/a/a/a/ak;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/squareup/a/a/a/a;->d:Lcom/squareup/a/a/a/ak;

    return-object v0
.end method

.method public static a(Lcom/squareup/a/a/a/ai;Lcom/squareup/a/a/a/ac;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/squareup/a/a/a/ai;->c()I

    move-result v1

    .line 66
    const/16 v2, 0xc8

    if-eq v1, v2, :cond_1

    const/16 v2, 0xcb

    if-eq v1, v2, :cond_1

    const/16 v2, 0x12c

    if-eq v1, v2, :cond_1

    const/16 v2, 0x12d

    if-eq v1, v2, :cond_1

    const/16 v2, 0x19a

    if-eq v1, v2, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v0

    .line 76
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/a/a/a/ai;->l()Lcom/squareup/a/i;

    move-result-object v1

    .line 77
    const-string/jumbo v2, "Authorization"

    invoke-virtual {p1, v2}, Lcom/squareup/a/a/a/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/squareup/a/i;->e()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/squareup/a/i;->f()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/squareup/a/i;->d()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 84
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/a/i;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 88
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b()Lcom/squareup/a/a/a/av;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/squareup/a/a/a/a;->e:Lcom/squareup/a/a/a/av;

    return-object v0
.end method
