.class public Lcom/squareup/a/a/c/ak;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/squareup/a/a/b/c;

.field private c:Lcom/squareup/a/a/b/b;

.field private d:Lcom/squareup/a/a/c/q;

.field private e:Lcom/squareup/a/v;

.field private f:Lcom/squareup/a/a/c/w;

.field private g:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLcom/squareup/a/a/b/c;Lcom/squareup/a/a/b/b;)V
    .locals 1

    .prologue
    .line 510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 496
    sget-object v0, Lcom/squareup/a/a/c/q;->a:Lcom/squareup/a/a/c/q;

    iput-object v0, p0, Lcom/squareup/a/a/c/ak;->d:Lcom/squareup/a/a/c/q;

    .line 497
    sget-object v0, Lcom/squareup/a/v;->b:Lcom/squareup/a/v;

    iput-object v0, p0, Lcom/squareup/a/a/c/ak;->e:Lcom/squareup/a/v;

    .line 498
    sget-object v0, Lcom/squareup/a/a/c/w;->a:Lcom/squareup/a/a/c/w;

    iput-object v0, p0, Lcom/squareup/a/a/c/ak;->f:Lcom/squareup/a/a/c/w;

    .line 511
    iput-object p1, p0, Lcom/squareup/a/a/c/ak;->a:Ljava/lang/String;

    .line 512
    iput-boolean p2, p0, Lcom/squareup/a/a/c/ak;->g:Z

    .line 513
    iput-object p3, p0, Lcom/squareup/a/a/c/ak;->b:Lcom/squareup/a/a/b/c;

    .line 514
    iput-object p4, p0, Lcom/squareup/a/a/c/ak;->c:Lcom/squareup/a/a/b/b;

    .line 515
    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/c/ak;)Lcom/squareup/a/v;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/squareup/a/a/c/ak;->e:Lcom/squareup/a/v;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/a/a/c/ak;)Lcom/squareup/a/a/c/w;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/squareup/a/a/c/ak;->f:Lcom/squareup/a/a/c/w;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/a/a/c/ak;)Z
    .locals 1

    .prologue
    .line 492
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ak;->g:Z

    return v0
.end method

.method static synthetic d(Lcom/squareup/a/a/c/ak;)Lcom/squareup/a/a/c/q;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/squareup/a/a/c/ak;->d:Lcom/squareup/a/a/c/q;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/a/a/c/ak;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/squareup/a/a/c/ak;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/a/a/c/ak;)Lcom/squareup/a/a/b/c;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/squareup/a/a/c/ak;->b:Lcom/squareup/a/a/b/c;

    return-object v0
.end method

.method static synthetic g(Lcom/squareup/a/a/c/ak;)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/squareup/a/a/c/ak;->c:Lcom/squareup/a/a/b/b;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/squareup/a/a/c/ac;
    .locals 2

    .prologue
    .line 533
    new-instance v0, Lcom/squareup/a/a/c/ac;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/a/a/c/ac;-><init>(Lcom/squareup/a/a/c/ak;Lcom/squareup/a/a/c/ad;)V

    return-object v0
.end method

.method public a(Lcom/squareup/a/v;)Lcom/squareup/a/a/c/ak;
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/squareup/a/a/c/ak;->e:Lcom/squareup/a/v;

    .line 524
    return-object p0
.end method
