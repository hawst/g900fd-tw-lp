.class final Lcom/squareup/a/a/a/n;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/v;


# instance fields
.field final synthetic a:Lcom/squareup/a/a/a/k;

.field private final b:[B

.field private c:Z


# direct methods
.method private constructor <init>(Lcom/squareup/a/a/a/k;)V
    .locals 1

    .prologue
    .line 290
    iput-object p1, p0, Lcom/squareup/a/a/a/n;->a:Lcom/squareup/a/a/a/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292
    const/16 v0, 0x12

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/squareup/a/a/a/n;->b:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0xdt
        0xat
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/squareup/a/a/a/k;Lcom/squareup/a/a/a/l;)V
    .locals 0

    .prologue
    .line 290
    invoke-direct {p0, p1}, Lcom/squareup/a/a/a/n;-><init>(Lcom/squareup/a/a/a/k;)V

    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    .line 326
    const/16 v0, 0x10

    .line 328
    :cond_0
    iget-object v1, p0, Lcom/squareup/a/a/a/n;->b:[B

    add-int/lit8 v0, v0, -0x1

    invoke-static {}, Lcom/squareup/a/a/a/k;->i()[B

    move-result-object v2

    const-wide/16 v4, 0xf

    and-long/2addr v4, p1

    long-to-int v3, v4

    aget-byte v2, v2, v3

    aput-byte v2, v1, v0

    .line 329
    const/4 v1, 0x4

    ushr-long/2addr p1, v1

    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/squareup/a/a/a/n;->a:Lcom/squareup/a/a/a/k;

    invoke-static {v1}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/b;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/a/a/a/n;->b:[B

    iget-object v3, p0, Lcom/squareup/a/a/a/n;->b:[B

    array-length v3, v3

    sub-int/2addr v3, v0

    invoke-interface {v1, v2, v0, v3}, Lcom/squareup/a/a/b/b;->a([BII)Lcom/squareup/a/a/b/b;

    .line 331
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 310
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/a/n;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 312
    :goto_0
    monitor-exit p0

    return-void

    .line 311
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/a/a/a/n;->a:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/squareup/a/a/b/j;J)V
    .locals 2

    .prologue
    .line 301
    iget-boolean v0, p0, Lcom/squareup/a/a/a/n;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_1

    .line 307
    :goto_0
    return-void

    .line 304
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/squareup/a/a/a/n;->a(J)V

    .line 305
    iget-object v0, p0, Lcom/squareup/a/a/a/n;->a:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/b;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/a/a/b/b;->a(Lcom/squareup/a/a/b/j;J)V

    .line 306
    iget-object v0, p0, Lcom/squareup/a/a/a/n;->a:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/b;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->a(Ljava/lang/String;)Lcom/squareup/a/a/b/b;

    goto :goto_0
.end method

.method public declared-synchronized close()V
    .locals 2

    .prologue
    .line 315
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/a/n;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 319
    :goto_0
    monitor-exit p0

    return-void

    .line 316
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/squareup/a/a/a/n;->c:Z

    .line 317
    iget-object v0, p0, Lcom/squareup/a/a/a/n;->a:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/b;

    move-result-object v0

    invoke-static {}, Lcom/squareup/a/a/a/k;->h()[B

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->a([B)Lcom/squareup/a/a/b/b;

    .line 318
    iget-object v0, p0, Lcom/squareup/a/a/a/n;->a:Lcom/squareup/a/a/a/k;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/k;I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 315
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
