.class public abstract Lcom/squareup/a/a/a/ak;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private a:Lcom/squareup/a/a/b/w;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/io/InputStream;
.end method

.method public b()Lcom/squareup/a/a/b/w;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/squareup/a/a/a/ak;->a:Lcom/squareup/a/a/b/w;

    .line 225
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/a/a/a/ak;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/b/m;->a(Ljava/io/InputStream;)Lcom/squareup/a/a/b/w;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ak;->a:Lcom/squareup/a/a/b/w;

    goto :goto_0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/squareup/a/a/a/ak;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 274
    return-void
.end method
