.class Lcom/squareup/a/a/a/o;
.super Lcom/squareup/a/a/a/m;

# interfaces
.implements Lcom/squareup/a/a/b/w;


# instance fields
.field final synthetic d:Lcom/squareup/a/a/a/k;

.field private e:I

.field private f:Z

.field private final g:Lcom/squareup/a/a/a/u;


# direct methods
.method constructor <init>(Lcom/squareup/a/a/a/k;Ljava/net/CacheRequest;Lcom/squareup/a/a/a/u;)V
    .locals 1

    .prologue
    .line 454
    iput-object p1, p0, Lcom/squareup/a/a/a/o;->d:Lcom/squareup/a/a/a/k;

    .line 455
    invoke-direct {p0, p1, p2}, Lcom/squareup/a/a/a/m;-><init>(Lcom/squareup/a/a/a/k;Ljava/net/CacheRequest;)V

    .line 450
    const/4 v0, -0x1

    iput v0, p0, Lcom/squareup/a/a/a/o;->e:I

    .line 451
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/a/a/a/o;->f:Z

    .line 456
    iput-object p3, p0, Lcom/squareup/a/a/a/o;->g:Lcom/squareup/a/a/a/u;

    .line 457
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 482
    iget v0, p0, Lcom/squareup/a/a/a/o;->e:I

    if-eq v0, v2, :cond_0

    .line 483
    iget-object v0, p0, Lcom/squareup/a/a/a/o;->d:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->f(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/c;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    .line 485
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/o;->d:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->f(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/c;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 486
    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 487
    if-eq v1, v2, :cond_1

    .line 488
    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 491
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/squareup/a/a/a/o;->e:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 495
    iget v0, p0, Lcom/squareup/a/a/a/o;->e:I

    if-nez v0, :cond_2

    .line 496
    iput-boolean v4, p0, Lcom/squareup/a/a/a/o;->f:Z

    .line 497
    new-instance v0, Lcom/squareup/a/a/a/h;

    invoke-direct {v0}, Lcom/squareup/a/a/a/h;-><init>()V

    .line 498
    iget-object v1, p0, Lcom/squareup/a/a/a/o;->d:Lcom/squareup/a/a/a/k;

    invoke-virtual {v1, v0}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/h;)V

    .line 499
    iget-object v1, p0, Lcom/squareup/a/a/a/o;->g:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/h;->a()Lcom/squareup/a/a/a/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/a/a/a/u;->a(Lcom/squareup/a/a/a/f;)V

    .line 500
    invoke-virtual {p0, v3}, Lcom/squareup/a/a/a/o;->a(Z)V

    .line 502
    :cond_2
    return-void

    .line 492
    :catch_0
    move-exception v1

    .line 493
    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Expected a hex chunk size but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public b(Lcom/squareup/a/a/b/j;J)J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 461
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 462
    :cond_0
    iget-boolean v2, p0, Lcom/squareup/a/a/a/o;->b:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 463
    :cond_1
    iget-boolean v2, p0, Lcom/squareup/a/a/a/o;->f:Z

    if-nez v2, :cond_3

    .line 477
    :cond_2
    :goto_0
    return-wide v0

    .line 465
    :cond_3
    iget v2, p0, Lcom/squareup/a/a/a/o;->e:I

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/squareup/a/a/a/o;->e:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_5

    .line 466
    :cond_4
    invoke-direct {p0}, Lcom/squareup/a/a/a/o;->b()V

    .line 467
    iget-boolean v2, p0, Lcom/squareup/a/a/a/o;->f:Z

    if-eqz v2, :cond_2

    .line 470
    :cond_5
    iget-object v2, p0, Lcom/squareup/a/a/a/o;->d:Lcom/squareup/a/a/a/k;

    invoke-static {v2}, Lcom/squareup/a/a/a/k;->f(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/c;

    move-result-object v2

    iget v3, p0, Lcom/squareup/a/a/a/o;->e:I

    int-to-long v4, v3

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface {v2, p1, v4, v5}, Lcom/squareup/a/a/b/c;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v2

    .line 471
    cmp-long v0, v2, v0

    if-nez v0, :cond_6

    .line 472
    invoke-virtual {p0}, Lcom/squareup/a/a/a/o;->a()V

    .line 473
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 475
    :cond_6
    iget v0, p0, Lcom/squareup/a/a/a/o;->e:I

    int-to-long v0, v0

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/squareup/a/a/a/o;->e:I

    .line 476
    invoke-virtual {p0, p1, v2, v3}, Lcom/squareup/a/a/a/o;->a(Lcom/squareup/a/a/b/j;J)V

    move-wide v0, v2

    .line 477
    goto :goto_0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 510
    iget-boolean v0, p0, Lcom/squareup/a/a/a/o;->b:Z

    if-eqz v0, :cond_0

    .line 515
    :goto_0
    return-void

    .line 511
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/a/a/a/o;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/a/a/a/o;->d:Lcom/squareup/a/a/a/k;

    const/16 v1, 0x64

    invoke-virtual {v0, p0, v1}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/b/w;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 512
    invoke-virtual {p0}, Lcom/squareup/a/a/a/o;->a()V

    .line 514
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/a/a/a/o;->b:Z

    goto :goto_0
.end method
