.class public final Lcom/squareup/a/a/c/j;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/c/as;


# static fields
.field private static final a:Lcom/squareup/a/a/b/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-string/jumbo v0, "PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"

    invoke-static {v0}, Lcom/squareup/a/a/b/d;->a(Ljava/lang/String;)Lcom/squareup/a/a/b/d;

    move-result-object v0

    sput-object v0, Lcom/squareup/a/a/c/j;->a:Lcom/squareup/a/a/b/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 498
    return-void
.end method

.method static synthetic a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;
    .locals 1

    .prologue
    .line 32
    invoke-static {p0, p1}, Lcom/squareup/a/a/c/j;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Lcom/squareup/a/a/b/d;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/squareup/a/a/c/j;->a:Lcom/squareup/a/a/b/d;

    return-object v0
.end method

.method static synthetic b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;
    .locals 1

    .prologue
    .line 32
    invoke-static {p0, p1}, Lcom/squareup/a/a/c/j;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    return-object v0
.end method

.method private static varargs c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;
    .locals 2

    .prologue
    .line 486
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static varargs d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;
    .locals 2

    .prologue
    .line 490
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 68
    const/16 v0, 0x3fff

    return v0
.end method

.method public a(Lcom/squareup/a/a/b/c;Z)Lcom/squareup/a/a/c/b;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lcom/squareup/a/a/c/l;

    const/16 v1, 0x1000

    invoke-direct {v0, p1, v1, p2}, Lcom/squareup/a/a/c/l;-><init>(Lcom/squareup/a/a/b/c;IZ)V

    return-object v0
.end method

.method public a(Lcom/squareup/a/a/b/b;Z)Lcom/squareup/a/a/c/d;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/squareup/a/a/c/m;

    invoke-direct {v0, p1, p2}, Lcom/squareup/a/a/c/m;-><init>(Lcom/squareup/a/a/b/b;Z)V

    return-object v0
.end method
