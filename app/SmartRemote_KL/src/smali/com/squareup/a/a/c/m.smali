.class final Lcom/squareup/a/a/c/m;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/c/d;


# instance fields
.field private final a:Lcom/squareup/a/a/b/b;

.field private final b:Z

.field private final c:Lcom/squareup/a/a/b/j;

.field private final d:Lcom/squareup/a/a/c/i;

.field private e:Z


# direct methods
.method constructor <init>(Lcom/squareup/a/a/b/b;Z)V
    .locals 2

    .prologue
    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299
    iput-object p1, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    .line 300
    iput-boolean p2, p0, Lcom/squareup/a/a/c/m;->b:Z

    .line 301
    new-instance v0, Lcom/squareup/a/a/b/j;

    invoke-direct {v0}, Lcom/squareup/a/a/b/j;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/c/m;->c:Lcom/squareup/a/a/b/j;

    .line 302
    new-instance v0, Lcom/squareup/a/a/c/i;

    iget-object v1, p0, Lcom/squareup/a/a/c/m;->c:Lcom/squareup/a/a/b/j;

    invoke-direct {v0, v1}, Lcom/squareup/a/a/c/i;-><init>(Lcom/squareup/a/a/b/j;)V

    iput-object v0, p0, Lcom/squareup/a/a/c/m;->d:Lcom/squareup/a/a/c/i;

    .line 303
    return-void
.end method

.method private a(ZIILjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZII",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 363
    iget-boolean v0, p0, Lcom/squareup/a/a/c/m;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->c:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 365
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->d:Lcom/squareup/a/a/c/i;

    invoke-virtual {v0, p4}, Lcom/squareup/a/a/c/i;->a(Ljava/util/List;)V

    .line 367
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->c:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v0

    long-to-int v0, v0

    .line 368
    const/4 v2, 0x1

    .line 369
    const/4 v1, 0x4

    .line 370
    if-eqz p1, :cond_2

    const/4 v1, 0x5

    int-to-byte v1, v1

    .line 371
    :cond_2
    if-eq p3, v4, :cond_3

    or-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    .line 372
    :cond_3
    if-eq p3, v4, :cond_4

    add-int/lit8 v0, v0, 0x4

    .line 373
    :cond_4
    invoke-virtual {p0, v0, v2, v1, p2}, Lcom/squareup/a/a/c/m;->a(IBBI)V

    .line 374
    if-eq p3, v4, :cond_5

    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    const v1, 0x7fffffff

    and-int/2addr v1, p3

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 375
    :cond_5
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    iget-object v1, p0, Lcom/squareup/a/a/c/m;->c:Lcom/squareup/a/a/b/j;

    iget-object v2, p0, Lcom/squareup/a/a/c/m;->c:Lcom/squareup/a/a/b/j;

    invoke-virtual {v2}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/a/a/b/b;->a(Lcom/squareup/a/a/b/j;J)V

    .line 376
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    .prologue
    .line 321
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/m;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 322
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/squareup/a/a/c/m;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 325
    :goto_0
    monitor-exit p0

    return-void

    .line 323
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-static {}, Lcom/squareup/a/a/c/j;->b()Lcom/squareup/a/a/b/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/a/a/b/d;->f()[B

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->a([B)Lcom/squareup/a/a/b/b;

    .line 324
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method a(IBBI)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 478
    const/16 v0, 0x3fff

    if-le p1, v0, :cond_0

    const-string/jumbo v0, "FRAME_SIZE_ERROR length > 16383: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 479
    :cond_0
    const/high16 v0, -0x80000000

    and-int/2addr v0, p4

    if-eqz v0, :cond_1

    const-string/jumbo v0, "reserved bit set: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 480
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    and-int/lit16 v1, p1, 0x3fff

    shl-int/lit8 v1, v1, 0x10

    and-int/lit16 v2, p2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    and-int/lit16 v2, p3, 0xff

    or-int/2addr v1, v2

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 481
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    const v1, 0x7fffffff

    and-int/2addr v1, p4

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 482
    return-void
.end method

.method a(IBLcom/squareup/a/a/b/j;I)V
    .locals 4

    .prologue
    .line 405
    const/4 v0, 0x0

    .line 406
    invoke-virtual {p0, p4, v0, p2, p1}, Lcom/squareup/a/a/c/m;->a(IBBI)V

    .line 407
    if-lez p4, :cond_0

    .line 408
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    int-to-long v2, p4

    invoke-interface {v0, p3, v2, v3}, Lcom/squareup/a/a/b/b;->a(Lcom/squareup/a/a/b/j;J)V

    .line 410
    :cond_0
    return-void
.end method

.method public declared-synchronized a(IILjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 349
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/m;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 350
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->c:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->d:Lcom/squareup/a/a/c/i;

    invoke-virtual {v0, p3}, Lcom/squareup/a/a/c/i;->a(Ljava/util/List;)V

    .line 353
    const-wide/16 v0, 0x4

    iget-object v2, p0, Lcom/squareup/a/a/c/m;->c:Lcom/squareup/a/a/b/j;

    invoke-virtual {v2}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v2

    add-long/2addr v0, v2

    long-to-int v0, v0

    .line 354
    const/4 v1, 0x5

    .line 355
    const/4 v2, 0x4

    .line 356
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/squareup/a/a/c/m;->a(IBBI)V

    .line 357
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    const v1, 0x7fffffff

    and-int/2addr v1, p2

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 358
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    iget-object v1, p0, Lcom/squareup/a/a/c/m;->c:Lcom/squareup/a/a/b/j;

    iget-object v2, p0, Lcom/squareup/a/a/c/m;->c:Lcom/squareup/a/a/b/j;

    invoke-virtual {v2}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/a/a/b/b;->a(Lcom/squareup/a/a/b/j;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 359
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(IJ)V
    .locals 4

    .prologue
    .line 459
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/m;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 460
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_1

    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p2, v0

    if-lez v0, :cond_2

    .line 461
    :cond_1
    :try_start_1
    const-string/jumbo v0, "windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 464
    :cond_2
    const/4 v0, 0x4

    .line 465
    const/16 v1, 0x9

    .line 466
    const/4 v2, 0x0

    .line 467
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/squareup/a/a/c/m;->a(IBBI)V

    .line 468
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    long-to-int v1, p2

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 469
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 470
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ILcom/squareup/a/a/c/a;)V
    .locals 3

    .prologue
    .line 380
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/m;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 381
    :cond_0
    :try_start_1
    iget v0, p2, Lcom/squareup/a/a/c/a;->p:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 383
    :cond_1
    const/4 v0, 0x4

    .line 384
    const/4 v1, 0x3

    .line 385
    const/4 v2, 0x0

    .line 386
    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/squareup/a/a/c/m;->a(IBBI)V

    .line 387
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    iget v1, p2, Lcom/squareup/a/a/c/a;->o:I

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 388
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ILcom/squareup/a/a/c/a;[B)V
    .locals 4

    .prologue
    .line 442
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/m;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 443
    :cond_0
    :try_start_1
    iget v0, p2, Lcom/squareup/a/a/c/a;->o:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string/jumbo v0, "errorCode.httpCode == -1"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 444
    :cond_1
    array-length v0, p3

    add-int/lit8 v0, v0, 0x8

    .line 445
    const/4 v1, 0x7

    .line 446
    const/4 v2, 0x0

    .line 447
    const/4 v3, 0x0

    .line 448
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/squareup/a/a/c/m;->a(IBBI)V

    .line 449
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0, p1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 450
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    iget v1, p2, Lcom/squareup/a/a/c/a;->o:I

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 451
    array-length v0, p3

    if-lez v0, :cond_2

    .line 452
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0, p3}, Lcom/squareup/a/a/b/b;->a([B)Lcom/squareup/a/a/b/b;

    .line 454
    :cond_2
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 455
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lcom/squareup/a/a/c/y;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 413
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/squareup/a/a/c/m;->e:Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 414
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/squareup/a/a/c/y;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x8

    .line 415
    const/4 v2, 0x4

    .line 416
    const/4 v3, 0x0

    .line 417
    const/4 v4, 0x0

    .line 418
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/squareup/a/a/c/m;->a(IBBI)V

    .line 419
    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    .line 420
    invoke-virtual {p1, v0}, Lcom/squareup/a/a/c/y;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 419
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 421
    :cond_1
    iget-object v1, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    const v2, 0xffffff

    and-int/2addr v2, v0

    invoke-interface {v1, v2}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 422
    iget-object v1, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-virtual {p1, v0}, Lcom/squareup/a/a/c/y;->b(I)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    goto :goto_1

    .line 424
    :cond_2
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 425
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ZII)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 429
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/squareup/a/a/c/m;->e:Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 430
    :cond_0
    const/16 v1, 0x8

    .line 431
    const/4 v2, 0x6

    .line 432
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 433
    :cond_1
    const/4 v3, 0x0

    .line 434
    :try_start_1
    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/squareup/a/a/c/m;->a(IBBI)V

    .line 435
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0, p2}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 436
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0, p3}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 437
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 438
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ZILcom/squareup/a/a/b/j;I)V
    .locals 2

    .prologue
    .line 398
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/m;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 399
    :cond_0
    const/4 v0, 0x0

    .line 400
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    int-to-byte v0, v0

    .line 401
    :cond_1
    :try_start_1
    invoke-virtual {p0, p2, v0, p3, p4}, Lcom/squareup/a/a/c/m;->a(IBLcom/squareup/a/a/b/j;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 402
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ZZIIIILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZIIII",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 330
    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 331
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/squareup/a/a/c/m;->e:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 332
    :cond_1
    invoke-direct {p0, p1, p3, p5, p7}, Lcom/squareup/a/a/c/m;->a(ZIILjava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized b()V
    .locals 4

    .prologue
    .line 311
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/m;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 312
    :cond_0
    const/4 v0, 0x0

    .line 313
    const/4 v1, 0x4

    .line 314
    const/4 v2, 0x1

    .line 315
    const/4 v3, 0x0

    .line 316
    :try_start_1
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/squareup/a/a/c/m;->a(IBBI)V

    .line 317
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 318
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized c()V
    .locals 2

    .prologue
    .line 306
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/m;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 307
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 308
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 473
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/squareup/a/a/c/m;->e:Z

    .line 474
    iget-object v0, p0, Lcom/squareup/a/a/c/m;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 475
    monitor-exit p0

    return-void

    .line 473
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
