.class Lcom/squareup/a/a/c/al;
.super Lcom/squareup/a/a/n;

# interfaces
.implements Lcom/squareup/a/a/c/c;


# instance fields
.field final synthetic a:Lcom/squareup/a/a/c/ac;


# direct methods
.method private constructor <init>(Lcom/squareup/a/a/c/ac;)V
    .locals 4

    .prologue
    .line 542
    iput-object p1, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    .line 543
    const-string/jumbo v0, "OkHttp %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/squareup/a/a/n;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 544
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/a/a/c/ac;Lcom/squareup/a/a/c/ad;)V
    .locals 0

    .prologue
    .line 541
    invoke-direct {p0, p1}, Lcom/squareup/a/a/c/al;-><init>(Lcom/squareup/a/a/c/ac;)V

    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    .line 685
    invoke-static {}, Lcom/squareup/a/a/c/ac;->f()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/squareup/a/a/c/an;

    const-string/jumbo v2, "OkHttp %s ACK Settings"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v5}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, v3}, Lcom/squareup/a/a/c/an;-><init>(Lcom/squareup/a/a/c/al;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 693
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 5

    .prologue
    .line 547
    sget-object v0, Lcom/squareup/a/a/c/a;->g:Lcom/squareup/a/a/c/a;

    .line 548
    sget-object v2, Lcom/squareup/a/a/c/a;->g:Lcom/squareup/a/a/c/a;

    .line 550
    :try_start_0
    iget-object v1, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    iget-boolean v1, v1, Lcom/squareup/a/a/c/ac;->b:Z

    if-nez v1, :cond_0

    .line 551
    iget-object v1, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    iget-object v1, v1, Lcom/squareup/a/a/c/ac;->g:Lcom/squareup/a/a/c/b;

    invoke-interface {v1}, Lcom/squareup/a/a/c/b;->a()V

    .line 553
    :cond_0
    iget-object v1, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    iget-object v1, v1, Lcom/squareup/a/a/c/ac;->g:Lcom/squareup/a/a/c/b;

    invoke-interface {v1, p0}, Lcom/squareup/a/a/c/b;->a(Lcom/squareup/a/a/c/c;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 555
    sget-object v0, Lcom/squareup/a/a/c/a;->a:Lcom/squareup/a/a/c/a;

    .line 556
    sget-object v1, Lcom/squareup/a/a/c/a;->l:Lcom/squareup/a/a/c/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 562
    :try_start_1
    iget-object v2, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v2, v0, v1}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;Lcom/squareup/a/a/c/a;Lcom/squareup/a/a/c/a;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 566
    :goto_0
    return-void

    .line 557
    :catch_0
    move-exception v1

    .line 558
    :try_start_2
    sget-object v1, Lcom/squareup/a/a/c/a;->b:Lcom/squareup/a/a/c/a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 559
    :try_start_3
    sget-object v0, Lcom/squareup/a/a/c/a;->b:Lcom/squareup/a/a/c/a;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 562
    :try_start_4
    iget-object v2, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v2, v1, v0}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;Lcom/squareup/a/a/c/a;Lcom/squareup/a/a/c/a;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 563
    :catch_1
    move-exception v0

    goto :goto_0

    .line 561
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 562
    :goto_1
    :try_start_5
    iget-object v3, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v3, v1, v2}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;Lcom/squareup/a/a/c/a;Lcom/squareup/a/a/c/a;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 564
    :goto_2
    throw v0

    .line 563
    :catch_2
    move-exception v1

    goto :goto_2

    .line 561
    :catchall_1
    move-exception v0

    goto :goto_1

    .line 563
    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 748
    return-void
.end method

.method public a(IILjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 752
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0, p2, p3}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;ILjava/util/List;)V

    .line 753
    return-void
.end method

.method public a(IJ)V
    .locals 4

    .prologue
    .line 731
    if-nez p1, :cond_1

    .line 732
    iget-object v1, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    monitor-enter v1

    .line 733
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    iget-wide v2, v0, Lcom/squareup/a/a/c/ac;->d:J

    add-long/2addr v2, p2

    iput-wide v2, v0, Lcom/squareup/a/a/c/ac;->d:J

    .line 734
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 735
    monitor-exit v1

    .line 744
    :cond_0
    :goto_0
    return-void

    .line 735
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 737
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/c/ac;->a(I)Lcom/squareup/a/a/c/ao;

    move-result-object v1

    .line 738
    if-eqz v1, :cond_0

    .line 739
    monitor-enter v1

    .line 740
    :try_start_1
    invoke-virtual {v1, p2, p3}, Lcom/squareup/a/a/c/ao;->b(J)V

    .line 741
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method public a(ILcom/squareup/a/a/c/a;)V
    .locals 1

    .prologue
    .line 643
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0, p1}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 644
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0, p1, p2}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;ILcom/squareup/a/a/c/a;)V

    .line 651
    :cond_0
    :goto_0
    return-void

    .line 647
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/c/ac;->b(I)Lcom/squareup/a/a/c/ao;

    move-result-object v0

    .line 648
    if-eqz v0, :cond_0

    .line 649
    invoke-virtual {v0, p2}, Lcom/squareup/a/a/c/ao;->c(Lcom/squareup/a/a/c/a;)V

    goto :goto_0
.end method

.method public a(ILcom/squareup/a/a/c/a;Lcom/squareup/a/a/b/d;)V
    .locals 4

    .prologue
    .line 712
    invoke-virtual {p3}, Lcom/squareup/a/a/b/d;->e()I

    move-result v0

    if-lez v0, :cond_0

    .line 714
    :cond_0
    iget-object v2, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    monitor-enter v2

    .line 715
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/ac;->b(Lcom/squareup/a/a/c/ac;Z)Z

    .line 718
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0}, Lcom/squareup/a/a/c/ac;->e(Lcom/squareup/a/a/c/ac;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 719
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 720
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 721
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 722
    if-le v1, p1, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/a/a/c/ao;

    invoke-virtual {v1}, Lcom/squareup/a/a/c/ao;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 723
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/ao;

    sget-object v1, Lcom/squareup/a/a/c/a;->k:Lcom/squareup/a/a/c/a;

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/c/ao;->c(Lcom/squareup/a/a/c/a;)V

    .line 724
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 727
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 728
    return-void
.end method

.method public a(ZII)V
    .locals 3

    .prologue
    .line 700
    if-eqz p1, :cond_1

    .line 701
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0, p2}, Lcom/squareup/a/a/c/ac;->c(Lcom/squareup/a/a/c/ac;I)Lcom/squareup/a/a/c/v;

    move-result-object v0

    .line 702
    if-eqz v0, :cond_0

    .line 703
    invoke-virtual {v0}, Lcom/squareup/a/a/c/v;->b()V

    .line 709
    :cond_0
    :goto_0
    return-void

    .line 707
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, p2, p3, v2}, Lcom/squareup/a/a/c/ac;->b(Lcom/squareup/a/a/c/ac;ZIILcom/squareup/a/a/c/v;)V

    goto :goto_0
.end method

.method public a(ZILcom/squareup/a/a/b/c;I)V
    .locals 2

    .prologue
    .line 570
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0, p2}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 571
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0, p2, p3, p4, p1}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;ILcom/squareup/a/a/b/c;IZ)V

    .line 584
    :cond_0
    :goto_0
    return-void

    .line 574
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v0, p2}, Lcom/squareup/a/a/c/ac;->a(I)Lcom/squareup/a/a/c/ao;

    move-result-object v0

    .line 575
    if-nez v0, :cond_2

    .line 576
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    sget-object v1, Lcom/squareup/a/a/c/a;->c:Lcom/squareup/a/a/c/a;

    invoke-virtual {v0, p2, v1}, Lcom/squareup/a/a/c/ac;->a(ILcom/squareup/a/a/c/a;)V

    .line 577
    int-to-long v0, p4

    invoke-interface {p3, v0, v1}, Lcom/squareup/a/a/b/c;->b(J)V

    goto :goto_0

    .line 580
    :cond_2
    invoke-virtual {v0, p3, p4}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/b/c;I)V

    .line 581
    if-eqz p1, :cond_0

    .line 582
    invoke-virtual {v0}, Lcom/squareup/a/a/c/ao;->g()V

    goto :goto_0
.end method

.method public a(ZLcom/squareup/a/a/c/y;)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 654
    .line 655
    const/4 v0, 0x0

    .line 656
    iget-object v1, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    monitor-enter v1

    .line 657
    :try_start_0
    iget-object v2, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    iget-object v2, v2, Lcom/squareup/a/a/c/ac;->f:Lcom/squareup/a/a/c/y;

    const/high16 v3, 0x10000

    invoke-virtual {v2, v3}, Lcom/squareup/a/a/c/y;->d(I)I

    move-result v2

    .line 658
    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    iget-object v3, v3, Lcom/squareup/a/a/c/ac;->f:Lcom/squareup/a/a/c/y;

    invoke-virtual {v3}, Lcom/squareup/a/a/c/y;->a()V

    .line 659
    :cond_0
    iget-object v3, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    iget-object v3, v3, Lcom/squareup/a/a/c/ac;->f:Lcom/squareup/a/a/c/y;

    invoke-virtual {v3, p2}, Lcom/squareup/a/a/c/y;->a(Lcom/squareup/a/a/c/y;)V

    .line 660
    iget-object v3, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v3}, Lcom/squareup/a/a/c/ac;->a()Lcom/squareup/a/v;

    move-result-object v3

    sget-object v6, Lcom/squareup/a/v;->a:Lcom/squareup/a/v;

    if-ne v3, v6, :cond_1

    .line 661
    invoke-direct {p0}, Lcom/squareup/a/a/c/al;->c()V

    .line 663
    :cond_1
    iget-object v3, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    iget-object v3, v3, Lcom/squareup/a/a/c/ac;->f:Lcom/squareup/a/a/c/y;

    const/high16 v6, 0x10000

    invoke-virtual {v3, v6}, Lcom/squareup/a/a/c/y;->d(I)I

    move-result v3

    .line 664
    const/4 v6, -0x1

    if-eq v3, v6, :cond_5

    if-eq v3, v2, :cond_5

    .line 665
    sub-int v2, v3, v2

    int-to-long v2, v2

    .line 666
    iget-object v6, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v6}, Lcom/squareup/a/a/c/ac;->g(Lcom/squareup/a/a/c/ac;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 667
    iget-object v6, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v6, v2, v3}, Lcom/squareup/a/a/c/ac;->a(J)V

    .line 668
    iget-object v6, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;Z)Z

    .line 670
    :cond_2
    iget-object v6, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v6}, Lcom/squareup/a/a/c/ac;->e(Lcom/squareup/a/a/c/ac;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 671
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0}, Lcom/squareup/a/a/c/ac;->e(Lcom/squareup/a/a/c/ac;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v6, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v6}, Lcom/squareup/a/a/c/ac;->e(Lcom/squareup/a/a/c/ac;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    new-array v6, v6, [Lcom/squareup/a/a/c/ao;

    invoke-interface {v0, v6}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/a/a/c/ao;

    .line 674
    :cond_3
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 675
    if-eqz v0, :cond_4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    .line 676
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0}, Lcom/squareup/a/a/c/ac;->e(Lcom/squareup/a/a/c/ac;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/ao;

    .line 677
    monitor-enter v0

    .line 678
    :try_start_1
    invoke-virtual {v0, v2, v3}, Lcom/squareup/a/a/c/ao;->b(J)V

    .line 679
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 674
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 682
    :cond_4
    return-void

    :cond_5
    move-wide v2, v4

    goto :goto_0
.end method

.method public a(ZZIIILjava/util/List;Lcom/squareup/a/a/c/f;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZIII",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;",
            "Lcom/squareup/a/a/c/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 588
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0, p3}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 589
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0, p3, p6, p2}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;ILjava/util/List;Z)V

    .line 640
    :cond_0
    :goto_0
    return-void

    .line 593
    :cond_1
    iget-object v7, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    monitor-enter v7

    .line 595
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0}, Lcom/squareup/a/a/c/ac;->b(Lcom/squareup/a/a/c/ac;)Z

    move-result v0

    if-eqz v0, :cond_2

    monitor-exit v7

    goto :goto_0

    .line 628
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 597
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v0, p3}, Lcom/squareup/a/a/c/ac;->a(I)Lcom/squareup/a/a/c/ao;

    move-result-object v0

    .line 599
    if-nez v0, :cond_6

    .line 601
    invoke-virtual {p7}, Lcom/squareup/a/a/c/f;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 602
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    sget-object v1, Lcom/squareup/a/a/c/a;->c:Lcom/squareup/a/a/c/a;

    invoke-virtual {v0, p3, v1}, Lcom/squareup/a/a/c/ac;->a(ILcom/squareup/a/a/c/a;)V

    .line 603
    monitor-exit v7

    goto :goto_0

    .line 607
    :cond_3
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v0}, Lcom/squareup/a/a/c/ac;->c(Lcom/squareup/a/a/c/ac;)I

    move-result v0

    if-gt p3, v0, :cond_4

    monitor-exit v7

    goto :goto_0

    .line 610
    :cond_4
    rem-int/lit8 v0, p3, 0x2

    iget-object v1, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v1}, Lcom/squareup/a/a/c/ac;->d(Lcom/squareup/a/a/c/ac;)I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-ne v0, v1, :cond_5

    monitor-exit v7

    goto :goto_0

    .line 613
    :cond_5
    new-instance v0, Lcom/squareup/a/a/c/ao;

    iget-object v2, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    move v1, p3

    move v3, p1

    move v4, p2

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/squareup/a/a/c/ao;-><init>(ILcom/squareup/a/a/c/ac;ZZILjava/util/List;)V

    .line 615
    iget-object v1, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v1, p3}, Lcom/squareup/a/a/c/ac;->b(Lcom/squareup/a/a/c/ac;I)I

    .line 616
    iget-object v1, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v1}, Lcom/squareup/a/a/c/ac;->e(Lcom/squareup/a/a/c/ac;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 617
    invoke-static {}, Lcom/squareup/a/a/c/ac;->f()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    new-instance v2, Lcom/squareup/a/a/c/am;

    const-string/jumbo v3, "OkHttp %s stream %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-static {v6}, Lcom/squareup/a/a/c/ac;->a(Lcom/squareup/a/a/c/ac;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v2, p0, v3, v4, v0}, Lcom/squareup/a/a/c/am;-><init>(Lcom/squareup/a/a/c/al;Ljava/lang/String;[Ljava/lang/Object;Lcom/squareup/a/a/c/ao;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 626
    monitor-exit v7

    goto/16 :goto_0

    .line 628
    :cond_6
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 631
    invoke-virtual {p7}, Lcom/squareup/a/a/c/f;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 632
    sget-object v1, Lcom/squareup/a/a/c/a;->b:Lcom/squareup/a/a/c/a;

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/c/ao;->b(Lcom/squareup/a/a/c/a;)V

    .line 633
    iget-object v0, p0, Lcom/squareup/a/a/c/al;->a:Lcom/squareup/a/a/c/ac;

    invoke-virtual {v0, p3}, Lcom/squareup/a/a/c/ac;->b(I)Lcom/squareup/a/a/c/ao;

    goto/16 :goto_0

    .line 638
    :cond_7
    invoke-virtual {v0, p6, p7}, Lcom/squareup/a/a/c/ao;->a(Ljava/util/List;Lcom/squareup/a/a/c/f;)V

    .line 639
    if-eqz p2, :cond_0

    invoke-virtual {v0}, Lcom/squareup/a/a/c/ao;->g()V

    goto/16 :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 697
    return-void
.end method
