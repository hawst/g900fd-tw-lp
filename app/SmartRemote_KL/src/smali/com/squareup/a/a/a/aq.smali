.class final Lcom/squareup/a/a/a/aq;
.super Lcom/squareup/a/a/a/d;


# instance fields
.field private final a:Lcom/squareup/a/a/a/ap;


# direct methods
.method public constructor <init>(Lcom/squareup/a/a/a/ap;)V
    .locals 0

    .prologue
    .line 502
    invoke-direct {p0, p1}, Lcom/squareup/a/a/a/d;-><init>(Ljava/net/HttpURLConnection;)V

    .line 503
    iput-object p1, p0, Lcom/squareup/a/a/a/aq;->a:Lcom/squareup/a/a/a/ap;

    .line 504
    return-void
.end method


# virtual methods
.method protected a()Lcom/squareup/a/m;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/squareup/a/a/a/aq;->a:Lcom/squareup/a/a/a/ap;

    invoke-static {v0}, Lcom/squareup/a/a/a/ap;->a(Lcom/squareup/a/a/a/ap;)Lcom/squareup/a/a/a/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->f()Lcom/squareup/a/m;

    move-result-object v0

    return-object v0
.end method

.method public getHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    .prologue
    .line 515
    invoke-static {}, Lcom/squareup/a/a/a/an;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    .prologue
    .line 523
    invoke-static {}, Lcom/squareup/a/a/a/an;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setFixedLengthStreamingMode(J)V
    .locals 1

    .prologue
    .line 531
    iget-object v0, p0, Lcom/squareup/a/a/a/aq;->a:Lcom/squareup/a/a/a/ap;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/a/a/a/ap;->setFixedLengthStreamingMode(J)V

    .line 532
    return-void
.end method

.method public setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V
    .locals 1

    .prologue
    .line 511
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V
    .locals 1

    .prologue
    .line 519
    invoke-static {}, Lcom/squareup/a/a/a/an;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
