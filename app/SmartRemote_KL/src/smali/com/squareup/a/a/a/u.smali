.class public Lcom/squareup/a/a/a/u;
.super Ljava/lang/Object;


# instance fields
.field final a:Lcom/squareup/a/r;

.field b:J

.field public final c:Z

.field private d:Lcom/squareup/a/j;

.field private e:Lcom/squareup/a/a/a/as;

.field private f:Lcom/squareup/a/x;

.field private g:Lcom/squareup/a/a/a/aw;

.field private h:Z

.field private i:Lcom/squareup/a/a/a/ac;

.field private j:Lcom/squareup/a/a/a/ac;

.field private k:Lcom/squareup/a/a/b/v;

.field private l:Lcom/squareup/a/a/b/b;

.field private m:Lcom/squareup/a/w;

.field private n:Lcom/squareup/a/a/a/ai;

.field private o:Lcom/squareup/a/a/b/w;

.field private p:Lcom/squareup/a/a/b/w;

.field private q:Ljava/io/InputStream;

.field private r:Lcom/squareup/a/a/a/ai;

.field private s:Ljava/net/CacheRequest;


# direct methods
.method public constructor <init>(Lcom/squareup/a/r;Lcom/squareup/a/a/a/ac;ZLcom/squareup/a/j;Lcom/squareup/a/a/a/as;Lcom/squareup/a/a/a/ar;)V
    .locals 2

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/squareup/a/a/a/u;->b:J

    .line 135
    iput-object p1, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    .line 136
    iput-object p2, p0, Lcom/squareup/a/a/a/u;->i:Lcom/squareup/a/a/a/ac;

    .line 137
    iput-object p2, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    .line 138
    iput-boolean p3, p0, Lcom/squareup/a/a/a/u;->c:Z

    .line 139
    iput-object p4, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    .line 140
    iput-object p5, p0, Lcom/squareup/a/a/a/u;->e:Lcom/squareup/a/a/a/as;

    .line 141
    iput-object p6, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    .line 143
    if-eqz p4, :cond_0

    .line 144
    invoke-virtual {p4, p0}, Lcom/squareup/a/j;->a(Ljava/lang/Object;)V

    .line 145
    invoke-virtual {p4}, Lcom/squareup/a/j;->d()Lcom/squareup/a/x;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->f:Lcom/squareup/a/x;

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->f:Lcom/squareup/a/x;

    goto :goto_0
.end method

.method private static a(Lcom/squareup/a/a/a/ai;Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/a/a/ai;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 642
    new-instance v2, Lcom/squareup/a/a/a/h;

    invoke-direct {v2}, Lcom/squareup/a/a/a/h;-><init>()V

    .line 644
    invoke-virtual {p0}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v3

    move v0, v1

    .line 645
    :goto_0
    invoke-virtual {v3}, Lcom/squareup/a/a/a/f;->a()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 646
    invoke-virtual {v3, v0}, Lcom/squareup/a/a/a/f;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 647
    invoke-virtual {v3, v0}, Lcom/squareup/a/a/a/f;->b(I)Ljava/lang/String;

    move-result-object v5

    .line 648
    const-string/jumbo v6, "Warning"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string/jumbo v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 645
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 651
    :cond_1
    invoke-static {v4}, Lcom/squareup/a/a/a/u;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p1, v4}, Lcom/squareup/a/a/a/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_0

    .line 652
    :cond_2
    invoke-virtual {v2, v4, v5}, Lcom/squareup/a/a/a/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    goto :goto_1

    .line 656
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v0

    .line 657
    :goto_2
    invoke-virtual {v0}, Lcom/squareup/a/a/a/f;->a()I

    move-result v3

    if-ge v1, v3, :cond_5

    .line 658
    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/f;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 659
    invoke-static {v3}, Lcom/squareup/a/a/a/u;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 660
    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/f;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/squareup/a/a/a/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    .line 657
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 664
    :cond_5
    invoke-virtual {p0}, Lcom/squareup/a/a/a/ai;->i()Lcom/squareup/a/a/a/al;

    move-result-object v0

    invoke-virtual {v2}, Lcom/squareup/a/a/a/h;->a()Lcom/squareup/a/a/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/f;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/al;->a()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/net/URL;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 551
    invoke-static {p0}, Lcom/squareup/a/a/t;->a(Ljava/net/URL;)I

    move-result v0

    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/a/a/t;->a(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/net/URL;->getPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/squareup/a/a/b/w;)V
    .locals 3

    .prologue
    .line 458
    iput-object p1, p0, Lcom/squareup/a/a/a/u;->o:Lcom/squareup/a/a/b/w;

    .line 459
    iget-boolean v0, p0, Lcom/squareup/a/a/a/u;->h:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "gzip"

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    const-string/jumbo v2, "Content-Encoding"

    invoke-virtual {v1, v2}, Lcom/squareup/a/a/a/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->i()Lcom/squareup/a/a/a/al;

    move-result-object v0

    const-string/jumbo v1, "Content-Encoding"

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/al;->b(Ljava/lang/String;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    const-string/jumbo v1, "Content-Length"

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/al;->b(Ljava/lang/String;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/al;->a()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    .line 464
    new-instance v0, Lcom/squareup/a/a/b/h;

    invoke-direct {v0, p1}, Lcom/squareup/a/a/b/h;-><init>(Lcom/squareup/a/a/b/w;)V

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->p:Lcom/squareup/a/a/b/w;

    .line 468
    :goto_0
    return-void

    .line 466
    :cond_0
    iput-object p1, p0, Lcom/squareup/a/a/a/u;->p:Lcom/squareup/a/a/b/w;

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 672
    const-string/jumbo v0, "Connection"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Keep-Alive"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Proxy-Authenticate"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Proxy-Authorization"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "TE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Trailers"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Transfer-Encoding"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Upgrade"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/io/IOException;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 347
    instance-of v0, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljava/security/cert/CertificateException;

    if-eqz v0, :cond_0

    move v0, v1

    .line 349
    :goto_0
    instance-of v3, p1, Ljava/net/ProtocolException;

    .line 350
    if-nez v0, :cond_1

    if-nez v3, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 347
    goto :goto_0

    :cond_1
    move v1, v2

    .line 350
    goto :goto_1
.end method

.method public static q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 546
    const-string/jumbo v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 547
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Java"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "java.version"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private s()Lcom/squareup/a/a/a/ai;
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->i()Lcom/squareup/a/a/a/al;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/ak;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/al;->a()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    return-object v0
.end method

.method private t()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 223
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->e:Lcom/squareup/a/a/a/as;

    if-nez v0, :cond_3

    .line 226
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->a()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 227
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 228
    :cond_1
    new-instance v0, Ljava/net/UnknownHostException;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v1}, Lcom/squareup/a/a/a/ac;->a()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_2
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->l()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 233
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->g()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v3

    .line 234
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->h()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v4

    .line 236
    :goto_0
    new-instance v0, Lcom/squareup/a/a;

    iget-object v2, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/ac;->a()Ljava/net/URL;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/a/a/t;->a(Ljava/net/URL;)I

    move-result v2

    iget-object v5, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v5}, Lcom/squareup/a/r;->i()Lcom/squareup/a/o;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v6}, Lcom/squareup/a/r;->c()Ljava/net/Proxy;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v7}, Lcom/squareup/a/r;->m()Ljava/util/List;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/squareup/a/a;-><init>(Ljava/lang/String;ILjavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lcom/squareup/a/o;Ljava/net/Proxy;Ljava/util/List;)V

    .line 238
    new-instance v1, Lcom/squareup/a/a/a/as;

    iget-object v2, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/ac;->b()Ljava/net/URI;

    move-result-object v3

    iget-object v2, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v2}, Lcom/squareup/a/r;->d()Ljava/net/ProxySelector;

    move-result-object v4

    iget-object v2, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v2}, Lcom/squareup/a/r;->j()Lcom/squareup/a/k;

    move-result-object v5

    sget-object v6, Lcom/squareup/a/a/l;->a:Lcom/squareup/a/a/l;

    iget-object v2, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v2}, Lcom/squareup/a/r;->l()Lcom/squareup/a/y;

    move-result-object v7

    move-object v2, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/a/a/a/as;-><init>(Lcom/squareup/a/a;Ljava/net/URI;Ljava/net/ProxySelector;Lcom/squareup/a/k;Lcom/squareup/a/a/l;Lcom/squareup/a/y;)V

    iput-object v1, p0, Lcom/squareup/a/a/a/u;->e:Lcom/squareup/a/a/a/as;

    .line 242
    :cond_3
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->e:Lcom/squareup/a/a/a/as;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v1}, Lcom/squareup/a/a/a/ac;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/as;->a(Ljava/lang/String;)Lcom/squareup/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    .line 243
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v0, p0}, Lcom/squareup/a/j;->a(Ljava/lang/Object;)V

    .line 245
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v0}, Lcom/squareup/a/j;->c()Z

    move-result v0

    if-nez v0, :cond_6

    .line 246
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v1}, Lcom/squareup/a/r;->a()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v2}, Lcom/squareup/a/r;->b()I

    move-result v2

    invoke-direct {p0}, Lcom/squareup/a/a/a/u;->w()Lcom/squareup/a/z;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/a/j;->a(IILcom/squareup/a/z;)V

    .line 247
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v0}, Lcom/squareup/a/j;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->j()Lcom/squareup/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v0, v1}, Lcom/squareup/a/k;->b(Lcom/squareup/a/j;)V

    .line 248
    :cond_4
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->l()Lcom/squareup/a/y;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v1}, Lcom/squareup/a/j;->d()Lcom/squareup/a/x;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/y;->b(Lcom/squareup/a/x;)V

    .line 253
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v0}, Lcom/squareup/a/j;->d()Lcom/squareup/a/x;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->f:Lcom/squareup/a/x;

    .line 254
    return-void

    .line 249
    :cond_6
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v0}, Lcom/squareup/a/j;->l()Z

    move-result v0

    if-nez v0, :cond_5

    .line 250
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v1}, Lcom/squareup/a/r;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/j;->b(I)V

    goto :goto_1

    :cond_7
    move-object v3, v4

    goto/16 :goto_0
.end method

.method private u()V
    .locals 3

    .prologue
    .line 362
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->f()Lcom/squareup/a/t;

    move-result-object v0

    .line 363
    if-nez v0, :cond_0

    .line 373
    :goto_0
    return-void

    .line 366
    :cond_0
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    iget-object v2, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-static {v1, v2}, Lcom/squareup/a/a/a/a;->a(Lcom/squareup/a/a/a/ai;Lcom/squareup/a/a/a/ac;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 367
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-interface {v0, v1}, Lcom/squareup/a/t;->b(Lcom/squareup/a/a/a/ac;)Z

    goto :goto_0

    .line 372
    :cond_1
    invoke-direct {p0}, Lcom/squareup/a/a/a/u;->s()Lcom/squareup/a/a/a/ai;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/a/t;->a(Lcom/squareup/a/a/a/ai;)Ljava/net/CacheRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->s:Ljava/net/CacheRequest;

    goto :goto_0
.end method

.method private v()V
    .locals 4

    .prologue
    .line 505
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->g()Lcom/squareup/a/a/a/af;

    move-result-object v0

    .line 507
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v1}, Lcom/squareup/a/a/a/ac;->i()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 508
    invoke-static {}, Lcom/squareup/a/a/a/u;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    .line 511
    :cond_0
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    const-string/jumbo v2, "Host"

    invoke-virtual {v1, v2}, Lcom/squareup/a/a/a/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 512
    const-string/jumbo v1, "Host"

    iget-object v2, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/ac;->a()Ljava/net/URL;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/a/a/a/u;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    .line 515
    :cond_1
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v1}, Lcom/squareup/a/j;->m()I

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    const-string/jumbo v2, "Connection"

    invoke-virtual {v1, v2}, Lcom/squareup/a/a/a/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 517
    const-string/jumbo v1, "Connection"

    const-string/jumbo v2, "Keep-Alive"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    .line 520
    :cond_3
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    const-string/jumbo v2, "Accept-Encoding"

    invoke-virtual {v1, v2}, Lcom/squareup/a/a/a/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    .line 521
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/squareup/a/a/a/u;->h:Z

    .line 522
    const-string/jumbo v1, "Accept-Encoding"

    const-string/jumbo v2, "gzip"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    .line 525
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/a/a/a/u;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    const-string/jumbo v2, "Content-Type"

    invoke-virtual {v1, v2}, Lcom/squareup/a/a/a/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    .line 526
    const-string/jumbo v1, "Content-Type"

    const-string/jumbo v2, "application/x-www-form-urlencoded"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    .line 529
    :cond_5
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v1}, Lcom/squareup/a/r;->e()Ljava/net/CookieHandler;

    move-result-object v1

    .line 530
    if-eqz v1, :cond_6

    .line 534
    invoke-virtual {v0}, Lcom/squareup/a/a/a/af;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/a/a/a/ac;->e()Lcom/squareup/a/a/a/f;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/squareup/a/a/a/aa;->a(Lcom/squareup/a/a/a/f;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 536
    iget-object v3, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v3}, Lcom/squareup/a/a/a/ac;->b()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Ljava/net/CookieHandler;->get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 539
    invoke-static {v0, v1}, Lcom/squareup/a/a/a/aa;->a(Lcom/squareup/a/a/a/af;Ljava/util/Map;)V

    .line 542
    :cond_6
    invoke-virtual {v0}, Lcom/squareup/a/a/a/af;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    .line 543
    return-void
.end method

.method private w()Lcom/squareup/a/z;
    .locals 5

    .prologue
    .line 683
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->l()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 689
    :goto_0
    return-object v0

    .line 685
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->i()Ljava/lang/String;

    move-result-object v0

    .line 686
    if-nez v0, :cond_1

    invoke-static {}, Lcom/squareup/a/a/a/u;->q()Ljava/lang/String;

    move-result-object v0

    .line 688
    :cond_1
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v1}, Lcom/squareup/a/a/a/ac;->a()Ljava/net/URL;

    move-result-object v2

    .line 689
    new-instance v1, Lcom/squareup/a/z;

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Lcom/squareup/a/a/t;->a(Ljava/net/URL;)I

    move-result v2

    iget-object v4, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v4}, Lcom/squareup/a/a/a/ac;->j()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v2, v0, v4}, Lcom/squareup/a/z;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/io/IOException;)Lcom/squareup/a/a/a/u;
    .locals 7

    .prologue
    .line 325
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->e:Lcom/squareup/a/a/a/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->e:Lcom/squareup/a/a/a/as;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/a/a/a/as;->a(Lcom/squareup/a/j;Ljava/io/IOException;)V

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    instance-of v0, v0, Lcom/squareup/a/a/a/ar;

    if-eqz v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    .line 330
    :goto_0
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->e:Lcom/squareup/a/a/a/as;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->e:Lcom/squareup/a/a/a/as;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->e:Lcom/squareup/a/a/a/as;

    invoke-virtual {v1}, Lcom/squareup/a/a/a/as;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/a/a/a/u;->b(Ljava/io/IOException;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez v0, :cond_6

    .line 334
    :cond_4
    const/4 v0, 0x0

    .line 340
    :goto_1
    return-object v0

    .line 329
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 337
    :cond_6
    invoke-virtual {p0}, Lcom/squareup/a/a/a/u;->o()Lcom/squareup/a/j;

    move-result-object v4

    .line 340
    new-instance v0, Lcom/squareup/a/a/a/u;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    iget-object v2, p0, Lcom/squareup/a/a/a/u;->i:Lcom/squareup/a/a/a/ac;

    iget-boolean v3, p0, Lcom/squareup/a/a/a/u;->c:Z

    iget-object v5, p0, Lcom/squareup/a/a/a/u;->e:Lcom/squareup/a/a/a/as;

    iget-object v6, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    check-cast v6, Lcom/squareup/a/a/a/ar;

    invoke-direct/range {v0 .. v6}, Lcom/squareup/a/a/a/u;-><init>(Lcom/squareup/a/r;Lcom/squareup/a/a/a/ac;ZLcom/squareup/a/j;Lcom/squareup/a/a/a/as;Lcom/squareup/a/a/a/ar;)V

    goto :goto_1
.end method

.method public final a()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 157
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->m:Lcom/squareup/a/w;

    if-eqz v0, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 160
    :cond_2
    invoke-direct {p0}, Lcom/squareup/a/a/a/u;->v()V

    .line 161
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->f()Lcom/squareup/a/t;

    move-result-object v2

    .line 163
    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-interface {v2, v0}, Lcom/squareup/a/t;->a(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/ai;

    move-result-object v0

    .line 166
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 167
    new-instance v3, Lcom/squareup/a/a/a/c;

    iget-object v6, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-direct {v3, v4, v5, v6, v0}, Lcom/squareup/a/a/a/c;-><init>(JLcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ai;)V

    invoke-virtual {v3}, Lcom/squareup/a/a/a/c;->a()Lcom/squareup/a/a/a/a;

    move-result-object v3

    .line 168
    iget-object v4, v3, Lcom/squareup/a/a/a/a;->c:Lcom/squareup/a/w;

    iput-object v4, p0, Lcom/squareup/a/a/a/u;->m:Lcom/squareup/a/w;

    .line 169
    iget-object v4, v3, Lcom/squareup/a/a/a/a;->a:Lcom/squareup/a/a/a/ac;

    iput-object v4, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    .line 171
    if-eqz v2, :cond_3

    .line 172
    iget-object v4, p0, Lcom/squareup/a/a/a/u;->m:Lcom/squareup/a/w;

    invoke-interface {v2, v4}, Lcom/squareup/a/t;->a(Lcom/squareup/a/w;)V

    .line 175
    :cond_3
    iget-object v2, p0, Lcom/squareup/a/a/a/u;->m:Lcom/squareup/a/w;

    sget-object v4, Lcom/squareup/a/w;->c:Lcom/squareup/a/w;

    if-eq v2, v4, :cond_4

    .line 176
    iget-object v2, v3, Lcom/squareup/a/a/a/a;->b:Lcom/squareup/a/a/a/ai;

    iput-object v2, p0, Lcom/squareup/a/a/a/u;->r:Lcom/squareup/a/a/a/ai;

    .line 179
    :cond_4
    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/squareup/a/a/a/u;->m:Lcom/squareup/a/w;

    invoke-virtual {v2}, Lcom/squareup/a/w;->b()Z

    move-result v2

    if-nez v2, :cond_5

    .line 180
    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->h()Lcom/squareup/a/a/a/ak;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/t;->a(Ljava/io/Closeable;)V

    .line 183
    :cond_5
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->m:Lcom/squareup/a/w;

    invoke-virtual {v0}, Lcom/squareup/a/w;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 185
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    if-nez v0, :cond_6

    .line 186
    invoke-direct {p0}, Lcom/squareup/a/a/a/u;->t()V

    .line 190
    :cond_6
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v0}, Lcom/squareup/a/j;->a()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_8

    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v0}, Lcom/squareup/a/j;->l()Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_7
    move-object v0, v1

    .line 163
    goto :goto_1

    .line 192
    :cond_8
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v0, p0}, Lcom/squareup/a/j;->a(Lcom/squareup/a/a/a/u;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/a/aw;

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    .line 196
    invoke-virtual {p0}, Lcom/squareup/a/a/a/u;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    if-nez v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-interface {v0, v1}, Lcom/squareup/a/a/a/aw;->a(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/b/v;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    goto/16 :goto_0

    .line 202
    :cond_9
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    if-eqz v0, :cond_a

    .line 203
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->j()Lcom/squareup/a/k;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v0, v2}, Lcom/squareup/a/k;->a(Lcom/squareup/a/j;)V

    .line 204
    iput-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    .line 208
    :cond_a
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->r:Lcom/squareup/a/a/a/ai;

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    .line 209
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->r:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->h()Lcom/squareup/a/a/a/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->r:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->h()Lcom/squareup/a/a/a/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ak;->b()Lcom/squareup/a/a/b/w;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/a/a/a/u;->a(Lcom/squareup/a/a/b/w;)V

    goto/16 :goto_0
.end method

.method public a(Lcom/squareup/a/a/a/f;)V
    .locals 3

    .prologue
    .line 694
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->e()Ljava/net/CookieHandler;

    move-result-object v0

    .line 695
    if-eqz v0, :cond_0

    .line 696
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v1}, Lcom/squareup/a/a/a/ac;->b()Ljava/net/URI;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/squareup/a/a/a/aa;->a(Lcom/squareup/a/a/a/f;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/CookieHandler;->put(Ljava/net/URI;Ljava/util/Map;)V

    .line 698
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 261
    iget-wide v0, p0, Lcom/squareup/a/a/a/u;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 262
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/a/a/a/u;->b:J

    .line 263
    return-void
.end method

.method c()Z
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/a/v;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final d()Lcom/squareup/a/a/b/v;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->m:Lcom/squareup/a/w;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    return-object v0
.end method

.method public final e()Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->l:Lcom/squareup/a/a/b/b;

    .line 277
    if-eqz v0, :cond_0

    .line 279
    :goto_0
    return-object v0

    .line 278
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/a/a/a/u;->d()Lcom/squareup/a/a/b/v;

    move-result-object v0

    .line 279
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/v;)Lcom/squareup/a/a/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->l:Lcom/squareup/a/a/b/b;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/squareup/a/a/a/ac;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    return-object v0
.end method

.method public final h()Lcom/squareup/a/a/a/ai;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    return-object v0
.end method

.method public final i()Lcom/squareup/a/a/b/w;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->p:Lcom/squareup/a/a/b/w;

    return-object v0
.end method

.method public final j()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->q:Ljava/io/InputStream;

    .line 310
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/a/a/a/u;->i()Lcom/squareup/a/a/b/w;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/w;)Lcom/squareup/a/a/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->k()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->q:Ljava/io/InputStream;

    goto :goto_0
.end method

.method public final k()Lcom/squareup/a/j;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    return-object v0
.end method

.method public l()Lcom/squareup/a/x;
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->f:Lcom/squareup/a/x;

    return-object v0
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    invoke-interface {v0}, Lcom/squareup/a/a/a/aw;->c()V

    .line 384
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    .line 385
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    invoke-interface {v0, p0}, Lcom/squareup/a/a/a/aw;->a(Lcom/squareup/a/a/a/u;)V

    .line 397
    :cond_0
    return-void
.end method

.method public final o()Lcom/squareup/a/j;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 404
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->l:Lcom/squareup/a/a/b/b;

    if-eqz v1, :cond_1

    .line 406
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->l:Lcom/squareup/a/a/b/b;

    invoke-static {v1}, Lcom/squareup/a/a/t;->a(Ljava/io/Closeable;)V

    .line 412
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->p:Lcom/squareup/a/a/b/w;

    if-nez v1, :cond_2

    .line 413
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-static {v1}, Lcom/squareup/a/a/t;->a(Ljava/io/Closeable;)V

    .line 414
    iput-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    .line 438
    :goto_1
    return-object v0

    .line 407
    :cond_1
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    if-eqz v1, :cond_0

    .line 408
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    invoke-static {v1}, Lcom/squareup/a/a/t;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 419
    :cond_2
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->p:Lcom/squareup/a/a/b/w;

    invoke-static {v1}, Lcom/squareup/a/a/t;->a(Ljava/io/Closeable;)V

    .line 422
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->q:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/squareup/a/a/t;->a(Ljava/io/Closeable;)V

    .line 425
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    invoke-interface {v1}, Lcom/squareup/a/a/a/aw;->d()Z

    move-result v1

    if-nez v1, :cond_3

    .line 426
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-static {v1}, Lcom/squareup/a/a/t;->a(Ljava/io/Closeable;)V

    .line 427
    iput-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    goto :goto_1

    .line 432
    :cond_3
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v1}, Lcom/squareup/a/j;->b()Z

    move-result v1

    if-nez v1, :cond_4

    .line 433
    iput-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    .line 436
    :cond_4
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    .line 437
    iput-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    move-object v0, v1

    .line 438
    goto :goto_1
.end method

.method public final p()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 476
    iget-object v2, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/ac;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "HEAD"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 495
    :cond_0
    :goto_0
    return v0

    .line 480
    :cond_1
    iget-object v2, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/ai;->c()I

    move-result v2

    .line 481
    const/16 v3, 0x64

    if-lt v2, v3, :cond_2

    const/16 v3, 0xc8

    if-lt v2, v3, :cond_3

    :cond_2
    const/16 v3, 0xcc

    if-eq v2, v3, :cond_3

    const/16 v3, 0x130

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 484
    goto :goto_0

    .line 490
    :cond_3
    iget-object v2, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    invoke-static {v2}, Lcom/squareup/a/a/a/aa;->a(Lcom/squareup/a/a/a/ai;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    const-string/jumbo v2, "chunked"

    iget-object v3, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    const-string/jumbo v4, "Transfer-Encoding"

    invoke-virtual {v3, v4}, Lcom/squareup/a/a/a/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    .line 492
    goto :goto_0
.end method

.method public final r()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 561
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    if-eqz v0, :cond_1

    .line 635
    :cond_0
    :goto_0
    return-void

    .line 562
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->m:Lcom/squareup/a/w;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "call sendRequest() first!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 563
    :cond_2
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->m:Lcom/squareup/a/w;

    invoke-virtual {v0}, Lcom/squareup/a/w;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->l:Lcom/squareup/a/a/b/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/a/a/a/u;->l:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->b()Lcom/squareup/a/a/b/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 567
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->l:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V

    .line 570
    :cond_3
    iget-wide v0, p0, Lcom/squareup/a/a/a/u;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_5

    .line 571
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-static {v0}, Lcom/squareup/a/a/a/aa;->a(Lcom/squareup/a/a/a/ac;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    instance-of v0, v0, Lcom/squareup/a/a/a/ar;

    if-eqz v0, :cond_4

    .line 573
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    check-cast v0, Lcom/squareup/a/a/a/ar;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ar;->b()J

    move-result-wide v0

    .line 574
    iget-object v2, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/ac;->g()Lcom/squareup/a/a/a/af;

    move-result-object v2

    const-string/jumbo v3, "Content-Length"

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/af;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    .line 578
    :cond_4
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-interface {v0, v1}, Lcom/squareup/a/a/a/aw;->b(Lcom/squareup/a/a/a/ac;)V

    .line 581
    :cond_5
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    if-eqz v0, :cond_6

    .line 582
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->l:Lcom/squareup/a/a/b/b;

    if-eqz v0, :cond_7

    .line 584
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->l:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->close()V

    .line 588
    :goto_1
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    instance-of v0, v0, Lcom/squareup/a/a/a/ar;

    if-eqz v0, :cond_6

    .line 589
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    iget-object v0, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    check-cast v0, Lcom/squareup/a/a/a/ar;

    invoke-interface {v1, v0}, Lcom/squareup/a/a/a/aw;->a(Lcom/squareup/a/a/a/ar;)V

    .line 593
    :cond_6
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    invoke-interface {v0}, Lcom/squareup/a/a/a/aw;->a()V

    .line 595
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    invoke-interface {v0}, Lcom/squareup/a/a/a/aw;->b()Lcom/squareup/a/a/a/al;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->j:Lcom/squareup/a/a/a/ac;

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    invoke-virtual {v1}, Lcom/squareup/a/j;->k()Lcom/squareup/a/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/m;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    sget-object v1, Lcom/squareup/a/a/a/aa;->b:Ljava/lang/String;

    iget-wide v2, p0, Lcom/squareup/a/a/a/u;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/a/a/a/al;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    sget-object v1, Lcom/squareup/a/a/a/aa;->c:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/a/a/a/al;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->m:Lcom/squareup/a/w;

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/w;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/al;->a()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    .line 602
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->d:Lcom/squareup/a/j;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v1}, Lcom/squareup/a/a/a/ai;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/j;->a(I)V

    .line 603
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/a/u;->a(Lcom/squareup/a/a/a/f;)V

    .line 605
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->m:Lcom/squareup/a/w;

    sget-object v1, Lcom/squareup/a/w;->b:Lcom/squareup/a/w;

    if-ne v0, v1, :cond_9

    .line 606
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->r:Lcom/squareup/a/a/a/ai;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/ai;->a(Lcom/squareup/a/a/a/ai;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 607
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    invoke-interface {v0}, Lcom/squareup/a/a/a/aw;->e()V

    .line 608
    invoke-virtual {p0}, Lcom/squareup/a/a/a/u;->m()V

    .line 609
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->r:Lcom/squareup/a/a/a/ai;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    invoke-static {v0, v1}, Lcom/squareup/a/a/a/u;->a(Lcom/squareup/a/a/a/ai;Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/a/a/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->n:Lcom/squareup/a/a/a/ai;

    .line 613
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->f()Lcom/squareup/a/t;

    move-result-object v0

    .line 614
    invoke-interface {v0}, Lcom/squareup/a/t;->a()V

    .line 615
    iget-object v1, p0, Lcom/squareup/a/a/a/u;->r:Lcom/squareup/a/a/a/ai;

    invoke-direct {p0}, Lcom/squareup/a/a/a/u;->s()Lcom/squareup/a/a/a/ai;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/a/t;->a(Lcom/squareup/a/a/a/ai;Lcom/squareup/a/a/a/ai;)V

    .line 617
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->r:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->h()Lcom/squareup/a/a/a/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 618
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->r:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->h()Lcom/squareup/a/a/a/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ak;->b()Lcom/squareup/a/a/b/w;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/a/a/a/u;->a(Lcom/squareup/a/a/b/w;)V

    goto/16 :goto_0

    .line 586
    :cond_7
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->k:Lcom/squareup/a/a/b/v;

    invoke-interface {v0}, Lcom/squareup/a/a/b/v;->close()V

    goto/16 :goto_1

    .line 622
    :cond_8
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->r:Lcom/squareup/a/a/a/ai;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->h()Lcom/squareup/a/a/a/ak;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/t;->a(Ljava/io/Closeable;)V

    .line 626
    :cond_9
    invoke-virtual {p0}, Lcom/squareup/a/a/a/u;->p()Z

    move-result v0

    if-nez v0, :cond_a

    .line 628
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->s:Ljava/net/CacheRequest;

    invoke-interface {v0, v1}, Lcom/squareup/a/a/a/aw;->a(Ljava/net/CacheRequest;)Lcom/squareup/a/a/b/w;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->o:Lcom/squareup/a/a/b/w;

    .line 629
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->o:Lcom/squareup/a/a/b/w;

    iput-object v0, p0, Lcom/squareup/a/a/a/u;->p:Lcom/squareup/a/a/b/w;

    goto/16 :goto_0

    .line 633
    :cond_a
    invoke-direct {p0}, Lcom/squareup/a/a/a/u;->u()V

    .line 634
    iget-object v0, p0, Lcom/squareup/a/a/a/u;->g:Lcom/squareup/a/a/a/aw;

    iget-object v1, p0, Lcom/squareup/a/a/a/u;->s:Ljava/net/CacheRequest;

    invoke-interface {v0, v1}, Lcom/squareup/a/a/a/aw;->a(Ljava/net/CacheRequest;)Lcom/squareup/a/a/b/w;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/a/a/a/u;->a(Lcom/squareup/a/a/b/w;)V

    goto/16 :goto_0
.end method
