.class public Lcom/squareup/a/a/a/af;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/net/URL;

.field private b:Ljava/lang/String;

.field private c:Lcom/squareup/a/a/a/h;

.field private d:Lcom/squareup/a/a/a/ae;

.field private e:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    const-string/jumbo v0, "GET"

    iput-object v0, p0, Lcom/squareup/a/a/a/af;->b:Ljava/lang/String;

    .line 243
    new-instance v0, Lcom/squareup/a/a/a/h;

    invoke-direct {v0}, Lcom/squareup/a/a/a/h;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/a/af;->c:Lcom/squareup/a/a/a/h;

    .line 244
    return-void
.end method

.method private constructor <init>(Lcom/squareup/a/a/a/ac;)V
    .locals 1

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    invoke-static {p1}, Lcom/squareup/a/a/a/ac;->a(Lcom/squareup/a/a/a/ac;)Ljava/net/URL;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/af;->a:Ljava/net/URL;

    .line 248
    invoke-static {p1}, Lcom/squareup/a/a/a/ac;->b(Lcom/squareup/a/a/a/ac;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/af;->b:Ljava/lang/String;

    .line 249
    invoke-static {p1}, Lcom/squareup/a/a/a/ac;->c(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/af;->d:Lcom/squareup/a/a/a/ae;

    .line 250
    invoke-static {p1}, Lcom/squareup/a/a/a/ac;->d(Lcom/squareup/a/a/a/ac;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/af;->e:Ljava/lang/Object;

    .line 251
    invoke-static {p1}, Lcom/squareup/a/a/a/ac;->e(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/f;->b()Lcom/squareup/a/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/af;->c:Lcom/squareup/a/a/a/h;

    .line 252
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ad;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/squareup/a/a/a/af;-><init>(Lcom/squareup/a/a/a/ac;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/a/af;)Ljava/net/URL;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/squareup/a/a/a/af;->a:Ljava/net/URL;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/a/a/a/af;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/squareup/a/a/a/af;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/a/a/a/af;)Lcom/squareup/a/a/a/h;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/squareup/a/a/a/af;->c:Lcom/squareup/a/a/a/h;

    return-object v0
.end method

.method static synthetic d(Lcom/squareup/a/a/a/af;)Lcom/squareup/a/a/a/ae;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/squareup/a/a/a/af;->d:Lcom/squareup/a/a/a/ae;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/a/a/a/af;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/squareup/a/a/a/af;->e:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/squareup/a/a/a/ac;
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/squareup/a/a/a/af;->a:Ljava/net/URL;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_0
    new-instance v0, Lcom/squareup/a/a/a/ac;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/a/a/a/ac;-><init>(Lcom/squareup/a/a/a/af;Lcom/squareup/a/a/a/ad;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/squareup/a/a/a/af;
    .locals 1

    .prologue
    .line 298
    const-string/jumbo v0, "User-Agent"

    invoke-virtual {p0, v0, p1}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lcom/squareup/a/a/a/ae;)Lcom/squareup/a/a/a/af;
    .locals 2

    .prologue
    .line 318
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 319
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "method == null || method.length() == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 321
    :cond_1
    iput-object p1, p0, Lcom/squareup/a/a/a/af;->b:Ljava/lang/String;

    .line 322
    iput-object p2, p0, Lcom/squareup/a/a/a/af;->d:Lcom/squareup/a/a/a/ae;

    .line 323
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/squareup/a/a/a/af;->c:Lcom/squareup/a/a/a/h;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/a/a/a/h;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    .line 274
    return-object p0
.end method

.method public a(Ljava/net/URL;)Lcom/squareup/a/a/a/af;
    .locals 2

    .prologue
    .line 263
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_0
    iput-object p1, p0, Lcom/squareup/a/a/a/af;->a:Ljava/net/URL;

    .line 265
    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/squareup/a/a/a/af;->c:Lcom/squareup/a/a/a/h;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/a/a/a/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    .line 283
    return-object p0
.end method
