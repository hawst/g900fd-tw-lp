.class public Lcom/squareup/a/a/a/x;
.super Ljava/net/HttpURLConnection;


# instance fields
.field final a:Lcom/squareup/a/r;

.field protected b:Ljava/io/IOException;

.field protected c:Lcom/squareup/a/a/a/u;

.field d:Lcom/squareup/a/m;

.field private e:Lcom/squareup/a/a/a/h;

.field private f:J

.field private g:I

.field private h:Lcom/squareup/a/x;


# direct methods
.method public constructor <init>(Ljava/net/URL;Lcom/squareup/a/r;)V
    .locals 2

    .prologue
    .line 95
    invoke-direct {p0, p1}, Ljava/net/HttpURLConnection;-><init>(Ljava/net/URL;)V

    .line 74
    new-instance v0, Lcom/squareup/a/a/a/h;

    invoke-direct {v0}, Lcom/squareup/a/a/a/h;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/a/x;->e:Lcom/squareup/a/a/a/h;

    .line 77
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/squareup/a/a/a/x;->f:J

    .line 96
    iput-object p2, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    .line 97
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/squareup/a/j;Lcom/squareup/a/a/a/ar;)Lcom/squareup/a/a/a/u;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 285
    new-instance v0, Lcom/squareup/a/a/a/af;

    invoke-direct {v0}, Lcom/squareup/a/a/a/af;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/a/a/a/x;->getURL()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/a/a/a/af;->a(Ljava/net/URL;)Lcom/squareup/a/a/a/af;

    move-result-object v0

    invoke-virtual {v0, p1, v5}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Lcom/squareup/a/a/a/ae;)Lcom/squareup/a/a/a/af;

    move-result-object v2

    .line 288
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->e:Lcom/squareup/a/a/a/h;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/h;->a()Lcom/squareup/a/a/a/f;

    move-result-object v3

    move v0, v1

    .line 289
    :goto_0
    invoke-virtual {v3}, Lcom/squareup/a/a/a/f;->a()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 290
    invoke-virtual {v3, v0}, Lcom/squareup/a/a/a/f;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0}, Lcom/squareup/a/a/a/f;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v6}, Lcom/squareup/a/a/a/af;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    .line 289
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 294
    :cond_0
    invoke-static {p1}, Lcom/squareup/a/a/a/v;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 295
    iget-wide v6, p0, Lcom/squareup/a/a/a/x;->f:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    .line 296
    const-string/jumbo v0, "Content-Length"

    iget-wide v6, p0, Lcom/squareup/a/a/a/x;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    move v3, v1

    .line 304
    :goto_1
    invoke-virtual {v2}, Lcom/squareup/a/a/a/af;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v2

    .line 307
    iget-object v1, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    .line 308
    invoke-virtual {v1}, Lcom/squareup/a/r;->f()Lcom/squareup/a/t;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/a/a/a/x;->getUseCaches()Z

    move-result v0

    if-nez v0, :cond_1

    .line 309
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->o()Lcom/squareup/a/r;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/squareup/a/r;->a(Lcom/squareup/a/t;)Lcom/squareup/a/r;

    move-result-object v1

    .line 312
    :cond_1
    new-instance v0, Lcom/squareup/a/a/a/u;

    move-object v4, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/a/a/a/u;-><init>(Lcom/squareup/a/r;Lcom/squareup/a/a/a/ac;ZLcom/squareup/a/j;Lcom/squareup/a/a/a/as;Lcom/squareup/a/a/a/ar;)V

    return-object v0

    .line 297
    :cond_2
    iget v0, p0, Lcom/squareup/a/a/a/x;->chunkLength:I

    if-lez v0, :cond_3

    .line 298
    const-string/jumbo v0, "Transfer-Encoding"

    const-string/jumbo v3, "chunked"

    invoke-virtual {v2, v0, v3}, Lcom/squareup/a/a/a/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/af;

    move v3, v1

    goto :goto_1

    .line 300
    :cond_3
    const/4 v1, 0x1

    move v3, v1

    goto :goto_1

    :cond_4
    move v3, v1

    goto :goto_1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 259
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->b:Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->b:Ljava/io/IOException;

    throw v0

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    if-eqz v0, :cond_1

    .line 281
    :goto_0
    return-void

    .line 265
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/a/a/a/x;->connected:Z

    .line 267
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/a/x;->doOutput:Z

    if-eqz v0, :cond_2

    .line 268
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->method:Ljava/lang/String;

    const-string/jumbo v1, "GET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 270
    const-string/jumbo v0, "POST"

    iput-object v0, p0, Lcom/squareup/a/a/a/x;->method:Ljava/lang/String;

    .line 276
    :cond_2
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->method:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/a/a/a/x;->a(Ljava/lang/String;Lcom/squareup/a/j;Lcom/squareup/a/a/a/ar;)Lcom/squareup/a/a/a/u;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 277
    :catch_0
    move-exception v0

    .line 278
    iput-object v0, p0, Lcom/squareup/a/a/a/x;->b:Ljava/io/IOException;

    .line 279
    throw v0

    .line 271
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->method:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/a/a/a/v;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 273
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/squareup/a/a/a/x;->method:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " does not support writing"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 562
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 563
    if-eqz p2, :cond_0

    .line 564
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->m()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 566
    :cond_0
    const-string/jumbo v0, ","

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 568
    :try_start_0
    invoke-static {v4}, Lcom/squareup/a/a/b/d;->a(Ljava/lang/String;)Lcom/squareup/a/a/b/d;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/a/a/t;->a(Lcom/squareup/a/a/b/d;)Lcom/squareup/a/v;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 569
    :catch_0
    move-exception v0

    .line 570
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 573
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    invoke-virtual {v0, v1}, Lcom/squareup/a/r;->a(Ljava/util/List;)Lcom/squareup/a/r;

    .line 574
    return-void
.end method

.method private a(Z)Z
    .locals 2

    .prologue
    .line 375
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->a()V

    .line 376
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->l()Lcom/squareup/a/x;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/x;->h:Lcom/squareup/a/x;

    .line 377
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->k()Lcom/squareup/a/j;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->k()Lcom/squareup/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/j;->k()Lcom/squareup/a/m;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/squareup/a/a/a/x;->d:Lcom/squareup/a/m;

    .line 380
    if-eqz p1, :cond_0

    .line 381
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->r()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 384
    :cond_0
    const/4 v0, 0x1

    .line 389
    :goto_1
    return v0

    .line 377
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 385
    :catch_0
    move-exception v0

    .line 386
    iget-object v1, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v1, v0}, Lcom/squareup/a/a/a/u;->a(Ljava/io/IOException;)Lcom/squareup/a/a/a/u;

    move-result-object v1

    .line 387
    if-eqz v1, :cond_2

    .line 388
    iput-object v1, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    .line 389
    const/4 v0, 0x0

    goto :goto_1

    .line 393
    :cond_2
    iput-object v0, p0, Lcom/squareup/a/a/a/x;->b:Ljava/io/IOException;

    .line 394
    throw v0
.end method

.method private b()Lcom/squareup/a/a/a/u;
    .locals 5

    .prologue
    .line 321
    invoke-direct {p0}, Lcom/squareup/a/a/a/x;->a()V

    .line 323
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    .line 335
    :goto_0
    return-object v0

    .line 328
    :cond_0
    :goto_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/squareup/a/a/a/x;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    invoke-direct {p0}, Lcom/squareup/a/a/a/x;->c()Lcom/squareup/a/a/a/y;

    move-result-object v2

    .line 333
    sget-object v0, Lcom/squareup/a/a/a/y;->a:Lcom/squareup/a/a/a/y;

    if-ne v2, v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->m()V

    .line 335
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    goto :goto_0

    .line 339
    :cond_1
    iget-object v1, p0, Lcom/squareup/a/a/a/x;->method:Ljava/lang/String;

    .line 340
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->d()Lcom/squareup/a/a/b/v;

    move-result-object v0

    .line 345
    iget-object v3, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v3}, Lcom/squareup/a/a/a/u;->h()Lcom/squareup/a/a/a/ai;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/a/a/a/ai;->c()I

    move-result v3

    .line 346
    const/16 v4, 0x12c

    if-eq v3, v4, :cond_2

    const/16 v4, 0x12d

    if-eq v3, v4, :cond_2

    const/16 v4, 0x12e

    if-eq v3, v4, :cond_2

    const/16 v4, 0x12f

    if-ne v3, v4, :cond_3

    .line 350
    :cond_2
    const-string/jumbo v1, "GET"

    .line 351
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->e:Lcom/squareup/a/a/a/h;

    const-string/jumbo v4, "Content-Length"

    invoke-virtual {v0, v4}, Lcom/squareup/a/a/a/h;->b(Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    .line 352
    const/4 v0, 0x0

    .line 355
    :cond_3
    if-eqz v0, :cond_4

    instance-of v4, v0, Lcom/squareup/a/a/a/ar;

    if-nez v4, :cond_4

    .line 356
    new-instance v0, Ljava/net/HttpRetryException;

    const-string/jumbo v1, "Cannot retry streamed HTTP body"

    invoke-direct {v0, v1, v3}, Ljava/net/HttpRetryException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 359
    :cond_4
    sget-object v3, Lcom/squareup/a/a/a/y;->c:Lcom/squareup/a/a/a/y;

    if-ne v2, v3, :cond_5

    .line 360
    iget-object v2, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/u;->m()V

    .line 363
    :cond_5
    iget-object v2, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/u;->o()Lcom/squareup/a/j;

    move-result-object v2

    .line 364
    check-cast v0, Lcom/squareup/a/a/a/ar;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/a/a/a/x;->a(Ljava/lang/String;Lcom/squareup/a/j;Lcom/squareup/a/a/a/ar;)Lcom/squareup/a/a/a/u;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    goto :goto_1
.end method

.method private c()Lcom/squareup/a/a/a/y;
    .locals 4

    .prologue
    .line 410
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->k()Lcom/squareup/a/j;

    move-result-object v0

    .line 411
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/a/j;->d()Lcom/squareup/a/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/x;->b()Ljava/net/Proxy;

    move-result-object v0

    .line 414
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/a/a/a/x;->getResponseCode()I

    move-result v1

    .line 415
    sparse-switch v1, :sswitch_data_0

    .line 466
    sget-object v0, Lcom/squareup/a/a/a/y;->a:Lcom/squareup/a/a/a/y;

    :goto_1
    return-object v0

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->c()Ljava/net/Proxy;

    move-result-object v0

    goto :goto_0

    .line 417
    :sswitch_0
    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v2, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-eq v1, v2, :cond_1

    .line 418
    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "Received HTTP_PROXY_AUTH (407) code while not using proxy"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 422
    :cond_1
    :sswitch_1
    iget-object v1, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    invoke-virtual {v1}, Lcom/squareup/a/r;->i()Lcom/squareup/a/o;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/u;->h()Lcom/squareup/a/a/a/ai;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/squareup/a/a/a/i;->a(Lcom/squareup/a/o;Lcom/squareup/a/a/a/ai;Ljava/net/Proxy;)Lcom/squareup/a/a/a/ac;

    move-result-object v0

    .line 424
    if-nez v0, :cond_2

    sget-object v0, Lcom/squareup/a/a/a/y;->a:Lcom/squareup/a/a/a/y;

    goto :goto_1

    .line 425
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->h()Lcom/squareup/a/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/f;->b()Lcom/squareup/a/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/x;->e:Lcom/squareup/a/a/a/h;

    .line 426
    sget-object v0, Lcom/squareup/a/a/a/y;->b:Lcom/squareup/a/a/a/y;

    goto :goto_1

    .line 433
    :sswitch_2
    invoke-virtual {p0}, Lcom/squareup/a/a/a/x;->getInstanceFollowRedirects()Z

    move-result v0

    if-nez v0, :cond_3

    .line 434
    sget-object v0, Lcom/squareup/a/a/a/y;->a:Lcom/squareup/a/a/a/y;

    goto :goto_1

    .line 436
    :cond_3
    iget v0, p0, Lcom/squareup/a/a/a/x;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/a/a/a/x;->g:I

    const/16 v2, 0x14

    if-le v0, v2, :cond_4

    .line 437
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Too many redirects: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/a/a/a/x;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 439
    :cond_4
    const/16 v0, 0x133

    if-ne v1, v0, :cond_5

    iget-object v0, p0, Lcom/squareup/a/a/a/x;->method:Ljava/lang/String;

    const-string/jumbo v1, "GET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/squareup/a/a/a/x;->method:Ljava/lang/String;

    const-string/jumbo v1, "HEAD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 442
    sget-object v0, Lcom/squareup/a/a/a/y;->a:Lcom/squareup/a/a/a/y;

    goto/16 :goto_1

    .line 444
    :cond_5
    const-string/jumbo v0, "Location"

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/a/x;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 445
    if-nez v0, :cond_6

    .line 446
    sget-object v0, Lcom/squareup/a/a/a/y;->a:Lcom/squareup/a/a/a/y;

    goto/16 :goto_1

    .line 448
    :cond_6
    iget-object v1, p0, Lcom/squareup/a/a/a/x;->url:Ljava/net/URL;

    .line 449
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1, v0}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/squareup/a/a/a/x;->url:Ljava/net/URL;

    .line 450
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/squareup/a/a/a/x;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 451
    sget-object v0, Lcom/squareup/a/a/a/y;->a:Lcom/squareup/a/a/a/y;

    goto/16 :goto_1

    .line 453
    :cond_7
    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/a/a/a/x;->url:Ljava/net/URL;

    invoke-virtual {v2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 454
    if-nez v2, :cond_8

    iget-object v0, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->k()Z

    move-result v0

    if-nez v0, :cond_8

    .line 455
    sget-object v0, Lcom/squareup/a/a/a/y;->a:Lcom/squareup/a/a/a/y;

    goto/16 :goto_1

    .line 457
    :cond_8
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/a/a/a/x;->url:Ljava/net/URL;

    invoke-virtual {v3}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 458
    invoke-static {v1}, Lcom/squareup/a/a/t;->a(Ljava/net/URL;)I

    move-result v0

    iget-object v1, p0, Lcom/squareup/a/a/a/x;->url:Ljava/net/URL;

    invoke-static {v1}, Lcom/squareup/a/a/t;->a(Ljava/net/URL;)I

    move-result v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    .line 459
    :goto_2
    if-eqz v3, :cond_a

    if-eqz v0, :cond_a

    if-eqz v2, :cond_a

    .line 460
    sget-object v0, Lcom/squareup/a/a/a/y;->b:Lcom/squareup/a/a/a/y;

    goto/16 :goto_1

    .line 458
    :cond_9
    const/4 v0, 0x0

    goto :goto_2

    .line 462
    :cond_a
    sget-object v0, Lcom/squareup/a/a/a/y;->c:Lcom/squareup/a/a/a/y;

    goto/16 :goto_1

    .line 415
    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_2
        0x12d -> :sswitch_2
        0x12e -> :sswitch_2
        0x12f -> :sswitch_2
        0x133 -> :sswitch_2
        0x191 -> :sswitch_1
        0x197 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 531
    iget-boolean v0, p0, Lcom/squareup/a/a/a/x;->connected:Z

    if-eqz v0, :cond_0

    .line 532
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot add request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 534
    :cond_0
    if-nez p1, :cond_1

    .line 535
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "field == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 537
    :cond_1
    if-nez p2, :cond_2

    .line 543
    invoke-static {}, Lcom/squareup/a/a/o;->a()Lcom/squareup/a/a/o;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Ignoring header "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " because its value was null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/o;->a(Ljava/lang/String;)V

    .line 553
    :goto_0
    return-void

    .line 548
    :cond_2
    const-string/jumbo v0, "X-Android-Transports"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "X-Android-Protocols"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 549
    :cond_3
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/squareup/a/a/a/x;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 551
    :cond_4
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->e:Lcom/squareup/a/a/a/h;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/a/a/a/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    goto :goto_0
.end method

.method public final connect()V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/squareup/a/a/a/x;->a()V

    .line 103
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/squareup/a/a/a/x;->a(Z)Z

    move-result v0

    .line 104
    if-eqz v0, :cond_0

    .line 105
    return-void
.end method

.method public final disconnect()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    if-nez v0, :cond_0

    .line 121
    :goto_0
    return-void

    .line 112
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->n()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getConnectTimeout()I
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->a()I

    move-result v0

    return v0
.end method

.method public final getErrorStream()Ljava/io/InputStream;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 129
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/a/a/a/x;->b()Lcom/squareup/a/a/a/u;

    move-result-object v1

    .line 130
    invoke-virtual {v1}, Lcom/squareup/a/a/a/u;->p()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/a/a/a/u;->h()Lcom/squareup/a/a/a/ai;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/a/a/a/ai;->c()I

    move-result v2

    const/16 v3, 0x190

    if-lt v2, v3, :cond_0

    .line 131
    invoke-virtual {v1}, Lcom/squareup/a/a/a/u;->j()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 135
    :cond_0
    :goto_0
    return-object v0

    .line 134
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final getHeaderField(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/a/a/a/x;->b()Lcom/squareup/a/a/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->h()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/f;->b(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 147
    :goto_0
    return-object v0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/a/a/a/x;->b()Lcom/squareup/a/a/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->h()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    .line 159
    if-nez p1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->b()Ljava/lang/String;

    move-result-object v0

    .line 161
    :goto_0
    return-object v0

    .line 159
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/f;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 160
    :catch_0
    move-exception v0

    .line 161
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeaderFieldKey(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/a/a/a/x;->b()Lcom/squareup/a/a/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->h()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/f;->a(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 169
    :goto_0
    return-object v0

    .line 168
    :catch_0
    move-exception v0

    .line 169
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeaderFields()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 175
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/a/a/a/x;->b()Lcom/squareup/a/a/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->h()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    .line 176
    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->g()Lcom/squareup/a/a/a/f;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/a/a/a/aa;->a(Lcom/squareup/a/a/a/f;Ljava/lang/String;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 178
    :goto_0
    return-object v0

    .line 177
    :catch_0
    move-exception v0

    .line 178
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/squareup/a/a/a/x;->doInput:Z

    if-nez v0, :cond_0

    .line 193
    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "This protocol does not support input"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 196
    :cond_0
    invoke-direct {p0}, Lcom/squareup/a/a/a/x;->b()Lcom/squareup/a/a/a/u;

    move-result-object v0

    .line 202
    invoke-virtual {p0}, Lcom/squareup/a/a/a/x;->getResponseCode()I

    move-result v1

    const/16 v2, 0x190

    if-lt v1, v2, :cond_1

    .line 203
    new-instance v0, Ljava/io/FileNotFoundException;

    iget-object v1, p0, Lcom/squareup/a/a/a/x;->url:Ljava/net/URL;

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->j()Ljava/io/InputStream;

    move-result-object v0

    .line 207
    if-nez v0, :cond_2

    .line 208
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No response body exists; responseCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/a/a/a/x;->getResponseCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 210
    :cond_2
    return-object v0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 3

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/squareup/a/a/a/x;->connect()V

    .line 216
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->e()Lcom/squareup/a/a/b/b;

    move-result-object v0

    .line 217
    if-nez v0, :cond_0

    .line 218
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "method does not support a request body: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/a/a/a/x;->method:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/squareup/a/a/a/x;->c:Lcom/squareup/a/a/a/u;

    invoke-virtual {v1}, Lcom/squareup/a/a/a/u;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220
    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "cannot write request body after response has been read"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 223
    :cond_1
    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->d()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public final getPermission()Ljava/security/Permission;
    .locals 4

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/squareup/a/a/a/x;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 228
    invoke-virtual {p0}, Lcom/squareup/a/a/a/x;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/t;->a(Ljava/net/URL;)I

    move-result v0

    .line 229
    invoke-virtual {p0}, Lcom/squareup/a/a/a/x;->usingProxy()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 230
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->c()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    .line 231
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    .line 232
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    .line 234
    :cond_0
    new-instance v2, Ljava/net/SocketPermission;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "connect, resolve"

    invoke-direct {v2, v0, v1}, Ljava/net/SocketPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public getReadTimeout()I
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->b()I

    move-result v0

    return v0
.end method

.method public final getRequestProperties()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/squareup/a/a/a/x;->connected:Z

    if-eqz v0, :cond_0

    .line 184
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot access request header fields after connection is set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->e:Lcom/squareup/a/a/a/h;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/h;->a()Lcom/squareup/a/a/a/f;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/a/a/a/aa;->a(Lcom/squareup/a/a/a/f;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final getRequestProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 239
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->e:Lcom/squareup/a/a/a/h;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/h;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getResponseCode()I
    .locals 1

    .prologue
    .line 493
    invoke-direct {p0}, Lcom/squareup/a/a/a/x;->b()Lcom/squareup/a/a/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->h()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->c()I

    move-result v0

    return v0
.end method

.method public getResponseMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 489
    invoke-direct {p0}, Lcom/squareup/a/a/a/x;->b()Lcom/squareup/a/a/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->h()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ai;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setConnectTimeout(I)V
    .locals 4

    .prologue
    .line 243
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    int-to-long v2, p1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcom/squareup/a/r;->a(JLjava/util/concurrent/TimeUnit;)V

    .line 244
    return-void
.end method

.method public setFixedLengthStreamingMode(I)V
    .locals 2

    .prologue
    .line 585
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/a/x;->setFixedLengthStreamingMode(J)V

    .line 586
    return-void
.end method

.method public setFixedLengthStreamingMode(J)V
    .locals 3

    .prologue
    .line 590
    iget-boolean v0, p0, Ljava/net/HttpURLConnection;->connected:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 591
    :cond_0
    iget v0, p0, Lcom/squareup/a/a/a/x;->chunkLength:I

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Already in chunked mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 592
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "contentLength < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 593
    :cond_2
    iput-wide p1, p0, Lcom/squareup/a/a/a/x;->f:J

    .line 594
    const-wide/32 v0, 0x7fffffff

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Ljava/net/HttpURLConnection;->fixedContentLength:I

    .line 595
    return-void
.end method

.method public setIfModifiedSince(J)V
    .locals 7

    .prologue
    .line 522
    invoke-super {p0, p1, p2}, Ljava/net/HttpURLConnection;->setIfModifiedSince(J)V

    .line 523
    iget-wide v0, p0, Lcom/squareup/a/a/a/x;->ifModifiedSince:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 524
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->e:Lcom/squareup/a/a/a/h;

    const-string/jumbo v1, "If-Modified-Since"

    new-instance v2, Ljava/util/Date;

    iget-wide v4, p0, Lcom/squareup/a/a/a/x;->ifModifiedSince:J

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lcom/squareup/a/a/a/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/a/a/a/h;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    .line 528
    :goto_0
    return-void

    .line 526
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->e:Lcom/squareup/a/a/a/h;

    const-string/jumbo v1, "If-Modified-Since"

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/h;->b(Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    goto :goto_0
.end method

.method public setReadTimeout(I)V
    .locals 4

    .prologue
    .line 251
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    int-to-long v2, p1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcom/squareup/a/r;->b(JLjava/util/concurrent/TimeUnit;)V

    .line 252
    return-void
.end method

.method public setRequestMethod(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 577
    sget-object v0, Lcom/squareup/a/a/a/v;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 578
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Expected one of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/squareup/a/a/a/v;->a:Ljava/util/Set;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 581
    :cond_0
    iput-object p1, p0, Lcom/squareup/a/a/a/x;->method:Ljava/lang/String;

    .line 582
    return-void
.end method

.method public final setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 497
    iget-boolean v0, p0, Lcom/squareup/a/a/a/x;->connected:Z

    if-eqz v0, :cond_0

    .line 498
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot set request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500
    :cond_0
    if-nez p1, :cond_1

    .line 501
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "field == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 503
    :cond_1
    if-nez p2, :cond_2

    .line 509
    invoke-static {}, Lcom/squareup/a/a/o;->a()Lcom/squareup/a/a/o;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Ignoring header "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " because its value was null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/o;->a(Ljava/lang/String;)V

    .line 519
    :goto_0
    return-void

    .line 514
    :cond_2
    const-string/jumbo v0, "X-Android-Transports"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "X-Android-Protocols"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 515
    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/squareup/a/a/a/x;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 517
    :cond_4
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->e:Lcom/squareup/a/a/a/h;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/a/a/a/h;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    goto :goto_0
.end method

.method public final usingProxy()Z
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->h:Lcom/squareup/a/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/a/x;->h:Lcom/squareup/a/x;

    invoke-virtual {v0}, Lcom/squareup/a/x;->b()Ljava/net/Proxy;

    move-result-object v0

    .line 485
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/x;->a:Lcom/squareup/a/r;

    invoke-virtual {v0}, Lcom/squareup/a/r;->c()Ljava/net/Proxy;

    move-result-object v0

    goto :goto_0

    .line 485
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
