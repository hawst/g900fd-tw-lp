.class public final Lcom/squareup/a/a/h;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/squareup/a/a/e;

.field private final b:Lcom/squareup/a/a/j;

.field private final c:[Z

.field private d:Z

.field private e:Z


# direct methods
.method private constructor <init>(Lcom/squareup/a/a/e;Lcom/squareup/a/a/j;)V
    .locals 1

    .prologue
    .line 723
    iput-object p1, p0, Lcom/squareup/a/a/h;->a:Lcom/squareup/a/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 724
    iput-object p2, p0, Lcom/squareup/a/a/h;->b:Lcom/squareup/a/a/j;

    .line 725
    invoke-static {p2}, Lcom/squareup/a/a/j;->d(Lcom/squareup/a/a/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/squareup/a/a/h;->c:[Z

    .line 726
    return-void

    .line 725
    :cond_0
    invoke-static {p1}, Lcom/squareup/a/a/e;->e(Lcom/squareup/a/a/e;)I

    move-result v0

    new-array v0, v0, [Z

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/squareup/a/a/e;Lcom/squareup/a/a/j;Lcom/squareup/a/a/f;)V
    .locals 0

    .prologue
    .line 717
    invoke-direct {p0, p1, p2}, Lcom/squareup/a/a/h;-><init>(Lcom/squareup/a/a/e;Lcom/squareup/a/a/j;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/h;)Lcom/squareup/a/a/j;
    .locals 1

    .prologue
    .line 717
    iget-object v0, p0, Lcom/squareup/a/a/h;->b:Lcom/squareup/a/a/j;

    return-object v0
.end method

.method static synthetic a(Lcom/squareup/a/a/h;Z)Z
    .locals 0

    .prologue
    .line 717
    iput-boolean p1, p0, Lcom/squareup/a/a/h;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/squareup/a/a/h;)[Z
    .locals 1

    .prologue
    .line 717
    iget-object v0, p0, Lcom/squareup/a/a/h;->c:[Z

    return-object v0
.end method


# virtual methods
.method public a(I)Ljava/io/OutputStream;
    .locals 4

    .prologue
    .line 765
    iget-object v2, p0, Lcom/squareup/a/a/h;->a:Lcom/squareup/a/a/e;

    monitor-enter v2

    .line 766
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/h;->b:Lcom/squareup/a/a/j;

    invoke-static {v0}, Lcom/squareup/a/a/j;->a(Lcom/squareup/a/a/j;)Lcom/squareup/a/a/h;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 767
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 787
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 769
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/a/a/h;->b:Lcom/squareup/a/a/j;

    invoke-static {v0}, Lcom/squareup/a/a/j;->d(Lcom/squareup/a/a/j;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 770
    iget-object v0, p0, Lcom/squareup/a/a/h;->c:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    .line 772
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/h;->b:Lcom/squareup/a/a/j;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/j;->b(I)Ljava/io/File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 775
    :try_start_2
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v0

    .line 786
    :goto_0
    :try_start_3
    new-instance v0, Lcom/squareup/a/a/i;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v3}, Lcom/squareup/a/a/i;-><init>(Lcom/squareup/a/a/h;Ljava/io/OutputStream;Lcom/squareup/a/a/f;)V

    monitor-exit v2

    :goto_1
    return-object v0

    .line 776
    :catch_0
    move-exception v0

    .line 778
    iget-object v0, p0, Lcom/squareup/a/a/h;->a:Lcom/squareup/a/a/e;

    invoke-static {v0}, Lcom/squareup/a/a/e;->f(Lcom/squareup/a/a/e;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 780
    :try_start_4
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v1, v0

    .line 784
    goto :goto_0

    .line 781
    :catch_1
    move-exception v0

    .line 783
    :try_start_5
    invoke-static {}, Lcom/squareup/a/a/e;->b()Ljava/io/OutputStream;

    move-result-object v0

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 802
    iget-boolean v0, p0, Lcom/squareup/a/a/h;->d:Z

    if-eqz v0, :cond_0

    .line 803
    iget-object v0, p0, Lcom/squareup/a/a/h;->a:Lcom/squareup/a/a/e;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/squareup/a/a/e;->a(Lcom/squareup/a/a/e;Lcom/squareup/a/a/h;Z)V

    .line 804
    iget-object v0, p0, Lcom/squareup/a/a/h;->a:Lcom/squareup/a/a/e;

    iget-object v1, p0, Lcom/squareup/a/a/h;->b:Lcom/squareup/a/a/j;

    invoke-static {v1}, Lcom/squareup/a/a/j;->c(Lcom/squareup/a/a/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/e;->c(Ljava/lang/String;)Z

    .line 808
    :goto_0
    iput-boolean v2, p0, Lcom/squareup/a/a/h;->e:Z

    .line 809
    return-void

    .line 806
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/h;->a:Lcom/squareup/a/a/e;

    invoke-static {v0, p0, v2}, Lcom/squareup/a/a/e;->a(Lcom/squareup/a/a/e;Lcom/squareup/a/a/h;Z)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 816
    iget-object v0, p0, Lcom/squareup/a/a/h;->a:Lcom/squareup/a/a/e;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/squareup/a/a/e;->a(Lcom/squareup/a/a/e;Lcom/squareup/a/a/h;Z)V

    .line 817
    return-void
.end method
