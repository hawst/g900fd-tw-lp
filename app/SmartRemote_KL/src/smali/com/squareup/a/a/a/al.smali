.class public Lcom/squareup/a/a/a/al;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/squareup/a/a/a/ac;

.field private b:Lcom/squareup/a/a/a/av;

.field private c:Lcom/squareup/a/m;

.field private d:Lcom/squareup/a/a/a/h;

.field private e:Lcom/squareup/a/a/a/ak;

.field private f:Lcom/squareup/a/a/a/ai;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 368
    new-instance v0, Lcom/squareup/a/a/a/h;

    invoke-direct {v0}, Lcom/squareup/a/a/a/h;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/a/al;->d:Lcom/squareup/a/a/a/h;

    .line 369
    return-void
.end method

.method private constructor <init>(Lcom/squareup/a/a/a/ai;)V
    .locals 1

    .prologue
    .line 371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372
    invoke-static {p1}, Lcom/squareup/a/a/a/ai;->b(Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/a/a/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/al;->a:Lcom/squareup/a/a/a/ac;

    .line 373
    invoke-static {p1}, Lcom/squareup/a/a/a/ai;->c(Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/a/a/av;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/al;->b:Lcom/squareup/a/a/a/av;

    .line 374
    invoke-static {p1}, Lcom/squareup/a/a/a/ai;->d(Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/m;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/al;->c:Lcom/squareup/a/m;

    .line 375
    invoke-static {p1}, Lcom/squareup/a/a/a/ai;->e(Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/f;->b()Lcom/squareup/a/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/al;->d:Lcom/squareup/a/a/a/h;

    .line 376
    invoke-static {p1}, Lcom/squareup/a/a/a/ai;->f(Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/a/a/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/al;->e:Lcom/squareup/a/a/a/ak;

    .line 377
    invoke-static {p1}, Lcom/squareup/a/a/a/ai;->g(Lcom/squareup/a/a/a/ai;)Lcom/squareup/a/a/a/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/al;->f:Lcom/squareup/a/a/a/ai;

    .line 378
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/a/a/a/ai;Lcom/squareup/a/a/a/aj;)V
    .locals 0

    .prologue
    .line 359
    invoke-direct {p0, p1}, Lcom/squareup/a/a/a/al;-><init>(Lcom/squareup/a/a/a/ai;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/a/al;)Lcom/squareup/a/a/a/ac;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/squareup/a/a/a/al;->a:Lcom/squareup/a/a/a/ac;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/a/a/a/al;)Lcom/squareup/a/a/a/av;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/squareup/a/a/a/al;->b:Lcom/squareup/a/a/a/av;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/a/a/a/al;)Lcom/squareup/a/m;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/squareup/a/a/a/al;->c:Lcom/squareup/a/m;

    return-object v0
.end method

.method static synthetic d(Lcom/squareup/a/a/a/al;)Lcom/squareup/a/a/a/h;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/squareup/a/a/a/al;->d:Lcom/squareup/a/a/a/h;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/a/a/a/al;)Lcom/squareup/a/a/a/ak;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/squareup/a/a/a/al;->e:Lcom/squareup/a/a/a/ak;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/a/a/a/al;)Lcom/squareup/a/a/a/ai;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/squareup/a/a/a/al;->f:Lcom/squareup/a/a/a/ai;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/squareup/a/a/a/ai;
    .locals 2

    .prologue
    .line 449
    iget-object v0, p0, Lcom/squareup/a/a/a/al;->a:Lcom/squareup/a/a/a/ac;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "request == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 450
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/al;->b:Lcom/squareup/a/a/a/av;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "statusLine == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 451
    :cond_1
    new-instance v0, Lcom/squareup/a/a/a/ai;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/a/a/a/ai;-><init>(Lcom/squareup/a/a/a/al;Lcom/squareup/a/a/a/aj;)V

    return-object v0
.end method

.method public a(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/al;
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcom/squareup/a/a/a/al;->a:Lcom/squareup/a/a/a/ac;

    .line 382
    return-object p0
.end method

.method public a(Lcom/squareup/a/a/a/ak;)Lcom/squareup/a/a/a/al;
    .locals 0

    .prologue
    .line 434
    iput-object p1, p0, Lcom/squareup/a/a/a/al;->e:Lcom/squareup/a/a/a/ak;

    .line 435
    return-object p0
.end method

.method public a(Lcom/squareup/a/a/a/av;)Lcom/squareup/a/a/a/al;
    .locals 2

    .prologue
    .line 386
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "statusLine == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387
    :cond_0
    iput-object p1, p0, Lcom/squareup/a/a/a/al;->b:Lcom/squareup/a/a/a/av;

    .line 388
    return-object p0
.end method

.method public a(Lcom/squareup/a/a/a/f;)Lcom/squareup/a/a/a/al;
    .locals 1

    .prologue
    .line 429
    invoke-virtual {p1}, Lcom/squareup/a/a/a/f;->b()Lcom/squareup/a/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/al;->d:Lcom/squareup/a/a/a/h;

    .line 430
    return-object p0
.end method

.method public a(Lcom/squareup/a/m;)Lcom/squareup/a/a/a/al;
    .locals 0

    .prologue
    .line 400
    iput-object p1, p0, Lcom/squareup/a/a/a/al;->c:Lcom/squareup/a/m;

    .line 401
    return-object p0
.end method

.method public a(Lcom/squareup/a/w;)Lcom/squareup/a/a/a/al;
    .locals 3

    .prologue
    .line 440
    sget-object v0, Lcom/squareup/a/a/a/aa;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/a/a/a/al;->b:Lcom/squareup/a/a/a/av;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/av;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/a/al;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/squareup/a/a/a/al;
    .locals 2

    .prologue
    .line 393
    :try_start_0
    new-instance v0, Lcom/squareup/a/a/a/av;

    invoke-direct {v0, p1}, Lcom/squareup/a/a/a/av;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/av;)Lcom/squareup/a/a/a/al;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 394
    :catch_0
    move-exception v0

    .line 395
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/al;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/squareup/a/a/a/al;->d:Lcom/squareup/a/a/a/h;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/a/a/a/h;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    .line 410
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/squareup/a/a/a/al;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/squareup/a/a/a/al;->d:Lcom/squareup/a/a/a/h;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/h;->b(Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    .line 424
    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/al;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/squareup/a/a/a/al;->d:Lcom/squareup/a/a/a/h;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/a/a/a/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    .line 419
    return-object p0
.end method
