.class Lcom/squareup/a/a/c/s;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/squareup/a/a/b/i;

.field private b:I

.field private final c:Lcom/squareup/a/a/b/c;


# direct methods
.method public constructor <init>(Lcom/squareup/a/a/b/c;)V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/squareup/a/a/c/t;

    invoke-direct {v0, p0, p1}, Lcom/squareup/a/a/c/t;-><init>(Lcom/squareup/a/a/c/s;Lcom/squareup/a/a/b/c;)V

    .line 60
    new-instance v1, Lcom/squareup/a/a/c/u;

    invoke-direct {v1, p0}, Lcom/squareup/a/a/c/u;-><init>(Lcom/squareup/a/a/c/s;)V

    .line 72
    new-instance v2, Lcom/squareup/a/a/b/i;

    invoke-direct {v2, v0, v1}, Lcom/squareup/a/a/b/i;-><init>(Lcom/squareup/a/a/b/w;Ljava/util/zip/Inflater;)V

    iput-object v2, p0, Lcom/squareup/a/a/c/s;->a:Lcom/squareup/a/a/b/i;

    .line 73
    iget-object v0, p0, Lcom/squareup/a/a/c/s;->a:Lcom/squareup/a/a/b/i;

    invoke-static {v0}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/w;)Lcom/squareup/a/a/b/c;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/c/s;->c:Lcom/squareup/a/a/b/c;

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/c/s;)I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/squareup/a/a/c/s;->b:I

    return v0
.end method

.method static synthetic a(Lcom/squareup/a/a/c/s;J)I
    .locals 3

    .prologue
    .line 22
    iget v0, p0, Lcom/squareup/a/a/c/s;->b:I

    int-to-long v0, v0

    sub-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, Lcom/squareup/a/a/c/s;->b:I

    return v0
.end method

.method private b()Lcom/squareup/a/a/b/d;
    .locals 4

    .prologue
    .line 96
    iget-object v0, p0, Lcom/squareup/a/a/c/s;->c:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    .line 97
    iget-object v1, p0, Lcom/squareup/a/a/c/s;->c:Lcom/squareup/a/a/b/c;

    int-to-long v2, v0

    invoke-interface {v1, v2, v3}, Lcom/squareup/a/a/b/c;->c(J)Lcom/squareup/a/a/b/d;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 104
    iget v0, p0, Lcom/squareup/a/a/c/s;->b:I

    if-lez v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/squareup/a/a/c/s;->a:Lcom/squareup/a/a/b/i;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/i;->a()Z

    .line 106
    iget v0, p0, Lcom/squareup/a/a/c/s;->b:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "compressedLimit > 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/a/a/c/s;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget v0, p0, Lcom/squareup/a/a/c/s;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/squareup/a/a/c/s;->b:I

    .line 79
    iget-object v0, p0, Lcom/squareup/a/a/c/s;->c:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v1

    .line 80
    if-gez v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "numberOfPairs < 0: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    const/16 v0, 0x400

    if-le v1, v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "numberOfPairs > 1024: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 84
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    .line 85
    invoke-direct {p0}, Lcom/squareup/a/a/c/s;->b()Lcom/squareup/a/a/b/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/a/a/b/d;->d()Lcom/squareup/a/a/b/d;

    move-result-object v3

    .line 86
    invoke-direct {p0}, Lcom/squareup/a/a/c/s;->b()Lcom/squareup/a/a/b/d;

    move-result-object v4

    .line 87
    invoke-virtual {v3}, Lcom/squareup/a/a/b/d;->e()I

    move-result v5

    if-nez v5, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "name.size == 0"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_2
    new-instance v5, Lcom/squareup/a/a/c/e;

    invoke-direct {v5, v3, v4}, Lcom/squareup/a/a/c/e;-><init>(Lcom/squareup/a/a/b/d;Lcom/squareup/a/a/b/d;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_3
    invoke-direct {p0}, Lcom/squareup/a/a/c/s;->c()V

    .line 92
    return-object v2
.end method

.method public a()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/squareup/a/a/c/s;->c:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->close()V

    .line 112
    return-void
.end method
