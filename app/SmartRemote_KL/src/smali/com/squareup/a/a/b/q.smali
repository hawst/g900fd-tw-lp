.class Lcom/squareup/a/a/b/q;
.super Ljava/io/OutputStream;


# instance fields
.field final synthetic a:Lcom/squareup/a/a/b/p;


# direct methods
.method constructor <init>(Lcom/squareup/a/a/b/p;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/squareup/a/a/b/q;->a:Lcom/squareup/a/a/b/p;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/squareup/a/a/b/q;->a:Lcom/squareup/a/a/b/p;

    invoke-static {v0}, Lcom/squareup/a/a/b/p;->a(Lcom/squareup/a/a/b/p;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/squareup/a/a/b/q;->a:Lcom/squareup/a/a/b/p;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/p;->close()V

    .line 119
    return-void
.end method

.method public flush()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/squareup/a/a/b/q;->a:Lcom/squareup/a/a/b/p;

    invoke-static {v0}, Lcom/squareup/a/a/b/p;->a(Lcom/squareup/a/a/b/p;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/squareup/a/a/b/q;->a:Lcom/squareup/a/a/b/p;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/p;->a()V

    .line 115
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/a/a/b/q;->a:Lcom/squareup/a/a/b/p;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".outputStream()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(I)V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/squareup/a/a/b/q;->a()V

    .line 100
    iget-object v0, p0, Lcom/squareup/a/a/b/q;->a:Lcom/squareup/a/a/b/p;

    iget-object v0, v0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/b/j;->c(I)Lcom/squareup/a/a/b/j;

    .line 101
    iget-object v0, p0, Lcom/squareup/a/a/b/q;->a:Lcom/squareup/a/a/b/p;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/p;->c()Lcom/squareup/a/a/b/b;

    .line 102
    return-void
.end method

.method public write([BII)V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/squareup/a/a/b/q;->a()V

    .line 106
    iget-object v0, p0, Lcom/squareup/a/a/b/q;->a:Lcom/squareup/a/a/b/p;

    iget-object v0, v0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/a/a/b/j;->c([BII)Lcom/squareup/a/a/b/j;

    .line 107
    iget-object v0, p0, Lcom/squareup/a/a/b/q;->a:Lcom/squareup/a/a/b/p;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/p;->c()Lcom/squareup/a/a/b/b;

    .line 108
    return-void
.end method
