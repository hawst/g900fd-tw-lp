.class final Lcom/squareup/a/a/c/k;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/w;


# instance fields
.field a:I

.field b:B

.field c:I

.field d:I

.field private final e:Lcom/squareup/a/a/b/c;


# direct methods
.method public constructor <init>(Lcom/squareup/a/a/b/c;)V
    .locals 0

    .prologue
    .line 507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 508
    iput-object p1, p0, Lcom/squareup/a/a/c/k;->e:Lcom/squareup/a/a/b/c;

    .line 509
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 533
    iget v0, p0, Lcom/squareup/a/a/c/k;->c:I

    .line 534
    iget-object v1, p0, Lcom/squareup/a/a/c/k;->e:Lcom/squareup/a/a/b/c;

    invoke-interface {v1}, Lcom/squareup/a/a/b/c;->i()I

    move-result v1

    .line 535
    iget-object v2, p0, Lcom/squareup/a/a/c/k;->e:Lcom/squareup/a/a/b/c;

    invoke-interface {v2}, Lcom/squareup/a/a/b/c;->i()I

    move-result v2

    .line 536
    const/high16 v3, 0x3fff0000    # 1.9921875f

    and-int/2addr v3, v1

    shr-int/lit8 v3, v3, 0x10

    int-to-short v3, v3

    iput v3, p0, Lcom/squareup/a/a/c/k;->d:I

    iput v3, p0, Lcom/squareup/a/a/c/k;->a:I

    .line 537
    const v3, 0xff00

    and-int/2addr v3, v1

    shr-int/lit8 v3, v3, 0x8

    int-to-byte v3, v3

    .line 538
    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    iput-byte v1, p0, Lcom/squareup/a/a/c/k;->b:B

    .line 539
    const v1, 0x7fffffff

    and-int/2addr v1, v2

    iput v1, p0, Lcom/squareup/a/a/c/k;->c:I

    .line 540
    const/16 v1, 0xa

    if-eq v3, v1, :cond_0

    const-string/jumbo v0, "%s != TYPE_CONTINUATION"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 541
    :cond_0
    iget v1, p0, Lcom/squareup/a/a/c/k;->c:I

    if-eq v1, v0, :cond_1

    const-string/jumbo v0, "TYPE_CONTINUATION streamId changed"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 542
    :cond_1
    return-void
.end method


# virtual methods
.method public b(Lcom/squareup/a/a/b/j;J)J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 512
    :goto_0
    iget v2, p0, Lcom/squareup/a/a/c/k;->d:I

    if-nez v2, :cond_2

    .line 513
    iget-byte v2, p0, Lcom/squareup/a/a/c/k;->b:B

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    .line 521
    :cond_0
    :goto_1
    return-wide v0

    .line 514
    :cond_1
    invoke-direct {p0}, Lcom/squareup/a/a/c/k;->a()V

    goto :goto_0

    .line 518
    :cond_2
    iget-object v2, p0, Lcom/squareup/a/a/c/k;->e:Lcom/squareup/a/a/b/c;

    iget v3, p0, Lcom/squareup/a/a/c/k;->d:I

    int-to-long v4, v3

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface {v2, p1, v4, v5}, Lcom/squareup/a/a/b/c;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v2

    .line 519
    cmp-long v4, v2, v0

    if-eqz v4, :cond_0

    .line 520
    iget v0, p0, Lcom/squareup/a/a/c/k;->d:I

    int-to-long v0, v0

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/squareup/a/a/c/k;->d:I

    move-wide v0, v2

    .line 521
    goto :goto_1
.end method

.method public close()V
    .locals 0

    .prologue
    .line 530
    return-void
.end method
