.class public final Lcom/squareup/a/a/b/j;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/b;
.implements Lcom/squareup/a/a/b/c;
.implements Ljava/lang/Cloneable;


# instance fields
.field a:Lcom/squareup/a/a/b/t;

.field b:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method private f(J)[B
    .locals 7

    .prologue
    .line 303
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v2, 0x0

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/a/a/b/x;->a(JJJ)V

    .line 304
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 305
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount > Integer.MAX_VALUE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 308
    :cond_0
    const/4 v0, 0x0

    .line 309
    long-to-int v1, p1

    new-array v1, v1, [B

    .line 311
    :cond_1
    :goto_0
    int-to-long v2, v0

    cmp-long v2, v2, p1

    if-gez v2, :cond_2

    .line 312
    int-to-long v2, v0

    sub-long v2, p1, v2

    iget-object v4, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v4, v4, Lcom/squareup/a/a/b/t;->c:I

    iget-object v5, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v5, v5, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v4, v5

    int-to-long v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    .line 313
    iget-object v3, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v3, v3, Lcom/squareup/a/a/b/t;->a:[B

    iget-object v4, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v4, v4, Lcom/squareup/a/a/b/t;->b:I

    invoke-static {v3, v4, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 315
    add-int/2addr v0, v2

    .line 316
    iget-object v3, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v4, v3, Lcom/squareup/a/a/b/t;->b:I

    add-int/2addr v2, v4

    iput v2, v3, Lcom/squareup/a/a/b/t;->b:I

    .line 318
    iget-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v2, v2, Lcom/squareup/a/a/b/t;->b:I

    iget-object v3, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v3, v3, Lcom/squareup/a/a/b/t;->c:I

    if-ne v2, v3, :cond_1

    .line 319
    iget-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 320
    invoke-virtual {v2}, Lcom/squareup/a/a/b/t;->a()Lcom/squareup/a/a/b/t;

    move-result-object v3

    iput-object v3, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 321
    sget-object v3, Lcom/squareup/a/a/b/u;->a:Lcom/squareup/a/a/b/u;

    invoke-virtual {v3, v2}, Lcom/squareup/a/a/b/u;->a(Lcom/squareup/a/a/b/t;)V

    goto :goto_0

    .line 325
    :cond_2
    iget-wide v2, p0, Lcom/squareup/a/a/b/j;->b:J

    sub-long/2addr v2, p1

    iput-wide v2, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 326
    return-object v1
.end method


# virtual methods
.method public a(B)J
    .locals 4

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lcom/squareup/a/a/b/j;->b(B)J

    move-result-wide v0

    .line 98
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 99
    :cond_0
    return-wide v0
.end method

.method public a(BJ)J
    .locals 12

    .prologue
    .line 577
    iget-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 578
    if-nez v2, :cond_0

    const-wide/16 v0, -0x1

    .line 594
    :goto_0
    return-wide v0

    .line 579
    :cond_0
    const-wide/16 v0, 0x0

    .line 581
    :cond_1
    iget v3, v2, Lcom/squareup/a/a/b/t;->c:I

    iget v4, v2, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v3, v4

    .line 582
    int-to-long v4, v3

    cmp-long v4, p2, v4

    if-lez v4, :cond_2

    .line 583
    int-to-long v4, v3

    sub-long/2addr p2, v4

    .line 591
    :goto_1
    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 592
    iget-object v2, v2, Lcom/squareup/a/a/b/t;->d:Lcom/squareup/a/a/b/t;

    .line 593
    iget-object v3, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    if-ne v2, v3, :cond_1

    .line 594
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 585
    :cond_2
    iget-object v6, v2, Lcom/squareup/a/a/b/t;->a:[B

    .line 586
    iget v4, v2, Lcom/squareup/a/a/b/t;->b:I

    int-to-long v4, v4

    add-long/2addr v4, p2

    iget v7, v2, Lcom/squareup/a/a/b/t;->c:I

    int-to-long v8, v7

    :goto_2
    cmp-long v7, v4, v8

    if-gez v7, :cond_4

    .line 587
    long-to-int v7, v4

    aget-byte v7, v6, v7

    if-ne v7, p1, :cond_3

    add-long/2addr v0, v4

    iget v2, v2, Lcom/squareup/a/a/b/t;->b:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_0

    .line 586
    :cond_3
    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    goto :goto_2

    .line 589
    :cond_4
    const-wide/16 p2, 0x0

    goto :goto_1
.end method

.method public synthetic a(I)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/a/a/b/j;->d(I)Lcom/squareup/a/a/b/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/squareup/a/a/b/d;)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/a/a/b/j;->b(Lcom/squareup/a/a/b/d;)Lcom/squareup/a/a/b/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/String;)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/a/a/b/j;->b(Ljava/lang/String;)Lcom/squareup/a/a/b/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a([B)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/a/a/b/j;->b([B)Lcom/squareup/a/a/b/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a([BII)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/a/a/b/j;->c([BII)Lcom/squareup/a/a/b/j;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x1

    .line 281
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/b/j;->b(B)J

    move-result-wide v0

    .line 283
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 284
    if-eqz p1, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 285
    :cond_0
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/b/j;->e(J)Ljava/lang/String;

    move-result-object v0

    .line 298
    :goto_0
    return-object v0

    .line 285
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 288
    :cond_2
    cmp-long v2, v0, v6

    if-lez v2, :cond_3

    sub-long v2, v0, v4

    invoke-virtual {p0, v2, v3}, Lcom/squareup/a/a/b/j;->d(J)B

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_3

    .line 290
    sub-long/2addr v0, v4

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/b/j;->e(J)Ljava/lang/String;

    move-result-object v0

    .line 291
    const-wide/16 v2, 0x2

    invoke-virtual {p0, v2, v3}, Lcom/squareup/a/a/b/j;->b(J)V

    goto :goto_0

    .line 296
    :cond_3
    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/b/j;->e(J)Ljava/lang/String;

    move-result-object v0

    .line 297
    invoke-virtual {p0, v4, v5}, Lcom/squareup/a/a/b/j;->b(J)V

    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 598
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 93
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 94
    :cond_0
    return-void
.end method

.method public a(Lcom/squareup/a/a/b/j;J)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 512
    if-ne p1, p0, :cond_0

    .line 513
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "source == this"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 515
    :cond_0
    iget-wide v0, p1, Lcom/squareup/a/a/b/j;->b:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/a/a/b/x;->a(JJJ)V

    .line 517
    :goto_0
    cmp-long v0, p2, v2

    if-lez v0, :cond_5

    .line 519
    iget-object v0, p1, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v0, v0, Lcom/squareup/a/a/b/t;->c:I

    iget-object v1, p1, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v1, v1, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    cmp-long v0, p2, v0

    if-gez v0, :cond_2

    .line 520
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v0, v0, Lcom/squareup/a/a/b/t;->e:Lcom/squareup/a/a/b/t;

    .line 521
    :goto_1
    if-eqz v0, :cond_1

    iget v1, v0, Lcom/squareup/a/a/b/t;->c:I

    iget v4, v0, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    add-long/2addr v4, p2

    const-wide/16 v6, 0x800

    cmp-long v1, v4, v6

    if-lez v1, :cond_4

    .line 524
    :cond_1
    iget-object v0, p1, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    long-to-int v1, p2

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/b/t;->a(I)Lcom/squareup/a/a/b/t;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 535
    :cond_2
    iget-object v0, p1, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 536
    iget v1, v0, Lcom/squareup/a/a/b/t;->c:I

    iget v4, v0, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    .line 537
    invoke-virtual {v0}, Lcom/squareup/a/a/b/t;->a()Lcom/squareup/a/a/b/t;

    move-result-object v1

    iput-object v1, p1, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 538
    iget-object v1, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    if-nez v1, :cond_6

    .line 539
    iput-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 540
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v1, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v6, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iput-object v6, v1, Lcom/squareup/a/a/b/t;->e:Lcom/squareup/a/a/b/t;

    iput-object v6, v0, Lcom/squareup/a/a/b/t;->d:Lcom/squareup/a/a/b/t;

    .line 546
    :goto_2
    iget-wide v0, p1, Lcom/squareup/a/a/b/j;->b:J

    sub-long/2addr v0, v4

    iput-wide v0, p1, Lcom/squareup/a/a/b/j;->b:J

    .line 547
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 548
    sub-long/2addr p2, v4

    .line 549
    goto :goto_0

    .line 520
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 527
    :cond_4
    iget-object v1, p1, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    long-to-int v2, p2

    invoke-virtual {v1, v0, v2}, Lcom/squareup/a/a/b/t;->a(Lcom/squareup/a/a/b/t;I)V

    .line 528
    iget-wide v0, p1, Lcom/squareup/a/a/b/j;->b:J

    sub-long/2addr v0, p2

    iput-wide v0, p1, Lcom/squareup/a/a/b/j;->b:J

    .line 529
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 550
    :cond_5
    return-void

    .line 542
    :cond_6
    iget-object v1, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v1, v1, Lcom/squareup/a/a/b/t;->e:Lcom/squareup/a/a/b/t;

    .line 543
    invoke-virtual {v1, v0}, Lcom/squareup/a/a/b/t;->a(Lcom/squareup/a/a/b/t;)Lcom/squareup/a/a/b/t;

    move-result-object v0

    .line 544
    invoke-virtual {v0}, Lcom/squareup/a/a/b/t;->b()V

    goto :goto_2
.end method

.method b([BII)I
    .locals 6

    .prologue
    .line 331
    iget-object v1, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 332
    if-nez v1, :cond_1

    const/4 v0, -0x1

    .line 344
    :cond_0
    :goto_0
    return v0

    .line 333
    :cond_1
    iget v0, v1, Lcom/squareup/a/a/b/t;->c:I

    iget v2, v1, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v0, v2

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 334
    iget-object v2, v1, Lcom/squareup/a/a/b/t;->a:[B

    iget v3, v1, Lcom/squareup/a/a/b/t;->b:I

    invoke-static {v2, v3, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 336
    iget v2, v1, Lcom/squareup/a/a/b/t;->b:I

    add-int/2addr v2, v0

    iput v2, v1, Lcom/squareup/a/a/b/t;->b:I

    .line 337
    iget-wide v2, p0, Lcom/squareup/a/a/b/j;->b:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 339
    iget v2, v1, Lcom/squareup/a/a/b/t;->b:I

    iget v3, v1, Lcom/squareup/a/a/b/t;->c:I

    if-ne v2, v3, :cond_0

    .line 340
    invoke-virtual {v1}, Lcom/squareup/a/a/b/t;->a()Lcom/squareup/a/a/b/t;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 341
    sget-object v2, Lcom/squareup/a/a/b/u;->a:Lcom/squareup/a/a/b/u;

    invoke-virtual {v2, v1}, Lcom/squareup/a/a/b/u;->a(Lcom/squareup/a/a/b/t;)V

    goto :goto_0
.end method

.method public b(B)J
    .locals 2

    .prologue
    .line 569
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/squareup/a/a/b/j;->a(BJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(Lcom/squareup/a/a/b/j;J)J
    .locals 4

    .prologue
    .line 553
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 p2, -0x1

    .line 556
    :goto_0
    return-wide p2

    .line 554
    :cond_0
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    iget-wide p2, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 555
    :cond_1
    invoke-virtual {p1, p0, p2, p3}, Lcom/squareup/a/a/b/j;->a(Lcom/squareup/a/a/b/j;J)V

    goto :goto_0
.end method

.method public synthetic b(I)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/a/a/b/j;->e(I)Lcom/squareup/a/a/b/j;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/squareup/a/a/b/j;
    .locals 0

    .prologue
    .line 59
    return-object p0
.end method

.method public b(Lcom/squareup/a/a/b/d;)Lcom/squareup/a/a/b/j;
    .locals 3

    .prologue
    .line 375
    iget-object v0, p1, Lcom/squareup/a/a/b/d;->b:[B

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/squareup/a/a/b/d;->b:[B

    array-length v2, v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/a/a/b/j;->c([BII)Lcom/squareup/a/a/b/j;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/squareup/a/a/b/j;
    .locals 3

    .prologue
    .line 381
    :try_start_0
    const-string/jumbo v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 382
    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/a/a/b/j;->c([BII)Lcom/squareup/a/a/b/j;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 383
    :catch_0
    move-exception v0

    .line 384
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public b([B)Lcom/squareup/a/a/b/j;
    .locals 2

    .prologue
    .line 389
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/squareup/a/a/b/j;->c([BII)Lcom/squareup/a/a/b/j;

    move-result-object v0

    return-object v0
.end method

.method public b(J)V
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 357
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/a/a/b/x;->a(JJJ)V

    .line 359
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    sub-long/2addr v0, p1

    iput-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 360
    :cond_0
    :goto_0
    cmp-long v0, p1, v2

    if-lez v0, :cond_1

    .line 361
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v0, v0, Lcom/squareup/a/a/b/t;->c:I

    iget-object v1, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v1, v1, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 362
    int-to-long v4, v0

    sub-long/2addr p1, v4

    .line 363
    iget-object v1, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v4, v1, Lcom/squareup/a/a/b/t;->b:I

    add-int/2addr v0, v4

    iput v0, v1, Lcom/squareup/a/a/b/t;->b:I

    .line 365
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v0, v0, Lcom/squareup/a/a/b/t;->b:I

    iget-object v1, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v1, v1, Lcom/squareup/a/a/b/t;->c:I

    if-ne v0, v1, :cond_0

    .line 366
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 367
    invoke-virtual {v0}, Lcom/squareup/a/a/b/t;->a()Lcom/squareup/a/a/b/t;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 368
    sget-object v1, Lcom/squareup/a/a/b/u;->a:Lcom/squareup/a/a/b/u;

    invoke-virtual {v1, v0}, Lcom/squareup/a/a/b/u;->a(Lcom/squareup/a/a/b/t;)V

    goto :goto_0

    .line 371
    :cond_1
    return-void
.end method

.method public synthetic c()Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/squareup/a/a/b/j;->m()Lcom/squareup/a/a/b/j;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/squareup/a/a/b/d;
    .locals 3

    .prologue
    .line 244
    new-instance v0, Lcom/squareup/a/a/b/d;

    invoke-direct {p0, p1, p2}, Lcom/squareup/a/a/b/j;->f(J)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/a/a/b/d;-><init>([B)V

    return-object v0
.end method

.method public c(I)Lcom/squareup/a/a/b/j;
    .locals 4

    .prologue
    .line 410
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/b/j;->f(I)Lcom/squareup/a/a/b/t;

    move-result-object v0

    .line 411
    iget-object v1, v0, Lcom/squareup/a/a/b/t;->a:[B

    iget v2, v0, Lcom/squareup/a/a/b/t;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v0, Lcom/squareup/a/a/b/t;->c:I

    int-to-byte v0, p1

    aput-byte v0, v1, v2

    .line 412
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 413
    return-object p0
.end method

.method public c([BII)Lcom/squareup/a/a/b/j;
    .locals 5

    .prologue
    .line 393
    add-int v0, p2, p3

    .line 394
    :goto_0
    if-ge p2, v0, :cond_0

    .line 395
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/squareup/a/a/b/j;->f(I)Lcom/squareup/a/a/b/t;

    move-result-object v1

    .line 397
    sub-int v2, v0, p2

    iget v3, v1, Lcom/squareup/a/a/b/t;->c:I

    rsub-int v3, v3, 0x800

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 398
    iget-object v3, v1, Lcom/squareup/a/a/b/t;->a:[B

    iget v4, v1, Lcom/squareup/a/a/b/t;->c:I

    invoke-static {p1, p2, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 400
    add-int/2addr p2, v2

    .line 401
    iget v3, v1, Lcom/squareup/a/a/b/t;->c:I

    add-int/2addr v2, v3

    iput v2, v1, Lcom/squareup/a/a/b/t;->c:I

    goto :goto_0

    .line 404
    :cond_0
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 405
    return-object p0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/squareup/a/a/b/j;->p()Lcom/squareup/a/a/b/j;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 0

    .prologue
    .line 601
    return-void
.end method

.method public d(J)B
    .locals 7

    .prologue
    .line 166
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v4, 0x1

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/a/a/b/x;->a(JJJ)V

    .line 167
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 168
    :goto_0
    iget v1, v0, Lcom/squareup/a/a/b/t;->c:I

    iget v2, v0, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v1, v2

    .line 169
    int-to-long v2, v1

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    iget-object v1, v0, Lcom/squareup/a/a/b/t;->a:[B

    iget v0, v0, Lcom/squareup/a/a/b/t;->b:I

    long-to-int v2, p1

    add-int/2addr v0, v2

    aget-byte v0, v1, v0

    return v0

    .line 170
    :cond_0
    int-to-long v2, v1

    sub-long/2addr p1, v2

    .line 167
    iget-object v0, v0, Lcom/squareup/a/a/b/t;->d:Lcom/squareup/a/a/b/t;

    goto :goto_0
.end method

.method public d(I)Lcom/squareup/a/a/b/j;
    .locals 5

    .prologue
    .line 418
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/b/j;->f(I)Lcom/squareup/a/a/b/t;

    move-result-object v0

    .line 419
    iget-object v1, v0, Lcom/squareup/a/a/b/t;->a:[B

    .line 420
    iget v2, v0, Lcom/squareup/a/a/b/t;->c:I

    .line 421
    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v4, p1, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 422
    add-int/lit8 v2, v3, 0x1

    and-int/lit16 v4, p1, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 423
    iput v2, v0, Lcom/squareup/a/a/b/t;->c:I

    .line 424
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v2, 0x2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 425
    return-object p0
.end method

.method public d()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/squareup/a/a/b/k;

    invoke-direct {v0, p0}, Lcom/squareup/a/a/b/k;-><init>(Lcom/squareup/a/a/b/j;)V

    return-object v0
.end method

.method public e(I)Lcom/squareup/a/a/b/j;
    .locals 5

    .prologue
    .line 430
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/b/j;->f(I)Lcom/squareup/a/a/b/t;

    move-result-object v0

    .line 431
    iget-object v1, v0, Lcom/squareup/a/a/b/t;->a:[B

    .line 432
    iget v2, v0, Lcom/squareup/a/a/b/t;->c:I

    .line 433
    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v4, p1, 0x18

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 434
    add-int/lit8 v2, v3, 0x1

    shr-int/lit8 v4, p1, 0x10

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 435
    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v4, p1, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    .line 436
    add-int/lit8 v2, v3, 0x1

    and-int/lit16 v4, p1, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 437
    iput v2, v0, Lcom/squareup/a/a/b/t;->c:I

    .line 438
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v2, 0x4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 439
    return-object p0
.end method

.method public e(J)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 248
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/a/a/b/x;->a(JJJ)V

    .line 249
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount > Integer.MAX_VALUE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :cond_0
    cmp-long v0, p1, v2

    if-nez v0, :cond_2

    const-string/jumbo v0, ""

    .line 274
    :cond_1
    :goto_0
    return-object v0

    .line 254
    :cond_2
    iget-object v1, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 255
    iget v0, v1, Lcom/squareup/a/a/b/t;->b:I

    int-to-long v2, v0

    add-long/2addr v2, p1

    iget v0, v1, Lcom/squareup/a/a/b/t;->c:I

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    .line 258
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/squareup/a/a/b/j;->f(J)[B

    move-result-object v1

    const-string/jumbo v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 259
    :catch_0
    move-exception v0

    .line 260
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 265
    :cond_3
    :try_start_1
    new-instance v0, Ljava/lang/String;

    iget-object v2, v1, Lcom/squareup/a/a/b/t;->a:[B

    iget v3, v1, Lcom/squareup/a/a/b/t;->b:I

    long-to-int v4, p1

    const-string/jumbo v5, "UTF-8"

    invoke-direct {v0, v2, v3, v4, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 266
    iget v2, v1, Lcom/squareup/a/a/b/t;->b:I

    int-to-long v2, v2

    add-long/2addr v2, p1

    long-to-int v2, v2

    iput v2, v1, Lcom/squareup/a/a/b/t;->b:I

    .line 267
    iget-wide v2, p0, Lcom/squareup/a/a/b/j;->b:J

    sub-long/2addr v2, p1

    iput-wide v2, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 269
    iget v2, v1, Lcom/squareup/a/a/b/t;->b:I

    iget v3, v1, Lcom/squareup/a/a/b/t;->c:I

    if-ne v2, v3, :cond_1

    .line 270
    invoke-virtual {v1}, Lcom/squareup/a/a/b/t;->a()Lcom/squareup/a/a/b/t;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 271
    sget-object v2, Lcom/squareup/a/a/b/u;->a:Lcom/squareup/a/a/b/u;

    invoke-virtual {v2, v1}, Lcom/squareup/a/a/b/u;->a(Lcom/squareup/a/a/b/t;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 275
    :catch_1
    move-exception v0

    .line 276
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public e()Z
    .locals 4

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 14

    .prologue
    const-wide/16 v0, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 615
    instance-of v2, p1, Lcom/squareup/a/a/b/j;

    if-nez v2, :cond_0

    move v0, v6

    .line 643
    :goto_0
    return v0

    .line 616
    :cond_0
    check-cast p1, Lcom/squareup/a/a/b/j;

    .line 617
    iget-wide v2, p0, Lcom/squareup/a/a/b/j;->b:J

    iget-wide v4, p1, Lcom/squareup/a/a/b/j;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    move v0, v6

    goto :goto_0

    .line 618
    :cond_1
    iget-wide v2, p0, Lcom/squareup/a/a/b/j;->b:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_2

    move v0, v7

    goto :goto_0

    .line 620
    :cond_2
    iget-object v5, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 621
    iget-object v4, p1, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 622
    iget v3, v5, Lcom/squareup/a/a/b/t;->b:I

    .line 623
    iget v2, v4, Lcom/squareup/a/a/b/t;->b:I

    .line 625
    :goto_1
    iget-wide v8, p0, Lcom/squareup/a/a/b/j;->b:J

    cmp-long v8, v0, v8

    if-gez v8, :cond_7

    .line 626
    iget v8, v5, Lcom/squareup/a/a/b/t;->c:I

    sub-int/2addr v8, v3

    iget v9, v4, Lcom/squareup/a/a/b/t;->c:I

    sub-int/2addr v9, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-long v10, v8

    move v8, v6

    .line 628
    :goto_2
    int-to-long v12, v8

    cmp-long v9, v12, v10

    if-gez v9, :cond_4

    .line 629
    iget-object v12, v5, Lcom/squareup/a/a/b/t;->a:[B

    add-int/lit8 v9, v3, 0x1

    aget-byte v12, v12, v3

    iget-object v13, v4, Lcom/squareup/a/a/b/t;->a:[B

    add-int/lit8 v3, v2, 0x1

    aget-byte v2, v13, v2

    if-eq v12, v2, :cond_3

    move v0, v6

    goto :goto_0

    .line 628
    :cond_3
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v2, v3

    move v3, v9

    goto :goto_2

    .line 632
    :cond_4
    iget v8, v5, Lcom/squareup/a/a/b/t;->c:I

    if-ne v3, v8, :cond_5

    .line 633
    iget-object v5, v5, Lcom/squareup/a/a/b/t;->d:Lcom/squareup/a/a/b/t;

    .line 634
    iget v3, v5, Lcom/squareup/a/a/b/t;->b:I

    .line 637
    :cond_5
    iget v8, v4, Lcom/squareup/a/a/b/t;->c:I

    if-ne v2, v8, :cond_6

    .line 638
    iget-object v4, v4, Lcom/squareup/a/a/b/t;->d:Lcom/squareup/a/a/b/t;

    .line 639
    iget v2, v4, Lcom/squareup/a/a/b/t;->b:I

    .line 625
    :cond_6
    add-long/2addr v0, v10

    goto :goto_1

    :cond_7
    move v0, v7

    .line 643
    goto :goto_0
.end method

.method public f()B
    .locals 10

    .prologue
    .line 144
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "size == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 147
    iget v1, v0, Lcom/squareup/a/a/b/t;->b:I

    .line 148
    iget v2, v0, Lcom/squareup/a/a/b/t;->c:I

    .line 150
    iget-object v3, v0, Lcom/squareup/a/a/b/t;->a:[B

    .line 151
    add-int/lit8 v4, v1, 0x1

    aget-byte v1, v3, v1

    .line 152
    iget-wide v6, p0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 154
    if-ne v4, v2, :cond_1

    .line 155
    invoke-virtual {v0}, Lcom/squareup/a/a/b/t;->a()Lcom/squareup/a/a/b/t;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 156
    sget-object v2, Lcom/squareup/a/a/b/u;->a:Lcom/squareup/a/a/b/u;

    invoke-virtual {v2, v0}, Lcom/squareup/a/a/b/u;->a(Lcom/squareup/a/a/b/t;)V

    .line 161
    :goto_0
    return v1

    .line 158
    :cond_1
    iput v4, v0, Lcom/squareup/a/a/b/t;->b:I

    goto :goto_0
.end method

.method f(I)Lcom/squareup/a/a/b/t;
    .locals 3

    .prologue
    const/16 v2, 0x800

    .line 447
    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    if-le p1, v2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    if-nez v0, :cond_3

    .line 450
    sget-object v0, Lcom/squareup/a/a/b/u;->a:Lcom/squareup/a/a/b/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/u;->a()Lcom/squareup/a/a/b/t;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 451
    iget-object v1, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iput-object v0, v2, Lcom/squareup/a/a/b/t;->e:Lcom/squareup/a/a/b/t;

    iput-object v0, v1, Lcom/squareup/a/a/b/t;->d:Lcom/squareup/a/a/b/t;

    .line 458
    :cond_2
    :goto_0
    return-object v0

    .line 454
    :cond_3
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v0, v0, Lcom/squareup/a/a/b/t;->e:Lcom/squareup/a/a/b/t;

    .line 455
    iget v1, v0, Lcom/squareup/a/a/b/t;->c:I

    add-int/2addr v1, p1

    if-le v1, v2, :cond_2

    .line 456
    sget-object v1, Lcom/squareup/a/a/b/u;->a:Lcom/squareup/a/a/b/u;

    invoke-virtual {v1}, Lcom/squareup/a/a/b/u;->a()Lcom/squareup/a/a/b/t;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/b/t;->a(Lcom/squareup/a/a/b/t;)Lcom/squareup/a/a/b/t;

    move-result-object v0

    goto :goto_0
.end method

.method public g()S
    .locals 10

    .prologue
    const-wide/16 v8, 0x2

    .line 175
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    cmp-long v0, v0, v8

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "size < 2: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/squareup/a/a/b/j;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 178
    iget v1, v0, Lcom/squareup/a/a/b/t;->b:I

    .line 179
    iget v2, v0, Lcom/squareup/a/a/b/t;->c:I

    .line 182
    sub-int v3, v2, v1

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    .line 183
    invoke-virtual {p0}, Lcom/squareup/a/a/b/j;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    invoke-virtual {p0}, Lcom/squareup/a/a/b/j;->f()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 185
    int-to-short v0, v0

    .line 200
    :goto_0
    return v0

    .line 188
    :cond_1
    iget-object v3, v0, Lcom/squareup/a/a/b/t;->a:[B

    .line 189
    add-int/lit8 v4, v1, 0x1

    aget-byte v1, v3, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v5, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v1, v3

    .line 191
    iget-wide v6, p0, Lcom/squareup/a/a/b/j;->b:J

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 193
    if-ne v5, v2, :cond_2

    .line 194
    invoke-virtual {v0}, Lcom/squareup/a/a/b/t;->a()Lcom/squareup/a/a/b/t;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 195
    sget-object v2, Lcom/squareup/a/a/b/u;->a:Lcom/squareup/a/a/b/u;

    invoke-virtual {v2, v0}, Lcom/squareup/a/a/b/u;->a(Lcom/squareup/a/a/b/t;)V

    .line 200
    :goto_1
    int-to-short v0, v1

    goto :goto_0

    .line 197
    :cond_2
    iput v5, v0, Lcom/squareup/a/a/b/t;->b:I

    goto :goto_1
.end method

.method public h()I
    .locals 1

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/squareup/a/a/b/j;->g()S

    move-result v0

    invoke-static {v0}, Lcom/squareup/a/a/b/x;->a(S)I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 647
    iget-object v1, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 648
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 656
    :goto_0
    return v0

    .line 649
    :cond_0
    const/4 v0, 0x1

    .line 651
    :cond_1
    iget v2, v1, Lcom/squareup/a/a/b/t;->b:I

    iget v4, v1, Lcom/squareup/a/a/b/t;->c:I

    :goto_1
    if-ge v2, v4, :cond_2

    .line 652
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, v1, Lcom/squareup/a/a/b/t;->a:[B

    aget-byte v3, v3, v2

    add-int/2addr v3, v0

    .line 651
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    .line 654
    :cond_2
    iget-object v1, v1, Lcom/squareup/a/a/b/t;->d:Lcom/squareup/a/a/b/t;

    .line 655
    iget-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    if-ne v1, v2, :cond_1

    goto :goto_0
.end method

.method public i()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x4

    .line 204
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    cmp-long v0, v0, v8

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "size < 4: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/squareup/a/a/b/j;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    iget-object v1, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 207
    iget v0, v1, Lcom/squareup/a/a/b/t;->b:I

    .line 208
    iget v2, v1, Lcom/squareup/a/a/b/t;->c:I

    .line 211
    sub-int v3, v2, v0

    const/4 v4, 0x4

    if-ge v3, v4, :cond_1

    .line 212
    invoke-virtual {p0}, Lcom/squareup/a/a/b/j;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    invoke-virtual {p0}, Lcom/squareup/a/a/b/j;->f()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/squareup/a/a/b/j;->f()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/squareup/a/a/b/j;->f()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 232
    :goto_0
    return v0

    .line 218
    :cond_1
    iget-object v3, v1, Lcom/squareup/a/a/b/t;->a:[B

    .line 219
    add-int/lit8 v4, v0, 0x1

    aget-byte v0, v3, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    add-int/lit8 v5, v4, 0x1

    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v0, v4

    add-int/lit8 v4, v5, 0x1

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v0, v5

    add-int/lit8 v5, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v0, v3

    .line 223
    iget-wide v6, p0, Lcom/squareup/a/a/b/j;->b:J

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 225
    if-ne v5, v2, :cond_2

    .line 226
    invoke-virtual {v1}, Lcom/squareup/a/a/b/t;->a()Lcom/squareup/a/a/b/t;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 227
    sget-object v2, Lcom/squareup/a/a/b/u;->a:Lcom/squareup/a/a/b/u;

    invoke-virtual {v2, v1}, Lcom/squareup/a/a/b/u;->a(Lcom/squareup/a/a/b/t;)V

    goto :goto_0

    .line 229
    :cond_2
    iput v5, v1, Lcom/squareup/a/a/b/t;->b:I

    goto :goto_0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/squareup/a/a/b/j;->i()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/a/a/b/x;->a(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/squareup/a/a/b/l;

    invoke-direct {v0, p0}, Lcom/squareup/a/a/b/l;-><init>(Lcom/squareup/a/a/b/j;)V

    return-object v0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    return-wide v0
.end method

.method public m()Lcom/squareup/a/a/b/j;
    .locals 0

    .prologue
    .line 85
    return-object p0
.end method

.method public n()J
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 131
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    .line 132
    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    move-wide v0, v2

    .line 140
    :cond_0
    :goto_0
    return-wide v0

    .line 135
    :cond_1
    iget-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v2, v2, Lcom/squareup/a/a/b/t;->e:Lcom/squareup/a/a/b/t;

    .line 136
    iget v3, v2, Lcom/squareup/a/a/b/t;->c:I

    const/16 v4, 0x800

    if-ge v3, v4, :cond_0

    .line 137
    iget v3, v2, Lcom/squareup/a/a/b/t;->c:I

    iget v2, v2, Lcom/squareup/a/a/b/t;->b:I

    sub-int v2, v3, v2

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public o()V
    .locals 2

    .prologue
    .line 352
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    invoke-virtual {p0, v0, v1}, Lcom/squareup/a/a/b/j;->b(J)V

    .line 353
    return-void
.end method

.method public p()Lcom/squareup/a/a/b/j;
    .locals 6

    .prologue
    .line 684
    new-instance v1, Lcom/squareup/a/a/b/j;

    invoke-direct {v1}, Lcom/squareup/a/a/b/j;-><init>()V

    .line 685
    invoke-virtual {p0}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    move-object v0, v1

    .line 692
    :goto_0
    return-object v0

    .line 687
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v0, v0, Lcom/squareup/a/a/b/t;->a:[B

    iget-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v2, v2, Lcom/squareup/a/a/b/t;->b:I

    iget-object v3, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v3, v3, Lcom/squareup/a/a/b/t;->c:I

    iget-object v4, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v4, v4, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/squareup/a/a/b/j;->c([BII)Lcom/squareup/a/a/b/j;

    .line 688
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v0, v0, Lcom/squareup/a/a/b/t;->d:Lcom/squareup/a/a/b/t;

    :goto_1
    iget-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    if-eq v0, v2, :cond_1

    .line 689
    iget-object v2, v0, Lcom/squareup/a/a/b/t;->a:[B

    iget v3, v0, Lcom/squareup/a/a/b/t;->b:I

    iget v4, v0, Lcom/squareup/a/a/b/t;->c:I

    iget v5, v0, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/squareup/a/a/b/j;->c([BII)Lcom/squareup/a/a/b/j;

    .line 688
    iget-object v0, v0, Lcom/squareup/a/a/b/t;->d:Lcom/squareup/a/a/b/t;

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 692
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 660
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 661
    const-string/jumbo v0, "OkBuffer[size=0]"

    .line 675
    :goto_0
    return-object v0

    .line 664
    :cond_0
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v2, 0x10

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 665
    invoke-virtual {p0}, Lcom/squareup/a/a/b/j;->p()Lcom/squareup/a/a/b/j;

    move-result-object v0

    iget-wide v2, p0, Lcom/squareup/a/a/b/j;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/squareup/a/a/b/j;->c(J)Lcom/squareup/a/a/b/d;

    move-result-object v0

    .line 666
    const-string/jumbo v1, "OkBuffer[size=%s data=%s]"

    new-array v2, v4, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/squareup/a/a/b/j;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0}, Lcom/squareup/a/a/b/d;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 670
    :cond_1
    :try_start_0
    const-string/jumbo v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 671
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v0, v0, Lcom/squareup/a/a/b/t;->a:[B

    iget-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v2, v2, Lcom/squareup/a/a/b/t;->b:I

    iget-object v3, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v3, v3, Lcom/squareup/a/a/b/t;->c:I

    iget-object v4, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget v4, v4, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v3, v4

    invoke-virtual {v1, v0, v2, v3}, Ljava/security/MessageDigest;->update([BII)V

    .line 672
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    iget-object v0, v0, Lcom/squareup/a/a/b/t;->d:Lcom/squareup/a/a/b/t;

    :goto_1
    iget-object v2, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    if-eq v0, v2, :cond_2

    .line 673
    iget-object v2, v0, Lcom/squareup/a/a/b/t;->a:[B

    iget v3, v0, Lcom/squareup/a/a/b/t;->b:I

    iget v4, v0, Lcom/squareup/a/a/b/t;->c:I

    iget v5, v0, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4}, Ljava/security/MessageDigest;->update([BII)V

    .line 672
    iget-object v0, v0, Lcom/squareup/a/a/b/t;->d:Lcom/squareup/a/a/b/t;

    goto :goto_1

    .line 675
    :cond_2
    const-string/jumbo v0, "OkBuffer[size=%s md5=%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/squareup/a/a/b/j;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/a/a/b/d;->a([B)Lcom/squareup/a/a/b/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/a/a/b/d;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 677
    :catch_0
    move-exception v0

    .line 678
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method
