.class final Lcom/squareup/a/a/b/p;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/b;


# instance fields
.field public final a:Lcom/squareup/a/a/b/j;

.field public final b:Lcom/squareup/a/a/b/v;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/squareup/a/a/b/v;)V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/squareup/a/a/b/j;

    invoke-direct {v0}, Lcom/squareup/a/a/b/j;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/squareup/a/a/b/p;-><init>(Lcom/squareup/a/a/b/v;Lcom/squareup/a/a/b/j;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/squareup/a/a/b/v;Lcom/squareup/a/a/b/j;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_0
    iput-object p2, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    .line 29
    iput-object p1, p0, Lcom/squareup/a/a/b/p;->b:Lcom/squareup/a/a/b/v;

    .line 30
    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/b/p;)Z
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/squareup/a/a/b/p;->c:Z

    return v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/squareup/a/a/b/p;->c:Z

    if-eqz v0, :cond_0

    .line 177
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/squareup/a/a/b/p;->e()V

    .line 79
    iget-object v0, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/b/j;->d(I)Lcom/squareup/a/a/b/j;

    .line 80
    invoke-virtual {p0}, Lcom/squareup/a/a/b/p;->c()Lcom/squareup/a/a/b/b;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/squareup/a/a/b/d;)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/squareup/a/a/b/p;->e()V

    .line 49
    iget-object v0, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/b/j;->b(Lcom/squareup/a/a/b/d;)Lcom/squareup/a/a/b/j;

    .line 50
    invoke-virtual {p0}, Lcom/squareup/a/a/b/p;->c()Lcom/squareup/a/a/b/b;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/squareup/a/a/b/p;->e()V

    .line 55
    iget-object v0, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/b/j;->b(Ljava/lang/String;)Lcom/squareup/a/a/b/j;

    .line 56
    invoke-virtual {p0}, Lcom/squareup/a/a/b/p;->c()Lcom/squareup/a/a/b/b;

    move-result-object v0

    return-object v0
.end method

.method public a([B)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/squareup/a/a/b/p;->e()V

    .line 61
    iget-object v0, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/b/j;->b([B)Lcom/squareup/a/a/b/j;

    .line 62
    invoke-virtual {p0}, Lcom/squareup/a/a/b/p;->c()Lcom/squareup/a/a/b/b;

    move-result-object v0

    return-object v0
.end method

.method public a([BII)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/squareup/a/a/b/p;->e()V

    .line 67
    iget-object v0, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/a/a/b/j;->c([BII)Lcom/squareup/a/a/b/j;

    .line 68
    invoke-virtual {p0}, Lcom/squareup/a/a/b/p;->c()Lcom/squareup/a/a/b/b;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/squareup/a/a/b/p;->e()V

    .line 136
    iget-object v0, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    iget-wide v0, v0, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/squareup/a/a/b/p;->b:Lcom/squareup/a/a/b/v;

    iget-object v1, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    iget-object v2, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    iget-wide v2, v2, Lcom/squareup/a/a/b/j;->b:J

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/a/a/b/v;->a(Lcom/squareup/a/a/b/j;J)V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/b/p;->b:Lcom/squareup/a/a/b/v;

    invoke-interface {v0}, Lcom/squareup/a/a/b/v;->a()V

    .line 140
    return-void
.end method

.method public a(Lcom/squareup/a/a/b/j;J)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/squareup/a/a/b/p;->e()V

    .line 43
    iget-object v0, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/a/a/b/j;->a(Lcom/squareup/a/a/b/j;J)V

    .line 44
    invoke-virtual {p0}, Lcom/squareup/a/a/b/p;->c()Lcom/squareup/a/a/b/b;

    .line 45
    return-void
.end method

.method public b(I)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/squareup/a/a/b/p;->e()V

    .line 85
    iget-object v0, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/b/j;->e(I)Lcom/squareup/a/a/b/j;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/a/a/b/p;->c()Lcom/squareup/a/a/b/b;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/squareup/a/a/b/j;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    return-object v0
.end method

.method public c()Lcom/squareup/a/a/b/b;
    .locals 4

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/squareup/a/a/b/p;->e()V

    .line 91
    iget-object v0, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->n()J

    move-result-wide v0

    .line 92
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/squareup/a/a/b/p;->b:Lcom/squareup/a/a/b/v;

    iget-object v3, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    invoke-interface {v2, v3, v0, v1}, Lcom/squareup/a/a/b/v;->a(Lcom/squareup/a/a/b/j;J)V

    .line 93
    :cond_0
    return-object p0
.end method

.method public close()V
    .locals 6

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/squareup/a/a/b/p;->c:Z

    if-eqz v0, :cond_1

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    const/4 v0, 0x0

    .line 149
    :try_start_0
    iget-object v1, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    iget-wide v2, v1, Lcom/squareup/a/a/b/j;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 150
    iget-object v1, p0, Lcom/squareup/a/a/b/p;->b:Lcom/squareup/a/a/b/v;

    iget-object v2, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    iget-object v3, p0, Lcom/squareup/a/a/b/p;->a:Lcom/squareup/a/a/b/j;

    iget-wide v4, v3, Lcom/squareup/a/a/b/j;->b:J

    invoke-interface {v1, v2, v4, v5}, Lcom/squareup/a/a/b/v;->a(Lcom/squareup/a/a/b/j;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 157
    :cond_2
    :goto_1
    :try_start_1
    iget-object v1, p0, Lcom/squareup/a/a/b/p;->b:Lcom/squareup/a/a/b/v;

    invoke-interface {v1}, Lcom/squareup/a/a/b/v;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 161
    :cond_3
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/squareup/a/a/b/p;->c:Z

    .line 163
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/a/a/b/x;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 158
    :catch_0
    move-exception v1

    .line 159
    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_2

    .line 152
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public d()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/squareup/a/a/b/q;

    invoke-direct {v0, p0}, Lcom/squareup/a/a/b/q;-><init>(Lcom/squareup/a/a/b/p;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "buffer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/b/p;->b:Lcom/squareup/a/a/b/v;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
