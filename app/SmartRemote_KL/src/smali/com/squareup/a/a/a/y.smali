.class final enum Lcom/squareup/a/a/a/y;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/squareup/a/a/a/y;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/squareup/a/a/a/y;

.field public static final enum b:Lcom/squareup/a/a/a/y;

.field public static final enum c:Lcom/squareup/a/a/a/y;

.field private static final synthetic d:[Lcom/squareup/a/a/a/y;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 399
    new-instance v0, Lcom/squareup/a/a/a/y;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/squareup/a/a/a/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/a/a/a/y;->a:Lcom/squareup/a/a/a/y;

    .line 400
    new-instance v0, Lcom/squareup/a/a/a/y;

    const-string/jumbo v1, "SAME_CONNECTION"

    invoke-direct {v0, v1, v3}, Lcom/squareup/a/a/a/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/a/a/a/y;->b:Lcom/squareup/a/a/a/y;

    .line 401
    new-instance v0, Lcom/squareup/a/a/a/y;

    const-string/jumbo v1, "DIFFERENT_CONNECTION"

    invoke-direct {v0, v1, v4}, Lcom/squareup/a/a/a/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/a/a/a/y;->c:Lcom/squareup/a/a/a/y;

    .line 398
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/a/a/a/y;

    sget-object v1, Lcom/squareup/a/a/a/y;->a:Lcom/squareup/a/a/a/y;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/a/a/a/y;->b:Lcom/squareup/a/a/a/y;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/a/a/a/y;->c:Lcom/squareup/a/a/a/y;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/a/a/a/y;->d:[Lcom/squareup/a/a/a/y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 398
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/a/a/a/y;
    .locals 1

    .prologue
    .line 398
    const-class v0, Lcom/squareup/a/a/a/y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/a/y;

    return-object v0
.end method

.method public static values()[Lcom/squareup/a/a/a/y;
    .locals 1

    .prologue
    .line 398
    sget-object v0, Lcom/squareup/a/a/a/y;->d:[Lcom/squareup/a/a/a/y;

    invoke-virtual {v0}, [Lcom/squareup/a/a/a/y;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/a/a/a/y;

    return-object v0
.end method
