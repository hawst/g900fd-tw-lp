.class final Lcom/squareup/a/a/c/aq;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/v;


# static fields
.field static final synthetic a:Z


# instance fields
.field final synthetic b:Lcom/squareup/a/a/c/ao;

.field private c:Z

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 512
    const-class v0, Lcom/squareup/a/a/c/ao;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/squareup/a/a/c/aq;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/squareup/a/a/c/ao;)V
    .locals 0

    .prologue
    .line 512
    iput-object p1, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/c/aq;)Z
    .locals 1

    .prologue
    .line 512
    iget-boolean v0, p0, Lcom/squareup/a/a/c/aq;->d:Z

    return v0
.end method

.method static synthetic a(Lcom/squareup/a/a/c/aq;Z)Z
    .locals 0

    .prologue
    .line 512
    iput-boolean p1, p0, Lcom/squareup/a/a/c/aq;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/squareup/a/a/c/aq;)Z
    .locals 1

    .prologue
    .line 512
    iget-boolean v0, p0, Lcom/squareup/a/a/c/aq;->c:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 545
    sget-boolean v0, Lcom/squareup/a/a/c/aq;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 546
    :cond_0
    iget-object v1, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    monitor-enter v1

    .line 547
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Lcom/squareup/a/a/c/ao;->f(Lcom/squareup/a/a/c/ao;)V

    .line 548
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/c/ac;->d()V

    .line 550
    return-void

    .line 548
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lcom/squareup/a/a/b/j;J)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 522
    sget-boolean v0, Lcom/squareup/a/a/c/aq;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 534
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Lcom/squareup/a/a/c/ao;->f(Lcom/squareup/a/a/c/ao;)V

    .line 535
    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    iget-wide v2, v0, Lcom/squareup/a/a/c/ao;->b:J

    invoke-static {v2, v3, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 536
    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    iget-wide v2, v0, Lcom/squareup/a/a/c/ao;->b:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/squareup/a/a/c/ao;->b:J

    .line 537
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 539
    sub-long/2addr p2, v4

    .line 540
    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v1}, Lcom/squareup/a/a/c/ao;->b(Lcom/squareup/a/a/c/ao;)I

    move-result v1

    const/4 v2, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/a/a/c/ac;->a(IZLcom/squareup/a/a/b/j;J)V

    .line 523
    :cond_1
    cmp-long v0, p2, v6

    if-lez v0, :cond_2

    .line 525
    iget-object v1, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    monitor-enter v1

    .line 527
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    iget-wide v2, v0, Lcom/squareup/a/a/c/ao;->b:J

    cmp-long v0, v2, v6

    if-gtz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 530
    :catch_0
    move-exception v0

    .line 531
    :try_start_2
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 537
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 542
    :cond_2
    return-void
.end method

.method public close()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 558
    sget-boolean v0, Lcom/squareup/a/a/c/aq;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 559
    :cond_0
    iget-object v1, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    monitor-enter v1

    .line 560
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/aq;->c:Z

    if-eqz v0, :cond_1

    monitor-exit v1

    .line 570
    :goto_0
    return-void

    .line 561
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 562
    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    iget-object v0, v0, Lcom/squareup/a/a/c/ao;->c:Lcom/squareup/a/a/c/aq;

    iget-boolean v0, v0, Lcom/squareup/a/a/c/aq;->d:Z

    if-nez v0, :cond_2

    .line 563
    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v1}, Lcom/squareup/a/a/c/ao;->b(Lcom/squareup/a/a/c/ao;)I

    move-result v1

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/a/a/c/ac;->a(IZLcom/squareup/a/a/b/j;J)V

    .line 565
    :cond_2
    iget-object v1, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    monitor-enter v1

    .line 566
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/squareup/a/a/c/aq;->c:Z

    .line 567
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 568
    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Lcom/squareup/a/a/c/ao;->a(Lcom/squareup/a/a/c/ao;)Lcom/squareup/a/a/c/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/c/ac;->d()V

    .line 569
    iget-object v0, p0, Lcom/squareup/a/a/c/aq;->b:Lcom/squareup/a/a/c/ao;

    invoke-static {v0}, Lcom/squareup/a/a/c/ao;->e(Lcom/squareup/a/a/c/ao;)V

    goto :goto_0

    .line 561
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 567
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
