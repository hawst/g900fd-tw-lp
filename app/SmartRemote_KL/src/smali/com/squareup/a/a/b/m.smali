.class public final Lcom/squareup/a/a/b/m;
.super Ljava/lang/Object;


# direct methods
.method public static a(Lcom/squareup/a/a/b/v;)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/squareup/a/a/b/p;

    invoke-direct {v0, p0}, Lcom/squareup/a/a/b/p;-><init>(Lcom/squareup/a/a/b/v;)V

    return-object v0
.end method

.method public static a(Lcom/squareup/a/a/b/w;)Lcom/squareup/a/a/b/c;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/squareup/a/a/b/r;

    invoke-direct {v0, p0}, Lcom/squareup/a/a/b/r;-><init>(Lcom/squareup/a/a/b/w;)V

    return-object v0
.end method

.method public static a(Ljava/io/OutputStream;)Lcom/squareup/a/a/b/v;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/squareup/a/a/b/n;

    invoke-direct {v0, p0}, Lcom/squareup/a/a/b/n;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;)Lcom/squareup/a/a/b/w;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/squareup/a/a/b/o;

    invoke-direct {v0, p0}, Lcom/squareup/a/a/b/o;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method public static a(Lcom/squareup/a/a/b/j;JJLjava/io/OutputStream;)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    .line 39
    iget-wide v0, p0, Lcom/squareup/a/a/b/j;->b:J

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/a/a/b/x;->a(JJJ)V

    .line 42
    iget-object v0, p0, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 43
    :goto_0
    iget v1, v0, Lcom/squareup/a/a/b/t;->c:I

    iget v2, v0, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v1, v2

    int-to-long v2, v1

    cmp-long v1, p1, v2

    if-ltz v1, :cond_0

    .line 44
    iget v1, v0, Lcom/squareup/a/a/b/t;->c:I

    iget v2, v0, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v1, v2

    int-to-long v2, v1

    sub-long/2addr p1, v2

    .line 45
    iget-object v0, v0, Lcom/squareup/a/a/b/t;->d:Lcom/squareup/a/a/b/t;

    goto :goto_0

    .line 49
    :cond_0
    :goto_1
    cmp-long v1, p3, v6

    if-lez v1, :cond_1

    .line 50
    iget v1, v0, Lcom/squareup/a/a/b/t;->b:I

    int-to-long v2, v1

    add-long/2addr v2, p1

    long-to-int v1, v2

    .line 51
    iget v2, v0, Lcom/squareup/a/a/b/t;->c:I

    sub-int/2addr v2, v1

    int-to-long v2, v2

    invoke-static {v2, v3, p3, p4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    .line 52
    iget-object v3, v0, Lcom/squareup/a/a/b/t;->a:[B

    invoke-virtual {p5, v3, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 53
    int-to-long v2, v2

    sub-long/2addr p3, v2

    move-wide p1, v6

    .line 55
    goto :goto_1

    .line 56
    :cond_1
    return-void
.end method
