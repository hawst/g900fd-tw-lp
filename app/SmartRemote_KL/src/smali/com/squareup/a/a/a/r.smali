.class Lcom/squareup/a/a/a/r;
.super Lcom/squareup/a/a/a/m;

# interfaces
.implements Lcom/squareup/a/a/b/w;


# instance fields
.field final synthetic d:Lcom/squareup/a/a/a/k;

.field private e:Z


# direct methods
.method constructor <init>(Lcom/squareup/a/a/a/k;Ljava/net/CacheRequest;)V
    .locals 0

    .prologue
    .line 522
    iput-object p1, p0, Lcom/squareup/a/a/a/r;->d:Lcom/squareup/a/a/a/k;

    .line 523
    invoke-direct {p0, p1, p2}, Lcom/squareup/a/a/a/m;-><init>(Lcom/squareup/a/a/a/k;Ljava/net/CacheRequest;)V

    .line 524
    return-void
.end method


# virtual methods
.method public b(Lcom/squareup/a/a/b/j;J)J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 528
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 529
    :cond_0
    iget-boolean v2, p0, Lcom/squareup/a/a/a/r;->b:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 530
    :cond_1
    iget-boolean v2, p0, Lcom/squareup/a/a/a/r;->e:Z

    if-eqz v2, :cond_2

    .line 539
    :goto_0
    return-wide v0

    .line 532
    :cond_2
    iget-object v2, p0, Lcom/squareup/a/a/a/r;->d:Lcom/squareup/a/a/a/k;

    invoke-static {v2}, Lcom/squareup/a/a/a/k;->f(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/c;

    move-result-object v2

    invoke-interface {v2, p1, p2, p3}, Lcom/squareup/a/a/b/c;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v2

    .line 533
    cmp-long v4, v2, v0

    if-nez v4, :cond_3

    .line 534
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/squareup/a/a/a/r;->e:Z

    .line 535
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/squareup/a/a/a/r;->a(Z)V

    goto :goto_0

    .line 538
    :cond_3
    invoke-virtual {p0, p1, v2, v3}, Lcom/squareup/a/a/a/r;->a(Lcom/squareup/a/a/b/j;J)V

    move-wide v0, v2

    .line 539
    goto :goto_0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 548
    iget-boolean v0, p0, Lcom/squareup/a/a/a/r;->b:Z

    if-eqz v0, :cond_0

    .line 554
    :goto_0
    return-void

    .line 550
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/a/a/a/r;->e:Z

    if-nez v0, :cond_1

    .line 551
    invoke-virtual {p0}, Lcom/squareup/a/a/a/r;->a()V

    .line 553
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/a/a/a/r;->b:Z

    goto :goto_0
.end method
