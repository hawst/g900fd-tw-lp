.class final Lcom/squareup/a/a/c/ab;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/c/d;


# instance fields
.field private final a:Lcom/squareup/a/a/b/b;

.field private final b:Lcom/squareup/a/a/b/j;

.field private final c:Lcom/squareup/a/a/b/b;

.field private final d:Z

.field private e:Z


# direct methods
.method constructor <init>(Lcom/squareup/a/a/b/b;Z)V
    .locals 3

    .prologue
    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 301
    iput-object p1, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    .line 302
    iput-boolean p2, p0, Lcom/squareup/a/a/c/ab;->d:Z

    .line 304
    new-instance v0, Ljava/util/zip/Deflater;

    invoke-direct {v0}, Ljava/util/zip/Deflater;-><init>()V

    .line 305
    sget-object v1, Lcom/squareup/a/a/c/z;->a:[B

    invoke-virtual {v0, v1}, Ljava/util/zip/Deflater;->setDictionary([B)V

    .line 306
    new-instance v1, Lcom/squareup/a/a/b/j;

    invoke-direct {v1}, Lcom/squareup/a/a/b/j;-><init>()V

    iput-object v1, p0, Lcom/squareup/a/a/c/ab;->b:Lcom/squareup/a/a/b/j;

    .line 307
    new-instance v1, Lcom/squareup/a/a/b/g;

    iget-object v2, p0, Lcom/squareup/a/a/c/ab;->b:Lcom/squareup/a/a/b/j;

    invoke-direct {v1, v2, v0}, Lcom/squareup/a/a/b/g;-><init>(Lcom/squareup/a/a/b/v;Ljava/util/zip/Deflater;)V

    invoke-static {v1}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/v;)Lcom/squareup/a/a/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/c/ab;->c:Lcom/squareup/a/a/b/b;

    .line 308
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 416
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->b:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 417
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->c:Lcom/squareup/a/a/b/b;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 418
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 419
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/e;

    iget-object v0, v0, Lcom/squareup/a/a/c/e;->h:Lcom/squareup/a/a/b/d;

    .line 420
    iget-object v3, p0, Lcom/squareup/a/a/c/ab;->c:Lcom/squareup/a/a/b/b;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/d;->e()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 421
    iget-object v3, p0, Lcom/squareup/a/a/c/ab;->c:Lcom/squareup/a/a/b/b;

    invoke-interface {v3, v0}, Lcom/squareup/a/a/b/b;->a(Lcom/squareup/a/a/b/d;)Lcom/squareup/a/a/b/b;

    .line 422
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/e;

    iget-object v0, v0, Lcom/squareup/a/a/c/e;->i:Lcom/squareup/a/a/b/d;

    .line 423
    iget-object v3, p0, Lcom/squareup/a/a/c/ab;->c:Lcom/squareup/a/a/b/b;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/d;->e()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 424
    iget-object v3, p0, Lcom/squareup/a/a/c/ab;->c:Lcom/squareup/a/a/b/b;

    invoke-interface {v3, v0}, Lcom/squareup/a/a/b/b;->a(Lcom/squareup/a/a/b/d;)Lcom/squareup/a/a/b/b;

    .line 418
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 426
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->c:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V

    .line 427
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 0

    .prologue
    .line 322
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method a(IILcom/squareup/a/a/b/j;I)V
    .locals 4

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ab;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 405
    :cond_0
    int-to-long v0, p4

    const-wide/32 v2, 0xffffff

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 406
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "FRAME_TOO_LARGE max size is 16Mib: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 408
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    const v1, 0x7fffffff

    and-int/2addr v1, p1

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 409
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    and-int/lit16 v1, p2, 0xff

    shl-int/lit8 v1, v1, 0x18

    const v2, 0xffffff

    and-int/2addr v2, p4

    or-int/2addr v1, v2

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 410
    if-lez p4, :cond_2

    .line 411
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    int-to-long v2, p4

    invoke-interface {v0, p3, v2, v3}, Lcom/squareup/a/a/b/b;->a(Lcom/squareup/a/a/b/j;J)V

    .line 413
    :cond_2
    return-void
.end method

.method public a(IILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 318
    return-void
.end method

.method public declared-synchronized a(IJ)V
    .locals 4

    .prologue
    .line 479
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ab;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 480
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_1

    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p2, v0

    if-lez v0, :cond_2

    .line 481
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "windowSizeIncrement must be between 1 and 0x7fffffff: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 487
    :cond_2
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    const v1, -0x7ffcfff7

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 488
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 489
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0, p1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 490
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    long-to-int v1, p2

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 491
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 492
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ILcom/squareup/a/a/c/a;)V
    .locals 2

    .prologue
    .line 379
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ab;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 380
    :cond_0
    :try_start_1
    iget v0, p2, Lcom/squareup/a/a/c/a;->p:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 384
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    const v1, -0x7ffcfffd

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 385
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 386
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    const v1, 0x7fffffff

    and-int/2addr v1, p1

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 387
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    iget v1, p2, Lcom/squareup/a/a/c/a;->p:I

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 388
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ILcom/squareup/a/a/c/a;[B)V
    .locals 2

    .prologue
    .line 463
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ab;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 464
    :cond_0
    :try_start_1
    iget v0, p2, Lcom/squareup/a/a/c/a;->q:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 465
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "errorCode.spdyGoAwayCode == -1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 470
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    const v1, -0x7ffcfff9

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 471
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 472
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0, p1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 473
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    iget v1, p2, Lcom/squareup/a/a/c/a;->q:I

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 474
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 475
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lcom/squareup/a/a/c/y;)V
    .locals 5

    .prologue
    const v4, 0xffffff

    .line 430
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ab;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 433
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/squareup/a/a/c/y;->b()I

    move-result v0

    .line 434
    mul-int/lit8 v1, v0, 0x8

    add-int/lit8 v1, v1, 0x4

    .line 435
    iget-object v2, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    const v3, -0x7ffcfffc

    invoke-interface {v2, v3}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 436
    iget-object v2, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    and-int/2addr v1, v4

    or-int/lit8 v1, v1, 0x0

    invoke-interface {v2, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 437
    iget-object v1, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v1, v0}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 438
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-gt v0, v1, :cond_2

    .line 439
    invoke-virtual {p1, v0}, Lcom/squareup/a/a/c/y;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 438
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 440
    :cond_1
    invoke-virtual {p1, v0}, Lcom/squareup/a/a/c/y;->c(I)I

    move-result v1

    .line 441
    iget-object v2, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    and-int v3, v0, v4

    or-int/2addr v1, v3

    invoke-interface {v2, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 442
    iget-object v1, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    invoke-virtual {p1, v0}, Lcom/squareup/a/a/c/y;->b(I)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    goto :goto_1

    .line 444
    :cond_2
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ZII)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 449
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/squareup/a/a/c/ab;->e:Z

    if-eqz v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 450
    :cond_0
    :try_start_1
    iget-boolean v3, p0, Lcom/squareup/a/a/c/ab;->d:Z

    and-int/lit8 v2, p2, 0x1

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_0
    if-eq v3, v2, :cond_2

    .line 451
    :goto_1
    if-eq p1, v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "payload != reply"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v2, v1

    .line 450
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 455
    :cond_3
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    const v1, -0x7ffcfffa

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 456
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 457
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0, p2}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 458
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ZILcom/squareup/a/a/b/j;I)V
    .locals 1

    .prologue
    .line 398
    monitor-enter p0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 399
    :goto_0
    :try_start_0
    invoke-virtual {p0, p2, v0, p3, p4}, Lcom/squareup/a/a/c/ab;->a(IILcom/squareup/a/a/b/j;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    monitor-exit p0

    return-void

    .line 398
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ZZIIIILjava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZIIII",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    const v6, 0x7fffffff

    const/4 v0, 0x0

    .line 332
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/squareup/a/a/c/ab;->e:Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 333
    :cond_0
    :try_start_1
    invoke-direct {p0, p7}, Lcom/squareup/a/a/c/ab;->a(Ljava/util/List;)V

    .line 334
    const-wide/16 v2, 0xa

    iget-object v1, p0, Lcom/squareup/a/a/c/ab;->b:Lcom/squareup/a/a/b/j;

    invoke-virtual {v1}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v4

    add-long/2addr v2, v4

    long-to-int v2, v2

    .line 336
    if-eqz p1, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-eqz p2, :cond_1

    const/4 v0, 0x2

    :cond_1
    or-int/2addr v0, v1

    .line 339
    iget-object v1, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    const v3, -0x7ffcffff

    invoke-interface {v1, v3}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 340
    iget-object v1, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    const v3, 0xffffff

    and-int/2addr v2, v3

    or-int/2addr v0, v2

    invoke-interface {v1, v0}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 341
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    and-int v1, p3, v6

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 342
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    and-int v1, p4, v6

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->b(I)Lcom/squareup/a/a/b/b;

    .line 343
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    and-int/lit8 v1, p5, 0x7

    shl-int/lit8 v1, v1, 0xd

    or-int/lit8 v1, v1, 0x0

    and-int/lit16 v2, p6, 0xff

    or-int/2addr v1, v2

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->a(I)Lcom/squareup/a/a/b/b;

    .line 344
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    iget-object v1, p0, Lcom/squareup/a/a/c/ab;->b:Lcom/squareup/a/a/b/j;

    iget-object v2, p0, Lcom/squareup/a/a/c/ab;->b:Lcom/squareup/a/a/b/j;

    invoke-virtual {v2}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/a/a/b/b;->a(Lcom/squareup/a/a/b/j;J)V

    .line 345
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346
    monitor-exit p0

    return-void

    :cond_2
    move v1, v0

    .line 336
    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 312
    return-void
.end method

.method public declared-synchronized c()V
    .locals 2

    .prologue
    .line 325
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/a/a/c/ab;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 326
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized close()V
    .locals 2

    .prologue
    .line 495
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/squareup/a/a/c/ab;->e:Z

    .line 496
    iget-object v0, p0, Lcom/squareup/a/a/c/ab;->a:Lcom/squareup/a/a/b/b;

    iget-object v1, p0, Lcom/squareup/a/a/c/ab;->c:Lcom/squareup/a/a/b/b;

    invoke-static {v0, v1}, Lcom/squareup/a/a/t;->a(Ljava/io/Closeable;Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 497
    monitor-exit p0

    return-void

    .line 495
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
