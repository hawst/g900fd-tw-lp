.class Lcom/squareup/a/a/c/ag;
.super Lcom/squareup/a/a/n;


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lcom/squareup/a/a/c/ac;


# direct methods
.method varargs constructor <init>(Lcom/squareup/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 772
    iput-object p1, p0, Lcom/squareup/a/a/c/ag;->c:Lcom/squareup/a/a/c/ac;

    iput p4, p0, Lcom/squareup/a/a/c/ag;->a:I

    iput-object p5, p0, Lcom/squareup/a/a/c/ag;->b:Ljava/util/List;

    invoke-direct {p0, p2, p3}, Lcom/squareup/a/a/n;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 774
    iget-object v0, p0, Lcom/squareup/a/a/c/ag;->c:Lcom/squareup/a/a/c/ac;

    invoke-static {v0}, Lcom/squareup/a/a/c/ac;->h(Lcom/squareup/a/a/c/ac;)Lcom/squareup/a/a/c/w;

    move-result-object v0

    iget v1, p0, Lcom/squareup/a/a/c/ag;->a:I

    iget-object v2, p0, Lcom/squareup/a/a/c/ag;->b:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Lcom/squareup/a/a/c/w;->a(ILjava/util/List;)Z

    move-result v0

    .line 776
    if-eqz v0, :cond_0

    .line 777
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/ag;->c:Lcom/squareup/a/a/c/ac;

    iget-object v0, v0, Lcom/squareup/a/a/c/ac;->h:Lcom/squareup/a/a/c/d;

    iget v1, p0, Lcom/squareup/a/a/c/ag;->a:I

    sget-object v2, Lcom/squareup/a/a/c/a;->l:Lcom/squareup/a/a/c/a;

    invoke-interface {v0, v1, v2}, Lcom/squareup/a/a/c/d;->a(ILcom/squareup/a/a/c/a;)V

    .line 778
    iget-object v1, p0, Lcom/squareup/a/a/c/ag;->c:Lcom/squareup/a/a/c/ac;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 779
    :try_start_1
    iget-object v0, p0, Lcom/squareup/a/a/c/ag;->c:Lcom/squareup/a/a/c/ac;

    invoke-static {v0}, Lcom/squareup/a/a/c/ac;->i(Lcom/squareup/a/a/c/ac;)Ljava/util/Set;

    move-result-object v0

    iget v2, p0, Lcom/squareup/a/a/c/ag;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 780
    monitor-exit v1

    .line 784
    :cond_0
    :goto_0
    return-void

    .line 780
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 782
    :catch_0
    move-exception v0

    goto :goto_0
.end method
