.class public final Lcom/squareup/a/a/a/w;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/a/aw;


# instance fields
.field private final a:Lcom/squareup/a/a/a/u;

.field private final b:Lcom/squareup/a/a/a/k;


# direct methods
.method public constructor <init>(Lcom/squareup/a/a/a/u;Lcom/squareup/a/a/a/k;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/a/a/a/w;->a:Lcom/squareup/a/a/a/u;

    .line 30
    iput-object p2, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    .line 31
    return-void
.end method


# virtual methods
.method public a(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/b/v;
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 34
    invoke-static {p1}, Lcom/squareup/a/a/a/aa;->a(Lcom/squareup/a/a/a/ac;)J

    move-result-wide v2

    .line 36
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->a:Lcom/squareup/a/a/a/u;

    iget-boolean v0, v0, Lcom/squareup/a/a/a/u;->c:Z

    if-eqz v0, :cond_2

    .line 37
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 38
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Use setFixedLengthStreamingMode() or setChunkedStreamingMode() for requests larger than 2 GiB."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/a/a/a/w;->b(Lcom/squareup/a/a/a/ac;)V

    .line 45
    new-instance v0, Lcom/squareup/a/a/a/ar;

    long-to-int v1, v2

    invoke-direct {v0, v1}, Lcom/squareup/a/a/a/ar;-><init>(I)V

    .line 63
    :goto_0
    return-object v0

    .line 50
    :cond_1
    new-instance v0, Lcom/squareup/a/a/a/ar;

    invoke-direct {v0}, Lcom/squareup/a/a/a/ar;-><init>()V

    goto :goto_0

    .line 54
    :cond_2
    const-string/jumbo v0, "chunked"

    const-string/jumbo v1, "Transfer-Encoding"

    invoke-virtual {p1, v1}, Lcom/squareup/a/a/a/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 56
    invoke-virtual {p0, p1}, Lcom/squareup/a/a/a/w;->b(Lcom/squareup/a/a/a/ac;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/k;->f()Lcom/squareup/a/a/b/v;

    move-result-object v0

    goto :goto_0

    .line 60
    :cond_3
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    .line 62
    invoke-virtual {p0, p1}, Lcom/squareup/a/a/a/w;->b(Lcom/squareup/a/a/a/ac;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/a/a/a/k;->a(J)Lcom/squareup/a/a/b/v;

    move-result-object v0

    goto :goto_0

    .line 66
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot stream a request body without chunked encoding or a known content length!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/net/CacheRequest;)Lcom/squareup/a/a/b/w;
    .locals 4

    .prologue
    .line 133
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->a:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, p1, v2, v3}, Lcom/squareup/a/a/a/k;->a(Ljava/net/CacheRequest;J)Lcom/squareup/a/a/b/w;

    move-result-object v0

    .line 149
    :goto_0
    return-object v0

    .line 137
    :cond_0
    const-string/jumbo v0, "chunked"

    iget-object v1, p0, Lcom/squareup/a/a/a/w;->a:Lcom/squareup/a/a/a/u;

    invoke-virtual {v1}, Lcom/squareup/a/a/a/u;->h()Lcom/squareup/a/a/a/ai;

    move-result-object v1

    const-string/jumbo v2, "Transfer-Encoding"

    invoke-virtual {v1, v2}, Lcom/squareup/a/a/a/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    iget-object v1, p0, Lcom/squareup/a/a/a/w;->a:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/a/a/a/k;->a(Ljava/net/CacheRequest;Lcom/squareup/a/a/a/u;)Lcom/squareup/a/a/b/w;

    move-result-object v0

    goto :goto_0

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->a:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->h()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/a/aa;->a(Lcom/squareup/a/a/a/ai;)J

    move-result-wide v0

    .line 142
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 143
    iget-object v2, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {v2, p1, v0, v1}, Lcom/squareup/a/a/a/k;->a(Ljava/net/CacheRequest;J)Lcom/squareup/a/a/b/w;

    move-result-object v0

    goto :goto_0

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/k;->a(Ljava/net/CacheRequest;)Lcom/squareup/a/a/b/w;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/k;->d()V

    .line 72
    return-void
.end method

.method public a(Lcom/squareup/a/a/a/ar;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/ar;)V

    .line 76
    return-void
.end method

.method public a(Lcom/squareup/a/a/a/u;)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/k;->a(Ljava/lang/Object;)V

    .line 154
    return-void
.end method

.method public b()Lcom/squareup/a/a/a/al;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/k;->e()Lcom/squareup/a/a/a/al;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/squareup/a/a/a/ac;)V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->a:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->b()V

    .line 92
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->a:Lcom/squareup/a/a/a/u;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/u;->k()Lcom/squareup/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/j;->d()Lcom/squareup/a/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/x;->b()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/a/w;->a:Lcom/squareup/a/a/a/u;

    invoke-virtual {v1}, Lcom/squareup/a/a/a/u;->k()Lcom/squareup/a/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/a/j;->m()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/squareup/a/a/a/ah;->a(Lcom/squareup/a/a/a/ac;Ljava/net/Proxy$Type;I)Ljava/lang/String;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {p1}, Lcom/squareup/a/a/a/ac;->h()Lcom/squareup/a/a/a/f;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/f;Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/squareup/a/a/a/w;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/k;->a()V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/k;->b()V

    goto :goto_0
.end method

.method public d()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 112
    const-string/jumbo v1, "close"

    iget-object v2, p0, Lcom/squareup/a/a/a/w;->a:Lcom/squareup/a/a/a/u;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/u;->g()Lcom/squareup/a/a/a/ac;

    move-result-object v2

    const-string/jumbo v3, "Connection"

    invoke-virtual {v2, v3}, Lcom/squareup/a/a/a/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 125
    :cond_0
    :goto_0
    return v0

    .line 117
    :cond_1
    const-string/jumbo v1, "close"

    iget-object v2, p0, Lcom/squareup/a/a/a/w;->a:Lcom/squareup/a/a/a/u;

    invoke-virtual {v2}, Lcom/squareup/a/a/a/u;->h()Lcom/squareup/a/a/a/ai;

    move-result-object v2

    const-string/jumbo v3, "Connection"

    invoke-virtual {v2, v3}, Lcom/squareup/a/a/a/ai;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {v1}, Lcom/squareup/a/a/a/k;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 125
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/squareup/a/a/a/w;->b:Lcom/squareup/a/a/a/k;

    invoke-virtual {v0}, Lcom/squareup/a/a/a/k;->g()V

    .line 130
    return-void
.end method
