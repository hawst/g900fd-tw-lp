.class final enum Lcom/squareup/a/a/c/o;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/squareup/a/a/c/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/squareup/a/a/c/o;

.field public static final enum b:Lcom/squareup/a/a/c/o;

.field private static final synthetic f:[Lcom/squareup/a/a/c/o;


# instance fields
.field private final c:Lcom/squareup/a/a/c/p;

.field private final d:[I

.field private final e:[B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 34
    new-instance v0, Lcom/squareup/a/a/c/o;

    const-string/jumbo v1, "REQUEST"

    invoke-static {}, Lcom/squareup/a/a/c/n;->a()[I

    move-result-object v2

    invoke-static {}, Lcom/squareup/a/a/c/n;->b()[B

    move-result-object v3

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/squareup/a/a/c/o;-><init>(Ljava/lang/String;I[I[B)V

    sput-object v0, Lcom/squareup/a/a/c/o;->a:Lcom/squareup/a/a/c/o;

    .line 35
    new-instance v0, Lcom/squareup/a/a/c/o;

    const-string/jumbo v1, "RESPONSE"

    invoke-static {}, Lcom/squareup/a/a/c/n;->c()[I

    move-result-object v2

    invoke-static {}, Lcom/squareup/a/a/c/n;->d()[B

    move-result-object v3

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/squareup/a/a/c/o;-><init>(Ljava/lang/String;I[I[B)V

    sput-object v0, Lcom/squareup/a/a/c/o;->b:Lcom/squareup/a/a/c/o;

    .line 33
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/a/a/c/o;

    sget-object v1, Lcom/squareup/a/a/c/o;->a:Lcom/squareup/a/a/c/o;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/a/a/c/o;->b:Lcom/squareup/a/a/c/o;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/a/a/c/o;->f:[Lcom/squareup/a/a/c/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I[I[B)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I[B)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    new-instance v0, Lcom/squareup/a/a/c/p;

    invoke-direct {v0}, Lcom/squareup/a/a/c/p;-><init>()V

    iput-object v0, p0, Lcom/squareup/a/a/c/o;->c:Lcom/squareup/a/a/c/p;

    .line 46
    invoke-direct {p0, p3, p4}, Lcom/squareup/a/a/c/o;->a([I[B)V

    .line 47
    iput-object p3, p0, Lcom/squareup/a/a/c/o;->d:[I

    .line 48
    iput-object p4, p0, Lcom/squareup/a/a/c/o;->e:[B

    .line 49
    return-void
.end method

.method private a(IIB)V
    .locals 6

    .prologue
    .line 138
    new-instance v3, Lcom/squareup/a/a/c/p;

    invoke-direct {v3, p1, p3}, Lcom/squareup/a/a/c/p;-><init>(II)V

    .line 140
    iget-object v0, p0, Lcom/squareup/a/a/c/o;->c:Lcom/squareup/a/a/c/p;

    move-object v2, v0

    .line 141
    :goto_0
    const/16 v0, 0x8

    if-le p3, v0, :cond_2

    .line 142
    add-int/lit8 v0, p3, -0x8

    int-to-byte p3, v0

    .line 143
    ushr-int v0, p2, p3

    and-int/lit16 v0, v0, 0xff

    .line 144
    invoke-static {v2}, Lcom/squareup/a/a/c/p;->a(Lcom/squareup/a/a/c/p;)[Lcom/squareup/a/a/c/p;

    move-result-object v1

    if-nez v1, :cond_0

    .line 145
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "invalid dictionary: prefix not unique"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_0
    invoke-static {v2}, Lcom/squareup/a/a/c/p;->a(Lcom/squareup/a/a/c/p;)[Lcom/squareup/a/a/c/p;

    move-result-object v1

    aget-object v1, v1, v0

    if-nez v1, :cond_1

    .line 148
    invoke-static {v2}, Lcom/squareup/a/a/c/p;->a(Lcom/squareup/a/a/c/p;)[Lcom/squareup/a/a/c/p;

    move-result-object v1

    new-instance v4, Lcom/squareup/a/a/c/p;

    invoke-direct {v4}, Lcom/squareup/a/a/c/p;-><init>()V

    aput-object v4, v1, v0

    .line 150
    :cond_1
    invoke-static {v2}, Lcom/squareup/a/a/c/p;->a(Lcom/squareup/a/a/c/p;)[Lcom/squareup/a/a/c/p;

    move-result-object v1

    aget-object v0, v1, v0

    move-object v2, v0

    .line 151
    goto :goto_0

    .line 153
    :cond_2
    rsub-int/lit8 v0, p3, 0x8

    .line 154
    shl-int v1, p2, v0

    and-int/lit16 v1, v1, 0xff

    .line 155
    const/4 v4, 0x1

    shl-int/2addr v4, v0

    move v0, v1

    .line 156
    :goto_1
    add-int v5, v1, v4

    if-ge v0, v5, :cond_3

    .line 157
    invoke-static {v2}, Lcom/squareup/a/a/c/p;->a(Lcom/squareup/a/a/c/p;)[Lcom/squareup/a/a/c/p;

    move-result-object v5

    aput-object v3, v5, v0

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 159
    :cond_3
    return-void
.end method

.method private a([I[B)V
    .locals 3

    .prologue
    .line 132
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 133
    aget v1, p1, v0

    aget-byte v2, p2, v0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/a/a/c/o;->a(IIB)V

    .line 132
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_0
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/a/a/c/o;
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/squareup/a/a/c/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/o;

    return-object v0
.end method

.method public static values()[Lcom/squareup/a/a/c/o;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/squareup/a/a/c/o;->f:[Lcom/squareup/a/a/c/o;

    invoke-virtual {v0}, [Lcom/squareup/a/a/c/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/a/a/c/o;

    return-object v0
.end method


# virtual methods
.method a(Lcom/squareup/a/a/b/d;)Lcom/squareup/a/a/b/d;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p1}, Lcom/squareup/a/a/b/d;->f()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/c/o;->a([B)[B

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/b/d;->a([B)Lcom/squareup/a/a/b/d;

    move-result-object v0

    return-object v0
.end method

.method a([B)[B
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 94
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/squareup/a/a/c/o;->c:Lcom/squareup/a/a/c/p;

    move v2, v0

    move-object v3, v1

    move v1, v0

    .line 98
    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_3

    .line 99
    aget-byte v5, p1, v0

    and-int/lit16 v5, v5, 0xff

    .line 100
    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v2, v5

    .line 101
    add-int/lit8 v1, v1, 0x8

    .line 102
    :goto_1
    const/16 v5, 0x8

    if-lt v1, v5, :cond_1

    .line 103
    add-int/lit8 v5, v1, -0x8

    ushr-int v5, v2, v5

    and-int/lit16 v5, v5, 0xff

    .line 104
    invoke-static {v3}, Lcom/squareup/a/a/c/p;->a(Lcom/squareup/a/a/c/p;)[Lcom/squareup/a/a/c/p;

    move-result-object v3

    aget-object v3, v3, v5

    .line 105
    invoke-static {v3}, Lcom/squareup/a/a/c/p;->a(Lcom/squareup/a/a/c/p;)[Lcom/squareup/a/a/c/p;

    move-result-object v5

    if-nez v5, :cond_0

    .line 107
    invoke-static {v3}, Lcom/squareup/a/a/c/p;->b(Lcom/squareup/a/a/c/p;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 108
    invoke-static {v3}, Lcom/squareup/a/a/c/p;->c(Lcom/squareup/a/a/c/p;)I

    move-result v3

    sub-int/2addr v1, v3

    .line 109
    iget-object v3, p0, Lcom/squareup/a/a/c/o;->c:Lcom/squareup/a/a/c/p;

    goto :goto_1

    .line 112
    :cond_0
    add-int/lit8 v1, v1, -0x8

    goto :goto_1

    .line 98
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_2
    invoke-static {v0}, Lcom/squareup/a/a/c/p;->b(Lcom/squareup/a/a/c/p;)I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 124
    invoke-static {v0}, Lcom/squareup/a/a/c/p;->c(Lcom/squareup/a/a/c/p;)I

    move-result v0

    sub-int/2addr v1, v0

    .line 125
    iget-object v3, p0, Lcom/squareup/a/a/c/o;->c:Lcom/squareup/a/a/c/p;

    .line 117
    :cond_3
    if-lez v1, :cond_4

    .line 118
    rsub-int/lit8 v0, v1, 0x8

    shl-int v0, v2, v0

    and-int/lit16 v0, v0, 0xff

    .line 119
    invoke-static {v3}, Lcom/squareup/a/a/c/p;->a(Lcom/squareup/a/a/c/p;)[Lcom/squareup/a/a/c/p;

    move-result-object v3

    aget-object v0, v3, v0

    .line 120
    invoke-static {v0}, Lcom/squareup/a/a/c/p;->a(Lcom/squareup/a/a/c/p;)[Lcom/squareup/a/a/c/p;

    move-result-object v3

    if-nez v3, :cond_4

    invoke-static {v0}, Lcom/squareup/a/a/c/p;->c(Lcom/squareup/a/a/c/p;)I

    move-result v3

    if-le v3, v1, :cond_2

    .line 128
    :cond_4
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
