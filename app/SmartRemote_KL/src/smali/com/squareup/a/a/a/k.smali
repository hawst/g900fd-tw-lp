.class public final Lcom/squareup/a/a/a/k;
.super Ljava/lang/Object;


# static fields
.field private static final g:[B

.field private static final h:[B


# instance fields
.field private final a:Lcom/squareup/a/k;

.field private final b:Lcom/squareup/a/j;

.field private final c:Lcom/squareup/a/a/b/c;

.field private final d:Lcom/squareup/a/a/b/b;

.field private e:I

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 280
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/a/a/a/k;->g:[B

    .line 283
    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/squareup/a/a/a/k;->h:[B

    return-void

    .line 280
    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
    .end array-data

    .line 283
    :array_1
    .array-data 1
        0x30t
        0xdt
        0xat
        0xdt
        0xat
    .end array-data
.end method

.method public constructor <init>(Lcom/squareup/a/k;Lcom/squareup/a/j;Lcom/squareup/a/a/b/c;Lcom/squareup/a/a/b/b;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput v0, p0, Lcom/squareup/a/a/a/k;->e:I

    .line 77
    iput v0, p0, Lcom/squareup/a/a/a/k;->f:I

    .line 81
    iput-object p1, p0, Lcom/squareup/a/a/a/k;->a:Lcom/squareup/a/k;

    .line 82
    iput-object p2, p0, Lcom/squareup/a/a/a/k;->b:Lcom/squareup/a/j;

    .line 83
    iput-object p3, p0, Lcom/squareup/a/a/a/k;->c:Lcom/squareup/a/a/b/c;

    .line 84
    iput-object p4, p0, Lcom/squareup/a/a/a/k;->d:Lcom/squareup/a/a/b/b;

    .line 85
    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/a/k;I)I
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/squareup/a/a/a/k;->e:I

    return p1
.end method

.method static synthetic a(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/b;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->d:Lcom/squareup/a/a/b/b;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/a/a/a/k;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    return v0
.end method

.method static synthetic b(Lcom/squareup/a/a/a/k;I)I
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/squareup/a/a/a/k;->f:I

    return p1
.end method

.method static synthetic c(Lcom/squareup/a/a/a/k;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/squareup/a/a/a/k;->f:I

    return v0
.end method

.method static synthetic d(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/j;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->b:Lcom/squareup/a/j;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/k;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->a:Lcom/squareup/a/k;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/a/b/c;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->c:Lcom/squareup/a/a/b/c;

    return-object v0
.end method

.method static synthetic h()[B
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/squareup/a/a/a/k;->h:[B

    return-object v0
.end method

.method static synthetic i()[B
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/squareup/a/a/a/k;->g:[B

    return-object v0
.end method


# virtual methods
.method public a(J)Lcom/squareup/a/a/b/v;
    .locals 3

    .prologue
    .line 203
    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/a/a/a/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/squareup/a/a/a/k;->e:I

    .line 205
    new-instance v0, Lcom/squareup/a/a/a/p;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/a/a/a/p;-><init>(Lcom/squareup/a/a/a/k;JLcom/squareup/a/a/a/l;)V

    return-object v0
.end method

.method public a(Ljava/net/CacheRequest;)Lcom/squareup/a/a/b/w;
    .locals 3

    .prologue
    .line 237
    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/a/a/a/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 238
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/squareup/a/a/a/k;->e:I

    .line 239
    new-instance v0, Lcom/squareup/a/a/a/r;

    invoke-direct {v0, p0, p1}, Lcom/squareup/a/a/a/r;-><init>(Lcom/squareup/a/a/a/k;Ljava/net/CacheRequest;)V

    return-object v0
.end method

.method public a(Ljava/net/CacheRequest;J)Lcom/squareup/a/a/b/w;
    .locals 4

    .prologue
    .line 216
    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/a/a/a/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/squareup/a/a/a/k;->e:I

    .line 218
    new-instance v0, Lcom/squareup/a/a/a/q;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/a/a/a/q;-><init>(Lcom/squareup/a/a/a/k;Ljava/net/CacheRequest;J)V

    return-object v0
.end method

.method public a(Ljava/net/CacheRequest;Lcom/squareup/a/a/a/u;)Lcom/squareup/a/a/b/w;
    .locals 3

    .prologue
    .line 231
    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/a/a/a/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/squareup/a/a/a/k;->e:I

    .line 233
    new-instance v0, Lcom/squareup/a/a/a/o;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/a/a/a/o;-><init>(Lcom/squareup/a/a/a/k;Ljava/net/CacheRequest;Lcom/squareup/a/a/a/u;)V

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 92
    const/4 v0, 0x1

    iput v0, p0, Lcom/squareup/a/a/a/k;->f:I

    .line 95
    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    if-nez v0, :cond_0

    .line 96
    const/4 v0, 0x0

    iput v0, p0, Lcom/squareup/a/a/a/k;->f:I

    .line 97
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->a:Lcom/squareup/a/k;

    iget-object v1, p0, Lcom/squareup/a/a/a/k;->b:Lcom/squareup/a/j;

    invoke-virtual {v0, v1}, Lcom/squareup/a/k;->a(Lcom/squareup/a/j;)V

    .line 99
    :cond_0
    return-void
.end method

.method public a(Lcom/squareup/a/a/a/ar;)V
    .locals 3

    .prologue
    .line 209
    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/a/a/a/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 210
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/squareup/a/a/a/k;->e:I

    .line 211
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->d:Lcom/squareup/a/a/b/b;

    invoke-virtual {p1, v0}, Lcom/squareup/a/a/a/ar;->a(Lcom/squareup/a/a/b/b;)V

    .line 212
    return-void
.end method

.method public a(Lcom/squareup/a/a/a/f;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 130
    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/a/a/a/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->d:Lcom/squareup/a/a/b/b;

    invoke-interface {v0, p2}, Lcom/squareup/a/a/b/b;->a(Ljava/lang/String;)Lcom/squareup/a/a/b/b;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->a(Ljava/lang/String;)Lcom/squareup/a/a/b/b;

    .line 132
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/squareup/a/a/a/f;->a()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 133
    iget-object v1, p0, Lcom/squareup/a/a/a/k;->d:Lcom/squareup/a/a/b/b;

    invoke-virtual {p1, v0}, Lcom/squareup/a/a/a/f;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/a/a/b/b;->a(Ljava/lang/String;)Lcom/squareup/a/a/b/b;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-interface {v1, v2}, Lcom/squareup/a/a/b/b;->a(Ljava/lang/String;)Lcom/squareup/a/a/b/b;

    move-result-object v1

    invoke-virtual {p1, v0}, Lcom/squareup/a/a/a/f;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/a/a/b/b;->a(Ljava/lang/String;)Lcom/squareup/a/a/b/b;

    move-result-object v1

    const-string/jumbo v2, "\r\n"

    invoke-interface {v1, v2}, Lcom/squareup/a/a/b/b;->a(Ljava/lang/String;)Lcom/squareup/a/a/b/b;

    .line 132
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->d:Lcom/squareup/a/a/b/b;

    const-string/jumbo v1, "\r\n"

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/b;->a(Ljava/lang/String;)Lcom/squareup/a/a/b/b;

    .line 139
    const/4 v0, 0x1

    iput v0, p0, Lcom/squareup/a/a/a/k;->e:I

    .line 140
    return-void
.end method

.method public a(Lcom/squareup/a/a/a/h;)V
    .locals 2

    .prologue
    .line 170
    :goto_0
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->c:Lcom/squareup/a/a/b/c;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    invoke-virtual {p1, v0}, Lcom/squareup/a/a/a/h;->a(Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    goto :goto_0

    .line 173
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->b:Lcom/squareup/a/j;

    invoke-virtual {v0, p1}, Lcom/squareup/a/j;->b(Ljava/lang/Object;)V

    .line 122
    return-void
.end method

.method public a(Lcom/squareup/a/a/b/w;I)Z
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->b:Lcom/squareup/a/j;

    invoke-virtual {v0}, Lcom/squareup/a/j;->e()Ljava/net/Socket;

    move-result-object v1

    .line 184
    :try_start_0
    invoke-virtual {v1}, Ljava/net/Socket;->getSoTimeout()I

    move-result v2

    .line 185
    invoke-virtual {v1, p2}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    :try_start_1
    invoke-static {p1, p2}, Lcom/squareup/a/a/t;->a(Lcom/squareup/a/a/b/w;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 189
    :try_start_2
    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 192
    :goto_0
    return v0

    .line 189
    :catchall_0
    move-exception v0

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 191
    :catch_0
    move-exception v0

    .line 192
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x2

    iput v0, p0, Lcom/squareup/a/a/a/k;->f:I

    .line 109
    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    if-nez v0, :cond_0

    .line 110
    const/4 v0, 0x6

    iput v0, p0, Lcom/squareup/a/a/a/k;->e:I

    .line 111
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->b:Lcom/squareup/a/j;

    invoke-virtual {v0}, Lcom/squareup/a/j;->close()V

    .line 113
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 117
    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->d:Lcom/squareup/a/a/b/b;

    invoke-interface {v0}, Lcom/squareup/a/a/b/b;->a()V

    .line 126
    return-void
.end method

.method public e()Lcom/squareup/a/a/a/al;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 144
    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 145
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/a/a/a/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/k;->c:Lcom/squareup/a/a/b/c;

    invoke-interface {v0, v4}, Lcom/squareup/a/a/b/c;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 150
    new-instance v1, Lcom/squareup/a/a/a/av;

    invoke-direct {v1, v0}, Lcom/squareup/a/a/a/av;-><init>(Ljava/lang/String;)V

    .line 152
    new-instance v0, Lcom/squareup/a/a/a/al;

    invoke-direct {v0}, Lcom/squareup/a/a/a/al;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/av;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    sget-object v2, Lcom/squareup/a/a/a/aa;->e:Ljava/lang/String;

    sget-object v3, Lcom/squareup/a/v;->c:Lcom/squareup/a/v;

    iget-object v3, v3, Lcom/squareup/a/v;->d:Lcom/squareup/a/a/b/d;

    invoke-virtual {v3}, Lcom/squareup/a/a/b/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/squareup/a/a/a/al;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/al;

    move-result-object v0

    .line 156
    new-instance v2, Lcom/squareup/a/a/a/h;

    invoke-direct {v2}, Lcom/squareup/a/a/a/h;-><init>()V

    .line 157
    invoke-virtual {p0, v2}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/h;)V

    .line 158
    invoke-virtual {v2}, Lcom/squareup/a/a/a/h;->a()Lcom/squareup/a/a/a/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/f;)Lcom/squareup/a/a/a/al;

    .line 160
    invoke-virtual {v1}, Lcom/squareup/a/a/a/av;->c()I

    move-result v1

    const/16 v2, 0x64

    if-eq v1, v2, :cond_0

    .line 161
    const/4 v1, 0x4

    iput v1, p0, Lcom/squareup/a/a/a/k;->e:I

    .line 162
    return-object v0
.end method

.method public f()Lcom/squareup/a/a/b/v;
    .locals 3

    .prologue
    .line 197
    iget v0, p0, Lcom/squareup/a/a/a/k;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/a/a/a/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/squareup/a/a/a/k;->e:I

    .line 199
    new-instance v0, Lcom/squareup/a/a/a/n;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/a/a/a/n;-><init>(Lcom/squareup/a/a/a/k;Lcom/squareup/a/a/a/l;)V

    return-object v0
.end method

.method public g()V
    .locals 4

    .prologue
    .line 226
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/squareup/a/a/a/k;->a(Ljava/net/CacheRequest;J)Lcom/squareup/a/a/b/w;

    .line 227
    return-void
.end method
