.class Lcom/squareup/a/a/a/au;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/w;


# instance fields
.field private final a:Lcom/squareup/a/a/c/ao;

.field private final b:Lcom/squareup/a/a/b/w;

.field private final c:Ljava/net/CacheRequest;

.field private final d:Ljava/io/OutputStream;

.field private e:Z

.field private f:Z


# direct methods
.method constructor <init>(Lcom/squareup/a/a/c/ao;Ljava/net/CacheRequest;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249
    iput-object p1, p0, Lcom/squareup/a/a/a/au;->a:Lcom/squareup/a/a/c/ao;

    .line 250
    invoke-virtual {p1}, Lcom/squareup/a/a/c/ao;->e()Lcom/squareup/a/a/b/w;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/a/a/a/au;->b:Lcom/squareup/a/a/b/w;

    .line 253
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/net/CacheRequest;->getBody()Ljava/io/OutputStream;

    move-result-object v1

    .line 254
    :goto_0
    if-nez v1, :cond_0

    move-object p2, v0

    .line 258
    :cond_0
    iput-object v1, p0, Lcom/squareup/a/a/a/au;->d:Ljava/io/OutputStream;

    .line 259
    iput-object p2, p0, Lcom/squareup/a/a/a/au;->c:Ljava/net/CacheRequest;

    .line 260
    return-void

    :cond_1
    move-object v1, v0

    .line 253
    goto :goto_0
.end method

.method private a()Z
    .locals 6

    .prologue
    .line 308
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/a/au;->a:Lcom/squareup/a/a/c/ao;

    invoke-virtual {v0}, Lcom/squareup/a/a/c/ao;->d()J

    move-result-wide v2

    .line 309
    iget-object v0, p0, Lcom/squareup/a/a/a/au;->a:Lcom/squareup/a/a/c/ao;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/a/a/c/ao;->a(J)V

    .line 310
    iget-object v0, p0, Lcom/squareup/a/a/a/au;->a:Lcom/squareup/a/a/c/ao;

    const-wide/16 v4, 0x64

    invoke-virtual {v0, v4, v5}, Lcom/squareup/a/a/c/ao;->a(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 312
    const/16 v0, 0x64

    :try_start_1
    invoke-static {p0, v0}, Lcom/squareup/a/a/t;->a(Lcom/squareup/a/a/b/w;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313
    const/4 v0, 0x1

    .line 315
    :try_start_2
    iget-object v1, p0, Lcom/squareup/a/a/a/au;->a:Lcom/squareup/a/a/c/ao;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/a/a/c/ao;->a(J)V

    .line 318
    :goto_0
    return v0

    .line 315
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/squareup/a/a/a/au;->a:Lcom/squareup/a/a/c/ao;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/a/a/c/ao;->a(J)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 317
    :catch_0
    move-exception v0

    .line 318
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public b(Lcom/squareup/a/a/b/j;J)J
    .locals 8

    .prologue
    const-wide/16 v0, -0x1

    .line 264
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 265
    :cond_0
    iget-boolean v2, p0, Lcom/squareup/a/a/a/au;->f:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :cond_1
    iget-boolean v2, p0, Lcom/squareup/a/a/a/au;->e:Z

    if-eqz v2, :cond_3

    move-wide v4, v0

    .line 281
    :cond_2
    :goto_0
    return-wide v4

    .line 268
    :cond_3
    iget-object v2, p0, Lcom/squareup/a/a/a/au;->b:Lcom/squareup/a/a/b/w;

    invoke-interface {v2, p1, p2, p3}, Lcom/squareup/a/a/b/w;->b(Lcom/squareup/a/a/b/j;J)J

    move-result-wide v4

    .line 269
    cmp-long v2, v4, v0

    if-nez v2, :cond_5

    .line 270
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/squareup/a/a/a/au;->e:Z

    .line 271
    iget-object v2, p0, Lcom/squareup/a/a/a/au;->c:Ljava/net/CacheRequest;

    if-eqz v2, :cond_4

    .line 272
    iget-object v2, p0, Lcom/squareup/a/a/a/au;->d:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    :cond_4
    move-wide v4, v0

    .line 274
    goto :goto_0

    .line 277
    :cond_5
    iget-object v0, p0, Lcom/squareup/a/a/a/au;->d:Ljava/io/OutputStream;

    if-eqz v0, :cond_2

    .line 278
    invoke-virtual {p1}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v0

    sub-long v2, v0, v4

    iget-object v6, p0, Lcom/squareup/a/a/a/au;->d:Ljava/io/OutputStream;

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/j;JJLjava/io/OutputStream;)V

    goto :goto_0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 290
    iget-boolean v0, p0, Lcom/squareup/a/a/a/au;->f:Z

    if-eqz v0, :cond_1

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 292
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/a/a/a/au;->e:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/a/a/a/au;->d:Ljava/io/OutputStream;

    if-eqz v0, :cond_2

    .line 293
    invoke-direct {p0}, Lcom/squareup/a/a/a/au;->a()Z

    .line 296
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/a/a/a/au;->f:Z

    .line 298
    iget-boolean v0, p0, Lcom/squareup/a/a/a/au;->e:Z

    if-nez v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/squareup/a/a/a/au;->a:Lcom/squareup/a/a/c/ao;

    sget-object v1, Lcom/squareup/a/a/c/a;->l:Lcom/squareup/a/a/c/a;

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/c/ao;->b(Lcom/squareup/a/a/c/a;)V

    .line 300
    iget-object v0, p0, Lcom/squareup/a/a/a/au;->c:Ljava/net/CacheRequest;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/squareup/a/a/a/au;->c:Ljava/net/CacheRequest;

    invoke-virtual {v0}, Ljava/net/CacheRequest;->abort()V

    goto :goto_0
.end method
