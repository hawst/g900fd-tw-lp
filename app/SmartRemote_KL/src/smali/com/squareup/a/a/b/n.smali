.class final Lcom/squareup/a/a/b/n;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/b/v;


# instance fields
.field final synthetic a:Ljava/io/OutputStream;

.field private b:Lcom/squareup/a/a/b/e;


# direct methods
.method constructor <init>(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 60
    iput-object p1, p0, Lcom/squareup/a/a/b/n;->a:Ljava/io/OutputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    sget-object v0, Lcom/squareup/a/a/b/e;->a:Lcom/squareup/a/a/b/e;

    iput-object v0, p0, Lcom/squareup/a/a/b/n;->b:Lcom/squareup/a/a/b/e;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/squareup/a/a/b/n;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 85
    return-void
.end method

.method public a(Lcom/squareup/a/a/b/j;J)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 65
    iget-wide v0, p1, Lcom/squareup/a/a/b/j;->b:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/a/a/b/x;->a(JJJ)V

    .line 66
    :cond_0
    :goto_0
    cmp-long v0, p2, v2

    if-lez v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/squareup/a/a/b/n;->b:Lcom/squareup/a/a/b/e;

    invoke-virtual {v0}, Lcom/squareup/a/a/b/e;->b()V

    .line 68
    iget-object v0, p1, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 69
    iget v1, v0, Lcom/squareup/a/a/b/t;->c:I

    iget v4, v0, Lcom/squareup/a/a/b/t;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v1, v4

    .line 70
    iget-object v4, p0, Lcom/squareup/a/a/b/n;->a:Ljava/io/OutputStream;

    iget-object v5, v0, Lcom/squareup/a/a/b/t;->a:[B

    iget v6, v0, Lcom/squareup/a/a/b/t;->b:I

    invoke-virtual {v4, v5, v6, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 72
    iget v4, v0, Lcom/squareup/a/a/b/t;->b:I

    add-int/2addr v4, v1

    iput v4, v0, Lcom/squareup/a/a/b/t;->b:I

    .line 73
    int-to-long v4, v1

    sub-long/2addr p2, v4

    .line 74
    iget-wide v4, p1, Lcom/squareup/a/a/b/j;->b:J

    int-to-long v6, v1

    sub-long/2addr v4, v6

    iput-wide v4, p1, Lcom/squareup/a/a/b/j;->b:J

    .line 76
    iget v1, v0, Lcom/squareup/a/a/b/t;->b:I

    iget v4, v0, Lcom/squareup/a/a/b/t;->c:I

    if-ne v1, v4, :cond_0

    .line 77
    invoke-virtual {v0}, Lcom/squareup/a/a/b/t;->a()Lcom/squareup/a/a/b/t;

    move-result-object v1

    iput-object v1, p1, Lcom/squareup/a/a/b/j;->a:Lcom/squareup/a/a/b/t;

    .line 78
    sget-object v1, Lcom/squareup/a/a/b/u;->a:Lcom/squareup/a/a/b/u;

    invoke-virtual {v1, v0}, Lcom/squareup/a/a/b/u;->a(Lcom/squareup/a/a/b/t;)V

    goto :goto_0

    .line 81
    :cond_1
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/squareup/a/a/b/n;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 89
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "sink("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/b/n;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
