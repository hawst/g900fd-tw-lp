.class final Lcom/squareup/a/a/c/i;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/squareup/a/a/b/j;


# direct methods
.method constructor <init>(Lcom/squareup/a/a/b/j;)V
    .locals 0

    .prologue
    .line 401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402
    iput-object p1, p0, Lcom/squareup/a/a/c/i;->a:Lcom/squareup/a/a/b/j;

    .line 403
    return-void
.end method


# virtual methods
.method a(III)V
    .locals 3

    .prologue
    .line 425
    if-ge p1, p2, :cond_0

    .line 426
    iget-object v0, p0, Lcom/squareup/a/a/c/i;->a:Lcom/squareup/a/a/b/j;

    or-int v1, p3, p1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/b/j;->c(I)Lcom/squareup/a/a/b/j;

    .line 441
    :goto_0
    return-void

    .line 431
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/i;->a:Lcom/squareup/a/a/b/j;

    or-int v1, p3, p2

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/b/j;->c(I)Lcom/squareup/a/a/b/j;

    .line 432
    sub-int v0, p1, p2

    .line 435
    :goto_1
    const/16 v1, 0x80

    if-lt v0, v1, :cond_1

    .line 436
    and-int/lit8 v1, v0, 0x7f

    .line 437
    iget-object v2, p0, Lcom/squareup/a/a/c/i;->a:Lcom/squareup/a/a/b/j;

    or-int/lit16 v1, v1, 0x80

    invoke-virtual {v2, v1}, Lcom/squareup/a/a/b/j;->c(I)Lcom/squareup/a/a/b/j;

    .line 438
    ushr-int/lit8 v0, v0, 0x7

    .line 439
    goto :goto_1

    .line 440
    :cond_1
    iget-object v1, p0, Lcom/squareup/a/a/c/i;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v1, v0}, Lcom/squareup/a/a/b/j;->c(I)Lcom/squareup/a/a/b/j;

    goto :goto_0
.end method

.method a(Lcom/squareup/a/a/b/d;)V
    .locals 3

    .prologue
    .line 444
    invoke-virtual {p1}, Lcom/squareup/a/a/b/d;->e()I

    move-result v0

    const/16 v1, 0x7f

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/a/a/c/i;->a(III)V

    .line 445
    iget-object v0, p0, Lcom/squareup/a/a/c/i;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/b/j;->b(Lcom/squareup/a/a/b/d;)Lcom/squareup/a/a/b/j;

    .line 446
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v4, 0x40

    .line 407
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 408
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/e;

    iget-object v3, v0, Lcom/squareup/a/a/c/e;->h:Lcom/squareup/a/a/b/d;

    .line 409
    invoke-static {}, Lcom/squareup/a/a/c/g;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 410
    if-eqz v0, :cond_0

    .line 412
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/16 v3, 0x3f

    invoke-virtual {p0, v0, v3, v4}, Lcom/squareup/a/a/c/i;->a(III)V

    .line 413
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/e;

    iget-object v0, v0, Lcom/squareup/a/a/c/e;->i:Lcom/squareup/a/a/b/d;

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/c/i;->a(Lcom/squareup/a/a/b/d;)V

    .line 407
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/i;->a:Lcom/squareup/a/a/b/j;

    invoke-virtual {v0, v4}, Lcom/squareup/a/a/b/j;->c(I)Lcom/squareup/a/a/b/j;

    .line 416
    invoke-virtual {p0, v3}, Lcom/squareup/a/a/c/i;->a(Lcom/squareup/a/a/b/d;)V

    .line 417
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/a/a/c/e;

    iget-object v0, v0, Lcom/squareup/a/a/c/e;->i:Lcom/squareup/a/a/b/d;

    invoke-virtual {p0, v0}, Lcom/squareup/a/a/c/i;->a(Lcom/squareup/a/a/b/d;)V

    goto :goto_1

    .line 420
    :cond_1
    return-void
.end method
