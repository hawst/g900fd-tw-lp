.class Lcom/squareup/a/a/a/m;
.super Ljava/lang/Object;


# instance fields
.field protected final a:Ljava/io/OutputStream;

.field protected b:Z

.field final synthetic c:Lcom/squareup/a/a/a/k;

.field private final d:Ljava/net/CacheRequest;


# direct methods
.method constructor <init>(Lcom/squareup/a/a/a/k;Ljava/net/CacheRequest;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 339
    iput-object p1, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/net/CacheRequest;->getBody()Ljava/io/OutputStream;

    move-result-object v1

    .line 342
    :goto_0
    if-nez v1, :cond_0

    move-object p2, v0

    .line 346
    :cond_0
    iput-object v1, p0, Lcom/squareup/a/a/a/m;->a:Ljava/io/OutputStream;

    .line 347
    iput-object p2, p0, Lcom/squareup/a/a/a/m;->d:Ljava/net/CacheRequest;

    .line 348
    return-void

    :cond_1
    move-object v1, v0

    .line 341
    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->d:Ljava/net/CacheRequest;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->d:Ljava/net/CacheRequest;

    invoke-virtual {v0}, Ljava/net/CacheRequest;->abort()V

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->d(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/j;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/t;->a(Ljava/io/Closeable;)V

    .line 395
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/k;I)I

    .line 396
    return-void
.end method

.method protected final a(Lcom/squareup/a/a/b/j;J)V
    .locals 8

    .prologue
    .line 352
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->a:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 353
    invoke-virtual {p1}, Lcom/squareup/a/a/b/j;->l()J

    move-result-wide v0

    sub-long v2, v0, p2

    iget-object v6, p0, Lcom/squareup/a/a/a/m;->a:Ljava/io/OutputStream;

    move-object v1, p1

    move-wide v4, p2

    invoke-static/range {v1 .. v6}, Lcom/squareup/a/a/b/m;->a(Lcom/squareup/a/a/b/j;JJLjava/io/OutputStream;)V

    .line 355
    :cond_0
    return-void
.end method

.method protected final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 362
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->b(Lcom/squareup/a/a/a/k;)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    invoke-static {v2}, Lcom/squareup/a/a/a/k;->b(Lcom/squareup/a/a/a/k;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->d:Ljava/net/CacheRequest;

    if-eqz v0, :cond_1

    .line 365
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 368
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    invoke-static {v0, v2}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/k;I)I

    .line 369
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->c(Lcom/squareup/a/a/a/k;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 370
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    invoke-static {v0, v2}, Lcom/squareup/a/a/a/k;->b(Lcom/squareup/a/a/a/k;I)I

    .line 371
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->e(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    invoke-static {v1}, Lcom/squareup/a/a/a/k;->d(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/k;->a(Lcom/squareup/a/j;)V

    .line 376
    :cond_2
    :goto_0
    return-void

    .line 372
    :cond_3
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->c(Lcom/squareup/a/a/a/k;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 373
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/squareup/a/a/a/k;->a(Lcom/squareup/a/a/a/k;I)I

    .line 374
    iget-object v0, p0, Lcom/squareup/a/a/a/m;->c:Lcom/squareup/a/a/a/k;

    invoke-static {v0}, Lcom/squareup/a/a/a/k;->d(Lcom/squareup/a/a/a/k;)Lcom/squareup/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/j;->close()V

    goto :goto_0
.end method
