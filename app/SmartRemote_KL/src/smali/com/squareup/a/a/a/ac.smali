.class public final Lcom/squareup/a/a/a/ac;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/net/URL;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/squareup/a/a/a/f;

.field private final d:Lcom/squareup/a/a/a/ae;

.field private final e:Ljava/lang/Object;

.field private volatile f:Lcom/squareup/a/a/a/ag;

.field private volatile g:Ljava/net/URI;

.field private volatile h:Lcom/squareup/a/i;


# direct methods
.method private constructor <init>(Lcom/squareup/a/a/a/af;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Lcom/squareup/a/a/a/af;->a(Lcom/squareup/a/a/a/af;)Ljava/net/URL;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ac;->a:Ljava/net/URL;

    .line 51
    invoke-static {p1}, Lcom/squareup/a/a/a/af;->b(Lcom/squareup/a/a/a/af;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ac;->b:Ljava/lang/String;

    .line 52
    invoke-static {p1}, Lcom/squareup/a/a/a/af;->c(Lcom/squareup/a/a/a/af;)Lcom/squareup/a/a/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/h;->a()Lcom/squareup/a/a/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ac;->c:Lcom/squareup/a/a/a/f;

    .line 53
    invoke-static {p1}, Lcom/squareup/a/a/a/af;->d(Lcom/squareup/a/a/a/af;)Lcom/squareup/a/a/a/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ac;->d:Lcom/squareup/a/a/a/ae;

    .line 54
    invoke-static {p1}, Lcom/squareup/a/a/a/af;->e(Lcom/squareup/a/a/a/af;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/a/a/a/af;->e(Lcom/squareup/a/a/a/af;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/squareup/a/a/a/ac;->e:Ljava/lang/Object;

    .line 55
    return-void

    :cond_0
    move-object v0, p0

    .line 54
    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/squareup/a/a/a/af;Lcom/squareup/a/a/a/ad;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/a/a/a/ac;-><init>(Lcom/squareup/a/a/a/af;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/a/a/a/ac;)Ljava/net/URL;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->a:Ljava/net/URL;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/a/a/a/ac;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/ae;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->d:Lcom/squareup/a/a/a/ae;

    return-object v0
.end method

.method static synthetic d(Lcom/squareup/a/a/a/ac;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->e:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/f;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->c:Lcom/squareup/a/a/a/f;

    return-object v0
.end method

.method private m()Lcom/squareup/a/a/a/ag;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->f:Lcom/squareup/a/a/a/ag;

    .line 116
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/a/a/a/ag;

    iget-object v1, p0, Lcom/squareup/a/a/a/ac;->c:Lcom/squareup/a/a/a/f;

    invoke-direct {v0, v1}, Lcom/squareup/a/a/a/ag;-><init>(Lcom/squareup/a/a/a/f;)V

    iput-object v0, p0, Lcom/squareup/a/a/a/ac;->f:Lcom/squareup/a/a/a/ag;

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->c:Lcom/squareup/a/a/a/f;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/net/URL;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->a:Ljava/net/URL;

    return-object v0
.end method

.method public b()Ljava/net/URI;
    .locals 2

    .prologue
    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->g:Ljava/net/URI;

    .line 64
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/squareup/a/a/o;->a()Lcom/squareup/a/a/o;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/a/a/a/ac;->a:Ljava/net/URL;

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/o;->a(Ljava/net/URL;)Ljava/net/URI;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ac;->g:Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public b(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->c:Lcom/squareup/a/a/a/f;

    invoke-virtual {v0, p1}, Lcom/squareup/a/a/a/f;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->a:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lcom/squareup/a/a/a/f;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->c:Lcom/squareup/a/a/a/f;

    return-object v0
.end method

.method public f()Lcom/squareup/a/a/a/ae;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->d:Lcom/squareup/a/a/a/ae;

    return-object v0
.end method

.method public g()Lcom/squareup/a/a/a/af;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Lcom/squareup/a/a/a/af;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/a/a/a/af;-><init>(Lcom/squareup/a/a/a/ac;Lcom/squareup/a/a/a/ad;)V

    return-object v0
.end method

.method public h()Lcom/squareup/a/a/a/f;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->c:Lcom/squareup/a/a/a/f;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/squareup/a/a/a/ac;->m()Lcom/squareup/a/a/a/ag;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/a/ag;->a(Lcom/squareup/a/a/a/ag;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/squareup/a/a/a/ac;->m()Lcom/squareup/a/a/a/ag;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/a/a/a/ag;->b(Lcom/squareup/a/a/a/ag;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Lcom/squareup/a/i;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->h:Lcom/squareup/a/i;

    .line 125
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/a/ac;->c:Lcom/squareup/a/a/a/f;

    invoke-static {v0}, Lcom/squareup/a/i;->a(Lcom/squareup/a/a/a/f;)Lcom/squareup/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/a/a/a/ac;->h:Lcom/squareup/a/i;

    goto :goto_0
.end method

.method public l()Z
    .locals 2

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/squareup/a/a/a/ac;->a()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
