.class public Lcom/squareup/a/a/a/an;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/t;


# instance fields
.field private final a:Ljava/net/ResponseCache;


# direct methods
.method public constructor <init>(Ljava/net/ResponseCache;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/a/a/a/an;->a:Ljava/net/ResponseCache;

    .line 50
    return-void
.end method

.method private static a(Lcom/squareup/a/a/a/f;Ljava/io/InputStream;)Lcom/squareup/a/a/a/ak;
    .locals 1

    .prologue
    .line 208
    new-instance v0, Lcom/squareup/a/a/a/ao;

    invoke-direct {v0, p0, p1}, Lcom/squareup/a/a/a/ao;-><init>(Lcom/squareup/a/a/a/f;Ljava/io/InputStream;)V

    return-object v0
.end method

.method private static a(Ljava/net/CacheResponse;)Lcom/squareup/a/a/a/f;
    .locals 5

    .prologue
    .line 169
    invoke-virtual {p0}, Ljava/net/CacheResponse;->getHeaders()Ljava/util/Map;

    move-result-object v0

    .line 170
    new-instance v2, Lcom/squareup/a/a/a/h;

    invoke-direct {v2}, Lcom/squareup/a/a/a/h;-><init>()V

    .line 171
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 172
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 173
    if-eqz v1, :cond_0

    .line 177
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 178
    invoke-virtual {v2, v1, v0}, Lcom/squareup/a/a/a/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/a/a/a/h;

    goto :goto_0

    .line 181
    :cond_1
    invoke-virtual {v2}, Lcom/squareup/a/a/a/h;->a()Lcom/squareup/a/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Ljava/lang/RuntimeException;
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/squareup/a/a/a/an;->f()Ljava/lang/RuntimeException;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/net/CacheResponse;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 190
    invoke-virtual {p0}, Ljava/net/CacheResponse;->getHeaders()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 191
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 194
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private static b(Lcom/squareup/a/a/a/ai;)Ljava/net/HttpURLConnection;
    .locals 2

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/squareup/a/a/a/ai;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v0

    .line 156
    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    new-instance v0, Lcom/squareup/a/a/a/aq;

    new-instance v1, Lcom/squareup/a/a/a/ap;

    invoke-direct {v1, p0}, Lcom/squareup/a/a/a/ap;-><init>(Lcom/squareup/a/a/a/ai;)V

    invoke-direct {v0, v1}, Lcom/squareup/a/a/a/aq;-><init>(Lcom/squareup/a/a/a/ap;)V

    .line 159
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/a/a/a/ap;

    invoke-direct {v0, p0}, Lcom/squareup/a/a/a/ap;-><init>(Lcom/squareup/a/a/a/ai;)V

    goto :goto_0
.end method

.method static synthetic c()Ljava/lang/RuntimeException;
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/squareup/a/a/a/an;->g()Ljava/lang/RuntimeException;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/squareup/a/a/a/ac;)Ljava/net/CacheResponse;
    .locals 4

    .prologue
    .line 146
    invoke-static {p1}, Lcom/squareup/a/a/a/an;->d(Lcom/squareup/a/a/a/ac;)Ljava/util/Map;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/squareup/a/a/a/an;->a:Ljava/net/ResponseCache;

    invoke-virtual {p1}, Lcom/squareup/a/a/a/ac;->b()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/a/a/a/ac;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Ljava/net/ResponseCache;->get(Ljava/net/URI;Ljava/lang/String;Ljava/util/Map;)Ljava/net/CacheResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d()Ljava/lang/RuntimeException;
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/squareup/a/a/a/an;->i()Ljava/lang/RuntimeException;

    move-result-object v0

    return-object v0
.end method

.method private static d(Lcom/squareup/a/a/a/ac;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/a/a/a/ac;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/squareup/a/a/a/ac;->e()Lcom/squareup/a/a/a/f;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/a/a/a/aa;->a(Lcom/squareup/a/a/a/f;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e()Ljava/lang/RuntimeException;
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/squareup/a/a/a/an;->h()Ljava/lang/RuntimeException;

    move-result-object v0

    return-object v0
.end method

.method private static f()Ljava/lang/RuntimeException;
    .locals 2

    .prologue
    .line 540
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "ResponseCache cannot modify the request."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static g()Ljava/lang/RuntimeException;
    .locals 2

    .prologue
    .line 544
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "ResponseCache cannot access request headers"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static h()Ljava/lang/RuntimeException;
    .locals 2

    .prologue
    .line 548
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "ResponseCache cannot access SSL internals"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static i()Ljava/lang/RuntimeException;
    .locals 2

    .prologue
    .line 552
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "ResponseCache cannot access the response body."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/ai;
    .locals 4

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/a/a/a/an;->c(Lcom/squareup/a/a/a/ac;)Ljava/net/CacheResponse;

    move-result-object v0

    .line 59
    if-nez v0, :cond_0

    .line 60
    const/4 v0, 0x0

    .line 102
    :goto_0
    return-object v0

    .line 63
    :cond_0
    new-instance v3, Lcom/squareup/a/a/a/al;

    invoke-direct {v3}, Lcom/squareup/a/a/a/al;-><init>()V

    .line 66
    invoke-virtual {v3, p1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/ac;)Lcom/squareup/a/a/a/al;

    .line 69
    invoke-static {v0}, Lcom/squareup/a/a/a/an;->b(Ljava/net/CacheResponse;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/squareup/a/a/a/al;->a(Ljava/lang/String;)Lcom/squareup/a/a/a/al;

    .line 72
    invoke-static {v0}, Lcom/squareup/a/a/a/an;->a(Ljava/net/CacheResponse;)Lcom/squareup/a/a/a/f;

    move-result-object v1

    .line 73
    invoke-virtual {v3, v1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/f;)Lcom/squareup/a/a/a/al;

    .line 76
    sget-object v2, Lcom/squareup/a/w;->a:Lcom/squareup/a/w;

    invoke-virtual {v3, v2}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/w;)Lcom/squareup/a/a/a/al;

    .line 79
    invoke-virtual {v0}, Ljava/net/CacheResponse;->getBody()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/a/a/a/an;->a(Lcom/squareup/a/a/a/f;Ljava/io/InputStream;)Lcom/squareup/a/a/a/ak;

    move-result-object v1

    .line 80
    invoke-virtual {v3, v1}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/a/a/ak;)Lcom/squareup/a/a/a/al;

    .line 83
    instance-of v1, v0, Ljava/net/SecureCacheResponse;

    if-eqz v1, :cond_2

    .line 84
    check-cast v0, Ljava/net/SecureCacheResponse;

    .line 89
    :try_start_0
    invoke-virtual {v0}, Ljava/net/SecureCacheResponse;->getServerCertificateChain()Ljava/util/List;
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 93
    :goto_1
    invoke-virtual {v0}, Ljava/net/SecureCacheResponse;->getLocalCertificateChain()Ljava/util/List;

    move-result-object v2

    .line 94
    if-nez v2, :cond_1

    .line 95
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 97
    :cond_1
    invoke-virtual {v0}, Ljava/net/SecureCacheResponse;->getCipherSuite()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1, v2}, Lcom/squareup/a/m;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/a/m;

    move-result-object v0

    .line 99
    invoke-virtual {v3, v0}, Lcom/squareup/a/a/a/al;->a(Lcom/squareup/a/m;)Lcom/squareup/a/a/a/al;

    .line 102
    :cond_2
    invoke-virtual {v3}, Lcom/squareup/a/a/a/al;->a()Lcom/squareup/a/a/a/ai;

    move-result-object v0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v1

    .line 91
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_1
.end method

.method public a(Lcom/squareup/a/a/a/ai;)Ljava/net/CacheRequest;
    .locals 3

    .prologue
    .line 107
    invoke-virtual {p1}, Lcom/squareup/a/a/a/ai;->a()Lcom/squareup/a/a/a/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/a/a/a/ac;->b()Ljava/net/URI;

    move-result-object v0

    .line 108
    invoke-static {p1}, Lcom/squareup/a/a/a/an;->b(Lcom/squareup/a/a/a/ai;)Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 109
    iget-object v2, p0, Lcom/squareup/a/a/a/an;->a:Ljava/net/ResponseCache;

    invoke-virtual {v2, v0, v1}, Ljava/net/ResponseCache;->put(Ljava/net/URI;Ljava/net/URLConnection;)Ljava/net/CacheRequest;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 134
    return-void
.end method

.method public a(Lcom/squareup/a/a/a/ai;Lcom/squareup/a/a/a/ai;)V
    .locals 0

    .prologue
    .line 129
    return-void
.end method

.method public a(Lcom/squareup/a/w;)V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method public b(Lcom/squareup/a/a/a/ac;)Z
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method
