.class final Lcom/squareup/a/a/c/l;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/a/a/c/b;


# instance fields
.field final a:Lcom/squareup/a/a/c/h;

.field private final b:Lcom/squareup/a/a/b/c;

.field private final c:Lcom/squareup/a/a/c/k;

.field private final d:Z


# direct methods
.method constructor <init>(Lcom/squareup/a/a/b/c;IZ)V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    .line 81
    iput-boolean p3, p0, Lcom/squareup/a/a/c/l;->d:Z

    .line 82
    new-instance v0, Lcom/squareup/a/a/c/k;

    iget-object v1, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-direct {v0, v1}, Lcom/squareup/a/a/c/k;-><init>(Lcom/squareup/a/a/b/c;)V

    iput-object v0, p0, Lcom/squareup/a/a/c/l;->c:Lcom/squareup/a/a/c/k;

    .line 83
    new-instance v0, Lcom/squareup/a/a/c/h;

    iget-object v1, p0, Lcom/squareup/a/a/c/l;->c:Lcom/squareup/a/a/c/k;

    invoke-direct {v0, p3, p2, v1}, Lcom/squareup/a/a/c/h;-><init>(ZILcom/squareup/a/a/b/w;)V

    iput-object v0, p0, Lcom/squareup/a/a/c/l;->a:Lcom/squareup/a/a/c/h;

    .line 84
    return-void
.end method

.method private a(SBI)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SBI)",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/a/a/c/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->c:Lcom/squareup/a/a/c/k;

    iget-object v1, p0, Lcom/squareup/a/a/c/l;->c:Lcom/squareup/a/a/c/k;

    iput p1, v1, Lcom/squareup/a/a/c/k;->d:I

    iput p1, v0, Lcom/squareup/a/a/c/k;->a:I

    .line 176
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->c:Lcom/squareup/a/a/c/k;

    iput-byte p2, v0, Lcom/squareup/a/a/c/k;->b:B

    .line 177
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->c:Lcom/squareup/a/a/c/k;

    iput p3, v0, Lcom/squareup/a/a/c/k;->c:I

    .line 179
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->a:Lcom/squareup/a/a/c/h;

    invoke-virtual {v0}, Lcom/squareup/a/a/c/h;->a()V

    .line 180
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->a:Lcom/squareup/a/a/c/h;

    invoke-virtual {v0}, Lcom/squareup/a/a/c/h;->b()V

    .line 183
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->a:Lcom/squareup/a/a/c/h;

    invoke-virtual {v0}, Lcom/squareup/a/a/c/h;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/squareup/a/a/c/c;SBI)V
    .locals 8

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 157
    if-nez p4, :cond_0

    const-string/jumbo v0, "PROTOCOL_ERROR: TYPE_HEADERS streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 159
    :cond_0
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    .line 162
    :goto_0
    and-int/lit8 v0, p3, 0x8

    if-eqz v0, :cond_2

    .line 163
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    const v3, 0x7fffffff

    and-int v5, v0, v3

    .line 164
    add-int/lit8 v0, p2, -0x4

    int-to-short p2, v0

    .line 167
    :goto_1
    invoke-direct {p0, p2, p3, p4}, Lcom/squareup/a/a/c/l;->a(SBI)Ljava/util/List;

    move-result-object v6

    .line 169
    sget-object v7, Lcom/squareup/a/a/c/f;->d:Lcom/squareup/a/a/c/f;

    move-object v0, p1

    move v3, p4

    invoke-interface/range {v0 .. v7}, Lcom/squareup/a/a/c/c;->a(ZZIIILjava/util/List;Lcom/squareup/a/a/c/f;)V

    .line 171
    return-void

    :cond_1
    move v2, v1

    .line 159
    goto :goto_0

    :cond_2
    move v5, v4

    goto :goto_1
.end method

.method private b(Lcom/squareup/a/a/c/c;SBI)V
    .locals 2

    .prologue
    .line 188
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 190
    :goto_0
    iget-object v1, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {p1, v0, p4, v1, p2}, Lcom/squareup/a/a/c/c;->a(ZILcom/squareup/a/a/b/c;I)V

    .line 191
    return-void

    .line 188
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/squareup/a/a/c/c;SBI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 195
    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const-string/jumbo v0, "TYPE_PRIORITY length: %d != 4"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 196
    :cond_0
    if-nez p4, :cond_1

    const-string/jumbo v0, "TYPE_PRIORITY streamId == 0"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    .line 199
    const v1, 0x7fffffff

    and-int/2addr v0, v1

    .line 200
    invoke-interface {p1, p4, v0}, Lcom/squareup/a/a/c/c;->a(II)V

    .line 201
    return-void
.end method

.method private d(Lcom/squareup/a/a/c/c;SBI)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 205
    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const-string/jumbo v0, "TYPE_RST_STREAM length: %d != 4"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 206
    :cond_0
    if-nez p4, :cond_1

    const-string/jumbo v0, "TYPE_RST_STREAM streamId == 0"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    .line 208
    invoke-static {v0}, Lcom/squareup/a/a/c/a;->b(I)Lcom/squareup/a/a/c/a;

    move-result-object v1

    .line 209
    if-nez v1, :cond_2

    .line 210
    const-string/jumbo v1, "TYPE_RST_STREAM unexpected error code: %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 212
    :cond_2
    invoke-interface {p1, p4, v1}, Lcom/squareup/a/a/c/c;->a(ILcom/squareup/a/a/c/a;)V

    .line 213
    return-void
.end method

.method private e(Lcom/squareup/a/a/c/c;SBI)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 217
    if-eqz p4, :cond_0

    const-string/jumbo v0, "TYPE_SETTINGS streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 218
    :cond_0
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_3

    .line 219
    if-eqz p2, :cond_1

    const-string/jumbo v0, "FRAME_SIZE_ERROR ack frame should be empty!"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 220
    :cond_1
    invoke-interface {p1}, Lcom/squareup/a/a/c/c;->b()V

    .line 237
    :cond_2
    :goto_0
    return-void

    .line 224
    :cond_3
    rem-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_4

    const-string/jumbo v0, "TYPE_SETTINGS length %% 8 != 0: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 225
    :cond_4
    new-instance v2, Lcom/squareup/a/a/c/y;

    invoke-direct {v2}, Lcom/squareup/a/a/c/y;-><init>()V

    move v0, v1

    .line 226
    :goto_1
    if-ge v0, p2, :cond_5

    .line 227
    iget-object v3, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v3}, Lcom/squareup/a/a/b/c;->i()I

    move-result v3

    .line 228
    iget-object v4, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v4}, Lcom/squareup/a/a/b/c;->i()I

    move-result v4

    .line 230
    const v5, 0xffffff

    and-int/2addr v3, v5

    .line 231
    invoke-virtual {v2, v3, v1, v4}, Lcom/squareup/a/a/c/y;->a(III)Lcom/squareup/a/a/c/y;

    .line 226
    add-int/lit8 v0, v0, 0x8

    goto :goto_1

    .line 233
    :cond_5
    invoke-interface {p1, v1, v2}, Lcom/squareup/a/a/c/c;->a(ZLcom/squareup/a/a/c/y;)V

    .line 234
    invoke-virtual {v2}, Lcom/squareup/a/a/c/y;->c()I

    move-result v0

    if-ltz v0, :cond_2

    .line 235
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->a:Lcom/squareup/a/a/c/h;

    invoke-virtual {v2}, Lcom/squareup/a/a/c/y;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/a/a/c/h;->a(I)V

    goto :goto_0
.end method

.method private f(Lcom/squareup/a/a/c/c;SBI)V
    .locals 2

    .prologue
    .line 241
    if-nez p4, :cond_0

    .line 242
    const-string/jumbo v0, "PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    .line 245
    add-int/lit8 v1, p2, -0x4

    int-to-short v1, v1

    .line 246
    invoke-direct {p0, v1, p3, p4}, Lcom/squareup/a/a/c/l;->a(SBI)Ljava/util/List;

    move-result-object v1

    .line 247
    invoke-interface {p1, p4, v0, v1}, Lcom/squareup/a/a/c/c;->a(IILjava/util/List;)V

    .line 248
    return-void
.end method

.method private g(Lcom/squareup/a/a/c/c;SBI)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 252
    const/16 v2, 0x8

    if-eq p2, v2, :cond_0

    const-string/jumbo v2, "TYPE_PING length != 8: %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v2, v0}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 253
    :cond_0
    if-eqz p4, :cond_1

    const-string/jumbo v0, "TYPE_PING streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 254
    :cond_1
    iget-object v2, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v2}, Lcom/squareup/a/a/b/c;->i()I

    move-result v2

    .line 255
    iget-object v3, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v3}, Lcom/squareup/a/a/b/c;->i()I

    move-result v3

    .line 256
    and-int/lit8 v4, p3, 0x1

    if-eqz v4, :cond_2

    .line 257
    :goto_0
    invoke-interface {p1, v0, v2, v3}, Lcom/squareup/a/a/c/c;->a(ZII)V

    .line 258
    return-void

    :cond_2
    move v0, v1

    .line 256
    goto :goto_0
.end method

.method private h(Lcom/squareup/a/a/c/c;SBI)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 262
    const/16 v0, 0x8

    if-ge p2, v0, :cond_0

    const-string/jumbo v0, "TYPE_GOAWAY length < 8: %s"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 263
    :cond_0
    if-eqz p4, :cond_1

    const-string/jumbo v0, "TYPE_GOAWAY streamId != 0"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v1

    .line 265
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    .line 266
    add-int/lit8 v2, p2, -0x8

    .line 267
    invoke-static {v0}, Lcom/squareup/a/a/c/a;->b(I)Lcom/squareup/a/a/c/a;

    move-result-object v3

    .line 268
    if-nez v3, :cond_2

    .line 269
    const-string/jumbo v1, "TYPE_GOAWAY unexpected error code: %d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 271
    :cond_2
    sget-object v0, Lcom/squareup/a/a/b/d;->a:Lcom/squareup/a/a/b/d;

    .line 272
    if-lez v2, :cond_3

    .line 273
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    int-to-long v4, v2

    invoke-interface {v0, v4, v5}, Lcom/squareup/a/a/b/c;->c(J)Lcom/squareup/a/a/b/d;

    move-result-object v0

    .line 275
    :cond_3
    invoke-interface {p1, v1, v3, v0}, Lcom/squareup/a/a/c/c;->a(ILcom/squareup/a/a/c/a;Lcom/squareup/a/a/b/d;)V

    .line 276
    return-void
.end method

.method private i(Lcom/squareup/a/a/c/c;SBI)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 280
    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const-string/jumbo v0, "TYPE_WINDOW_UPDATE length !=4: %s"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    .line 282
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "windowSizeIncrement was 0"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 283
    :cond_1
    invoke-interface {p1, p4, v0, v1}, Lcom/squareup/a/a/c/c;->a(IJ)V

    .line 284
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/squareup/a/a/c/l;->d:Z

    if-eqz v0, :cond_1

    .line 92
    :cond_0
    return-void

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-static {}, Lcom/squareup/a/a/c/j;->b()Lcom/squareup/a/a/b/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/a/a/b/d;->e()I

    move-result v1

    int-to-long v2, v1

    invoke-interface {v0, v2, v3}, Lcom/squareup/a/a/b/c;->c(J)Lcom/squareup/a/a/b/d;

    move-result-object v0

    .line 89
    invoke-static {}, Lcom/squareup/a/a/c/j;->b()Lcom/squareup/a/a/b/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/a/a/b/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 90
    const-string/jumbo v1, "Expected a connection header but was %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/squareup/a/a/b/d;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/squareup/a/a/c/j;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public a(Lcom/squareup/a/a/c/c;)Z
    .locals 5

    .prologue
    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->i()I

    move-result v0

    .line 99
    iget-object v1, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v1}, Lcom/squareup/a/a/b/c;->i()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 105
    const/high16 v2, 0x3fff0000    # 1.9921875f

    and-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x10

    int-to-short v2, v2

    .line 106
    const v3, 0xff00

    and-int/2addr v3, v0

    shr-int/lit8 v3, v3, 0x8

    int-to-byte v3, v3

    .line 107
    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    .line 109
    const v4, 0x7fffffff

    and-int/2addr v1, v4

    .line 111
    packed-switch v3, :pswitch_data_0

    .line 150
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    int-to-long v2, v2

    invoke-interface {v0, v2, v3}, Lcom/squareup/a/a/b/c;->b(J)V

    .line 152
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    const/4 v0, 0x0

    goto :goto_1

    .line 113
    :pswitch_1
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/a/a/c/l;->b(Lcom/squareup/a/a/c/c;SBI)V

    goto :goto_0

    .line 117
    :pswitch_2
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/a/a/c/l;->a(Lcom/squareup/a/a/c/c;SBI)V

    goto :goto_0

    .line 121
    :pswitch_3
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/a/a/c/l;->c(Lcom/squareup/a/a/c/c;SBI)V

    goto :goto_0

    .line 125
    :pswitch_4
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/a/a/c/l;->d(Lcom/squareup/a/a/c/c;SBI)V

    goto :goto_0

    .line 129
    :pswitch_5
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/a/a/c/l;->e(Lcom/squareup/a/a/c/c;SBI)V

    goto :goto_0

    .line 133
    :pswitch_6
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/a/a/c/l;->f(Lcom/squareup/a/a/c/c;SBI)V

    goto :goto_0

    .line 137
    :pswitch_7
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/a/a/c/l;->g(Lcom/squareup/a/a/c/c;SBI)V

    goto :goto_0

    .line 141
    :pswitch_8
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/a/a/c/l;->h(Lcom/squareup/a/a/c/c;SBI)V

    goto :goto_0

    .line 145
    :pswitch_9
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/a/a/c/l;->i(Lcom/squareup/a/a/c/c;SBI)V

    goto :goto_0

    .line 111
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public close()V
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/squareup/a/a/c/l;->b:Lcom/squareup/a/a/b/c;

    invoke-interface {v0}, Lcom/squareup/a/a/b/c;->close()V

    .line 288
    return-void
.end method
