.class Lcom/squareup/a/a/i;
.super Ljava/io/FilterOutputStream;


# instance fields
.field final synthetic a:Lcom/squareup/a/a/h;


# direct methods
.method private constructor <init>(Lcom/squareup/a/a/h;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 829
    iput-object p1, p0, Lcom/squareup/a/a/i;->a:Lcom/squareup/a/a/h;

    .line 830
    invoke-direct {p0, p2}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 831
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/a/a/h;Ljava/io/OutputStream;Lcom/squareup/a/a/f;)V
    .locals 0

    .prologue
    .line 828
    invoke-direct {p0, p1, p2}, Lcom/squareup/a/a/i;-><init>(Lcom/squareup/a/a/h;Ljava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 851
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/i;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 855
    :goto_0
    return-void

    .line 852
    :catch_0
    move-exception v0

    .line 853
    iget-object v0, p0, Lcom/squareup/a/a/i;->a:Lcom/squareup/a/a/h;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/a/a/h;->a(Lcom/squareup/a/a/h;Z)Z

    goto :goto_0
.end method

.method public flush()V
    .locals 2

    .prologue
    .line 859
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/i;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 863
    :goto_0
    return-void

    .line 860
    :catch_0
    move-exception v0

    .line 861
    iget-object v0, p0, Lcom/squareup/a/a/i;->a:Lcom/squareup/a/a/h;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/a/a/h;->a(Lcom/squareup/a/a/h;Z)Z

    goto :goto_0
.end method

.method public write(I)V
    .locals 2

    .prologue
    .line 835
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/i;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 839
    :goto_0
    return-void

    .line 836
    :catch_0
    move-exception v0

    .line 837
    iget-object v0, p0, Lcom/squareup/a/a/i;->a:Lcom/squareup/a/a/h;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/a/a/h;->a(Lcom/squareup/a/a/h;Z)Z

    goto :goto_0
.end method

.method public write([BII)V
    .locals 2

    .prologue
    .line 843
    :try_start_0
    iget-object v0, p0, Lcom/squareup/a/a/i;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 847
    :goto_0
    return-void

    .line 844
    :catch_0
    move-exception v0

    .line 845
    iget-object v0, p0, Lcom/squareup/a/a/i;->a:Lcom/squareup/a/a/h;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/a/a/h;->a(Lcom/squareup/a/a/h;Z)Z

    goto :goto_0
.end method
