.class public final Lcom/squareup/a/u;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/net/URLStreamHandlerFactory;


# instance fields
.field private final a:Lcom/squareup/a/r;


# direct methods
.method public constructor <init>(Lcom/squareup/a/r;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/a/u;->a:Lcom/squareup/a/r;

    .line 29
    return-void
.end method


# virtual methods
.method public a()Lcom/squareup/a/r;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/squareup/a/u;->a:Lcom/squareup/a/r;

    return-object v0
.end method

.method public a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/squareup/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v0, p1}, Lcom/squareup/a/r;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/squareup/a/u;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/squareup/a/u;

    iget-object v1, p0, Lcom/squareup/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v1}, Lcom/squareup/a/r;->o()Lcom/squareup/a/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/a/u;-><init>(Lcom/squareup/a/r;)V

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/squareup/a/u;->b()Lcom/squareup/a/u;

    move-result-object v0

    return-object v0
.end method

.method public createURLStreamHandler(Ljava/lang/String;)Ljava/net/URLStreamHandler;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/squareup/a/u;->a:Lcom/squareup/a/r;

    invoke-virtual {v0, p1}, Lcom/squareup/a/r;->createURLStreamHandler(Ljava/lang/String;)Ljava/net/URLStreamHandler;

    move-result-object v0

    return-object v0
.end method
