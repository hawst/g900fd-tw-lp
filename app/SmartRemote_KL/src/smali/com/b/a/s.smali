.class final Lcom/b/a/s;
.super Ljava/lang/Object;


# static fields
.field static a:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static b:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static c:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static d:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static f:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static g:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static h:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static i:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static j:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static k:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static l:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static m:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static n:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 10
    new-instance v0, Lcom/b/a/t;

    const-string/jumbo v1, "alpha"

    invoke-direct {v0, v1}, Lcom/b/a/t;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->a:Lcom/b/b/c;

    .line 21
    new-instance v0, Lcom/b/a/z;

    const-string/jumbo v1, "pivotX"

    invoke-direct {v0, v1}, Lcom/b/a/z;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->b:Lcom/b/b/c;

    .line 32
    new-instance v0, Lcom/b/a/aa;

    const-string/jumbo v1, "pivotY"

    invoke-direct {v0, v1}, Lcom/b/a/aa;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->c:Lcom/b/b/c;

    .line 43
    new-instance v0, Lcom/b/a/ab;

    const-string/jumbo v1, "translationX"

    invoke-direct {v0, v1}, Lcom/b/a/ab;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->d:Lcom/b/b/c;

    .line 54
    new-instance v0, Lcom/b/a/ac;

    const-string/jumbo v1, "translationY"

    invoke-direct {v0, v1}, Lcom/b/a/ac;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->e:Lcom/b/b/c;

    .line 65
    new-instance v0, Lcom/b/a/ad;

    const-string/jumbo v1, "rotation"

    invoke-direct {v0, v1}, Lcom/b/a/ad;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->f:Lcom/b/b/c;

    .line 76
    new-instance v0, Lcom/b/a/ae;

    const-string/jumbo v1, "rotationX"

    invoke-direct {v0, v1}, Lcom/b/a/ae;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->g:Lcom/b/b/c;

    .line 87
    new-instance v0, Lcom/b/a/af;

    const-string/jumbo v1, "rotationY"

    invoke-direct {v0, v1}, Lcom/b/a/af;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->h:Lcom/b/b/c;

    .line 98
    new-instance v0, Lcom/b/a/ag;

    const-string/jumbo v1, "scaleX"

    invoke-direct {v0, v1}, Lcom/b/a/ag;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->i:Lcom/b/b/c;

    .line 109
    new-instance v0, Lcom/b/a/u;

    const-string/jumbo v1, "scaleY"

    invoke-direct {v0, v1}, Lcom/b/a/u;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->j:Lcom/b/b/c;

    .line 120
    new-instance v0, Lcom/b/a/v;

    const-string/jumbo v1, "scrollX"

    invoke-direct {v0, v1}, Lcom/b/a/v;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->k:Lcom/b/b/c;

    .line 131
    new-instance v0, Lcom/b/a/w;

    const-string/jumbo v1, "scrollY"

    invoke-direct {v0, v1}, Lcom/b/a/w;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->l:Lcom/b/b/c;

    .line 142
    new-instance v0, Lcom/b/a/x;

    const-string/jumbo v1, "x"

    invoke-direct {v0, v1}, Lcom/b/a/x;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->m:Lcom/b/b/c;

    .line 153
    new-instance v0, Lcom/b/a/y;

    const-string/jumbo v1, "y"

    invoke-direct {v0, v1}, Lcom/b/a/y;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/b/a/s;->n:Lcom/b/b/c;

    return-void
.end method
