.class public final Lcom/google/android/gms/R$string;
.super Ljava/lang/Object;


# static fields
.field public static final auth_client_needs_enabling_title:I = 0x7f0d005d

.field public static final auth_client_needs_installation_title:I = 0x7f0d005e

.field public static final auth_client_needs_update_title:I = 0x7f0d005f

.field public static final auth_client_play_services_err_notification_msg:I = 0x7f0d0060

.field public static final auth_client_requested_by_msg:I = 0x7f0d0061

.field public static final auth_client_using_bad_version_title:I = 0x7f0d0062

.field public static final common_google_play_services_enable_button:I = 0x7f0d00f1

.field public static final common_google_play_services_enable_text:I = 0x7f0d00f2

.field public static final common_google_play_services_enable_title:I = 0x7f0d00f3

.field public static final common_google_play_services_install_button:I = 0x7f0d00f4

.field public static final common_google_play_services_install_text_phone:I = 0x7f0d00f5

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0d00f6

.field public static final common_google_play_services_install_title:I = 0x7f0d00f7

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0d00f8

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0d00f9

.field public static final common_google_play_services_network_error_text:I = 0x7f0d00fa

.field public static final common_google_play_services_network_error_title:I = 0x7f0d00fb

.field public static final common_google_play_services_unknown_issue:I = 0x7f0d00fc

.field public static final common_google_play_services_unsupported_date_text:I = 0x7f0d00fd

.field public static final common_google_play_services_unsupported_text:I = 0x7f0d00fe

.field public static final common_google_play_services_unsupported_title:I = 0x7f0d00ff

.field public static final common_google_play_services_update_button:I = 0x7f0d0100

.field public static final common_google_play_services_update_text:I = 0x7f0d0101

.field public static final common_google_play_services_update_title:I = 0x7f0d0102

.field public static final common_signin_button_text:I = 0x7f0d0103

.field public static final common_signin_button_text_long:I = 0x7f0d0104

.field public static final location_client_powered_by_google:I = 0x7f0d025f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
