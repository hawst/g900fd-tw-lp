.class public final Lcom/google/android/gms/drive/internal/y;
.super Lcom/google/android/gms/internal/ke;


# static fields
.field public static final a:[Lcom/google/android/gms/drive/internal/y;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field public d:J

.field public e:J

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/drive/internal/y;

    sput-object v0, Lcom/google/android/gms/drive/internal/y;->a:[Lcom/google/android/gms/drive/internal/y;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, -0x1

    invoke-direct {p0}, Lcom/google/android/gms/internal/ke;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/y;->b:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/y;->c:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/gms/drive/internal/y;->d:J

    iput-wide v2, p0, Lcom/google/android/gms/drive/internal/y;->e:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/y;->f:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/drive/internal/y;->f:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/internal/y;->b()I

    :cond_0
    iget v0, p0, Lcom/google/android/gms/drive/internal/y;->f:I

    return v0
.end method

.method public a(Lcom/google/android/gms/internal/jy;)Lcom/google/android/gms/drive/internal/y;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/jy;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/kh;->a(Lcom/google/android/gms/internal/jy;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/android/gms/internal/jy;->e()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/internal/y;->b:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/android/gms/internal/jy;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/y;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/android/gms/internal/jy;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/internal/y;->d:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/android/gms/internal/jy;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/internal/y;->e:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/google/android/gms/internal/jz;)V
    .locals 4

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/drive/internal/y;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/jz;->a(II)V

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/drive/internal/y;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/jz;->a(ILjava/lang/String;)V

    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/y;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/internal/jz;->b(IJ)V

    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/y;->e:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/internal/jz;->b(IJ)V

    return-void
.end method

.method public b()I
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/drive/internal/y;->b:I

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/jz;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/drive/internal/y;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/jz;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/y;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/jz;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/y;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/jz;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/drive/internal/y;->f:I

    return v0
.end method

.method public synthetic b(Lcom/google/android/gms/internal/jy;)Lcom/google/android/gms/internal/ke;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/internal/y;->a(Lcom/google/android/gms/internal/jy;)Lcom/google/android/gms/drive/internal/y;

    move-result-object v0

    return-object v0
.end method
