.class public Lcom/google/android/gms/cast/RemoteMediaPlayer;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/cast/Cast$MessageReceivedCallback;


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Lcom/google/android/gms/internal/dv;

.field private final c:Lcom/google/android/gms/cast/RemoteMediaPlayer$a;

.field private d:Lcom/google/android/gms/cast/RemoteMediaPlayer$OnMetadataUpdatedListener;

.field private e:Lcom/google/android/gms/cast/RemoteMediaPlayer$OnStatusUpdatedListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->a:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/cast/RemoteMediaPlayer$a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/RemoteMediaPlayer$a;-><init>(Lcom/google/android/gms/cast/RemoteMediaPlayer;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->c:Lcom/google/android/gms/cast/RemoteMediaPlayer$a;

    new-instance v0, Lcom/google/android/gms/cast/RemoteMediaPlayer$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/RemoteMediaPlayer$1;-><init>(Lcom/google/android/gms/cast/RemoteMediaPlayer;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->b:Lcom/google/android/gms/internal/dv;

    iget-object v0, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->b:Lcom/google/android/gms/internal/dv;

    iget-object v1, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->c:Lcom/google/android/gms/cast/RemoteMediaPlayer$a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/dv;->a(Lcom/google/android/gms/internal/dw;)V

    return-void
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->e:Lcom/google/android/gms/cast/RemoteMediaPlayer$OnStatusUpdatedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->e:Lcom/google/android/gms/cast/RemoteMediaPlayer$OnStatusUpdatedListener;

    invoke-interface {v0}, Lcom/google/android/gms/cast/RemoteMediaPlayer$OnStatusUpdatedListener;->a()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/RemoteMediaPlayer;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->a()V

    return-void
.end method

.method private b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->d:Lcom/google/android/gms/cast/RemoteMediaPlayer$OnMetadataUpdatedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->d:Lcom/google/android/gms/cast/RemoteMediaPlayer$OnMetadataUpdatedListener;

    invoke-interface {v0}, Lcom/google/android/gms/cast/RemoteMediaPlayer$OnMetadataUpdatedListener;->a()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/cast/RemoteMediaPlayer;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/cast/RemoteMediaPlayer;->b()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->c:Lcom/google/android/gms/cast/RemoteMediaPlayer$a;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/internal/dv;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->b:Lcom/google/android/gms/internal/dv;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/RemoteMediaPlayer;->b:Lcom/google/android/gms/internal/dv;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/internal/dv;->a(Ljava/lang/String;)V

    return-void
.end method
