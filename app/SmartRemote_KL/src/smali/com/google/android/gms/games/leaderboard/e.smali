.class public final Lcom/google/android/gms/games/leaderboard/e;
.super Lcom/google/android/gms/common/data/b;

# interfaces
.implements Lcom/google/android/gms/games/leaderboard/LeaderboardScore;


# instance fields
.field private final c:Lcom/google/android/gms/games/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/data/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    new-instance v0, Lcom/google/android/gms/games/d;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/d;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/leaderboard/e;->c:Lcom/google/android/gms/games/d;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    const-string/jumbo v0, "rank"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->b(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "display_rank"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "display_score"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()J
    .locals 2

    const-string/jumbo v0, "raw_score"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->b(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public e()J
    .locals 2

    const-string/jumbo v0, "achieved_timestamp"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->b(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-static {p0, p1}, Lcom/google/android/gms/games/leaderboard/d;->a(Lcom/google/android/gms/games/leaderboard/LeaderboardScore;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "external_player_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "default_display_name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/leaderboard/e;->c:Lcom/google/android/gms/games/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/d;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public g()Landroid/net/Uri;
    .locals 1

    const-string/jumbo v0, "external_player_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "default_display_image_uri"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->g(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/leaderboard/e;->c:Lcom/google/android/gms/games/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/d;->c()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic h()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/games/leaderboard/e;->n()Lcom/google/android/gms/games/leaderboard/LeaderboardScore;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/games/leaderboard/d;->a(Lcom/google/android/gms/games/leaderboard/LeaderboardScore;)I

    move-result v0

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "external_player_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "default_display_image_url"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/leaderboard/e;->c:Lcom/google/android/gms/games/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/d;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public j()Landroid/net/Uri;
    .locals 1

    const-string/jumbo v0, "external_player_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/leaderboard/e;->c:Lcom/google/android/gms/games/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/d;->e()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "external_player_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/leaderboard/e;->c:Lcom/google/android/gms/games/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/d;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public l()Lcom/google/android/gms/games/Player;
    .locals 1

    const-string/jumbo v0, "external_player_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/leaderboard/e;->c:Lcom/google/android/gms/games/d;

    goto :goto_0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "score_tag"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/leaderboard/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcom/google/android/gms/games/leaderboard/LeaderboardScore;
    .locals 1

    new-instance v0, Lcom/google/android/gms/games/leaderboard/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/leaderboard/d;-><init>(Lcom/google/android/gms/games/leaderboard/LeaderboardScore;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/games/leaderboard/d;->b(Lcom/google/android/gms/games/leaderboard/LeaderboardScore;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
