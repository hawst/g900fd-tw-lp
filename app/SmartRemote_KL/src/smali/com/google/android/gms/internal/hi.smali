.class public Lcom/google/android/gms/internal/hi;
.super Lcom/google/android/gms/internal/eh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/eh",
        "<",
        "Lcom/google/android/gms/internal/hg;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/internal/hh;

.field private final b:Ljava/lang/String;


# direct methods
.method static synthetic a(Lcom/google/android/gms/internal/hi;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/hi;->m()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/internal/hi;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/hi;->n()Landroid/os/IInterface;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/hg;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/hg$a;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/hg;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/android/gms/internal/en;Lcom/google/android/gms/internal/eh$e;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "client_name"

    iget-object v2, p0, Lcom/google/android/gms/internal/hi;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x41f6b8

    invoke-virtual {p0}, Lcom/google/android/gms/internal/hi;->k()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/internal/en;->e(Lcom/google/android/gms/internal/em;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public a_()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/hi;->a:Lcom/google/android/gms/internal/hh;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/hi;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/hi;->a:Lcom/google/android/gms/internal/hh;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/hh;->a()V

    iget-object v0, p0, Lcom/google/android/gms/internal/hi;->a:Lcom/google/android/gms/internal/hh;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/hh;->b()V

    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/internal/eh;->a_()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected synthetic b(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/hi;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/hg;

    move-result-object v0

    return-object v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.gms.location.internal.IGoogleLocationManagerService"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    return-object v0
.end method
