.class public final Lcom/google/android/gms/plus/Plus;
.super Ljava/lang/Object;


# static fields
.field static final a:Lcom/google/android/gms/common/api/Api$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/Api$b",
            "<",
            "Lcom/google/android/gms/plus/internal/e;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/android/gms/common/api/Api;

.field public static final c:Lcom/google/android/gms/common/api/Scope;

.field public static final d:Lcom/google/android/gms/common/api/Scope;

.field public static final e:Lcom/google/android/gms/plus/Moments;

.field public static final f:Lcom/google/android/gms/plus/People;

.field public static final g:Lcom/google/android/gms/plus/Account;

.field public static final h:Lcom/google/android/gms/plus/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/gms/plus/Plus$1;

    invoke-direct {v0}, Lcom/google/android/gms/plus/Plus$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/Plus;->a:Lcom/google/android/gms/common/api/Api$b;

    new-instance v0, Lcom/google/android/gms/common/api/Api;

    sget-object v1, Lcom/google/android/gms/plus/Plus;->a:Lcom/google/android/gms/common/api/Api$b;

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/api/Api;-><init>(Lcom/google/android/gms/common/api/Api$b;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/plus/Plus;->b:Lcom/google/android/gms/common/api/Api;

    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string/jumbo v1, "https://www.googleapis.com/auth/plus.login"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/plus/Plus;->c:Lcom/google/android/gms/common/api/Scope;

    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string/jumbo v1, "https://www.googleapis.com/auth/plus.me"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/plus/Plus;->d:Lcom/google/android/gms/common/api/Scope;

    new-instance v0, Lcom/google/android/gms/internal/ik;

    sget-object v1, Lcom/google/android/gms/plus/Plus;->a:Lcom/google/android/gms/common/api/Api$b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ik;-><init>(Lcom/google/android/gms/common/api/Api$b;)V

    sput-object v0, Lcom/google/android/gms/plus/Plus;->e:Lcom/google/android/gms/plus/Moments;

    new-instance v0, Lcom/google/android/gms/internal/il;

    sget-object v1, Lcom/google/android/gms/plus/Plus;->a:Lcom/google/android/gms/common/api/Api$b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/il;-><init>(Lcom/google/android/gms/common/api/Api$b;)V

    sput-object v0, Lcom/google/android/gms/plus/Plus;->f:Lcom/google/android/gms/plus/People;

    new-instance v0, Lcom/google/android/gms/internal/ii;

    sget-object v1, Lcom/google/android/gms/plus/Plus;->a:Lcom/google/android/gms/common/api/Api$b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ii;-><init>(Lcom/google/android/gms/common/api/Api$b;)V

    sput-object v0, Lcom/google/android/gms/plus/Plus;->g:Lcom/google/android/gms/plus/Account;

    new-instance v0, Lcom/google/android/gms/internal/ij;

    sget-object v1, Lcom/google/android/gms/plus/Plus;->a:Lcom/google/android/gms/common/api/Api$b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ij;-><init>(Lcom/google/android/gms/common/api/Api$b;)V

    sput-object v0, Lcom/google/android/gms/plus/Plus;->h:Lcom/google/android/gms/plus/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
