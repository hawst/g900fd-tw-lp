.class final Lcom/google/android/gms/internal/hi$b;
.super Lcom/google/android/gms/internal/hf$a;


# instance fields
.field private a:Lcom/google/android/gms/location/LocationClient$OnAddGeofencesResultListener;

.field private b:Lcom/google/android/gms/location/LocationClient$OnRemoveGeofencesResultListener;

.field private c:Lcom/google/android/gms/internal/hi;


# virtual methods
.method public a(ILandroid/app/PendingIntent;)V
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/hi$b;->c:Lcom/google/android/gms/internal/hi;

    if-nez v0, :cond_0

    const-string/jumbo v0, "LocationClientImpl"

    const-string/jumbo v1, "onRemoveGeofencesByPendingIntentResult called multiple times"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/internal/hi$b;->c:Lcom/google/android/gms/internal/hi;

    new-instance v0, Lcom/google/android/gms/internal/hi$d;

    iget-object v1, p0, Lcom/google/android/gms/internal/hi$b;->c:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/internal/hi$b;->b:Lcom/google/android/gms/location/LocationClient$OnRemoveGeofencesResultListener;

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/hi$d;-><init>(Lcom/google/android/gms/internal/hi;ILcom/google/android/gms/location/LocationClient$OnRemoveGeofencesResultListener;ILandroid/app/PendingIntent;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/internal/hi;->a(Lcom/google/android/gms/internal/eh$b;)V

    iput-object v7, p0, Lcom/google/android/gms/internal/hi$b;->c:Lcom/google/android/gms/internal/hi;

    iput-object v7, p0, Lcom/google/android/gms/internal/hi$b;->a:Lcom/google/android/gms/location/LocationClient$OnAddGeofencesResultListener;

    iput-object v7, p0, Lcom/google/android/gms/internal/hi$b;->b:Lcom/google/android/gms/location/LocationClient$OnRemoveGeofencesResultListener;

    goto :goto_0
.end method

.method public a(I[Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/hi$b;->c:Lcom/google/android/gms/internal/hi;

    if-nez v0, :cond_0

    const-string/jumbo v0, "LocationClientImpl"

    const-string/jumbo v1, "onAddGeofenceResult called multiple times"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/hi$b;->c:Lcom/google/android/gms/internal/hi;

    new-instance v1, Lcom/google/android/gms/internal/hi$a;

    iget-object v2, p0, Lcom/google/android/gms/internal/hi$b;->c:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v3, p0, Lcom/google/android/gms/internal/hi$b;->a:Lcom/google/android/gms/location/LocationClient$OnAddGeofencesResultListener;

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/google/android/gms/internal/hi$a;-><init>(Lcom/google/android/gms/internal/hi;Lcom/google/android/gms/location/LocationClient$OnAddGeofencesResultListener;I[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/hi;->a(Lcom/google/android/gms/internal/eh$b;)V

    iput-object v4, p0, Lcom/google/android/gms/internal/hi$b;->c:Lcom/google/android/gms/internal/hi;

    iput-object v4, p0, Lcom/google/android/gms/internal/hi$b;->a:Lcom/google/android/gms/location/LocationClient$OnAddGeofencesResultListener;

    iput-object v4, p0, Lcom/google/android/gms/internal/hi$b;->b:Lcom/google/android/gms/location/LocationClient$OnRemoveGeofencesResultListener;

    goto :goto_0
.end method

.method public b(I[Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/hi$b;->c:Lcom/google/android/gms/internal/hi;

    if-nez v0, :cond_0

    const-string/jumbo v0, "LocationClientImpl"

    const-string/jumbo v1, "onRemoveGeofencesByRequestIdsResult called multiple times"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/internal/hi$b;->c:Lcom/google/android/gms/internal/hi;

    new-instance v0, Lcom/google/android/gms/internal/hi$d;

    iget-object v1, p0, Lcom/google/android/gms/internal/hi$b;->c:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/internal/hi$b;->b:Lcom/google/android/gms/location/LocationClient$OnRemoveGeofencesResultListener;

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/hi$d;-><init>(Lcom/google/android/gms/internal/hi;ILcom/google/android/gms/location/LocationClient$OnRemoveGeofencesResultListener;I[Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/internal/hi;->a(Lcom/google/android/gms/internal/eh$b;)V

    iput-object v7, p0, Lcom/google/android/gms/internal/hi$b;->c:Lcom/google/android/gms/internal/hi;

    iput-object v7, p0, Lcom/google/android/gms/internal/hi$b;->a:Lcom/google/android/gms/location/LocationClient$OnAddGeofencesResultListener;

    iput-object v7, p0, Lcom/google/android/gms/internal/hi$b;->b:Lcom/google/android/gms/location/LocationClient$OnRemoveGeofencesResultListener;

    goto :goto_0
.end method
