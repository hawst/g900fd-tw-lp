.class public Lcom/google/android/gms/drive/query/SearchableField;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/google/android/gms/drive/metadata/CollectionMetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/CollectionMetadataField",
            "<",
            "Lcom/google/android/gms/drive/DriveId;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lcom/google/android/gms/drive/metadata/OrderedMetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/OrderedMetadataField",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lcom/google/android/gms/drive/metadata/OrderedMetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/OrderedMetadataField",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Lcom/google/android/gms/drive/metadata/OrderedMetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/OrderedMetadataField",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/fs;->b:Lcom/google/android/gms/drive/metadata/MetadataField;

    sput-object v0, Lcom/google/android/gms/drive/query/SearchableField;->a:Lcom/google/android/gms/drive/metadata/MetadataField;

    sget-object v0, Lcom/google/android/gms/internal/fs;->c:Lcom/google/android/gms/drive/metadata/MetadataField;

    sput-object v0, Lcom/google/android/gms/drive/query/SearchableField;->b:Lcom/google/android/gms/drive/metadata/MetadataField;

    sget-object v0, Lcom/google/android/gms/internal/fs;->e:Lcom/google/android/gms/drive/metadata/MetadataField;

    sput-object v0, Lcom/google/android/gms/drive/query/SearchableField;->c:Lcom/google/android/gms/drive/metadata/MetadataField;

    sget-object v0, Lcom/google/android/gms/internal/fs;->j:Lcom/google/android/gms/drive/metadata/CollectionMetadataField;

    sput-object v0, Lcom/google/android/gms/drive/query/SearchableField;->d:Lcom/google/android/gms/drive/metadata/CollectionMetadataField;

    sget-object v0, Lcom/google/android/gms/internal/ft;->d:Lcom/google/android/gms/drive/metadata/OrderedMetadataField;

    sput-object v0, Lcom/google/android/gms/drive/query/SearchableField;->e:Lcom/google/android/gms/drive/metadata/OrderedMetadataField;

    sget-object v0, Lcom/google/android/gms/internal/fs;->d:Lcom/google/android/gms/drive/metadata/MetadataField;

    sput-object v0, Lcom/google/android/gms/drive/query/SearchableField;->f:Lcom/google/android/gms/drive/metadata/MetadataField;

    sget-object v0, Lcom/google/android/gms/internal/ft;->a:Lcom/google/android/gms/drive/metadata/OrderedMetadataField;

    sput-object v0, Lcom/google/android/gms/drive/query/SearchableField;->g:Lcom/google/android/gms/drive/metadata/OrderedMetadataField;

    sget-object v0, Lcom/google/android/gms/internal/ft;->e:Lcom/google/android/gms/drive/metadata/OrderedMetadataField;

    sput-object v0, Lcom/google/android/gms/drive/query/SearchableField;->h:Lcom/google/android/gms/drive/metadata/OrderedMetadataField;

    sget-object v0, Lcom/google/android/gms/internal/fs;->g:Lcom/google/android/gms/drive/metadata/MetadataField;

    sput-object v0, Lcom/google/android/gms/drive/query/SearchableField;->i:Lcom/google/android/gms/drive/metadata/MetadataField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
