.class Lcom/google/android/gms/plus/PlusClient$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/a$c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/a$c",
        "<",
        "Lcom/google/android/gms/plus/People$LoadPeopleResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/PlusClient$OnPeopleLoadedListener;


# virtual methods
.method public a(Lcom/google/android/gms/plus/People$LoadPeopleResult;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient$3;->a:Lcom/google/android/gms/plus/PlusClient$OnPeopleLoadedListener;

    invoke-interface {p1}, Lcom/google/android/gms/plus/People$LoadPeopleResult;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->g()Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/plus/People$LoadPeopleResult;->h_()Lcom/google/android/gms/plus/model/people/PersonBuffer;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/plus/People$LoadPeopleResult;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/PlusClient$OnPeopleLoadedListener;->a(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/model/people/PersonBuffer;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/gms/plus/People$LoadPeopleResult;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/PlusClient$3;->a(Lcom/google/android/gms/plus/People$LoadPeopleResult;)V

    return-void
.end method
