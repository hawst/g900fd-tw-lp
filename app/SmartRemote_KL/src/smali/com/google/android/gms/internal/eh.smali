.class public abstract Lcom/google/android/gms/internal/eh;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient;
.implements Lcom/google/android/gms/common/api/Api$a;
.implements Lcom/google/android/gms/internal/ei$b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/os/IInterface;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/GooglePlayServicesClient;",
        "Lcom/google/android/gms/common/api/Api$a;",
        "Lcom/google/android/gms/internal/ei$b;"
    }
.end annotation


# static fields
.field public static final e:[Ljava/lang/String;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/os/Looper;

.field final c:Landroid/os/Handler;

.field d:Z

.field private f:Landroid/os/IInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/internal/eh",
            "<TT;>.b<*>;>;"
        }
    .end annotation
.end field

.field private h:Lcom/google/android/gms/internal/eh$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/eh",
            "<TT;>.f;"
        }
    .end annotation
.end field

.field private volatile i:I

.field private final j:[Ljava/lang/String;

.field private final k:Lcom/google/android/gms/internal/ei;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "service_googleme"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/internal/eh;->e:[Ljava/lang/String;

    return-void
.end method

.method protected varargs constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;[Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/eh;->g:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/eh;->i:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/eh;->d:Z

    invoke-static {p1}, Lcom/google/android/gms/internal/er;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/internal/eh;->a:Landroid/content/Context;

    const-string/jumbo v0, "Looper must not be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/internal/er;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, Lcom/google/android/gms/internal/eh;->b:Landroid/os/Looper;

    new-instance v0, Lcom/google/android/gms/internal/ei;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/android/gms/internal/ei;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/internal/ei$b;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/eh;->k:Lcom/google/android/gms/internal/ei;

    new-instance v0, Lcom/google/android/gms/internal/eh$a;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/internal/eh$a;-><init>(Lcom/google/android/gms/internal/eh;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/eh;->c:Landroid/os/Handler;

    invoke-virtual {p0, p5}, Lcom/google/android/gms/internal/eh;->a([Ljava/lang/String;)V

    iput-object p5, p0, Lcom/google/android/gms/internal/eh;->j:[Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/gms/internal/er;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/eh;->a(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V

    invoke-static {p4}, Lcom/google/android/gms/internal/er;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/eh;->a(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V

    return-void
.end method

.method protected varargs constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V
    .locals 6

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/internal/eh$c;

    invoke-direct {v3, p2}, Lcom/google/android/gms/internal/eh$c;-><init>(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    new-instance v4, Lcom/google/android/gms/internal/eh$g;

    invoke-direct {v4, p3}, Lcom/google/android/gms/internal/eh$g;-><init>(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/eh;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/eh;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/gms/internal/eh;->i:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/internal/eh;Landroid/os/IInterface;)Landroid/os/IInterface;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/eh;->f:Landroid/os/IInterface;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/internal/eh;Lcom/google/android/gms/internal/eh$f;)Lcom/google/android/gms/internal/eh$f;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/eh;->h:Lcom/google/android/gms/internal/eh$f;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/internal/eh;)Lcom/google/android/gms/internal/ei;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->k:Lcom/google/android/gms/internal/ei;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/eh;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/internal/eh;)Landroid/os/IInterface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->f:Landroid/os/IInterface;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/internal/eh;)Lcom/google/android/gms/internal/eh$f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->h:Lcom/google/android/gms/internal/eh$f;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/internal/eh;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    const/4 v3, 0x3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/internal/eh;->d:Z

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/internal/eh;->i:I

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    iput v1, p0, Lcom/google/android/gms/internal/eh;->i:I

    iget-object v1, p0, Lcom/google/android/gms/internal/eh;->c:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/internal/eh;->c:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->h:Lcom/google/android/gms/internal/eh$f;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "GmsClient"

    const-string/jumbo v1, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/eh;->f:Landroid/os/IInterface;

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/ej;->a(Landroid/content/Context;)Lcom/google/android/gms/internal/ej;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/eh;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/eh;->h:Lcom/google/android/gms/internal/eh$f;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/ej;->b(Ljava/lang/String;Lcom/google/android/gms/internal/eh$f;)V

    :cond_2
    new-instance v0, Lcom/google/android/gms/internal/eh$f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/eh$f;-><init>(Lcom/google/android/gms/internal/eh;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/eh;->h:Lcom/google/android/gms/internal/eh$f;

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/ej;->a(Landroid/content/Context;)Lcom/google/android/gms/internal/ej;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/eh;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/eh;->h:Lcom/google/android/gms/internal/eh$f;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/ej;->a(Ljava/lang/String;Lcom/google/android/gms/internal/eh$f;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unable to connect to service: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/eh;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/internal/eh;->c:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method protected a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/internal/eh;->c:Landroid/os/Handler;

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/gms/internal/eh$h;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/google/android/gms/internal/eh$h;-><init>(Lcom/google/android/gms/internal/eh;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->k:Lcom/google/android/gms/internal/ei;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/ei;->a(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->k:Lcom/google/android/gms/internal/ei;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/ei;->a(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/internal/eh$b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/eh",
            "<TT;>.b<*>;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/internal/eh;->g:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/internal/eh;->c:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected abstract a(Lcom/google/android/gms/internal/en;Lcom/google/android/gms/internal/eh$e;)V
.end method

.method protected varargs a([Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a_()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/eh;->d:Z

    iget-object v2, p0, Lcom/google/android/gms/internal/eh;->g:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/internal/eh;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/eh$b;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/eh$b;->e()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/eh;->i:I

    iput-object v4, p0, Lcom/google/android/gms/internal/eh;->f:Landroid/os/IInterface;

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->h:Lcom/google/android/gms/internal/eh$f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/ej;->a(Landroid/content/Context;)Lcom/google/android/gms/internal/ej;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/eh;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/eh;->h:Lcom/google/android/gms/internal/eh$f;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/ej;->b(Ljava/lang/String;Lcom/google/android/gms/internal/eh$f;)V

    iput-object v4, p0, Lcom/google/android/gms/internal/eh;->h:Lcom/google/android/gms/internal/eh$f;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract b(Landroid/os/IBinder;)Landroid/os/IInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            ")TT;"
        }
    .end annotation
.end method

.method public b(I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/internal/eh;->c:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public b_()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/eh;->d:Z

    return v0
.end method

.method protected final c(Landroid/os/IBinder;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/internal/en$a;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/en;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/internal/eh$e;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/eh$e;-><init>(Lcom/google/android/gms/internal/eh;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/eh;->a(Lcom/google/android/gms/internal/en;Lcom/google/android/gms/internal/eh$e;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "GmsClient"

    const-string/jumbo v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public c()Z
    .locals 2

    iget v0, p0, Lcom/google/android/gms/internal/eh;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->b:Landroid/os/Looper;

    return-object v0
.end method

.method protected abstract e()Ljava/lang/String;
.end method

.method protected abstract f()Ljava/lang/String;
.end method

.method public j()Z
    .locals 2

    iget v0, p0, Lcom/google/android/gms/internal/eh;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->a:Landroid/content/Context;

    return-object v0
.end method

.method public final l()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->j:[Ljava/lang/String;

    return-object v0
.end method

.method protected final m()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/eh;->c()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected final n()Landroid/os/IInterface;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/internal/eh;->m()V

    iget-object v0, p0, Lcom/google/android/gms/internal/eh;->f:Landroid/os/IInterface;

    return-object v0
.end method
