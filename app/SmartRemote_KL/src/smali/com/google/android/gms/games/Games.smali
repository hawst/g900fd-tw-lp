.class public final Lcom/google/android/gms/games/Games;
.super Ljava/lang/Object;


# static fields
.field static final a:Lcom/google/android/gms/common/api/Api$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/Api$b",
            "<",
            "Lcom/google/android/gms/internal/fx;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/android/gms/common/api/Scope;

.field public static final c:Lcom/google/android/gms/common/api/Api;

.field public static final d:Lcom/google/android/gms/common/api/Scope;

.field public static final e:Lcom/google/android/gms/common/api/Api;

.field public static final f:Lcom/google/android/gms/games/GamesMetadata;

.field public static final g:Lcom/google/android/gms/games/achievement/Achievements;

.field public static final h:Lcom/google/android/gms/games/leaderboard/Leaderboards;

.field public static final i:Lcom/google/android/gms/games/multiplayer/Invitations;

.field public static final j:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMultiplayer;

.field public static final k:Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMultiplayer;

.field public static final l:Lcom/google/android/gms/games/multiplayer/Multiplayer;

.field public static final m:Lcom/google/android/gms/games/Players;

.field public static final n:Lcom/google/android/gms/games/Notifications;

.field public static final o:Lcom/google/android/gms/games/request/Requests;

.field public static final p:Lcom/google/android/gms/internal/gw;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/gms/games/Games$1;

    invoke-direct {v0}, Lcom/google/android/gms/games/Games$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/Games;->a:Lcom/google/android/gms/common/api/Api$b;

    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string/jumbo v1, "https://www.googleapis.com/auth/games"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/games/Games;->b:Lcom/google/android/gms/common/api/Scope;

    new-instance v0, Lcom/google/android/gms/common/api/Api;

    sget-object v1, Lcom/google/android/gms/games/Games;->a:Lcom/google/android/gms/common/api/Api$b;

    new-array v2, v5, [Lcom/google/android/gms/common/api/Scope;

    sget-object v3, Lcom/google/android/gms/games/Games;->b:Lcom/google/android/gms/common/api/Scope;

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/api/Api;-><init>(Lcom/google/android/gms/common/api/Api$b;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/games/Games;->c:Lcom/google/android/gms/common/api/Api;

    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string/jumbo v1, "https://www.googleapis.com/auth/games.firstparty"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/games/Games;->d:Lcom/google/android/gms/common/api/Scope;

    new-instance v0, Lcom/google/android/gms/common/api/Api;

    sget-object v1, Lcom/google/android/gms/games/Games;->a:Lcom/google/android/gms/common/api/Api$b;

    new-array v2, v5, [Lcom/google/android/gms/common/api/Scope;

    sget-object v3, Lcom/google/android/gms/games/Games;->d:Lcom/google/android/gms/common/api/Scope;

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/api/Api;-><init>(Lcom/google/android/gms/common/api/Api$b;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/games/Games;->e:Lcom/google/android/gms/common/api/Api;

    new-instance v0, Lcom/google/android/gms/internal/gh;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gh;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/Games;->f:Lcom/google/android/gms/games/GamesMetadata;

    new-instance v0, Lcom/google/android/gms/internal/gf;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gf;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/Games;->g:Lcom/google/android/gms/games/achievement/Achievements;

    new-instance v0, Lcom/google/android/gms/internal/gj;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gj;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/Games;->h:Lcom/google/android/gms/games/leaderboard/Leaderboards;

    new-instance v0, Lcom/google/android/gms/internal/gi;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gi;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/Games;->i:Lcom/google/android/gms/games/multiplayer/Invitations;

    new-instance v0, Lcom/google/android/gms/internal/gp;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gp;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/Games;->j:Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMultiplayer;

    new-instance v0, Lcom/google/android/gms/internal/gn;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gn;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/Games;->k:Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMultiplayer;

    new-instance v0, Lcom/google/android/gms/internal/gk;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gk;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/Games;->l:Lcom/google/android/gms/games/multiplayer/Multiplayer;

    new-instance v0, Lcom/google/android/gms/internal/gm;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gm;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/Games;->m:Lcom/google/android/gms/games/Players;

    new-instance v0, Lcom/google/android/gms/internal/gl;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gl;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/Games;->n:Lcom/google/android/gms/games/Notifications;

    new-instance v0, Lcom/google/android/gms/internal/go;

    invoke-direct {v0}, Lcom/google/android/gms/internal/go;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/Games;->o:Lcom/google/android/gms/games/request/Requests;

    new-instance v0, Lcom/google/android/gms/internal/gg;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gg;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/Games;->p:Lcom/google/android/gms/internal/gw;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
