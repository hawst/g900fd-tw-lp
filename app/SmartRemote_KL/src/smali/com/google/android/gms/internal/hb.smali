.class public final Lcom/google/android/gms/internal/hb;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:I

.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "requestId"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "outcome"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/internal/hb;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(ILjava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/hb;->b:I

    iput-object p2, p0, Lcom/google/android/gms/internal/hb;->c:Ljava/util/HashMap;

    return-void
.end method

.method synthetic constructor <init>(ILjava/util/HashMap;Lcom/google/android/gms/internal/hb$1;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/hb;-><init>(ILjava/util/HashMap;)V

    return-void
.end method

.method public static a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/internal/hb;
    .locals 6

    new-instance v1, Lcom/google/android/gms/internal/hb$a;

    invoke-direct {v1}, Lcom/google/android/gms/internal/hb$a;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/common/data/DataHolder;->e()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/hb$a;->a(I)Lcom/google/android/gms/internal/hb$a;

    invoke-virtual {p0}, Lcom/google/android/gms/common/data/DataHolder;->g()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v3

    const-string/jumbo v4, "requestId"

    invoke-virtual {p0, v4, v0, v3}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "outcome"

    invoke-virtual {p0, v5, v0, v3}, Lcom/google/android/gms/common/data/DataHolder;->b(Ljava/lang/String;II)I

    move-result v3

    invoke-virtual {v1, v4, v3}, Lcom/google/android/gms/internal/hb$a;->a(Ljava/lang/String;I)Lcom/google/android/gms/internal/hb$a;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/internal/hb$a;->a()Lcom/google/android/gms/internal/hb;

    move-result-object v0

    return-object v0
.end method
