.class public final Lcom/google/android/gms/internal/iq;
.super Lcom/google/android/gms/common/data/b;

# interfaces
.implements Lcom/google/android/gms/plus/model/moments/Moment;


# instance fields
.field private c:Lcom/google/android/gms/internal/io;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/data/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    return-void
.end method

.method private b()Lcom/google/android/gms/internal/io;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/iq;->c:Lcom/google/android/gms/internal/io;

    if-nez v0, :cond_0

    const-string/jumbo v0, "momentImpl"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/iq;->f(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    sget-object v0, Lcom/google/android/gms/internal/io;->CREATOR:Lcom/google/android/gms/internal/ip;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ip;->a(Landroid/os/Parcel;)Lcom/google/android/gms/internal/io;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/iq;->c:Lcom/google/android/gms/internal/io;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/internal/iq;->c:Lcom/google/android/gms/internal/io;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()Lcom/google/android/gms/internal/io;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/iq;->b()Lcom/google/android/gms/internal/io;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/iq;->a()Lcom/google/android/gms/internal/io;

    move-result-object v0

    return-object v0
.end method
