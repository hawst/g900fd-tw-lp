.class Lcom/google/android/gms/drive/internal/o$1;
.super Lcom/google/android/gms/drive/internal/o$d;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/android/gms/drive/DriveFile$DownloadProgressListener;

.field final synthetic c:Lcom/google/android/gms/drive/internal/o;


# virtual methods
.method protected bridge synthetic a(Lcom/google/android/gms/common/api/Api$a;)V
    .locals 0

    check-cast p1, Lcom/google/android/gms/drive/internal/n;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/internal/o$1;->a(Lcom/google/android/gms/drive/internal/n;)V

    return-void
.end method

.method protected a(Lcom/google/android/gms/drive/internal/n;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/n;->g()Lcom/google/android/gms/drive/internal/u;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    iget-object v2, p0, Lcom/google/android/gms/drive/internal/o$1;->c:Lcom/google/android/gms/drive/internal/o;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/o;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/drive/internal/o$1;->a:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;-><init>(Lcom/google/android/gms/drive/DriveId;I)V

    new-instance v2, Lcom/google/android/gms/drive/internal/o$c;

    iget-object v3, p0, Lcom/google/android/gms/drive/internal/o$1;->b:Lcom/google/android/gms/drive/DriveFile$DownloadProgressListener;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/drive/internal/o$c;-><init>(Lcom/google/android/gms/common/api/a$c;Lcom/google/android/gms/drive/DriveFile$DownloadProgressListener;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/internal/u;->a(Lcom/google/android/gms/drive/internal/OpenContentsRequest;Lcom/google/android/gms/drive/internal/v;)V

    return-void
.end method
