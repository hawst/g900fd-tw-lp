.class final Lcom/google/android/gms/tagmanager/bh;
.super Ljava/lang/Object;


# static fields
.field static a:Lcom/google/android/gms/tagmanager/bi;

.field static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/tagmanager/x;

    invoke-direct {v0}, Lcom/google/android/gms/tagmanager/x;-><init>()V

    sput-object v0, Lcom/google/android/gms/tagmanager/bh;->a:Lcom/google/android/gms/tagmanager/bi;

    return-void
.end method

.method public static a()I
    .locals 1

    sget v0, Lcom/google/android/gms/tagmanager/bh;->b:I

    return v0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/tagmanager/bh;->a:Lcom/google/android/gms/tagmanager/bi;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/bi;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/tagmanager/bh;->a:Lcom/google/android/gms/tagmanager/bi;

    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/tagmanager/bi;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/tagmanager/bh;->a:Lcom/google/android/gms/tagmanager/bi;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/bi;->b(Ljava/lang/String;)V

    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/tagmanager/bh;->a:Lcom/google/android/gms/tagmanager/bi;

    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/tagmanager/bi;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static c(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/tagmanager/bh;->a:Lcom/google/android/gms/tagmanager/bi;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/bi;->c(Ljava/lang/String;)V

    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/tagmanager/bh;->a:Lcom/google/android/gms/tagmanager/bi;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/bi;->d(Ljava/lang/String;)V

    return-void
.end method

.method public static e(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/tagmanager/bh;->a:Lcom/google/android/gms/tagmanager/bi;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/bi;->e(Ljava/lang/String;)V

    return-void
.end method
