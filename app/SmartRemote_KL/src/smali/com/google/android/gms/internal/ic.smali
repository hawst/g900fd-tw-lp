.class public Lcom/google/android/gms/internal/ic;
.super Ljava/lang/Object;


# static fields
.field private static a:I

.field private static b:I


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/google/android/gms/internal/hx$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x2710

    sput v0, Lcom/google/android/gms/internal/ic;->a:I

    const/16 v0, 0x3e8

    sput v0, Lcom/google/android/gms/internal/ic;->b:I

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/internal/hx$a$a;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/ic;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/hx$a$a;->a(Ljava/lang/String;)Lcom/google/android/gms/internal/hx$a$a;

    iget v0, p0, Lcom/google/android/gms/internal/ic;->d:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/hx$a$a;->a(I)Lcom/google/android/gms/internal/hx$a$a;

    iget-object v0, p0, Lcom/google/android/gms/internal/ic;->e:Ljava/util/concurrent/BlockingQueue;

    invoke-virtual {p1}, Lcom/google/android/gms/internal/hx$a$a;->a()Lcom/google/android/gms/internal/hx$a;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    return-void
.end method
