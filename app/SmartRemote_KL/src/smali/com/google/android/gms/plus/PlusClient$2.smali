.class Lcom/google/android/gms/plus/PlusClient$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/a$c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/a$c",
        "<",
        "Lcom/google/android/gms/plus/Moments$LoadMomentsResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;


# virtual methods
.method public a(Lcom/google/android/gms/plus/Moments$LoadMomentsResult;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient$2;->a:Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;

    invoke-interface {p1}, Lcom/google/android/gms/plus/Moments$LoadMomentsResult;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->g()Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/plus/Moments$LoadMomentsResult;->e_()Lcom/google/android/gms/plus/model/moments/MomentBuffer;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/plus/Moments$LoadMomentsResult;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/gms/plus/Moments$LoadMomentsResult;->f_()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;->a(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/model/moments/MomentBuffer;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/gms/plus/Moments$LoadMomentsResult;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/PlusClient$2;->a(Lcom/google/android/gms/plus/Moments$LoadMomentsResult;)V

    return-void
.end method
