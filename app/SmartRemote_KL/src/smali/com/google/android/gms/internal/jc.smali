.class public final Lcom/google/android/gms/internal/jc;
.super Lcom/google/android/gms/common/data/b;

# interfaces
.implements Lcom/google/android/gms/plus/model/people/Person;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/data/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "displayName"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/jc;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "personId"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/jc;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/android/gms/plus/model/people/Person$Image;
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/ir$c;

    const-string/jumbo v1, "image"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/internal/jc;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ir$c;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public d()I
    .locals 1

    const-string/jumbo v0, "objectType"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/jc;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/ir$e;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "url"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/jc;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/google/android/gms/plus/model/people/Person;
    .locals 6

    new-instance v0, Lcom/google/android/gms/internal/ir;

    invoke-virtual {p0}, Lcom/google/android/gms/internal/jc;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/jc;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/jc;->c()Lcom/google/android/gms/plus/model/people/Person$Image;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/internal/ir$c;

    invoke-virtual {p0}, Lcom/google/android/gms/internal/jc;->d()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/gms/internal/jc;->e()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/ir;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/ir$c;ILjava/lang/String;)V

    return-object v0
.end method

.method public synthetic h()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/jc;->f()Lcom/google/android/gms/plus/model/people/Person;

    move-result-object v0

    return-object v0
.end method
