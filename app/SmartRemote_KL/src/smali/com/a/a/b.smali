.class public abstract Lcom/a/a/b;
.super Landroid/widget/BaseAdapter;

# interfaces
.implements Landroid/widget/SectionIndexer;
.implements Lcom/a/a/c/h;
.implements Lcom/a/a/c;


# instance fields
.field protected final a:Landroid/widget/BaseAdapter;

.field private b:Landroid/widget/AbsListView;

.field private c:Z

.field private d:I


# direct methods
.method public constructor <init>(Landroid/widget/BaseAdapter;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    .line 46
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/AbsListView;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/a/a/b;->b:Landroid/widget/AbsListView;

    return-object v0
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    instance-of v0, v0, Lcom/a/a/c/h;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    check-cast v0, Lcom/a/a/c/h;

    invoke-interface {v0, p1, p2}, Lcom/a/a/c/h;->a(II)V

    .line 190
    :cond_0
    return-void
.end method

.method public a(Landroid/widget/AbsListView;)V
    .locals 2

    .prologue
    .line 50
    iput-object p1, p0, Lcom/a/a/b;->b:Landroid/widget/AbsListView;

    .line 52
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    instance-of v0, v0, Lcom/a/a/c;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    check-cast v0, Lcom/a/a/c;

    invoke-interface {v0, p1}, Lcom/a/a/c;->a(Landroid/widget/AbsListView;)V

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/a/a/b;->b:Landroid/widget/AbsListView;

    instance-of v0, v0, Lcom/a/a/c/a;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/a/a/b;->b:Landroid/widget/AbsListView;

    check-cast v0, Lcom/a/a/c/a;

    .line 58
    iget-boolean v1, p0, Lcom/a/a/b;->c:Z

    invoke-virtual {v0, v1}, Lcom/a/a/c/a;->setIsParentHorizontalScrollContainer(Z)V

    .line 59
    iget v1, p0, Lcom/a/a/b;->d:I

    invoke-virtual {v0, v1}, Lcom/a/a/c/a;->setDynamicTouchChild(I)V

    .line 61
    :cond_1
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/BaseAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public getPositionForSection(I)I
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    instance-of v0, v0, Landroid/widget/SectionIndexer;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    check-cast v0, Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v0

    .line 162
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    instance-of v0, v0, Landroid/widget/SectionIndexer;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    check-cast v0, Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getSectionForPosition(I)I

    move-result v0

    .line 170
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    instance-of v0, v0, Landroid/widget/SectionIndexer;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    check-cast v0, Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    .line 178
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getViewTypeCount()I

    move-result v0

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    instance-of v0, v0, Lcom/a/a/a;

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 129
    :cond_0
    return-void
.end method

.method public notifyDataSetInvalidated()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    .line 145
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 150
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/a/a/b;->a:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 155
    return-void
.end method
