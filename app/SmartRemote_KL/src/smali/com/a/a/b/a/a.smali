.class public Lcom/a/a/b/a/a;
.super Lcom/a/a/b/b;


# instance fields
.field private final b:J

.field private final c:J


# direct methods
.method public constructor <init>(Landroid/widget/BaseAdapter;)V
    .locals 6

    .prologue
    .line 37
    const-wide/16 v2, 0x64

    const-wide/16 v4, 0x12c

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/a/a/b/a/a;-><init>(Landroid/widget/BaseAdapter;JJ)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/widget/BaseAdapter;JJ)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/a/a/b/b;-><init>(Landroid/widget/BaseAdapter;)V

    .line 46
    iput-wide p2, p0, Lcom/a/a/b/a/a;->b:J

    .line 47
    iput-wide p4, p0, Lcom/a/a/b/a/a;->c:J

    .line 48
    return-void
.end method


# virtual methods
.method protected b(Landroid/view/ViewGroup;Landroid/view/View;)Lcom/b/a/a;
    .locals 2

    .prologue
    .line 63
    const-string/jumbo v0, "translationY"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p2, v0, v1}, Lcom/b/a/r;->a(Ljava/lang/Object;Ljava/lang/String;[F)Lcom/b/a/r;

    move-result-object v0

    return-object v0

    :array_0
    .array-data 4
        0x43fa0000    # 500.0f
        0x0
    .end array-data
.end method

.method protected c()J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/a/a/b/a/a;->b:J

    return-wide v0
.end method

.method protected d()J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/a/a/b/a/a;->c:J

    return-wide v0
.end method
