.class public abstract Lcom/a/a/b/a;
.super Lcom/a/a/b;


# instance fields
.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/b/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private c:J

.field private d:I

.field private e:I

.field private f:Z

.field private g:Z

.field private h:J

.field private i:J

.field private j:J


# direct methods
.method public constructor <init>(Landroid/widget/BaseAdapter;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 56
    invoke-direct {p0, p1}, Lcom/a/a/b;-><init>(Landroid/widget/BaseAdapter;)V

    .line 49
    iput-boolean v3, p0, Lcom/a/a/b/a;->g:Z

    .line 51
    const-wide/16 v0, 0x96

    iput-wide v0, p0, Lcom/a/a/b/a;->h:J

    .line 52
    const-wide/16 v0, 0x64

    iput-wide v0, p0, Lcom/a/a/b/a;->i:J

    .line 53
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/a/a/b/a;->j:J

    .line 57
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/a/a/b/a;->b:Landroid/util/SparseArray;

    .line 59
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/a/a/b/a;->c:J

    .line 60
    iput v2, p0, Lcom/a/a/b/a;->d:I

    .line 61
    iput v2, p0, Lcom/a/a/b/a;->e:I

    .line 63
    instance-of v0, p1, Lcom/a/a/b/a;

    if-eqz v0, :cond_0

    .line 64
    check-cast p1, Lcom/a/a/b/a;

    invoke-virtual {p1, v3}, Lcom/a/a/b/a;->a(Z)V

    .line 66
    :cond_0
    return-void
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/a/a/b/a;->a()Landroid/widget/AbsListView;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/GridView;

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 153
    :goto_0
    iget v1, p0, Lcom/a/a/b/a;->e:I

    if-le p1, v1, :cond_1

    iget-boolean v1, p0, Lcom/a/a/b/a;->g:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 154
    iget v0, p0, Lcom/a/a/b/a;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 155
    iput p1, p0, Lcom/a/a/b/a;->d:I

    .line 158
    :cond_0
    invoke-direct {p0, p3, p2}, Lcom/a/a/b/a;->b(Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 159
    iput p1, p0, Lcom/a/a/b/a;->e:I

    .line 161
    :cond_1
    return-void

    .line 151
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 142
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    .line 143
    iget-object v0, p0, Lcom/a/a/b/a;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/a;

    .line 144
    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {v0}, Lcom/b/a/a;->c()V

    .line 146
    iget-object v0, p0, Lcom/a/a/b/a;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 148
    :cond_0
    return-void
.end method

.method private a([Lcom/b/a/a;[Lcom/b/a/a;Lcom/b/a/a;)[Lcom/b/a/a;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 189
    array-length v0, p1

    array-length v2, p2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    new-array v2, v0, [Lcom/b/a/a;

    move v0, v1

    .line 192
    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_0

    .line 193
    aget-object v3, p2, v0

    aput-object v3, v2, v0

    .line 192
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 196
    :cond_0
    array-length v3, p1

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, p1, v1

    .line 197
    aput-object v4, v2, v0

    .line 198
    add-int/lit8 v0, v0, 0x1

    .line 196
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 201
    :cond_1
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aput-object p3, v2, v0

    .line 202
    return-object v2
.end method

.method private b(Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 164
    iget-wide v0, p0, Lcom/a/a/b/a;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 165
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/b/a;->c:J

    .line 168
    :cond_0
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/b/c/a;->a(Landroid/view/View;F)V

    .line 171
    iget-object v0, p0, Lcom/a/a/b/a;->a:Landroid/widget/BaseAdapter;

    instance-of v0, v0, Lcom/a/a/b/a;

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/a/a/b/a;->a:Landroid/widget/BaseAdapter;

    check-cast v0, Lcom/a/a/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/a/a/b/a;->a(Landroid/view/ViewGroup;Landroid/view/View;)[Lcom/b/a/a;

    move-result-object v0

    .line 176
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/a/a/b/a;->a(Landroid/view/ViewGroup;Landroid/view/View;)[Lcom/b/a/a;

    move-result-object v1

    .line 177
    const-string/jumbo v2, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {p2, v2, v3}, Lcom/b/a/r;->a(Ljava/lang/Object;Ljava/lang/String;[F)Lcom/b/a/r;

    move-result-object v2

    .line 179
    new-instance v3, Lcom/b/a/d;

    invoke-direct {v3}, Lcom/b/a/d;-><init>()V

    .line 180
    invoke-direct {p0, v0, v1, v2}, Lcom/a/a/b/a;->a([Lcom/b/a/a;[Lcom/b/a/a;Lcom/b/a/a;)[Lcom/b/a/a;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/b/a/d;->a([Lcom/b/a/a;)V

    .line 181
    invoke-direct {p0}, Lcom/a/a/b/a;->e()J

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Lcom/b/a/d;->b(J)V

    .line 182
    invoke-virtual {p0}, Lcom/a/a/b/a;->d()J

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Lcom/b/a/d;->c(J)Lcom/b/a/d;

    .line 183
    invoke-virtual {v3}, Lcom/b/a/d;->a()V

    .line 185
    iget-object v0, p0, Lcom/a/a/b/a;->b:Landroid/util/SparseArray;

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 186
    return-void

    .line 174
    :cond_1
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/b/a/a;

    goto :goto_0

    .line 177
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private e()J
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/a/a/b/a;->a()Landroid/widget/AbsListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    move-result v0

    .line 210
    invoke-virtual {p0}, Lcom/a/a/b/a;->a()Landroid/widget/AbsListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v1

    .line 212
    sub-int/2addr v0, v1

    .line 213
    iget v1, p0, Lcom/a/a/b/a;->e:I

    iget v2, p0, Lcom/a/a/b/a;->d:I

    sub-int/2addr v1, v2

    .line 215
    add-int/lit8 v0, v0, 0x1

    if-ge v0, v1, :cond_0

    .line 216
    invoke-virtual {p0}, Lcom/a/a/b/a;->c()J

    move-result-wide v2

    .line 218
    invoke-virtual {p0}, Lcom/a/a/b/a;->a()Landroid/widget/AbsListView;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/GridView;

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 219
    invoke-virtual {p0}, Lcom/a/a/b/a;->c()J

    move-result-wide v4

    iget v0, p0, Lcom/a/a/b/a;->e:I

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0}, Lcom/a/a/b/a;->a()Landroid/widget/AbsListView;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getNumColumns()I

    move-result v0

    rem-int v0, v1, v0

    int-to-long v0, v0

    mul-long/2addr v0, v4

    add-long/2addr v0, v2

    .line 225
    :goto_0
    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0

    .line 222
    :cond_0
    iget v0, p0, Lcom/a/a/b/a;->e:I

    iget v1, p0, Lcom/a/a/b/a;->d:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    invoke-virtual {p0}, Lcom/a/a/b/a;->c()J

    move-result-wide v2

    mul-long/2addr v0, v2

    .line 223
    iget-wide v2, p0, Lcom/a/a/b/a;->c:J

    invoke-virtual {p0}, Lcom/a/a/b/a;->b()J

    move-result-wide v4

    add-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    goto :goto_0

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method


# virtual methods
.method public a(J)V
    .locals 1

    .prologue
    .line 250
    iput-wide p1, p0, Lcom/a/a/b/a;->h:J

    .line 251
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 235
    iput-boolean p1, p0, Lcom/a/a/b/a;->f:Z

    .line 236
    return-void
.end method

.method public abstract a(Landroid/view/ViewGroup;Landroid/view/View;)[Lcom/b/a/a;
.end method

.method protected b()J
    .locals 2

    .prologue
    .line 242
    iget-wide v0, p0, Lcom/a/a/b/a;->h:J

    return-wide v0
.end method

.method protected c()J
    .locals 2

    .prologue
    .line 257
    iget-wide v0, p0, Lcom/a/a/b/a;->i:J

    return-wide v0
.end method

.method protected d()J
    .locals 2

    .prologue
    .line 273
    iget-wide v0, p0, Lcom/a/a/b/a;->j:J

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/a/a/b/a;->f:Z

    if-nez v0, :cond_1

    .line 124
    invoke-virtual {p0}, Lcom/a/a/b/a;->a()Landroid/widget/AbsListView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 125
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Call setListView() on this AnimationAdapter before setAdapter()!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    if-eqz p2, :cond_1

    .line 129
    invoke-direct {p0, p2}, Lcom/a/a/b/a;->a(Landroid/view/View;)V

    .line 133
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/a/a/b;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 135
    iget-boolean v1, p0, Lcom/a/a/b/a;->f:Z

    if-nez v1, :cond_2

    .line 136
    invoke-direct {p0, p1, v0, p3}, Lcom/a/a/b/a;->a(ILandroid/view/View;Landroid/view/ViewGroup;)V

    .line 138
    :cond_2
    return-object v0
.end method
