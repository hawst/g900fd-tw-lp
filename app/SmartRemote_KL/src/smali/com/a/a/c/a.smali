.class public Lcom/a/a/c/a;
.super Landroid/widget/ListView;


# static fields
.field private static final B:Lcom/b/a/ak;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/a/ak",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:I

.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Z

.field private h:Z

.field private i:I

.field private j:J

.field private k:J

.field private l:J

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:Landroid/graphics/Rect;

.field private o:Landroid/graphics/Rect;

.field private p:I

.field private q:Z

.field private r:I

.field private s:Landroid/view/View$OnTouchListener;

.field private t:Z

.field private u:I

.field private v:Z

.field private w:I

.field private x:Z

.field private y:Lcom/a/a/c/f;

.field private z:Lcom/a/a/c/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 629
    new-instance v0, Lcom/a/a/c/e;

    invoke-direct {v0}, Lcom/a/a/c/e;-><init>()V

    sput-object v0, Lcom/a/a/c/a;->B:Lcom/b/a/ak;

    return-void
.end method

.method static synthetic a(Lcom/a/a/c/a;I)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/a/a/c/a;->f:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/a/a/c/a;->f:I

    return v0
.end method

.method private a(Landroid/view/View;Landroid/view/View;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 323
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-direct {v1, v0, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 324
    if-ne p1, p2, :cond_0

    move-object v0, v1

    .line 334
    :goto_0
    return-object v0

    .line 329
    :cond_0
    :goto_1
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eq v0, p1, :cond_1

    .line 330
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    move-object p2, v0

    .line 331
    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 334
    goto :goto_0
.end method

.method private a(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 7

    .prologue
    .line 223
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 224
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 225
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    .line 226
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 228
    invoke-direct {p0, p1}, Lcom/a/a/c/a;->b(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 230
    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/a/a/c/a;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 232
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v0, v3

    add-int/2addr v1, v2

    invoke-direct {v4, v3, v2, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/a/a/c/a;->o:Landroid/graphics/Rect;

    .line 233
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/a/a/c/a;->o:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/a/a/c/a;->n:Landroid/graphics/Rect;

    .line 235
    iget-object v0, p0, Lcom/a/a/c/a;->n:Landroid/graphics/Rect;

    invoke-virtual {v5, v0}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 237
    return-object v5
.end method

.method static synthetic a(Lcom/a/a/c/a;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/a/a/c/a;->m:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic a(Lcom/a/a/c/a;J)Landroid/view/View;
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/a/a/c/a;->b(J)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/a/a/c/a;)Lcom/a/a/c/g;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/a/a/c/a;->z:Lcom/a/a/c/g;

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 191
    iget v0, p0, Lcom/a/a/c/a;->e:I

    iget v1, p0, Lcom/a/a/c/a;->d:I

    invoke-virtual {p0, v0, v1}, Lcom/a/a/c/a;->pointToPosition(II)I

    move-result v0

    .line 192
    invoke-virtual {p0}, Lcom/a/a/c/a;->getFirstVisiblePosition()I

    move-result v1

    sub-int v1, v0, v1

    .line 193
    invoke-virtual {p0, v1}, Lcom/a/a/c/a;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 194
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/a/a/c/a;->getHeaderViewsCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/a/a/c/a;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    invoke-virtual {p0}, Lcom/a/a/c/a;->getHeaderViewsCount()I

    move-result v3

    sub-int/2addr v2, v3

    if-lt v0, v2, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    invoke-virtual {p0}, Lcom/a/a/c/a;->getTranscriptMode()I

    move-result v2

    iput v2, p0, Lcom/a/a/c/a;->a:I

    .line 199
    invoke-virtual {p0, v4}, Lcom/a/a/c/a;->setTranscriptMode(I)V

    .line 202
    const/4 v2, 0x0

    iput v2, p0, Lcom/a/a/c/a;->f:I

    .line 204
    invoke-virtual {p0}, Lcom/a/a/c/a;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/a/a/c/a;->k:J

    .line 205
    invoke-direct {p0, v1}, Lcom/a/a/c/a;->a(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/c/a;->m:Landroid/graphics/drawable/Drawable;

    .line 206
    iget-object v0, p0, Lcom/a/a/c/a;->y:Lcom/a/a/c/f;

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lcom/a/a/c/a;->y:Lcom/a/a/c/f;

    iget-object v2, p0, Lcom/a/a/c/a;->m:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v2}, Lcom/a/a/c/f;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/c/a;->m:Landroid/graphics/drawable/Drawable;

    .line 209
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 211
    iput-boolean v4, p0, Lcom/a/a/c/a;->g:Z

    .line 212
    invoke-virtual {p0}, Lcom/a/a/c/a;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 214
    iget-wide v0, p0, Lcom/a/a/c/a;->k:J

    invoke-direct {p0, v0, v1}, Lcom/a/a/c/a;->a(J)V

    goto :goto_0
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 534
    iput p2, p0, Lcom/a/a/c/a;->A:I

    .line 535
    invoke-virtual {p0}, Lcom/a/a/c/a;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 537
    instance-of v1, v0, Landroid/widget/HeaderViewListAdapter;

    if-eqz v1, :cond_0

    .line 538
    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 541
    :cond_0
    instance-of v1, v0, Lcom/a/a/c/h;

    if-eqz v1, :cond_1

    .line 542
    check-cast v0, Lcom/a/a/c/h;

    invoke-virtual {p0}, Lcom/a/a/c/a;->getHeaderViewsCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {p0}, Lcom/a/a/c/a;->getHeaderViewsCount()I

    move-result v2

    sub-int v2, p2, v2

    invoke-interface {v0, v1, v2}, Lcom/a/a/c/h;->a(II)V

    .line 544
    :cond_1
    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 257
    invoke-direct {p0, p1, p2}, Lcom/a/a/c/a;->c(J)I

    move-result v4

    .line 258
    invoke-virtual {p0}, Lcom/a/a/c/a;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    .line 259
    invoke-interface {v5}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Adapter doesn\'t have stable ids! Make sure your adapter has stable ids, and override hasStableIds() to return true."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_0
    add-int/lit8 v0, v4, -0x1

    if-ltz v0, :cond_2

    add-int/lit8 v0, v4, -0x1

    invoke-interface {v5, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/a/a/c/a;->j:J

    .line 264
    add-int/lit8 v0, v4, 0x1

    invoke-interface {v5}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    add-int/lit8 v0, v4, 0x1

    invoke-interface {v5, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    :cond_1
    iput-wide v2, p0, Lcom/a/a/c/a;->l:J

    .line 265
    return-void

    :cond_2
    move-wide v0, v2

    .line 263
    goto :goto_0
.end method

.method private a(Landroid/graphics/Rect;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 654
    invoke-virtual {p0}, Lcom/a/a/c/a;->computeVerticalScrollOffset()I

    move-result v2

    .line 655
    invoke-virtual {p0}, Lcom/a/a/c/a;->getHeight()I

    move-result v3

    .line 656
    invoke-virtual {p0}, Lcom/a/a/c/a;->computeVerticalScrollExtent()I

    move-result v4

    .line 657
    invoke-virtual {p0}, Lcom/a/a/c/a;->computeVerticalScrollRange()I

    move-result v5

    .line 658
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 659
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    .line 661
    if-gtz v6, :cond_0

    if-lez v2, :cond_0

    .line 662
    iget v2, p0, Lcom/a/a/c/a;->i:I

    neg-int v2, v2

    invoke-virtual {p0, v2, v1}, Lcom/a/a/c/a;->smoothScrollBy(II)V

    .line 671
    :goto_0
    return v0

    .line 666
    :cond_0
    add-int/2addr v6, v7

    if-lt v6, v3, :cond_1

    add-int/2addr v2, v4

    if-ge v2, v5, :cond_1

    .line 667
    iget v2, p0, Lcom/a/a/c/a;->i:I

    invoke-virtual {p0, v2, v1}, Lcom/a/a/c/a;->smoothScrollBy(II)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 671
    goto :goto_0
.end method

.method static synthetic b(Lcom/a/a/c/a;)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/a/a/c/a;->A:I

    return v0
.end method

.method static synthetic b(Lcom/a/a/c/a;J)J
    .locals 1

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/a/a/c/a;->j:J

    return-wide p1
.end method

.method private b(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 244
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 245
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 246
    invoke-virtual {p1, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 247
    return-object v0
.end method

.method private b(J)Landroid/view/View;
    .locals 7

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/a/a/c/a;->getFirstVisiblePosition()I

    move-result v2

    .line 272
    invoke-virtual {p0}, Lcom/a/a/c/a;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 273
    invoke-interface {v3}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-nez v0, :cond_0

    .line 274
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Adapter doesn\'t have stable ids! Make sure your adapter has stable ids, and override hasStableIds() to return true."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/a/a/c/a;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 278
    invoke-virtual {p0, v0}, Lcom/a/a/c/a;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 279
    add-int v4, v2, v0

    .line 280
    invoke-interface {v3, v4}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    .line 281
    cmp-long v4, v4, p1

    if-nez v4, :cond_1

    move-object v0, v1

    .line 285
    :goto_1
    return-object v0

    .line 277
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 285
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b()V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 465
    iget v0, p0, Lcom/a/a/c/a;->b:I

    iget v1, p0, Lcom/a/a/c/a;->d:I

    sub-int v6, v0, v1

    .line 466
    iget-object v0, p0, Lcom/a/a/c/a;->o:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/a/a/c/a;->f:I

    add-int/2addr v0, v1

    add-int v4, v0, v6

    .line 468
    iget-wide v0, p0, Lcom/a/a/c/a;->l:J

    invoke-direct {p0, v0, v1}, Lcom/a/a/c/a;->b(J)Landroid/view/View;

    move-result-object v0

    .line 469
    iget-wide v8, p0, Lcom/a/a/c/a;->k:J

    invoke-direct {p0, v8, v9}, Lcom/a/a/c/a;->b(J)Landroid/view/View;

    move-result-object v8

    .line 470
    iget-wide v10, p0, Lcom/a/a/c/a;->j:J

    invoke-direct {p0, v10, v11}, Lcom/a/a/c/a;->b(J)Landroid/view/View;

    move-result-object v1

    .line 472
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    if-le v4, v5, :cond_3

    move v7, v2

    .line 473
    :goto_0
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 475
    :goto_1
    if-nez v7, :cond_0

    if-eqz v2, :cond_2

    .line 477
    :cond_0
    if-eqz v7, :cond_5

    iget-wide v4, p0, Lcom/a/a/c/a;->l:J

    .line 478
    :goto_2
    if-eqz v7, :cond_1

    move-object v1, v0

    .line 479
    :cond_1
    invoke-virtual {p0, v8}, Lcom/a/a/c/a;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 481
    if-nez v1, :cond_6

    .line 482
    iget-wide v0, p0, Lcom/a/a/c/a;->k:J

    invoke-direct {p0, v0, v1}, Lcom/a/a/c/a;->a(J)V

    .line 531
    :cond_2
    :goto_3
    return-void

    :cond_3
    move v7, v3

    .line 472
    goto :goto_0

    :cond_4
    move v2, v3

    .line 473
    goto :goto_1

    .line 477
    :cond_5
    iget-wide v4, p0, Lcom/a/a/c/a;->j:J

    goto :goto_2

    .line 486
    :cond_6
    invoke-virtual {p0, v1}, Lcom/a/a/c/a;->getPositionForView(Landroid/view/View;)I

    move-result v2

    invoke-virtual {p0}, Lcom/a/a/c/a;->getHeaderViewsCount()I

    move-result v7

    if-lt v2, v7, :cond_2

    .line 489
    invoke-virtual {p0, v1}, Lcom/a/a/c/a;->getPositionForView(Landroid/view/View;)I

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/a/a/c/a;->a(II)V

    .line 492
    invoke-virtual {p0}, Lcom/a/a/c/a;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/HeaderViewListAdapter;

    if-eqz v0, :cond_7

    .line 493
    invoke-virtual {p0}, Lcom/a/a/c/a;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 497
    :goto_4
    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 499
    iget v0, p0, Lcom/a/a/c/a;->b:I

    iput v0, p0, Lcom/a/a/c/a;->d:I

    .line 500
    iget v0, p0, Lcom/a/a/c/a;->c:I

    iput v0, p0, Lcom/a/a/c/a;->e:I

    .line 502
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v7

    .line 504
    invoke-virtual {v8, v3}, Landroid/view/View;->setVisibility(I)V

    .line 505
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 507
    iget-wide v0, p0, Lcom/a/a/c/a;->k:J

    invoke-direct {p0, v0, v1}, Lcom/a/a/c/a;->a(J)V

    .line 509
    invoke-virtual {p0}, Lcom/a/a/c/a;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    .line 510
    new-instance v1, Lcom/a/a/c/b;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/a/a/c/b;-><init>(Lcom/a/a/c/a;Landroid/view/ViewTreeObserver;JII)V

    invoke-virtual {v3, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_3

    .line 495
    :cond_7
    invoke-virtual {p0}, Lcom/a/a/c/a;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    goto :goto_4
.end method

.method private c(J)I
    .locals 1

    .prologue
    .line 292
    invoke-direct {p0, p1, p2}, Lcom/a/a/c/a;->b(J)Landroid/view/View;

    move-result-object v0

    .line 293
    if-nez v0, :cond_0

    .line 294
    const/4 v0, -0x1

    .line 296
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/a/a/c/a;->getPositionForView(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/a/a/c/a;J)J
    .locals 1

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/a/a/c/a;->k:J

    return-wide p1
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 551
    iget-wide v0, p0, Lcom/a/a/c/a;->k:J

    invoke-direct {p0, v0, v1}, Lcom/a/a/c/a;->b(J)Landroid/view/View;

    move-result-object v0

    .line 552
    iget-boolean v1, p0, Lcom/a/a/c/a;->g:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/a/a/c/a;->q:Z

    if-eqz v1, :cond_2

    .line 553
    :cond_0
    iput-boolean v6, p0, Lcom/a/a/c/a;->g:Z

    .line 554
    iput-boolean v6, p0, Lcom/a/a/c/a;->q:Z

    .line 555
    iput-boolean v6, p0, Lcom/a/a/c/a;->h:Z

    .line 556
    const/4 v1, -0x1

    iput v1, p0, Lcom/a/a/c/a;->p:I

    .line 559
    iget v1, p0, Lcom/a/a/c/a;->a:I

    invoke-virtual {p0, v1}, Lcom/a/a/c/a;->setTranscriptMode(I)V

    .line 566
    iget v1, p0, Lcom/a/a/c/a;->r:I

    if-eqz v1, :cond_1

    .line 567
    iput-boolean v4, p0, Lcom/a/a/c/a;->q:Z

    .line 604
    :goto_0
    return-void

    .line 571
    :cond_1
    iget-object v1, p0, Lcom/a/a/c/a;->n:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/a/a/c/a;->o:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 573
    iget-object v1, p0, Lcom/a/a/c/a;->m:Landroid/graphics/drawable/Drawable;

    const-string/jumbo v2, "bounds"

    sget-object v3, Lcom/a/a/c/a;->B:Lcom/b/a/ak;

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/a/a/c/a;->n:Landroid/graphics/Rect;

    aput-object v5, v4, v6

    invoke-static {v1, v2, v3, v4}, Lcom/b/a/r;->a(Ljava/lang/Object;Ljava/lang/String;Lcom/b/a/ak;[Ljava/lang/Object;)Lcom/b/a/r;

    move-result-object v1

    .line 574
    new-instance v2, Lcom/a/a/c/c;

    invoke-direct {v2, p0}, Lcom/a/a/c/c;-><init>(Lcom/a/a/c/a;)V

    invoke-virtual {v1, v2}, Lcom/b/a/r;->a(Lcom/b/a/as;)V

    .line 580
    new-instance v2, Lcom/a/a/c/d;

    invoke-direct {v2, p0, v0}, Lcom/a/a/c/d;-><init>(Lcom/a/a/c/a;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Lcom/b/a/r;->a(Lcom/b/a/b;)V

    .line 600
    invoke-virtual {v1}, Lcom/b/a/r;->a()V

    goto :goto_0

    .line 602
    :cond_2
    invoke-direct {p0}, Lcom/a/a/c/a;->d()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/a/a/c/a;J)J
    .locals 1

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/a/a/c/a;->l:J

    return-wide p1
.end method

.method private d()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    .line 610
    iget-wide v0, p0, Lcom/a/a/c/a;->k:J

    invoke-direct {p0, v0, v1}, Lcom/a/a/c/a;->b(J)Landroid/view/View;

    move-result-object v0

    .line 611
    iget-boolean v1, p0, Lcom/a/a/c/a;->g:Z

    if-eqz v1, :cond_0

    .line 612
    iput-wide v4, p0, Lcom/a/a/c/a;->j:J

    .line 613
    iput-wide v4, p0, Lcom/a/a/c/a;->k:J

    .line 614
    iput-wide v4, p0, Lcom/a/a/c/a;->l:J

    .line 615
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 616
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/c/a;->m:Landroid/graphics/drawable/Drawable;

    .line 617
    invoke-virtual {p0}, Lcom/a/a/c/a;->invalidate()V

    .line 619
    :cond_0
    iput-boolean v2, p0, Lcom/a/a/c/a;->g:Z

    .line 620
    iput-boolean v2, p0, Lcom/a/a/c/a;->h:Z

    .line 621
    const/4 v0, -0x1

    iput v0, p0, Lcom/a/a/c/a;->p:I

    .line 622
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/a/a/c/a;->n:Landroid/graphics/Rect;

    invoke-direct {p0, v0}, Lcom/a/a/c/a;->a(Landroid/graphics/Rect;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/a/a/c/a;->h:Z

    .line 646
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 307
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 308
    iget-object v0, p0, Lcom/a/a/c/a;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/a/a/c/a;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 311
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 339
    iget-boolean v0, p0, Lcom/a/a/c/a;->x:Z

    if-eqz v0, :cond_0

    .line 340
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 452
    :goto_0
    return v0

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/a/a/c/a;->s:Landroid/view/View$OnTouchListener;

    instance-of v0, v0, Lcom/a/a/a/a/a;

    if-eqz v0, :cond_3

    .line 344
    iget-object v0, p0, Lcom/a/a/c/a;->s:Landroid/view/View$OnTouchListener;

    check-cast v0, Lcom/a/a/a/a/a;

    invoke-interface {v0}, Lcom/a/a/a/a/a;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 345
    iput-boolean v2, p0, Lcom/a/a/c/a;->x:Z

    .line 346
    iget-object v0, p0, Lcom/a/a/c/a;->s:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 347
    iput-boolean v1, p0, Lcom/a/a/c/a;->x:Z

    .line 348
    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 352
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 442
    :cond_4
    :goto_1
    :pswitch_0
    iget-boolean v0, p0, Lcom/a/a/c/a;->g:Z

    if-eqz v0, :cond_a

    move v0, v1

    .line 443
    goto :goto_0

    .line 354
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/a/a/c/a;->e:I

    .line 355
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/a/a/c/a;->d:I

    .line 356
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/a/a/c/a;->p:I

    .line 358
    iput-boolean v1, p0, Lcom/a/a/c/a;->v:Z

    .line 359
    iget v0, p0, Lcom/a/a/c/a;->u:I

    if-eqz v0, :cond_5

    .line 360
    iput-boolean v1, p0, Lcom/a/a/c/a;->t:Z

    .line 362
    iget v0, p0, Lcom/a/a/c/a;->e:I

    iget v5, p0, Lcom/a/a/c/a;->d:I

    invoke-virtual {p0, v0, v5}, Lcom/a/a/c/a;->pointToPosition(II)I

    move-result v0

    .line 363
    if-eq v0, v4, :cond_6

    invoke-virtual {p0}, Lcom/a/a/c/a;->getFirstVisiblePosition()I

    move-result v4

    sub-int/2addr v0, v4

    .line 364
    :goto_2
    if-ltz v0, :cond_7

    invoke-virtual {p0, v0}, Lcom/a/a/c/a;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 365
    :goto_3
    if-eqz v0, :cond_8

    iget v3, p0, Lcom/a/a/c/a;->u:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 366
    :goto_4
    if-eqz v0, :cond_5

    .line 367
    invoke-direct {p0, p0, v0}, Lcom/a/a/c/a;->a(Landroid/view/View;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 368
    iget v3, p0, Lcom/a/a/c/a;->e:I

    iget v4, p0, Lcom/a/a/c/a;->d:I

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 369
    iput-boolean v2, p0, Lcom/a/a/c/a;->v:Z

    .line 370
    invoke-virtual {p0}, Lcom/a/a/c/a;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 375
    :cond_5
    iget-boolean v0, p0, Lcom/a/a/c/a;->t:Z

    if-eqz v0, :cond_4

    .line 378
    invoke-virtual {p0}, Lcom/a/a/c/a;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_1

    :cond_6
    move v0, v4

    .line 363
    goto :goto_2

    :cond_7
    move-object v0, v3

    .line 364
    goto :goto_3

    :cond_8
    move-object v0, v3

    .line 365
    goto :goto_4

    .line 382
    :pswitch_2
    iget v0, p0, Lcom/a/a/c/a;->p:I

    if-eq v0, v4, :cond_4

    .line 386
    iget v0, p0, Lcom/a/a/c/a;->p:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 388
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/a/a/c/a;->b:I

    .line 389
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/a/a/c/a;->c:I

    .line 390
    iget v0, p0, Lcom/a/a/c/a;->b:I

    iget v3, p0, Lcom/a/a/c/a;->d:I

    sub-int/2addr v0, v3

    .line 391
    iget v3, p0, Lcom/a/a/c/a;->c:I

    iget v4, p0, Lcom/a/a/c/a;->e:I

    sub-int/2addr v3, v4

    .line 393
    iget-boolean v4, p0, Lcom/a/a/c/a;->g:Z

    if-nez v4, :cond_9

    iget-boolean v4, p0, Lcom/a/a/c/a;->v:Z

    if-eqz v4, :cond_9

    .line 394
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, p0, Lcom/a/a/c/a;->w:I

    if-le v4, v5, :cond_9

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-le v4, v3, :cond_9

    .line 395
    invoke-direct {p0}, Lcom/a/a/c/a;->a()V

    .line 398
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    .line 399
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v4

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x3

    invoke-virtual {v3, v4}, Landroid/view/MotionEvent;->setAction(I)V

    .line 400
    invoke-super {p0, v3}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 401
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    .line 405
    :cond_9
    iget-boolean v3, p0, Lcom/a/a/c/a;->g:Z

    if-eqz v3, :cond_4

    .line 406
    iget-object v3, p0, Lcom/a/a/c/a;->n:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/a/a/c/a;->o:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/a/a/c/a;->o:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v5

    iget v5, p0, Lcom/a/a/c/a;->f:I

    add-int/2addr v0, v5

    invoke-virtual {v3, v4, v0}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 407
    iget-object v0, p0, Lcom/a/a/c/a;->m:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/a/a/c/a;->n:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 408
    invoke-virtual {p0}, Lcom/a/a/c/a;->invalidate()V

    .line 410
    invoke-direct {p0}, Lcom/a/a/c/a;->b()V

    .line 412
    iput-boolean v1, p0, Lcom/a/a/c/a;->h:Z

    .line 413
    invoke-direct {p0}, Lcom/a/a/c/a;->e()V

    goto/16 :goto_1

    .line 417
    :pswitch_3
    iput-boolean v1, p0, Lcom/a/a/c/a;->v:Z

    .line 418
    invoke-direct {p0}, Lcom/a/a/c/a;->c()V

    goto/16 :goto_1

    .line 421
    :pswitch_4
    iput-boolean v1, p0, Lcom/a/a/c/a;->v:Z

    .line 422
    invoke-direct {p0}, Lcom/a/a/c/a;->d()V

    goto/16 :goto_1

    .line 431
    :pswitch_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v3, 0xff00

    and-int/2addr v0, v3

    shr-int/lit8 v0, v0, 0x8

    .line 432
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 433
    iget v3, p0, Lcom/a/a/c/a;->p:I

    if-ne v0, v3, :cond_4

    .line 434
    iput-boolean v1, p0, Lcom/a/a/c/a;->v:Z

    .line 435
    invoke-direct {p0}, Lcom/a/a/c/a;->c()V

    goto/16 :goto_1

    .line 444
    :cond_a
    iget-object v0, p0, Lcom/a/a/c/a;->s:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_b

    .line 445
    iput-boolean v2, p0, Lcom/a/a/c/a;->x:Z

    .line 446
    iget-object v0, p0, Lcom/a/a/c/a;->s:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 447
    iput-boolean v1, p0, Lcom/a/a/c/a;->x:Z

    .line 448
    if-eqz v0, :cond_b

    move v0, v2

    .line 449
    goto/16 :goto_0

    .line 452
    :cond_b
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 352
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 73
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/a/a/c/a;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/BaseAdapter;)V
    .locals 0

    .prologue
    .line 160
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 161
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 169
    instance-of v0, p1, Landroid/widget/BaseAdapter;

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "DynamicListView needs a BaseAdapter!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 173
    return-void
.end method

.method public setDynamicTouchChild(I)V
    .locals 1

    .prologue
    .line 683
    iput p1, p0, Lcom/a/a/c/a;->u:I

    .line 684
    if-eqz p1, :cond_0

    .line 685
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/a/a/c/a;->setIsParentHorizontalScrollContainer(Z)V

    .line 687
    :cond_0
    return-void
.end method

.method public setIsParentHorizontalScrollContainer(Z)V
    .locals 1

    .prologue
    .line 675
    iget v0, p0, Lcom/a/a/c/a;->u:I

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/a/a/c/a;->t:Z

    .line 676
    return-void

    .line 675
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnHoverCellListener(Lcom/a/a/c/f;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/a/a/c/a;->y:Lcom/a/a/c/f;

    .line 320
    return-void
.end method

.method public setOnItemMovedListener(Lcom/a/a/c/g;)V
    .locals 0

    .prologue
    .line 776
    iput-object p1, p0, Lcom/a/a/c/a;->z:Lcom/a/a/c/g;

    .line 777
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/a/a/c/a;->s:Landroid/view/View$OnTouchListener;

    .line 316
    return-void
.end method
