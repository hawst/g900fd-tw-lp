.class Lcom/a/a/c/d;
.super Lcom/b/a/c;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/a/a/c/a;


# direct methods
.method constructor <init>(Lcom/a/a/c/a;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 580
    iput-object p1, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/a;

    iput-object p2, p0, Lcom/a/a/c/d;->a:Landroid/view/View;

    invoke-direct {p0}, Lcom/b/a/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/b/a/a;)V
    .locals 2

    .prologue
    .line 583
    iget-object v0, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/a/a/c/a;->setEnabled(Z)V

    .line 584
    return-void
.end method

.method public b(Lcom/b/a/a;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 588
    iget-object v0, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/a;

    invoke-static {v0, v2, v3}, Lcom/a/a/c/a;->b(Lcom/a/a/c/a;J)J

    .line 589
    iget-object v0, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/a;

    invoke-static {v0, v2, v3}, Lcom/a/a/c/a;->c(Lcom/a/a/c/a;J)J

    .line 590
    iget-object v0, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/a;

    invoke-static {v0, v2, v3}, Lcom/a/a/c/a;->d(Lcom/a/a/c/a;J)J

    .line 591
    iget-object v0, p0, Lcom/a/a/c/d;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 592
    iget-object v0, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/a/a/c/a;->a(Lcom/a/a/c/a;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 593
    iget-object v0, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/a/a/c/a;->setEnabled(Z)V

    .line 594
    iget-object v0, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/a;

    invoke-virtual {v0}, Lcom/a/a/c/a;->invalidate()V

    .line 595
    iget-object v0, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/a;

    invoke-static {v0}, Lcom/a/a/c/a;->a(Lcom/a/a/c/a;)Lcom/a/a/c/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/a;

    invoke-static {v0}, Lcom/a/a/c/a;->a(Lcom/a/a/c/a;)Lcom/a/a/c/g;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/a;

    invoke-static {v1}, Lcom/a/a/c/a;->b(Lcom/a/a/c/a;)I

    move-result v1

    iget-object v2, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/a;

    invoke-virtual {v2}, Lcom/a/a/c/a;->getHeaderViewsCount()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-interface {v0, v1}, Lcom/a/a/c/g;->a(I)V

    .line 598
    :cond_0
    return-void
.end method
