.class Lcom/a/a/c/b;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Landroid/view/ViewTreeObserver;

.field final synthetic b:J

.field final synthetic c:I

.field final synthetic d:I

.field final synthetic e:Lcom/a/a/c/a;


# direct methods
.method constructor <init>(Lcom/a/a/c/a;Landroid/view/ViewTreeObserver;JII)V
    .locals 1

    .prologue
    .line 510
    iput-object p1, p0, Lcom/a/a/c/b;->e:Lcom/a/a/c/a;

    iput-object p2, p0, Lcom/a/a/c/b;->a:Landroid/view/ViewTreeObserver;

    iput-wide p3, p0, Lcom/a/a/c/b;->b:J

    iput p5, p0, Lcom/a/a/c/b;->c:I

    iput p6, p0, Lcom/a/a/c/b;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 512
    iget-object v0, p0, Lcom/a/a/c/b;->a:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 514
    iget-object v0, p0, Lcom/a/a/c/b;->e:Lcom/a/a/c/a;

    iget-wide v2, p0, Lcom/a/a/c/b;->b:J

    invoke-static {v0, v2, v3}, Lcom/a/a/c/a;->a(Lcom/a/a/c/a;J)Landroid/view/View;

    move-result-object v0

    .line 516
    iget-object v1, p0, Lcom/a/a/c/b;->e:Lcom/a/a/c/a;

    iget v2, p0, Lcom/a/a/c/b;->c:I

    invoke-static {v1, v2}, Lcom/a/a/c/a;->a(Lcom/a/a/c/a;I)I

    .line 518
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    .line 519
    iget v2, p0, Lcom/a/a/c/b;->d:I

    sub-int v1, v2, v1

    .line 521
    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/b/c/a;->b(Landroid/view/View;F)V

    .line 523
    const-string/jumbo v1, "translationY"

    new-array v2, v5, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/b/a/r;->a(Ljava/lang/Object;Ljava/lang/String;[F)Lcom/b/a/r;

    move-result-object v0

    .line 524
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Lcom/b/a/r;->b(J)Lcom/b/a/r;

    .line 525
    invoke-virtual {v0}, Lcom/b/a/r;->a()V

    .line 527
    return v5
.end method
